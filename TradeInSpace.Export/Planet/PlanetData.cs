﻿using Newtonsoft.Json.Linq;
using Serilog;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Xml;
using TradeInSpace.Explore;
using TradeInSpace.Explore.DataForge;
using TradeInSpace.Explore.Socpak;

namespace TradeInSpace.Export.Planet
{
    public class PlanetData
    {
        public string BaseName { get; private set; }
        public int TileCount { get; private set; }
        public float RadiusM { get; private set; }
        public float HumidityInfluence { get; private set; }
        public float TemperatureInfluence { get; private set; }

        public bool OceanEnabled { get; private set; }
        public bool OceanFrozen { get; private set; }
        public float OceanRadius { get; private set; }
        public Color OceanColor { get; private set; }

        public PlanetBrush[] Brushes { get; private set; }
        public EcosystemData[] EcoSystems { get; private set; }

        public LUTData[,] ClimateLUT { get; private set; }
        public LocalizedClimateData[,] GlobalClimateData { get; private set; }

        private PlanetData() { }

        public static PlanetData CreateFromSocpakPath(DFDatabase GameDatabase, string SocpakPath, PlanetRenderer Render = null)
        {
            SOC_Archive archive = SOC_Archive.LoadFromGameDBSocPak(GameDatabase, SocpakPath, false, null);
            var archiveNameBase = Path.GetFileNameWithoutExtension(archive.Name);
            archive.RootEntry.LoadAllEntities(true, false, false);
            JObject plaObj = JObject.Parse(archive.RootEntry.PlanetJSON);

            return CreateFromPLAJson(GameDatabase, archiveNameBase, plaObj, Render);
        }

        public RectangleF SplatToCoordinates(RectangleF SplatPixels, bool NASACoords = true)
        {
            if (NASACoords)
            {
                return new RectangleF(MathHelper.LerpValue(0f, 360f, (SplatPixels.X / TileCount)),
                                       MathHelper.LerpValue(90f, -90f, (SplatPixels.Bottom / TileCount)),
                                        SplatPixels.Width / TileCount * 360.0f,
                                        SplatPixels.Height / TileCount * 180.0f);
            }
            else
            {
                return new RectangleF(MathHelper.LerpValue(-180f, 180f, (SplatPixels.X / TileCount)),
                                       MathHelper.LerpValue(90f, -90f, (SplatPixels.Bottom / TileCount)),
                                        SplatPixels.Width / TileCount * 360.0f,
                                        SplatPixels.Height / TileCount * 180.0f);
            }
        }

        public RectangleF CoordinatesToSplat(RectangleF CoordinateBounds, bool NASACoords = true)
        {
            float xNorm, yNorm;
            if (NASACoords)
            {
                xNorm = (CoordinateBounds.X) / 360f;
            }
            else
            {
                xNorm = (CoordinateBounds.X + 180f) / 360f;
            }
            yNorm = 1.0f - ((CoordinateBounds.Bottom + 90f) / 180f);
            return new RectangleF(xNorm * TileCount, yNorm * TileCount,
                CoordinateBounds.Width / 360f * TileCount, CoordinateBounds.Height / 180f * TileCount);
        }


        public static PlanetData CreateFromPLAJson(DFDatabase GameDatabase, string BaseName, JObject PLAObject, PlanetRenderer Render = null)
        {
            if (Render == null) Render = new PlanetRenderer(GameDatabase);

            PlanetData pd = new PlanetData();

            pd.BaseName = BaseName;
            pd.TileCount = PLAObject.SelectToken("$.data.globalSplatWidth").Value<int>();
            pd.RadiusM = PLAObject.SelectToken("$.data.General.tSphere.fPlanetTerrainRadius").Value<float>();
            pd.HumidityInfluence = PLAObject.SelectToken($".data.General.textureLayers.localHumidityInfluence").Value<float>();
            pd.TemperatureInfluence = PLAObject.SelectToken($".data.General.textureLayers.localTemperatureInfluence").Value<float>();

            var oceanMaterial = PLAObject.SelectToken("$.data.oceanParams.Geometry.MaterialOceanPlanet").Value<string>();
            var oceanXML = GameDatabase.ReadPackedFileCryXML(oceanMaterial);
            if (oceanXML != null)
            {
                var oceanDiffuse = oceanXML.SelectSingleNode("./Material/@Diffuse") as XmlAttribute;
                var oceanDiffuseColorParts = oceanDiffuse.Value.Split(',')
                    .Select(p => MathHelper.ClampValue((int)(float.Parse(p, CultureInfo.InvariantCulture) * 255), 0, 255)).ToArray();

                pd.OceanColor = Color.FromArgb(oceanDiffuseColorParts[0], oceanDiffuseColorParts[1], oceanDiffuseColorParts[2]);
            }
            else
            {
                pd.OceanColor = Color.Black;
            }
            pd.OceanEnabled = PLAObject.SelectToken("$.data.oceanParams.General.Enabled").Value<bool>();
            pd.OceanFrozen = PLAObject.SelectToken("$.data.oceanParams.General.Frozen").Value<bool>();
            pd.OceanRadius = PLAObject.SelectToken("$.data.oceanParams.Geometry.OceanRadius").Value<float>();

            //Init planet climate data
            pd.GlobalClimateData = new LocalizedClimateData[pd.TileCount, pd.TileCount];
            for (int y = 0; y < pd.TileCount; y++)
                for (int x = 0; x < pd.TileCount; x++)
                    pd.GlobalClimateData[x, y] = new LocalizedClimateData() { X = x, Y = y };

            pd.Brushes =
                PLAObject.SelectTokens("$.data.General.brushes")
                .Values<JObject>()
                .Select(bjson => PlanetBrush.FromJObject(bjson))
                .ToArray();

            pd.EcoSystems =
                PLAObject.SelectToken("$.data.General.uniqueEcoSystemsGUIDs")
                .Values<string>()
                .Select(eco_id => Guid.Parse(eco_id))
                .Select(eco_id => Render.Biomes.ContainsKey(eco_id) ? Render.Biomes[eco_id] : null)
                .Where(eco => eco != null)
                .ToArray();

            Log.Information("Pre-Loading Planet Ecosystem texture data");

            int eco_num = 0;
            foreach (var eco in pd.EcoSystems)
            {
                Log.Information($"Loading: {eco.Name}");
                eco.LoadClimateData(GameDatabase);
                eco_num++;
                Render.EcosystemPreloaded(eco_num, pd.EcoSystems.Length);
            }

            Log.Information("Ecosystem textures preloaded.");

            pd.LoadSplatData(PLAObject, pd.TileCount);
            pd.LoadRandomOffsetData(PLAObject, pd.TileCount);
            pd.LoadHeightmapData(GameDatabase, PLAObject);

            pd.BuildLUT(PLAObject);

            return pd;
        }

        public IEnumerable<EcosystemHit> GetProjectedEcosystemsWithRandomOffsets(float X, float Y, SizeF ProjectedSizeM, SizeF TerrainSizeM)
        {
            var (pos_m_x, pos_m_y) = MathHelper.ImagePixelsToM(X, Y, TileCount, RadiusM);

            var proj_warping = MathHelper.GetLocalImageWarping(pos_m_x, pos_m_y, ProjectedSizeM.Width, ProjectedSizeM.Height, RadiusM);
            var phys_warping = MathHelper.GetLocalImageWarping(pos_m_x, pos_m_y, TerrainSizeM.Width, TerrainSizeM.Height, RadiusM);

            //upper_bound will be the lower number here because image is 0,0 top-left
            float image_upper_bound = Y - (proj_warping.dy * TileCount);
            float image_lower_bound = Y + (proj_warping.dy * TileCount);

            //No wrapping for Y-axis
            float search_y_start = MathHelper.ClampValue((float)Math.Floor(image_upper_bound), 0, TileCount - 1);
            float search_y_end = MathHelper.ClampValue((float)Math.Ceiling(image_lower_bound), 0, TileCount - 1);

            int terrain_step = 1;

            var pole_distance = TileCount / 16;


            //TODO Vary terrain step from 1 at pole_distance to TileCount at the pole
            if (Y < pole_distance / 2 || Y >= TileCount - pole_distance / 2)
            {
                terrain_step = 8;
            }
            else if (Y < pole_distance || Y >= TileCount - pole_distance)
            {
                terrain_step = 2;
            }

            //Search vertically all cells that our projection overlaps with
            for (float search_y_px = search_y_start; search_y_px <= search_y_end; search_y_px += 1.0f)
            {
                //Turn this cells position back into meters, and calculate local distortion size for this row specifically
                var (_, search_y_m) = MathHelper.ImagePixelsToM(0, search_y_px, TileCount, RadiusM);
                var search_circumference = MathHelper.CircumferenceAtDistanceFromEquator(search_y_m, RadiusM);
                var half_projected_width_px = (ProjectedSizeM.Width / 2 / search_circumference) * TileCount;

                //Break if the circumference at this pixel is less than a single projection (directly at poles)
                if (search_circumference < ProjectedSizeM.Width) 
                    break;

                float row_left_bound = X - half_projected_width_px;
                float row_right_bound = X + half_projected_width_px;
                float search_x_start = (float)Math.Floor(row_left_bound);
                float search_x_end = (float)Math.Ceiling(row_right_bound);

                //Now search horizontally all cells that out projection (at this vertical position) overlaps with
                for (float search_x_px = search_x_start; search_x_px <= search_x_end; search_x_px += 1.0f)
                {
                    if ((int)search_x_px % terrain_step != 0) continue;

                    //We can use NN here since we are just looking for the ecosystem data
                    var global_data = InterpolateClimateData((int)Math.Floor(search_x_px), (int)Math.Floor(search_y_px), 0, 0,
                        LocalizedClimateData.InterpolationMode.NearestNeighbor);
                    var local_ecosystem = EcoSystems[global_data.EcoSystemID];

                    var terrain_center_x = search_x_px + global_data.RandomOffset;
                    var terrain_center_y = search_y_px + global_data.RandomOffset;

                    //Now finally calculate the local distortion at the center of the terrain
                    var (terrain_center_m_x, terrain_center_m_y) = MathHelper.ImagePixelsToM(terrain_center_x, terrain_center_y, TileCount, RadiusM);
                    var terrain_circumference = MathHelper.CircumferenceAtDistanceFromEquator(terrain_center_m_y, RadiusM);
                    var half_terrain_width_projected_px = (ProjectedSizeM.Width / 2 / terrain_circumference) * TileCount;
                    var half_terrain_width_physical_px = (TerrainSizeM.Width / 2 / terrain_circumference) * TileCount;

                    var terrain_left_edge = terrain_center_x - half_terrain_width_projected_px;
                    var terrain_right_edge = terrain_center_x + half_terrain_width_projected_px;
                    var terrain_top_edge = terrain_center_y - (proj_warping.dy * TileCount);
                    var terrain_bottom_edge = terrain_center_y + (proj_warping.dy * TileCount);

                    //Reject pixels outside of the terrains projected pixel borders
                    if (X < terrain_left_edge || X > terrain_right_edge)
                        continue;
                    if (Y < terrain_top_edge || Y > terrain_bottom_edge)
                        continue;

                    //Finally calculate UV coordinates and return result
                    var terrain_u = ((X - terrain_center_x) / half_terrain_width_physical_px / 2) + 0.5f;
                    var terrain_v = ((Y - terrain_center_y) / (phys_warping.dy * TileCount * 2)) + 0.5f;
                    var patch_u = ((X - terrain_left_edge) / (half_terrain_width_projected_px * 2));
                    var patch_v = ((Y - terrain_top_edge) / (proj_warping.dy * TileCount * 2));

                    if (terrain_u < 0) terrain_u += 1;
                    if (terrain_v < 0) terrain_v += 1;
                    if (terrain_u >= 1) terrain_u -= 1;
                    if (terrain_v >= 1) terrain_v -= 1;

                    if (patch_u < 0) patch_u += 1;
                    if (patch_v < 0) patch_v += 1;
                    if (patch_u >= 1) patch_u -= 1;
                    if (patch_v >= 1) patch_v -= 1;

                    yield return new EcosystemHit(global_data.EcoSystemID, local_ecosystem,
                        search_x_px / TileCount, search_y_px / TileCount,
                        terrain_u, terrain_v, patch_u, patch_v,
                        global_data.X, global_data.Y, global_data.RandomOffset);
                }
            }
        }

        static Font render_font = new Font("Times New Roman", 12);

        public void RenderTile(RenderSettings settings, RenderResult result, Point InTile, Rectangle OutRectangle, int OutputOffsetX)
        {
            var TileX = InTile.X;
            var TileY = InTile.Y;
            var RealTileX = TileX + OutputOffsetX;

            //Use cheap NN here since we are only looking at pixel-level detail anyway
            //Hacky solution to handle offset.
            //Just shift all read in data by this amount
            var local_main = InterpolateClimateData(RealTileX, TileY, 0, 0, 
                LocalizedClimateData.InterpolationMode.NearestNeighbor);
            var local_ecosystem = EcoSystems[local_main.EcoSystemID];

            //for (int y = 0; y < OutRectangle.Height; y++)
            //{
            //    for (int x = 0; x < OutRectangle.Width; x++)
            //    {
            //        var output_x = OutRectangle.Left + x;
            //        var output_y = OutRectangle.Top + y;
            //        var local_xnorm = x / (float)OutRectangle.Width;
            //        var local_ynorm = y / (float)OutRectangle.Height;
            //
            //        //Use more expensive bicubic here to provide a better result
            //        var globalInterpolated = InterpolateClimateData(RealTileX, TileY, local_xnorm, local_ynorm,
            //            LocalizedClimateData.InterpolationMode.BiCubic);
            //        //Calculate mask value for X/Y position

            foreach(var (x, y, globalInterpolated) in 
                InterpolateClimateSubgrid(RealTileX, TileY, OutRectangle.Width, OutRectangle.Height,LocalizedClimateData.InterpolationMode.BiCubic))
            {
                var output_x = OutRectangle.Left + x;
                var output_y = OutRectangle.Top + y;
                var local_xnorm = x / (float)OutRectangle.Width;
                var local_ynorm = y / (float)OutRectangle.Height;
                float global_x = RealTileX + local_xnorm, global_y = TileY + local_ynorm;

                float final_temp;
                float final_humid;
                float final_height;
                float local_temp = 0;
                float local_humid = 0;
                float local_elevation = 0;
                float mask_total = 0;

                //TODO: Improve this
                bool at_poles = false;// TileY < (TileCount / 16) || TileY > (TileCount * 15.0f / 16.0f);

                if (settings.BlendEcosystems)
                {
                    if (!at_poles)
                    {
                        bool exit_early = false;

                        foreach (var eco_hit in GetProjectedEcosystemsWithRandomOffsets(global_x, global_y,
                            settings.TerrainProjectionSize, settings.TerrainRenderSize))
                        {
                            //We can use NN here since we are likely to be downsampling for the biome terrain
                            var localClimate = eco_hit.Data.InterpolateClimateDataWithOffset(
                                eco_hit.TerrainU, eco_hit.TerrainV,
                                eco_hit.TileRandomOffset, eco_hit.TileRandomOffset,
                                LocalizedClimateData.InterpolationMode.NearestNeighbor);

                            //Mask calculation
                            var xd = (eco_hit.PatchU - 0.5f);
                            var yd = (eco_hit.PatchV - 0.5f);
                            var center_distance = Math.Sqrt(xd * xd + yd * yd) * settings.EcosystemBlendingShrink;
                            var local_mask_value = (float)(center_distance > 0.5f ? 0 : Math.Cos(center_distance * Math.PI));

                            int distance = 1;
                            bool near_sketto = Math.Abs(eco_hit.TileX - 245) < distance && Math.Abs(eco_hit.TileY - 232) < distance;

                            if (near_sketto && settings.Debug_DrawSkettoBound)
                            {
                                byte mu = (byte)(local_mask_value * 255);
                                byte mv = (byte)(local_mask_value * 255);
                                result.SetResultPixel(output_x, output_y, mu, mv, 0);
                                exit_early = true;
                                break;
                            }

                            if (true)
                            {
                                local_temp += localClimate.Temperature * local_mask_value;
                                local_humid += localClimate.Humidity * local_mask_value;
                                local_elevation += localClimate.Elevation * local_mask_value;

                                mask_total += local_mask_value;
                            }
                        }

                        if (exit_early)
                            continue;
                    }
                    else
                    {
                        //TODO: Just project less frequently near poles
                        //Scale ecosystem data 1:1 to cell, doesn't work well for many reasons.
                        //need to decrease projection size/frequency near poles instead
                        var localClimate = local_ecosystem.InterpolateClimateDataWithOffset(local_xnorm, local_ynorm,
                            local_main.RandomOffset, local_main.RandomOffset,
                            LocalizedClimateData.InterpolationMode.BiLinear);
                        var local_mask_value = (float)(Math.Sin(local_xnorm * Math.PI) * Math.Sin(local_ynorm * Math.PI));

                        local_temp = localClimate.Temperature * local_mask_value;
                        local_humid = localClimate.Humidity * local_mask_value;

                        mask_total += local_mask_value;
                    }
                }

                local_temp = MathHelper.ClampValue(local_temp, -1, 1);
                local_humid = MathHelper.ClampValue(local_humid, -1, 1);
                //Don't clamp the elevation... (allow for deltas > +/-1k)
                //Normalize against mask_total to properly blend values
                if(mask_total > 0)
                {
                    local_temp /= mask_total;
                    local_humid /= mask_total;
                    local_elevation /= mask_total;
                } 

                //Finally blend final climate values with simple addition.
                final_temp = globalInterpolated.Temperature + (local_temp * TemperatureInfluence);
                final_humid = globalInterpolated.Humidity + (local_humid * HumidityInfluence);
                final_height =  (globalInterpolated.Elevation * settings.GlobalTerrainInfluence) + 
                                (local_elevation * settings.EcosystemTerrainInfluence);
                //Clamp temp/humid
                final_temp = MathHelper.ClampValue(final_temp, 0, 127);
                final_humid = MathHelper.ClampValue(final_humid, 0, 127);


                byte result_r, result_g, result_b, result_ocean_mask = 0;
                short result_height;


                /* --Height Calculation-- */

                // normalized to +/- 1
                float height_normalized = final_height / (settings.GlobalTerrainInfluence + settings.EcosystemTerrainInfluence);
                result_height = (short)MathHelper.ClampValue(height_normalized * short.MaxValue, short.MinValue, short.MaxValue);


                //Debug local climate influence
                if (settings.Debug_ShowLocalClimateInfluence)
                {
                    var ct = (int)(local_temp * TemperatureInfluence);
                    var ch = (int)(local_humid * HumidityInfluence);
                    if (ct < 0) ct = 0;
                    if (ch < 0) ch = 0;

                    result_r = (byte)ct;
                    result_g = (byte)ch;
                    result_b = 0;
                }
                else if (settings.Debug_DrawPatchUV)
                {
                    var ct = (int)(local_xnorm * 255.0f);
                    var ch = (int)(local_ynorm * 255.0f);
                    if (ct < 0) ct = 0;
                    if (ch < 0) ch = 0;

                    result_r = (byte)ct;
                    result_g = (byte)ch;
                    result_b = 0;
                }
                else
                {
                    /* --Ocean Calculation-- */
                    //I think heightmaps are "centered" around -2000 of the range +/- 4000km
                    //This biases the terrain to allow for taller mountains
                    var half_terrain = settings.GlobalTerrainInfluence / 2;
                    if (OceanEnabled && final_height < settings.OceanDepth)
                    {
                        result_r = OceanColor.R;
                        result_g = OceanColor.G;
                        result_b = OceanColor.B;

                        if (settings.BinaryOceanMask)
                        {
                            result_ocean_mask = 255;
                        }
                        else
                        {
                            float ocean_max = -(settings.EcosystemTerrainInfluence + settings.GlobalTerrainInfluence + settings.OceanDepth);
                            //NOTE: OceanDepth will be negative, so this comes out to a rang of 0 to -3km, flipped to 0/3km
                            float depth_normalized = (final_height + settings.OceanDepth) / ocean_max;
                            result_ocean_mask = (byte)MathHelper.ClampValue((depth_normalized * 255), 1, 255);
                        }

                        if (settings.HeightmapOceanSurface)
                        {
                            //Re-assign heightmap data to be directly flat on the ocean
                            height_normalized = settings.OceanDepth / (settings.GlobalTerrainInfluence + settings.EcosystemTerrainInfluence);
                            result_height = (short)MathHelper.ClampValue(height_normalized * short.MaxValue, short.MinValue, short.MaxValue);
                        }
                    }
                    /* --Color Calculation-- */
                    else
                    {
                        var lut_data = GetLUTData(final_temp, final_humid);
                        //TODO: Blend bedrock color based on slope?
                        result_r = lut_data.SurfaceColor.R;
                        result_g = lut_data.SurfaceColor.G;
                        result_b = lut_data.SurfaceColor.B;

                        result_ocean_mask = 0;
                    }
                }

                result.SetAllResultChannels((byte)final_temp, (byte)final_humid, output_x, output_y, result_r, result_g, result_b, result_height, result_ocean_mask);
            }
            
        }

        //TODO: Update this to directly write to raster
        private void DrawAdjustedRectangle(Graphics g, Pen p, float CenterXM, float CenterYM, float WidthM, float HeightM, float ScaleFactor,
            float OffsetX = 0, float OffsetY = 0)
        {
            var warp = MathHelper.GetLocalImageWarping(CenterXM, CenterYM, WidthM, HeightM, RadiusM);

            float w = TileCount * ScaleFactor * 2, h = TileCount * ScaleFactor;

            var col = Color.Red;

            float tl_x = warp.c_x - warp.udx;
            float tr_x = warp.c_x + warp.udx;
            float bl_x = warp.c_x - warp.ldx;
            float br_x = warp.c_x + warp.ldx;
            float tl_y = warp.c_y + warp.dy;
            float tr_y = warp.c_y + warp.dy;
            float bl_y = warp.c_y - warp.dy;
            float br_y = warp.c_y - warp.dy;


            g.DrawLine(Pens.Green, (warp.c_x - OffsetX) * w, (warp.c_y - OffsetY) * h, (tl_x - OffsetX) * w, (tl_y - OffsetY) * h);

            g.DrawPolygon(p, new Point[]
            {
                new Point((int)((tl_x - OffsetX) * w), (int)((tl_y - OffsetY) * h)),
                new Point((int)((tr_x - OffsetX) * w), (int)((tr_y - OffsetY) * h)),
                new Point((int)((br_x - OffsetX) * w), (int)((br_y - OffsetY) * h)),
                new Point((int)((bl_x - OffsetX) * w), (int)((bl_y - OffsetY) * h)),
                new Point((int)((tl_x - OffsetX) * w), (int)((tl_y - OffsetY) * h)),
            });
        }

        public LUTData GetLUTData(float Temperature, float Humidity)
        {
            Temperature = MathHelper.ClampValue(Temperature, 0, 127);;
            Humidity = MathHelper.ClampValue(Humidity, 0, 127);

            return ClimateLUT[(int)Humidity, (int)Temperature];
        }

        private void LoadSplatData(JObject PLAObject, int SplatSize)
        {
            var splatData = PLAObject.SelectToken("$.data.splatMap").Value<JArray>();
            for (int y = 0; y < SplatSize; y++)
            {
                for (int x = 0; x < SplatSize; x++)
                {
                    GlobalClimateData[x, y].Temperature = splatData[(y * SplatSize + x) * 4 + 0].Value<float>() / 2;
                    GlobalClimateData[x, y].Humidity = splatData[(y * SplatSize + x) * 4 + 1].Value<float>() / 2;
                    GlobalClimateData[x, y].EcoSystemID = (byte)(splatData[(y * SplatSize + x) * 4 + 2].Value<byte>() / 16);
                }
            }
        }

        private void LoadRandomOffsetData(JObject PLAObject, int SplatSize)
        {
            var offsetData = PLAObject.SelectToken("$.data.randomOffset").Value<JArray>();
            var offsetSize = PLAObject.SelectToken("$.data.randomOffsetWidth").Value<int>();
            if (offsetSize != SplatSize)
            {
                //If whole offset data is zeros, just load out data with zeros and call it good
                if(offsetData.Max(v => v.Value<int>()) == 0)
                {
                    for (int y = 0; y < GlobalClimateData.GetLength(1); y++)
                    {
                        for (int x = 0; x < GlobalClimateData.GetLength(0); x++)
                        {
                            GlobalClimateData[x, y].RandomOffset = 0.0f;
                        }
                    }
                }
                else
                {
                    throw new InvalidOperationException("Offset and splat map differ in size! Cannot proceed!");
                }
            }
            for (int y = 0; y < offsetSize; y++)
            {
                for (int x = 0; x < offsetSize; x++)
                {
                    GlobalClimateData[x, y].RandomOffset = offsetData[y * offsetSize + x].Value<float>() / 256.0f;
                }
            }
        }

        private void LoadHeightmapData(DFDatabase GameDatabase, JObject PLAObject)
        {
            var heightmapPath = PLAObject.SelectToken("$.data.General.tSphere.sHMWorld").Value<string>();
            var heightmapData = GameDatabase.ReadPackedFileBinary(heightmapPath);
            var heightmapSpan = new ReadOnlySpan<byte>(heightmapData);
            var heightmapSize = (int)Math.Sqrt(heightmapData.Length / 2);

            if (heightmapSize != TileCount)
            {
                //throw new InvalidOperationException("Heightmap and splat map differ in size! Cannot proceed!");
            }
            else
            {
                for (int y = 0; y < heightmapSize; y++)
                {
                    for (int x = 0; x < heightmapSize; x++)
                    {
                        ushort height = System.Buffers.Binary.BinaryPrimitives.ReadUInt16BigEndian(heightmapSpan.Slice((y * heightmapSize + x) * 2));
                        float height_normalized = ((float)height - short.MaxValue) / short.MaxValue;

                        GlobalClimateData[x, y].Elevation = height_normalized;
                    }
                }
            }
        }

        private void BuildLUT(JObject PLAObject)
        {
            int LUTSize = 128;
            ClimateLUT = new LUTData[LUTSize, LUTSize];

            for (int y = 0; y < LUTSize; y++)
            {
                for (int x = 0; x < LUTSize; x++)
                {
                    LUTData lut_pixel;

                    lut_pixel.GroundTextureID = PLAObject.SelectToken($".data.groundTexIDLUT[{y}][{x}]").Value<int>();
                    lut_pixel.ObjectPresetID = PLAObject.SelectToken($".data.objectPresetLUT[{y}][{x}]").Value<int>();
                    lut_pixel.BrushID = PLAObject.SelectToken($".data.brushIDLUT[{y}][{x}]").Value<int>();
                    lut_pixel.BrushData = Brushes[lut_pixel.BrushID];

                    var brushDataLUT = PLAObject.SelectToken($".data.brushDataLUT[{y}][{x}]");
                    lut_pixel.BD_GradientValBedrock = brushDataLUT.Value<byte>("gradientPosBedRock");
                    lut_pixel.BD_GradientValSurface = brushDataLUT.Value<byte>("gradientPosSurface");
                    lut_pixel.BD_ValueOffsetBedrock = brushDataLUT.Value<float>("valOffsetBedRock");
                    lut_pixel.BD_ValueOffsetSurface = brushDataLUT.Value<float>("valOffsetSurface");
                    lut_pixel.BD_SaturationOffsetBedrock = brushDataLUT.Value<float>("satOffsetBedRock");
                    lut_pixel.BD_SaturationOffsetSurface = brushDataLUT.Value<float>("satOffsetSurface");
                    lut_pixel.BD_OPRBlendIndex = brushDataLUT.Value<byte>("oprBlendIndex");
                    lut_pixel.BD_TextureLayerIndex = brushDataLUT.Value<byte>("texturLayerIndex");

                    var bedrockRGB = MathHelper.LerpColor(
                        lut_pixel.BrushData.BedrockGradientColorA,
                        lut_pixel.BrushData.BedrockGradientColorB,
                        lut_pixel.BD_GradientValBedrock);
                    var surfaceRGB = MathHelper.LerpColor(
                        lut_pixel.BrushData.SurfaceGradientColorA,
                        lut_pixel.BrushData.SurfaceGradientColorB,
                        lut_pixel.BD_GradientValSurface);

                    lut_pixel.BedrockColor = Color.FromArgb(bedrockRGB.Item1, bedrockRGB.Item2, bedrockRGB.Item3);
                    //.AdjustHSV(0, lut_pixel.BD_SaturationOffsetBedrock, lut_pixel.BD_ValueOffsetBedrock);
                    lut_pixel.SurfaceColor = Color.FromArgb(surfaceRGB.Item1, surfaceRGB.Item2, surfaceRGB.Item3);
                    //.AdjustHSV(0, lut_pixel.BD_SaturationOffsetSurface, lut_pixel.BD_ValueOffsetSurface);

                    //var gloss_color = PLAObject.SelectToken($"$.data.nearColorGlossLUT[{y}][{x}]").Values<int>().ToArray();

                    //lut_pixel.SurfaceGloss = Color.FromArgb(gloss_color[0], gloss_color[1], gloss_color[2]);

                    ClimateLUT[x, y] = lut_pixel;
                }
            }
        }

        public LocalizedClimateData InterpolateClimateData(int TileX, int TileY, float TilePositionX, float TilePositionY, LocalizedClimateData.InterpolationMode InterpolationMode)
        {
            return LocalizedClimateData.InterpolateClimate(GlobalClimateData, TileX + TilePositionX, TileY + TilePositionY, Mode: InterpolationMode);
        }

        public IEnumerable<(int X, int Y, LocalizedClimateData Climate)> InterpolateClimateSubgrid(int TileX, int TileY, int Width, int Height, LocalizedClimateData.InterpolationMode InterpolationMode)
        {
            return LocalizedClimateData.InterpolateSubgrid(GlobalClimateData, TileX, TileY, Width, Height, Mode: InterpolationMode);
        }
    }

    public class PlanetBrush
    {
        internal static Color ReadColorRGBA(JToken token)
        {
            if (token.Type != JTokenType.Array)
                throw new InvalidOperationException();

            var token_values = token.Values<byte>().ToArray();
            return Color.FromArgb(token_values[3], token_values[0], token_values[1], token_values[2]);
        }

        public static PlanetBrush FromJObject(JObject brushObject)
        {
            return new PlanetBrush()
            {
                BedrockGradientColorA = ReadColorRGBA(brushObject.SelectToken("bedrockBrush.colorGradient.gradientColorA")),
                BedrockGradientColorB = ReadColorRGBA(brushObject.SelectToken("bedrockBrush.colorGradient.gradientColorB")),
                SurfaceGradientColorA = ReadColorRGBA(brushObject.SelectToken("surfaceBrush.colorGradient.gradientColorA")),
                SurfaceGradientColorB = ReadColorRGBA(brushObject.SelectToken("surfaceBrush.colorGradient.gradientColorB")),
                TMin = brushObject.Value<int>("tMin"),
                TMax = brushObject.Value<int>("tMax"),
                HMin = brushObject.Value<int>("hMin"),
                HMax = brushObject.Value<int>("hMax"),
            };
        }

        public Color BedrockGradientColorA;
        public Color BedrockGradientColorB;
        public Color SurfaceGradientColorA;
        public Color SurfaceGradientColorB;

        public int TMin;
        public int TMax;
        public int HMin;
        public int HMax;
    }
}
