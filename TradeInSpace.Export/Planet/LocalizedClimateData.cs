﻿using System;
using System.Collections.Generic;

namespace TradeInSpace.Export.Planet
{
    public struct LocalizedClimateData
    {
        public float X;
        public float Y;

        public float Temperature;
        public float Humidity;
        public byte EcoSystemID;

        public float Elevation;
        public float RandomOffset;

        public float NormalX;
        public float NormalY;
        public float NormalZ;

        public int SurfaceTextureMap;
        public int OPRBlendIndex;

        public enum InterpolationMode
        {
            NearestNeighbor,
            BiLinear,
            BiCubic
        }

        public static IEnumerable<(int X, int Y, LocalizedClimateData Climate)>
            InterpolateSubgrid(LocalizedClimateData[,] ClimateData, int X, int Y, int Width, int Height, float ReadOffsetX = 0, float ReadOffsetY = 0, InterpolationMode Mode = InterpolationMode.NearestNeighbor)
        {
            //Helper function to handle wrapping
            LocalizedClimateData SampleDataRelative(float XDif, float YDif)
            {
                var width = ClimateData.GetLength(0);
                var height = ClimateData.GetLength(1);

                var XPos = (X + XDif + ReadOffsetX) % width;
                var YPos = (Y + YDif + ReadOffsetY) % height;

                if (XPos < 0) XPos += width;
                if (YPos < 0) YPos += height;

                return ClimateData[(int)Math.Floor(XPos), (int)Math.Floor(YPos)];
            }

            switch (Mode)
            {
                case InterpolationMode.NearestNeighbor:
                    for (int y_division = 0; y_division < Height; y_division++)
                    {
                        for (int x_division = 0; x_division < Width; x_division++)
                        {
                            float cellX = (x_division / (float)Width), cellY = (y_division / (float)Height);

                            yield return (x_division, y_division, SampleDataRelative((int)cellX, (int)cellY));
                        }
                    }
                    break;
                //Dont' actuall optimize here, linear is cheap
                case InterpolationMode.BiLinear:
                    for (int y_division = 0; y_division < Height; y_division++)
                    {
                        for (int x_division = 0; x_division < Width; x_division++)
                        {
                            float cellX = (x_division / (float)Width), cellY = (y_division / (float)Height);

                            yield return (x_division, y_division, InterpolateClimate(ClimateData, X + cellX, Y + cellY, ReadOffsetX, ReadOffsetY, Mode));
                        }
                    }
                    break;
                case InterpolationMode.BiCubic:
                    var temp_cubic = new MathHelper.CachedBicubicInterpolator((x, y) => SampleDataRelative(x - 1, y - 1).Temperature);
                    var humid_cubic = new MathHelper.CachedBicubicInterpolator((x, y) => SampleDataRelative(x - 1, y - 1).Humidity);
                    var elev_cubic = new MathHelper.CachedBicubicInterpolator((x, y) => SampleDataRelative(x - 1, y - 1).Elevation);

                    for (int y_division = 0; y_division < Height; y_division++)
                    {
                        for (int x_division = 0; x_division < Width; x_division++)
                        {
                            float cellX = (x_division / (float)Width), cellY = (y_division / (float)Height);

                            if (true)
                            {
                                yield return (x_division, y_division, InterpolateClimate(ClimateData, X + cellX, Y + cellY, ReadOffsetX, ReadOffsetY, Mode));
                            }
                            else
                            {
                                yield return (x_division, y_division, new LocalizedClimateData()
                                {
                                    X = X + cellX,
                                    Y = Y + cellY,

                                    Temperature = (float)temp_cubic.CalculateValue(cellX, cellY),
                                    Humidity = (float)humid_cubic.CalculateValue(cellX, cellY),
                                    //EcoSystemID = (byte)TakeSample(lcd => lcd.EcoSystemID),

                                    Elevation = (float)elev_cubic.CalculateValue(cellX, cellY),
                                    //RandomOffset = TakeSample(lcd => lcd.RandomOffset),

                                    //NormalX = TakeSample(lcd => lcd.NormalX),
                                    //NormalY = TakeSample(lcd => lcd.NormalY),
                                    //NormalZ = TakeSample(lcd => lcd.NormalZ),
                                });
                            }
                        }
                    }
                    break;
            }
        }

        public static LocalizedClimateData InterpolateClimate(LocalizedClimateData[,] ClimateData, float X, float Y, 
            float ReadOffsetX = 0, float ReadOffsetY = 0, InterpolationMode Mode = InterpolationMode.NearestNeighbor)
        {
            //Helper function to handle wrapping
            LocalizedClimateData SampleDataRelative(float XDif, float YDif)
            {
                var width = ClimateData.GetLength(0);
                var height = ClimateData.GetLength(1);

                var XPos = (X + XDif + ReadOffsetX) % width;
                var YPos = (Y + YDif + ReadOffsetY) % height;

                if (XPos < 0) XPos += ClimateData.GetLength(0);
                if (YPos < 0) YPos += ClimateData.GetLength(1);

                return ClimateData[(int)Math.Floor(XPos), (int)Math.Floor(YPos)];
            }

            int TileX = (int)Math.Floor(X);
            int TileY = (int)Math.Floor(Y);
            float TilePositionX = X - TileX;
            float TilePositionY = Y - TileY;

            if (TilePositionX == 0 && TilePositionY == 0)
                return SampleDataRelative(0, 0);

            if (Mode == InterpolationMode.NearestNeighbor)
            {
                return SampleDataRelative((int)TilePositionX, (int)TilePositionY);
            }
            else if (Mode == InterpolationMode.BiLinear)
            {
                //Modulus these by width/height to wrap around the image
                var UL = SampleDataRelative(0, 0);
                var UR = SampleDataRelative(1, 0);
                var LL = SampleDataRelative(0, 1);
                var LR = SampleDataRelative(1, 1);

                return new LocalizedClimateData()
                {
                    X = X,
                    Y = Y,

                    Temperature = MathHelper.LerpValue2D(UL.Temperature, UR.Temperature, LL.Temperature, LR.Temperature, TilePositionX, TilePositionY),
                    Humidity = MathHelper.LerpValue2D(UL.Humidity, UR.Humidity, LL.Humidity, LR.Humidity, TilePositionX, TilePositionY),
                    EcoSystemID = (byte)MathHelper.LerpValue2D(UL.EcoSystemID, UR.EcoSystemID, LL.EcoSystemID, LR.EcoSystemID, TilePositionX, TilePositionY),

                    Elevation = MathHelper.LerpValue2D(UL.Elevation, UR.Elevation, LL.Elevation, LR.Elevation, TilePositionX, TilePositionY),
                    RandomOffset = MathHelper.LerpValue2D(UL.RandomOffset, UR.RandomOffset, LL.RandomOffset, LR.RandomOffset, TilePositionX, TilePositionY),

                    NormalX = MathHelper.LerpValue2D(UL.NormalX, UR.NormalX, LL.NormalX, LR.NormalX, TilePositionX, TilePositionY),
                    NormalY = MathHelper.LerpValue2D(UL.NormalY, UR.NormalY, LL.NormalY, LR.NormalY, TilePositionX, TilePositionY),
                    NormalZ = MathHelper.LerpValue2D(UL.NormalZ, UR.NormalZ, LL.NormalZ, LR.NormalZ, TilePositionX, TilePositionY),

                    SurfaceTextureMap = (int)MathHelper.LerpValue2D(UL.SurfaceTextureMap, UR.SurfaceTextureMap, LL.SurfaceTextureMap, LR.SurfaceTextureMap, TilePositionX, TilePositionY),
                };
            }
            else if(Mode == InterpolationMode.BiCubic)
            {
                var samples = new LocalizedClimateData[4, 4];
                for (int y = 0; y < 4; y++)
                {
                    for (int x = 0; x < 4; x++)
                    {
                        samples[x, y] = SampleDataRelative(x - 1, y - 1);
                    }
                }

                float TakeSample(Func<LocalizedClimateData, float> ValueAction)
                {
                    //Calculate horizontal interpolations to create vertical co-efficients
                    var r_1 = MathHelper.InterpolateCubic(ValueAction(samples[0, 0]), ValueAction(samples[1, 0]), ValueAction(samples[2, 0]), ValueAction(samples[3, 0]), TilePositionX);
                    var r_2 = MathHelper.InterpolateCubic(ValueAction(samples[0, 1]), ValueAction(samples[1, 1]), ValueAction(samples[2, 1]), ValueAction(samples[3, 1]), TilePositionX);
                    var r_3 = MathHelper.InterpolateCubic(ValueAction(samples[0, 2]), ValueAction(samples[1, 2]), ValueAction(samples[2, 2]), ValueAction(samples[3, 2]), TilePositionX);
                    var r_4 = MathHelper.InterpolateCubic(ValueAction(samples[0, 3]), ValueAction(samples[1, 3]), ValueAction(samples[2, 3]), ValueAction(samples[3, 3]), TilePositionX);
                    //Calculate vertical interpolation
                    return MathHelper.InterpolateCubic(r_1, r_2, r_3, r_4, TilePositionY);
                }

                //Save on a bit of processing here.....
                return new LocalizedClimateData()
                {
                    X = X,
                    Y = Y,

                    Temperature = TakeSample(lcd => lcd.Temperature),
                    Humidity = TakeSample(lcd => lcd.Humidity),
                    //EcoSystemID = (byte)TakeSample(lcd => lcd.EcoSystemID),

                    Elevation = TakeSample(lcd => lcd.Elevation),
                    //RandomOffset = TakeSample(lcd => lcd.RandomOffset),

                    //NormalX = TakeSample(lcd => lcd.NormalX),
                    //NormalY = TakeSample(lcd => lcd.NormalY),
                    //NormalZ = TakeSample(lcd => lcd.NormalZ),
                };
            }
            else
            {
                throw new Exception("Unknown Mode!");
            }
        }
    }
}
