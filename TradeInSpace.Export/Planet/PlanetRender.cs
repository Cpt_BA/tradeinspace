﻿using ICSharpCode.SharpZipLib.Zip;
using NetTopologySuite.Geometries;
using Newtonsoft.Json.Linq;
using OSGeo.GDAL;
using Serilog;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using TradeInSpace.Explore.DataForge;

namespace TradeInSpace.Export.Planet
{
    public class PlanetRenderer
    {
        public DFDatabase GameDatabase { get; }
        public Dictionary<Guid, EcosystemData> Biomes { get; set; }
        public RenderSettings Settings { get; set; } = new RenderSettings();

        //Progress event callbacks
        public delegate void ProgressEventHandler(object sender, int progress, int maximum);

        public event ProgressEventHandler PreLoadProgress;
        public event ProgressEventHandler RenderProgress;

        public PlanetRenderer(DFDatabase GameDatabase)
        {
            this.GameDatabase = GameDatabase;

            Biomes = new Dictionary<Guid, EcosystemData>();
            foreach (ZipEntry entry in GameDatabase.GamePack)
            {
                try
                {
                    if (Path.GetExtension(entry.Name) == ".eco")
                    {
                        var ecosystemText = GameDatabase.ReadPackedFile(entry.Name);
                        if (ecosystemText == null)
                            continue;

                        var ecosystemJson = JObject.Parse(ecosystemText);
                        var ecosystemData = EcosystemData.FromJObject(ecosystemJson);

                        Biomes[ecosystemData.ID] = ecosystemData;
                    }
                }
                catch (Exception ex)
                {
                    Log.Information($"Error loading ecosytstem: {ex.Message}", ex);
                }
            }
        }

        internal void EcosystemPreloaded(int number, int maximum)
        {
            PreLoadProgress?.Invoke(this, number, maximum);
        }

        public RenderResult RenderScaledPlanet(PlanetData Planet, RenderSettings SettingsOverride = null)
        {
            var SettingsToUse = SettingsOverride ?? Settings ?? RenderSettings.DefaultSettings;

            var ExportRegion = SettingsToUse.SplatMapRenderArea;
            var TileSize = SettingsToUse.TileSize;

            var off_x = ExportRegion.X / (float)Planet.TileCount;
            var off_y = ExportRegion.Y / (float)Planet.TileCount;
            var nasacoords = SettingsToUse.CoordinateMode == CoordinateMode.NASA;

            RenderResult result = new RenderResult(Planet, ExportRegion, TileSize, nasacoords);
            result.Allocate();

            //You'd think Parallel.For would be sequential....but apparently not??
            int count = 0;

            //Shift by half planet to make 0deg line up with nasa coord system
            var OutputOffsetTilesX = SettingsToUse.CoordinateMode == CoordinateMode.EarthShifted ?
                Planet.TileCount / 2 : 0;

            Parallel.For(0, (int)(ExportRegion.Width * ExportRegion.Height), (tile_num) =>
            {
                int tile_x = tile_num % ExportRegion.Width;
                int tile_y = tile_num / ExportRegion.Width;

                if ((count % ExportRegion.Width) == 0)
                {
                    //Log.Information($"Render Line: {count / ExportRegion.Width}/{ExportRegion.Height}");
                }

                var in_pixel = new System.Drawing.Point((int)(ExportRegion.X + tile_x), (int)(ExportRegion.Y + tile_y));
                var out_rect = new Rectangle(
                    (int)(tile_x * TileSize * 2), (int)(tile_y * TileSize),
                    (int)(TileSize * 2), (int)TileSize);

                Planet.RenderTile((RenderSettings)SettingsToUse, result, in_pixel, out_rect, OutputOffsetTilesX);
                count++;
                if (count % ExportRegion.Width == ExportRegion.Width - 1)
                {
                    RenderProgress?.Invoke(this, count / ExportRegion.Width, ExportRegion.Height);
                }
            });

            /*
            //Draw debug squares over every projected heightmap
            if (Settings.Debug_DrawEcosystemOutlintes)
            {
                for (int tile_y = StartTileY; tile_y < StartTileY + RenderHeight; tile_y++)
                {
                    Log.Information($"Render Line: {tile_y}");
                    for (int tile_x = StartTileX; tile_x < StartTileX + RenderWidth; tile_x++)
                    {
                        var cell_data = InterpolateClimateData(tile_x, tile_y, 0, 0, LocalizedClimateData.InterpolationMode.NearestNeighbor);
                        var (center_m_x, center_m_y) = MathHelper.ImagePixelsToM(tile_x + cell_data.RandomOffset, tile_y + cell_data.RandomOffset, TileCount, RadiusM);
                        DrawAdjustedRectangle(render_g, Pens.Red, center_m_x, center_m_y, Settings.TerrainProjectionSize.Width, Settings.TerrainProjectionSize.Height, ScaleFactor, off_x, off_y);
                    }
                }
            }

            //Draw 4x4km projected grid over sketto
            if (Settings.Debug_DrawSkettoBound && BaseName == "stanton1c")
            {
                var sketto_data = InterpolateClimateData(sketto_offset_x, sketto_offset_y, 0, 0, LocalizedClimateData.InterpolationMode.NearestNeighbor);
                var (center_m_x, center_m_y) = MathHelper.ImagePixelsToM(sketto_offset_x + sketto_data.RandomOffset, sketto_offset_y + sketto_data.RandomOffset, TileCount, RadiusM);
                DrawAdjustedRectangle(render_g, Pens.Blue, center_m_x, center_m_y, Settings.TerrainProjectionSize.Width, Settings.TerrainProjectionSize.Height, ScaleFactor, off_x, off_y);

                //(center_m_x, center_m_y) = MathHelper.ImagePixelsToM(sketto_offset_x + 0.5f, sketto_offset_y + 0.5f, TileCount, RadiusM);
                //DrawAdjustedRectangle(render_g, Pens.Blue, center_m_x, center_m_y, 4000, 4000, ScaleFactor, off_x, off_y);
            }
            */


            if (SettingsToUse.WIP_HillShading)
            {
                count = 0;
                //Hillshading pass, currently non-functioning due to inaccurate terrain scaling
                float zenith = (float)(45.0f / 180.0f * Math.PI); // 0-90deg
                float azimuth = (float)(135.0f / 180.0f * Math.PI); // 


                Parallel.For(0, ExportRegion.Width * ExportRegion.Height, (tile_num) =>
                {
                    int tile_x = tile_num % ExportRegion.Width;
                    int tile_y = tile_num / ExportRegion.Width;

                    if ((count % ExportRegion.Width) == 0)
                    {
                        //Log.Information($"Hillshader Line: {count / ExportRegion.Width} / {ExportRegion.Height}");
                    }

                    //Compute one cell at a time per parallel task
                    for (int inner_y = 0; inner_y < TileSize; inner_y++)
                    {
                        for (int inner_x = 0; inner_x < TileSize * 2; inner_x++)
                        {
                            int output_x = (tile_x * TileSize * 2) + inner_x, output_y = (tile_y * TileSize) + inner_y;

                            //Don't hillshade oceans
                            if (result.GetResultOceanMask(output_x, output_y) != 0)
                                continue;

                            var (slope, aspect) = result.ComputeSlope(output_x, output_y, ZFactor: 1f);

                            slope = MathHelper.ClampValue(slope, (float)-Math.PI, (float)Math.PI);
                            slope /= (float)Math.PI;

                            var old_pixel = result.GetResultPixel(output_x, output_y);

                            var hillshade = (int)
                                (255 * ((Math.Cos(zenith) * Math.Cos(slope)) + (Math.Sin(zenith) * Math.Sin(slope) * Math.Cos(azimuth - aspect))));

                            //Tone down the hill shading a bit. Otherwise it's too strong
                            var new_r = (byte)MathHelper.ClampValue(old_pixel.R + (hillshade - 127) / 4, 0, 255);
                            var new_g = (byte)MathHelper.ClampValue(old_pixel.G + (hillshade - 127) / 4, 0, 255);
                            var new_b = (byte)MathHelper.ClampValue(old_pixel.B + (hillshade - 127) / 4, 0, 255);

                            result.SetResultPixel(output_x, output_y, new_r, new_g, new_b);
                        }
                    }

                    count++;
                });


            }

            return result;
        }

        public void RenderWholeSurface(string OutputPath, string DriverName, PlanetData Planet, RenderSettings SettingsOverride = null
            , int RenderSize = 10240, bool IncludeCopy = false)
        {
            var SettingsToUse = SettingsOverride ?? Settings ?? RenderSettings.DefaultSettings;
            var OriginalDriver = DriverName;

            int complete_width = Planet.TileCount * SettingsToUse.TileSize * 2;
            int complete_height = Planet.TileCount * SettingsToUse.TileSize;

            int render_tiles = RenderSize / SettingsToUse.TileSize;
            var entire_planet = new RenderResult(Planet, new Rectangle(0, 0, Planet.TileCount, Planet.TileCount), SettingsToUse.TileSize);
            List<string> create_options = new List<string>();
            if (DriverName.Equals("GTiff", StringComparison.CurrentCultureIgnoreCase))
            {
                create_options.Add("TILED=YES");
                create_options.Add("BIGTIFF=YES");
                create_options.Add("COMPRESS=DEFLATE");
            }
            else if (DriverName.Equals("GPKG", StringComparison.CurrentCultureIgnoreCase))
            {
            }
            else if (DriverName.Equals("PNG", StringComparison.CurrentCultureIgnoreCase) && IncludeCopy)
            {
                DriverName = "MEM";
            }
            else
            {
                throw new NotSupportedException($"Unknown GDAL driver: {DriverName}");
            }

            var surface_options = new List<string>(create_options);
            var heightmap_options = new List<string>(create_options);

            surface_options.Add($"RASTER_TABLE={Planet.BaseName}_surface");
            heightmap_options.Add($"RASTER_TABLE={Planet.BaseName}_heightmap");
            heightmap_options.Add("APPEND_SUBDATASET=YES");

            var surface_bandmap = new int[] { 1, 2, 3, 4 };
            var heightmap_bandmap = new int[] { 1 };

            if (File.Exists(OutputPath))
                File.Delete(OutputPath);

            using (var drv = Gdal.GetDriverByName(DriverName))
            {
                if (drv == null)
                    throw new Exception("No GDAL driver found!");

                using (var surface_ds = drv.Create(OutputPath, complete_width, complete_height, 4, DataType.GDT_Byte, surface_options.ToArray()))
                using (var heightmap_ds = drv.Create(OutputPath, complete_width, complete_height, 1, DataType.GDT_Int16, heightmap_options.ToArray()))
                {
                    surface_ds.SetGeoTransform(entire_planet.GetGeometryTransform());
                    heightmap_ds.SetGeoTransform(entire_planet.GetGeometryTransform());
                    
                    for (int x = 0; x < Planet.TileCount; x += render_tiles / 2)
                    {
                        int end_x = MathHelper.ClampValue(x + render_tiles / 2, 0, Planet.TileCount);

                        for (int y = 0; y < Planet.TileCount; y += render_tiles)
                        {
                            int end_y = MathHelper.ClampValue(y + render_tiles, 0, Planet.TileCount);
                            var input_region = new Rectangle(x, y, end_x - x, end_y - y);
                            var output_region = new Rectangle(
                                input_region.X * SettingsToUse.TileSize * 2,
                                input_region.Y * SettingsToUse.TileSize,
                                input_region.Width * SettingsToUse.TileSize * 2,
                                input_region.Height * SettingsToUse.TileSize);

                            var region_settings = SettingsToUse.Copy();
                            region_settings.SplatMapRenderArea = input_region;

                            using (var section = this.RenderScaledPlanet(Planet, region_settings))
                            {
                                //TODO: Optional layers / layer selection
                                surface_ds.WriteRaster(output_region.X, output_region.Y, output_region.Width, output_region.Height,
                                    section.Image, section.TotalWidth, section.TotalHeight, 4, surface_bandmap, 0, 0, 0);
                                surface_ds.FlushCache();

                                heightmap_ds.WriteRaster(output_region.X, output_region.Y, output_region.Width, output_region.Height,
                                    section.Heightmap, section.TotalWidth, section.TotalHeight, 1, heightmap_bandmap, 0, 0, 0);
                                heightmap_ds.FlushCache();
                            }

                            var used_gb = Process.GetCurrentProcess().PrivateMemorySize64 / 1024 / 1024 / 1024.0f;
                            Log.Information($"Rendered chunk: {x},{y},{input_region.Width},{input_region.Height} - {used_gb:N2}GB usage");
                        }
                    }

                    surface_ds.BuildOverviews("NEAREST_NEIGHBOR", new int[] { 2, 4, 8, 16, 32, 64, 128, 256 });
                    surface_ds.FlushCache();

                    heightmap_ds.BuildOverviews("NEAREST_NEIGHBOR", new int[] { 2, 4, 8, 16, 32, 64, 128, 256 });
                    heightmap_ds.FlushCache();

                    if (IncludeCopy)
                    {
                        using(var copy_drv = Gdal.GetDriverByName(OriginalDriver))
                        {
                            var copy_path = Path.ChangeExtension(OutputPath, $".{OriginalDriver.ToLower()}");

                            copy_drv.CreateCopy(copy_path, surface_ds, 0, new string[] { }, null, null);
                        }
                    }
                }
            }
        }
    }

    public class RenderSettings
    {
        public static RenderSettings DefaultSettings = new RenderSettings();

        //Commonly changed settings
        public int TileSize { get; set; } = 1;
        public Rectangle SplatMapRenderArea { get; set; } = Rectangle.Empty;

        public CoordinateMode CoordinateMode { get; set; } = CoordinateMode.NASA;

        //Less common
        public SizeF TerrainRenderSize { get; set; } = new SizeF(4000, 4000);
        public SizeF TerrainProjectionSize { get; set; } = new SizeF(6000, 6000);

        public float GlobalTerrainInfluence { get; set; } = 4000;
        public float EcosystemTerrainInfluence { get; set; } = 1000;
        public float EcosystemBlendingShrink { get; set; } = 1f;

        public float OceanDepth { get; set; } = -2000f;
        public bool HeightmapOceanSurface { get; set; } = true;
        public bool BinaryOceanMask { get; set; } = false;

        public bool BlendEcosystems { get; set; } = true;
        public bool WIP_HillShading { get; set; } = true;

        public bool Debug_DrawEcosystemOutlintes { get; set; } = false;
        public bool Debug_DrawPatchUV { get; set; } = false;
        public bool Debug_DrawCells { get; set; } = false;
        public bool Debug_DrawSkettoBound { get; set; } = false;
        public bool Debug_ShowLocalClimateInfluence { get; set; } = false;

        public RenderSettings Copy()
        {
            var result = new RenderSettings();
            foreach (var prop in GetType().GetProperties())
            {
                prop.SetValue(result, prop.GetValue(this));
            }
            return result;
        }
    }

    //Represents one cell from the 1024x1024 (or 512x512 for moons) map supplied with the planet
    public class PlanetTile
    {
        public PlanetData Planet { get; private set; }

        //In Tiles
        public double X { get; private set; }
        public double Y { get; private set; }

        //Local Tile Data


        public PlanetTile(PlanetData planet, double x, double y)
        {
            Planet = planet;
            X = x;
            Y = y;
        }
    }

    public struct LUTData
    {
        public Color BedrockGloss;
        public Color SurfaceGloss;

        public Color BedrockColor;
        public Color SurfaceColor;

        public int BrushID;
        public int GroundTextureID;
        public int ObjectPresetID;
        public byte BD_GradientValBedrock;
        public byte BD_GradientValSurface;
        public float BD_ValueOffsetBedrock;
        public float BD_ValueOffsetSurface;
        public float BD_SaturationOffsetBedrock;
        public float BD_SaturationOffsetSurface;
        public int BD_OPRBlendIndex;
        public int BD_TextureLayerIndex;

        public PlanetBrush BrushData;
    }

    public struct EcosystemHit
    {
        public int EcosystemID;
        public EcosystemData Data;
        public float CenterX;
        public float CenterY;
        public float TerrainU;
        public float TerrainV;
        public float PatchU;
        public float PatchV;
        public float TileX;
        public float TileY;
        public float TileRandomOffset;

        public EcosystemHit(int ecosystemID, EcosystemData data, 
            float centerX, float centerY, float terrainU, float terrainV, 
            float patchU, float patchV, float tileX, float tileY, 
            float tileRandomOffset)
        {
            EcosystemID = ecosystemID;
            Data = data;
            CenterX = centerX;
            CenterY = centerY;
            TerrainU = terrainU;
            TerrainV = terrainV;
            PatchU = patchU;
            PatchV = patchV;
            TileX = tileX;
            TileY = tileY;
            TileRandomOffset = tileRandomOffset;
        }
    }

    public enum CoordinateMode
    {
        NASA,
        EarthShifted,
        EarthUnshifted
    }
}
