﻿using Newtonsoft.Json.Linq;
using System;
using System.Drawing;
using System.IO;
using TradeInSpace.Explore.DataForge;

namespace TradeInSpace.Export.Planet
{
    public class EcosystemData
    {
        private static readonly string TerrainTextureBase = "Data/Textures/planets/terrain";

        public Guid ID { get; set; }
        public string Name { get; set; }
        public float Offset { get; set; }
        private JObject Object { get; set; }

        public LocalizedClimateData[,] ClimateData { get; private set; } = null;

        private EcosystemData() { }

        public static EcosystemData FromJObject(JObject Data)
        {
            return new EcosystemData()
            {
                ID = Guid.Parse(Data.Value<string>("GUID")),
                Name = Data.Value<string>("name"),
                Offset = Data.Value<float>("fOffsetH"),
                Object = Data
            };
        }

        public void LoadClimateData(DFDatabase GameDatabase)
        {
            if (ClimateData != null)
                return;

            //Yes the path is for albedo texture, but it points to a clim texture for all 
            var climTexturePath = Object.SelectToken("$.Textures.ecoSystemAlbedoTexture").Value<string>().Replace(".tif", ".dds");
            var normalTexturePath = Object.SelectToken("$.Textures.ecoSystemNormalTexture").Value<string>().Replace(".tif", ".dds");
            var elevationPath = Object.SelectToken("$.Textures.elevationTexture").Value<string>();

            var climateImage = ExportUtils.ExtractImage(GameDatabase, $"{TerrainTextureBase}/{climTexturePath}.6");
            var normalImage = ExportUtils.ExtractImage(GameDatabase, $"{TerrainTextureBase}/{normalTexturePath}.6");
            var elevationMap = GameDatabase.ReadPackedFileBinary($"{TerrainTextureBase}/{elevationPath}");
            var elevationSpan = new Span<byte>(elevationMap);
            var elevationSize = (int)Math.Sqrt(elevationMap.Length / 2);

            //TODO: Better way of handling that the heightmap is 2x resolution over the terrain data
            //using (var climateImageResized = new Bitmap(climateImage, elevationSize, elevationSize))
            //using (var normalImageResized = new Bitmap(normalImage, elevationSize, elevationSize))
            {
                var climateFinal = climateImage;
                var normalFinal = normalImage;

                if (normalImage.Width != climateImage.Width)
                {
                    throw new InvalidDataException();
                }

                ClimateData = new LocalizedClimateData[elevationSize / 2, elevationSize / 2];

                var climate_raw = climateFinal.LockBits(new Rectangle(0, 0, climateFinal.Width, climateFinal.Height),
                    System.Drawing.Imaging.ImageLockMode.ReadOnly,
                    System.Drawing.Imaging.PixelFormat.Format32bppArgb);
                var normal_raw = normalFinal.LockBits(new Rectangle(0, 0, normalFinal.Width, normalFinal.Height),
                    System.Drawing.Imaging.ImageLockMode.ReadOnly,
                    System.Drawing.Imaging.PixelFormat.Format32bppArgb);

                unsafe
                {
                    var climate_span = new Span<byte>(climate_raw.Scan0.ToPointer(), climate_raw.Width * climate_raw.Height * 4);
                    var normal_span = new Span<byte>(normal_raw.Scan0.ToPointer(), normal_raw.Width * normal_raw.Height * 4);

                    for (int y = 0; y < climate_raw.Height; y++)
                    {
                        for (int x = 0; x < climate_raw.Width; x++)
                        {
                            var climate_a = climate_span[(y * climate_raw.Width + x) * 4 + 0];
                            var climate_r = climate_span[(y * climate_raw.Width + x) * 4 + 1];
                            var climate_g = climate_span[(y * climate_raw.Width + x) * 4 + 2];
                            var climate_b = climate_span[(y * climate_raw.Width + x) * 4 + 3];
                            var normal_a = normal_span[(y * normal_raw.Width + x) * 4 + 0];
                            var normal_r = normal_span[(y * normal_raw.Width + x) * 4 + 1];
                            var normal_g = normal_span[(y * normal_raw.Width + x) * 4 + 2];
                            var normal_b = normal_span[(y * normal_raw.Width + x) * 4 + 3];

                            float local_temp = (climate_r - 128.0f) / 128.0f;
                            float local_humid = (climate_g - 128.0f) / 128.0f;
                            ushort elevation = System.Buffers.Binary.BinaryPrimitives.ReadUInt16BigEndian(elevationSpan.Slice((y * 2 * elevationSize + x * 2) * 2));
                            float elevation_norm = ((elevation - (ushort.MaxValue / 2.0f)) / (ushort.MaxValue / 2.0f));
                            float normal_x = (normal_r - 128.0f) / 128.0f;
                            float normal_y = (normal_g - 128.0f) / 128.0f;
                            float normal_z = (normal_b - 128.0f) / 128.0f;

                            ClimateData[x, y].Temperature = local_temp;
                            ClimateData[x, y].Humidity = local_humid;
                            ClimateData[x, y].Elevation = elevation_norm;
                            ClimateData[x, y].NormalX = normal_x;
                            ClimateData[x, y].NormalY = normal_y;
                            ClimateData[x, y].NormalZ = normal_z;
                        }
                    }
                }

                climateFinal.UnlockBits(climate_raw);
                normalFinal.UnlockBits(normal_raw);
            }
        }

        //LocalX/LocalY normalized 0-1
        public LocalizedClimateData InterpolateClimateData(float LocalX, float LocalY, LocalizedClimateData.InterpolationMode InterpolationMode)
        {
            return LocalizedClimateData.InterpolateClimate(ClimateData, LocalX * ClimateData.GetLength(0), LocalY * ClimateData.GetLength(1), Mode: InterpolationMode);
        }

        public LocalizedClimateData InterpolateClimateDataWithOffset(float LocalX, float LocalY, float OffsetX, float OffsetY, LocalizedClimateData.InterpolationMode InterpolationMode)
        {
            var w = ClimateData.GetLength(0);
            var h = ClimateData.GetLength(1);
            return LocalizedClimateData.InterpolateClimate(ClimateData, LocalX * w, LocalY * h, OffsetX * w, OffsetY * h, Mode: InterpolationMode);
        }
    }
}
