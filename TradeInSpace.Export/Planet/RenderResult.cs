﻿using OSGeo.GDAL;
using System;
using System.Drawing;
using System.IO;

namespace TradeInSpace.Export.Planet
{
    public class RenderResult : IDisposable
    {
        public PlanetData PlanetData;

        public Rectangle InputImageBounds;
        public RectangleF CoordinateBounds;

        public byte[] Image;
        public short[] Heightmap;
        public byte[] OceanMask;
        public byte[] Temperature;
        public byte[] Humidity;

        public int TileSize { get; set; }
        public int TilesWide { get { return InputImageBounds.Width; } }
        public int TilesHigh { get { return InputImageBounds.Height; } }

        public int TotalWidth { get { return TilesWide * TileSize * 2; } }
        public int TotalHeight { get { return TilesHigh * TileSize; } }

        private bool IsDisposed;

        public RenderResult(PlanetData Planet, Rectangle ImageRegion, int TileRenderSize, bool NASACoords = true)
        {
            InputImageBounds = ImageRegion;
            TileSize = TileRenderSize;
            PlanetData = Planet;

            CoordinateBounds = PlanetData.SplatToCoordinates(ImageRegion, NASACoords);
        }

        #region Dispose code

        // // TODO: override finalizer only if 'Dispose(bool disposing)' has code to free unmanaged resources
        // ~RenderResult()
        // {
        //     // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
        //     Dispose(disposing: false);
        // }

        public void Dispose()
        {
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!IsDisposed)
            {
                if (disposing)
                {
                }

                Image = null;
                Heightmap = null;
                OceanMask = null;
                Temperature = null;
                Humidity = null;
                PlanetData = null;

                IsDisposed = true;
            }
        }

        #endregion

        public void Allocate()
        {
            Image = new byte[TotalWidth * TotalHeight * 4];
            Heightmap = new short[TotalWidth * TotalHeight];
            OceanMask = new byte[TotalWidth * TotalHeight];
            Temperature = new byte[TotalWidth * TotalHeight];
            Humidity = new byte[TotalWidth * TotalHeight];
        }

        public double[] GetGeometryTransform()
        {
            float blocksize_deg = CoordinateBounds.Width / TotalWidth;
            return new double[] { CoordinateBounds.Left, blocksize_deg, 0, CoordinateBounds.Bottom, 0, -blocksize_deg };
        }

        public void ExportImageGDAL(string OutputPath, string Driver = "GTiff", bool IncludeExport = false, string ExportDriver = "PNG", string ExportPath = null, string[] ExportOptions = null)
        {
            InnerExportGDAL(GDALSurfaceExport, OutputPath, Driver, IncludeExport, ExportDriver, ExportPath, ExportOptions);
        }

        public void ExportHeightmapGDAL(string OutputPath, string Driver = "GTiff", bool IncludeExport = false, string ExportDriver = "PNG", string ExportPath = null, string[] ExportOptions = null)
        {
            InnerExportGDAL(GDALHeightmapExport, OutputPath, Driver, IncludeExport, ExportDriver, ExportPath, ExportOptions);
        }

        public void ExportTemperatureGDAL(string OutputPath, string Driver = "GTiff", bool IncludeExport = false, string ExportDriver = "PNG", string ExportPath = null, string[] ExportOptions = null)
        {
            InnerExportGDAL(GDALTemperatureExport, OutputPath, Driver, IncludeExport, ExportDriver, ExportPath, ExportOptions);
        }

        public void ExportHumidityGDAL(string OutputPath, string Driver = "GTiff", bool IncludeExport = false, string ExportDriver = "PNG", string ExportPath = null, string[] ExportOptions = null)
        {
            InnerExportGDAL(GDALHumidityExport, OutputPath, Driver, IncludeExport, ExportDriver, ExportPath, ExportOptions);
        }

        public void ExportOceanMaskGDAL(string OutputPath, string Driver = "GTiff", bool IncludeExport = false, string ExportDriver = "PNG", string ExportPath = null, string[] ExportOptions = null)
        {
            InnerExportGDAL(GDALOceanMaskExport, OutputPath, Driver, IncludeExport, ExportDriver, ExportPath, ExportOptions);
        }

        private void InnerExportGDAL(Func<Driver, string, string[], Dataset> fnGDALLayer, string OutputPath, string Driver = "GTiff", bool IncludeExport = false, string ExportDriver = "PNG", string ExportPath = null, string[] ExportOptions = null, bool Append = false)
        {
            if (string.IsNullOrEmpty(ExportPath) && IncludeExport)
                throw new ArgumentNullException(ExportPath);

            if (File.Exists(OutputPath) && !Append)
                File.Delete(OutputPath);

            using (var drv = Gdal.GetDriverByName(Driver))
            {
                if (drv == null)
                {
                    throw new Exception("No GDAL driver found!");
                }

                string[] options = new string[] { "COMPRESS=DEFLATE" };


                using (var ds = fnGDALLayer(drv, OutputPath, options))
                {
                    ds.FlushCache();

                    if (IncludeExport)
                    {
                        string[] export_options = ExportOptions ?? new string[] { };
                        using (Driver export_drv = Gdal.GetDriverByName(ExportDriver))
                        using (Dataset export_ds = export_drv.CreateCopy(ExportPath, ds, 0, export_options, null, null))
                        {
                            export_ds.SetGeoTransform(GetGeometryTransform());
                            export_ds.FlushCache();
                        }
                    }
                }
            }
        }
        /*
        public void RenderBigTiff(string OutputPath, int ExportWidth, int ExportHeight, bool Append = false)
        {
            InnerExportGDAL(GDALSurfaceExport, "", "MEM", IncludeExport: true, Append: Append,
                ExportDriver: "GTiff", ExportPath: OutputPath);

            if (string.IsNullOrEmpty(OutputPath))
                throw new ArgumentNullException(OutputPath);

            if (File.Exists(OutputPath) && !Append)
                File.Delete(OutputPath);

            string[] Options = new[] {
                    "TILED=YES",
                    "BIGTIFF=YES",
                    "COMPRESS=DEFLATE",
                    "APPEND_SUBDATASET=YES"
                };

            Driver d = Gdal.GetDriverByName("GTiff");
            Dataset ds;
            if (Append)
            {
                
                ds = Gdal.OpenEx(OutputPath, (uint)GdalConst.GA_Update, new string[] { "GTiff" }, new string[] { "COMPRESS=DEFLATE" }, new string[] { });
            }
            else
            {
                var bandmap = new[] { 1, 2, 3, 4 };
                ds = d.Create(OutputPath, ExportWidth, ExportHeight, bandmap.Length, DataType.GDT_Byte, Options);

                ds.SetGeoTransform(GetGeometryTransform());
                ds.WriteRaster(0, 0, ExportWidth, ExportHeight, Image, ExportWidth, ExportHeight, 4, bandmap, 0, 0, 0);
            }

            using (var drv = d)
            {
                if (drv == null)
                {
                    throw new Exception("No GDAL driver found!");
                }

                string[] options = new string[] { "COMPRESS=DEFLATE" };


                using (var ds = fnGDALLayer(drv, OutputPath, options))
                {
                    ds.FlushCache();

                    if (IncludeExport)
                    {
                        string[] export_options = ExportOptions ?? new string[] { };
                        using (Driver export_drv = Gdal.GetDriverByName(ExportDriver))
                        using (Dataset export_ds = export_drv.CreateCopy(ExportPath, ds, 0, export_options, null, null))
                        {
                            export_ds.SetGeoTransform(GetGeometryTransform());
                            export_ds.FlushCache();
                        }
                    }
                }
            }
        }
        */

        public void RenderGeoPackage(string OutputPath, string TableName,  
            bool IncludeSurface = true, bool IncludeHeightmap = true, bool IncludeTemp = false, bool IncludeHumid = false, bool IncludeOceanMask = false, bool Append = false)
        {
            
            using (var drv = Gdal.GetDriverByName("GPKG"))
            {
                if (drv == null)
                    throw new Exception("No GDAL driver found!");
                if (File.Exists(OutputPath) && !Append)
                    File.Delete(OutputPath);

                var appendValue = "YES";
                
                if (IncludeSurface)
                {
                    var surface_options = new string[] { $"APPEND_SUBDATASET={appendValue}", "TILE_FORMAT=PNG", $"RASTER_TABLE={TableName}_surface" };
                    using (Dataset ds = GDALSurfaceExport(drv, OutputPath, surface_options))
                    {
                        ds.FlushCache();
                    }
                }

                if (IncludeHeightmap)
                {
                    var hm_options = new string[] { $"APPEND_SUBDATASET={appendValue}", "TILE_FORMAT=PNG", $"RASTER_TABLE={TableName}_heightmap" };
                    using (Dataset ds = GDALHeightmapExport(drv, OutputPath, hm_options))
                    {
                        ds.FlushCache();
                    }
                }

                if (IncludeTemp)
                {
                    var hm_options = new string[] { $"APPEND_SUBDATASET={appendValue}", "TILE_FORMAT=PNG", $"RASTER_TABLE={TableName}_temperature" };
                    using (Dataset ds = GDALTemperatureExport(drv, OutputPath, hm_options))
                    {
                        ds.FlushCache();
                    }
                }

                if (IncludeHumid)
                {
                    var hm_options = new string[] { $"APPEND_SUBDATASET={appendValue}", "TILE_FORMAT=PNG", $"RASTER_TABLE={TableName}_humidity" };
                    using (Dataset ds = GDALHumidityExport(drv, OutputPath, hm_options))
                    {
                        ds.FlushCache();
                    }
                }

                if (IncludeOceanMask)
                {
                    var hm_options = new string[] { $"APPEND_SUBDATASET={appendValue}", "TILE_FORMAT=PNG", $"RASTER_TABLE={TableName}_ocean" };
                    using (Dataset ds = GDALOceanMaskExport(drv, OutputPath, hm_options))
                    {
                        ds.FlushCache();
                    }
                }
            }
        }

        private Dataset GDALSurfaceExport(Driver GdalDriver, string OutputPath, string[] Options)
        {
            var bandmap = new[] { 1, 2, 3, 4 };
            Dataset ds = GdalDriver.Create(OutputPath, TotalWidth, TotalHeight, bandmap.Length, DataType.GDT_Byte, Options);
            
            ds.SetGeoTransform(GetGeometryTransform());
            ds.WriteRaster(0, 0, TotalWidth, TotalHeight, Image, TotalWidth, TotalHeight, 4, bandmap, 0, 0, 0);
            return ds;
        }

        private Dataset GDALHeightmapExport(Driver GdalDriver, string OutputPath, string[] Options)
        {
            Dataset ds = GdalDriver.Create(OutputPath, TotalWidth, TotalHeight, 1, DataType.GDT_Int16, Options);
            ds.SetGeoTransform(GetGeometryTransform());
            ds.WriteRaster(0, 0, TotalWidth, TotalHeight, Heightmap, TotalWidth, TotalHeight, 1, new[] { 1 }, 0, 0, 0);
            return ds;
        }

        private Dataset GDALTemperatureExport(Driver GdalDriver, string OutputPath, string[] Options)
        {
            Dataset ds = GdalDriver.Create(OutputPath, TotalWidth, TotalHeight, 1, DataType.GDT_Byte, Options);
            ds.SetGeoTransform(GetGeometryTransform());
            ds.WriteRaster(0, 0, TotalWidth, TotalHeight, Temperature, TotalWidth, TotalHeight, 1, new[] { 1 }, 0, 0, 0);
            return ds;
        }

        private Dataset GDALHumidityExport(Driver GdalDriver, string OutputPath, string[] Options)
        {
            Dataset ds = GdalDriver.Create(OutputPath, TotalWidth, TotalHeight, 1, DataType.GDT_Byte, Options);
            ds.SetGeoTransform(GetGeometryTransform());
            ds.WriteRaster(0, 0, TotalWidth, TotalHeight, Humidity, TotalWidth, TotalHeight, 1, new[] { 1 }, 0, 0, 0);
            return ds;
        }

        private Dataset GDALOceanMaskExport(Driver GdalDriver, string OutputPath, string[] Options)
        {
            Dataset ds = GdalDriver.Create(OutputPath, TotalWidth, TotalHeight, 1, DataType.GDT_Byte, Options);
            ds.SetGeoTransform(GetGeometryTransform());
            ds.WriteRaster(0, 0, TotalWidth, TotalHeight, OceanMask, TotalWidth, TotalHeight, 1, new[] { 1 }, 0, 0, 0);
            return ds;
        }

        private void ValidateCoords(ref int ResultX, ref int ResultY)
        {
            if (ResultX < 0) throw new ArgumentException(nameof(ResultX));
            if (ResultY < 0) throw new ArgumentException(nameof(ResultY));
            if (ResultX >= TotalWidth) throw new ArgumentException(nameof(ResultX));
            if (ResultY >= TotalHeight) throw new ArgumentException(nameof(ResultY));
        }

        public void SetAllResultChannels(byte Temp, byte Humid, int ResultX, int ResultY, byte ColorR, byte ColorG, byte ColorB, short Elevation, byte OceanValue)
        {
            ValidateCoords(ref ResultX, ref ResultY);
            int output_index = (ResultY * TotalWidth + ResultX);
            int channel_stride = TotalWidth * TotalHeight;

            Image[(channel_stride * 0) + output_index] = ColorR;
            Image[(channel_stride * 1) + output_index] = ColorG;
            Image[(channel_stride * 2) + output_index] = ColorB;
            Image[(channel_stride * 3) + output_index] = 255;
            Heightmap[output_index] = Elevation;
            OceanMask[output_index] = OceanValue;
            Temperature[output_index] = Temp;
            Humidity[output_index] = Humid;
        }

        public void SetResultPixel(int ResultX, int ResultY, byte ColorR, byte ColorG, byte ColorB)
        {
            ValidateCoords(ref ResultX, ref ResultY);
            int output_index = (ResultY * TotalWidth + ResultX);
            int channel_stride = TotalWidth * TotalHeight;

            Image[(channel_stride * 0) + output_index] = ColorR;
            Image[(channel_stride * 1) + output_index] = ColorG;
            Image[(channel_stride * 2) + output_index] = ColorB;
            Image[(channel_stride * 3) + output_index] = 255;
        }

        public (byte R, byte G, byte B, byte A) GetResultPixel(int ResultX, int ResultY)
        {
            ValidateCoords(ref ResultX, ref ResultY);
            int output_index = (ResultY * TotalWidth + ResultX);
            int channel_stride = TotalWidth * TotalHeight;

            return (Image[(channel_stride * 0) + output_index],
                    Image[(channel_stride * 1) + output_index],
                    Image[(channel_stride * 2) + output_index],
                    Image[(channel_stride * 3) + output_index]);
        }

        public (byte T, byte H) GetResultClimate(int ResultX, int ResultY)
        {
            ValidateCoords(ref ResultX, ref ResultY);
            int output_index = (ResultY * TotalWidth + ResultX);

            return (Temperature[output_index], Humidity[output_index]);
        }

        public short GetResultHeight(int ResultX, int ResultY)
        {
            ValidateCoords(ref ResultX, ref ResultY);
            int output_index = (ResultY * TotalWidth + ResultX);

            return Heightmap[output_index];
        }

        public byte GetResultOceanMask(int ResultX, int ResultY)
        {
            ValidateCoords(ref ResultX, ref ResultY);
            int output_index = (ResultY * TotalWidth + ResultX);

            return OceanMask[output_index];
        }

        public (float Slope, float Aspect) ComputeSlope(int ResultX, int ResultY, float ZFactor = 1.0f)
        {
            int SamplePositionWithWrap(int XSample, int YSample)
            {
                var finalX = (ResultX + XSample) % TotalWidth;
                var finalY = (ResultY + YSample) % TotalHeight;
                if (finalX < 0) finalX += TotalWidth;
                if (finalY < 0) finalY += TotalHeight;
                return finalY * TotalWidth + finalX;
            }

            var a = Heightmap[SamplePositionWithWrap(-1, -1)];
            var b = Heightmap[SamplePositionWithWrap(0, -1)];
            var c = Heightmap[SamplePositionWithWrap(1, -1)];
            var d = Heightmap[SamplePositionWithWrap(-1, 0)];
            //var e = HeightmapData[SamplePosition(0, 0)];
            var f = Heightmap[SamplePositionWithWrap(1, 0)];
            var g = Heightmap[SamplePositionWithWrap(-1, 1)];
            var h = Heightmap[SamplePositionWithWrap(0, 1)];
            var i = Heightmap[SamplePositionWithWrap(1, 1)];

            //Cellsize needs to be the same scale as the X/Y distance
            //This calculation does not take into account projection warping at all
            var planet_circ_m = Math.PI * 2 * PlanetData.RadiusM;
            var cellsize_m = planet_circ_m / (PlanetData.TileCount * TileSize * 2);

            var dzdx = ((c + 2 * f + i) - (a + 2 * d + g)) / (8 * cellsize_m);
            var dzdy = ((g + 2 * h + i) - (a + 2 * b + c)) / (8 * cellsize_m);
            var aspect = 0.0;

            var slope = Math.Atan(ZFactor * Math.Sqrt(dzdx * dzdx + dzdy * dzdy));

            if (dzdx != 0)
            {
                aspect = Math.Atan2(dzdy, -dzdx);
                if (aspect < 0)
                    aspect += Math.PI * 2;
            }
            else
            {
                if (dzdy > 0)
                    aspect = Math.PI / 2;
                else if (dzdy < 0)
                    aspect = (Math.PI * 2) - (Math.PI / 2);
            }

            return ((float)slope, (float)aspect);
        }
    }
}
