﻿using System;
using System.Drawing;

namespace TradeInSpace.Export.Planet
{
    public class MathHelper
    {
        public static float ClampValue(float Value, float Min, float Max)
        {
            if (Value == float.NaN) return 0.0f;
            return (float)Math.Min(Max, Math.Max(Min, Value));
        }
        public static int ClampValue(int Value, int Min, int Max)
        {
            if (Value > Max) return Max;
            if (Value < Min) return Min;
            return Value;
        }

        public static float LerpValue(float Min, float Max, float Progress)
        {
            return ((Max - Min) * Progress) + Min;
        }

        public static float LerpValue2D(float UL, float UR, float LL, float LR, float ValueX, float ValueY)
        {
            var yMinValue = LerpValue(UL, UR, ValueX);
            var yMaxValue = LerpValue(LL, LR, ValueX);
            return LerpValue(yMinValue, yMaxValue, ValueY);
        }

        public static (int, int, int) LerpColor(Color GradientA, Color GradientB, byte GradientValue)
        {
            return (
                (int)LerpValue(GradientA.R, GradientB.R, GradientValue / 127.0f),
                (int)LerpValue(GradientA.G, GradientB.G, GradientValue / 127.0f),
                (int)LerpValue(GradientA.B, GradientB.B, GradientValue / 127.0f));
        }

        // props to https://stackoverflow.com/a/20924576/240845 for getting me started
        public static float InterpolateCubic(float v0, float v1, float v2, float v3, float fraction)
        {
            float p = (v3 - v2) - (v0 - v1);
            float q = (v0 - v1) - p;
            float r = v2 - v0;

            return (fraction * ((fraction * ((fraction * p) + q)) + r)) + v1;
        }

        public static (float slope, float offset) CalculateLinear(float V0, float V1)
        {
            float slope = V1 - V0;
            float offset = V0;
            return (slope, offset);
        }

        public static RectangleF ClampRectangle(
            float MinX, float MinY, float MaxX, float MaxY, 
            float ClampMinX, float ClampMinY, float ClampMaxX, float ClampMaxY)
        {
            MinX = Math.Min(MinX, MaxX);
            MaxX = Math.Max(MinX, MaxX);
            MinY = Math.Min(MinY, MaxY);
            MaxY = Math.Max(MinY, MaxY);

            var clampedStart = new PointF(
                MathHelper.ClampValue(MinX, ClampMinX, ClampMaxX),
                MathHelper.ClampValue(MinY, ClampMinY, ClampMaxY));
            var clampedSize = new SizeF(
                MathHelper.ClampValue(MaxX, ClampMinX, ClampMaxX) - clampedStart.X,
                MathHelper.ClampValue(MaxY, ClampMinY, ClampMaxY) - clampedStart.Y);
            return new RectangleF(clampedStart, clampedSize);
        }

        //https://www.paulinternet.nl/?page=bicubic
        public static double[,] CalculateBiCubicCoefficients(Func<int, int, float> SampleFunction)
        {
            var result = new double[4, 4];
            result[0,0] = SampleFunction(1,1);
            result[0,1] = -.5 * SampleFunction(1,0) + .5 * SampleFunction(1,2);
            result[0,2] = SampleFunction(1,0) - 2.5 * SampleFunction(1,1) + 2 * SampleFunction(1,2) - .5 * SampleFunction(1,3);
            result[0,3] = -.5 * SampleFunction(1,0) + 1.5 * SampleFunction(1,1) - 1.5 * SampleFunction(1,2) + .5 * SampleFunction(1,3);
            result[1,0] = -.5 * SampleFunction(0,1) + .5 * SampleFunction(2,1);
            result[1,1] = .25 * SampleFunction(0,0) - .25 * SampleFunction(0,2) - .25 * SampleFunction(2,0) + .25 * SampleFunction(2,2);
            result[1,2] = -.5 * SampleFunction(0,0) + 1.25 * SampleFunction(0,1) - SampleFunction(0,2) + .25 * SampleFunction(0,3) + 5 * SampleFunction(2,0) - 1.25 * SampleFunction(2,1) + SampleFunction(2,2) - .25 * SampleFunction(2,3);
            result[1,3] = .25 * SampleFunction(0,0) - .75 * SampleFunction(0,1) + .75 * SampleFunction(0,2) - .25 * SampleFunction(0,3) - .25 * SampleFunction(2,0) + .75 * SampleFunction(2,1) - .75 * SampleFunction(2,2) + .25 * SampleFunction(2,3);
            result[2,0] = SampleFunction(0,1) - 2.5 * SampleFunction(1,1) + 2 * SampleFunction(2,1) - 5 * SampleFunction(3,1);
            result[2,1] = -5 * SampleFunction(0,0) + .5 * SampleFunction(0,2) + 1.25 * SampleFunction(1,0) - 1.25 * SampleFunction(1,2) - SampleFunction(2,0) + SampleFunction(2,2) + .25 * SampleFunction(3,0) - .25 * SampleFunction(3,2);
            result[2,2] = SampleFunction(0,0) - 2.5 * SampleFunction(0,1) + 2 * SampleFunction(0,2) - 5 * SampleFunction(0,3) - 25 * SampleFunction(1,0) + 6.25 * SampleFunction(1,1) - 5 * SampleFunction(1,2) + 1.25 * SampleFunction(1,3) + 2 * SampleFunction(2,0) - 5 * SampleFunction(2,1) + 4 * SampleFunction(2,2) - SampleFunction(2,3) - 5 * SampleFunction(3,0) + 1.25 * SampleFunction(3,1) - SampleFunction(3,2) + .25 * SampleFunction(3,3);
            result[2,3] = -.5 * SampleFunction(0,0) + 1.5 * SampleFunction(0,1) - 1.5 * SampleFunction(0,2) + 5 * SampleFunction(0,3) + 1.25 * SampleFunction(1,0) - 3.75 * SampleFunction(1,1) + 3.75 * SampleFunction(1,2) - 1.25 * SampleFunction(1,3) - SampleFunction(2,0) + 3 * SampleFunction(2,1) - 3 * SampleFunction(2,2) + SampleFunction(2,3) + .25 * SampleFunction(3,0) - .75 * SampleFunction(3,1) + .75 * SampleFunction(3,2) - .25 * SampleFunction(3,3);
            result[3,0] = -.5 * SampleFunction(0,1) + 1.5 * SampleFunction(1,1) - 1.5 * SampleFunction(2,1) + 5 * SampleFunction(3,1);
            result[3,1] = .25 * SampleFunction(0,0) - .25 * SampleFunction(0,2) - .75 * SampleFunction(1,0) + .75 * SampleFunction(1,2) + .75 * SampleFunction(2,0) - .75 * SampleFunction(2,2) - .25 * SampleFunction(3,0) + .25 * SampleFunction(3,2);
            result[3,2] = -.5 * SampleFunction(0,0) + 1.25 * SampleFunction(0,1) - SampleFunction(0,2) + .25 * SampleFunction(0,3) + 15 * SampleFunction(1,0) - 3.75 * SampleFunction(1,1) + 3 * SampleFunction(1,2) - .75 * SampleFunction(1,3) - 1.5 * SampleFunction(2,0) + 3.75 * SampleFunction(2,1) - 3 * SampleFunction(2,2) + .75 * SampleFunction(2,3) + .5 * SampleFunction(3,0) - 1.25 * SampleFunction(3,1) + SampleFunction(3,2) - .25 * SampleFunction(3,3);
            result[3,3] = .25 * SampleFunction(0,0) - .75 * SampleFunction(0,1) + .75 * SampleFunction(0,2) - .25 * SampleFunction(0,3) - .75 * SampleFunction(1,0) + 2.25 * SampleFunction(1,1) - 2.25 * SampleFunction(1,2) + .75 * SampleFunction(1,3) + .75 * SampleFunction(2,0) - 2.25 * SampleFunction(2,1) + 2.25 * SampleFunction(2,2) - .75 * SampleFunction(2,3) - .25 * SampleFunction(3,0) + .75 * SampleFunction(3,1) - .75 * SampleFunction(3,2) + .25 * SampleFunction(3,3);

            return result;
        }

        public static double CalculateBiCubicValue(double[,] coeffients, float x, float y)
        {
            double x2 = x * x;
            double x3 = x2 * x;
            double y2 = y * y;
            double y3 = y2 * y;

            return (coeffients[0,0] + coeffients[0,1] * y + coeffients[0,2] * y2 + coeffients[0,3] * y3) +
                   (coeffients[1,0] + coeffients[1,1] * y + coeffients[1,2] * y2 + coeffients[1,3] * y3) * x +
                   (coeffients[2,0] + coeffients[2,1] * y + coeffients[2,2] * y2 + coeffients[2,3] * y3) * x2 +
                   (coeffients[3,0] + coeffients[3,1] * y + coeffients[3,2] * y2 + coeffients[3,3] * y3) * x3;
        }


        public static (float, float) ImagePixelsToM(float X, float Y, int Size, float PlanetRadiusM)
        {
            if (X < 0) X += Size;
            if (Y < 0) Y += Size;
            if (X >= Size) X -= Size;
            if (Y >= Size) Y -= Size;

            var half_circumference_km = (float)(Math.PI * PlanetRadiusM);

            var vert_distance = ((Y / Size) - 0.5f) * half_circumference_km; // +/- 1/4 circumference
            var vert_circ = CircumferenceAtDistanceFromEquator(vert_distance, PlanetRadiusM);
            var horiz_distance = ((X / Size) - 0.5f) * vert_circ; // +/- 1/2 circumference

            return (horiz_distance, vert_distance);
        }

        public static float CircumferenceAtDistanceFromEquator(float VerticalDistanceM, float PlanetRadiusM)
        {
            var half_circumference_km = Math.PI * PlanetRadiusM;

            var angle = (VerticalDistanceM / half_circumference_km) * Math.PI; //Normalize to +/-0.5 * pi to get +/- half_pi
            return (float)(Math.Cos(angle) * PlanetRadiusM * Math.PI * 2);
        }

        public static (float lat, float lon) PositionToLatLon(float HorizontalDistanceKM, float VerticalDistanceKM, float PlanetRadiusM)
        {
            var (vert_normalized, horiz_normalized) = NormalizedLocation(HorizontalDistanceKM, VerticalDistanceKM, PlanetRadiusM);

            return (vert_normalized * 180, horiz_normalized * 360);
        }

        public static (float x, float y) NormalizedLocation(float HorizontalDistanceM, float VerticalDistanceM, float PlanetRadiusM)
        {
            var half_circumference_m = (float)(Math.PI * PlanetRadiusM);

            var vert_normalized = ((VerticalDistanceM / half_circumference_m) + 0.5f);
            var vert_circ = CircumferenceAtDistanceFromEquator(VerticalDistanceM, PlanetRadiusM);
            var horiz_normalized = ((HorizontalDistanceM / vert_circ) + 0.5f);

            return (Math.Min(1.0f, Math.Max(0.0f, horiz_normalized)),
                    Math.Min(1.0f, Math.Max(0.0f, vert_normalized))); //  0.0 - 1.0 for x,y
        }

        public static (float c_x, float c_y, float dy, float udx, float ldx) GetLocalImageWarping(float HDistanceM, float VDistanceM, float PatchWidthM, float PatchHeightM, float PlanetRadiusM)
        {
            var center = NormalizedLocation(HDistanceM, VDistanceM, PlanetRadiusM);

            var upper_circ = CircumferenceAtDistanceFromEquator(VDistanceM + (PatchHeightM / 2), PlanetRadiusM);
            var lower_circ = CircumferenceAtDistanceFromEquator(VDistanceM - (PatchHeightM / 2), PlanetRadiusM);

            var vertical_delta = (float)(PatchHeightM / 2.0f / (Math.PI * PlanetRadiusM));
            var upper_width = (PatchWidthM / 2.0f) / upper_circ;
            var lower_width = (PatchWidthM / 2.0f) / lower_circ;

            //All coordinates are in normalized image space (0-1.0 X/Y)
            return (center.x, center.y, vertical_delta, upper_width, lower_width);
        }

        public class CachedBicubicInterpolator
        {
            private double a00, a01, a02, a03;
            private double a10, a11, a12, a13;
            private double a20, a21, a22, a23;
            private double a30, a31, a32, a33;

            public CachedBicubicInterpolator() { }
            public CachedBicubicInterpolator(Func<int, int, float> fnSample)
            {
                updateCoefficients(fnSample);
            }

            public void updateCoefficients(Func<int, int, float> fnSample)
            {
                a00 = fnSample(1,1);
                a01 = -.5 * fnSample(1,0) + .5 * fnSample(1,2);
                a02 = fnSample(1,0) - 2.5 * fnSample(1,1) + 2 * fnSample(1,2) - .5 * fnSample(1,3);
                a03 = -.5 * fnSample(1,0) + 1.5 * fnSample(1,1) - 1.5 * fnSample(1,2) + .5 * fnSample(1,3);
                a10 = -.5 * fnSample(0,1) + .5 * fnSample(2,1);
                a11 = .25 * fnSample(0,0) - .25 * fnSample(0,2) - .25 * fnSample(2,0) + .25 * fnSample(2,2);
                a12 = -.5 * fnSample(0,0) + 1.25 * fnSample(0,1) - fnSample(0,2) + .25 * fnSample(0,3) + .5 * fnSample(2,0) - 1.25 * fnSample(2,1) + fnSample(2,2) - .25 * fnSample(2,3);
                a13 = .25 * fnSample(0,0) - .75 * fnSample(0,1) + .75 * fnSample(0,2) - .25 * fnSample(0,3) - .25 * fnSample(2,0) + .75 * fnSample(2,1) - .75 * fnSample(2,2) + .25 * fnSample(2,3);
                a20 = fnSample(0,1) - 2.5 * fnSample(1,1) + 2 * fnSample(2,1) - .5 * fnSample(3,1);
                a21 = -.5 * fnSample(0,0) + .5 * fnSample(0,2) + 1.25 * fnSample(1,0) - 1.25 * fnSample(1,2) - fnSample(2,0) + fnSample(2,2) + .25 * fnSample(3,0) - .25 * fnSample(3,2);
                a22 = fnSample(0,0) - 2.5 * fnSample(0,1) + 2 * fnSample(0,2) - .5 * fnSample(0,3) - 2.5 * fnSample(1,0) + 6.25 * fnSample(1,1) - 5 * fnSample(1,2) + 1.25 * fnSample(1,3) + 2 * fnSample(2,0) - 5 * fnSample(2,1) + 4 * fnSample(2,2) - fnSample(2,3) - .5 * fnSample(3,0) + 1.25 * fnSample(3,1) - fnSample(3,2) + .25 * fnSample(3,3);
                a23 = -.5 * fnSample(0,0) + 1.5 * fnSample(0,1) - 1.5 * fnSample(0,2) + .5 * fnSample(0,3) + 1.25 * fnSample(1,0) - 3.75 * fnSample(1,1) + 3.75 * fnSample(1,2) - 1.25 * fnSample(1,3) - fnSample(2,0) + 3 * fnSample(2,1) - 3 * fnSample(2,2) + fnSample(2,3) + .25 * fnSample(3,0) - .75 * fnSample(3,1) + .75 * fnSample(3,2) - .25 * fnSample(3,3);
                a30 = -.5 * fnSample(0,1) + 1.5 * fnSample(1,1) - 1.5 * fnSample(2,1) + .5 * fnSample(3,1);
                a31 = .25 * fnSample(0,0) - .25 * fnSample(0,2) - .75 * fnSample(1,0) + .75 * fnSample(1,2) + .75 * fnSample(2,0) - .75 * fnSample(2,2) - .25 * fnSample(3,0) + .25 * fnSample(3,2);
                a32 = -.5 * fnSample(0,0) + 1.25 * fnSample(0,1) - fnSample(0,2) + .25 * fnSample(0,3) + 1.5 * fnSample(1,0) - 3.75 * fnSample(1,1) + 3 * fnSample(1,2) - .75 * fnSample(1,3) - 1.5 * fnSample(2,0) + 3.75 * fnSample(2,1) - 3 * fnSample(2,2) + .75 * fnSample(2,3) + .5 * fnSample(3,0) - 1.25 * fnSample(3,1) + fnSample(3,2) - .25 * fnSample(3,3);
                a33 = .25 * fnSample(0,0) - .75 * fnSample(0,1) + .75 * fnSample(0,2) - .25 * fnSample(0,3) - .75 * fnSample(1,0) + 2.25 * fnSample(1,1) - 2.25 * fnSample(1,2) + .75 * fnSample(1,3) + .75 * fnSample(2,0) - 2.25 * fnSample(2,1) + 2.25 * fnSample(2,2) - .75 * fnSample(2,3) - .25 * fnSample(3,0) + .75 * fnSample(3,1) - .75 * fnSample(3,2) + .25 * fnSample(3,3);
            }

            public double CalculateValue(double x, double y)
            {
                double x2 = x * x;
                double x3 = x2 * x;
                double y2 = y * y;
                double y3 = y2 * y;

                return (a00 + a01 * y + a02 * y2 + a03 * y3) +
                       (a10 + a11 * y + a12 * y2 + a13 * y3) * x +
                       (a20 + a21 * y + a22 * y2 + a23 * y3) * x2 +
                       (a30 + a31 * y + a32 * y2 + a33 * y3) * x3;
            }
        }
    }
}
