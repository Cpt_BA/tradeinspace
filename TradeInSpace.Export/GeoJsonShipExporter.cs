﻿using NetTopologySuite.Features;
using NetTopologySuite.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using TradeInSpace.Explore;
using TradeInSpace.Explore.DataForge;
using TradeInSpace.Explore.Socpak;

namespace TradeInSpace.Export
{
    using NTSG = NetTopologySuite.Geometries;

    public class GeoJsonShipExporter
    {
        public static bool ExportShip(DFDatabase gameData, SCHieracrhyNode node, string outPath,
            Func<Vector3, double> dimA, Func<Vector3, double> dimB, Func<Vector3, double> dimDepth)
        {
            FeatureCollection features = new FeatureCollection();

            void ExploreNode(SCHieracrhyNode _exploreNode)
            {
                try
                {
                    var verts = _exploreNode.GeometryData?.Vertexes;

                    if (verts != null)
                    {
                        if (verts.Count < 4) return;

                        var projectedPoints = verts
                            .Select(v => new Vector3(v.Item1, v.Item2, v.Item3))
                            .Select(_exploreNode.TranfomPoint);

                        var mapPoints = projectedPoints
                            .Select(pv => new Vector2(dimA(pv), dimB(pv)))
                            .ToList();
                        
                        var startDepth = projectedPoints.Min(dimDepth);
                        var endDepth = projectedPoints.Max(dimDepth);

                        var modelHull = ModelCache.GetConvexHull(mapPoints);

                        var props = new Dictionary<string, object>()
                        {
                            { "StartDepth", startDepth },
                            { "EndDepth", endDepth }
                        };

                        modelHull.Add(modelHull[0]); //Close the loop

                        if (modelHull.Count < 4) return;

                        var loop = new NTSG.LinearRing(modelHull.Select(p => new NTSG.Coordinate((float)p.X, (float)p.Y)).ToArray());
                        var modelPoly = new NTSG.Polygon(loop);
                        //var feature = new Feature(modelPoly, props);

                        features.Add(new Feature(modelPoly, new AttributesTable(props)));
                    }
                }
                finally
                {
                    foreach (var child in _exploreNode.Children)
                        ExploreNode(child);
                }
            }

            ExploreNode(node);


            bool saved = false;
            var dummyItem = new Feature(new NTSG.Point(0, 0), new AttributesTable());

            for (int attempt = 0; attempt < 5 && !saved; attempt++)
            {
                try
                {
                    var shpWriter = new ShapefileDataWriter(outPath, NTSG.GeometryFactory.Default);
                    if (features.Count > 0)
                        shpWriter.Header = ShapefileDataWriter.GetHeader(features.FirstOrDefault(), features.Count);
                    else
                        shpWriter.Header = ShapefileDataWriter.GetHeader(new DbaseFieldDescriptor[] { }, 0);
                    shpWriter.Write(features); //One collection containing all features.
                    saved = true;
                }
                catch (Exception ex)
                {

                }
            }

            return saved;
        }
    }
}
