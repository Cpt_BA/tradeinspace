﻿using CgfConverter;
using CgfConverter.CryEngineCore;
using CgfConverter.Models;
using Serilog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using TradeInSpace.Explore;
using TradeInSpace.Explore.DataForge;
using TradeInSpace.Explore.Socpak;
using TradeInSpace.Explore.Subsumption;
using UkooLabs.FbxSharpie;

namespace TradeInSpace.Export
{
    class ExportPoint
    {
        public int ID;
        public string Name;
        public Vector3 Position = new Vector3();
        public Vector4 Rotation = new Vector4(0, 0, 0, 1);
        public Vector3 Scale = new Vector3(-1, 1, 1);
        public Vector3 MinBounds = new Vector3();
        public Vector3 MaxBounds = new Vector3();
        public int ParentID = -1;
        public string ModelPath = null;
    }

    enum ModelUpdateMode
    {
        OnlyNew,
        ThisRun,
        All
    }

    public class CSVExporter
    {
        public static string ExportCSV(DFDatabase gameData, SCHieracrhyNode rootNode, string ModelExportPath)
        {
            ArgsHandler modelArgs = new ArgsHandler()
            {
                OutputDir = ModelExportPath,
                NoTextures = true
            };

            StringBuilder finalOutput = new StringBuilder();
            List<(SCHieracrhyNode, int, int)> globalPositions = new List<(SCHieracrhyNode, int, int)>();
            Dictionary<int, List<Vector3>> splines = new Dictionary<int, List<Vector3>>();

            ModelUpdateMode updateMode = ModelUpdateMode.All;
            DateTime startTime = DateTime.Now;

            bool globalMode = false;

            int id = 0;

            int fnAddPoint(SCHieracrhyNode node, int parent_id = -1)
            {
                id++;
                globalPositions.Add((node, id, parent_id));
                return id;
            }

            void fnProcessNode(SCHieracrhyNode procNode, SCHieracrhyNode parentNode, int parentID)
            {
                int node_id = fnAddPoint(procNode, parentID);

                foreach (var child in procNode.Children)
                {
                    fnProcessNode(child, procNode, node_id);
                }
            }

            fnProcessNode(rootNode, null, -1);

            finalOutput.AppendLine("Name,X,Y,Z,rX,rY,rZ,rW,sX,sY,sZ,bMinX,bMinY,bMinZ,bMaxX,bMaxY,bMaxZ,id,p_id,model");

            foreach (var (point, my_id, parent_id) in globalPositions)
            {
                var clean_name = string.Join("_", point.Name
                    .Replace('\u00A0', '_')
                    .Replace(" ", "_")
                    .Replace("-", "_")
                    .Replace("&", "_")
                    .Replace(",", "_")
                    .Replace("'", "")
                    .Replace("\"", "")
                    .Replace("#", "")
                    .Replace("(", "")
                    .Replace(")", "")
                    .Split(new char[] { '_' }, StringSplitOptions.RemoveEmptyEntries));

                Vector3 exportPos = new Vector3();
                Vector4 exportRot = new Vector4(0, 0, 0, 1);
                Vector3 exportScale = new Vector3(1, 1, 1);

                if (globalMode)
                {
                    switch (point.NodeType)
                    {
                        case SCHieracrhyNodeType.Entity:
                        case SCHieracrhyNodeType.Geometry:
                            exportPos = point.GlobalPosition;
                            exportRot = point.GlobalRotation;
                            exportScale = point.GlobalScale;
                            break;
                    }
                }
                else
                {
                    exportPos = point.LocalPosition;
                    exportRot = point.LocalRotation;
                    exportScale = point.LocalScale;
                }

                if(point.ModelPath != null)
                {
                    var outPath = Path.Combine(ModelExportPath, point.ModelPathFixed);

                    bool takeModel = false;

                    if (updateMode == ModelUpdateMode.All) takeModel = true;
                    else if (updateMode == ModelUpdateMode.ThisRun && new FileInfo(outPath).CreationTime < startTime) takeModel = true;
                    else takeModel = !File.Exists(outPath);

                    if (takeModel)
                    {
                        var modelCGA = gameData.ReadPackedCGAFile(point.ModelPath);

                        
                        try
                        {
                            modelCGA.ProcessCryengineFiles();
                            //Collada exporter = new Collada(modelArgs, modelCGA);
                            //exporter.Render(ModelExportPath, true);
                        }
                        catch (Exception ex)
                        {

                        }
                        
                        try
                        {

                            var fbxModel = CryToFBXExporter.ConvertModelToScene(modelCGA);
                            var fbxPath = Path.ChangeExtension(outPath, ".fbx");

                            if (File.Exists(fbxPath))
                                File.Delete(fbxPath);

                            using (var out_stream = File.OpenWrite(fbxPath))
                                new FbxBinaryWriter(out_stream).Write(fbxModel.Construct());
                        }
                        catch(Exception ex) { }
                    }
                }

                var type_prefix = point.NodeType.ToString().Substring(0, 1);
                
                var pos = FormattableString.Invariant($"{exportPos.X},{exportPos.Y},{exportPos.Z}");
                var rot = FormattableString.Invariant($"{exportRot.X},{exportRot.Y},{exportRot.Z},{exportRot.W}");
                var scl = FormattableString.Invariant($"{exportScale.X},{exportScale.Y},{exportScale.Z}");
                var bmin = "0,0,0";// $"{point.MinBounds.X},{point.MinBounds.Y},{point.MinBounds.Z}";
                var bmax = "0,0,0";// $"{point.MaxBounds.X},{point.MaxBounds.Y},{point.MaxBounds.Z}";

                finalOutput.Append(FormattableString.Invariant($"{type_prefix}_{clean_name},{pos},{rot},{scl},{bmin},{bmax},{my_id},{parent_id},{point.ModelPath}"));
                if (splines.ContainsKey(my_id))
                {
                    var pointText = string.Join(",", splines[my_id].Select(c => FormattableString.Invariant($"{c.X / 10000},{c.Y / 10000},{c.Z / 10000}")));

                    finalOutput.Append(FormattableString.Invariant($",{pointText}"));
                }

                finalOutput.AppendLine();
            }

            return finalOutput.ToString();
        }

    }
}
