﻿using System.Collections.Generic;
using System.IO;
using UkooLabs.FbxSharpie;
using UkooLabs.FbxSharpie.Tokens;
using UkooLabs.FbxSharpie.Tokens.Value;

namespace TradeInSpace.Export.FBX
{
    public abstract class FBXSceneItem
    {
        public long ID { get; }
        public string Name { get; }
        public string ObjectType { get; }
        public string Extra { get; set; }

        public List<FBXSceneItem> Children { get; set; } = new List<FBXSceneItem>();
        public FBXProperties70 Properties { get; set; } = new FBXProperties70();
        public Dictionary<string, Token> DirectTokens { get; set; } = new Dictionary<string, Token>();

        protected FBXSceneItem(long ID, string Name, string ObjectType, string Extra = "Null")
        {
            this.ID = ID;
            this.Name = Name;
            this.ObjectType = ObjectType;
            this.Extra = Extra;
        }

        public virtual FbxNode Construct()
        {
            var node = new FbxNode(new IdentifierToken(ObjectType));

            node.AddProperty(new LongToken(ID));
            node.AddProperty(new StringToken(Name));
            node.AddProperty(new StringToken(Extra)); //?

            foreach (var token in DirectTokens)
            {
                node.AddNode(new FbxNode(new IdentifierToken(token.Key)) { Value = token.Value });
            }

            return node;
        }
    }

    public class FBXSceneNodeAttribute : FBXSceneItem
    {
        public FBXSceneNodeAttribute(long ID, string Name) :
            base(ID, Name + "\x00\x01NodeAttribute", "NodeAttribute")
        {

        }

        public override FbxNode Construct()
        {
            var base_node = base.Construct();

            var typeFlags = new FbxNode(new IdentifierToken("TypeFlags"));
            typeFlags.AddProperty(new StringToken("Null"));
            base_node.AddNode(typeFlags);

            base_node.AddNode(Properties.Construct());

            return base_node;
        }
    }

    public class FBXSceneGeometry : FBXSceneItem
    {
        public List<FBXLayer> Layers { get; set; } = new List<FBXLayer>();
        public double[] Vertices { get; set; } = null;
        public int[] VertexIndicies { get; set; } = null;
        public int[] Edges { get; set; } = null;

        public FBXSceneGeometry(long ID, string Name) :
            base(ID, Name + "\x00\x01Geometry", "Geometry", Extra: "Mesh")
        {

        }

        public override FbxNode Construct()
        {
            var base_node = base.Construct();

            base_node.AddNode(this.Properties.Construct());
            base_node.AddNode(FBXSimpleNode.FromInt("GeometryVersion", 124));
            if (Vertices?.Length > 0)
                base_node.AddNode(FBXSimpleNode.FromArray("Vertices", Vertices));
            if (VertexIndicies?.Length > 0)
                base_node.AddNode(FBXSimpleNode.FromArray("PolygonVertexIndex", VertexIndicies));
            if (Edges?.Length > 0)
                base_node.AddNode(FBXSimpleNode.FromArray("Edges", Edges));
            if (Layers.Count > 0)
            {
                foreach (var layer in Layers)
                {
                    base_node.AddNode(layer.Construct());
                }

                var layer_node = FBXSimpleNode.FromInt("Layer", 0);

                layer_node.AddNode(FBXSimpleNode.FromInt("Version", 100));

                foreach (var layer in Layers)
                {
                    var child_node = new FbxNode(new IdentifierToken("LayerElement"));

                    child_node.AddNode(FBXSimpleNode.FromString("Type", layer.Name));
                    child_node.AddNode(FBXSimpleNode.FromInt("TypedIndex", 0));
                }
            }

            return base_node;
        }
    }

    public class FBXSceneModel : FBXSceneItem
    {
        public FBXSceneModel(long ID, string Name) :
            base(ID, Name + "\x00\x01Model", "Model")
        {

        }

        public override FbxNode Construct()
        {
            var base_node = base.Construct();

            base_node.AddNode(FBXSimpleNode.FromInt("Version", 232));
            base_node.AddNode(this.Properties.Construct());
            base_node.AddNode(FBXSimpleNode.FromInt("MultiLayer", 0));
            base_node.AddNode(FBXSimpleNode.FromInt("MultiTake", 0));
            base_node.AddNode(FBXSimpleNode.FromBool("Shading", true));
            base_node.AddNode(FBXSimpleNode.FromString("Culling", "CullingOff"));

            return base_node;
        }
    }

    public class FBXSceneMaterial : FBXSceneItem
    {
        public FBXSceneMaterial(long ID, string Name) :
            base(ID, Name, "Material", "")
        {

        }

        public override FbxNode Construct()
        {
            var base_node = base.Construct();

            base_node.AddNode(FBXSimpleNode.FromInt("Version", 102));
            base_node.AddNode(FBXSimpleNode.FromString("ShadingModel", "Phong"));
            base_node.AddNode(FBXSimpleNode.FromInt("MultiLayer", 0));
            base_node.AddNode(this.Properties.Construct());

            return base_node;
        }
    }

    public class FBXLayer
    {
        public FBXLayer(string LayerType, string Name)
        {
            this.LayerType = LayerType;
            this.Name = Name;
        }

        public string LayerType { get; }
        public string Name { get; }
        public Dictionary<string, object> Values { get; } = new Dictionary<string, object>();

        public FbxNode Construct()
        {
            var base_node = new FbxNode(new IdentifierToken(LayerType));

            base_node.AddProperty(new IntegerToken(0)); //Index for type?

            base_node.AddNode(FBXSimpleNode.FromInt("Version", 101));
            base_node.AddNode(FBXSimpleNode.FromString("Name", Name));
            base_node.AddNode(FBXSimpleNode.FromString("MappingInformationType", "ByVertice"));
            base_node.AddNode(FBXSimpleNode.FromString("ReferenceInformationType", "IndexToDirect"));

            foreach (var kvp in Values)
                base_node.AddNode(FBXSimpleNode.FromArray(kvp.Key, kvp.Value));

            return base_node;
        }
    }
}
