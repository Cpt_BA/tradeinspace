﻿using UkooLabs.FbxSharpie;

namespace TradeInSpace.Export.FBX
{
    public class FBXSceneDocument : FBXSceneItem
    {
        public FBXSceneDocument(long ID) :
            base(ID, "Scene", "Document", "Scene")
        {
            Properties.Properties.Add(new FBXPropertyValue("SourceObject", "object", "", ""));
            Properties.Properties.Add(FBXPropertyValue.DirectString("ActiveAnimStackName", ""));
        }

        public override FbxNode Construct()
        {
            var base_node = base.Construct();

            base_node.AddNode(Properties.Construct());
            base_node.AddNode(FBXSimpleNode.FromLong("RootNode", 0));

            return base_node;
        }
    }
}
