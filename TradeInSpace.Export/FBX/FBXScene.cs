﻿using System;
using System.Collections.Generic;
using System.Linq;
using UkooLabs.FbxSharpie;
using UkooLabs.FbxSharpie.Tokens;
using UkooLabs.FbxSharpie.Tokens.Value;

namespace TradeInSpace.Export.FBX
{
    public class FBXScene
    {
        Dictionary<long, FBXSceneItem> Items = new Dictionary<long, FBXSceneItem>();

        public FBXScene()
        {
        }

        public void Add(FBXSceneItem Item)
        {
            if (Items.ContainsKey(Item.ID))
                throw new Exception("ID Already exists!");
            Items[Item.ID] = Item;
        }

        public FbxDocument Construct(FBXExportSettings Settings = null)
        {
            Settings = Settings ?? FBXExportSettings.DefaultSettings;

            FbxDocument document = new FbxDocument();

            void WriteDefinition<T>(string ObjectType, FbxNode IntoNode)
            {
                var count = Items.Values.OfType<T>().Count();

                if (ObjectType == "GlobalSettings")
                    count = 1;

                if (count == 0) return;

                var def_node = FBXSimpleNode.FromString("ObjectType", ObjectType);

                def_node.AddNode(FBXSimpleNode.FromInt("Count", count));
                switch (ObjectType)
                {
                    case "GlobalSettings":
                        break;
                    case "NodeAttribute":
                        def_node.AddNode(FBXSimpleNode.FromString("PropertyTemplate", "FbxNull"));
                        break;
                    case "Geometry":
                        def_node.AddNode(FBXSimpleNode.FromString("PropertyTemplate", "FbxMesh"));
                        break;
                    case "Model":
                        def_node.AddNode(FBXSimpleNode.FromString("PropertyTemplate", "FbxNode"));
                        break;
                    case "Material":
                        def_node.AddNode(FBXSimpleNode.FromString("PropertyTemplate", "FbxSurfacePhong"));
                        break;
                }

                if (ObjectType != "GlobalSettings")
                {
                    var definition_properties = new FBXProperties70();
                    def_node.AddNode(definition_properties.Construct());
                }

                IntoNode.AddNode(def_node);
            }

            FbxNode ConstructSceneInfo()
            {
                var scene_info = new FbxNode(new IdentifierToken("SceneInfo"));

                scene_info.AddProperty(new StringToken("GlobalInfo SceneInfo"));
                scene_info.AddProperty(new StringToken("UserData"));

                scene_info.AddNode(FBXSimpleNode.FromString("Type", "UserData"));
                scene_info.AddNode(FBXSimpleNode.FromInt("Version", 100));

                var metadata_node = new FbxNode(new IdentifierToken("MetaData"));
                scene_info.AddNode(metadata_node);

                //TODO: Variables!
                metadata_node.AddNode(FBXSimpleNode.FromInt("Version", 100));
                metadata_node.AddNode(FBXSimpleNode.FromString("Title", ""));
                metadata_node.AddNode(FBXSimpleNode.FromString("Subject", ""));
                metadata_node.AddNode(FBXSimpleNode.FromString("Author", ""));
                metadata_node.AddNode(FBXSimpleNode.FromString("Keywords", ""));
                metadata_node.AddNode(FBXSimpleNode.FromString("Revision", ""));
                metadata_node.AddNode(FBXSimpleNode.FromString("Comment", ""));

                var scene_properties = new FBXProperties70();
                scene_properties.Properties.Add(FBXPropertyValue.DirectString("DocumentUrl", "/foobar.fbx", "KString", "Url"));
                scene_properties.Properties.Add(FBXPropertyValue.DirectString("SrcDocumentUrl", "/foobar.fbx", "KString", "Url"));
                scene_properties.Properties.Add(new FBXPropertyValue("Original", "Compound", "", ""));
                scene_properties.Properties.Add(FBXPropertyValue.DirectString("Original|ApplicationVendor", "Blender Foundation", "KString"));
                scene_properties.Properties.Add(FBXPropertyValue.DirectString("Original|ApplicationName", "Blender (stable FBX IO)", "KString"));
                scene_properties.Properties.Add(FBXPropertyValue.DirectString("Original|ApplicationVersion", "3.0.0", "KString"));
                scene_properties.Properties.Add(FBXPropertyValue.DirectString("Original|DateTime_GMT", "01/01/1970 00:00:00.000", "DateTime"));
                scene_properties.Properties.Add(FBXPropertyValue.DirectString("Original|FileName", "/foobar.fbx", "KString"));
                scene_properties.Properties.Add(new FBXPropertyValue("LastSaved", "Compound", "", ""));
                scene_properties.Properties.Add(FBXPropertyValue.DirectString("LastSaved|ApplicationVendor", "Blender Foundation", "KString"));
                scene_properties.Properties.Add(FBXPropertyValue.DirectString("LastSaved|ApplicationName", "Blender (stable FBX IO)", "KString"));
                scene_properties.Properties.Add(FBXPropertyValue.DirectString("LastSaved|ApplicationVersion", "3.0.0", "KString"));
                scene_properties.Properties.Add(FBXPropertyValue.DirectString("LastSaved|DateTime_GMT", "01/01/1970 00:00:00.000", "DateTime"));

                scene_info.AddNode(scene_properties.Construct());

                return scene_info;
            }

            FbxNode ConstructTimestamp(string Identifier, DateTime time)
            {
                var timestamp = new FbxNode(new IdentifierToken(Identifier));
                timestamp.AddNode(FBXSimpleNode.FromInt("Version", 1000));
                timestamp.AddNode(FBXSimpleNode.FromInt("Year", time.Year));
                timestamp.AddNode(FBXSimpleNode.FromInt("Month", time.Month));
                timestamp.AddNode(FBXSimpleNode.FromInt("Day", time.Day));
                timestamp.AddNode(FBXSimpleNode.FromInt("Hour", time.Hour));
                timestamp.AddNode(FBXSimpleNode.FromInt("Minute", time.Minute));
                timestamp.AddNode(FBXSimpleNode.FromInt("Second", time.Second));
                timestamp.AddNode(FBXSimpleNode.FromInt("Millisecond", time.Millisecond));
                return timestamp;
            }

            FbxNode ConstructGlobalSettings()
            {
                var global_settings = new FbxNode(new IdentifierToken("GlobalSettings"));

                var global_properties = new FBXProperties70();
                //global_properties.Properties.Add(FBXPropertyValue.DirectString("DocumentUrl", "/foobar.fbx"));

                global_properties.Properties.Add(FBXPropertyValue.DirectInt("UpAxis", 1));
                global_properties.Properties.Add(FBXPropertyValue.DirectInt("UpAxisSign", 1));
                global_properties.Properties.Add(FBXPropertyValue.DirectInt("FrontAxis", 0));
                global_properties.Properties.Add(FBXPropertyValue.DirectInt("FrontAxisSign", 1));
                global_properties.Properties.Add(FBXPropertyValue.DirectInt("CoordAxis", 2));
                global_properties.Properties.Add(FBXPropertyValue.DirectInt("CoordAxisSign", 1));
                global_properties.Properties.Add(FBXPropertyValue.DirectInt("OriginalUpAxis", 2));
                global_properties.Properties.Add(FBXPropertyValue.DirectInt("OriginalUpAxisSign", 1));
                global_properties.Properties.Add(FBXPropertyValue.DirectDouble("UnitScaleFactor", 1.0));
                global_properties.Properties.Add(FBXPropertyValue.DirectDouble("OriginalUnitScaleFactor", 1.0));
                global_properties.Properties.Add(FBXPropertyValue.DirectVector3("AmbientColor", 0, 0, 0, "ColorRGB", "Color"));
                global_properties.Properties.Add(FBXPropertyValue.DirectString("DefaultCamera", "Producer Perspective"));
                global_properties.Properties.Add(FBXPropertyValue.DirectInt("TimeMode", 10, "enum", ""));
                global_properties.Properties.Add(FBXPropertyValue.DirectLong("TimeSpanStart", 0, "KTime", "Time"));
                global_properties.Properties.Add(FBXPropertyValue.DirectLong("TimeSpanStop", 46186158000, "KTime", "Time"));
                global_properties.Properties.Add(FBXPropertyValue.DirectDouble("CustomFrameRate", 25));

                global_settings.AddNode(FBXSimpleNode.FromInt("Version", 1000));
                global_settings.AddNode(global_properties.Construct());

                return global_settings;
            }

            FbxNode ConstructConnection(long FromID, long ToID)
            {
                var connection_obj = FBXSimpleNode.FromString("C", "OO");
                connection_obj.AddProperty(new LongToken(ToID));
                connection_obj.AddProperty(new LongToken(FromID));
                return connection_obj;
            }

            IEnumerable<FbxNode> ConstructConnections(FBXSceneItem Item)
            {
                foreach (var child in Item.Children)
                {
                    yield return ConstructConnection(Item.ID, child.ID);

                    foreach (var child_connection in ConstructConnections(child))
                    {
                        yield return child_connection;
                    }
                }
            }

            var create_time = DateTime.Now;

            var header = new FbxNode(new IdentifierToken("FBXHeaderExtension"));

            header.AddNode(FBXSimpleNode.FromInt("FBXHeaderVersion", 1003));
            header.AddNode(FBXSimpleNode.FromInt("FBXVersion", 7400));
            header.AddNode(FBXSimpleNode.FromInt("EncryptionType", 0));
            header.AddNode(ConstructTimestamp("CreationTimeStamp", create_time));
            header.AddNode(FBXSimpleNode.FromString("Creator", "TIS FBX Exporter".PadRight(40, '_')));
            header.AddNode(ConstructSceneInfo());


            document.AddNode(header);
            document.AddNode(FBXSimpleNode.FromRaw("FileId", new byte[] { (byte)'(', 0xb3, (byte)'*', 0xeb, 0xb6, (byte)'$', 0xcc, 0xc2, 0xbf, 0xc8, 0xb0, (byte)'*', 0xa9, (byte)'+', 0xfc, 0xf1 }));
            document.AddNode(FBXSimpleNode.FromString("CreationTime", create_time.ToString("yyyy-MM-dd HH:mm:ss:fff")));
            document.AddNode(FBXSimpleNode.FromString("Creator", "TIS FBX Exporter".PadRight(40, '_')));

            document.AddNode(ConstructGlobalSettings());

            var sceneDocs = new FbxNode(new IdentifierToken("Documents"));
            sceneDocs.AddNode(FBXSimpleNode.FromInt("Count", 1));
            sceneDocs.AddNode(new FBXSceneDocument(Settings.MakeElementID()).Construct());
            document.AddNode(sceneDocs);

            var references = new FbxNode(new IdentifierToken("References"));
            references.AddNode(null);
            document.AddNode(references);

            var definition_node = new FbxNode(new IdentifierToken("Definitions"));
            definition_node.AddNode(FBXSimpleNode.FromInt("Version", 100));
            definition_node.AddNode(FBXSimpleNode.FromInt("Count", Items.Count + 1)); //Account for pseudo-entry
            WriteDefinition<string>("GlobalSettings", definition_node); //Hack to write psuedo-entry for GlobalSettings
            WriteDefinition<FBXSceneNodeAttribute>("NodeAttribute", definition_node);
            WriteDefinition<FBXSceneGeometry>("Geometry", definition_node);
            WriteDefinition<FBXSceneModel>("Model", definition_node);
            WriteDefinition<FBXSceneMaterial>("Material", definition_node);

            document.AddNode(definition_node);

            var objectsNode = new FbxNode(new IdentifierToken("Objects"));

            foreach (var item in Items.Values.OfType<FBXSceneNodeAttribute>()) objectsNode.AddNode(item.Construct());
            foreach (var item in Items.Values.OfType<FBXSceneGeometry>()) objectsNode.AddNode(item.Construct());
            foreach (var item in Items.Values.OfType<FBXSceneModel>()) objectsNode.AddNode(item.Construct());
            foreach (var item in Items.Values.OfType<FBXSceneMaterial>()) objectsNode.AddNode(item.Construct());

            document.AddNode(objectsNode);

            var connection_node = new FbxNode(new IdentifierToken("Connections"));

            //Go over every object in the scene, and keey any ID's that show up as a CHILD
            var child_ids = Items.Values.SelectMany(i => i.Children.Select(c => c.ID)).Distinct().ToList();
            //Anything that isn't in that list, is a root object
            var root_items = Items.Values.Where(i => !child_ids.Contains(i.ID));

            foreach (var root_item in root_items)
            {
                connection_node.AddNode(ConstructConnection(0, root_item.ID));

                foreach (var connection in ConstructConnections(root_item))
                {
                    connection_node.AddNode(connection);
                }
            }
            document.AddNode(connection_node);

            //Takes

            return document;
        }
    }
}
