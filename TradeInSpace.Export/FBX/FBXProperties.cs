﻿using System;
using System.Collections.Generic;
using UkooLabs.FbxSharpie;
using UkooLabs.FbxSharpie.Tokens;
using UkooLabs.FbxSharpie.Tokens.Value;
using UkooLabs.FbxSharpie.Tokens.ValueArray;

namespace TradeInSpace.Export.FBX
{
    public class FBXProperties70
    {
        public List<FBXPropertyValue> Properties { get; set; } = new List<FBXPropertyValue>();

        public FBXProperties70()
        {
        }

        public void AddInteger(FBXPropertyValue.Property property, int value)
        {
            Properties.Add(FBXPropertyValue.Integer(property, value));
        }

        public void AddVector3(FBXPropertyValue.Property property, double X, double Y, double Z)
        {
            Properties.Add(FBXPropertyValue.Vector3(property, X, Y, Z));
        }

        public void AddVector4(FBXPropertyValue.Property property, double X, double Y, double Z, double W)
        {
            Properties.Add(FBXPropertyValue.Vector4(property, X, Y, Z, W));
        }

        public void AddString(FBXPropertyValue.Property property, string value)
        {
            Properties.Add(FBXPropertyValue.String(property, value));
        }

        internal void AddBool(FBXPropertyValue.Property property, bool value)
        {
            Properties.Add(FBXPropertyValue.Bool(property, value));
        }

        public FbxNode Construct()
        {
            var node = new FbxNode(new IdentifierToken("Properties70"));

            foreach (var property in Properties)
                node.AddNode(property.Construct());

            return node;
        }
    }

    public class FBXSimpleNode
    {
        public static FbxNode FromString(string Name, string Value)
        {
            var node = new FbxNode(new IdentifierToken(Name));
            node.AddProperty(new StringToken(Value));
            return node;
        }
        public static FbxNode FromInt(string Name, int Value)
        {
            var node = new FbxNode(new IdentifierToken(Name));
            node.AddProperty(new IntegerToken(Value));
            return node;
        }
        internal static FbxNode FromLong(string Name, long Value)
        {
            var node = new FbxNode(new IdentifierToken(Name));
            node.AddProperty(new LongToken(Value));
            return node;
        }
        public static FbxNode FromBool(string Name, bool Value)
        {
            var node = new FbxNode(new IdentifierToken(Name));
            node.AddProperty(new BooleanToken(Value));
            return node;
        }
        public static FbxNode FromRaw(string Name, byte[] Value)
        {
            var node = new FbxNode(new IdentifierToken(Name));
            node.AddProperty(new ByteArrayToken(Value));
            return node;
        }
        public static FbxNode FromArray(string Name, object Value)
        {
            var array_node = new FbxNode(new IdentifierToken(Name));

            switch (Value)
            {
                case byte[] byteValues:
                    array_node.AddProperty(new ByteArrayToken(byteValues));
                    break;
                case int[] intValues:
                    array_node.AddProperty(new IntegerArrayToken(intValues));
                    break;
                case long[] longValues:
                    array_node.AddProperty(new LongArrayToken(longValues));
                    break;
                case bool[] boolValues:
                    array_node.AddProperty(new BooleanArrayToken(boolValues));
                    break;
                case float[] floatValues:
                    array_node.AddProperty(new FloatArrayToken(floatValues));
                    break;
                case double[] doubleValues:
                    array_node.AddProperty(new DoubleArrayToken(doubleValues));
                    break;
                default:
                    throw new Exception("Unknown array type");
                    break;

            }
            return array_node;
        }
    }

    public class FBXPropertyValue
    {
        public Token[] Values { get; set; } = new Token[0];

        private PropertyTypeDetails Details { get; set; }

        public FBXPropertyValue(Property Property)
        {
            Details = Properties[Property];
        }
        public FBXPropertyValue(PropertyTypeDetails PropertyDetails)
        {
            Details = PropertyDetails;
        }
        public FBXPropertyValue(string Name, string Type, string Unknown1, string Unknown2)
        {
            Details = new PropertyTypeDetails(Name, Type, Unknown1, Unknown2);
        }

        public static FBXPropertyValue Integer(Property Property, int Value)
        {
            return new FBXPropertyValue(Property)
            {
                Values = new[]
                {
                    new IntegerToken(Value)
                }
            };
        }

        public static FBXPropertyValue Vector3(Property Property, double X, double Y, double Z)
        {
            return new FBXPropertyValue(Property)
            {
                Values = new[]
                {
                    new DoubleToken(X),
                    new DoubleToken(Y),
                    new DoubleToken(Z)
                }
            };
        }

        public static FBXPropertyValue Vector4(Property Property, double X, double Y, double Z, double W)
        {
            return new FBXPropertyValue(Property)
            {
                Values = new[]
                {
                    new DoubleToken(X),
                    new DoubleToken(Y),
                    new DoubleToken(Z),
                    new DoubleToken(W)
                }
            };
        }

        internal static FBXPropertyValue String(Property property, string value)
        {
            return new FBXPropertyValue(property)
            {
                Values = new[]
                {
                    new StringToken(value)
                }
            };
        }

        internal static FBXPropertyValue Bool(Property property, bool value)
        {
            return new FBXPropertyValue(property)
            {
                Values = new[]
                {
                    new BooleanToken(value)
                }
            };
        }

        public static FBXPropertyValue DirectVector3(string Name, double V1, double V2, double V3, string Type = "Vector3", string Unknown1 = "", string Unknonw2 = "")
        {
            return new FBXPropertyValue(Name, Type, Unknown1, Unknonw2)
            {
                Values = new[]
                {
                    new DoubleToken(V1),
                    new DoubleToken(V2),
                    new DoubleToken(V3)
                }
            };
        }

        public static FBXPropertyValue DirectString(string Name, string value, string Type = "kString", string Unknown1 = "", string Unknown2 = "")
        {
            return new FBXPropertyValue(Name, Type, Unknown1, Unknown2)
            {
                Values = new[]
                {
                    new StringToken(value)
                }
            };
        }

        public static FBXPropertyValue DirectInt(string Name, int value, string Type = "int", string Unknown1 = "Integer", string Unknown2 = "")
        {
            return new FBXPropertyValue(Name, Type, Unknown1, Unknown2)
            {
                Values = new[]
                {
                    new IntegerToken(value)
                }
            };
        }

        public static FBXPropertyValue DirectLong(string Name, long value, string Type = "int", string Unknown1 = "Integer", string Unknown2 = "")
        {
            return new FBXPropertyValue(Name, Type, Unknown1, Unknown2)
            {
                Values = new[]
                {
                    new LongToken(value)
                }
            };
        }

        public static FBXPropertyValue DirectDouble(string Name, double value, string Type = "double", string Unknown1 = "Number", string Unknown2 = "")
        {
            return new FBXPropertyValue(Name, Type, Unknown1, Unknown2)
            {
                Values = new[]
                {
                    new DoubleToken(value)
                }
            };
        }

        public FbxNode Construct()
        {
            FbxNode node = new FbxNode(new IdentifierToken("P"));

            node.AddProperty(new StringToken(Details.Name));
            node.AddProperty(new StringToken(Details.Type));
            node.AddProperty(new StringToken(Details.Unknown1));
            node.AddProperty(new StringToken(Details.Unknown2));

            foreach (var value in Values)
                node.AddProperty(value);

            return node;
        }

        static readonly Dictionary<Property, PropertyTypeDetails> Properties
            = new Dictionary<Property, PropertyTypeDetails>()
        {
            { Property.LCL_Translation, new PropertyTypeDetails("Lcl Translation", "Lcl Translation", "", "A") },
            { Property.LCL_Rotation, new PropertyTypeDetails("Lcl Rotation", "Lcl Rotation", "", "A") },
            { Property.LCL_Scale, new PropertyTypeDetails("Lcl Scaling", "Lcl Scaling", "", "A") },
            { Property.DefaultAttributeIndex, new PropertyTypeDetails("DefaultAttributeIndex", "int", "Integer", "" ) },
            { Property.RotationOrder, new PropertyTypeDetails("RotationOrder", "enum", "", "") },
            { Property.RotationActive, new PropertyTypeDetails("RotationActive", "bool", "", "") },
            { Property.InheritType, new PropertyTypeDetails("InheritType", "enum", "", "") }
        };

        public enum Property
        {
            LCL_Translation,
            LCL_Rotation,
            LCL_Scale,
            DefaultAttributeIndex,
            InheritType,
            DiffuseColor,
            AmbientColor,
            AmbientFactor,
            BumpFactor,
            SpecularColor,
            SpecularFactor,
            Shininess,
            ShininessExponent,
            ReflectionFactor,
            RotationOrder,
            RotationActive
        }

        public class PropertyTypeDetails
        {
            public PropertyTypeDetails(string name, string type, string unknown1, string unknown2)
            {
                Name = name;
                Type = type;
                Unknown1 = unknown1;
                Unknown2 = unknown2;
            }

            public string Name { get; set; }
            public string Type { get; set; }
            public string Unknown1 { get; set; }
            public string Unknown2 { get; set; }
        }
    }
}
