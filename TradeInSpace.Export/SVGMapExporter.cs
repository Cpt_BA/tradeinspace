﻿using CgfConverter;
using CgfConverter.CryEngineCore;
using Serilog;
using Svg;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using TradeInSpace.Explore;
using TradeInSpace.Explore.DataForge;
using TradeInSpace.Explore.Socpak;
using TradeInSpace.Explore.Subsumption;
using TradeInSpace.Explore.Util;

namespace TradeInSpace.Export
{
    using DN = System.DoubleNumerics;

    public class SVGMapExporter
    {
        public static string GenerateSVG(DFDatabase gameData, SCHieracrhyNode node, 
            Func<Vector3, double> dimA, Func<Vector3, double> dimB, Func<Vector3, double> dimRot,
            bool IncludeEntities = false, bool FlipY = false)
        {
            var svgDoc = new SvgDocument
            {
                Width = 2000,
                Height = 2000,
                ViewBox = new SvgViewBox(0, 0, 1000, 1000),
            };

            var spaceBounds = new AARect3()
            {
                Min = new Vector3(-75, -75, -75),
                Max = new Vector3(75, 75, 75)
            };
            spaceBounds = DetermineBounds(node);
            var vb = svgDoc.ViewBox;

            Log.Information($"Drawing SVG with real-world (meters) bounds of: [{spaceBounds.Min}] - [{spaceBounds.Max}]");

            DrawGrid(svgDoc);//, GridSize: 1, GridWidth: 0.05f);

            var ModelSizes = new Dictionary<string, (Vector3, Vector3)>();

            void CheckBoundAxis(Func<Vector3, double> axis, Func<double, Vector3> newVec, double minValue = 500)
            {
                double size = axis(spaceBounds.Max) - axis(spaceBounds.Min);
                if(size < minValue)
                {
                    double halfDiff = (minValue - size) / 2.0f;
                    spaceBounds.AddPoint(spaceBounds.Min - newVec(halfDiff));
                    spaceBounds.AddPoint(spaceBounds.Max + newVec(halfDiff));
                }
            }

            (double X, double Y) RemapCoord(Vector3 globalPosition, bool aspectCorrection = true)
            {
                double correctionA = 1, correctionB = 1;

                if (aspectCorrection)
                {
                    var size = (spaceBounds.Max - spaceBounds.Min);
                    correctionA = (dimB(size) > dimA(size)) ? dimB(size) / dimA(size) : 1;
                    correctionB = (dimA(size) > dimB(size)) ? dimA(size) / dimB(size) : 1;
                }

                return (
                    correctionA * Remap(dimA(globalPosition), dimA(spaceBounds.Min), dimA(spaceBounds.Max), vb.MinX, vb.MinX + vb.Width),
                    correctionB * Remap(dimB(globalPosition), dimB(spaceBounds.Min), dimB(spaceBounds.Max), vb.MinY, vb.MinY + vb.Height, Invert: FlipY));
            }

            ModelCache models = new ModelCache(gameData);

            //CheckBoundAxis(v => v.X, x => new DN.Vector3(x, 0, 0), minValue: 5);
            //CheckBoundAxis(v => v.Y, y => new DN.Vector3(0, y, 0), minValue: 5);

            Dictionary<string, List<List<Vector3>>> loadedModels = new Dictionary<string, List<List<Vector3>>>();

            SvgElement fnDrawContainer(SCHieracrhyNode drawNode)
            {
                var group = new SvgGroup()
                {
                    ID = Utilities.CleanKey(drawNode.Name)
                };

                Log.Verbose($"Drawing Container - {drawNode.Name}");

                foreach(var child in drawNode.Children)
                {
                    //var childPos = RemapCoord(child.GlobalPosition);

                    var vertexes = child.GeometryData?.Vertexes;

                    if (vertexes != null)
                    {
                        if (vertexes.Count < 4) continue;

                        var projectedPoints = vertexes
                            .Select(v => new Vector3(v.Item1, v.Item2, v.Item3))
                            .Select(child.TranfomPoint)
                            .Select(pv => new Vector2(dimA(pv), dimB(pv)))
                            .ToList();

                        var modelHull = ModelCache.GetConvexHull(projectedPoints);

                        var min = (modelHull.Min(v2 => v2.X), modelHull.Min(v2 => v2.Y));
                        var max = (modelHull.Max(v2 => v2.X), modelHull.Max(v2 => v2.Y));

                        Log.Verbose($"Drawing model chunk {child.Name} with {projectedPoints.Count} points. Range: {min}, {max}");

                        var pts = new SvgPointCollection();
                        foreach (var vert in modelHull)
                        {
                            var pt = RemapCoord(new Vector3(vert.X, vert.Y, 0));
                            pts.Add((float)pt.X);
                            pts.Add((float)pt.Y);
                            //pts.Add((float)Remap(vert.X, dimA(spaceBounds.Min), dimA(spaceBounds.Max), vb.MinX, vb.MinX + vb.Width));
                            //pts.Add((float)Remap(vert.Y, dimB(spaceBounds.Min), dimB(spaceBounds.Max), vb.MinY, vb.MinY + vb.Height));
                        }

                        group.Children.Add(new SvgPolygon()
                        {
                            //ID = Utilities.CleanKey(drawNode.Name),
                            Points = pts,
                            Fill = new SvgColourServer(Color.Transparent),
                            Stroke = new SvgColourServer(Color.Red),
                            StrokeWidth = 0.1f,
                            //Transforms = tranform
                        });
                    }

                    /*
                    if (!string.IsNullOrEmpty(child.ModelPath))
                    {
                        var modelPath = child.ModelPath.ToLower();

                        var modelDetails = models[modelPath];

                        var lowerExtent = child.GlobalPosition + modelDetails.LowerBounds;
                        var upperExtent = child.GlobalPosition + modelDetails.UpperBounds;

                        Log.Verbose($"Model - {child.Name} [{modelPath}] - World: {child.GlobalPosition} - Lower: {lowerExtent}\t Upper: {upperExtent}");

                        if (!loadedModels.ContainsKey(modelPath))
                        {
                            var model = gameData.ReadPackedCGAFile(modelPath);
                            try
                            {
                                model.ProcessCryengineFiles();

                                var modelVerts = models.GetAllModelVerticies(model);
                                loadedModels[modelPath] = modelVerts;
                            }
                            catch (Exception ex)
                            {
                                Log.Error(ex, $"Error drawing mode: {child.ModelPath}");
                                loadedModels[modelPath] = new List<List<Vector3>>();
                            }
                            //var alphaShape = AlphaShape.ComputeAlphaShape(modelVerts.Select(v => new Vector2(dimA(v), dimB(v))).ToList(), 1f);
                        }

                        foreach (var chunk in loadedModels[modelPath])
                        {
                            if (chunk.Count < 4) continue;

                            var projectedPoints = chunk
                                .Select(child.TranfomPoint)
                                .Select(pv => new Vector2(dimA(pv), dimB(pv)))
                                .ToList();

                            var modelHull = ModelCache.GetConvexHull(projectedPoints);

                            var pts = new SvgPointCollection();
                            foreach (var vert in modelHull)
                            {
                                pts.Add((float)Remap(vert.X, dimA(spaceBounds.Min), dimA(spaceBounds.Max), vb.MinX, vb.MinX + vb.Width));
                                pts.Add((float)Remap(vert.Y, dimB(spaceBounds.Min), dimB(spaceBounds.Max), vb.MinY, vb.MinY + vb.Height));
                            }

                            group.Children.Add(new SvgPolygon()
                            {
                                //ID = Utilities.CleanKey(drawNode.Name),
                                Points = pts,
                                Fill = new SvgColourServer(Color.Transparent),
                                Stroke = new SvgColourServer(Color.Black),
                                StrokeWidth = 0.1f,
                                //Transforms = tranform
                            });
                        }
                    }
                    else
                    */
                    {
                        //Log.Verbose($"Entity - {child.Name} - World: {child.GlobalPosition} - Screen: {childPos}");

                        /*
                        group.Children.Add(new SvgCircle()
                        {
                            //ID = Utilities.CleanKey(drawNode.Name),
                            CenterX = (float)childPos.X,
                            CenterY = (float)childPos.Y,
                            Radius = 0.1f,
                            Fill = new SvgColourServer(Color.Transparent),
                            Stroke = new SvgColourServer(Color.Black),
                            StrokeWidth = 0.1f,
                            //Transforms = tranform
                        });
                        */
                    }

                    if(child.Children?.Count > 0)
                    {
                        group.Children.Add(fnDrawContainer(child));
                    }
                }

                return group;
            }

            svgDoc.Children.Add(fnDrawContainer(node));

            using (var stream = new MemoryStream())
            {
                using (var reader = new StreamReader(stream))
                {
                    svgDoc.Write(stream);
                    stream.Seek(0, SeekOrigin.Begin);
                    return reader.ReadToEnd();
                }
            }
        }

        private static AARect3 DetermineBounds(SCHieracrhyNode node)
        {
            AARect3 bounds = new AARect3();

            void _SearchBounds(SCHieracrhyNode search_node)
            {
                if(search_node.NodeType == SCHieracrhyNodeType.Geometry || 
                    search_node.NodeType == SCHieracrhyNodeType.Entity)
                    if(search_node.GlobalPosition != Vector3.Zero)
                        bounds.AddPoint(search_node.GlobalPosition);

                foreach (var child in search_node.Children)
                    _SearchBounds(child);
            }

            _SearchBounds(node);

            bounds.Ensure();

            return bounds;
        }

        private static void DrawGrid(SvgDocument svgDoc, float GridSize = 100.0f, float GridWidth = 1.0f)
        {
            SvgGroup gridGroup = new SvgGroup();
            var vb = svgDoc.ViewBox;

            for (float x = vb.MinX + GridSize; x < vb.MinX + vb.Width; x += GridSize)
            {
                gridGroup.Children.Add(new SvgLine()
                {
                    StartX = x,
                    StartY = 0,
                    EndX = x,
                    EndY = vb.MinY + vb.Height,
                    Stroke = new SvgColourServer(Color.Gray)
                });
            }

            for (float y = vb.MinY + GridSize; y < vb.MinY + vb.Height; y += GridSize)
            {
                gridGroup.Children.Add(new SvgLine()
                {
                    StartX = vb.MinX,
                    StartY = y,
                    EndX = vb.MinX + vb.Width,
                    EndY = y,
                    Stroke = new SvgColourServer(Color.Gray),
                    StrokeWidth = GridWidth
                });
            }

            var XAxis = vb.MinX + vb.Width / 2;
            var YAxis = vb.MinY + vb.Height / 2;

            gridGroup.Children.Add(new SvgLine()
            {
                StartX = XAxis,
                StartY = vb.MinY,
                EndX = XAxis,
                EndY = vb.MinY + vb.Height,
                Stroke = new SvgColourServer(Color.Black),
                StrokeWidth = GridWidth * 2
            });

            gridGroup.Children.Add(new SvgLine()
            {
                StartX = vb.MinX,
                StartY = YAxis,
                EndX = vb.MinX + vb.Width,
                EndY = YAxis,
                Stroke = new SvgColourServer(Color.Black),
                StrokeWidth = GridWidth * 2
            });

            svgDoc.Children.Add(gridGroup);
        }

        static double Remap(double ValueIn, double InMin, double InMax, double OutMin, double OutMax, bool Invert = false)
        {
            var normalized = (ValueIn - InMin) / (InMax - InMin);
            return ((Invert ? 1 - normalized : normalized) * (OutMax - OutMin)) + OutMin;
        }
    }
}
