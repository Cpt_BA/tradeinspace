﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.IO.Compression;
using System.Diagnostics;

namespace TradeInSpace.Export.Ignore
{
    public class FBXDocument
    {
        internal static readonly string HEADER_STRING = "Kaydara FBX Binary  ";

        public List<FBXNode> Nodes { get; set; } = new List<FBXNode>();
        public int Version { get; set; } = 7400;

        public static FBXDocument ReadFile(string file)
        {
            using (var reader = File.OpenRead(file))
                return ReadStream(reader);
        }

        public static FBXDocument ReadStream(Stream stream)
        {
            using (var reader = new BinaryReader(stream))
                return ReadDocument(reader);
        }

        public static FBXDocument ReadDocument(BinaryReader reader)
        {
            var header_text = Encoding.UTF8.GetString(reader.ReadBytes(HEADER_STRING.Length));
            if (header_text != HEADER_STRING)
                throw new Exception("Invalid header string");

            var nullbyte = reader.ReadByte(); //0x00
            var unknown = reader.ReadInt16(); //0x1A00
            var version = reader.ReadInt32();

            if (version != 7400)
                throw new Exception($"Unknown version {version}");

            var document = new FBXDocument()
            {
                Version = version
            };


            while(true)
            {
                var node = FBXNode.ReadNode(reader);

                if (node != null)
                    document.Nodes.Add(node);
                else
                    break;
            }

            var footerCode = reader.ReadBytes(footerCodeSize);
            //TODO: Validate?
            //TODO: Other reading

            return document;
        }

        public void WriteFile(string file)
        {
            using (var writer = File.OpenWrite(file))
                WriteStream(writer);
        }

        public void WriteStream(Stream stream)
        {
            using (var writer = new BinaryWriter(stream))
                WriteDocument(writer);
        }

        public void WriteDocument(BinaryWriter writer)
        {
            writer.Write(Encoding.UTF8.GetBytes(HEADER_STRING));
            writer.Write((byte)0);
            writer.Write((short)0x001A);
            writer.Write(Version);
            foreach(var node in Nodes)
            {
                node.WriteNode(writer);
            }
            //NULL Record (13x 0-bytes)
            writer.Write((int)0);
            writer.Write((int)0);
            writer.Write((int)0);
            writer.Write((byte)0);

            writer.Write(GenerateFooterCode(0, 0, 0, 0, 0, 0, 0));

            for (int i = 0; i < 20; i++)
                writer.Write(0);

            writer.Write((int)Version);

            for (int i = 0; i < 120; i++)
                writer.Write(0);

            writer.Write(extension);
        }

        //https://github.com/hamish-milne/FbxWriter/blob/53d9479af87639307b7443bae70ca0c8cc58e48d/Fbx/FbxBinary.cs
        protected const int footerCodeSize = 16;
        private static readonly byte[] sourceId =
            { 0x58, 0xAB, 0xA9, 0xF0, 0x6C, 0xA2, 0xD8, 0x3F, 0x4D, 0x47, 0x49, 0xA3, 0xB4, 0xB2, 0xE7, 0x3D };
        private static readonly byte[] key =
            { 0xE2, 0x4F, 0x7B, 0x5F, 0xCD, 0xE4, 0xC8, 0x6D, 0xDB, 0xD8, 0xFB, 0xD7, 0x40, 0x58, 0xC6, 0x78 };
        private static readonly byte[] extension =
            { 0xF8, 0x5A, 0x8C, 0x6A, 0xDE, 0xF5, 0xD9, 0x7E, 0xEC, 0xE9, 0x0C, 0xE3, 0x75, 0x8F, 0x29, 0x0B };

        // Turns out this is the algorithm they use to generate the footer. Who knew!
        static void Encrypt(byte[] a, byte[] b)
        {
            byte c = 64;
            for (int i = 0; i < footerCodeSize; i++)
            {
                a[i] = (byte)(a[i] ^ (byte)(c ^ b[i]));
                c = a[i];
            }
        }
        protected static byte[] GenerateFooterCode(
            int year, int month, int day,
            int hour, int minute, int second, int millisecond)
        {
            if (year < 0 || year > 9999)
                throw new ArgumentOutOfRangeException(nameof(year));
            if (month < 0 || month > 12)
                throw new ArgumentOutOfRangeException(nameof(month));
            if (day < 0 || day > 31)
                throw new ArgumentOutOfRangeException(nameof(day));
            if (hour < 0 || hour >= 24)
                throw new ArgumentOutOfRangeException(nameof(hour));
            if (minute < 0 || minute >= 60)
                throw new ArgumentOutOfRangeException(nameof(minute));
            if (second < 0 || second >= 60)
                throw new ArgumentOutOfRangeException(nameof(second));
            if (millisecond < 0 || millisecond >= 1000)
                throw new ArgumentOutOfRangeException(nameof(millisecond));

            var str = (byte[])sourceId.Clone();
            var mangledTime = $"{second:00}{month:00}{hour:00}{day:00}{(millisecond / 10):00}{year:0000}{minute:00}";
            var mangledBytes = Encoding.ASCII.GetBytes(mangledTime);
            Encrypt(str, mangledBytes);
            Encrypt(str, key);
            Encrypt(str, mangledBytes);
            return str;
        }
    }

    [DebuggerDisplay("{" + nameof(GetDebuggerDisplay) + "(),nq}")]
    public class FBXNode
    {
        public FBXNode(string name)
        {
            Name = name;
        }

        public string Name { get; set; }

        public List<FBXNode> Children { get; set; } = new List<FBXNode>();
        public List<FBXProperty> Properties { get; set; } = new List<FBXProperty>();

        internal static FBXNode ReadNode(BinaryReader reader)
        {
            var start_offset = reader.BaseStream.Position;
            var end_offset = reader.ReadInt32();
            var num_properties = reader.ReadInt32();
            var propert_list_length = reader.ReadInt32();

            var name_length = reader.ReadByte();
            var name = Encoding.UTF8.GetString(reader.ReadBytes(name_length));

            if(end_offset == 0 && num_properties == 0 && propert_list_length == 0 && name_length == 0)
            {
                //end record
                return null;
            }

            var property_end = reader.BaseStream.Position + propert_list_length;

            var node = new FBXNode(name);

            for(int i = 0; i < num_properties; i++)
            {
                node.Properties.Add(FBXProperty.ReadProperty(reader));
            }

            //Make sure we are at the right place
            reader.BaseStream.Position = property_end;

            if(property_end < end_offset)
            {
                while(reader.BaseStream.Position < end_offset)
                {
                    var child_node = ReadNode(reader);

                    //Additional of possible null element here intentional
                    //To preserve documents that have empty child sets

                    node.Children.Add(child_node);
                }
            }

            //Again, make sure we line up correctly
            reader.BaseStream.Seek(end_offset, SeekOrigin.Begin);

            return node;
        }

        public void WriteNode(BinaryWriter writer)
        {
            var start_position = writer.BaseStream.Position;

            // Placeholder for end_position Offset: 0
            writer.Write((int)0xAAAA);

            //Property count Offset: 4
            writer.Write((int)Properties.Count);

            // Placeholder for property list length (bytes) Offset: 8
            writer.Write((int)0xAAAA); 

            var name_bytes = Encoding.UTF8.GetBytes(Name);
            //Name Length Offset: 12
            writer.Write((byte)name_bytes.Length);
            //Name Offset: 13
            writer.Write(name_bytes);

            var property_start = writer.BaseStream.Position;

            foreach (var property in Properties)
                property.WriteProperty(writer);

            var property_length = writer.BaseStream.Position - property_start;

            if (Children.Count > 0)
            {
                foreach (var child in Children)
                    if(child != null)
                        child.WriteNode(writer);

                //NULL Record (13x 0-bytes)
                writer.Write((int)0);
                writer.Write((int)0);
                writer.Write((int)0);
                writer.Write((byte)0);
            }

            var end_position = writer.BaseStream.Position;

            //Write back over Property List Length at Offset: 0
            writer.BaseStream.Seek(start_position, SeekOrigin.Begin);
            writer.Write((int)end_position);

            //Write back over Property List Length at Offset: 8
            writer.BaseStream.Seek(start_position + 8, SeekOrigin.Begin);
            writer.Write((int)property_length);

            writer.BaseStream.Seek(end_position, SeekOrigin.Begin);
        }

        private string GetDebuggerDisplay()
        {
            return $"{Name} - [P: {Properties.Count}, C: {Children.Count}]";
        }
    }

    [DebuggerDisplay("{" + nameof(GetDebuggerDisplay) + "(),nq}")]
    public class FBXProperty
    {
        public FBXProperty(FBXPropertyType type, object value)
        {
            Type = type;
            Value = value;
        }

        public FBXPropertyType Type { get; set; }
        public object Value { get; set; }

        internal static FBXProperty ReadProperty(BinaryReader reader)
        {
            var type = (FBXPropertyType)reader.ReadByte();
            object value = null;

            switch (type)
            {
                case FBXPropertyType.Boolean:
                    value = reader.ReadBoolean();
                    break;
                case FBXPropertyType.Integer16:
                    value = reader.ReadInt16();
                    break;
                case FBXPropertyType.Integer32:
                    value = reader.ReadInt32();
                    break;
                case FBXPropertyType.Integer64:
                    value = reader.ReadInt64();
                    break;
                case FBXPropertyType.Float:
                    value = reader.ReadSingle();
                    break;
                case FBXPropertyType.Double:
                    value = reader.ReadDouble();
                    break;

                case FBXPropertyType.ArrayBoolean:
                case FBXPropertyType.ArrayInteger16:
                case FBXPropertyType.ArrayInteger32:
                case FBXPropertyType.ArrayInteger64:
                case FBXPropertyType.ArrayFloat:
                case FBXPropertyType.ArrayDouble:
                    value = ReadArray(reader, type);
                    break;

                case FBXPropertyType.String:
                case FBXPropertyType.Raw:
                    var length = reader.ReadInt32();
                    var raw_data = reader.ReadBytes(length);

                    if(type == FBXPropertyType.String)
                        value = Encoding.UTF8.GetString(raw_data);
                    else
                        value = raw_data;
                    break;
                default:
                    break;
            }

            return new FBXProperty(type, value);
        }

        internal void WriteProperty(BinaryWriter writer)
        {
            writer.Write((byte)Type);
            switch (Type)
            {
                case FBXPropertyType.Boolean:
                    writer.Write((bool)Value);
                    break;
                case FBXPropertyType.Integer16:
                    writer.Write((short)Value);
                    break;
                case FBXPropertyType.Integer32:
                    writer.Write((int)Value);
                    break;
                case FBXPropertyType.Integer64:
                    writer.Write((long)Value);
                    break;
                case FBXPropertyType.Float:
                    writer.Write((float)Value);
                    break;
                case FBXPropertyType.Double:
                    writer.Write((double)Value);
                    break;

                case FBXPropertyType.ArrayBoolean:
                case FBXPropertyType.ArrayInteger16:
                case FBXPropertyType.ArrayInteger32:
                case FBXPropertyType.ArrayInteger64:
                case FBXPropertyType.ArrayFloat:
                case FBXPropertyType.ArrayDouble:
                    WriteArray(writer, Type);
                    break;

                case FBXPropertyType.String:
                    var text = Value as string;
                    var text_bytes = Encoding.UTF8.GetBytes(text);
                    writer.Write((int)text_bytes.Length);
                    writer.Write(text_bytes);
                    break;
                case FBXPropertyType.Raw:
                    byte[] data = Value as byte[];
                    writer.Write((int)data.Length);
                    writer.Write(data);
                    break;
            }
        }

        private void WriteArray(BinaryWriter writer, FBXPropertyType fBXPropertyType)
        {
            var values = Value as object[];

            if (values == null)
                throw new Exception("Invaild data!");

            byte[] compressed_data;

            using (var outputBytes = new MemoryStream())
            {
                using(var compressedWriter = new BinaryWriter(outputBytes))
                {
                    foreach (object item in values)
                    {
                        switch (fBXPropertyType)
                        {
                            case FBXPropertyType.ArrayBoolean:
                                compressedWriter.Write((bool)item);
                                break;
                            case FBXPropertyType.ArrayInteger16:
                                compressedWriter.Write((short)item);
                                break;
                            case FBXPropertyType.ArrayInteger32:
                                compressedWriter.Write((int)item);
                                break;
                            case FBXPropertyType.ArrayInteger64:
                                compressedWriter.Write((long)item);
                                break;
                            case FBXPropertyType.ArrayFloat:
                                compressedWriter.Write((float)item);
                                break;
                            case FBXPropertyType.ArrayDouble:
                                compressedWriter.Write((double)item);
                                break;
                            default:
                                break;
                        }
                    }

                    compressedWriter.Flush();
                    compressedWriter.Close();
                }

                compressed_data = Deflate(outputBytes.ToArray());
            }

            writer.Write((int)values.Length);
            writer.Write((int)1);

            var lengthPos = writer.BaseStream.Position;
            writer.Write((int)0xCCCC); //Placeholder

            var dataStart = writer.BaseStream.Position;
            //ZLib header
            //writer.Write(new byte[] { 0x78, 0x9C }, 0, 2);
            writer.Write(compressed_data);
            //writer.Write((int)0xCCCC); //TODO: Checksum

            var endPos = writer.BaseStream.Position;

            writer.BaseStream.Seek(lengthPos, SeekOrigin.Begin);
            writer.Write((int)(endPos - dataStart));
            writer.BaseStream.Seek(endPos, SeekOrigin.Begin);
        }

        // uses System.IO, System.IO.Compression
        private static byte[] Deflate(byte[] data, CompressionLevel? level = null)
        {
            byte[] newData;
            using (var memStream = new MemoryStream())
            {
                // write header:
                memStream.WriteByte(0x78);
                switch (level)
                {
                    case CompressionLevel.NoCompression:
                    case CompressionLevel.Fastest:
                        memStream.WriteByte(0x01);
                        break;
                    case CompressionLevel.Optimal:
                        memStream.WriteByte(0xDA);
                        break;
                    default:
                        memStream.WriteByte(0x9C);
                        break;
                }
                // write compressed data (with Deflate headers):
                using (var dflStream = level.HasValue
                    ? new DeflateStream(memStream, level.Value)
                    : new DeflateStream(memStream, CompressionMode.Compress
                )) dflStream.Write(data, 0, data.Length);
                //
                newData = memStream.ToArray();
            }
            // compute Adler-32:
            uint a1 = 1, a2 = 0;
            foreach (byte b in data)
            {
                a1 = (a1 + b) % 65521;
                a2 = (a2 + a1) % 65521;
            }
            // append the checksum-trailer:
            var adlerPos = newData.Length;
            Array.Resize(ref newData, adlerPos + 4);
            newData[adlerPos] = (byte)(a2 >> 8);
            newData[adlerPos + 1] = (byte)a2;
            newData[adlerPos + 2] = (byte)(a1 >> 8);
            newData[adlerPos + 3] = (byte)a1;
            return newData;
        }

        private static object[] ReadArray(BinaryReader reader, FBXPropertyType propertyType)
        {
            var elements = reader.ReadInt32();
            var encoding = reader.ReadInt32();
            var compressed_length = reader.ReadInt32();

            var values = new object[elements];

            var field_data = reader.ReadBytes(compressed_length);

            reader.BaseStream.Seek(-4, SeekOrigin.Current);
            var checksum = reader.ReadInt32();

            switch (encoding)
            {
                case 0:
                    break;
                case 1:
                    MemoryStream outputBytes = new MemoryStream();

                    using (var ms = new MemoryStream(field_data))
                    {
                        ms.ReadByte(); //Skip zlib header
                        ms.ReadByte();

                        using (var inflate = new DeflateWithChecksum(ms, CompressionMode.Decompress))
                        {
                            inflate.CopyTo(outputBytes);
                            inflate.Close();
                        }
                    }

                    field_data = outputBytes.ToArray();
                    break;
                default:
                    throw new Exception($"Unknown encoding: {encoding}");
            }

            using(var stream = new MemoryStream(field_data))
            {
                using (var data_reader = new BinaryReader(stream))
                {
                    for (int i = 0; i < elements; i++)
                    {
                        switch (propertyType)
                        {
                            case FBXPropertyType.ArrayBoolean:
                                values[i] = data_reader.ReadBoolean();
                                break;
                            case FBXPropertyType.ArrayInteger16:
                                values[i] = data_reader.ReadInt16();
                                break;
                            case FBXPropertyType.ArrayInteger32:
                                values[i] = data_reader.ReadInt32();
                                break;
                            case FBXPropertyType.ArrayInteger64:
                                values[i] = data_reader.ReadInt64();
                                break;
                            case FBXPropertyType.ArrayFloat:
                                values[i] = data_reader.ReadSingle();
                                break;
                            case FBXPropertyType.ArrayDouble:
                                values[i] = data_reader.ReadDouble();
                                break;
                            default:
                                break;
                        }
                    }
                }
            }

            return values;
        }

        private string GetDebuggerDisplay()
        {
            switch (Type)
            {
                case FBXPropertyType.ArrayBoolean:
                case FBXPropertyType.ArrayInteger16:
                case FBXPropertyType.ArrayInteger32:
                case FBXPropertyType.ArrayInteger64:
                case FBXPropertyType.ArrayFloat:
                case FBXPropertyType.ArrayDouble:
                case FBXPropertyType.Raw:
                    object[] values = Value as object[];
                    return $"'{Type}' - Array({values.Length})";

                default:
                    return $"'{Type}' - {Value}";
            }
        }
    }

    ///https://github.com/hamish-milne/FbxWriter/blob/53d9479af87639307b7443bae70ca0c8cc58e48d/Fbx/DeflateWithChecksum.cs
    /// <summary>
    /// A wrapper for DeflateStream that calculates the Adler32 checksum of the payload
    /// </summary>
    public class DeflateWithChecksum : DeflateStream
    {
        private const int modAdler = 65521;
        private uint checksumA;
        private uint checksumB;

        /// <summary>
        /// Gets the Adler32 checksum at the current point in the stream
        /// </summary>
        public int Checksum
        {
            get
            {
                checksumA %= modAdler;
                checksumB %= modAdler;
                return (int)((checksumB << 16) | checksumA);
            }
        }

        /// <inheritdoc />
        public DeflateWithChecksum(Stream stream, CompressionMode mode) : base(stream, mode)
        {
            ResetChecksum();
        }

        /// <inheritdoc />
        public DeflateWithChecksum(Stream stream, CompressionMode mode, bool leaveOpen) : base(stream, mode, leaveOpen)
        {
            ResetChecksum();
        }

        // Efficiently extends the checksum with the given buffer
        void CalcChecksum(byte[] array, int offset, int count)
        {
            checksumA %= modAdler;
            checksumB %= modAdler;
            for (int i = offset, c = 0; i < (offset + count); i++, c++)
            {
                checksumA += array[i];
                checksumB += checksumA;
                if (c > 4000) // This is about how many iterations it takes for B to reach IntMax
                {
                    checksumA %= modAdler;
                    checksumB %= modAdler;
                    c = 0;
                }
            }
        }

        /// <inheritdoc />
        public override void Write(byte[] array, int offset, int count)
        {
            base.Write(array, offset, count);
            CalcChecksum(array, offset, count);
        }

        /// <inheritdoc />
        public override int Read(byte[] array, int offset, int count)
        {
            var ret = base.Read(array, offset, count);
            CalcChecksum(array, offset, count);
            return ret;
        }

        /// <summary>
        /// Initializes the checksum values
        /// </summary>
        public void ResetChecksum()
        {
            checksumA = 1;
            checksumB = 0;
        }
    }

    public enum FBXPropertyType : byte
    {
        Boolean = (byte)'C',
        Integer16 = (byte)'Y',
        Integer32 = (byte)'I',
        Integer64 = (byte)'L',
        Float = (byte)'F',
        Double = (byte)'D',

        ArrayBoolean = (byte)'b',
        ArrayInteger16 = (byte)'y',
        ArrayInteger32 = (byte)'i',
        ArrayInteger64 = (byte)'l',
        ArrayFloat = (byte)'f',
        ArrayDouble = (byte)'d',

        String = (byte)'S',
        Raw = (byte)'R'
    }
}
