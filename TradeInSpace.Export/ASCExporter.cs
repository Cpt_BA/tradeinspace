﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TradeInSpace.Export
{
    public class ASCExporter
    {

        public static string GenerateASCFromHeightmap(byte[] HeightmapData, int SuperSampleCount = 1)
        {
            var inPixels = HeightmapData.Length / 2;
            var inRows = inPixels / (int)Math.Floor(Math.Sqrt(inPixels));
            var inCols = inPixels / inRows;

            var verticalSS = SuperSampleCount;
            var horizontalSS = SuperSampleCount * 2;

            //Output needs to be twice as wide to be properly sized
            int[,] OutputHeights = new int[inCols * horizontalSS, inRows * verticalSS];

            var outCols = OutputHeights.GetLength(0);
            var outRows = OutputHeights.GetLength(1);
            var test = new ReadOnlySpan<byte>(HeightmapData);

            for (var y = 0; y < inRows; y++)
            {
                for (var x = 0; x < inCols; x++)
                {
                    var pos = (x + y * inCols) * 2;
                    int height = System.Buffers.Binary.BinaryPrimitives.ReadUInt16BigEndian(test.Slice(pos));

                    OutputHeights[x * horizontalSS, y * verticalSS] = height;
                }
            }

            //Horizontal smoothing pass, every SuperSampleCount column
            for (var y = 0; y < outRows; y += verticalSS)
            {
                for (var x = 0; x < outCols; x += horizontalSS)
                {
                    int currentValue = OutputHeights[x, y];
                    int nextValue = OutputHeights[(x + horizontalSS) % outCols, y];
                    float stepSize = (nextValue - currentValue) / (float)horizontalSS;

                    for (var sample = 1; sample < horizontalSS; sample++)
                    {
                        OutputHeights[x + sample, y] = currentValue + (int)(sample * stepSize);
                    }
                }
            }

            //Vertical smoothing pass, every column now
            for (var x = 0; x < outCols; x++)
            {
                for (var y = 0; y < outRows; y += verticalSS)
                {
                    int currentValue = OutputHeights[x, y];
                    int nextValue = OutputHeights[x, (y + verticalSS) % outRows];
                    float stepSize = (nextValue - currentValue) / (float)verticalSS;

                    for (var sample = 1; sample < verticalSS; sample++)
                    {
                        OutputHeights[x, y + sample] = currentValue + (int)(sample * stepSize);
                    }
                }
            }

            return GenerateASCFromHeightmap(OutputHeights, false, false);
        }

        public static string GenerateASCFromHeightmap(int[,] Samples, bool xFlip = false, bool yFlip = false)
        {
            int xOffset = 0;// (int)(outCols * 0.25);

            int outRows = Samples.GetLength(1);
            int outCols = Samples.GetLength(0);

            //Image is resized at 2:1 aspect ratio to handle lat/long being +- 90/180 not 180/180
            var rowSize = 180.0 / outRows;
            var outLines = new StringBuilder();
            outLines.AppendLine($"NCOLS {outCols}");
            outLines.AppendLine($"NROWS {outRows}");
            outLines.AppendLine($"XLLCORNER -180");
            outLines.AppendLine($"YLLCORNER -90");
            outLines.AppendLine($"CELLSIZE {rowSize}");
            outLines.AppendLine("NODATA_VALUE -99");

            for (var y = 0; y < outRows; y++)
            {
                for (var x = 0; x < outCols; x++)
                {
                    int xFinal = (xOffset + (xFlip ? (outCols - 1 - x) : x)) % outCols;

                    outLines.Append($"{Samples[xFinal, yFlip ? (outRows - 1 - y) : y]:D2} ");
                }

                outLines.AppendLine();
            }

            return outLines.ToString();
        }

        public static string GenerateASCFromHeightmap(ushort[,] Samples, bool xFlip = false, bool yFlip = false)
        {
            int xOffset = 0;// (int)(outCols * 0.25);

            int outRows = Samples.GetLength(1);
            int outCols = Samples.GetLength(0);

            //Image is resized at 2:1 aspect ratio to handle lat/long being +- 90/180 not 180/180
            var rowSize = 180.0 / outRows;
            var outLines = new StringBuilder();
            outLines.AppendLine($"NCOLS {outCols}");
            outLines.AppendLine($"NROWS {outRows}");
            outLines.AppendLine($"XLLCORNER -180");
            outLines.AppendLine($"YLLCORNER -90");
            outLines.AppendLine($"CELLSIZE {rowSize}");
            outLines.AppendLine("NODATA_VALUE -99");

            for (var y = 0; y < outRows; y++)
            {
                for (var x = 0; x < outCols; x++)
                {
                    int xFinal = (xOffset + (xFlip ? (outCols - 1 - x) : x)) % outCols;

                    outLines.Append($"{Samples[xFinal, yFlip ? (outRows - 1 - y) : y]:D2} ");
                }

                outLines.AppendLine();
            }

            return outLines.ToString();
        }

        public static string GenerateASCFromHeightmap(ushort[] Samples, int SampleStride, 
            float XLLCorner = -180, float YLLCorner = -90, float ImageExtentHeight = 180.0f, bool xFlip = false, bool yFlip = false)
        {
            int xOffset = 0;// (int)(outCols * 0.25);

            int outRows = Samples.Length / SampleStride;
            int outCols = SampleStride;

            //Image is resized at 2:1 aspect ratio to handle lat/long being +- 90/180 not 180/180
            var rowSize = ImageExtentHeight / outRows;
            var outLines = new StringBuilder();
            outLines.AppendLine($"NCOLS {outCols}");
            outLines.AppendLine($"NROWS {outRows}");
            outLines.AppendLine($"XLLCORNER {XLLCorner}");
            outLines.AppendLine($"YLLCORNER {YLLCorner}");
            outLines.AppendLine($"CELLSIZE {rowSize}");
            outLines.AppendLine("NODATA_VALUE -99");

            for (var y = 0; y < outRows; y++)
            {
                for (var x = 0; x < outCols; x++)
                {
                    int xFinal = (xOffset + (xFlip ? (outCols - 1 - x) : x)) % outCols;
                    int yFinal = yFlip ? (outRows - 1 - y) : y;

                    outLines.Append($"{Samples[yFinal * SampleStride + xFinal]:D2} ");
                }

                outLines.AppendLine();
            }

            return outLines.ToString();
        }
    }
}
