﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TradeInSpace.Explore;
using TradeInSpace.Explore.Socpak;

namespace TradeInSpace.Export
{
    public class PointCloudExporter
    {
        public static string ExportPointCloud(SOC_Entry fromEntry)
        {
            StringBuilder finalOutput = new StringBuilder();
            List<Vector3> globalPositions = new List<Vector3>();

            void fnProcessNode(SOC_Entry procNode)
            {
                globalPositions.Add(procNode.GlobalPosition);

                foreach (var child in procNode.Children)
                {
                    fnProcessNode(child);
                }

                foreach (var entity in procNode.Entities.OfType<FullEntity>())
                {
                    globalPositions.Add(entity.GlobalPosition);
                }
            }

            fnProcessNode(fromEntry);

            finalOutput.AppendLine("# .PCD v.7 - Point Cloud Data file format");
            finalOutput.AppendLine("VERSION .7");
            finalOutput.AppendLine("FIELDS x y z rgb");
            finalOutput.AppendLine("SIZE 4 4 4 4");
            finalOutput.AppendLine("TYPE F F F F");
            finalOutput.AppendLine("COUNT 1 1 1 1");
            finalOutput.AppendLine("WIDTH 213");
            finalOutput.AppendLine("HEIGHT 1");
            finalOutput.AppendLine("VIEWPOINT 0 0 0 1 0 0 0");
            finalOutput.AppendLine($"POINTS {globalPositions.Count}");
            finalOutput.AppendLine("DATA ascii");

            foreach(var point in globalPositions)
            {
                finalOutput.AppendLine($"{(float)point.X} {(float)point.Y} {(float)point.Z} 4.2108e+06");
            }

            return finalOutput.ToString();
        }
    }
}
