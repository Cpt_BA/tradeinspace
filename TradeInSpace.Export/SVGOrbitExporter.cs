﻿using Svg;
using System;
using System.Drawing;
using System.IO;
using System.Linq;
using TradeInSpace.Explore;
using TradeInSpace.Explore.Socpak;
using TradeInSpace.Explore.Subsumption;

namespace TradeInSpace.Export
{
    public class SVGOrbitExporter
    {
        public static string GenerateSVG(SOC_Entry node, Func<Vector3, double> dimA, Func<Vector3, double> dimB, bool IncludeEntities = false)
        {
            var svgDoc = new SvgDocument
            {
                Width = 1000,
                Height = 1000,
                ViewBox = new SvgViewBox(0, 0, 1000, 1000),
            };

            var spaceBounds = DetermineBounds2(node);
            var vb = svgDoc.ViewBox;

            DrawGrid(svgDoc);

            void CheckBoundAxis(Func<Vector3, double> axis, Func<double, Vector3> newVec, double minValue = 500)
            {
                var size = axis(spaceBounds.Max) - axis(spaceBounds.Min);
                if(size < minValue)
                {
                    var halfDiff = (minValue - size) / 2;
                    spaceBounds.AddPoint(spaceBounds.Min - newVec(halfDiff));
                    spaceBounds.AddPoint(spaceBounds.Max + newVec(halfDiff));
                }
            }

            CheckBoundAxis(v => v.X, x => new Vector3(x, 0, 0), minValue: 500);
            CheckBoundAxis(v => v.Y, y => new Vector3(0, y, 0), minValue: 500);

            SvgElement fnDrawEntity(FullEntity entity, Vector3 parentPos)
            {
                //var nodePos = Vector3.FromString(soc_entry.Pos);
                var nodePos = entity.GlobalPosition;

                var drawX = Remap(dimA(nodePos), dimA(spaceBounds.Min), dimA(spaceBounds.Max), vb.MinX, vb.MinX + vb.Width);
                var drawY = Remap(dimB(nodePos), dimB(spaceBounds.Min), dimB(spaceBounds.Max), vb.MinY, vb.MinY + vb.Height);

                return new SvgRectangle()
                {
                    X = new SvgUnit(SvgUnitType.User, (float)drawX - 1),
                    Y = new SvgUnit(SvgUnitType.User, (float)drawY - 1),
                    Width = new SvgUnit(SvgUnitType.User, 2),
                    Height = new SvgUnit(SvgUnitType.User, 2),
                    Fill = new SvgColourServer(Color.Blue),
                    Stroke = new SvgColourServer(Color.Black),
                    StrokeWidth = 1
                };
            }

            SvgElement fnDrawNode(SOC_Entry socEntry)
            {
                //var nodePos = drawNode.Position ?? Vector3.Zero;
                var nodePos = socEntry.GlobalPosition;

                var drawX = Remap(dimA(nodePos), dimA(spaceBounds.Min), dimA(spaceBounds.Max), vb.MinX, vb.MinX + vb.Width);
                var drawY = Remap(dimB(nodePos), dimB(spaceBounds.Min), dimB(spaceBounds.Max), vb.MinY, vb.MinY + vb.Height);

                return new SvgCircle()
                {
                    CenterX = new SvgUnit(SvgUnitType.User, (float)drawX),
                    CenterY = new SvgUnit(SvgUnitType.User, (float)drawY),
                    Radius = 3,
                    Fill = new SvgColourServer(Color.Red),
                    Stroke = new SvgColourServer(Color.Black),
                    StrokeWidth = 1,
                    ID = socEntry.Name
                };
            }

            SvgGroup fnProcessNode(SOC_Entry procNode, SOC_Entry parent)
            {
                var group = new SvgGroup();
                svgDoc.Children.Add(group);

                var nodeEntry = fnDrawNode(procNode);
                group.Children.Add(nodeEntry);

                if (IncludeEntities)
                {
                    foreach (var entity in procNode.Entities.OfType<FullEntity>())
                    {
                        group.Children.Add(fnDrawEntity(entity, procNode.GlobalPosition));
                    }
                }

                foreach (var child in procNode.Children)
                {
                    group.Children.Add(fnProcessNode(child, procNode));
                }

                return group;
            }

            fnProcessNode(node, null);

            using (var stream = new MemoryStream())
            {
                using (var reader = new StreamReader(stream))
                {
                    svgDoc.Write(stream);
                    stream.Seek(0, SeekOrigin.Begin);
                    return reader.ReadToEnd();
                }
            }
        }

        private static AARect3 DetermineBounds2(SOC_Entry node)
        {
            AARect3 bounds = new AARect3()
            {
                Min = new Vector3(-10, -10, -10),
                Max = new Vector3(10, 10, 10),
            };

            foreach (var child in node.Children)
            {
                var childPos = child.GlobalPosition;
                bounds.AddPoint(childPos);

                var innerBounds = DetermineBounds2(child);

                bounds.AddPoint(innerBounds.Min);
                bounds.AddPoint(innerBounds.Max);

                if (child?.Entities != null)
                    foreach (var entity in child.Entities.OfType<FullEntity>())
                        bounds.AddPoint(entity.GlobalPosition);
            }

            return bounds;
        }

        private static void DrawGrid(SvgDocument svgDoc, float GridSize = 100.0f, float GridWidth = 1.0f)
        {
            SvgGroup gridGroup = new SvgGroup();
            var vb = svgDoc.ViewBox;

            for (float x = vb.MinX + GridSize; x < vb.MinX + vb.Width; x += GridSize)
            {
                gridGroup.Children.Add(new SvgLine()
                {
                    StartX = x,
                    StartY = 0,
                    EndX = x,
                    EndY = vb.MinY + vb.Height,
                    Stroke = new SvgColourServer(Color.Gray)
                });
            }

            for (float y = vb.MinY + GridSize; y < vb.MinY + vb.Height; y += GridSize)
            {
                gridGroup.Children.Add(new SvgLine()
                {
                    StartX = vb.MinX,
                    StartY = y,
                    EndX = vb.MinX + vb.Width,
                    EndY = y,
                    Stroke = new SvgColourServer(Color.Gray),
                    StrokeWidth = GridWidth
                });
            }

            var XAxis = vb.MinX + vb.Width / 2;
            var YAxis = vb.MinY + vb.Height / 2;

            gridGroup.Children.Add(new SvgLine()
            {
                StartX = XAxis,
                StartY = vb.MinY,
                EndX = XAxis,
                EndY = vb.MinY + vb.Height,
                Stroke = new SvgColourServer(Color.Black),
                StrokeWidth = GridWidth * 2
            });

            gridGroup.Children.Add(new SvgLine()
            {
                StartX = vb.MinX,
                StartY = YAxis,
                EndX = vb.MinX + vb.Width,
                EndY = YAxis,
                Stroke = new SvgColourServer(Color.Black),
                StrokeWidth = GridWidth * 2
            });

            svgDoc.Children.Add(gridGroup);
        }

        private static double Remap(double ValueIn, double InMin, double InMax, double OutMin, double OutMax)
        {
            var normalized = (ValueIn - InMin) / (InMax - InMin);
            return (normalized * (OutMax - OutMin)) + OutMin;
        }
    }
}
