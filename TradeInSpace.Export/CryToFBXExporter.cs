﻿using CgfConverter;
using CgfConverter.CryEngineCore;
using System;
using System.Collections.Generic;
using System.DoubleNumerics;
using System.IO;
using System.Linq;
using System.Text;
using TradeInSpace.Explore;
using TradeInSpace.Explore.DataForge;
using TradeInSpace.Export.FBX;
using TradeInSpace.Parse;
using UkooLabs.FbxSharpie;
using UkooLabs.FbxSharpie.Tokens;
using UkooLabs.FbxSharpie.Tokens.Value;
using UkooLabs.FbxSharpie.Tokens.ValueArray;

namespace TradeInSpace.Export
{
    using DN = System.DoubleNumerics;

    public class CryToFBXExporter
    {
        private static CryModelGeometryData TestGeometryCube = new CryModelGeometryData()
        {
            Vertexes = new List<(double, double, double)>()
            {
                (-1, -1, 1),
                (1, -1, 1),
                (-1, 1, 1),
                (1, 1, 1),
                (-1, -1, -1),
                (1, -1, -1),
                (-1, 1, -1),
                (1, 1, -1)
            },
            VertexIndexes = new List<(uint, uint, uint)>()
            {
                (2, 6, 7),
                (2, 3, 7),

                (0, 4, 5),
                (0, 1, 5),
                
                (0, 2, 6),
                (0, 4, 6),
                
                (1, 3, 7),
                (1, 5, 7),
                
                (0, 2, 3),
                (0, 1, 3),

                (4, 6, 7),
                (4, 5, 7)
            }
        };

        public static void WriteHierarchy(SCHieracrhyNode Hierarchy, string FileName, DFDatabase GameDatabase, FBXExportSettings ConvertSettings = null)
        {
            ConvertSettings = ConvertSettings ?? FBXExportSettings.DefaultSettings;

            var scene = ConvertHierarchyToScene(Hierarchy, GameDatabase, ConvertSettings);
            WriteScene(scene, FileName);
        }

        public static void WriteScene(FBXScene Scene, string FileName, bool Binary = true)
        {
            if (File.Exists(FileName)) File.Delete(FileName);

            if (!Directory.Exists(Path.GetDirectoryName(FileName)))
                Directory.CreateDirectory(Path.GetDirectoryName(FileName));

            using (var fs = File.OpenWrite(FileName))
            {
                if (Binary)
                    new FbxBinaryWriter(fs).Write(Scene.Construct());
                else
                    new FbxAsciiWriter(fs).Write(Scene.Construct());
            }
        }

        public static void WriteHierarchy(SCHieracrhyNode Hierarchy, string FileName, DFDatabase GameDatabase)
        {
            var fbx_scene = CryToFBXExporter.ConvertHierarchyToScene(Hierarchy, GameDatabase);
            WriteScene(fbx_scene, FileName);
        }

        public static FBXScene ConstructTestSceneFromHierarchy()
        {
            var settings = FBXExportSettings.DefaultSettings;

            SCHieracrhyNode root_node = new SCHieracrhyNode();
            SCHieracrhyNode geom_node = new SCHieracrhyNode()
            {
                GeometryData = TestGeometryCube
            };
            root_node.Children.Add(geom_node);

            var scene = ConvertHierarchyToScene(root_node, null, settings);

            return scene;
        }

        public static FBXScene ConstructTestScene()
        {
            var settings = FBXExportSettings.DefaultSettings;
            var scene = new FBXScene();


            var fbx_model = CreateEmpty("Test", settings.MakeElementID(), Matrix4x4.Identity);
            var fbx_geo = AttachGeometry(TestGeometryCube, "Test", settings.MakeElementID(), fbx_model, settings);

            scene.Add(fbx_model);
            scene.Add(fbx_geo);

            return scene;
        }

        public static FBXScene ConvertModelToScene(CryEngine cryModel, FBXExportSettings convertSettings = null)
        {
            var scene = new FBXScene();

            foreach(var child in ConvertModel(cryModel, null, convertSettings))
            {
                scene.Add(child);
            }

            return scene;
        }

        public static FBXScene ConvertHierarchyToScene(SCHieracrhyNode Hierarchy, IPackedFileReader GameData,
            FBXExportSettings ConvertSettings = null)
        {
            //Default settings
            ConvertSettings = ConvertSettings ?? FBXExportSettings.DefaultSettings;
            
            var _WrittenGeometryCache = new Dictionary<(long, string), FBXSceneGeometry>();

            IEnumerable<FBXSceneItem> _ExploreNodeInner(SCHieracrhyNode _node, FBXSceneModel _parent)
            {
                var fbx_name = _node.Name;
                var fbx_model = CreateEmpty(fbx_name, ConvertSettings.MakeElementID(), _node.LocalPosition, _node.LocalRotation, _node.LocalScale);
                yield return fbx_model;
                _parent?.Children.Add(fbx_model);

                fbx_model.DirectTokens.Add("TIS_Type", new StringToken(_node.NodeType.ToString()));
                //Any extra data from the SC Hierarchy scan...
                WriteCustomProperties(fbx_model, _node);

                //Attach a dummy node for everything without geometry
                var fbx_node = AttachNode(_node.Name, ConvertSettings.MakeElementID(), fbx_model);
                yield return fbx_node;

                if (_node.GeometryData != null)
                {
                    var model_chunk_key = (_node.ModelID, _node.Name);

                    if (_WrittenGeometryCache.ContainsKey(model_chunk_key))
                    {
                        fbx_model.Extra = "Mesh";
                        fbx_model.Children.Add(_WrittenGeometryCache[model_chunk_key]);
                    }
                    else
                    {
                        var geo_node = AttachGeometry(_node.GeometryData, fbx_name, ConvertSettings.MakeElementID(), fbx_model, ConvertSettings);
                        _WrittenGeometryCache[model_chunk_key] = geo_node;
                        yield return geo_node;
                    }
                }
                else if (!string.IsNullOrEmpty(_node.ModelPath))
                {
                    fbx_model.DirectTokens.Add("ModelPath", new StringToken(_node.ModelPath));

                    if (ConvertSettings.ExportNesetedModels)
                    {
                        CryEngine model = new CryEngine(_node.ModelPath, "", GameData.HasEntry, GameData.ReadFileBinary);

                        try
                        {
                            model.ProcessCryengineFiles();
                        }
                        catch (Exception ex)
                        {
                            //TODO: Better handling in general..........
                        }

                        //Here as well.....
                        if (model?.Models?.Count == 2)
                        {
                            foreach (var child in ConvertModel(model, fbx_model, ConvertSettings))
                            {
                                yield return child;
                            }
                        }
                    }
                }

                //Recursively explore children.
                foreach(var child_node in _node.Children)
                {
                    foreach(var fbx_child in _ExploreNodeInner(child_node, fbx_model))
                    {
                        yield return fbx_child;
                    }
                }
            }

            var scene = new FBXScene();

            foreach (var item in _ExploreNodeInner(Hierarchy, null))
                scene.Add(item);

            return scene;
        }

        private static void WriteCustomProperties(FBXSceneModel fbx_model, SCHieracrhyNode hierarchy_node)
        {
            foreach (var property in hierarchy_node.AdditionalProperties)
            {
                switch (property.Value)
                {
                    case string str:
                        fbx_model.DirectTokens.Add(property.Key, new StringToken(str));
                        break;
                    case float f:
                        fbx_model.DirectTokens.Add(property.Key, new FloatToken(f));
                        break;
                    case int i:
                        fbx_model.DirectTokens.Add(property.Key, new IntegerToken(i));
                        break;
                    case List<Explore.Socpak.Vector2> vecs:
                        float[] vec_flat = new float[vecs.Count * 2];
                        for(int i = 0; i < vecs.Count; i++)
                        {
                            vec_flat[i * 2 + 0] = (float)vecs[i].X;
                            vec_flat[i * 2 + 1] = (float)vecs[i].Y;
                        }
                        fbx_model.DirectTokens.Add(property.Key, new FloatArrayToken(vec_flat));
                        break;
                    case null:
                        break;
                    default:
                        break;
                }
            }
        }

        private static FBXSceneModel CreateEmpty(string Name, long ID, Matrix4x4 Transform)
        {
            var transformation = Transform;// Matrix4x4.Transpose(Transform);
            transformation.Decompose(out var _pos, out var _rot, out var _sca);

            if(_pos.X == 0 && _pos.Y == 0 && _pos.Z == 0 && 
                (transformation.M41 != 0 || transformation.M42 != 0 || transformation.M43 != 0))
            {
                //Potential transposed matrix!
                Transform.Decompose(out _pos, out _rot, out _sca);
            }

            return CreateEmpty(Name, ID, _pos, _rot, _sca);
        }

        private static FBXSceneModel CreateEmpty(string Name, long ID, Explore.Vector3 Pos, Explore.Vector4 Rot, Explore.Vector3 Scale)
        {
            var _erot = ((Quaternion)Rot).ToEulerAngles();

            var fbx_model = new FBXSceneModel(ID, Name);
            fbx_model.Properties.AddVector3(FBXPropertyValue.Property.LCL_Translation, Pos.X, Pos.Y, Pos.Z);
            fbx_model.Properties.AddVector3(FBXPropertyValue.Property.LCL_Rotation, _erot.X, _erot.Y, _erot.Z);
            fbx_model.Properties.AddVector3(FBXPropertyValue.Property.LCL_Scale, Scale.X, Scale.Y, Scale.Z);

            fbx_model.Properties.AddInteger(FBXPropertyValue.Property.RotationActive, 1);
            fbx_model.Properties.AddInteger(FBXPropertyValue.Property.RotationOrder, 0);
            fbx_model.Properties.AddInteger(FBXPropertyValue.Property.InheritType, 1);

            return fbx_model;
        }

        private static FBXSceneNodeAttribute AttachNode(string Name, long ID, FBXSceneModel Model)
        {
            var fbx_node = new FBXSceneNodeAttribute(ID, Name);
            Model.Children.Add(fbx_node);
            return fbx_node;
        }

        private static IEnumerable<FBXSceneItem> ChunkToModel(ChunkNode Node, FBXSceneModel Parent, CryEngine CryModel,
            FBXExportSettings convertSettings, List<int> ignoreTextureIDs)
        {
            var node_name = Node.Name;
            var transform = Utilities.MatrixFromTupleTransposed(Node.LocalTransformTuple);

            //Create invisible node for position
            var fbx_model = CreateEmpty(node_name, convertSettings.MakeElementID(), transform);


            bool has_model = false;

            if (Node.ChunkType != ChunkType.Helper)
            {
                //TODO: Better mechanism? Pre-cache?
                var geoNode = CryModel.Models[1].NodeMap.Values.FirstOrDefault(n => n.Name == Node.Name);

                if (geoNode != null)
                {
                    switch (geoNode.ObjectChunk.ChunkType)
                    {
                        case ChunkType.Mesh:

                            var cry_geo_data = CryModelGeometryData.ReadFromChunk(geoNode, ignoreTextureIDs);
                            if (cry_geo_data != null)
                            {
                                yield return AttachGeometry(cry_geo_data, geoNode.Name, convertSettings.MakeElementID(), fbx_model, convertSettings);
                                has_model = true;
                            }
                            break;
                        case ChunkType.MeshSubsets:
                            throw new NotSupportedException();
                            break;
                        default:
                            throw new NotSupportedException();
                            break;
                    }
                }
            }

            if (!has_model)
            {
                //Create a pseudo-node for this element if we do not have a model.
                yield return AttachNode(node_name, convertSettings.MakeElementID(), fbx_model);
            }

            //Once done, add it to a potential parent and emit it
            Parent?.Children.Add(fbx_model);
            yield return fbx_model;

            //Recursively explore child nodes
            if (Node.AllChildNodes != null)
            {
                foreach (var child_node in Node.AllChildNodes)
                {
                    foreach (var nested_node in ChunkToModel(child_node, fbx_model, CryModel, convertSettings, ignoreTextureIDs))
                    {
                        yield return nested_node;
                    }
                }
            }
        }

        private static FBXSceneGeometry AttachGeometry(CryModelGeometryData GeometryData, string Name, long ID, FBXSceneModel Model, 
            FBXExportSettings convertSettings)
        {
            FBXSceneGeometry geometry = new FBXSceneGeometry(ID, Name);

            geometry.Vertices = FlattenVector3(GeometryData.Vertexes);

            if (GeometryData.MeshSubSets.Count == 0)
            {
                geometry.VertexIndicies = FlattenUnit3(GeometryData.VertexIndexes);
            }
            else
            {
                geometry.VertexIndicies = FlattenUnit3(GeometryData.MeshSubSets.SelectMany(s => s.Item2).ToList());
            }

            //Write Normals
            if (convertSettings.ExportV_Normals)
            {
                if (GeometryData.Normals.Count > 0)
                {
                    var normals = new FBXLayer("LayerElementNormal", "Test");
                    normals.Values["Normals"] = FlattenVector3(GeometryData.Normals);
                    geometry.Layers.Add(normals);
                }
            }

            //Update our type info to state we have geometry
            Model.Children.Add(geometry);
            Model.Extra = "Mesh";

            return geometry;
        }
        private static double[] FlattenVector3(List<(double, double, double)> Vectors)
        {
            double[] doubles = new double[Vectors.Count * 3];
            for (int i = 0; i < Vectors.Count; i++)
            {
                doubles[i * 3 + 0] = Vectors[i].Item1;
                doubles[i * 3 + 1] = Vectors[i].Item2;
                doubles[i * 3 + 2] = Vectors[i].Item3;
            }
            return doubles;
        }
        private static int[] FlattenUnit3(List<(uint, uint, uint)> Vectors, bool FlipFace = true)
        {
            int[] ints = new int[Vectors.Count * 3];
            for (int i = 0; i < Vectors.Count; i++)
            {
                ints[i * 3 + 0] = (int)Vectors[i].Item1;
                ints[i * 3 + 1] = (int)Vectors[i].Item2;
                //This is flipped to mark the end of the face
                if (FlipFace)
                    ints[i * 3 + 2] = (int)Vectors[i].Item3 * -1 - 1;
                else
                    ints[i * 3 + 2] = (int)Vectors[i].Item3;
            }
            return ints;
        }

        public static IEnumerable<FBXSceneItem> ConvertModel(CryEngine CryModel, FBXSceneModel ParentItem = null, 
            FBXExportSettings ConvertSettings = null)
        {
            //Default settings
            ConvertSettings = ConvertSettings ?? FBXExportSettings.DefaultSettings;

            var ignoreTextureIDs = new List<int>();
            for (int i = 0; i < CryModel.Materials.Count; i++)
                if (CryModel.Materials[i].Shader?.Equals("nodraw", StringComparison.CurrentCultureIgnoreCase) ?? true)
                    ignoreTextureIDs.Add(i);

            //Iterate over all root nodes in the primary model
            foreach (var positionRoot in CryModel.Models[0].NodeMap.Values.Where(a => a.ParentNodeID == ~0))
            {
                //Convert them FBX Model entries
                foreach (var modelEntry in ChunkToModel(positionRoot, ParentItem, CryModel, ConvertSettings, ignoreTextureIDs))
                {
                    yield return modelEntry;
                }
            }
        }

    }

    public class FBXExportSettings
    {
        private HashSet<long> used_ids = new HashSet<long>();
        private Random random = null;
        public long MakeElementID()
        {
            if (random == null)
                random = new Random();
            
            long attempt = random.Next();
            //Better way of doing this?
            while(used_ids.Contains(attempt))
                attempt = random.Next();

            used_ids.Add(attempt);
            return attempt;
        }

        public bool ExportTextures { get; set; }    = false;
        public bool ExportNesetedModels { get; set; } = false;

        public bool ExportGeometry { get; set; }    = true;
        public bool ExportVertexes { get; set; }    = true;
        public bool ExportV_Normals { get; set; }   = false;
        public bool ExportV_UVs { get; set; }       = false;
        public bool ExportV_Colors { get; set; }    = false;

        public bool CustomSetting_ModelPath { get; set; } = false;

        public static readonly FBXExportSettings DefaultSettings = new FBXExportSettings();
    }
}
