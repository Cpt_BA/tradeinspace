﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Text;
using TradeInSpace.Explore;
using TradeInSpace.Explore.DataForge;
using TradeInSpace.Parse;

namespace TradeInSpace.Export
{
    public class ExportUtils
    {
        public static Bitmap ExtractImage(DFDatabase GameDatabase, string DDSPath)
        {
            string baseName = DDSPath;
            int fileIndex = 0;

            if (Path.GetExtension(baseName) != ".dds")
            {
                fileIndex = int.Parse(baseName.Substring(baseName.LastIndexOf('.') + 1));
                baseName = baseName.Substring(0, baseName.LastIndexOf('.'));
            }

            var headerData = GameDatabase.ReadPackedFileBinary(baseName);
            var header = DDSHeader.Read(headerData);
            int width = header.Width, height = header.Height;
            var data = fileIndex == 0 ? header.AdditionalData : GameDatabase.ReadPackedFileBinary(DDSPath);
            byte[] decodedData = null;
            int bitsPerPixel = 0;

            if (header.DX10Header?.DxgiFormat == DDSHeaderDX10.DX10_DXGIFormat.DXGI_FORMAT_BC7_TYPELESS ||
                header.DX10Header?.DxgiFormat == DDSHeaderDX10.DX10_DXGIFormat.DXGI_FORMAT_BC7_UNORM ||
                header.DX10Header?.DxgiFormat == DDSHeaderDX10.DX10_DXGIFormat.DXGI_FORMAT_BC7_UNORM_SRGB)
            {
                //BLEH. Assemble entire file properly and use pfim
                //Total hack to handle BC7 data for now.
                //Either commit to pfim or write the full bc7 algo.......
                using (MemoryStream ms = new MemoryStream())
                {
                    //Header first
                    ms.Write(headerData, 0, header.HeaderLength);
                    Console.WriteLine(ms.Position);
                    //Then largest mip down to smallest
                    for (int mip_level = header.MipMapCount - 3; mip_level > 0; mip_level--)
                    {
                        var mip_name = $"{baseName}.{mip_level}";
                        var mip_data = GameDatabase.ReadPackedFileBinary(mip_name);

                        ms.Write(mip_data, 0, mip_data.Length);
                    }

                    if (header.AdditionalData != null)
                        ms.Write(header.AdditionalData, 0, header.AdditionalData.Length);

                    ms.Seek(0, SeekOrigin.Begin);

                    //File.WriteAllBytes(@"E:\SC\test.dds", ms.ToArray());

                    var pf_image = Pfim.Pfimage.FromStream(ms);
                    pf_image.Decompress();

                    //var pfimHeader = new Pfim.DdsHeader(ms);
                    //var config = new Pfim.PfimConfig(decompress: false);
                    //var dds = new Pfim.dds.Bc7Dds(pfimHeader, config);
                    //var ddsData = dds.DataDecode(ms, config);

                    //width/height from DecodeDDSData will be correct, dds.Width is the overall width

                    bitsPerPixel = pf_image.BitsPerPixel;

                    if (fileIndex == 0)
                        return null;

                    int mip_index = header.MipMapCount - 3 - fileIndex;
                    int mip_offset, mip_length;
                    
                    if(mip_index == 0)
                    {
                        width = header.Width;
                        height = header.Height;
                        mip_offset = 0;
                        mip_length = pf_image.DataLen;
                    }
                    else
                    {
                        var mip_data = pf_image.MipMaps[mip_index - 1];
                        width = mip_data.Width;
                        height = mip_data.Height;
                        mip_offset = mip_data.DataOffset;
                        mip_length = mip_data.DataLen;
                    }

                    if(mip_length != width * height * (bitsPerPixel / 8))
                    {

                    }

                    decodedData = new byte[width * height * (bitsPerPixel / 8)];
                    Array.Copy(pf_image.Data, mip_offset, decodedData, 0, mip_length);

                    for (int i = 3; i < decodedData.Length; i += 4)
                    {
                        decodedData[i] = 255; //Set max alpha
                    }
                }
            }
            else
            {
                try
                {
                    decodedData = DDSParser.DecodeDDSData(header, fileIndex, data, out bitsPerPixel, out width, out height);
                }
                //Total hack to handle BC7 data for now.
                //Either commit to pfim or write the full algo.......
                catch (InvalidDataException idex)
                {
                    /*
                    using (MemoryStream ms = new MemoryStream(headerData))
                    using (MemoryStream ds = new MemoryStream(data))
                    {
                        var pfimHeader = new Pfim.DdsHeader(ms);
                        var config = new Pfim.PfimConfig(decompress: false);
                        var dds = new Pfim.dds.Bc7Dds(pfimHeader, config);
                        var ddsData = dds.DataDecode(ds, config);

                        //width/height from DecodeDDSData will be correct, dds.Width is the overall width

                        bitsPerPixel = dds.BitsPerPixel;
                        decodedData = new byte[width * height * (bitsPerPixel / 8)];
                        Array.Copy(ddsData, decodedData, decodedData.Length);

                        for (int i = 3; i < decodedData.Length; i += 4)
                        {
                            decodedData[i] = 255; //Set max alpha
                        }
                    }
                    */
                }
            }
            int stride = width * (bitsPerPixel / 8);

            if (bitsPerPixel != 32)
            {
                throw new NotSupportedException();
            }

            if (width * height * (bitsPerPixel / 8) != decodedData.Length)
            {
                throw new InvalidDataException();
            }

            Bitmap result = new Bitmap(width, height);
            var bmpData = result.LockBits(new Rectangle(0, 0, width, height),
                System.Drawing.Imaging.ImageLockMode.ReadWrite,
                System.Drawing.Imaging.PixelFormat.Format32bppArgb);
            System.Runtime.InteropServices.Marshal.Copy(decodedData, 0, bmpData.Scan0, decodedData.Length);
            result.UnlockBits(bmpData);

            return result;
        }

        public static Color ColorFromHSV(float Hue, float Saturation, float Value)
        {
            return Utilities.hsv2rgb(Hue, Saturation, Value);
        }
    }
}
