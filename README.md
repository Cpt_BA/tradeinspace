Table Of Contents

[TOC]

# TradeIn.Space Complete Repository

This repository contains complete source code for the TradeIn.Space website. This includes the website, database models, import utility, and general-purpose data-scraping desktop application.

TradeIn.Space is a utility website for all players of star citizen. It has many useful features that can help with just about any activity or mission available to players.

Some Key features of the website are:

- Advanced trade calculation algorithm, taking into account restock, transit times, quantum cooldowns, and more.
- 2D Maps of planet surfaces, including buildings and structures!
- 3D Ship Maps, allowing you to explore the interiors of ships.
- Mission listing, including requirements and rewards.
- Raw data tables, with saveable export to csv.
- Ship equiment data: QD, Mining Laser, Cooler, Shield, Weapons, Power, Missiles, and Paint.
- FPS equipment data: Food, Undersuit, Armor, Weapons, Clothing, and more.
- Ship listing, with loadout editor (Loadout editor needs work)
- Custom hangar for own ship builds
- Mining elements (with asteroid composition), modules, and gadgets.


# Projects

## TIS Projects
Most projects are compiled to DLL's and referenced by other projects.

### [TradeInSpace.dll](https://bitbucket.org/Cpt_BA/tradeinspace/src/master/TradeInSpace/)
.Net Core repository for the website itself.

### [SC_Explorer.exe](https://bitbucket.org/Cpt_BA/tradeinspace/src/master/SC_Explorer/)
WPF Data-mining application.

### [DataImport.dll](https://bitbucket.org/Cpt_BA/tradeinspace/src/master/DataImport/)
.Net Core application, for populating the website database from new game data files, and performing renders of planets or models.

### [MapTest.exe](https://bitbucket.org/Cpt_BA/tradeinspace/src/master/MapTest/)
.Net Framework app for testing map rendering.

### [TradeInSpace.Data.dll](https://bitbucket.org/Cpt_BA/tradeinspace/src/master/TradeInSpace.Data/)
Minor project for database setup and initialization.

### [TradeInSpace.Explore.dll](https://bitbucket.org/Cpt_BA/tradeinspace/src/master/TradeInSpace.Explore/)
Most data classes for exploring SC archives and data. Many of these classes are mapped directly to SC data.

### [TradeInSpace.Export.dll](https://bitbucket.org/Cpt_BA/tradeinspace/src/master/TradeInSpace.Export/)
A handful of various classes used for exporting data.

### [TradeInSpace.Models.dll](https://bitbucket.org/Cpt_BA/tradeinspace/src/master/TradeInSpace.Models/)
Database models.

### [TradeInSpace.Parse.dll](https://bitbucket.org/Cpt_BA/tradeinspace/src/master/TradeInSpace.Parse/)
A handful of various classes used for parsing data.

## Dependencies
Many of these dependencies are out-of-date since they are full copies. I want to replace them with custom parsing at some point, and hopfully be able to remove them entirely eventually.

### CgfConverter/Cryengine-Converter
[Original Repository](https://github.com/Markemp/Cryengine-Converter)
CgfConverter is largely unmodified, mostly just better hooks to prevent having to write files to disk, and some custom chunks were (mostly) reverse-engineered and added.

### ICSharpCode.SharpZipLib
[Original Repository](https://github.com/icsharpcode/SharpZipLib)
SharpZipLib is mostly modified for the same reasons, however some small performance changes were made (if I recall correctly) to help this use-case.

### unforge
[Original Repository](https://github.com/dolkensp/unp4k)
unforge is largely unmodified, again just hooked for direct buffer file reading.

# Disclaimer
*TradeInSpace is not affiliated, associated, authorized, endorsed by, or in any way officially connected with R.S.I. (Roberts Space Industries), C.I.G.(Cloud Imperium Games), or any other related parties.*
