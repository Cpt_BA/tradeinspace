﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Xml;
using System.Xml.Linq;
using TradeInSpace.Data;
using TradeInSpace.Models;
using static TradeInSpace.Models.Enums;
using Microsoft.Extensions.Logging;
using Version = TradeInSpace.Models.Version;

namespace TradeInSpace.Data
{
    public class TISContext : DbContext
    {
        public TISContext(DbContextOptions options) : base(options)
        {
            
        }
        
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            //use this to configure the contex
            
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //use this to configure the model
            modelBuilder.Entity<Version>().ToTable("Version");

            modelBuilder.Entity<GameDatabase>().ToTable("GameDatabase");
            modelBuilder.Entity<GameShip>().ToTable("GameShip");
            modelBuilder.Entity<GameEquipment>().ToTable("GameEquipment");

            modelBuilder.Entity<SystemBody>().ToTable("SystemBody");
            modelBuilder.Entity<SystemLocation>().ToTable("SystemLocation");
            modelBuilder.Entity<SystemLandingZone>().ToTable("SystemLandingZones");

            modelBuilder.Entity<TradeItem>().ToTable("TradeItem");
            modelBuilder.Entity<TradeShop>().ToTable("TradeShop");
            modelBuilder.Entity<TradeTableEntry>().ToTable("TradeTableEntry");
            modelBuilder.Entity<TradeTable>().ToTable("TradeTable");

            modelBuilder.Entity<MiningElement>().ToTable("MiningElement");
            modelBuilder.Entity<HarvestableLocation>().ToTable("HarvestableLocation");
            modelBuilder.Entity<Harvestable>().ToTable("Harvestable");
            modelBuilder.Entity<SubHarvestable>().ToTable("SubHarvestable");
            modelBuilder.Entity<MiningComposition>().ToTable("MiningComposition");
            modelBuilder.Entity<MiningCompositionEntry>().ToTable("MiningCompositionEntry");
            modelBuilder.Entity<MissionGiver>().ToTable("MissionGiver");
            modelBuilder.Entity<MissionGiverEntry>().ToTable("MissionGiverEntry");
            modelBuilder.Entity<MissionGiverScope>().ToTable("MissionGiverScope");

            //modelBuilder.Entity<OSM_Waypoint>().ToTable("OSM_Waypoint");
            //modelBuilder.Entity<OSM_Area>().ToTable("OSM_Area");

            modelBuilder.Entity<TradeShop>()
                .HasOne(ts => ts.Location)
                .WithMany(l => l.Shops)
                .OnDelete(DeleteBehavior.ClientSetNull);

            modelBuilder.Entity<TradeEquipmentEntry>()
                .HasOne(ts => ts.Station)
                .WithMany(s => s.EquipmentEntries)
                .OnDelete(DeleteBehavior.ClientSetNull);

            modelBuilder.Entity<TradeEquipmentEntry>()
                .HasOne(tee => tee.Equipment)
                .WithMany(e => e.TradeEntries)
                .OnDelete(DeleteBehavior.ClientSetNull);

            modelBuilder.Entity<GameEquipment>()
                .HasOne(ge => ge.BaseEquipment)
                .WithMany(be => be.DerivedEquipments)
                .OnDelete(DeleteBehavior.ClientSetNull);

            modelBuilder.Entity<Harvestable>()
                .HasMany(h => h.SubHarvestables)
                .WithOne(sh => sh.ParentHarvestable)
                .OnDelete(DeleteBehavior.ClientSetNull);

            modelBuilder.Entity<Harvestable>()
                .HasMany(h => h.ParentHarvestables)
                .WithOne(ph => ph.ChildHarvestable)
                .OnDelete(DeleteBehavior.ClientSetNull);

            modelBuilder.Entity<Harvestable>()
                .HasOne(h => h.Element)
                .WithMany()
                .OnDelete(DeleteBehavior.ClientSetNull);

            modelBuilder.Entity<Harvestable>()
                .HasOne(h => h.Composition)
                .WithMany()
                .OnDelete(DeleteBehavior.ClientSetNull);

            modelBuilder.Entity<Harvestable>()
                .HasMany(h => h.ParentHarvestables)
                .WithOne(ph => ph.ChildHarvestable)
                .OnDelete(DeleteBehavior.ClientSetNull);

            modelBuilder.Entity<HarvestableLocation>()
                .HasOne(hl => hl.Harvestable)
                .WithMany(h => h.Locations)
                .OnDelete(DeleteBehavior.ClientSetNull);

            modelBuilder.Entity<SystemLocation>()
                .HasOne(l => l.ParentLocation)
                .WithMany(l => l.ChildLocations)
                .OnDelete(DeleteBehavior.ClientSetNull);

            modelBuilder.Entity<SystemLocation>()
                .HasOne(l => l.ParentBody);
                //.WithMany(l => l.ChildLocations)
                //.OnDelete(DeleteBehavior.ClientSetNull);

            modelBuilder.Entity<SystemBody>()
                .HasOne(b => b.ParentBody)
                .WithMany(b => b.ChildBodies)
                .OnDelete(DeleteBehavior.ClientSetNull);

            modelBuilder.Entity<TradeTableEntry>()
                .HasOne(tte => tte.Station)
                .WithMany(s => s.TradeEntries)
                .OnDelete(DeleteBehavior.ClientSetNull);

            modelBuilder.Entity<TradeTableEntry>()
                .HasOne(tte => tte.Item)
                .WithMany()
                .OnDelete(DeleteBehavior.ClientSetNull);

            modelBuilder.Entity<MiningCompositionEntry>()
                .HasOne(mc => mc.Element)
                .WithMany()
                .OnDelete(DeleteBehavior.ClientSetNull);

            modelBuilder.Entity<MiningElement>()
                .HasOne(ti => ti.TradeItem)
                .WithMany()
                .OnDelete(DeleteBehavior.ClientSetNull);

            modelBuilder.Entity<SystemLocation>()
                .Property(sl => sl.StarmapID)
                .IsRequired(false);
        }

        public IQueryable<Version> GetAvailableVersions(bool includeTrades = false)
        {
            var versions = this.Versions
                .Where(v => v.Enabled);
            if (includeTrades) {
                return versions
                    .Include(v => v.GameDatabases).ThenInclude(gd => gd.TradeTables).ThenInclude(tt => tt.Entries)
                    .Include(v => v.GameDatabases).ThenInclude(gd => gd.TradeTables).ThenInclude(tt => tt.EquipmentEntries);
            }
            else
            {
                return versions.Include(v => v.GameDatabases);
            }
        }

        public Version GetDefaultVersion(bool includeTrades = false)
        {
            return GetAvailableVersions(includeTrades).FirstOrDefault(v => v.Default);
        }

        public GameDatabase GetTradeData(string Channel = "LIVE")
        {
            return GetDefaultVersion(true).GameDatabases.FirstOrDefault(gd => gd.Enabled && gd.DB_Channel == Channel);
        }

        public IEnumerable<Version> GetVersionListing()
        {
            var versions = this.Versions
                .Where(v => v.Enabled)
                .Include(v => v.GameDatabases);
            var dbVersions = versions.ThenInclude(gd => gd.TradeTables);
            foreach (var version in dbVersions)
            {
                version.GameDatabases = version.GameDatabases.Where(gd => gd.Enabled).ToList();

                foreach (var versionDB in version.GameDatabases)
                {
                    versionDB.TradeTables = versionDB.TradeTables.Where(tt => tt.Active).ToList();
                }
            }
            return dbVersions;
        }

        public GameDatabase GetPartialGameData(string Channel = "LIVE")
        {
            var version = GetDefaultVersion();

            return this.GameDatabases
                .Where(gd => gd.Enabled && gd.VersionID == version.ID && gd.DB_Channel == Channel)
                .FirstOrDefault();
        }

        public GameDatabase GetCompleteGameData(string Channel = "LIVE")
        {
            var version = GetDefaultVersion();

            return this.GameDatabases
                .Where(gd => gd.Enabled && gd.VersionID == version.ID && gd.DB_Channel == Channel)
                .Include( gd => gd.Ships)
                .Include(gd => gd.Equipment)
                .Include(gd => gd.SystemBodies)
                .Include(gd => gd.SystemLocations)
                .Include(gd => gd.LandingZones)
                .Include(gd => gd.TradeItems)
                .Include(gd => gd.TradeShops)
                .Include(gd => gd.Manufacturers)
                .Include(gd => gd.Harvestables)
                .Include(gd => gd.SubHarvestables)
                .Include(gd => gd.HarvestableLocations)
                .Include(gd => gd.MiningElements)
                .Include(gd => gd.MiningCompositions)
                .Include(gd => gd.MiningCompositionEntries)
                .Include(gd => gd.MissionGivers)
                .Include(gd => gd.MissionGiverScopes)
                .Include(gd => gd.Missions)
                .FirstOrDefault();
        }



        public DbSet<Version> Versions { get; set; }
        public DbSet<GameDatabase> GameDatabases { get; set; }

        public DbSet<GameShip> Ships { get; set; }
        public DbSet<GameEquipment> Equipment { get; set; }

        public DbSet<SystemBody> SystemBodies { get; set; }
        public DbSet<SystemLocation> SystemLocations { get; set; }
        public DbSet<SystemLandingZone> SystemLandingZones { get; set; }
        
        public DbSet<TradeItem> Items { get; set; }
        public DbSet<TradeTable> TradeTables { get; set; }
        public DbSet<TradeShop> Shops { get; set; }
        public DbSet<TradeTableEntry> TradeEntries { get; set; }
        public DbSet<TradeEquipmentEntry> EquipmentTrades { get; set; }

        public DbSet<MiningElement> MiningElements { get; set; }
        public DbSet<HarvestableLocation> MiningLocations { get; set; }
        public DbSet<Harvestable> MiningLocationEntries { get; set; }
        public DbSet<MiningComposition> MiningCompositions { get; set; }
        public DbSet<MiningCompositionEntry> MiningCompositionEntries { get; set; }
        public DbSet<MissionGiver> MissionGivers { get; set; }
        public DbSet<MissionGiverScope> MissionGiverScopes { get; set; }
        public DbSet<MissionGiverEntry> Missions { get; set; }


        //public DbSet<OSM_Waypoint> Waypoints { get; set; }
        //public DbSet<OSM_Area> Areas { get; set; }
    }
}
