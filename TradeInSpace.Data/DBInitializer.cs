﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TradeInSpace.Models;

namespace TradeInSpace.Data
{
    public class DBInitializer
    {
        private static string TestImportDBName = "Test_DB_Generated";

        public static void Initialize(TISContext context)
        {
            context.Database.EnsureCreated();

            if (context.Versions.Any(v => v.Name == TestImportDBName)) return;

            Random randomData = new Random("Star Citizen :)".GetHashCode());




            var version = new Models.Version()
            {
                Name = TestImportDBName,
                Enabled = false,
                Description = "Version for testing",
                ReleaseDate = DateTime.UtcNow
            };


            var gameDB = new GameDatabase()
            {
                Name = TestImportDBName,
                Description = "Test game database from random data.",
                Enabled = false,
                FinishedImporting = false,
                Version = version
            };

            context.Versions.Add(version);
            context.SaveChanges();

            int numPlanets = randomData.Next(3, 5);
            int numItems = randomData.Next(40, 60);
            int numTrades = randomData.Next(500, 800);
            int numCategories = randomData.Next(4, 9);
            int numManufacturer = randomData.Next(4, 9);
            var numShips = randomData.Next(30, 40);
            var numDrives = randomData.Next(10, 15);
            var numGivers = randomData.Next(5, 10);
            var numMissions = randomData.Next(30, 50);

            for (int planetNum = 0; planetNum < numPlanets; planetNum++)
            {
                var bodyLocation = new Models.SystemLocation()
                {
                    GameDB = gameDB,
                    Name = $"Planet {GenerateName(randomData, 8)}",
                    Description = $"Test planet.",
                    LocationType = Enums.LocationType.Planet
                };

                var currentBody = new Models.SystemBody()
                {
                    GameDB = gameDB,
                    //BodyLocation = bodyLocation,
                    Name = $"Planet {GenerateName(randomData, 8)}",
                    Description = $"Test planet.",
                    OrbitRadius = (float)(randomData.NextDouble() + 0.5),
                    OrbitStart = (float)(randomData.NextDouble() * 360),
                    AtmosphereHeightKM = randomData.Next(40, 100)
                };

                context.SystemBodies.Add(currentBody);
                int numMoons = randomData.Next(1, 4);

                for (int moonNum = 0; moonNum < numMoons; moonNum++)
                {
                    var moonLocation = new Models.SystemLocation()
                    {
                        GameDB = gameDB,
                        ParentBody = bodyLocation,
                        ParentLocation = bodyLocation,
                        Name = $"Moon {GenerateName(randomData, 8)}",
                        Description = $"Test moon.",
                        LocationType = Enums.LocationType.Planet
                    };

                    var planetMoon = new Models.SystemBody()
                    {
                        GameDB = gameDB,
                        ParentBody = currentBody,
                        Name = $"Moon {GenerateName(randomData, 8)}",
                        Description = $"Test moon.",
                        OrbitRadius = RandFloat(randomData, 0.1f, 0.5f),
                        OrbitStart = (float)(randomData.NextDouble() * 360),
                        AtmosphereHeightKM = 0
                    };

                    context.SystemBodies.Add(planetMoon);
                    int numMoonStations = randomData.Next(0, 3);

                    for (int stationNum = 0; stationNum < numMoonStations; stationNum++)
                    {
                        var location = new Models.SystemLocation()
                        {
                            GameDB = gameDB,
                            ParentBody = moonLocation,
                            ParentLocation = moonLocation,
                            Name = $"Moon Base {stationNum}",
                            Description = "Moon Location",
                            LocationType = (Enums.LocationType)randomData.Next(0, 3),
                        };

                        var station = new Models.TradeShop()
                        {
                            GameDB = gameDB,
                            Location = location,
                            Name = $"Moon Station {GenerateName(randomData, 6)}",
                            Description = "Moon Station!",
                            ShopType = (Enums.ShopType)randomData.Next(0, 3),
                            TransitDurationS = randomData.Next(0, 1) == 0 ? 0 : randomData.Next(30, 300),
                            RunningDurationS = randomData.Next(30, 180)
                        };

                        context.Shops.Add(station);
                    }
                }

                int numStations = randomData.Next(0, 3);
                for (int stationNum = 0; stationNum < numStations; stationNum++)
                {
                    var location = new Models.SystemLocation()
                    {
                        GameDB = gameDB,
                        ParentBody = bodyLocation,
                        ParentLocation = bodyLocation,
                        Name = $"Planet Base {stationNum}",
                        Description = "Location",
                        LocationType = (Enums.LocationType)randomData.Next(0, 3),

                    };

                    var station = new Models.TradeShop()
                    {
                        GameDB = gameDB,
                        Location = location,
                        Name = $"Station {GenerateName(randomData, 10)}",
                        Description = "Test Station",
                        ShopType = (Enums.ShopType)randomData.Next(0, 3)
                    };

                    context.Shops.Add(station);
                }
            }

            context.SaveChanges();

            var categoryNames = Enumerable.Range(0, numCategories).Select(c => GenerateName(randomData, 10)).ToList();

            for (int itemNumber = 0; itemNumber < numItems; itemNumber++)
            {
                var item = new Models.TradeItem()
                {
                    GameDB = gameDB,
                    Name = GenerateName(randomData, 18),
                    Description = "Test Item",
                    Category = categoryNames[randomData.Next(0, categoryNames.Count)]
                };

                context.Items.Add(item);
            }

            context.SaveChanges();

            var manufacturerNames = Enumerable.Range(0, numManufacturer).Select(c => GenerateName(randomData, 12)).ToList();

            for (int shipNum = 0; shipNum < numShips;shipNum++)
            {
                var ship = new Models.GameShip()
                {
                    GameDB = gameDB,
                    Name = $"Ship {GenerateName(randomData, 16)}",
                    Description = "Ship",
                    Manufacturer = manufacturerNames[randomData.Next(0, manufacturerNames.Count)],
                    CargoCapacity = randomData.Next(0, 500),
                    QuantumDriveSize = randomData.Next(0, 4)
                };

                context.Ships.Add(ship);
            }

            context.SaveChanges();




            /**************** Trade Table Info ****************/

            var allStations = context.Shops.ToList();
            var allItems = context.Items.ToList();

            var tradeTable = new Models.TradeTable()
            {
                GameDatabase = gameDB,
                Name = "Default",
                Active = true
            };
            context.TradeTables.Add(tradeTable);
            context.SaveChanges();

            var itemPrices = allItems.ToDictionary(i => i.ID, i => RandFloat(randomData, 5, 250));

            for (int tradeNumber = 0; tradeNumber < numTrades; tradeNumber++)
            {
                var item = allItems[randomData.Next(0, allItems.Count)];

                var midPrice = itemPrices[item.ID];
                var buyType = randomData.Next(0, 2) == 0 ? Enums.TradeType.Buy : Enums.TradeType.Sell;

                var profitMarginRough = RandFloat(randomData, 0.05f, 0.50f);
                if (buyType == Enums.TradeType.Buy)
                    midPrice *= (1.0f - (profitMarginRough / 2));
                else
                    midPrice *= (1.0f + (profitMarginRough / 2));

                var trade = new Models.TradeTableEntry()
                {
                    Table = tradeTable,
                    Station = allStations[randomData.Next(0, allStations.Count)],
                    Item = item,
                    Type = buyType,
                    MinUnitPrice = midPrice * RandFloat(randomData, 0.9f, 1.0f),
                    MaxUnitPrice = midPrice * RandFloat(randomData, 1.0f, 1.1f),
                    UnitCapacityRate = (midPrice > 60) ? randomData.Next(1, 20) : randomData.Next(20, 2000),
                    UnitCapacityMax = (midPrice > 60) ? randomData.Next(20, 400) : randomData.Next(400, 80000)
                };

                if(context.TradeEntries.Any(te => 
                                te.Station == trade.Station && 
                                te.Item == trade.Item && 
                                te.Type != trade.Type))
                {
                    tradeNumber--;
                    continue;
                }

                context.TradeEntries.Add(trade);
            }


            /****** Mission Givers & Missions ********/

            for (int giverNum = 0; giverNum < numGivers; giverNum++)
            {
                var name = $"{GenerateName(randomData, 10)} {GenerateName(randomData, 15)}";

                var giver = new Models.MissionGiver()
                {
                    Name = name,
                    Description = "Test",
                    SystemLocation = allStations[randomData.Next(0, allStations.Count)].Location
                };

                context.MissionGivers.Add(giver);
            }

            var allGivers = context.MissionGivers.ToList();

            for (int missionNum = 0; missionNum < numMissions; missionNum++)
            {
                var name = $"Do the thing!";
                var needsLocation = randomData.Next(0, 2) == 1; // 50-50
                var giver = allGivers[randomData.Next(0, allGivers.Count)];

                var mission = new Models.MissionGiverEntry()
                {
                    Title = name,
                    Description = "Test",
                    Giver = giver,
                    AvailableLocation = needsLocation ? allStations[randomData.Next(0, allStations.Count)].Location : null,
                };

                context.Missions.Add(mission);
            }


            gameDB.FinishedImporting = true;
            context.SaveChanges();
        }

        public static float RandFloat(Random r, float min, float max)
        {
            return (float)(r.NextDouble() * (max - min)) + min;
        }

        public static string GenerateName(Random r, int len)
        {
            string[] consonants = { "b", "c", "d", "f", "g", "h", "j", "k", "l", "m", "l", "n", "p", "q", "r", "s", "sh", "zh", "t", "v", "w", "x" };
            string[] vowels = { "a", "e", "i", "o", "u", "ae", "y" };
            string Name = "";
            Name += consonants[r.Next(consonants.Length)].ToUpper();
            Name += vowels[r.Next(vowels.Length)];
            int b = 2; //b tells how many times a new letter has been added. It's 2 right now because the first two letters are already in the name.
            while (b < len)
            {
                Name += consonants[r.Next(consonants.Length)];
                b++;
                Name += vowels[r.Next(vowels.Length)];
                b++;
            }

            return Name;


        }
    }


}
