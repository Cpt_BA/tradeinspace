import bpy
import math
import bmesh
import struct

bm = bmesh.new()
bmesh.ops.create_cube(bm, size=1.0)

def create_cube(scene, xyz):
    mesh = bpy.data.meshes.new('Basic_Cube')
    basic_cube = bpy.data.objects.new("Basic_Cube", mesh)
    
    scene.objects.link(basic_cube)
    
    basic_cube.location = xyz
    
    bm.to_mesh(mesh)

def read_some_data(context, filepath, use_some_setting):
    print("\r\n" * 50)
    print("Importing: " + filepath)
    
    print("Cleaning")
    selected = 0
    for existing_object in context.scene.objects:
        if existing_object.type == 'MESH':
            existing_object.select = True
            selected += 1
            if selected > 1000:
                print("Removing 1k early")
                bpy.ops.object.delete()
                selected = 0
        else:
            existing_object.select = False
    
    bpy.ops.object.delete()
    print("Done Cleaning!")
    
    #return {'FINISHED'}
    
    with open(filepath, mode='rb') as file:
        magic = struct.unpack('L', file.read(4))[0]
        if(magic != 0xCAFEBABF):
            raise Exception("Invalid magic signature. - %s" % hex(magic));
        
        min_xyz = struct.unpack('f' * 3, file.read(4 * 3))
        max_xyz = struct.unpack('f' * 3, file.read(4 * 3))
        
        chunks = struct.unpack('LL', file.read(4 * 2))
        print("Bounds Min (%s)" % (','.join(map(str, min_xyz))))
        print("Bounds Max (%s)" % (','.join(map(str, max_xyz))))
        print("Chunks (%s)" % (','.join(map(str, chunks))))
        
        chunk_size_a = 128
        chunk_size_b = 64
        chunk_size_bit = 32
        
        for layer in range(0, 2):
            print("Layer:", layer + 1)
            
            unk = struct.unpack('f' * 3, file.read(4 * 3))
            layer_xyz = struct.unpack('L' * 3, file.read(4 * 3))
            unk2 = struct.unpack('f' * 3, file.read(4 * 3))
            unk3 = struct.unpack('f' * 3, file.read(4 * 3))
            
            print("Unk (%f, %f, %f)" % unk)
            print("Size (%i, %i, %i)" % layer_xyz)
            print("Unk2 (%f, %f, %f)" % unk2)
            print("Unk3 (%f, %f, %f)" % unk3)
            
            
            for grid_z in range(0, 5):
                for grid_y in range(0, 5):
                    for grid_x in range(0, 5):
                        create_cube(context.scene, (grid_x * chunk_size_a, grid_y * chunk_size_b, grid_z*chunk_size_bit))
            for chunk_z in range(0, layer_xyz[2]):
                for chunk_y in range(0, layer_xyz[1]):
                    for chunk_x in range(0, layer_xyz[0]):
                        found = 0
                        chunk_en = struct.unpack('L', file.read(4))[0]
                        print("Chunk (%i, %i, %i) enabled:%i" % (chunk_x, chunk_y, chunk_z, chunk_en))
                        if chunk_en == 1:
                            #create_cube(context.scene, (chunk_x * chunk_size_a, chunk_y * chunk_size_b, chunk_z*chunk_size_bit))
                            chunk_data = struct.unpack("L" * 0x2000, file.read(0x8000))
                            if(layer == 1):
                                continue
                            for data_index in range(0, 0x2000):
                                pos_a = data_index % chunk_size_a
                                pos_b = math.floor(data_index / chunk_size_a)
                                for bit in range(0, chunk_size_bit):
                                    if (chunk_data[data_index] & (1 << bit))  != 0:
                                        create_cube(context.scene, (
                                            (chunk_x * chunk_size_a) + pos_a, 
                                            (chunk_y * chunk_size_b) + pos_b, 
                                            ((chunk_z * chunk_size_bit) + bit) * ([1,-1][layer]) ))
                                        found += 1
                                        
                            print("Created", found, "blocks")
        
        remaining = file.read()
        print(len(remaining), "bytes remaining in file.")

    return {'FINISHED'}


# ImportHelper is a helper class, defines filename and
# invoke() function which calls the file selector.
from bpy_extras.io_utils import ImportHelper
from bpy.props import StringProperty, BoolProperty, EnumProperty
from bpy.types import Operator


class ImportSomeData(Operator, ImportHelper):
    """This appears in the tooltip of the operator and in the generated docs"""
    bl_idname = "import_test.some_data"  # important since its how bpy.ops.import_test.some_data is constructed
    bl_label = "Import Some Data"

    # ImportHelper mixin class uses this
    filename_ext = ".vvg"

    filter_glob = StringProperty(
            default="*.vvg",
            options={'HIDDEN'},
            maxlen=255,  # Max internal buffer length, longer would be clamped.
            )

    # List of operator properties, the attributes will be assigned
    # to the class instance from the operator settings before calling.
    use_setting = BoolProperty(
            name="Example Boolean",
            description="Example Tooltip",
            default=True,
            )

    type = EnumProperty(
            name="Example Enum",
            description="Choose between two items",
            items=(('OPT_A', "First Option", "Description one"),
                   ('OPT_B', "Second Option", "Description two")),
            default='OPT_A',
            )

    def execute(self, context):
        return read_some_data(context, self.filepath, self.use_setting)


# Only needed if you want to add into a dynamic menu
def menu_func_import(self, context):
    self.layout.operator(ImportSomeData.bl_idname, text="Vehicle Voxel Data (.vvg)")


def register():
    bpy.utils.register_class(ImportSomeData)
    bpy.types.INFO_MT_file_import.append(menu_func_import)


def unregister():
    bpy.utils.unregister_class(ImportSomeData)
    bpy.types.INFO_MT_file_import.remove(menu_func_import)


if __name__ == "__main__":
    register()

    # test call
    bpy.ops.import_test.some_data('INVOKE_DEFAULT')
