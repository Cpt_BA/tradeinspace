﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace TradeInSpace.Parse
{
    public enum ByteOrderEnum
    {
        AutoDetect,
        BigEndian,
        LittleEndian,
    }

    internal static class ParseHelper
    {

        public static String ReadCString(this BinaryReader binaryReader)
        {
            Int32 stringLength = 0;

            while (binaryReader.BaseStream.Position < binaryReader.BaseStream.Length && binaryReader.ReadChar() != 0)
                stringLength++;

            Int64 nul = binaryReader.BaseStream.Position;

            binaryReader.BaseStream.Seek(0 - stringLength - 1, SeekOrigin.Current);

            byte[] chars = binaryReader.ReadBytes(stringLength + 1);

            binaryReader.BaseStream.Seek(nul, SeekOrigin.Begin);

            // If there is actually a string to read
            if (stringLength > 0)
            {
                //return new String(chars, 0, stringLength).Replace("\u0000", "");
                return Encoding.UTF8.GetString(chars, 0, stringLength).Replace("\u0000", "");
            }

            return null;
        }

        public static string ReadFString(this BinaryReader br, int length)
        {
            char[] chars = br.ReadChars(length);

            for (Int32 i = 0; i < length; i++)
            {
                if (chars[i] == 0)
                {
                    return new string(chars, 0, i);
                }
            }

            return new string(chars);
        }

        private static byte[] _readBuffer = new byte[8];

        public static Int64 ReadInt64(this BinaryReader br, ByteOrderEnum byteOrder = ByteOrderEnum.BigEndian)
        {
            _readBuffer[byteOrder == ByteOrderEnum.LittleEndian ? 7 : 0] = br.ReadByte();
            _readBuffer[byteOrder == ByteOrderEnum.LittleEndian ? 6 : 1] = br.ReadByte();
            _readBuffer[byteOrder == ByteOrderEnum.LittleEndian ? 5 : 2] = br.ReadByte();
            _readBuffer[byteOrder == ByteOrderEnum.LittleEndian ? 4 : 3] = br.ReadByte();
            _readBuffer[byteOrder == ByteOrderEnum.LittleEndian ? 3 : 4] = br.ReadByte();
            _readBuffer[byteOrder == ByteOrderEnum.LittleEndian ? 2 : 5] = br.ReadByte();
            _readBuffer[byteOrder == ByteOrderEnum.LittleEndian ? 1 : 6] = br.ReadByte();
            _readBuffer[byteOrder == ByteOrderEnum.LittleEndian ? 0 : 7] = br.ReadByte();

            return BitConverter.ToInt64(_readBuffer, 0);
        }

        public static Int32 ReadInt32(this BinaryReader br, ByteOrderEnum byteOrder = ByteOrderEnum.BigEndian)
        {
            _readBuffer[byteOrder == ByteOrderEnum.LittleEndian ? 3 : 0] = br.ReadByte();
            _readBuffer[byteOrder == ByteOrderEnum.LittleEndian ? 2 : 1] = br.ReadByte();
            _readBuffer[byteOrder == ByteOrderEnum.LittleEndian ? 1 : 2] = br.ReadByte();
            _readBuffer[byteOrder == ByteOrderEnum.LittleEndian ? 0 : 3] = br.ReadByte();

            return BitConverter.ToInt32(_readBuffer, 0);
        }

        public static Int16 ReadInt16(this BinaryReader br, ByteOrderEnum byteOrder = ByteOrderEnum.BigEndian)
        {
            _readBuffer[byteOrder == ByteOrderEnum.LittleEndian ? 1 : 0] = br.ReadByte();
            _readBuffer[byteOrder == ByteOrderEnum.LittleEndian ? 0 : 1] = br.ReadByte();

            return BitConverter.ToInt16(_readBuffer, 0);
        }

        public static UInt64 ReadUInt64(this BinaryReader br, ByteOrderEnum byteOrder = ByteOrderEnum.BigEndian)
        {
            _readBuffer[byteOrder == ByteOrderEnum.LittleEndian ? 7 : 0] = br.ReadByte();
            _readBuffer[byteOrder == ByteOrderEnum.LittleEndian ? 6 : 1] = br.ReadByte();
            _readBuffer[byteOrder == ByteOrderEnum.LittleEndian ? 5 : 2] = br.ReadByte();
            _readBuffer[byteOrder == ByteOrderEnum.LittleEndian ? 4 : 3] = br.ReadByte();
            _readBuffer[byteOrder == ByteOrderEnum.LittleEndian ? 3 : 4] = br.ReadByte();
            _readBuffer[byteOrder == ByteOrderEnum.LittleEndian ? 2 : 5] = br.ReadByte();
            _readBuffer[byteOrder == ByteOrderEnum.LittleEndian ? 1 : 6] = br.ReadByte();
            _readBuffer[byteOrder == ByteOrderEnum.LittleEndian ? 0 : 7] = br.ReadByte();

            return BitConverter.ToUInt64(_readBuffer, 0);
        }

        public static UInt32 ReadUInt32(this BinaryReader br, ByteOrderEnum byteOrder = ByteOrderEnum.BigEndian)
        {
            _readBuffer[byteOrder == ByteOrderEnum.LittleEndian ? 3 : 0] = br.ReadByte();
            _readBuffer[byteOrder == ByteOrderEnum.LittleEndian ? 2 : 1] = br.ReadByte();
            _readBuffer[byteOrder == ByteOrderEnum.LittleEndian ? 1 : 2] = br.ReadByte();
            _readBuffer[byteOrder == ByteOrderEnum.LittleEndian ? 0 : 3] = br.ReadByte();

            return BitConverter.ToUInt32(_readBuffer, 0);
        }

        public static UInt16 ReadUInt16(this BinaryReader br, ByteOrderEnum byteOrder = ByteOrderEnum.BigEndian)
        {
            _readBuffer[byteOrder == ByteOrderEnum.LittleEndian ? 1 : 0] = br.ReadByte();
            _readBuffer[byteOrder == ByteOrderEnum.LittleEndian ? 0 : 1] = br.ReadByte();

            return BitConverter.ToUInt16(_readBuffer, 0);
        }
    }
}
