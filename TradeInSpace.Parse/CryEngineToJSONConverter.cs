﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace TradeInSpace.Parse
{
    public class CryEngineToJSONConverter
    {
        internal class CryXmlNode
        {
            public Int32 NodeID { get; set; }
            public Int32 NodeNameOffset { get; set; }
            public Int32 ContentOffset { get; set; }
            public Int16 AttributeCount { get; set; }
            public Int16 ChildCount { get; set; }
            public Int32 ParentNodeID { get; set; }
            public Int32 FirstAttributeIndex { get; set; }
            public Int32 FirstChildIndex { get; set; }
            public Int32 Reserved { get; set; }
        }

        public class CryXmlReference
        {
            public Int32 NameOffset { get; set; }
            public Int32 ValueOffset { get; set; }
        }

        public class CryXmlValue
        {
            public Int32 Offset { get; set; }
            public String Value { get; set; }
        }

        //I don't love this, but it's honestly not the worst solution....
        private static string[] ArrayTypeNames = new string[]
        {
            "Elem"
        };

        public static JObject ParseToJObject(Stream inStream, ByteOrderEnum byteOrder = ByteOrderEnum.BigEndian, Boolean writeLog = false)
        {
            using (BinaryReader br = new BinaryReader(inStream, Encoding.Default, true))
            {
                var peek = br.PeekChar();

                if (peek == '<')
                {
                    return null; // File is already XML
                }
                else if (peek != 'C')
                {
                    throw new FormatException("Unknown File Format"); // Unknown file format
                }

                var header = br.ReadFString(7);

                if (header == "CryXml" || header == "CryXmlB")
                {
                    br.ReadCString();
                }
                else if (header == "CRY3SDK")
                {
                    var bytes = br.ReadBytes(2);
                }
                else
                {
                    throw new FormatException("Unknown File Format");
                }

                var headerLength = br.BaseStream.Position;

                byteOrder = ByteOrderEnum.BigEndian;

                var fileLength = br.ReadInt32(byteOrder);

                if (fileLength != br.BaseStream.Length)
                {
                    br.BaseStream.Seek(headerLength, SeekOrigin.Begin);
                    byteOrder = ByteOrderEnum.LittleEndian;
                    fileLength = br.ReadInt32(byteOrder);
                }

                var nodeTableOffset = br.ReadInt32(byteOrder);
                var nodeTableCount = br.ReadInt32(byteOrder);
                var nodeTableSize = 28;

                var attributeTableOffset = br.ReadInt32(byteOrder);
                var attributeTableCount = br.ReadInt32(byteOrder);
                var referenceTableSize = 8;

                var childTableOffset = br.ReadInt32(byteOrder);
                var childTableCount = br.ReadInt32(byteOrder);
                var length3 = 4;

                var stringTableOffset = br.ReadInt32(byteOrder);
                var stringTableCount = br.ReadInt32(byteOrder);

                var nodeTable = new List<CryXmlNode> { };
                br.BaseStream.Seek(nodeTableOffset, SeekOrigin.Begin);
                Int32 nodeID = 0;
                while (br.BaseStream.Position < nodeTableOffset + nodeTableCount * nodeTableSize)
                {
                    var position = br.BaseStream.Position;
                    var value = new CryXmlNode
                    {
                        NodeID = nodeID++,
                        NodeNameOffset = br.ReadInt32(byteOrder),
                        ContentOffset = br.ReadInt32(byteOrder),
                        AttributeCount = br.ReadInt16(byteOrder),
                        ChildCount = br.ReadInt16(byteOrder),
                        ParentNodeID = br.ReadInt32(byteOrder),
                        FirstAttributeIndex = br.ReadInt32(byteOrder),
                        FirstChildIndex = br.ReadInt32(byteOrder),
                        Reserved = br.ReadInt32(byteOrder),
                    };

                    nodeTable.Add(value);
                }

                List<CryXmlReference> attributeTable = new List<CryXmlReference> { };
                br.BaseStream.Seek(attributeTableOffset, SeekOrigin.Begin);
                while (br.BaseStream.Position < attributeTableOffset + attributeTableCount * referenceTableSize)
                {
                    var position = br.BaseStream.Position;
                    var value = new CryXmlReference
                    {
                        NameOffset = br.ReadInt32(byteOrder),
                        ValueOffset = br.ReadInt32(byteOrder)
                    };

                    attributeTable.Add(value);
                }

                List<Int32> parentTable = new List<Int32> { };
                br.BaseStream.Seek(childTableOffset, SeekOrigin.Begin);
                while (br.BaseStream.Position < childTableOffset + childTableCount * length3)
                {
                    var position = br.BaseStream.Position;
                    var value = br.ReadInt32(byteOrder);

                    parentTable.Add(value);
                }

                List<CryXmlValue> dataTable = new List<CryXmlValue> { };
                br.BaseStream.Seek(stringTableOffset, SeekOrigin.Begin);
                while (br.BaseStream.Position < br.BaseStream.Length)
                {
                    var position = br.BaseStream.Position;
                    var value = new CryXmlValue
                    {
                        Offset = (Int32)position - stringTableOffset,
                        Value = br.ReadCString(),
                    };
                    dataTable.Add(value);
                }

                var dataMap = dataTable.ToDictionary(k => k.Offset, v => v.Value);

                var attributeIndex = 0;

                var bugged = false;

                Dictionary<Int32, JToken> objectMap = new Dictionary<Int32, JToken> { };

                JObject RootObject = new JObject();

                foreach (var node in nodeTable)
                {
                    JToken element = null;
                    var elementName = dataMap[node.NodeNameOffset];

                    if (node.AttributeCount != 0)
                    {
                        element = new JObject();

                        for (Int32 i = 0, j = node.AttributeCount; i < j; i++)
                        {
                            var attr = attributeTable[attributeIndex];
                            var attrName = dataMap[attr.NameOffset];
                            if (dataMap.ContainsKey(attr.ValueOffset))
                            {
                                element[attrName] = dataMap[attr.ValueOffset];
                            }
                            else
                            {
                                bugged = true;
                                element[attrName] = "BUGGED";
                            }
                            attributeIndex++;
                        }
                    }
                    else if (node.AttributeCount == 0 && node.ChildCount != 0)
                    {
                        element = new JObject();//new JArray();
                    }
                    else
                    {
                        element = new JObject();
                    }


                    objectMap[node.NodeID] = element;

                    if (dataMap.ContainsKey(node.ContentOffset))
                    {
                        if (!String.IsNullOrWhiteSpace(dataMap[node.ContentOffset]))
                        {
                            //element.AppendChild(xmlDoc.CreateCDataSection(dataMap[node.ContentOffset]));
                        }
                    }
                    else
                    {
                        bugged = true;
                        //element.AppendChild(xmlDoc.CreateCDataSection("BUGGED"));
                    }

                    if (objectMap.ContainsKey(node.ParentNodeID))
                    {
                        var parentToken = objectMap[node.ParentNodeID];
                        if (parentToken is JObject parentObj)
                        {
                            var targetName = string.IsNullOrWhiteSpace(elementName) ? "Children" : elementName;

                            //if (parentObj.GetValue(targetName) == null)
                            //    parentObj[targetName] = new JArray();
                            var parentVal = parentObj.GetValue(targetName);
                            if (parentVal != null)
                            {
                                if(parentVal is JObject existingValue)
                                {
                                    //I don't like dynamic conversion of object to a array here
                                    //but there is no way to know ahead of time if there is a conflict in names of children
                                    var newValue = new JArray(new object[] { existingValue, element });
                                    //Update the map and parentToken with the new array
                                    parentToken = objectMap[node.ParentNodeID] = newValue;
                                    //Replace the property on the object itself
                                    parentObj[targetName] = newValue;
                                } else if(parentVal is JArray parentPropertyArr)
                                {
                                    parentPropertyArr.Add(element);
                                }
                            }
                            else
                            {
                                if (ArrayTypeNames.Contains(targetName))
                                {
                                    parentObj[targetName] = objectMap[node.NodeID] = new JArray(element);
                                }
                                else
                                {
                                    parentObj[targetName] = element;
                                }
                            }

                            //parentObj.Value<JArray>(targetName).Add(element);
                        }
                        else if (parentToken is JArray parentArray)
                        {
                            parentArray.Add(element);
                        }
                        else
                        {
                            //WTF??
                        }
                    }
                    else
                    {
                        RootObject[elementName] = element;
                    }
                }

                return RootObject;
            }
        }
    }
}
