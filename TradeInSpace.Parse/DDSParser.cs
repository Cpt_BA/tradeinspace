﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Runtime.CompilerServices;

namespace TradeInSpace.Parse
{
    public class DDSParser
    {
        public static byte[] DecodeDDSData(DDSHeader Header, int FileIndex, byte[] InputData, out int BytesPerPixel, out int finalWidth, out int finalHeight)
        {
            int width = Header.Width, height = Header.Height;

            if (FileIndex != 0)
            {
                int mipScale = (Header.MipMapCount - 3) - FileIndex;

                width >>= mipScale;
                height >>= mipScale;
            }

            finalWidth = width;
            finalHeight = height;

            BytesPerPixel = Header.PixelFormat.Size;

            if (Header.DX10Header != null)
            {
                switch (Header.DX10Header.DxgiFormat)
                {
                    case DDSHeaderDX10.DX10_DXGIFormat.DXGI_FORMAT_BC4_SNORM:
                    case DDSHeaderDX10.DX10_DXGIFormat.DXGI_FORMAT_BC4_UNORM:
                    case DDSHeaderDX10.DX10_DXGIFormat.DXGI_FORMAT_BC4_TYPELESS:

                        return DDSDecoder.DecodeBC4(InputData, width, Header.DX10Header.DxgiFormat == DDSHeaderDX10.DX10_DXGIFormat.DXGI_FORMAT_BC4_SNORM);

                    case DDSHeaderDX10.DX10_DXGIFormat.DXGI_FORMAT_BC5_SNORM:
                    case DDSHeaderDX10.DX10_DXGIFormat.DXGI_FORMAT_BC5_UNORM:
                    case DDSHeaderDX10.DX10_DXGIFormat.DXGI_FORMAT_BC5_TYPELESS:
                        return DDSDecoder.DecodeBC5(InputData, width, Header.DX10Header.DxgiFormat == DDSHeaderDX10.DX10_DXGIFormat.DXGI_FORMAT_BC5_SNORM);

                    case DDSHeaderDX10.DX10_DXGIFormat.DXGI_FORMAT_R8_UNORM:

                        BytesPerPixel = 8;

                        return InputData;

                    case DDSHeaderDX10.DX10_DXGIFormat.DXGI_FORMAT_BC7_UNORM:

                        throw new InvalidDataException();

                        break;

                    default:

                        break;
                }
            }
            else
            {
                switch (Header.PixelFormat.FourCC)
                {
                    case "DXT1":
                        return DDSDecoder.DecodeBC1(InputData, width, false);

                    case "DXT5":
                        return DDSDecoder.DecodeBC3(InputData, width, false);

                    case "":
                        return InputData;

                    default:
                        break;
                }
            }

            return InputData;
        }

        public static byte[,] ExtractDDSChannel(byte[] FileData, int DataChannel)
        {
            var header = DDSHeader.Read(FileData);
            return ExtractDDSChannel(header, 0, header.AdditionalData, DataChannel);
        }

        public static byte[,] ExtractDDSChannel(DDSHeader Header, int FileIndex, byte[] InputData, int DataChannel)
        {
            int width = Header.Width, height = Header.Height;
            byte[] decodedData = DDSParser.DecodeDDSData(Header, FileIndex, InputData, out int bitsPerPixel, out width, out height);
            int bytesPerPixel = bitsPerPixel / 8;

            if (DataChannel >= bytesPerPixel)
                throw new ArgumentException("Not enough channels in source data", nameof(DataChannel));

            if (width * height * bytesPerPixel != decodedData.Length)
            {
                throw new InvalidDataException();
            }

            byte[,] outputData = new byte[width, height];

            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < height; y++)
                {
                    outputData[x, y] = decodedData[(y * width + x) * bytesPerPixel + DataChannel];
                }
            }

            return outputData;
        }
    }

    public class DDSDecoder
    {

        public static byte[] DecodeBC1(byte[] Input, int Width, bool SNORM = false)
        {
            //https://learn.microsoft.com/en-us/windows/win32/direct3d10/d3d10-graphics-programming-guide-resources-block-compression#bc1

            byte[] Output = new byte[Input.Length * 8]; //Input is 1byte-per-pixel, output is 4bytes-per-pixel

            uint[,] ColorBlock = new uint[4, 4];

            int ReadPosition = 0;
            int OutputBytesPerPixel = 4; //RGBA
            int BlockSize = 4;
            int OutputBlockIndex = 0;

            while (ReadPosition < Input.Length)
            {
                int blockRow = (OutputBlockIndex / (Width / BlockSize)) * BlockSize;
                int blockCol = (OutputBlockIndex % (Width / BlockSize)) * BlockSize;

                ReadRGB_565Block(ColorBlock, Input, ref ReadPosition);

                for (int i = 0; i < 16; i++)
                {
                    int row = (int)(i / 4);
                    int col = i % 4;

                    int outX = blockCol + col;
                    int outY = blockRow + row;

                    var outputOffset = (outY * Width + outX) * OutputBytesPerPixel;

                    uint pixelValue = ColorBlock[col, row];

                    Output[outputOffset + 0] = (byte)((pixelValue & 0xFF000000) >> 24); //R
                    Output[outputOffset + 1] = (byte)((pixelValue & 0x00FF0000) >> 16); //G
                    Output[outputOffset + 2] = (byte)((pixelValue & 0x0000FF00) >> 8); //B
                    Output[outputOffset + 3] = (byte)((pixelValue & 0x000000FF) >> 0); //A
                }

                OutputBlockIndex++;
            }

            return Output;
        }

        public static byte[] DecodeBC3(byte[] Input, int Width, bool SNORM = false)
        {
            //https://learn.microsoft.com/en-us/windows/win32/direct3d10/d3d10-graphics-programming-guide-resources-block-compression#bc3

            byte[] Output = new byte[Input.Length * 4]; //Input is 1byte-per-pixel, output is 4bytes-per-pixel

            byte[,] AlphaBlock = new byte[4, 4];
            uint[,] ColorBlock = new uint[4, 4];

            int ReadPosition = 0;
            int OutputBytesPerPixel = 4; //RGBA
            int BlockSize = 4;
            int OutputBlockIndex = 0;

            while (ReadPosition < Input.Length)
            {
                int blockRow = (OutputBlockIndex / (Width / BlockSize)) * BlockSize;
                int blockCol = (OutputBlockIndex % (Width / BlockSize)) * BlockSize;

                ReadUNormBlock(AlphaBlock, Input, ref ReadPosition);
                ReadRGB_565Block(ColorBlock, Input, ref ReadPosition);


                for (int i = 0; i < 16; i++)
                {
                    int row = (int)(i / 4);
                    int col = i % 4;

                    int outX = blockCol + col;
                    int outY = blockRow + row;

                    var outputOffset = (outY * Width + outX) * OutputBytesPerPixel;

                    uint pixelValue = ColorBlock[col, row];
                    pixelValue &= 0xFFFFFF00;
                    pixelValue |= AlphaBlock[col, row];

                    Output[outputOffset + 0] = (byte)((pixelValue & 0xFF000000) >> 24); //R
                    Output[outputOffset + 1] = (byte)((pixelValue & 0x00FF0000) >> 16); //G
                    Output[outputOffset + 2] = (byte)((pixelValue & 0x0000FF00) >> 8); //B
                    Output[outputOffset + 3] = (byte)((pixelValue & 0x000000FF) >> 0); //A
                }

                OutputBlockIndex++;
            }

            return Output;
        }

        public static byte[] DecodeBC4(byte[] Input, int Width, bool SNORM = false)
        {
            //https://learn.microsoft.com/en-us/windows/win32/direct3d10/d3d10-graphics-programming-guide-resources-block-compression#bc4

            byte[] Output = new byte[Input.Length * 8]; //Input is 0.5byte-per-pixel, output is 4bytes-per-pixel

            byte[,] BlockA = new byte[4, 4];

            int ReadPosition = 0;
            int OutputBytesPerPixel = 4; //RGBA
            int BlockSize = 4;
            int OutputBlockIndex = 0;

            while (ReadPosition < Input.Length)
            {
                int blockRow = (OutputBlockIndex / (Width / BlockSize)) * BlockSize;
                int blockCol = (OutputBlockIndex % (Width / BlockSize)) * BlockSize;

                if (SNORM)
                {
                    ReadSNormBlock(BlockA, Input, ref ReadPosition);
                }
                else
                {
                    ReadUNormBlock(BlockA, Input, ref ReadPosition);
                }

                for (int i = 0; i < 16; i++)
                {
                    int row = (int)(i / 4);
                    int col = i % 4;

                    int outX = blockCol + col;
                    int outY = blockRow + row;

                    var outputOffset = (outY * Width + outX) * OutputBytesPerPixel;

                    Output[outputOffset + 0] = 0;   //B
                    Output[outputOffset + 1] = 0;   //G
                    Output[outputOffset + 2] = BlockA[col, row];   //R
                    Output[outputOffset + 3] = 255; //A
                }

                OutputBlockIndex++;
            }

            return Output;
        }

        public static byte[] DecodeBC5(byte[] Input, int Width, bool SNORM = false)
        {
            //https://learn.microsoft.com/en-us/windows/win32/direct3d10/d3d10-graphics-programming-guide-resources-block-compression#bc5

            byte[] Output = new byte[Input.Length * 4]; //Input is 1byte-per-pixel, output is 4bytes-per-pixel

            byte[,] BlockA = new byte[4, 4];
            byte[,] BlockB = new byte[4, 4];

            int ReadPosition = 0;
            int OutputBytesPerPixel = 4; //RGBA
            int BlockSize = 4;
            int OutputBlockIndex = 0;

            while (ReadPosition < Input.Length)
            {
                int blockRow = (OutputBlockIndex / (Width / BlockSize)) * BlockSize;
                int blockCol = (OutputBlockIndex % (Width / BlockSize)) * BlockSize;

                if (SNORM)
                {
                    ReadSNormBlock(BlockA, Input, ref ReadPosition);
                    ReadSNormBlock(BlockB, Input, ref ReadPosition);
                }
                else
                {
                    ReadUNormBlock(BlockA, Input, ref ReadPosition);
                    ReadUNormBlock(BlockB, Input, ref ReadPosition);
                }

                for (int i = 0; i < 16; i++)
                {
                    int row = (int)(i / 4);
                    int col = i % 4;

                    int outX = blockCol + col;
                    int outY = blockRow + row;

                    var outputOffset = (outY * Width + outX) * OutputBytesPerPixel;

                    Output[outputOffset + 0] = 0;   //B
                    Output[outputOffset + 1] = BlockB[col, row];   //G
                    Output[outputOffset + 2] = BlockA[col, row];   //R
                    Output[outputOffset + 3] = 255; //A
                }

                OutputBlockIndex++;
            }
            
            return Output;
        }

        //public static byte[] DecodeBC7(byte[] Input, int Width, bool SNORM = false)
        //{
        //    byte GetBit(ref uint uStartBit)
        //    {
        //        uint uIndex = uStartBit >> 3;
        //        var ret = (byte)((currentBlock[uIndex] >> (int)(uStartBit - (uIndex << 3))) & 0x01);
        //        uStartBit++;
        //        return ret;
        //    }
        //    byte GetBits(ref uint uStartBit, uint uNumBits)
        //    {
        //        if (uNumBits == 0) return 0;
        //        byte ret;
        //        uint uIndex = uStartBit >> 3;
        //        uint uBase = uStartBit - (uIndex << 3);
        //        if (uBase + uNumBits > 8)
        //        {
        //            uint uFirstIndexBits = 8 - uBase;
        //            uint uNextIndexBits = uNumBits - uFirstIndexBits;
        //            ret = (byte)((uint)(currentBlock[uIndex] >> (int)uBase) | ((currentBlock[uIndex + 1] & ((1u << (int)uNextIndexBits) - 1)) << (int)uFirstIndexBits));
        //        }
        //        else
        //        {
        //            ret = (byte)((currentBlock[uIndex] >> (int)uBase) & ((1 << (int)uNumBits) - 1));
        //        }
        //        uStartBit += uNumBits;
        //        return ret;
        //    }

        //    byte Unquantize(byte comp, uint uPrec)
        //    {
        //        comp = (byte)(comp << (int)(8u - uPrec));
        //        return (byte)(comp | (comp >> (int)uPrec));
        //    }

        //    uint uFirst = 0;
        //    while (uFirst < 128 && GetBit(ref uFirst) == 0) { }
        //    byte uMode = (byte)(uFirst - 1);

        //    if (uMode < 8)
        //    {
        //        byte uPartitions = ms_aInfo[uMode].uPartitions;
        //        Debug.Assert(uPartitions < Constants.BC7_MAX_REGIONS);

        //        var uNumEndPts = (byte)((uPartitions + 1u) << 1);
        //        byte uIndexPrec = ms_aInfo[uMode].uIndexPrec;
        //        byte uIndexPrec2 = ms_aInfo[uMode].uIndexPrec2;
        //        int i;
        //        uint uStartBit = uMode + 1u;
        //        int[] P = new int[6];
        //        byte uShape = GetBits(ref uStartBit, ms_aInfo[uMode].uPartitionBits);
        //        byte uRotation = GetBits(ref uStartBit, ms_aInfo[uMode].uRotationBits);
        //        byte uIndexMode = GetBits(ref uStartBit, ms_aInfo[uMode].uIndexModeBits);

        //        LDRColorA[] c = new LDRColorA[Constants.BC7_MAX_REGIONS << 1];
        //        for (i = 0; i < c.Length; ++i) c[i] = new LDRColorA();
        //        LDRColorA RGBAPrec = ms_aInfo[uMode].RGBAPrec;
        //        LDRColorA RGBAPrecWithP = ms_aInfo[uMode].RGBAPrecWithP;

        //        Debug.Assert(uNumEndPts <= (Constants.BC7_MAX_REGIONS << 1));

        //        // Red channel
        //        for (i = 0; i < uNumEndPts; i++)
        //        {
        //            if (uStartBit + RGBAPrec.r > 128)
        //            {
        //                Debug.WriteLine("BC7: Invalid block encountered during decoding");
        //                Helpers.FillWithErrorColors(data, ref dataIndex, Constants.NUM_PIXELS_PER_BLOCK, DivSize, stride);
        //                return streamIndex;
        //            }

        //            c[i].r = GetBits(ref uStartBit, RGBAPrec.r);
        //        }

        //        // Green channel
        //        for (i = 0; i < uNumEndPts; i++)
        //        {
        //            if (uStartBit + RGBAPrec.g > 128)
        //            {
        //                Debug.WriteLine("BC7: Invalid block encountered during decoding");
        //                Helpers.FillWithErrorColors(data, ref dataIndex, Constants.NUM_PIXELS_PER_BLOCK, DivSize, stride);
        //                return streamIndex;
        //            }

        //            c[i].g = GetBits(ref uStartBit, RGBAPrec.g);
        //        }

        //        // Blue channel
        //        for (i = 0; i < uNumEndPts; i++)
        //        {
        //            if (uStartBit + RGBAPrec.b > 128)
        //            {
        //                Debug.WriteLine("BC7: Invalid block encountered during decoding");
        //                Helpers.FillWithErrorColors(data, ref dataIndex, Constants.NUM_PIXELS_PER_BLOCK, DivSize, stride);
        //                return streamIndex;
        //            }

        //            c[i].b = GetBits(ref uStartBit, RGBAPrec.b);
        //        }

        //        // Alpha channel
        //        for (i = 0; i < uNumEndPts; i++)
        //        {
        //            if (uStartBit + RGBAPrec.a > 128)
        //            {
        //                Debug.WriteLine("BC7: Invalid block encountered during decoding");
        //                Helpers.FillWithErrorColors(data, ref dataIndex, Constants.NUM_PIXELS_PER_BLOCK, DivSize, stride);
        //                return streamIndex;
        //            }

        //            c[i].a = (byte)(RGBAPrec.a != 0 ? GetBits(ref uStartBit, RGBAPrec.a) : 255u);
        //        }

        //        // P-bits
        //        Debug.Assert(ms_aInfo[uMode].uPBits <= 6);
        //        for (i = 0; i < ms_aInfo[uMode].uPBits; i++)
        //        {
        //            if (uStartBit > 127)
        //            {
        //                Debug.WriteLine("BC7: Invalid block encountered during decoding");
        //                Helpers.FillWithErrorColors(data, ref dataIndex, Constants.NUM_PIXELS_PER_BLOCK, DivSize, stride);
        //                return streamIndex;
        //            }

        //            P[i] = GetBit(ref uStartBit);
        //        }

        //        if (ms_aInfo[uMode].uPBits != 0)
        //        {
        //            for (i = 0; i < uNumEndPts; i++)
        //            {
        //                int pi = i * ms_aInfo[uMode].uPBits / uNumEndPts;
        //                for (byte ch = 0; ch < Constants.BC7_NUM_CHANNELS; ch++)
        //                {
        //                    if (RGBAPrec[ch] != RGBAPrecWithP[ch])
        //                    {
        //                        c[i][ch] = (byte)((c[i][ch] << 1) | P[pi]);
        //                    }
        //                }
        //            }
        //        }

        //        for (i = 0; i < uNumEndPts; i++)
        //        {
        //            c[i] = Unquantize(c[i], RGBAPrecWithP);
        //        }

        //        byte[] w1 = new byte[Constants.NUM_PIXELS_PER_BLOCK], w2 = new byte[Constants.NUM_PIXELS_PER_BLOCK];

        //        // read color indices
        //        for (i = 0; i < Constants.NUM_PIXELS_PER_BLOCK; i++)
        //        {
        //            uint uNumBits = Helpers.IsFixUpOffset(ms_aInfo[uMode].uPartitions, uShape, i) ? uIndexPrec - 1u : uIndexPrec;
        //            if (uStartBit + uNumBits > 128)
        //            {
        //                Debug.WriteLine("BC7: Invalid block encountered during decoding");
        //                Helpers.FillWithErrorColors(data, ref dataIndex, Constants.NUM_PIXELS_PER_BLOCK, DivSize, stride);
        //                return streamIndex;
        //            }
        //            w1[i] = GetBits(ref uStartBit, uNumBits);
        //        }

        //        // read alpha indices
        //        if (uIndexPrec2 != 0)
        //        {
        //            for (i = 0; i < Constants.NUM_PIXELS_PER_BLOCK; i++)
        //            {
        //                uint uNumBits = (i != 0 ? uIndexPrec2 : uIndexPrec2 - 1u);
        //                if (uStartBit + uNumBits > 128)
        //                {
        //                    Debug.WriteLine("BC7: Invalid block encountered during decoding");
        //                    Helpers.FillWithErrorColors(data, ref dataIndex, Constants.NUM_PIXELS_PER_BLOCK, DivSize, stride);
        //                    return streamIndex;
        //                }
        //                w2[i] = GetBits(ref uStartBit, uNumBits);
        //            }
        //        }

        //        for (i = 0; i < Constants.NUM_PIXELS_PER_BLOCK; ++i)
        //        {
        //            byte uRegion = Constants.g_aPartitionTable[uPartitions][uShape][i];
        //            LDRColorA outPixel = new LDRColorA();
        //            if (uIndexPrec2 == 0)
        //            {
        //                LDRColorA.Interpolate(c[uRegion << 1], c[(uRegion << 1) + 1], w1[i], w1[i], uIndexPrec, uIndexPrec, outPixel);
        //            }
        //            else
        //            {
        //                if (uIndexMode == 0)
        //                {
        //                    LDRColorA.Interpolate(c[uRegion << 1], c[(uRegion << 1) + 1], w1[i], w2[i], uIndexPrec, uIndexPrec2, outPixel);
        //                }
        //                else
        //                {
        //                    LDRColorA.Interpolate(c[uRegion << 1], c[(uRegion << 1) + 1], w2[i], w1[i], uIndexPrec2, uIndexPrec, outPixel);
        //                }
        //            }

        //            switch (uRotation)
        //            {
        //                case 1: ByteSwap(ref outPixel.r, ref outPixel.a); break;
        //                case 2: ByteSwap(ref outPixel.g, ref outPixel.a); break;
        //                case 3: ByteSwap(ref outPixel.b, ref outPixel.a); break;
        //            }

        //            // Note: whether it's sRGB is not taken into consideration
        //            // we're returning data that could be either/or depending 
        //            // on the input BC7 format
        //            data[dataIndex++] = outPixel.b;
        //            data[dataIndex++] = outPixel.g;
        //            data[dataIndex++] = outPixel.r;
        //            data[dataIndex++] = outPixel.a;

        //            // Is mult 4?
        //            if (((i + 1) & 0x3) == 0)
        //                dataIndex += PixelDepthBytes * (stride - DivSize);
        //        }
        //    }
        //    else
        //    {
        //        Debug.WriteLine("BC7: Reserved mode 8 encountered during decoding");
        //        // Per the BC7 format spec, we must return transparent black
        //        for (int i = 0; i < Constants.NUM_PIXELS_PER_BLOCK; ++i)
        //        {
        //            data[dataIndex++] = 0;
        //            data[dataIndex++] = 0;
        //            data[dataIndex++] = 0;
        //            data[dataIndex++] = 0;

        //            // Is mult 4?
        //            if (((i + 1) & 0x3) == 0)
        //                dataIndex += PixelDepthBytes * (stride - DivSize);
        //        }
        //    }

        //    return streamIndex;
        //}






        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        internal static byte InterpolateColorU(byte index, byte red0, byte red1)
        {
            byte red;
            if (index == 0)
            {
                red = red0;
            }
            else if (index == 1)
            {
                red = red1;
            }
            else
            {
                if (red0 > red1)
                {
                    index -= 1;
                    red = (byte)(((red0 * (7 - index) + red1 * index) / 7.0f) + 0.5f);
                }
                else
                {
                    if (index == 6)
                    {
                        red = 0;
                    }
                    else if (index == 7)
                    {
                        red = 255;
                    }
                    else
                    {
                        index -= 1;
                        red = (byte)(((red0 * (5 - index) + red1 * index) / 5.0f) + 0.5f);
                    }
                }
            }
            return red;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        internal static byte InterpolateColorS(byte index, sbyte red0, sbyte red1)
        {
            float red;
            if (index == 0)
            {
                red = red0;
            }
            else if (index == 1)
            {
                red = red1;
            }
            else
            {
                if (red0 > red1)
                {
                    index -= 1;
                    red = (red0 * (7 - index) + red1 * index) / 7.0f;
                }
                else
                {
                    if (index == 6)
                    {
                        red = -127.0f;
                    }
                    else if (index == 7)
                    {
                        red = 127.0f;
                    }
                    else
                    {
                        index -= 1;
                        red = (red0 * (5 - index) + red1 * index) / 5.0f;
                    }
                }
            }
            return (byte)(((red + 127) * (255.0f / 254.0f)) + 0.5f);
        }

        static uint[] _ColorLUT = new uint[4];
        internal static void ReadRGB_565Block(uint[,] BlockData, byte[] Input, ref int ReadPosition)
        {
            DDSColorInterpolate ReadRGB565(ref int _ReadPosition)
            {
                ushort rgb565 = BitConverter.ToUInt16(Input, _ReadPosition);
                _ReadPosition += 2;
                return DDSColorInterpolate.FromRgb565(rgb565);
            }

            var colorA = ReadRGB565(ref ReadPosition);
            var colorB = ReadRGB565(ref ReadPosition);

            _ColorLUT[0] = colorA.AsRGBA32Int();
            _ColorLUT[1] = colorB.AsRGBA32Int();
            _ColorLUT[2] = colorA.Lerp(colorB, 1f / 3f).AsRGBA32Int();
            _ColorLUT[3] = colorA.Lerp(colorB, 2f / 3f).AsRGBA32Int();

            uint ColorValues = BitConverter.ToUInt32(Input, ReadPosition);
            ReadPosition += 4;

            for (int i = 0; i < 16; i++)
            {
                int row = (int)(i / 4);
                int col = i % 4;
                int offsetBits = i * 2; //2 bits per pixel
                byte colorIndex = (byte)((ColorValues >> offsetBits) & 0x03);

                BlockData[col, row] = _ColorLUT[colorIndex];
            }
        }

        internal static ulong ReadIndexes(byte[] Input, ref int ReadPosition)
        {
            ulong result = 0;

            for (int i = 0; i < 6; i++)
                result |= ((ulong)Input[ReadPosition + i]) << (i * 8);

            ReadPosition += 6;

            return result;
        }

        internal static void ReadUNormBlock(byte[,] BlockData, byte[] Input, ref int Position)
        {
            byte gradientA = Input[Position];
            byte gradientB = Input[Position + 1];
            Position += 2;

            ulong completeValue = ReadIndexes(Input, ref Position);

            for (int i = 0; i < 16; i++)
            {
                int row = (int)(i / 4);
                int col = i % 4;
                int offsetBits = i * 3; //3 bits per pixel
                byte pixelIndex = (byte)((completeValue >> offsetBits) & 0x07);

                BlockData[col, row] = InterpolateColorU(pixelIndex, gradientA, gradientB);
            }
        }

        internal static void ReadSNormBlock(byte[,] BlockData, byte[] Input, ref int Position)
        {
            sbyte gradientA = (sbyte)Input[Position];
            sbyte gradientB = (sbyte)Input[Position + 1];
            Position += 2;

            gradientA = (gradientA == -128) ? (sbyte)-127 : gradientA;
            gradientB = (gradientB == -128) ? (sbyte)-127 : gradientB;

            ulong completeValue = ReadIndexes(Input, ref Position);

            for (int i = 0; i < 16; i++)
            {
                int row = (int)(i / 4);
                int col = i % 4;
                int offsetBits = i * 3; //3 bits per pixel
                byte pixelIndex = (byte)((completeValue >> offsetBits) & 0x07);

                BlockData[col, row] = InterpolateColorS(pixelIndex, gradientA, gradientB);
            }
        }
    }

    internal class DDSColorInterpolate
    {
        public float r;
        public float g;
        public float b;

        public DDSColorInterpolate(float r, float g, float b)
        {
            this.r = r;
            this.g = g;
            this.b = b;
        }

        public static DDSColorInterpolate FromRgb565(ushort rgb565)
        {
            const float F5 = 255f / 31f;
            const float F6 = 255f / 63f;

            return new DDSColorInterpolate(
                ((rgb565 & 0x1f)) * F5,
                ((rgb565 & 0x7E0) >> 5) * F6,
                ((rgb565 & 0xF800) >> 11) * F5
            );
        }

        public DDSColorInterpolate Lerp(DDSColorInterpolate other, float blend) =>
            new DDSColorInterpolate(
                r + blend * (other.r - r),
                g + blend * (other.g - g),
                b + blend * (other.b - b));

        public (byte r, byte g, byte b) As8Bit() =>
        (
            (byte)(r + 0.5f),
            (byte)(g + 0.5f),
            (byte)(b + 0.5f)
        );

        public (byte r, byte g, byte b, byte a) As8BitA() =>
        (
            (byte)(r + 0.5f),
            (byte)(g + 0.5f),
            (byte)(b + 0.5f),
            255
        );

        public uint AsRGBA32Int() {
            var rgba = As8BitA();
            return (uint)(rgba.r << 24 | rgba.g << 16 | rgba.b << 8 | rgba.a);
        }
    }

    public class DDSHeader
    {
        public int Size;
        public int Flags;
        public int Height;
        public int Width;
        public int PitchOrLinearSize;
        public int Depth;
        public int MipMapCount;
        private int[] Reserved1;
        public DDSPixelFormat PixelFormat;
        public int Caps;
        public int Caps2;
        public int Caps3;
        public int Caps4;
        public int Reserved2;
        public DDSHeaderDX10 DX10Header;

        public int HeaderLength;

        public byte[] AdditionalData;

        public static DDSHeader Read(byte[] data)
        {
            using (var ms = new MemoryStream(data))
            using (var bs = new BinaryReader(ms))
                return Read(bs);
        }

        public static DDSHeader Read(BinaryReader Reader)
        {
            DDSHeader DDSHeader = new DDSHeader();

            var magic = Reader.ReadFString(4);
            if (magic != "DDS ")
                throw new InvalidDataException();

            DDSHeader.Size = Reader.ReadInt32();
            DDSHeader.Flags = Reader.ReadInt32();
            DDSHeader.Height = Reader.ReadInt32();
            DDSHeader.Width = Reader.ReadInt32();
            DDSHeader.PitchOrLinearSize = Reader.ReadInt32();
            DDSHeader.Depth = Reader.ReadInt32();
            DDSHeader.MipMapCount = Reader.ReadInt32();
            
            DDSHeader.Reserved1 = new int[11];
            for(int i = 0; i < 11; i++)
                DDSHeader.Reserved1[i] = Reader.ReadInt32();
            
            DDSHeader.PixelFormat = DDSPixelFormat.Read(Reader);
            DDSHeader.Caps = Reader.ReadInt32();
            DDSHeader.Caps2 = Reader.ReadInt32();
            DDSHeader.Caps3 = Reader.ReadInt32();
            DDSHeader.Caps4 = Reader.ReadInt32();
            DDSHeader.Reserved2 = Reader.ReadInt32();

            if(DDSHeader.PixelFormat.FourCC == "DX10")
                DDSHeader.DX10Header = DDSHeaderDX10.Read(Reader);

            DDSHeader.HeaderLength = (int)Reader.BaseStream.Position;

            if (DDSHeader.HeaderLength < Reader.BaseStream.Length - 1)
            {
                DDSHeader.AdditionalData = Reader.ReadBytes((int)(Reader.BaseStream.Length - DDSHeader.HeaderLength));
            }

            return DDSHeader;
        }
    }

    public class DDSPixelFormat
    {
        public int Size;
        public int Flags;
        public string FourCC;
        public int RGBBitCount;
        public int RBitMask;
        public int GBitMask;
        public int BBitMask;
        public int ABitMask;

        internal static DDSPixelFormat Read(BinaryReader reader)
        {
            DDSPixelFormat format = new DDSPixelFormat();

            format.Size = reader.ReadInt32();
            format.Flags = reader.ReadInt32();
            format.FourCC =  reader.ReadFString(4);
            format.RGBBitCount = reader.ReadInt32(); 
            format.RBitMask = reader.ReadInt32();
            format.GBitMask = reader.ReadInt32();
            format.BBitMask = reader.ReadInt32();
            format.ABitMask = reader.ReadInt32();

            return format;
        }
    }

    public class DDSHeaderDX10
    {
        public DX10_DXGIFormat DxgiFormat;
        public DX10_ResourceDimension ResourceDimension;
        public int MiscFlag;
        public int ArraySize;
        public int MiscFlags2;

        internal static DDSHeaderDX10 Read(BinaryReader reader)
        {
            DDSHeaderDX10 dx10 = new DDSHeaderDX10();

            dx10.DxgiFormat = (DX10_DXGIFormat)reader.ReadUInt32();
            dx10.ResourceDimension = (DX10_ResourceDimension)reader.ReadUInt32();
            dx10.MiscFlag = reader.ReadInt32();
            dx10.ArraySize = reader.ReadInt32();
            dx10.MiscFlags2 = reader.ReadInt32();

            return dx10;
        }

        public enum DX10_DXGIFormat : uint
        {
            DXGI_FORMAT_UNKNOWN = 0,
            DXGI_FORMAT_R32G32B32A32_TYPELESS = 1,
            DXGI_FORMAT_R32G32B32A32_FLOAT = 2,
            DXGI_FORMAT_R32G32B32A32_UINT = 3,
            DXGI_FORMAT_R32G32B32A32_SINT = 4,
            DXGI_FORMAT_R32G32B32_TYPELESS = 5,
            DXGI_FORMAT_R32G32B32_FLOAT = 6,
            DXGI_FORMAT_R32G32B32_UINT = 7,
            DXGI_FORMAT_R32G32B32_SINT = 8,
            DXGI_FORMAT_R16G16B16A16_TYPELESS = 9,
            DXGI_FORMAT_R16G16B16A16_FLOAT = 10,
            DXGI_FORMAT_R16G16B16A16_UNORM = 11,
            DXGI_FORMAT_R16G16B16A16_UINT = 12,
            DXGI_FORMAT_R16G16B16A16_SNORM = 13,
            DXGI_FORMAT_R16G16B16A16_SINT = 14,
            DXGI_FORMAT_R32G32_TYPELESS = 15,
            DXGI_FORMAT_R32G32_FLOAT = 16,
            DXGI_FORMAT_R32G32_UINT = 17,
            DXGI_FORMAT_R32G32_SINT = 18,
            DXGI_FORMAT_R32G8X24_TYPELESS = 19,
            DXGI_FORMAT_D32_FLOAT_S8X24_UINT = 20,
            DXGI_FORMAT_R32_FLOAT_X8X24_TYPELESS = 21,
            DXGI_FORMAT_X32_TYPELESS_G8X24_UINT = 22,
            DXGI_FORMAT_R10G10B10A2_TYPELESS = 23,
            DXGI_FORMAT_R10G10B10A2_UNORM = 24,
            DXGI_FORMAT_R10G10B10A2_UINT = 25,
            DXGI_FORMAT_R11G11B10_FLOAT = 26,
            DXGI_FORMAT_R8G8B8A8_TYPELESS = 27,
            DXGI_FORMAT_R8G8B8A8_UNORM = 28,
            DXGI_FORMAT_R8G8B8A8_UNORM_SRGB = 29,
            DXGI_FORMAT_R8G8B8A8_UINT = 30,
            DXGI_FORMAT_R8G8B8A8_SNORM = 31,
            DXGI_FORMAT_R8G8B8A8_SINT = 32,
            DXGI_FORMAT_R16G16_TYPELESS = 33,
            DXGI_FORMAT_R16G16_FLOAT = 34,
            DXGI_FORMAT_R16G16_UNORM = 35,
            DXGI_FORMAT_R16G16_UINT = 36,
            DXGI_FORMAT_R16G16_SNORM = 37,
            DXGI_FORMAT_R16G16_SINT = 38,
            DXGI_FORMAT_R32_TYPELESS = 39,
            DXGI_FORMAT_D32_FLOAT = 40,
            DXGI_FORMAT_R32_FLOAT = 41,
            DXGI_FORMAT_R32_UINT = 42,
            DXGI_FORMAT_R32_SINT = 43,
            DXGI_FORMAT_R24G8_TYPELESS = 44,
            DXGI_FORMAT_D24_UNORM_S8_UINT = 45,
            DXGI_FORMAT_R24_UNORM_X8_TYPELESS = 46,
            DXGI_FORMAT_X24_TYPELESS_G8_UINT = 47,
            DXGI_FORMAT_R8G8_TYPELESS = 48,
            DXGI_FORMAT_R8G8_UNORM = 49,
            DXGI_FORMAT_R8G8_UINT = 50,
            DXGI_FORMAT_R8G8_SNORM = 51,
            DXGI_FORMAT_R8G8_SINT = 52,
            DXGI_FORMAT_R16_TYPELESS = 53,
            DXGI_FORMAT_R16_FLOAT = 54,
            DXGI_FORMAT_D16_UNORM = 55,
            DXGI_FORMAT_R16_UNORM = 56,
            DXGI_FORMAT_R16_UINT = 57,
            DXGI_FORMAT_R16_SNORM = 58,
            DXGI_FORMAT_R16_SINT = 59,
            DXGI_FORMAT_R8_TYPELESS = 60,
            DXGI_FORMAT_R8_UNORM = 61,
            DXGI_FORMAT_R8_UINT = 62,
            DXGI_FORMAT_R8_SNORM = 63,
            DXGI_FORMAT_R8_SINT = 64,
            DXGI_FORMAT_A8_UNORM = 65,
            DXGI_FORMAT_R1_UNORM = 66,
            DXGI_FORMAT_R9G9B9E5_SHAREDEXP = 67,
            DXGI_FORMAT_R8G8_B8G8_UNORM = 68,
            DXGI_FORMAT_G8R8_G8B8_UNORM = 69,
            DXGI_FORMAT_BC1_TYPELESS = 70,
            DXGI_FORMAT_BC1_UNORM = 71,
            DXGI_FORMAT_BC1_UNORM_SRGB = 72,
            DXGI_FORMAT_BC2_TYPELESS = 73,
            DXGI_FORMAT_BC2_UNORM = 74,
            DXGI_FORMAT_BC2_UNORM_SRGB = 75,
            DXGI_FORMAT_BC3_TYPELESS = 76,
            DXGI_FORMAT_BC3_UNORM = 77,
            DXGI_FORMAT_BC3_UNORM_SRGB = 78,
            DXGI_FORMAT_BC4_TYPELESS = 79,
            DXGI_FORMAT_BC4_UNORM = 80,
            DXGI_FORMAT_BC4_SNORM = 81,
            DXGI_FORMAT_BC5_TYPELESS = 82,
            DXGI_FORMAT_BC5_UNORM = 83,
            DXGI_FORMAT_BC5_SNORM = 84,
            DXGI_FORMAT_B5G6R5_UNORM = 85,
            DXGI_FORMAT_B5G5R5A1_UNORM = 86,
            DXGI_FORMAT_B8G8R8A8_UNORM = 87,
            DXGI_FORMAT_B8G8R8X8_UNORM = 88,
            DXGI_FORMAT_R10G10B10_XR_BIAS_A2_UNORM = 89,
            DXGI_FORMAT_B8G8R8A8_TYPELESS = 90,
            DXGI_FORMAT_B8G8R8A8_UNORM_SRGB = 91,
            DXGI_FORMAT_B8G8R8X8_TYPELESS = 92,
            DXGI_FORMAT_B8G8R8X8_UNORM_SRGB = 93,
            DXGI_FORMAT_BC6H_TYPELESS = 94,
            DXGI_FORMAT_BC6H_UF16 = 95,
            DXGI_FORMAT_BC6H_SF16 = 96,
            DXGI_FORMAT_BC7_TYPELESS = 97,
            DXGI_FORMAT_BC7_UNORM = 98,
            DXGI_FORMAT_BC7_UNORM_SRGB = 99,
            DXGI_FORMAT_AYUV = 100,
            DXGI_FORMAT_Y410 = 101,
            DXGI_FORMAT_Y416 = 102,
            DXGI_FORMAT_NV12 = 103,
            DXGI_FORMAT_P010 = 104,
            DXGI_FORMAT_P016 = 105,
            DXGI_FORMAT_420_OPAQUE = 106,
            DXGI_FORMAT_YUY2 = 107,
            DXGI_FORMAT_Y210 = 108,
            DXGI_FORMAT_Y216 = 109,
            DXGI_FORMAT_NV11 = 110,
            DXGI_FORMAT_AI44 = 111,
            DXGI_FORMAT_IA44 = 112,
            DXGI_FORMAT_P8 = 113,
            DXGI_FORMAT_A8P8 = 114,
            DXGI_FORMAT_B4G4R4A4_UNORM = 115,
            DXGI_FORMAT_P208 = 130,
            DXGI_FORMAT_V208 = 131,
            DXGI_FORMAT_V408 = 132,
            DXGI_FORMAT_SAMPLER_FEEDBACK_MIN_MIP_OPAQUE,
            DXGI_FORMAT_SAMPLER_FEEDBACK_MIP_REGION_USED_OPAQUE,
            DXGI_FORMAT_FORCE_UINT = 0xffffffff
        };

        public enum DX10_ResourceDimension
        {
            D3D10_RESOURCE_DIMENSION_UNKNOWN = 0,
            D3D10_RESOURCE_DIMENSION_BUFFER = 1,
            D3D10_RESOURCE_DIMENSION_TEXTURE1D = 2,
            D3D10_RESOURCE_DIMENSION_TEXTURE2D = 3,
            D3D10_RESOURCE_DIMENSION_TEXTURE3D = 4
        };
    }
}
