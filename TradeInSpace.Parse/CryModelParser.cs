﻿using CgfConverter;
using CgfConverter.CryEngineCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Numerics;
using System.Text;

namespace TradeInSpace.Parse
{
    public class CryModelParser
    {
        //TODO: Custom parser :^)
        public static Model ParseModel(byte[] ModelData, string Path)
        {
            var model = Model.FromBuffer(ModelData, Path);


            return model;
        }



    }

    public class CryModelGeometryData
    {
        public List<(double, double, double)> Vertexes { get; set; } = new List<(double, double, double)>();
        public List<(uint, uint, uint)> VertexIndexes { get; set; } = new List<(uint, uint, uint)>();

        public List<(double, double, double)> Normals { get; set; } = new List<(double, double, double)>();
        public List<(float, float, float)> Colors { get; set; } = new List<(float, float, float)>();

        public List<(int, List<(uint, uint, uint)>)> MeshSubSets { get; set; } = new List<(int, List<(uint, uint, uint)>)>();

        public static CryModelGeometryData ReadFromChunk(ChunkNode modelNode, List<int> IgnoreTextureID = null)
        {
            var mesh = modelNode.ObjectChunk as ChunkMesh;

            if (mesh == null) return null;

            return CryModelGeometryData.ReadFromChunk(mesh, modelNode, IgnoreTextureID);
        }

        public static CryModelGeometryData ReadFromChunk(ChunkMesh meshChunk, ChunkNode modelChunk, List<int> IgnoreTextureID = null)
        {
            ChunkDataStream 
                tmpVertices = null, tmpVertsUVs = null, tmpNormals = null, tmpUVs = null, 
                tmpIndices = null, tmpColors = null, tmpTangents = null;

            ChunkMeshSubsets tmpMeshSubsets = (ChunkMeshSubsets)modelChunk.ParentModel.ChunkMap[meshChunk.MeshSubsetsData]; 
            // Listed as Object ID for the Node

            if (meshChunk.VerticesData != 0)
                tmpVertices = (ChunkDataStream)modelChunk.ParentModel.ChunkMap[meshChunk.VerticesData];

            if (meshChunk.VertsUVsData != 0)
                tmpVertsUVs = (ChunkDataStream)modelChunk.ParentModel.ChunkMap[meshChunk.VertsUVsData];

            if (tmpVertices == null && tmpVertsUVs == null) // There is no vertex data for this node.  Skip.
                return null;

            if (meshChunk.NormalsData != 0)
                tmpNormals = (ChunkDataStream)modelChunk.ParentModel.ChunkMap[meshChunk.NormalsData];

            if (meshChunk.UVsData != 0)
                tmpUVs = (ChunkDataStream)modelChunk.ParentModel.ChunkMap[meshChunk.UVsData];

            if (meshChunk.IndicesData != 0)
                tmpIndices = (ChunkDataStream)modelChunk.ParentModel.ChunkMap[meshChunk.IndicesData];

            if (meshChunk.ColorsData != 0)
                tmpColors = (ChunkDataStream)modelChunk.ParentModel.ChunkMap[meshChunk.ColorsData];

            if (meshChunk.TangentsData != 0)
                tmpTangents = (ChunkDataStream)modelChunk.ParentModel.ChunkMap[meshChunk.TangentsData];

            var result = new CryModelGeometryData();

            var multiplerVector = new Vector3(
                    Math.Abs(meshChunk.BoundsMin.X - meshChunk.BoundsMax.X),
                    Math.Abs(meshChunk.BoundsMin.Y - meshChunk.BoundsMax.Y),
                    Math.Abs(meshChunk.BoundsMin.Z - meshChunk.BoundsMax.Z)) / 2f;
            if (multiplerVector.X < 1) { multiplerVector.X = 1; }
            if (multiplerVector.Y < 1) { multiplerVector.Y = 1; }
            if (multiplerVector.Z < 1) { multiplerVector.Z = 1; }
            var boundaryBoxCenter = new Vector3(
                    meshChunk.BoundsMin.X + meshChunk.BoundsMax.X,
                    meshChunk.BoundsMin.Y + meshChunk.BoundsMax.Y,
                    meshChunk.BoundsMin.Z + meshChunk.BoundsMax.Z) / 2f;

            if (tmpVertices?.VertCount() > 0)
            {
                foreach(var item in tmpVertices.Vertices)
                {
                    result.Vertexes.Add((item.X, item.Y, item.Z));
                }
            }
            if (tmpVertsUVs?.VertCount() > 0)
            {
                foreach (var item in tmpVertsUVs.Vertices)
                {
                    result.Vertexes.Add((
                        item.X * multiplerVector.X + boundaryBoxCenter.X, 
                        item.Y * multiplerVector.Y + boundaryBoxCenter.Y, 
                        item.Z * multiplerVector.Z + boundaryBoxCenter.Z));
                }
            }

            if(tmpNormals?.Normals?.Length > 0)
            {
                foreach(var item in tmpNormals.Normals)
                {
                    result.Normals.Add((item.X, item.Y, item.Z));
                }
            }
            else if(tmpTangents?.Normals?.Length > 0)
            {
                foreach (var item in tmpTangents.Normals)
                {
                    result.Normals.Add((item.X, item.Y, item.Z));
                }
            }


            if (tmpUVs?.VertCount() > 0)
            {
                foreach (var item in tmpVertices.Vertices)
                {
                    result.Vertexes.Add((item.X, item.Y, item.Z));
                }
            }


            for (uint j = 0; j < tmpMeshSubsets.NumMeshSubset; j++) // Need to make a new Triangles entry for each submesh.
            {
                var subset = tmpMeshSubsets.MeshSubsets[j];

                if (IgnoreTextureID?.Contains(subset.MatID) ?? false)
                    continue;

                //result.VertexIndexes = new List<(uint, uint, uint)>(subset.NumIndices / 3);

                var subsetIndexes = new List<(uint, uint, uint)>();

                for (int vert = 0; vert < subset.NumIndices; vert += 3)
                {
                    subsetIndexes.Add((
                        tmpIndices.Indices[subset.FirstIndex + vert],
                        tmpIndices.Indices[subset.FirstIndex + vert + 1],
                        tmpIndices.Indices[subset.FirstIndex + vert + 2]));
                }

                result.MeshSubSets.Add((subset.MatID, subsetIndexes));
            }

            return result;
        }
    }
}
