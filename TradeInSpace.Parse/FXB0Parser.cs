﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace TradeInSpace.Parse
{
    public class FXB0Parser
    {
        public static string Parse(BinaryReader reader)
        {
            var cc = reader.ReadInt32();
            reader.ReadInt32();
            reader.ReadInt32();
            var string_start = reader.ReadInt32();
            reader.ReadInt32();
            reader.ReadInt32();
            reader.ReadInt32();

            Dictionary<int, string> string_table = new Dictionary<int, string>();

            var original_position = reader.BaseStream.Position;

            reader.BaseStream.Seek(string_start, SeekOrigin.Begin);

            while (reader.BaseStream.Position < reader.BaseStream.Length)
            {
                var id = reader.ReadInt32();
                var str = reader.ReadCString();
                string_table.Add(id, str);
            }

            reader.BaseStream.Position = original_position;

            StringBuilder result = new StringBuilder();

            while (reader.BaseStream.Position < string_start)
            {
                int token = reader.ReadInt32();
                if (string_table.ContainsKey(token))
                {
                    result.AppendLine(string_table[token]);
                }
                else
                {
                    result.AppendLine($"{token:X8}");
                }
            }

            return result.ToString();
        }
    }


}
