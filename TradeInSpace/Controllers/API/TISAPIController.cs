﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TradeInSpace.Data;

namespace TradeInSpace.Controllers.API
{
    public class TISAPIController
    {
        internal TISContext TISData;
        private readonly IConfiguration configuration;

        public TISAPIController(TISContext context, IConfiguration configuration)
        {
            TISData = context;
            this.configuration = configuration;
        }

        internal Models.Version GetVersion()
        {
            return TISData.GetDefaultVersion();
        }

        internal Models.GameDatabase GetTradeData()
        {
            string Channel = this.configuration["SC_Channel"] ?? "LIVE";

            return TISData.GetTradeData(Channel);
        }

        internal Models.GameDatabase GetPartialGameData()
        {
            string Channel = this.configuration["SC_Channel"] ?? "LIVE";

            return TISData.GetPartialGameData(Channel);
        }

        internal Models.GameDatabase GetCompleteGameData()
        {
            string Channel = this.configuration["SC_Channel"] ?? "LIVE";

            return TISData.GetCompleteGameData(Channel);
        }

        internal List<Models.Version> GetVersionListing()
        {
            return TISData.GetVersionListing().ToList();
        }
    }
}
