﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Formatters;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Microsoft.Net.Http.Headers;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;
using TradeInSpace.Data;
using TradeInSpace.Models;

namespace TradeInSpace.Controllers.API
{
    [Route("api/Data")]
    public class DataController : TISAPIController
    {
        IMemoryCache MemoryCache;
        MediaTypeCollection ColladaMedia = new MediaTypeCollection()
        {
            MediaTypeNames.Text.Plain,
            MediaTypeNames.Text.Xml,
            new MediaTypeHeaderValue("application/collada+xml")
        };
        MediaTypeCollection RawMedia = new MediaTypeCollection()
        {
            MediaTypeNames.Application.Octet
        };
        FormatterCollection<IOutputFormatter> RawFormatters = new FormatterCollection<IOutputFormatter>()
            {
                //new XmlSerializerOutputFormatter(),
                new StringOutputFormatter()
            };

        public DataController(TISContext context, IMemoryCache memoryCache, IConfiguration configuration)
            : base(context, configuration)
        {
            MemoryCache = memoryCache;
        }

        [HttpGet("[action]")]
        [Produces(MediaTypeNames.Application.Json)]
        public IEnumerable<TradeTable> TradeTables()
        {
            return GetTradeData().TradeTables;
        }

        [HttpGet("[action]")]
        [Produces(MediaTypeNames.Application.Json)]
        public GameDatabase GameData()
        {
            var gameData = MemoryCache.Get<GameDatabase>(nameof(GameDatabase));

            //Invalidate if current partial details no longer match what we cache
            if(gameData != null)
            {
                var latestDetails = GetPartialGameData();
                if(latestDetails.DB_Version != gameData.DB_Version || 
                    latestDetails.ImportTime != gameData.ImportTime)
                {
                    gameData = null;
                    MemoryCache.Remove(nameof(GameDatabase));
                }
            }

            //If null from first or otherwise cleared
            if(gameData == null)
            {
                gameData = GetCompleteGameData();
                MemoryCache.Set(nameof(GameDatabase), gameData);
            }

            return gameData;
        }

        [HttpGet("[action]")]
        [Produces(MediaTypeNames.Application.Json)]
        public List<Models.Version> ListVersions()
        {
            return GetVersionListing();
        }
    }
}
