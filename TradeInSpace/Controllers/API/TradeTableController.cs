﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TradeInSpace.Data;
using TradeInSpace.Models;

namespace TradeInSpace.Controllers.API
{
    [Route("api/trade")]
    public class TradeTableController : TISAPIController
    {
        IMemoryCache MemoryCache;

        public TradeTableController(TISContext context, IMemoryCache memoryCache, IConfiguration configuration)
            : base(context, configuration)
        {
            MemoryCache = memoryCache;
        }
        
        [HttpGet()]
        [Produces("application/json")]
        public TradeTable Index()
        {
            var tradeData = MemoryCache.Get<TradeTable>(nameof(TradeTable));

            //Invalidate if current partial details no longer match what we cache
            if (tradeData != null)
            {
                var latestDetails = GetPartialGameData();
                if (latestDetails.DB_Version != tradeData.GameDatabase.DB_Version ||
                    latestDetails.ImportTime != tradeData.GameDatabase.ImportTime)
                {
                    tradeData = null;
                    MemoryCache.Remove(nameof(TradeTable));
                }
            }

            //If null from first or otherwise cleared
            if (tradeData == null)
            {
                tradeData = GetTradeData().TradeTables.First();
                MemoryCache.Set(nameof(TradeTable), tradeData);
            }

            return tradeData;
        }
    }
}
