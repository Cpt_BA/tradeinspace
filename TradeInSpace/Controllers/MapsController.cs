﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using TradeInSpace.Controllers.API;
using TradeInSpace.Data;

namespace TradeInSpace.Controllers
{
    public class MapsController : Controller
    {
        internal TISContext TISData;
        private readonly IConfiguration configuration;

        public MapsController(TISContext context, IConfiguration configuration)
        {
            TISData = context;
            this.configuration = configuration;
        }

        [HttpGet("mapviewer/{map_key}")]
        public IActionResult Index(string map_key)
        {
            var version = TISData.GetAvailableVersions()
                .Include(v => v.GameDatabases).ThenInclude(gd => gd.SystemBodies)
                .FirstOrDefault(gd => gd.Default && gd.Enabled);
            var gameDB = version
                .GameDatabases.FirstOrDefault(gd => gd.Enabled && gd.DB_Channel == "LIVE");

            var db = gameDB.SystemBodies.FirstOrDefault(b => b.Key == map_key);

            if(db != null)
            {
                ViewBag.map_key = map_key;
                ViewBag.body_radius = db.BodyRadius ?? 100000;
                ViewBag.db_version = gameDB.Name;

                return View();
            }
            else
            {
                return NotFound();
            }
        }

        [HttpGet("mapviewer/js/{*path}")]
        public IActionResult GetJS(string path)
        {
            //This is NOT GREAT protection =/
            if (path.Contains(".."))
                return NotFound();

            return File($"~/js/{path}", "text/javascript");
        }

        [HttpGet("mapviewer/css/{*path}")]
        public IActionResult GetCSS(string path)
        {
            //This is NOT GREAT protection =/
            if (path.Contains(".."))
                return NotFound();

            return File($"~/css/{path}", "text/css");
        }
    }
}
