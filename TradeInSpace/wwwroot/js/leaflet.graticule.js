//const { FloatType } = require("three");

/*
 Graticule plugin for Leaflet powered maps.
*/
L.Graticule = L.GeoJSON.extend({

    options: {
        style: {
            color: '#333',
            weight: 1
        },
        interval: 20,
        xmin: -180,
        ymin: -90,
        xmax: 180,
        ymax: 90
    },

    initialize: function (options) {
        L.Util.setOptions(this, options);
        this._layers = {};

        if (this.options.sphere) {
            this.addData(this._getFrame());
        } else {
            this.addData(this._getGraticule());
        }
    },

    _getFrame: function() {
        return { "type": "Polygon",
          "coordinates": [
              this._getMeridian(xmin).concat(this._getMeridian(xmax).reverse())
          ]
        };
    },

    _getGraticule: function () {
        var features = [],
            interval = this.options.interval,
            xmin = this.options.xmin,
            ymin = this.options.ymin,
            xmax = this.options.xmax,
            ymax = this.options.ymax;

        var xmid = xmin + (xmax - xmin) / 2;
        var ymid = ymin + (ymax - ymin) / 2;

        // Meridians
        for (var lng = xmin; lng <= xmax; lng += interval) {
            var name;
            if (lng < xmid) {
                name = `${lng}° W`
            } else if (lng == xmid) {
                name = "Prime meridian"
            } else {
                name = `${lng}° E`
            }

            features.push(this._getFeature(this._getMeridian(lng), {
                "name": name
            }));
        }

        // Parallels
        for (var lat = ymin; lat <= ymax; lat += interval) {
            var name;
            if (lat < ymid) {
                name = `${lat}° S`
            } else if (lat == ymid) {
                name = "Equator"
            } else {
                name = `${lat}° N`
            }

            features.push(this._getFeature(this._getParallel(lat), {
                "name": name
            }));
        }

        return {
            "type": "FeatureCollection",
            "features": features
        };
    },

    _getMeridian: function (lng) {
        lng = this._lngFix(lng);
        var coords = [],
            ymin = this.options.ymin,
            ymax = this.options.ymax;
        for (var lat = ymin; lat <= ymax; lat++) {
            coords.push([lng, lat]);
        }
        return coords;
    },

    _getParallel: function (lat) {
        var coords = [],
            xmin = this.options.xmin,
            xmax = this.options.xmax;
        for (var lng = xmin; lng <= xmax; lng++) {
            coords.push([this._lngFix(lng), lat]);
        }
        return coords;
    },

    _getFeature: function (coords, prop) {
        return {
            "type": "Feature",
            "geometry": {
                "type": "LineString",
                "coordinates": coords
            },
            "properties": prop
        };
    },

    _lngFix: function (lng) {
        var xmin = this.options.xmin,
            ymin = this.options.ymin,
            xmax = this.options.xmax,
            ymax = this.options.ymax;
        if (lng >= xmax) return xmax - 0.0001;
        if (lng <= xmin) return xmin + 0.0001;
        return lng;
    }

});

L.graticule = function (options) {
    return new L.Graticule(options);
};
