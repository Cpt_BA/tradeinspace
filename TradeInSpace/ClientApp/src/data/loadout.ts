import { GameShip, GameEquipment } from './generated_models';
import { GameDatabaseFull } from './game_data';
import { flattenRecursive, groupSet } from '../utilities/utilities';
import { DamageInfo, ZeroDamage, AddDamage, MultDamage, CollapseDamage, FiringAction, EquipmentProperties, AmmoData, MagazineData, DamageMode, DamageSummary, SlotDisplay } from './client_models';
import { ModelUtils } from '../utilities/model_utils';
import { ClientData } from './client_data';
import { LoadoutManagerService } from './loadout_manager';
import { displayName } from '../pipes/display_text_pipe';
import { environment } from '../environments/environment';
import { SavedLoadout } from './models';
import { ExceptionInfo } from '_debugger';
import { CursorError } from '@angular/compiler/src/ml_parser/lexer';
import { flatten } from '@angular/compiler';

export class Loadout {
  private raw_root: LoadoutSlotDTO;
  private default_loadout: LoadoutEntryDTO[];

  public name: string;
  public description: string;
  public notes: string;
  public owned: boolean;

  public root_slot: LoadoutSlot;
  public all_slots: LoadoutSlot[];

  public from_equipment?: GameEquipment;
  public from_ship?: GameShip;

  public total_power_avail: number;
  public total_heat_avail: number;
  public total_power_draw: number;
  public total_heat_draw: number;
  public total_heat_excess: number;

  public em_signature: number;
  public ir_signature: number;

  public signalCS: number;
  public signalEM: number;
  public signalIR: number;

  public power_ratio: number;
  public heat_ratio: number;
  public power_demand: number;
  public heat_demand: number;

  public resistance: DamageInfo;
  public total_volley: DamageInfo;
  public total_sustain: DamageInfo;
  public total_missile: DamageInfo;

  public total_hull: number;
  public total_shields: number;
  public total_shield_regen: number;

  public total_fire_rate: number;
  public total_projectiles: number;
  public total_volley_dps: number;
  public total_sustain_dps: number;
  public total_missile_count: number;

  private refreshStats = true;

  private constructor(
    public gameData: GameDatabaseFull,
    public key: string,
    public equipment: GameEquipment) {
    this.all_slots = [];

    this.name = this.equipment.Name;
    this.description = this.equipment.Description;

    this.raw_root = this.equipment.PortLayout;
    this.default_loadout = this.equipment.DefaultLoadout;

    this.root_slot = LoadoutSlot.fromLoadout(this.gameData, this);
    this.Refresh();

    this.from_equipment = equipment;

    this.recalculate();
  }

  public static FromEquipment(gameData: GameDatabaseFull, equipment: GameEquipment): Loadout {
    return new Loadout(gameData, equipment.Key, equipment);
  }

  public static FromEquipmentKey(gameData: GameDatabaseFull, equipmentKey: string): Loadout {
    return new Loadout(gameData, equipmentKey, gameData.EquipmentKeyMap[equipmentKey]);
  }

  public static FromShip(gameData: GameDatabaseFull, ship: GameShip): Loadout {
    var loadout = Loadout.FromEquipmentKey(gameData, ship.Key);
    loadout.from_ship = ship;
    return loadout;
  }

  public static FromShipKey(gameData: GameDatabaseFull, shipKey: string): Loadout {
    return Loadout.FromShip(gameData, gameData.ShipKeyMap[shipKey]);
  }

  public Duplicate(): Loadout {
    return Loadout.fromSaved(this.gameData, this.toSave());
  }

  public Refresh(): void {
    if (this.root_slot)
      this.all_slots = flattenRecursive(this.root_slot, (s) => s.children);
  }

  public recalculate(force = false): void {
    if (!this.refreshStats)

    this.total_heat_draw = 0;
    this.total_heat_avail = 0;
    this.total_heat_excess = 0;
    this.total_power_draw = 0;
    this.total_power_avail = 0;

    //Calculate power & heat stats first
    //Heat should probably be broken out into it's own phase too
    //Power => Heat ( * Ratio) => DPS ( * Ratio (heat capacity fits in here too I think))
    this.root_slot.calculateBaseStats(true);

    this.total_heat_draw = this.root_slot.heat_generate;
    this.total_heat_avail = this.root_slot.heat_removal;
    this.total_power_draw = this.root_slot.power_draw;
    this.total_power_avail = this.root_slot.power_generate;

    this.ir_signature = this.root_slot.generated_ir;
    this.em_signature = this.root_slot.generated_em;

    //Calculate damage stats after the fact.
    //Low power ( < 1:1 ratio ) affects heat accrual
    this.root_slot.calculateDamageStats(true);

    this.total_volley = this.root_slot.volley_dps;
    this.total_sustain = this.root_slot.sustain_dps;
    this.total_missile = this.root_slot.total_missile;
    this.total_missile_count = this.root_slot.total_missile_count;
    this.total_fire_rate = this.root_slot.total_fire_rate;
    this.total_projectiles = this.root_slot.total_projectiles;
    //Must be grabbed AFTER calculateDamageStats since this is part of damage calculation.
    this.total_heat_excess = this.root_slot.heat_excess;

    this.power_ratio = this.total_power_draw === 0 ? 1 : (this.total_power_avail / this.total_power_draw);
    this.power_demand = this.total_power_avail === 0 ? 1 : (this.total_power_draw / this.total_power_avail);

    this.heat_ratio = this.total_heat_draw === 0 ? 0 : (this.total_heat_avail / this.total_heat_draw);
    this.heat_demand = this.total_heat_avail === 0 ? 0 : (this.total_heat_draw / this.total_heat_avail);

    this.total_volley_dps = CollapseDamage(this.total_volley);
    this.total_sustain_dps = CollapseDamage(this.total_sustain);

    const armor = this.find_slots_equipment_type(ClientData.EquipmentTypes.Armor)
      .filter(s => !(!s.attached_equipment));
    if (armor.length >= 1) {
      const ap = armor[0].attached_equipment.Properties.Armor;
      this.resistance = {
        Physical: ap.DamagePhysical, Energy: ap.DamageEnergy, Distortion: ap.DamageDistortion,
        Thermal: ap.DamageThermal, Biochemical: ap.DamageBiochemical, Stun: ap.DamageStun
      }
      this.signalCS = ap.SignalCrossSection;
      this.signalEM = ap.SignalElectromagnetic;
      this.signalIR = ap.SignalInfrared;
    } else {
      this.resistance = { Physical: 1, Energy: 1, Distortion: 1, Thermal: 1, Biochemical: 1, Stun: 1 };
    }

    const shields = this.find_slots_equipment_type(ClientData.EquipmentTypes.Shield)
      .filter(s => !(!s.attached_equipment));
    if (shields.length >= 1) {
      this.total_shields = shields.reduce((p, c) => p + c.attached_equipment.Properties.Shield.MaxShield, 0);
      this.total_shield_regen = shields.reduce((p, c) => p + c.attached_equipment.Properties.Shield.MaxRegenRate, 0);
    } else {
      this.total_shields = 0;
      this.total_shield_regen = 0;
    }

    this.total_hull = this.all_slots.reduce((p, c) => p + c.health, 0);

    //if (!environment.production) console.log("Loadout calc: ", this);
  }

  all_equipment(): Array<GameEquipment> {
    return this.all_slots.map(s => s.attached_equipment);
  }

  root_slots(): Array<LoadoutSlot> {
    return this.root_slot.children;
  }

  find_slots(fnTest: (test_slot: LoadoutSlot) => boolean): LoadoutSlot[] {
    return this.all_slots.filter(s => fnTest(s))
  }

  find_slots_type(slot_type: string): LoadoutSlot[] {
    return this.find_slots(s => s.types.includes(slot_type));
  }

  find_slots_equipment_type(equipment_type: string): LoadoutSlot[] {
    return this.find_slots(s => s.attached_equipment && s.attached_equipment.AttachmentType === equipment_type);
  }


  /* Import/Export code */

  public static fromSaved(gameData: GameDatabaseFull, saved: SavedLoadout): Loadout {
    const loadout = Loadout.FromShipKey(gameData, saved.ship_key);
    loadout.name = saved.name;
    loadout.key = `custom_${saved.ship_key}_${saved.name}`
    loadout.description = saved.description;
    loadout.notes = saved.notes;
    loadout.owned = saved.owned;

    loadout.root_slot.applyEntries(saved.loadout_entries);

    loadout.recalculate();

    return loadout;
  }

  toSave(): SavedLoadout {
    return {
      name: this.name,
      description: this.description,
      notes: this.notes,
      owned: this.owned,
      ship_key: this.equipment.Key,
      loadout_entries: this.root_slot.children.map(c => c.toEntryDTO())
    }
  }

  public static fromImport(gameData: GameDatabaseFull, loadoutImoprt: string): LoadoutImportResult {
    let currentState: LoadoutParseState = LoadoutParseState.Header;
    let currentSection: string = null;
    let currentLoadout: Loadout = null;
    let currentParents: LoadoutSlot[] = null;

    let importedSlots: LoadoutSlot[] = [];

    const fnCleanText = (input: string): string => {
      return input
        .split("_")
        .map(p => p.replace(/^\w/, c => c.toUpperCase()))
        .join(" ");
    };

    console.log("Importing: ", loadoutImoprt);

    //Don't change without sufficiently high LCK or INT stats
    const lineProcess: RegExp = /^(?<prefix>\s*> )?(?<slot_name>.*?)\s*(\[(?<quantity>\d+)x\])?( -( \(S(?<size>\d+)(:G(?<grade>\d+))?\))? (?<attached_name>.*?))?$/
    const importLines = loadoutImoprt.split("\n").map(l => l.trim());

    try {
      importLines.forEach(line => {
        if (line.trim().startsWith("#")) return; //Skip comments
        lineProcess.lastIndex = 0; //Need to reset this every loop

        switch (currentState) {
          case LoadoutParseState.Header: {
            if (currentLoadout) throw new Error("Ship already specified")
            const headerParts = /(?<name>.*?)?\[(?<ship>.*?)\]/g.exec(line)["groups"]
            const shipName = headerParts["ship"]
            const matchingShip = gameData.Ships.find(s => s.Name === shipName)
            if (!matchingShip) throw new Error(`No ship matching name '${shipName}' found.`)

            currentLoadout = Loadout.FromShip(gameData, matchingShip);
            currentParents = [currentLoadout.root_slot];

            if (headerParts["name"]) currentLoadout.name = headerParts["name"].trim();

            currentState = LoadoutParseState.Section;
            break;
          }
          case LoadoutParseState.Section:
            if (line.trim() === "") return; //Skip empty lines
            currentSection = /--(.*?)--/g.exec(line)[0]

            currentState = LoadoutParseState.Slots;
            break;
          case LoadoutParseState.Slots: {
            const slotParts = lineProcess.exec(line)["groups"];
            if (!slotParts) throw new Error("")
            //Empty line indiciates a new system section is coming up.
            if (line.trim() === "") {
              currentState = LoadoutParseState.Section;
              return;
            }

            //TODO: Make this more leniant to handle more manual changes
            //This is a jenky way of handling it for now just b/c the section isn't used
            if (line.startsWith("--") && line.endsWith("--")) {
              return;
            }

            const slotName = slotParts["slot_name"].replace('\r', "");
            const slotCount = parseInt(slotParts["quantity"] || "1");
            const slotEqName = slotParts["attached_name"];
            const slotEqSize = slotParts["size"] ? parseInt(slotParts["size"]) : null;
            const slotEqGrade = slotParts["grade"] ? parseInt(slotParts["grade"]) : null;

            let newParents: LoadoutSlot[] = [];
            let newEquipment: GameEquipment = null;

            if (slotEqName) {
              //Has equipment attached
              newEquipment = gameData.Equipment.find(eq =>
                eq.Name === slotEqName &&
                (!slotEqSize || eq.Size === slotEqSize) &&
                (!slotEqGrade || eq.Grade === slotEqGrade));
              if (!newEquipment) {
                if (slotEqSize && slotEqGrade)
                  throw new Error(`Unable to find equipment ${slotEqName} (S${slotEqSize}:G${slotEqGrade})`);
                else if (slotEqSize)
                  throw new Error(`Unable to find equipment ${slotEqName} (S${slotEqSize})`);
                else if (slotEqGrade)
                  throw new Error(`Unable to find equipment ${slotEqName} (G${slotEqGrade})`);
                else
                  throw new Error(`Unable to find equipment ${slotEqName}`);
              }
            }

            if (!slotParts["prefix"])
              currentParents = [currentLoadout.root_slot];

            if (currentParents.length === 0) {
              console.error(line, currentState);
              throw new Error("Error while importing");
            }

            if (!environment.production)
              console.log(`Importing ${slotCount} slots (${slotName}). `, slotEqName ? `Equipment - ${slotEqName} (S${slotEqSize}:G${slotEqGrade})` : "");

            for (let num = 0; num < slotCount; num++) {
              currentParents.forEach(parent => {
                const matchingChild = parent.children.find(child => {
                  if (importedSlots.includes(child)) return false;
                  const locked = !child.editable || !child.display_params || child.display_params.locked;
                  if (locked && child.attached_equipment) {
                    return slotName === fnCleanText(displayName(child.attached_equipment));
                  }

                  return slotName === fnCleanText(child.name_group.group_name);
                });

                if (!matchingChild) {
                  console.error("No matching child found.", currentParents, slotName);
                  throw new Error(`Could not find slot ${slotName}.`);
                }

                newParents.push(matchingChild);
                importedSlots.push(matchingChild);
                if (newEquipment) {
                  matchingChild.attachComponent(newEquipment, true);
                } else if (matchingChild.editable) {
                  matchingChild.attachComponent(null, false);
                }
              });
            }

            if (newParents.length !== slotCount * currentParents.length) {
              console.error(currentParents, newParents, line, slotParts)
              throw new Error(`Expected to import into ${slotCount * currentParents.length} slots, but instead imported into ${newParents.length} slots.`);
            }

            currentParents = newParents;
            break;
          }

        }
      });

      currentLoadout.recalculate();

      return { success: true, loadout: currentLoadout }
    } catch (ex) {
      return { success: false, error: ex.message }
    }
  }

  toExport(loadoutMgr: LoadoutManagerService, differential = false): string {
    const baseLoadout = Loadout.FromEquipment(this.gameData, this.equipment);

    const fnCleanText = (input: string): string => {
      return input
        .split("_")
        .map(p => p.replace(/^\w/, c => c.toUpperCase()))
        .join(" ");
    };

    const fnKeepSlot = (slot: LoadoutSlot): boolean => {
      //Reject any slots that don't wouldn't show up on loadout screen anyway
      if (!slot.display_params || slot.display_params.locked)
        return false;
      //Hide slots that can't be edited
      if (!slot.editable) return false;

      if (differential) {
        const matchingSlots = baseLoadout.find_slots(s => s.name === slot.name && s.parent.name === slot.parent.name)
        //If we can't find the slot in the default loadout (???) just show this item
        if (matchingSlots.length < 1) return true;
        if (matchingSlots.length > 1) console.warn("Multiple matching base slots");
        const matchingSlot = matchingSlots[0];

        if (matchingSlot.attached_equipment && slot.attached_equipment && 
            matchingSlot.attached_equipment.Key === slot.attached_equipment.Key)
          return false;
      }

      return true;
    }

    //This will recursively search for children. might become prohibitively expensive
    const fnFilterSlot = (slot: LoadoutSlot): boolean => {
      return fnKeepSlot(slot) || slot.children.some(fnFilterSlot)
    }

    const outputLines = []
    const namePad = 25;

    //Ship name
    outputLines.push(`${this.name} [${this.equipment.Name}]`);

    const exportSlots = this.root_slot.children.filter(fnFilterSlot);
    const loadoutGroups = groupSet(exportSlots, s => s.display_params.title);
    const groupNames = Object.keys(loadoutGroups);

    const fmtRootSlot = (slot: LoadoutSlot, count: number, depth: number): string => {
      const eq = slot.attached_equipment;
      const quantity = count === 1 ? "" : ` [${count}x]`;
      const padLength = namePad - quantity.length;
      let groupName = fnKeepSlot(slot) ? slot.name_group.group_name : displayName(eq);
      groupName = fnCleanText(groupName).padEnd(padLength, " ");
      const slotEquipped = (eq && fnKeepSlot(slot)) ? ` - (S${eq.Size}:G${eq.Grade}) ${eq.Name}` : "";
      return `${groupName}${quantity}${slotEquipped}`;
    }

    const fmtNestedSlot = (slot: LoadoutSlot, count: number, depth: number): string => {
      const prefix = ''.padStart((depth - 2) * 2, " ") + "> ";
      const eq = slot.attached_equipment;
      const quantity = count === 1 ? "" : ` [${count}x]`;
      const padLength = namePad - quantity.length - prefix.length;
      let groupName = fnKeepSlot(slot) ? slot.name_group.group_name : displayName(eq);
      groupName = fnCleanText(groupName).padEnd(padLength, " ");
      const slotEquipped = (eq && fnKeepSlot(slot)) ? ` - (S${eq.Size}:G${eq.Grade}) ${eq.Name}` : "";
      return `${prefix}${groupName}${quantity}${slotEquipped}`;
    }

    const groupAndAdd = (slots: LoadoutSlot[], formatter: (slot: LoadoutSlot, count: number, depth: number) => string, depth: number = 1): void => {
      const validSlots = slots.filter(fnFilterSlot);
      const slotGroups = groupSet(validSlots, s =>
        `${s.name_group.group_name}_${s.attached_equipment ? s.attached_equipment.Key : ""}`);
      const slotKeys = Object.keys(slotGroups);

      slotKeys.forEach(sk => {
        const equipmentGroup = slotGroups[sk];
        const slot = equipmentGroup[0];
        outputLines.push(formatter(slot, equipmentGroup.length, depth))
        groupAndAdd(slot.children, fmtNestedSlot, depth + 1);
      });
    };


    groupNames
      .map(gn => loadoutGroups[gn])
      .sort((s1, s2) => s1[0].display_params.order - s2[0].display_params.order)
      .forEach(s => {
        outputLines.push("");
        outputLines.push(`--${s[0].display_params.title}--`);

        groupAndAdd(s, fmtRootSlot);
      });

    return outputLines.join("\r\n");
  }
};



export class LoadoutSlot {
  children: LoadoutSlot[] = [];

  name_group: NameGroupEntry;
  display_params: SlotDisplay = null;

  public power_draw: number;
  public power_generate: number;
  public heat_generate: number;
  public heat_removal: number;
  public heat_excess = 0;

  public generated_ir: number;
  public generated_em: number;

  public using_ammo: MagazineData;

  public volley_dps: DamageInfo;
  public sustain_dps: DamageInfo;
  public total_missile: DamageInfo;
  public total_missile_count: number;
  public total_fire_rate: number;
  public total_projectiles: number;

  attached_equipment?: GameEquipment;

  private constructor(private game_data: GameDatabaseFull,
    public name: string,
    public mass: number,
    public health: number,
    public editable: boolean,
    public visible: boolean,
    public types: string[],
    public tags: string[],
    public required_tags: string[],
    public min_size: number,
    public max_size: number,
    public connections: ConnectionDTO[],
    public loadout: Loadout,
    public parent: LoadoutSlot = null) {
    if (!this.types)
      this.types = [];
    if (!this.name)
      this.name = "< Empty >";
    this.name_group = CheckNameGrouping(this.name);

    const slotClasses = Array.from(new Set(this.types.map(type => {
      //Split if we have a sub-type
      return ClientData.slotDisplayClasses.find(dc => type.includes("/") ? dc.types.includes(type.split("/")[0]) : dc.types.includes(type));
    })));

    if (slotClasses.length > 1) console.warn(`Multiple classes defined for slot ${this.name} - ${this.types.join(",")}`);
    if (slotClasses.length > 0) this.display_params = slotClasses[0];
  }

  public hasConnections(name: string): boolean {
    return this.connections.some(p => p.Name === name)
  }

  public static fromLoadout(gameData: GameDatabaseFull, loadout: Loadout): LoadoutSlot {
    return LoadoutSlot.fromEquipment(gameData, loadout.equipment, loadout, null);
  }

  public static fromEquipment(gameData: GameDatabaseFull, equipment: GameEquipment, loadout?: Loadout, parent?: LoadoutSlot, applyDefault: boolean = true): LoadoutSlot {
    if (!equipment.PortLayout)
      return new LoadoutSlot(gameData, equipment.Name, 0, 0, false, false, [equipment.AttachmentType], [], [], equipment.Size, equipment.Size, [], loadout, parent);

    var port = this.fromDTO(gameData, equipment.PortLayout, loadout, parent);
    if (applyDefault && equipment.DefaultLoadout)
      port.applyEntries(equipment.DefaultLoadout);
    return port;
  }

  public static fromDTO(gameData: GameDatabaseFull, dto: LoadoutSlotDTO, loadout: Loadout, parent?: LoadoutSlot): LoadoutSlot {
    const slot = new LoadoutSlot(gameData,
      dto.PortName, dto.Mass, dto.Health,
      dto.Editable, dto.Visible, dto.Types || [],
      dto.Tags, dto.RequiredTags,
      dto.MinSize, dto.MaxSize, dto.Connections,
      loadout, parent);

    if (dto.Slots) {
      slot.children = dto.Slots.map(s => LoadoutSlot.fromDTO(gameData, s, loadout, slot));
    }

    //console.log("Slot from DTO: ", dto, loadout, parent, slot);

    return slot;
  }

  public toEntryDTO(): LoadoutEntryDTO {
    return {
      ComponentKey: this.attached_equipment ? this.attached_equipment.Key : null,
      PortName: this.name,
      Children: this.children.map(c => c.toEntryDTO())
    };
  }

  public canFitType(equipmentType: string): boolean {
    //.replace() to fix trailing slashes on slot types
    return this.types.some(t => {
      const typeParts = t.replace(/\/$/, "").split("/", 2);
      if (typeParts.length === 1) {
        //Look for exact or starting with that and slash to make sure we ignore simmilar names
        return equipmentType === typeParts[0] || equipmentType.startsWith(typeParts[0] + "/");
      } else {
        //Handle exact and fuzzy equipment matches on a complex slot type
        //This seems odd, but QDrives don't follow the expected logic
        return equipmentType === t || t.startsWith(equipmentType + "/");
      }
    });
  }

  public applyEntries(entries: LoadoutEntryDTO[]): void {
    entries.forEach(entry => {
      const matchingSlot = this.searchForPort(entry.PortName);

      if (!matchingSlot) {
        //console.warn(`Loadout specified unknown port ${entry.PortName}. Component (${entry.ComponentKey})`)
        return;
      }

      //Find and attach component
      if (entry.ComponentKey) {
        const attach_equipment = this.game_data.EquipmentKeyMap[entry.ComponentKey];
        if (attach_equipment) {
          matchingSlot.attachComponent(attach_equipment);
        } else {
          console.warn(`Unable to find equipment key specified in loadout - ${entry.ComponentKey}`);
          matchingSlot.attachComponent(null);
        }
      } else {
        //Otherwise set null so we explicityl un-equip items loaded by default settings
        matchingSlot.attachComponent(null);
      }

      //Apply loadout recursively
      if (entry.Children && entry.Children.length > 0) {
        matchingSlot.applyEntries(entry.Children);
      }
    });
  }

  //NOTE: This does not navigate attached components
  private searchForPort(PortName: string): LoadoutSlot {
    var result: LoadoutSlot = null;
    var _InnerSearch = (_slot: LoadoutSlot) => {
      if (_slot.name && PortName && _slot.name.toLowerCase() == PortName.toLowerCase()) {
        result = _slot;
      }
      _slot.children.forEach(c => { if (!result) _InnerSearch(c); })
    };
    _InnerSearch(this);
    return result;
  }

  public attachComponent(component: GameEquipment, load_defaults: boolean = true, clear: boolean = true): void {
    if (clear) {
      this.children = [];
    }
    this.attached_equipment = component;
    if (component && component.PortLayout) {
      //TODO: Properly fix after switching to hierarchical loadout structure.
      const loadoutApply: LoadoutEntryDTO[] = (load_defaults && component.DefaultLoadout) || [];

      this.children.push(LoadoutSlot.fromEquipment(this.game_data, component, this.loadout, this, load_defaults));

      this.applyEntries(loadoutApply);
      this.loadout.Refresh();
    } else {
      this.children = [];
    }
  }

  public calculateBaseStats(includeChildren: boolean = true): void {
    //Depth-first calculation of child stats
    if (this.children) {
      this.children.forEach(c => {
        c.calculateBaseStats(includeChildren);
      });
    }

    this.heat_generate = 0;
    this.heat_removal = 0;
    this.power_draw = 0;
    this.power_generate = 0;

    this.generated_em = 0;
    this.generated_ir = 0;

    if (this.attached_equipment) {

      const eqProps: EquipmentProperties = this.attached_equipment.Properties;

      if (!eqProps) debugger;

      if (eqProps.Heat) {
        this.heat_generate += eqProps.Heat.HeatActive;
        this.generated_ir += this.heat_generate * eqProps.Heat.TemperatureToIR;
      }

      if (eqProps.Energy) {
        this.power_draw += eqProps.Energy.PowerActive;
        this.generated_em += eqProps.Energy.PowerActive * eqProps.Energy.PowerToEM;
      }

      if (eqProps.PowerPlant) {
        this.power_generate += eqProps.PowerPlant.PowerActive;
        this.generated_em += eqProps.PowerPlant.PowerActive * eqProps.PowerPlant.PowerToEM;
      }

      this.heat_removal += eqProps.Cooler ? eqProps.Cooler.CoolingRate : 0;
    }

    if (includeChildren) {
      this.children.forEach(c => {
        this.heat_generate += c.heat_generate;
        this.heat_removal += c.heat_removal;
        this.power_draw += c.power_draw;
        this.power_generate += c.power_generate;

        this.generated_em += c.generated_em;
        this.generated_ir += c.generated_ir;
      });
    }
  }

  public calculateDamageStats(includeChildren: boolean = true): void {
    //Depth-first calculation of child stats
    if (this.children) {
      this.children.forEach(c => {
        c.calculateDamageStats(includeChildren);
      });
    }

    this.sustain_dps = ZeroDamage;
    this.volley_dps = ZeroDamage;
    this.total_missile = ZeroDamage;
    this.total_missile_count = 0;
    this.total_fire_rate = 0;
    this.total_projectiles = 0;
    this.heat_excess = 0;

    if (includeChildren) {
      this.children.forEach(c => {
        this.sustain_dps = AddDamage(this.sustain_dps, c.sustain_dps);
        this.volley_dps = AddDamage(this.volley_dps, c.volley_dps);
        this.total_missile = AddDamage(this.total_missile, c.total_missile);
        this.total_missile_count += c.total_missile_count;
        this.total_fire_rate += c.total_fire_rate;
        this.total_projectiles += c.total_projectiles;
        this.heat_excess += c.heat_excess;
      });
    }

    if (!this.attached_equipment) return;

    this.using_ammo = ModelUtils.getEquipmentAmmo(this.game_data, this.attached_equipment);

    if (!this.using_ammo) {
      if (this.attached_equipment.Properties.Missile) {
        //Missiles *are* the ammo :O
      } else if (this.attached_equipment.Properties.Weapon) {
        console.error(`No ammo details for weapon ${this.attached_equipment.Key}`);
        return;
      } else {
        return;
      }
    }

    const damage = calculateDamage(this.game_data, this.attached_equipment, this.using_ammo);
    if (damage) {
      this.sustain_dps = AddDamage(this.sustain_dps, damage.sustain);
      this.volley_dps = AddDamage(this.volley_dps, damage.volley);
      this.total_missile = AddDamage(this.total_missile, damage.missile);
      this.total_missile_count += damage.missile_count;
      this.total_fire_rate += damage.effective_rate;
      this.total_projectiles += damage.projectiles;
      this.heat_excess += damage.heat_generation;
    }
  }
};

//Damage calc code

export function calculateDamage(game_data: GameDatabaseFull, equipment: GameEquipment, ammo: MagazineData): DamageSummary {
  var eqProps: EquipmentProperties = equipment.Properties;

  if (!eqProps.Weapon && !eqProps.Missile) return null;

  let volleyDamage: DamageInfo = ZeroDamage;
  let onesecondDamage: DamageInfo = ZeroDamage;
  let missileDamage: DamageInfo = ZeroDamage;
  let missileCount: number = 0;
  let volleyPellets: number = 0;
  let fireRate: number = 0;
  let cooldown: number = 0;
  let mode: DamageMode = DamageMode.Single;
  let heat: number = 0;

  const factorDamage = (damage: DamageInfo, fire_rate: number, pellet_count: number) => {
    if (!damage) return;
    const fullDamage = MultDamage(damage, pellet_count);
    volleyDamage = AddDamage(volleyDamage, fullDamage);
    const dpsDmg = MultDamage(fullDamage, fireRate / 60);
    onesecondDamage = AddDamage(onesecondDamage, dpsDmg);
  };

  if (eqProps.Weapon && eqProps.Weapon.Actions.length > 0) {

    if (!ammo) {
      console.error("No ammo data supplied!");
    }

    const processSingle = (action: FiringAction, isSubAction: boolean = false) => {
      switch (action.ModeName) {
        case "Looping":
        case "Automatically":
        case "Individually":
          /*
          action.FireActions.forEach(subAction => {
            processSingle(subAction, true);
          });
          */
          //TODO: Process each shot independently, in case there are variations.
          if (action.FireActions.length > 0)
            processSingle(action.FireActions[0]);
          mode = DamageMode.Automatic;
          break;
        case "Single":
        case "Shotgun":
          if (!isSubAction) {
            if (action.PelletCount == 1) {
              mode = DamageMode.Single;
            } else {
              mode = DamageMode.Scattergun;
            }
          }

          fireRate += action.FireRate;
          heat += action.HeatPerShot * action.FireRate;
          volleyPellets += action.PelletCount;
          factorDamage(ammo.AmmoData.BaseDamage, action.FireRate, action.PelletCount);
          factorDamage(ammo.AmmoData.ExplosionDamage, action.FireRate, action.PelletCount);
          break;
        case "Rapid":
          mode = DamageMode.Automatic;
          fireRate += action.FireRate
          heat += action.HeatPerShot * action.FireRate;
          volleyPellets += action.PelletCount;
          factorDamage(ammo.AmmoData.BaseDamage, action.FireRate, action.PelletCount);
          factorDamage(ammo.AmmoData.ExplosionDamage, action.FireRate, action.PelletCount);
          break;
        case "Burst":
          mode = DamageMode.Burst;
          fireRate += action.FireRate
          heat += action.HeatPerShot * action.FireRate;
          volleyPellets += action.ShotCount;
          factorDamage(ammo.AmmoData.BaseDamage, action.FireRate, action.PelletCount);
          factorDamage(ammo.AmmoData.ExplosionDamage, action.FireRate, action.PelletCount);
          break;
        case "Charge": {
          mode = DamageMode.Charge;
          const localRate = 60 * (1 / (action.ChargeTime + action.CooldownTime));
          fireRate = localRate
          heat += action.HeatPerShot * action.FireRate;
          volleyPellets += 1
          cooldown = action.CooldownTime;
          factorDamage(ammo.AmmoData.BaseDamage, localRate, action.ChargedMultiplier);
          factorDamage(ammo.AmmoData.ExplosionDamage, localRate, action.ChargedMultiplier);
          break;
        }
        default:
          console.error(`Unkonwn fire action mode ${action.ModeName}`);
      }
    };

    /*
    eqProps.Weapon.Actions.forEach(action => {
      processSingle(action);
    });
    */

    if (eqProps.Weapon.Actions.length > 0) {
      processSingle(eqProps.Weapon.Actions[0]);
    }

  }

  if (eqProps.Missile) {
    mode = DamageMode.Missile;
    missileCount += 1;
    missileDamage = AddDamage(missileDamage, eqProps.Missile.ExplosionDamage);
  }

  //TODO: Calculate efficiency here instead
  return {
    volley: volleyDamage,
    volley_flat: CollapseDamage(volleyDamage),
    sustain: onesecondDamage,
    sustain_flat: CollapseDamage(onesecondDamage),
    missile: missileDamage,
    missile_flat: CollapseDamage(missileDamage),
    missile_count: missileCount,
    projectiles: volleyPellets,
    effective_rate: fireRate,
    cooldown_time: cooldown,
    heat_generation: heat,
    mode: mode
  }
}

//Name grouping code

export function CheckNameGrouping(hardpointName: string): NameGroupEntry {
  let underName = hardpointName.toLowerCase();
  underName = underName.replace(/^hardpoint_/, "");

  const groupers: NameGrouper[] = [
    { mode: LoadoutEntryMirrorMode.Side, indicators: ["Right Wing"], matchers: [/_right_wing$/] },
    { mode: LoadoutEntryMirrorMode.Side, indicators: ["Left Wing"], matchers: [/_left_wing$/] },
    { mode: LoadoutEntryMirrorMode.Side, indicators: ["Right Outer"], matchers: [/_right_outer$/] },
    { mode: LoadoutEntryMirrorMode.Side, indicators: ["Left Outer"], matchers: [/_left_outer$/] },

    { mode: LoadoutEntryMirrorMode.Side, indicators: ["Front", "Bottom", "Left"], matchers: [/_fbl$/] },
    { mode: LoadoutEntryMirrorMode.Side, indicators: ["Front", "Bottom", "Right"], matchers: [/_fbr$/] },
    { mode: LoadoutEntryMirrorMode.Side, indicators: ["Front", "Bottom"], matchers: [/_fb$/] },
    { mode: LoadoutEntryMirrorMode.Side, indicators: ["Front", "Top"], matchers: [/_ft$/] },
    { mode: LoadoutEntryMirrorMode.Side, indicators: ["Front", "Right"], matchers: [/_fr$/] },
    { mode: LoadoutEntryMirrorMode.Side, indicators: ["Front", "Left"], matchers: [/_fl$/] },
    { mode: LoadoutEntryMirrorMode.Side, indicators: ["Front", "Bottom"], matchers: [/_fb$/] },
    { mode: LoadoutEntryMirrorMode.Side, indicators: ["Rear", "Top"], matchers: [/_rt$/] },
    { mode: LoadoutEntryMirrorMode.Side, indicators: ["Rear", "Right"], matchers: [/_rr$/] },
    { mode: LoadoutEntryMirrorMode.Side, indicators: ["Rear", "Left"], matchers: [/_rl$/] },
    { mode: LoadoutEntryMirrorMode.Side, indicators: ["Rear", "Bottom"], matchers: [/_rb$/] },

    { mode: LoadoutEntryMirrorMode.Side, indicators: ["Top"], matchers: [/_top$/, /_u$/, /_upper$/] },
    { mode: LoadoutEntryMirrorMode.Side, indicators: ["Bottom"], matchers: [/_bottom$/, /_bot$/, /_d$/, /_lower$/] },

    { mode: LoadoutEntryMirrorMode.Side, indicators: ["Front"], matchers: [/_front$/, /_forward$/] },
    { mode: LoadoutEntryMirrorMode.Side, indicators: ["Rear"], matchers: [/_rear$/, /_back$/] },
    { mode: LoadoutEntryMirrorMode.Side, indicators: ["Tail"], matchers: [/_tail$/] },

    { mode: LoadoutEntryMirrorMode.Side, indicators: ["Left"], matchers: [/^left_/, /_left$/, /_l$/, /_left_(\d+)$/, /_left_/] },
    { mode: LoadoutEntryMirrorMode.Side, indicators: ["Center"], matchers: [/_center$/, /_middle$/] },
    { mode: LoadoutEntryMirrorMode.Side, indicators: ["Right"], matchers: [/^right_/, /_right$/, /_r$/, /_right_(\d+)$/, /_right_/] },

    { mode: LoadoutEntryMirrorMode.Copied, indicators: ["Repeat"], matchers: [/_(\d+)/, /_wing(\d+)_/] }
  ];

  var remaining_name = underName;
  var matching_groups: NameGrouper[] = [];
  var found_group = false;

  var _CleanName = (underscore_name: string): string => {
    return underscore_name
      .split(/[\s_]/)
      .map(c => c.charAt(0).toUpperCase() + c.slice(1))
      .join(" ");
  }
  

  do {
    found_group = false;
    groupers.forEach(g => {
      if (found_group) return;
      if (matching_groups.length > 0 && g.mode != matching_groups[0].mode) return;

      var matchingReg = g.matchers.find(reg => reg.test(remaining_name))

      if (matchingReg) {
        found_group = true;
        remaining_name = remaining_name.replace(matchingReg, "_");
        matching_groups.push(g);
      }
    });
  } while (found_group);

  if (matching_groups.length === 0) {
    //No grouping
    return {
      mode: LoadoutEntryMirrorMode.None,
      indicator: null,
      original: underName,
      group_name: _CleanName(underName)
    };
  } else {
    return {
      mode: matching_groups[0].mode,
      indicator: matching_groups.map(g => g.indicators.join(" ")).join(" "),
      original: underName,
      group_name: _CleanName(remaining_name)
    }
  }
}

//Loadout DTO's
export interface LoadoutSlotDTO {
  PortName: string;
  Mass: number;
  Health: number;
  Editable: boolean;
  Visible: boolean;
  Types: string[];
  Tags: string[];
  Flags: string[];
  RequiredTags: string[];
  MinSize: number;
  MaxSize: number;
  Slots?: LoadoutSlotDTO[];
  Connections: ConnectionDTO[];
}

export interface ConnectionDTO {
  PipeClass: string;
  Name: string;
}

export interface LoadoutEntryDTO {
  PortName: string;
  ComponentKey: string;
  Children?: LoadoutEntryDTO[];
}

//Name Grouping
export interface NameGroupEntry {
  mode: LoadoutEntryMirrorMode;
  original: string;
  indicator: string;
  group_name: string;
}

interface NameGrouper {
  matchers: RegExp[];
  mode: LoadoutEntryMirrorMode;
  indicators: string[];
}

export interface LoadoutImportResult {
  loadout?: Loadout;
  success: boolean;
  error?: string;
}

export enum LoadoutEntryMirrorMode {
  None,
  Copied,
  Side,
  TopBottom
}

export enum LoadoutParseState {
  Header,
  Section,
  Slots
}
