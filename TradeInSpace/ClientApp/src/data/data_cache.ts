import { Injectable, Inject, InjectionToken, OnInit, ErrorHandler } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { StorageService, LOCAL_STORAGE } from 'angular-webstorage-service';
import { BasicUserSettings, LoadoutStorage } from './models';
import { ReplaySubject, BehaviorSubject, Observable, Subscribable, Subscription, Observer } from 'rxjs';
import { ClientData } from './client_data';
import { GameDatabase, TradeTable } from './generated_models';
import { GameDatabaseLoaded, TradeTableLoaded, GameDatabaseFull } from './game_data';
import { environment } from '../environments/environment';
import { inspect } from 'util';
import { combineLatest } from 'rxjs';
import { TISErrorHandler } from '../utilities/error_handler';
import { AngularIndexedDB } from 'angular2-indexeddb';

@Injectable()
export class DataCacheService implements OnInit {

  _baseURL: string;
  _http: HttpClient;

  _alreadyMismatched: boolean = false;

  gameData: Observable<GameDatabaseFull>;
  tradeTable: Observable<TradeTable>;
  userDetails: Observable<BasicUserSettings>;
  loadoutData: Observable<LoadoutStorage>;

  userDetailsSource: BehaviorSubject<BasicUserSettings>;
  loadoutDataSource: BehaviorSubject<LoadoutStorage>;

  cacheDB: AngularIndexedDB;
  cacheDBRecordStore: IDBObjectStore;

  defaultSettings: BasicUserSettings = {
    active_ship: "rsi_aurora_mr",
    balance: 5000,
    custom_capacity: 1,
    owned_ships: [],
    last_filter: ClientData.FilterPresets.default,
    current_location: "port_olisar_admin",
    use_balance: true,
    use_location: false,
    use_capacity: false,
    shopping_cart: [],
    last_reset: null
  };

  gameDataCConfig: CachedEndpointConfig<GameDatabaseFull> = {
    path: "api/data/GameData",
    expirationSeconds: 60 * 60 * 24 * 7,
    storageKey: "GameData"
  };

  tradeDataCConfig: CachedEndpointConfig<TradeTable> = {
    path: "api/trade",
    expirationSeconds: 60 * 60 * 24,
    storageKey: "TradeTable"
  };

  loadoutSettingsName: string = "LoadoutSettings";
  userSettingsName: string = "UserSettings";
  dataTableName: string = "DataTables";

  constructor(http: HttpClient,
    @Inject(ErrorHandler) private err_handle: TISErrorHandler,
    @Inject('BASE_URL') baseUrl: string) {
    this._baseURL = baseUrl;
    this._http = http;

    if (!environment.production) {
      this.gameDataCConfig.expirationSeconds = 60;
      this.tradeDataCConfig.expirationSeconds = 60;
    }

    this.cacheDB = new AngularIndexedDB("TISDataCache", 1);

    this.gameDataCConfig.outSubject = this.gameData = new ReplaySubject(1);
    this.tradeDataCConfig.outSubject = this.tradeTable = new ReplaySubject(1);

    this.userDetailsSource = new BehaviorSubject(null);
    this.userDetails = this.userDetailsSource.filter(ud => !(!ud));

    this.loadoutDataSource = new BehaviorSubject(null);
    this.loadoutData = this.loadoutDataSource.filter(ld => !(!ld));

    this.cacheDB.openDatabase(1, (evt: any) => {
      const openResult = (evt.currentTarget as IDBOpenDBRequest).result;
      //TODO: A LOT! more here
      //fully transferring models to tables would be awesome
      this.cacheDBRecordStore = openResult.createObjectStore(this.dataTableName, { keyPath: "table_name", autoIncrement: false });
    }).then((db) => {
      // Load settings first thing
      this.checkCache<BasicUserSettings>(this.userSettingsName).then(userData => {
        const cacheValue: BasicUserSettings = userData && userData.data;

        if (cacheValue) {
          const mergedSettings = Object.assign({}, this.defaultSettings, cacheValue);
          this.userDetailsSource.next(mergedSettings);
        } else {
          this.userDetailsSource.next(this.defaultSettings);
        }
      });

      //Simple loader for loadouts
      this.checkCache<LoadoutStorage>(this.loadoutSettingsName).then(loadoutData => {
        const cacheValue: LoadoutStorage = loadoutData && loadoutData.data;
        this.loadoutDataSource.next(cacheValue || { loadouts: [] });
      });

      this.gameDataCConfig.rawSubject = new BehaviorSubject<GameDatabaseFull>(null);
      this.tradeDataCConfig.rawSubject = new BehaviorSubject<TradeTable>(null);

      this.gameDataCConfig.rawSubject
        .filter(gd => !(!gd))
        .subscribe(gd => {
          try {
            const fullDB = GameDatabaseLoaded(gd);

            if (this.tradeDataCConfig.rawSubject && this.tradeDataCConfig.rawSubject.value) {
              this.checkTradeMismatch(this.tradeDataCConfig.rawSubject.value);
            }

            this.gameDataCConfig.outSubject.next(fullDB);
          } catch (ex) {
            //Any errors during setup usually indicate updated client version vs. local database
            if (!this.err_handle.fatal_error) {
              //If we don't already have an error, force update game data
              this.requestGameData(true);
            }

            this.err_handle.handleDataError(ex);
            return;
          }
        });


      this.tradeDataCConfig.rawSubject
        .filter(tt => !(!tt))
        .subscribe(tt => {
          if (this.gameDataCConfig.rawSubject && this.gameDataCConfig.rawSubject.value) {
            this.checkTradeMismatch(tt);
          } else {
            //Do nothing, as the game db will happen after this and call the methods
          }
        });

      this.requestGameData();
      this.requestTradeData();
    }, (err) => {
      console.log("DB Open Error!")
      console.error(err);
    });

  }

  ngOnInit(): void {
    
  }

  private requestGameData(force = false): void {
    this.refreshEndpoint<GameDatabase>(this.gameDataCConfig, force);
  }

  private requestTradeData(force = false): void {
    this.refreshEndpoint<TradeTable>(this.tradeDataCConfig, force);
  }

  private checkTradeMismatch(tradeTable: TradeTable): void {
    if (this.gameDataCConfig.rawSubject.value.ID !== tradeTable.GameDatabaseID) {

      var reset_allowed: boolean = true;

      if (this.userDetailsSource.value.last_reset) {
        var time_since_last_s = (this.userDetailsSource.value.last_reset - Date.now()) / (1000 * 1000);
        console.log(time_since_last_s);
        if (time_since_last_s < 60 * 60) {
          console.warn("Attempted to reset twice within an hour. Something else must be wrong!");
          reset_allowed = false;
        }
      }

      if (reset_allowed) {
        //Trade table isn't for our loaded GameDB.
        //Must force refresh
        console.warn("Forcing full load due to mismatched GameDatabaseID");
        this._alreadyMismatched = true;
        //

        //Update that we forced a refresh to avoid a boot-loop
        var settings = this.userDetailsSource.value;
        settings.last_reset = Date.now();
        this.setUserDetails(settings, false);

        this.clearCache(false);
        this.requestGameData(true);
        this.requestTradeData(true);
      } else {
        this.err_handle.handleDataError(new Error("TradeTable doesn't belong to GameDatabase"));
      }
    } else {
      try {
        TradeTableLoaded(this.gameDataCConfig.currentValue, tradeTable);
        this.tradeDataCConfig.outSubject.next(tradeTable);
        this._alreadyMismatched = false;
        //Bit of a hack, but reset user properties on a good download
        this.userDetailsSource.value.last_reset = null;
        this.err_handle.clearDataError();
      } catch (ex) {
        this.err_handle.handleDataError(ex);
        return;
      }
    }
  }

  public clearCache(withUserDetails: boolean = false) {
    this.cacheDB.delete(this.dataTableName, this.gameDataCConfig.storageKey);
    this.cacheDB.delete(this.dataTableName, this.tradeDataCConfig.storageKey);
    this.gameDataCConfig.rawSubject.next(null);
    this.tradeDataCConfig.rawSubject.next(null);
    if (withUserDetails) {
      this.cacheDB.delete(this.dataTableName, this.userSettingsName);
    }
  }

  private hashString(input: string): number {
    var hash: number = 0, i: number, chr: number;
    if (input.length === 0) return hash;
    for (i = 0; i < input.length; i++) {
      chr = input.charCodeAt(i);
      hash = ((hash << 5) - hash) + chr;
      hash |= 0; // Convert to 32bit integer
    }
    return hash;
  }

  private async checkCache<T>(keyName: string): Promise<CacheRow<T>> {
    //if (!environment.production &&
    //  (keyName === this.gameDataCConfig.storageKey || keyName === this.tradeDataCConfig.storageKey))
    //  return;

    try {
      const cacheData = await this.cacheDB.getByKey(this.dataTableName, keyName);
      const cacheRow = new CacheRow<T>(cacheData);
      const dataRead = cacheRow.data; //Evaluates JSON
      return cacheRow;
    } catch (err) {
      console.warn(`Failed to load ${keyName} from cacheDB. Wiping local key.`);
      await this.cacheDB.delete(this.dataTableName, keyName);
      return null;
    }
  }

  public async getCacheAge(endpoint: CachedEndpointConfig<any>): Promise<Date> {
    return endpoint.cacheTime;
  }

  public async writeCache<T>(endpoint: CachedEndpointConfig<any>, value: T): Promise<any> {
    return await this.writeDirect<T>(endpoint.storageKey, value);
  }

  private async writeDirect<T>(keyName: string, value: T): Promise<any> {
    //if (!environment.production &&
    //  (keyName === this.gameDataCConfig.storageKey || keyName === this.tradeDataCConfig.storageKey))
    //  return;

    if (!environment.production)
      console.log(`Writing cache ${keyName}`, value);

    const recordData: ICacheRow = {
      table_name: keyName,
      data_json: JSON.stringify(value),
      last_updated: new Date()
    };

    let keyValue;
    try {
      keyValue = await this.cacheDB.getByKey(this.dataTableName, keyName);
    } catch { }

    if (keyValue) {
      this.cacheDB.update(this.dataTableName, recordData);
    } else {
      this.cacheDB.add(this.dataTableName, recordData)
    }
  }

  private async refreshEndpoint<T>(endpoint: CachedEndpointConfig<T>, force: boolean = false): Promise<void> {
    if (!force) {
      const cacheRecord = await this.checkCache<T>(endpoint.storageKey);
      if (cacheRecord) {
        const cacheAgeMS = new Date().getTime() - new Date(cacheRecord.last_updated).getTime();
        if (!cacheRecord.data) {
          console.log(`Empty cache record. Fetching new value.`);
        } else {
          endpoint.cacheTime = new Date(cacheRecord.last_updated);
          endpoint.currentValue = cacheRecord.data;
          endpoint.rawSubject.next(endpoint.currentValue);

          if (cacheAgeMS < 1000 * endpoint.expirationSeconds) {
            console.log(`Using cache for ${endpoint.storageKey} from ${cacheRecord.last_updated}`);
            return;
          } else {
            console.log(`Cache expired at ${cacheAgeMS / 1000 / 60} minutes old.`);
          }
        }
      }
    }

    console.log(`Downloading ${endpoint.storageKey} from ${endpoint.path}`);
    try {
      const webResult: any = await this._http.get<any>(this._baseURL + endpoint.path).toPromise();

      if (webResult) {
        const cacheRow = this.writeCache<T>(endpoint, webResult);
        endpoint.currentValue = webResult;
        endpoint.cacheTime = new Date();
        endpoint.rawSubject.next(webResult);

        return;
      } else {
        console.error("Recieved empty payload from server");
        this.err_handle.handleDataError(new Error("Empty/null payload returned from server."));
      }
    } catch (error) {
      this.err_handle.handleDataError(error);
    }
  }

  setUserDetails(newSettings: BasicUserSettings, forceRefresh: boolean): void {
    this.writeDirect<BasicUserSettings>(this.userSettingsName, newSettings);
    if (forceRefresh) {
      this.userDetailsSource.next(newSettings);
    }
  }

  setLoadouts(newLoadouts: LoadoutStorage, forceRefresh: boolean): void {
    this.writeDirect<LoadoutStorage>(this.loadoutSettingsName, newLoadouts);
    if (forceRefresh) {
      this.loadoutDataSource.next(newLoadouts);
    }
  }
}

interface ICacheRow {
  table_name: string;
  data_json: string;
  last_updated: Date;
}

class CacheRow<T> {
  table_name: string;
  data_json: string;
  last_updated: Date;

  constructor(row: ICacheRow) {
    this.table_name = row.table_name;
    this.data_json = row.data_json;
    this.last_updated = row.last_updated;
  }

  get data(): T {
    return <T>JSON.parse(this.data_json);
  }
}

interface CachedEndpointConfig<T> {
  path: string;
  expirationSeconds: number;
  storageKey: string;
  rawSubject?: BehaviorSubject<T>;
  outSubject?: ReplaySubject<T>;
  currentValue?: T;
  cacheTime?: Date;
}
