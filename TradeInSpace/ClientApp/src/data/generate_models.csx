using Microsoft.CodeAnalysis;
using System.Reflection;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System;

var outPath = Path.Combine(Path.GetDirectoryName(ProjectFilePath), "bin", "Debug", "netcoreapp2.1");
var skipProps = new[] { "GameDB", "TradeTables" };

//Output["generated_models.ts"].WriteLine($"//---Start--- {DateTime.Now.ToString()}");

try
{
	var tisModels = Assembly.LoadFile(Path.Combine(outPath, "TradeInSpace.Models.dll"));

	Output["generated_models.ts"].WriteLine($"//Generated: {DateTime.Now}");
	Output["generated_models.ts"].WriteLine($"//{Path.Combine(outPath, "TradeInSpace.Models.dll")}");

	var nSoft = Assembly.Load(new AssemblyName("Newtonsoft.Json"));

	var ignoreAttributeType = nSoft.GetType("JsonIgnoreAttribute");

	var tisExportAttributeType = tisModels.GetType("TradeInSpace.Models.GenerateTSAttribute");
	var tisGameRecordType = tisModels.GetType("TradeInSpace.Models.GameRecord");
	var tisGameDatabaseType = tisModels.GetType("TradeInSpace.Models.GameDatabase");

	var exportTypes = tisModels.GetTypes().Where(t => t.GetCustomAttribute(tisExportAttributeType) != null);

	Output["generated_models.ts"].WriteLine("import { KeyModel } from './models';");
	Output["generated_models.ts"].WriteLine("import { EquipmentProperties } from './client_models';");
	Output["generated_models.ts"].WriteLine("import { LoadoutSlotDTO, LoadoutEntryDTO } from './loadout';");
	Output["generated_models.ts"].WriteLine("");

	var mapperLookupSection = new StringWriter();
	var mapperMappingSection = new StringWriter();

	foreach (var exportType in exportTypes)
	{
		if (exportType.IsEnum)
		{
			Output["generated_models.ts"].WriteLine($"export enum {exportType.Name} {{");
			var enumFields = exportType.GetFields(BindingFlags.Static | BindingFlags.Public);
			foreach (var enumField in enumFields)
			{
				Output["generated_models.ts"].WriteLine($"  {enumField.Name} = {(int)Enum.Parse(exportType, enumField.Name)},");
			}
			Output["generated_models.ts"].WriteLine($"}}");
		}
		else
		{
			var isGameRecord = exportType.IsSubclassOf(tisGameRecordType);

			string extends = (isGameRecord ? "implements KeyModel" : "");
			Output["generated_models.ts"].WriteLine($"export class {exportType.Name} {extends} {{");
			var exportProperties = exportType.GetProperties();
			foreach (var exportProperty in exportProperties)
			{
				var propType = exportProperty.PropertyType;
				var typeName = "any";
				var propName = exportProperty.Name;

				if (exportProperty.PropertyType.IsGenericType)
				{
					propType = propType.GetGenericArguments().First();
				}


				switch (propType.Name)
				{
					case "String":
						typeName = "string";
						break;
					case "Int32":
					case "Int64":
					case "Single":
					case "Double":
						typeName = "number";
						break;
					case "Boolean":
						typeName = "boolean";
						break;
					default:
						if (propType.Namespace == "TradeInSpace.Models")
						{
							typeName = propType.Name;
						}
						break;
				}

				if (exportProperty.PropertyType.Name.Contains("Nullable"))
				{
					propName += "?";
				}
				else if (exportProperty.PropertyType.IsGenericType)
				{
					typeName += "[]";
				}

				if (skipProps.Contains(exportProperty.Name)) continue;

				var propertyAttributes = exportProperty.GetCustomAttributes();


				if (propertyAttributes.Any(a => a.GetType().Name == "ForeignKeyAttribute"))
				{
					mapperLookupSection.WriteLine($"  {propName} : {typeName}; //{propType.Name}");
				}

				var propertyAttr = propertyAttributes.FirstOrDefault(p => p.GetType().Name == "TSPropertyAttribute");
				if (propertyAttr != null)
				{
					var propertyName = propertyAttr.GetType().GetProperty("PropertyType").GetValue(propertyAttr) as string;
					if(!string.IsNullOrWhiteSpace(propertyName))
						typeName = propertyName;
				}

				//if (propertyAttributes.Any(a => a.AttributeType.Name == "JsonIgnoreAttribute")) continue;

				Output["generated_models.ts"].WriteLine($"  {propName} : {typeName}; //{propType.Name}");
			}
			Output["generated_models.ts"].WriteLine($"}}");
		}
	}

}
catch (Exception ex)
{
	Output["generated_models.ts"].WriteLine($"//Error {ex.Message}");
}

Output["generated_models.ts"].WriteLine("//---End---");


