"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DisplayShip = void 0;
var client_data_1 = require("./client_data");
var client_models_1 = require("./client_models");
var DisplayShip = /** @class */ (function () {
    function DisplayShip(ShipLoadout, Ship) {
        var _this = this;
        this.ShipLoadout = ShipLoadout;
        this.Ship = Ship;
        this.Manufacturer = Ship.Manufacturer;
        this.Name = ShipLoadout.name;
        this.Key = ShipLoadout.key;
        this.Cargo = Ship.CargoCapacity;
        this.DPS = this.ShipLoadout.total_sustain;
        this.Burst = this.ShipLoadout.total_volley;
        this.Missile = this.ShipLoadout.total_missile;
        this.DPS_Flat = client_models_1.CollapseDamage(this.DPS);
        this.Burst_Flat = client_models_1.CollapseDamage(this.Burst);
        this.Missile_Flat = client_models_1.CollapseDamage(this.Missile);
        var flightControllers = this.ShipLoadout.find_slots_equipment_type(client_data_1.ClientData.EquipmentTypes.Flight);
        var shieldControllers = this.ShipLoadout.find_slots_equipment_type(client_data_1.ClientData.EquipmentTypes.Shield);
        var quantumDrives = this.ShipLoadout.find_slots_equipment_type(client_data_1.ClientData.EquipmentTypes.QuantumDrive);
        if (flightControllers.length > 0) {
            var fcEq = flightControllers[0].attached_equipment;
            this.CruiseSpeed = fcEq.Properties.IFCS.MaxSpeed;
            this.MaxSpeed = fcEq.Properties.IFCS.MaxBoostSpeed;
        }
        this.HP = 0;
        this.ShipLoadout.all_slots
            .forEach(function (s) {
            _this.HP += s.health;
        });
        this.Shields = 0;
        shieldControllers.forEach(function (sc) {
            _this.Shields += sc.attached_equipment.Properties.Shield.MaxShield;
        });
        this.QT = false;
        this.QTSpeed = 0;
        this.QTAccel = 0;
        quantumDrives.forEach(function (qd) {
            _this.QT = true;
            _this.QTSpeed = Math.max(_this.QTSpeed, qd.attached_equipment.Properties.QDrive.DriveSpeed);
            _this.QTAccel = Math.max(_this.QTAccel, qd.attached_equipment.Properties.QDrive.S2AccelRate);
        });
    }
    return DisplayShip;
}());
exports.DisplayShip = DisplayShip;
//# sourceMappingURL=display_ship.js.map