import { FilterMetadata } from 'primeng/api';
import { FilterParams } from './client_models';
import { LoadoutSlotDTO, LoadoutEntryDTO } from './loadout';

export function MapKey<T extends KeyModel>(models: T[]): KeyMapping<T> {
  var result: KeyMapping<T> = {};
  for (var index in models) {
    var model = models[index];
    result[model.Key] = model;
  }
  return result;
}

export function MapId<T extends KeyModel>(models: T[]): IdMapping<T> {
  var result: IdMapping<T> = {};
  for (var index in models) {
    var model = models[index];
    result[model.ID] = model;
  }
  return result;
}

export interface KeyMapping<T> {
  [key: string]: T;
}

export interface IdMapping<T> {
  [id: number]: T;
}

export interface KeyModel {
  ID: number;
  Key: string;
  SCRecordID?: string;
}

export interface BasicUserSettings {
  balance: number;
  custom_capacity: number;
  owned_ships: string[];
  active_ship: string;
  last_filter: FilterParams;
  current_location: string;

  use_balance: boolean;
  use_location: boolean;
  use_capacity: boolean;

  last_reset?: number;

  shopping_cart: ShoppingCartEntry[];
}

export interface LoadoutStorage {
  loadouts: SavedLoadout[];
}

export interface SavedLoadout {
  name: string;
  description: string;
  notes: string;
  owned: boolean;
  ship_key: string;
  loadout_entries: LoadoutEntryDTO[];
}

export interface ShoppingCartEntry {
  equipment_key: string;
  location: string;
  price: number;
  quantity: number;
}
