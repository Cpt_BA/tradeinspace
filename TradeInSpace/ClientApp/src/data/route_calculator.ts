import { Injectable, ChangeDetectorRef, OnInit } from '@angular/core';

import { BehaviorSubject } from 'rxjs';
import { DataCacheService } from './data_cache';
import { CalculatedRoute } from './client_models';
import { TradeShop, TradeType, TradeTableEntry } from './generated_models';
import { ClientData } from './client_data';
import { TISBaseComponent } from '../app/tis_base.component';
import { ActivatedRoute } from '@angular/router';
import { MessageService } from 'primeng/api';

@Injectable()
export class RouteCalculatorService extends TISBaseComponent {
  public available_routes: BehaviorSubject<CalculatedRoute[]> = new BehaviorSubject<CalculatedRoute[]>([]);

  constructor(http_route: ActivatedRoute,
    dataStore: DataCacheService,
    toast: MessageService) {
    super(http_route, dataStore, null, toast, "Route Calculator");
    this.ngOnInit();
  }

  onAllDataGameUpdated(): void {
    this.calculateRoutes();
  }

  calculateRoutes() {
    if (!this.gameData || !this.tradeData) return;

    console.log("Calculating Main Routes");

    const buys: TradeTableEntry[] = this.tradeData.Entries.filter((val) => {
      return (val.BadTrade === false) &&
        val.Type == TradeType.Buy &&
        ClientData.TradeShopTypes.includes(val.Station.ShopType)
    });
    const sells: TradeTableEntry[] = this.tradeData.Entries.filter((val) => {
      return (val.BadTrade === false) &&
        val.Type == TradeType.Sell &&
        ClientData.TradeShopTypes.includes(val.Station.ShopType)
    });

    const finalRoutes: CalculatedRoute[] = new Array<CalculatedRoute>();

    for (const buyIndex in buys) {
      const buyOrder = buys[buyIndex];
      const buyStation: TradeShop = buyOrder.Station;

      for (const sellIndex in sells) {
        const sellOrder = sells[sellIndex];
        const sellStation: TradeShop = sellOrder.Station;

        if (buyOrder.ItemID !== sellOrder.ItemID) continue;
        if (buyOrder.MinUnitPrice > sellOrder.MaxUnitPrice) continue;

        const tradeItem = buyOrder.Item;

        const avgBuy = (buyOrder.MaxUnitPrice + buyOrder.MinUnitPrice) / 2;
        const avgSell = (sellOrder.MaxUnitPrice + sellOrder.MinUnitPrice) / 2;

        const avgUnitProfit = avgSell - avgBuy;
        const maxUnitProfit = sellOrder.MaxUnitPrice - buyOrder.MinUnitPrice;

        const calcRoute: CalculatedRoute = {
          from: buyStation,
          from_name: buyStation.Name,
          to: sellStation,
          to_name: sellStation.Name,
          item: tradeItem,
          item_name: tradeItem.Name,
          buy_entry: buyOrder,
          sell_entry: sellOrder,
          avg_profit_pct: (avgUnitProfit / avgBuy * 100),
          max_profit_pct: (maxUnitProfit / buyOrder.MinUnitPrice) * 100,
          min_buy: buyOrder.MinUnitPrice,
          avg_buy: avgBuy,
          max_buy: buyOrder.MaxUnitPrice,
          min_sell: sellOrder.MinUnitPrice,
          avg_sell: avgSell,
          max_sell: sellOrder.MaxUnitPrice,
          min_profit: sellOrder.MinUnitPrice - buyOrder.MaxUnitPrice,
          avg_profit: avgUnitProfit,
          max_profit: maxUnitProfit,
          buy_rate: buyOrder.UnitCapacityRate,
          sell_rate: sellOrder.UnitCapacityRate,
          avg_rate: (buyOrder.UnitCapacityRate + sellOrder.UnitCapacityRate) / 2
        };

        finalRoutes.push(calcRoute);
      }
    }


    console.log("Calculated " + finalRoutes.length + " routes");
    //this.available_routes = of(final_routes);
    this.available_routes.next(finalRoutes);
    //this.available_routes.complete();
  }
}
