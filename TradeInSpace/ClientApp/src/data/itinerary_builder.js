"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ItineraryBuilder = void 0;
var generated_models_1 = require("./generated_models");
var client_models_1 = require("./client_models");
var model_utils_1 = require("../utilities/model_utils");
var scaleunit_pipe_1 = require("../pipes/scaleunit_pipe");
var ItineraryBuilder = /** @class */ (function () {
    function ItineraryBuilder() {
    }
    ItineraryBuilder.CalculateForRoute = function (gameData, inShip, withDrive, route, userData, filterSettings) {
        var itinerary = {
            steps: [],
            totalDurationS: 0,
            totalBuyS: 0,
            totalTransitS: 0,
            totalShipS: 0,
            totalSellS: 0,
            totalJumps: 0
        };
        if (!filterSettings.looping) {
            if (userData.current_location && userData.use_location && userData.current_location !== route.from.Key) {
                this.TerminalToTerminal(gameData, inShip, withDrive, gameData.ShopKeyMap[userData.current_location], route.from, function (newStep) { return itinerary.steps.push(newStep); });
            }
        }
        itinerary.steps.push(this.TradeTerminal(gameData, route.buy_entry, route.cargo_units));
        this.TerminalToTerminal(gameData, inShip, withDrive, route.from, route.to, function (newStep) { return itinerary.steps.push(newStep); });
        itinerary.steps.push(this.TradeTerminal(gameData, route.sell_entry, route.cargo_units));
        if (filterSettings.looping) {
            this.TerminalToTerminal(gameData, inShip, withDrive, route.to, route.from, function (newStep) { return itinerary.steps.push(newStep); });
        }
        itinerary.steps.forEach(function (step) {
            itinerary.totalDurationS += step.durationS;
            switch (step.stepType) {
                case client_models_1.ItineraryStepType.BuyGoods:
                    itinerary.totalBuyS += step.durationS;
                    break;
                case client_models_1.ItineraryStepType.Running:
                case client_models_1.ItineraryStepType.Transit:
                    itinerary.totalTransitS += step.durationS;
                    break;
                case client_models_1.ItineraryStepType.ShipFlight:
                    itinerary.totalShipS += step.durationS;
                    break;
                case client_models_1.ItineraryStepType.QuantumJump:
                    itinerary.totalShipS += step.durationS;
                    itinerary.totalJumps += 1;
                    break;
                case client_models_1.ItineraryStepType.SellGoods:
                    itinerary.totalSellS += step.durationS;
                    break;
            }
        });
        route.itinerary = itinerary;
        return itinerary;
    };
    ItineraryBuilder.TerminalToTerminal = function (gameData, inShip, withDrive, from, to, fnAddElem) {
        //const fromBody: SystemBody = gameData.SystemBodies.find(b => b.SystemLocation == from.Location.ParentBody);
        //const toBody: SystemBody = gameData.SystemBodies.find(b => b.SystemLocation == to.Location.ParentBody);
        var fromBody = gameData.BodyKeyMap[from.Location.ParentBody.Key];
        var toBody = gameData.BodyKeyMap[to.Location.ParentBody.Key];
        if (!fromBody)
            debugger;
        if (!toBody)
            debugger;
        var fromRoot = model_utils_1.ModelUtils.getRootBody(gameData, fromBody);
        var toRoot = model_utils_1.ModelUtils.getRootBody(gameData, toBody);
        var currentBody = fromBody;
        this.StationToOrbit(gameData, inShip, from, fnAddElem);
        if (fromRoot != toRoot) {
            fnAddElem(this.BodyToBody(gameData, withDrive, currentBody, toRoot));
            currentBody = toRoot;
        }
        if (currentBody != toBody) {
            fnAddElem(this.BodyToBody(gameData, withDrive, currentBody, toBody));
            currentBody = toBody;
        }
        this.OrbitToStation(gameData, inShip, withDrive, to, fnAddElem);
    };
    ItineraryBuilder.TradeTerminal = function (gameData, entry, units) {
        var entryBody = entry.Station.Location.ParentBody;
        return {
            stepType: entry.Type === generated_models_1.TradeType.Sell ? client_models_1.ItineraryStepType.SellGoods : client_models_1.ItineraryStepType.BuyGoods,
            title: (entry.Type === generated_models_1.TradeType.Sell ? 'Sell' : 'Buy') + " " + scaleunit_pipe_1.fmtNumber(units, true, " units", "", true) + " of " + entry.Item.Name + " @ " + scaleunit_pipe_1.fmtNumber(entry.UnitCapacityRate / 100, false, ' scu/min') + ".",
            from: entryBody,
            to: entryBody,
            durationS: (units / entry.UnitCapacityRate) * 60
        };
    };
    ItineraryBuilder.BodyToBody = function (gameData, withDrive, fromBody, toBody) {
        var qProps = withDrive.Properties.QDrive;
        var jumpDetails = model_utils_1.ModelUtils.calculateQuantumJump(fromBody, toBody, withDrive);
        return {
            stepType: client_models_1.ItineraryStepType.QuantumJump,
            title: "QT Jump from " + fromBody.Name + " -> " + toBody.Name + ". " + qProps.SpoolTime + " Second Spool.",
            from: fromBody.SystemLocation,
            to: toBody.SystemLocation,
            durationS: jumpDetails.total_time + qProps.SpoolTime
        };
    };
    ItineraryBuilder.StationToOrbit = function (gameData, inShip, station, fnAddElem) {
        var stationBody = station.Location.ParentBody;
        if (station.RunningDurationS && station.RunningDurationS !== 0) {
            fnAddElem({
                stepType: client_models_1.ItineraryStepType.Running,
                from: stationBody,
                to: station.Location,
                title: "Run from trade terminal.",
                durationS: station.RunningDurationS
            });
        }
        //We check !IsHidden, because if it is a hidden location, we assume all transit time is just finding the location, so theres no out-going transit time.
        if (!station.Location.IsHidden && station.TransitDurationS && station.TransitDurationS !== 0) {
            fnAddElem({
                stepType: client_models_1.ItineraryStepType.Transit,
                from: station.Location,
                to: stationBody,
                title: "Transit from station to spaceport." + (station.TransitNotes ? "(" + station.TransitNotes + ")" : ""),
                durationS: station.TransitDurationS
            });
        }
        fnAddElem({
            stepType: client_models_1.ItineraryStepType.ShipFlight,
            from: stationBody,
            to: stationBody,
            title: "Takeoff in ship",
            durationS: 60 //TODO: Allow user to configure landing time
        });
        var leave = this.LeaveBody(gameData, inShip, stationBody);
        fnAddElem(leave);
    };
    ItineraryBuilder.OrbitToStation = function (gameData, inShip, withDrive, station, fnAddElem) {
        var qProps = withDrive.Properties.QDrive;
        var stationBody = station.Location.ParentBody;
        var stationOrbitalBody = gameData.BodyKeyMap[stationBody.Key];
        if (station.Location.IsHidden) {
            fnAddElem({
                stepType: client_models_1.ItineraryStepType.ShipFlight,
                from: stationBody,
                to: stationBody,
                title: "Travel to hidden location - " + station.Name + "." + (station.TravelNotes ? "(" + station.TravelNotes + ")" : ""),
                durationS: 300 //station.TravelDurationS //Hidden locations take a fixed 5 minutes for now
            });
        }
        else {
            fnAddElem({
                stepType: client_models_1.ItineraryStepType.QuantumJump,
                from: stationBody,
                to: stationBody,
                title: "QT To within " + station.ClosestApproachKM + " KM.  " + qProps.SpoolTime + " Second Spool Time and " + qProps.CooldownTime + " Second Cooldown.",
                durationS: qProps.SpoolTime + qProps.CooldownTime
            });
            //TODO: Account for atmospheric delay better.
            if (stationOrbitalBody && stationOrbitalBody.AtmosphereHeightKM > 0) {
                fnAddElem({
                    stepType: client_models_1.ItineraryStepType.ShipFlight,
                    from: stationBody,
                    to: stationBody,
                    title: "Fly in-atmosphere remaining " + station.ClosestApproachKM + " KM to " + station.Name,
                    durationS: station.ClosestApproachKM * 5
                    //TODO: Account for ship speed
                });
            }
            else {
                fnAddElem({
                    stepType: client_models_1.ItineraryStepType.ShipFlight,
                    from: stationBody,
                    to: stationBody,
                    title: "Fly remaining " + station.ClosestApproachKM + " KM to " + station.Name,
                    durationS: station.ClosestApproachKM //most ships hit roughly 1 km/s
                    //TODO: Account for ship speed
                });
            }
        }
        fnAddElem({
            stepType: client_models_1.ItineraryStepType.ShipFlight,
            from: stationBody,
            to: stationBody,
            title: "Land ship",
            durationS: 60 //TODO: Allow user to configure landing time
        });
        if (station.TransitDurationS && station.TransitDurationS !== 0) {
            fnAddElem({
                stepType: client_models_1.ItineraryStepType.Transit,
                from: stationBody,
                to: stationBody,
                title: "Transit from spaceport to shop." + (station.TransitNotes ? "(" + station.TransitNotes + ")" : ""),
                durationS: station.TransitDurationS
            });
        }
        if (station.RunningDurationS && station.RunningDurationS !== 0) {
            fnAddElem({
                stepType: client_models_1.ItineraryStepType.Running,
                from: stationBody,
                to: stationBody,
                title: "Running to trade terminal.",
                durationS: station.RunningDurationS
            });
        }
    };
    ItineraryBuilder.LeaveBody = function (gameData, inShip, body) {
        var fullBody = gameData.SystemBodies.filter(function (b) { return b.SystemLocationID === body.ID; })[0];
        if (!fullBody)
            debugger;
        if (fullBody.AtmosphereHeightKM && fullBody.AtmosphereHeightKM !== 0) {
            return {
                stepType: client_models_1.ItineraryStepType.ShipFlight,
                from: body,
                to: body,
                title: "Leave atmosphere @ " + fullBody.AtmosphereHeightKM.toFixed(0) + " KM",
                durationS: fullBody.AtmosphereHeightKM //most ships hit roughly 1 km/s
            };
        }
        else {
            return {
                stepType: client_models_1.ItineraryStepType.ShipFlight,
                from: body,
                to: body,
                title: "Get jump visibility to target.",
                durationS: 30
            };
        }
    };
    return ItineraryBuilder;
}());
exports.ItineraryBuilder = ItineraryBuilder;
//# sourceMappingURL=itinerary_builder.js.map