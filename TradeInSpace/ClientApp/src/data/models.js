"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.MapId = exports.MapKey = void 0;
function MapKey(models) {
    var result = {};
    for (var index in models) {
        var model = models[index];
        result[model.Key] = model;
    }
    return result;
}
exports.MapKey = MapKey;
function MapId(models) {
    var result = {};
    for (var index in models) {
        var model = models[index];
        result[model.ID] = model;
    }
    return result;
}
exports.MapId = MapId;
//# sourceMappingURL=models.js.map