"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ClientData = void 0;
var client_models_1 = require("./client_models");
var generated_models_1 = require("./generated_models");
var ClientData = /** @class */ (function () {
    function ClientData() {
    }
    ClientData.GetServiceFlaNames = function (flags) {
        switch (flags) {
            case generated_models_1.ServiceFlags.None:
                return "None";
            //Admin
            case generated_models_1.ServiceFlags.Trade:
                return "Trade Terminal";
            case generated_models_1.ServiceFlags.Refinery:
                return "Refinery Terminal";
            case generated_models_1.ServiceFlags.Fine:
                return "Fine Payment Terminal";
            case generated_models_1.ServiceFlags.PackageTerminal:
                return "Package Terminal";
            case generated_models_1.ServiceFlags.MissionGiver:
                return "Mission Giver";
            case generated_models_1.ServiceFlags.GreenZone:
                return "Green/Armistice Zone";
            //Ship
            case generated_models_1.ServiceFlags.ShipLanding:
                return "Ship Landing";
            case generated_models_1.ServiceFlags.ShipClaim:
                return "Ship Claiming & Customization";
            case generated_models_1.ServiceFlags.ShipPurchase:
                return "Ship Purchasing";
            case generated_models_1.ServiceFlags.ShipEquipment:
                return "Ship Equipment";
            case generated_models_1.ServiceFlags.ShipRental:
                return "Ship Rental";
            case generated_models_1.ServiceFlags.FPSArmor:
                return "FPS Armor";
            case generated_models_1.ServiceFlags.FPSWeapon:
                return "FPS Weapons";
            case generated_models_1.ServiceFlags.FPSEquipment:
                return "FPS Equipment/Utilities";
            case generated_models_1.ServiceFlags.FPSClothing:
                return "FPS Clothing";
            case generated_models_1.ServiceFlags.FPSRespawnPoint:
                return "FPS Respawn Point";
            case generated_models_1.ServiceFlags.FPSCrypto:
                return "FPS Crypto Modules";
            case generated_models_1.ServiceFlags.FPSFood:
                return "Food";
            case generated_models_1.ServiceFlags.Hacking:
                return "Security Hacking";
        }
        return "Multiple";
    };
    ClientData.RouteRankProperties = [
        {
            name: "max_unit_cost",
            fnValue: function (route) { return route.max_buy; },
            fnMixColor: RedToGreen
        },
        {
            name: "avg_profit_pct",
            fnValue: function (route) { return route.avg_profit_pct; },
            fnMixColor: RedToGreen
        },
        {
            name: "avg_profit",
            fnValue: function (route) { return route.avg_profit; },
            fnMixColor: RedToGreen
        },
        {
            name: "buy_rate",
            fnValue: function (route) { return route.buy_rate; },
            fnMixColor: RedToGreen
        },
        {
            name: "sell_rate",
            fnValue: function (route) { return route.sell_rate; },
            fnMixColor: RedToGreen
        },
        {
            name: "total_time",
            fnValue: function (route) { return route.itinerary.totalDurationS; },
            fnMixColor: GreenToRed
        },
        {
            name: "total_buy",
            fnValue: function (route) { return route.itinerary.totalBuyS; },
            fnMixColor: GreenToRed
        },
        {
            name: "total_transit",
            fnValue: function (route) { return route.itinerary.totalTransitS; },
            fnMixColor: GreenToRed
        },
        {
            name: "total_ship",
            fnValue: function (route) { return route.itinerary.totalShipS; },
            fnMixColor: GreenToRed
        },
        {
            name: "total_sell",
            fnValue: function (route) { return route.itinerary.totalSellS; },
            fnMixColor: GreenToRed
        },
        {
            name: "total_jumps",
            fnValue: function (route) { return route.itinerary.totalJumps; },
            fnMixColor: GreenToRed
        },
        {
            name: "avg_hold_cost",
            fnValue: function (route) { return (route.cargo_units * route.avg_buy); },
            fnMixColor: RedToGreen
        },
        {
            name: "avg_hold_profit",
            fnValue: function (route) { return (route.cargo_units * route.avg_profit); },
            fnMixColor: RedToGreen
        },
        {
            name: "max_profit_rate",
            fnValue: function (route) { return (route.cargo_units * route.max_profit) / (route.itinerary.totalDurationS / 60); },
            fnMixColor: RedToGreen
        }
    ];
    ClientData.ModeOptions = [
        { label: "One-Way", value: false },
        { label: "Loop", value: true }
    ];
    ClientData.SizeFilterOptions = [
        { label: "None", value: null },
        { label: "Any/Tiny", value: generated_models_1.PadSize.Tiny },
        { label: "X-Small", value: generated_models_1.PadSize.XSmall },
        { label: "Small", value: generated_models_1.PadSize.Small },
        { label: "Medium", value: generated_models_1.PadSize.Medium },
        { label: "Large", value: generated_models_1.PadSize.Large },
        { label: "X-Large", value: generated_models_1.PadSize.XLarge },
    ];
    ClientData.SizeFilterOptionsSmall = [
        { label: "N/A", value: null },
        { label: "All", value: generated_models_1.PadSize.Tiny },
        { label: "XS", value: generated_models_1.PadSize.XSmall },
        { label: "S", value: generated_models_1.PadSize.Small },
        { label: "M", value: generated_models_1.PadSize.Medium },
        { label: "L", value: generated_models_1.PadSize.Large },
        { label: "XL", value: generated_models_1.PadSize.XLarge },
    ];
    ClientData.SizeSelectOptions = [
        { label: "Tiny", value: generated_models_1.PadSize.Tiny },
        { label: "X-Small", value: generated_models_1.PadSize.XSmall },
        { label: "Small", value: generated_models_1.PadSize.Small },
        { label: "Medium", value: generated_models_1.PadSize.Medium },
        { label: "Large", value: generated_models_1.PadSize.Large },
        { label: "X-Large", value: generated_models_1.PadSize.XLarge },
    ];
    ClientData.EquipmentSizes = [
        { label: "0", value: 0 },
        { label: "1", value: 1 },
        { label: "2", value: 2 },
        { label: "3", value: 3 },
        { label: "4", value: 4 },
        { label: "5", value: 5 },
        { label: "6", value: 6 },
        { label: "7", value: 7 },
        { label: "8", value: 8 },
        { label: "9", value: 9 },
        { label: "10", value: 10 }
    ];
    ClientData.EquipmentGrades = [
        { label: "1", value: 1 },
        { label: "2", value: 2 },
        { label: "3", value: 3 },
        { label: "4", value: 4 },
    ];
    ClientData.GunDamageModes = [
        { label: "Single Fire", value: client_models_1.DamageMode.Single },
        { label: "Charge", value: client_models_1.DamageMode.Charge },
        { label: "Automatic", value: client_models_1.DamageMode.Automatic },
        { label: "Scattergun", value: client_models_1.DamageMode.Scattergun }
    ];
    ClientData.FoodTypes = [
        { label: "Food", value: "Food" },
        { label: "Drink", value: "Drink" },
    ];
    ClientData.ClothingTypes = [
        { label: "Hat", value: "Char_Clothing_Hat" },
        { label: "Torso 1", value: "Char_Clothing_Torso_0" },
        { label: "Torso 2", value: "Char_Clothing_Torso_1" },
        { label: "Torso 3", value: "Char_Clothing_Torso_2" },
        { label: "Legs", value: "Char_Clothing_Legs" },
        { label: "Hands", value: "Char_Clothing_Hands" },
        { label: "Feet", value: "Char_Clothing_Feet" },
    ];
    ClientData.ArmorTypes = [
        { label: "Helmet", value: "Char_Armor_Helmet" },
        { label: "Torso", value: "Char_Armor_Torso" },
        { label: "Arms", value: "Char_Armor_Arms" },
        { label: "Legs", value: "Char_Armor_Legs" },
    ];
    ClientData.DamageTypes = [
        { label: "Physical", value: "Physical" },
        { label: "Energy", value: "Energy" },
        { label: "Distortion", value: "Distortion" },
        { label: "Thermal", value: "Thermal" },
        { label: "Biochemical", value: "Biochemical" },
        { label: "Stun", value: "Stun" },
    ];
    ClientData.MissionTypes = [
        { label: "Appointment", value: "Appointment", icon: "nf-mdi-calendar_clock" },
        { label: "Bounty Hunter", value: "Bounty Hunter", icon: "nf-fa-user_times" },
        { label: "Delivery", value: "Delivery", icon: "nf-oct-package" },
        { label: "ECN Alert", value: "ECN Alert", icon: "nf-oct-alert" },
        //{ label: "Investigate", value: "Investigate", icon: "nf-oct-question" }, //Disabled for now, only one is a hidden mission
        { label: "Investigation", value: "Investigation", icon: "nf-oct-question" },
        { label: "Maintenance", value: "Maintenance", icon: "nf-fa-wrench" },
        { label: "Mercenary", value: "Mercenary", icon: "nf-mdi-target" },
        { label: "Priority", value: "Priority", icon: "nf-mdi-comment_alert" },
        //{ label: "Racing", value: "Racing", icon: "nf-mdi-car_sports" }, //Sad fance no racing missions yet 
        { label: "Research", value: "Research", icon: "nf-oct-beaker" },
        { label: "Search", value: "Search", icon: "nf-fa-binoculars" },
        { label: "Service Beacons", value: "Service Beacons", icon: "nf-mdi-account_alert" }
    ];
    ClientData.ServiceOptions = [
        { label: ClientData.GetServiceFlaNames(generated_models_1.ServiceFlags.Fine), value: generated_models_1.ServiceFlags.Fine },
        { label: ClientData.GetServiceFlaNames(generated_models_1.ServiceFlags.FPSArmor), value: generated_models_1.ServiceFlags.FPSArmor },
        { label: ClientData.GetServiceFlaNames(generated_models_1.ServiceFlags.FPSClothing), value: generated_models_1.ServiceFlags.FPSClothing },
        { label: ClientData.GetServiceFlaNames(generated_models_1.ServiceFlags.FPSCrypto), value: generated_models_1.ServiceFlags.FPSCrypto },
        { label: ClientData.GetServiceFlaNames(generated_models_1.ServiceFlags.FPSEquipment), value: generated_models_1.ServiceFlags.FPSEquipment },
        { label: ClientData.GetServiceFlaNames(generated_models_1.ServiceFlags.FPSFood), value: generated_models_1.ServiceFlags.FPSFood },
        { label: ClientData.GetServiceFlaNames(generated_models_1.ServiceFlags.FPSRespawnPoint), value: generated_models_1.ServiceFlags.FPSRespawnPoint },
        { label: ClientData.GetServiceFlaNames(generated_models_1.ServiceFlags.FPSWeapon), value: generated_models_1.ServiceFlags.FPSWeapon },
        { label: ClientData.GetServiceFlaNames(generated_models_1.ServiceFlags.GreenZone), value: generated_models_1.ServiceFlags.GreenZone },
        { label: ClientData.GetServiceFlaNames(generated_models_1.ServiceFlags.MissionGiver), value: generated_models_1.ServiceFlags.MissionGiver },
        { label: ClientData.GetServiceFlaNames(generated_models_1.ServiceFlags.PackageTerminal), value: generated_models_1.ServiceFlags.PackageTerminal },
        { label: ClientData.GetServiceFlaNames(generated_models_1.ServiceFlags.Refinery), value: generated_models_1.ServiceFlags.Refinery },
        { label: ClientData.GetServiceFlaNames(generated_models_1.ServiceFlags.ShipClaim), value: generated_models_1.ServiceFlags.ShipClaim },
        { label: ClientData.GetServiceFlaNames(generated_models_1.ServiceFlags.ShipEquipment), value: generated_models_1.ServiceFlags.ShipEquipment },
        { label: ClientData.GetServiceFlaNames(generated_models_1.ServiceFlags.ShipLanding), value: generated_models_1.ServiceFlags.ShipLanding },
        { label: ClientData.GetServiceFlaNames(generated_models_1.ServiceFlags.ShipPurchase), value: generated_models_1.ServiceFlags.ShipPurchase },
        { label: ClientData.GetServiceFlaNames(generated_models_1.ServiceFlags.ShipRental), value: generated_models_1.ServiceFlags.ShipRental },
        { label: ClientData.GetServiceFlaNames(generated_models_1.ServiceFlags.Trade), value: generated_models_1.ServiceFlags.Trade }
    ];
    ClientData.TradeShopTypes = [
        generated_models_1.ShopType.AdminTerminal,
        generated_models_1.ShopType.FenceTerminal,
        generated_models_1.ShopType.CommExTransfers,
        generated_models_1.ShopType.TDD,
        generated_models_1.ShopType.CargoOffice
    ];
    ClientData.SystemBodyLocationTypes = [
        generated_models_1.LocationType.Planet,
        generated_models_1.LocationType.Moon,
        generated_models_1.LocationType.LagrangePoint,
        generated_models_1.LocationType.AsteroidField
    ];
    ClientData.EquipmentTypes = {
        QuantumDrive: "QuantumDrive",
        QTank: "QuantumFuelTank",
        HTank: "FuelTank",
        Shield: "Shield",
        Flight: "FlightController",
        Power: "PowerPlant",
        Cooler: "Cooler",
        Armor: "Armor",
        MiningLaser: "WeaponMining",
        WeaponShip: "WeaponGun",
        Vehicle: "NOITEM_Vehicle",
        Food: "Food",
        Drink: "Drink",
        Gadget: "Gadget",
        Missile: "Missile",
        Paint: "Paints",
        MiningModifier: "MiningModifier",
        FPS_Undersuit: "Char_Armor_Undersuit",
        FPS_Helmet: "Char_Armor_Helmet",
        FPS_Torso: "Char_Armor_Torso",
        FPS_Arms: "Char_Armor_Arms",
        FPS_Legs: "Char_Armor_Legs",
        FPS_Weapon: "WeaponPersonal",
        FPS_Hacking: "RemovableChip",
        FPS_Consumable: "FPS_Consumable",
        Clothing_Hat: "Char_Clothing_Hat",
        Clothing_Torso0: "Char_Clothing_Torso_0",
        Clothing_Torso1: "Char_Clothing_Torso_1",
        Clothing_Torso2: "Char_Clothing_Torso_2",
        Clothing_Arms: "Char_Clothing_Arms",
        Clothing_Hands: "Char_Clothing_Hands",
        Clothing_Legs: "Char_Clothing_Legs",
        Clothing_Feet: "Char_Clothing_Feet"
    };
    ClientData.EquipmentSubtypes = {
        VehicleSpaceship: "Vehicle_Spaceship",
        VehicleGround: "Vehicle_GroundVehicle"
    };
    ClientData.FilterPresets = {
        default: {
            city_preference: client_models_1.PreferenceEnum.Neurtral,
            hidden_preference: client_models_1.PreferenceEnum.Neurtral,
            quantum_preference: client_models_1.PreferenceEnum.Neurtral,
            hv_cargo_preference: client_models_1.PreferenceEnum.Neurtral,
            short_route_preference: client_models_1.PreferenceEnum.Neurtral,
            minimum_pad_size: null,
            looping: true,
            total_time_max: 0,
            buy_time_max: 0,
            transit_time_max: 0,
            ship_time_max: 0,
            sell_time_max: 0,
            item_keys: [],
            station_keys: [],
            sort_column: "rank",
            sort_direction: 1
        },
        pct_profit: {
            city_preference: client_models_1.PreferenceEnum.Neurtral,
            hidden_preference: client_models_1.PreferenceEnum.Neurtral,
            quantum_preference: client_models_1.PreferenceEnum.Neurtral,
            hv_cargo_preference: client_models_1.PreferenceEnum.Prefer,
            short_route_preference: client_models_1.PreferenceEnum.Neurtral,
            minimum_pad_size: null,
            looping: true,
            total_time_max: 0,
            buy_time_max: 0,
            transit_time_max: 0,
            ship_time_max: 0,
            sell_time_max: 0,
            item_keys: [],
            station_keys: [],
            sort_column: "avg_profit_pct",
            sort_direction: -1
        },
        uec_profit: {
            city_preference: client_models_1.PreferenceEnum.Neurtral,
            hidden_preference: client_models_1.PreferenceEnum.Neurtral,
            quantum_preference: client_models_1.PreferenceEnum.Neurtral,
            hv_cargo_preference: client_models_1.PreferenceEnum.Neurtral,
            short_route_preference: client_models_1.PreferenceEnum.Neurtral,
            minimum_pad_size: null,
            looping: true,
            total_time_max: 0,
            buy_time_max: 0,
            transit_time_max: 0,
            ship_time_max: 0,
            sell_time_max: 0,
            item_keys: [],
            station_keys: [],
            sort_column: "avg_profit",
            sort_direction: -1
        },
        speed: {
            city_preference: client_models_1.PreferenceEnum.Avoid,
            hidden_preference: client_models_1.PreferenceEnum.Avoid,
            quantum_preference: client_models_1.PreferenceEnum.Avoid,
            hv_cargo_preference: client_models_1.PreferenceEnum.Neurtral,
            short_route_preference: client_models_1.PreferenceEnum.Prefer,
            minimum_pad_size: null,
            looping: false,
            total_time_max: 30,
            buy_time_max: 10,
            transit_time_max: 0,
            ship_time_max: 0,
            sell_time_max: 10,
            item_keys: [],
            station_keys: [],
            sort_column: "total_time",
            sort_direction: 1
        },
        convenient: {
            city_preference: client_models_1.PreferenceEnum.Prefer,
            hidden_preference: client_models_1.PreferenceEnum.Avoid,
            quantum_preference: client_models_1.PreferenceEnum.Prefer,
            hv_cargo_preference: client_models_1.PreferenceEnum.Neurtral,
            short_route_preference: client_models_1.PreferenceEnum.Neurtral,
            minimum_pad_size: null,
            looping: false,
            total_time_max: 0,
            buy_time_max: 10,
            transit_time_max: 0,
            ship_time_max: 0,
            sell_time_max: 10,
            item_keys: [],
            station_keys: [],
            sort_column: "rank",
            sort_direction: 1
        },
        qt_distance: {
            city_preference: client_models_1.PreferenceEnum.Neurtral,
            hidden_preference: client_models_1.PreferenceEnum.Neurtral,
            quantum_preference: client_models_1.PreferenceEnum.Prefer,
            hv_cargo_preference: client_models_1.PreferenceEnum.Neurtral,
            short_route_preference: client_models_1.PreferenceEnum.Neurtral,
            minimum_pad_size: null,
            looping: true,
            total_time_max: 0,
            buy_time_max: 0,
            transit_time_max: 0,
            ship_time_max: 0,
            sell_time_max: 0,
            item_keys: [],
            station_keys: [],
            sort_column: "rank",
            sort_direction: 1
        }
    };
    ClientData.slotDisplayClasses = [
        { order: 0, locked: false, title: "Weapon", class: "weapon", types: ["TurretBase", "Turret", "MissileLauncher", "Missile", "WeaponGun"] },
        { order: 1, locked: false, title: "System", class: "system", types: ["PowerPlant", "Cooler", "Shield", "QuantumDrive"] },
        { order: 2, locked: false, title: "Paint", class: "other", types: ["Paints"] },
        { order: 3, locked: true, title: "Aux", class: "auxilary", types: ["Armor", "FuelTank", "QuantumFuelTank", "QuantumInterdictionGenerator"] },
        { order: 4, locked: true, title: "Other", class: "other", types: ["Cargo", "Container", "Radar"] },
        { order: 5, locked: true, title: "Propulsion", class: "propulsion", types: ["MainThruster", "ManneuverThruster"] },
    ];
    return ClientData;
}());
exports.ClientData = ClientData;
;
var RED = { r: 255, g: 0, b: 0 };
var GREEN = { r: 0, g: 255, b: 0 };
var BLUE = { r: 0, g: 0, b: 255 };
var YELLOW = { r: 255, g: 255, b: 0 };
var CYAN = { r: 0, g: 255, b: 255 };
var PURPLE = { r: 255, g: 0, b: 255 };
function mixColor(a, b, mix) {
    mix = Math.min(Math.max(0, mix), 1);
    var color = {
        r: (b.r * mix) + (a.r * (1.0 - mix)),
        g: (b.g * mix) + (a.g * (1.0 - mix)),
        b: (b.b * mix) + (a.b * (1.0 - mix)),
    };
    return "rgb(" + color.r + "," + color.g + "," + color.b + ")";
}
function RedToGreen(val) { return mixColor(RED, GREEN, val); }
;
function GreenToRed(val) { return mixColor(GREEN, RED, val); }
;
function GreenToYellow(val) { return mixColor(GREEN, YELLOW, val); }
;
//# sourceMappingURL=client_data.js.map