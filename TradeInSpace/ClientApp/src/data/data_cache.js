"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var angular_webstorage_service_1 = require("angular-webstorage-service");
var DataCacheService = /** @class */ (function () {
    function DataCacheService(storage) {
        var avail = angular_webstorage_service_1.isStorageAvailable(localStorage);
        console.log("Cache init. ", storage, avail);
        if (avail) {
            if (storage.get("test") === null) {
                storage.set("test", "ABCD");
            }
            console.log(storage.get("test"));
        }
    }
    return DataCacheService;
}());
exports.DataCacheService = DataCacheService;
//# sourceMappingURL=data_cache.js.map