import { GameShip } from './generated_models';
import { ClientData } from './client_data';
import { Loadout } from './loadout';
import { DamageInfo, CollapseDamage } from './client_models';

export class DisplayShip { 
  public Manufacturer: string;
  public Name: string;
  public Key: string;

  public Cargo: number;
  public TurnRate: number;
  public CruiseSpeed: number;
  public MaxSpeed: number;
  public HP: number;
  public Shields: number;

  public DPS: DamageInfo;
  public Burst: DamageInfo;
  public Missile: DamageInfo;
  public DPS_Flat: number;
  public Burst_Flat: number;
  public Missile_Flat: number;

  public QT: boolean;
  public QTSpeed: number;
  public QTAccel: number;

  public constructor(private ShipLoadout: Loadout, public Ship: GameShip) {
    this.Manufacturer = Ship.Manufacturer;
    this.Name = ShipLoadout.name;
    this.Key = ShipLoadout.key;
    this.Cargo = Ship.CargoCapacity;

    this.DPS = this.ShipLoadout.total_sustain;
    this.Burst = this.ShipLoadout.total_volley;
    this.Missile = this.ShipLoadout.total_missile;
    this.DPS_Flat = CollapseDamage(this.DPS);
    this.Burst_Flat = CollapseDamage(this.Burst);
    this.Missile_Flat = CollapseDamage(this.Missile);

    const flightControllers = this.ShipLoadout.find_slots_equipment_type(ClientData.EquipmentTypes.Flight);
    const shieldControllers = this.ShipLoadout.find_slots_equipment_type(ClientData.EquipmentTypes.Shield);
    const quantumDrives = this.ShipLoadout.find_slots_equipment_type(ClientData.EquipmentTypes.QuantumDrive);

    if (flightControllers.length > 0) {
      const fcEq = flightControllers[0].attached_equipment;
      this.CruiseSpeed = fcEq.Properties.IFCS.MaxSpeed;
      this.MaxSpeed = fcEq.Properties.IFCS.MaxBoostSpeed;
    }

    this.HP = 0;
    this.ShipLoadout.all_slots
      .forEach(s => {
        this.HP += s.health;
      });

    this.Shields = 0;
    shieldControllers.forEach(sc => {
      this.Shields += sc.attached_equipment.Properties.Shield.MaxShield;
    });

    this.QT = false;
    this.QTSpeed = 0;
    this.QTAccel = 0;
    quantumDrives.forEach(qd => {
      this.QT = true;
      this.QTSpeed = Math.max(this.QTSpeed, qd.attached_equipment.Properties.QDrive.DriveSpeed);
      this.QTAccel = Math.max(this.QTAccel, qd.attached_equipment.Properties.QDrive.S2AccelRate);
    })
  }
}
