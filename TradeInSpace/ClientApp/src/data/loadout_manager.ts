import { Injectable, Inject, InjectionToken, OnInit, ErrorHandler, ChangeDetectorRef } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { DataCacheService } from './data_cache';
import { ActivatedRoute } from '@angular/router';
import { GameEquipment, TradeShop, TradeEquipmentEntry } from './generated_models';
import { ShoppingCartEntry, BasicUserSettings, LoadoutStorage } from './models';
import { equal } from 'assert';
import { MessageService } from 'primeng/api';
import { fmtUEC } from '../pipes/scaleunit_pipe';
import { BehaviorSubject, Subscription, ReplaySubject, Observable } from 'rxjs';
import { GameDatabaseFull } from './game_data';
import { TISBaseComponent } from '../app/tis_base.component';
import { Loadout } from './loadout';
import { DisplayShip } from './display_ship';

@Injectable()
export class LoadoutManagerService extends TISBaseComponent implements OnInit {

  public activeLoadout: BehaviorSubject<Loadout>;
  public activeLoadoutDisplay: DisplayShip;

  customLoadouts: LoadoutStorage;
  customLoadoutCache: { [key: string]: Loadout } = {};
  defaultLoadoutCache: { [key: string]: Loadout } = {};

  constructor(http_route: ActivatedRoute,
    dataStore: DataCacheService,
    toast: MessageService) {
    super(http_route, dataStore, null, toast, "Loadout Manager");

    this.activeLoadout = new BehaviorSubject(null);
    this.ngOnInit();
  }

  onAllDataReady(): void {
    this.dataStore.loadoutData.subscribe(l => {
      this.customLoadouts = l || {
        loadouts: []
      };
      this.customLoadoutCache = {};
      this.customLoadouts.loadouts.forEach(ld => {
        const fullLoadout = Loadout.fromSaved(this.gameData, ld);
        this.customLoadoutCache[fullLoadout.key] = fullLoadout;
      });
    });
  }

  onAllDataUserUpdated(): void {
    if (this.userData.active_ship) {
      if (this.userData.active_ship === (this.activeLoadout.value && this.activeLoadout.value.key)) return;
      const customLoadout = this.getCustomLoadout(this.userData.active_ship);
      const defaultLoadout = customLoadout ? null : this.getDefaultLoadout(this.userData.active_ship);
      this.setActiveShipLoadout(customLoadout || defaultLoadout);
    }    
  }

  public setActiveShipLoadout(loadout: Loadout): void {
    this.activeLoadout.next(loadout);
    if (loadout)
      this.activeLoadoutDisplay = new DisplayShip(loadout, this.gameData.ShipKeyMap[loadout.equipment.Key]);
    else
      this.activeLoadoutDisplay = null;
  }

  //Cache all default loadouts for use anywhere in app instantly
  onGameDataUpdated(): void {
    //this.populateAllDefaultLoadouts();
    
  }

  public addCustomLoadout(newLoadout: Loadout, force = false): void {
    if (this.customLoadoutCache[newLoadout.key]) {
      if (force) {
        this.deleteCustomLoadout(newLoadout.key, false);
      } else {
        throw new Error("Loadout already defined with that name");
      }
    }

    this.customLoadoutCache[newLoadout.key] = newLoadout;
    this.customLoadouts.loadouts.push(newLoadout.toSave())
    //Save it back to user storage.
    this.dataStore.setLoadouts(this.customLoadouts, false);
  }

  private populateAllDefaultLoadouts(): void {
    this.gameData.Ships.forEach(s => {
      this.getDefaultLoadout(s.Key)
    });
  }

  public getDefaultLoadout(shipKey: string): Loadout | null {
    const ship = this.gameData.ShipKeyMap[shipKey];
    if (!ship) return null;

    if (!this.defaultLoadoutCache[shipKey]) {
      try {
        this.defaultLoadoutCache[shipKey] = Loadout.FromShip(this.gameData, ship);
      } catch (ex) {
        console.error(`Error loading ship: '${shipKey}'`, ex);
        return null;
      }
    }

    return this.defaultLoadoutCache[shipKey];
  }

  public getCustomLoadout(loadoutKey: string): Loadout {
    return this.customLoadoutCache[loadoutKey];
  }

  public getLoadoutWithName(loadoutName: string): Loadout {
    return this.getCustomLoadouts().find(l => l.name === loadoutName);
  }

  public getCustomLoadouts(): Loadout[] {
    return Object.values(this.customLoadoutCache);
  }

  public deleteCustomLoadout(loadoutKey: string, save = true): void {
    const srcLoadout = this.customLoadoutCache[loadoutKey];
    if (!srcLoadout) return;

    delete this.customLoadoutCache[loadoutKey];
    const index = this.customLoadouts.loadouts.findIndex(l => l.name === srcLoadout.name);
    this.customLoadouts.loadouts.splice(index, 1);
    if (save)
      this.dataStore.setLoadouts(this.customLoadouts, false);
  }
}
