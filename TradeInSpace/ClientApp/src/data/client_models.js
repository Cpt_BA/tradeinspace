"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CollapseResistance = exports.CollapseDamage = exports.MultDamage = exports.AddDamage = exports.ZeroDamage = exports.DamageMode = exports.ItineraryStepType = exports.PreferenceEnum = void 0;
var PreferenceEnum;
(function (PreferenceEnum) {
    PreferenceEnum[PreferenceEnum["Prefer"] = 0] = "Prefer";
    PreferenceEnum[PreferenceEnum["Neurtral"] = 1] = "Neurtral";
    PreferenceEnum[PreferenceEnum["Avoid"] = 2] = "Avoid";
    PreferenceEnum[PreferenceEnum["Ignore"] = 3] = "Ignore";
})(PreferenceEnum = exports.PreferenceEnum || (exports.PreferenceEnum = {}));
;
;
;
;
;
;
;
;
;
;
;
var ItineraryStepType;
(function (ItineraryStepType) {
    ItineraryStepType[ItineraryStepType["QuantumJump"] = 0] = "QuantumJump";
    ItineraryStepType[ItineraryStepType["ShipFlight"] = 1] = "ShipFlight";
    ItineraryStepType[ItineraryStepType["Running"] = 2] = "Running";
    ItineraryStepType[ItineraryStepType["Transit"] = 3] = "Transit";
    ItineraryStepType[ItineraryStepType["BuyGoods"] = 4] = "BuyGoods";
    ItineraryStepType[ItineraryStepType["SellGoods"] = 5] = "SellGoods";
})(ItineraryStepType = exports.ItineraryStepType || (exports.ItineraryStepType = {}));
;
var DamageMode;
(function (DamageMode) {
    DamageMode[DamageMode["Single"] = 0] = "Single";
    DamageMode[DamageMode["Burst"] = 1] = "Burst";
    DamageMode[DamageMode["Automatic"] = 2] = "Automatic";
    DamageMode[DamageMode["Scattergun"] = 3] = "Scattergun";
    DamageMode[DamageMode["Charge"] = 4] = "Charge";
    DamageMode[DamageMode["Missile"] = 5] = "Missile";
    DamageMode[DamageMode["Multiple"] = 6] = "Multiple";
})(DamageMode = exports.DamageMode || (exports.DamageMode = {}));
exports.ZeroDamage = { Physical: 0, Energy: 0, Distortion: 0, Thermal: 0, Biochemical: 0, Stun: 0 };
function AddDamage(A, B) {
    return {
        Physical: A.Physical + B.Physical,
        Energy: A.Energy + B.Energy,
        Distortion: A.Distortion + B.Distortion,
        Thermal: A.Thermal + B.Thermal,
        Biochemical: A.Biochemical + B.Biochemical,
        Stun: A.Stun + B.Stun
    };
}
exports.AddDamage = AddDamage;
function MultDamage(A, amount) {
    return {
        Physical: A.Physical * amount,
        Energy: A.Energy * amount,
        Distortion: A.Distortion * amount,
        Thermal: A.Thermal * amount,
        Biochemical: A.Biochemical * amount,
        Stun: A.Stun * amount
    };
}
exports.MultDamage = MultDamage;
function CollapseDamage(A) {
    return A ? (A.Physical + A.Energy + A.Distortion + A.Thermal + A.Biochemical + A.Stun) : 0;
}
exports.CollapseDamage = CollapseDamage;
function CollapseResistance(A) {
    return CollapseDamage(A) / 6;
}
exports.CollapseResistance = CollapseResistance;
;
//# sourceMappingURL=client_models.js.map