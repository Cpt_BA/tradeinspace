import { Injectable, Inject, InjectionToken, OnInit, ErrorHandler, ChangeDetectorRef } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { DataCacheService } from './data_cache';
import { ActivatedRoute } from '@angular/router';
import { GameEquipment, TradeShop, TradeEquipmentEntry } from './generated_models';
import { ShoppingCartEntry, BasicUserSettings } from './models';
import { equal } from 'assert';
import { MessageService } from 'primeng/api';
import { fmtUEC } from '../pipes/scaleunit_pipe';
import { BehaviorSubject, Subscription } from 'rxjs';
import { GameDatabaseFull } from './game_data';
import { TISBaseComponent } from '../app/tis_base.component';


@Injectable()
export class ShoppingCartManagerService extends TISBaseComponent implements OnInit {

  public ShoppingList: ShoppingCartItem[] = [];
  public ListTotal = 0;
  public ItemCount = 0;
  public StationCount = 0;

  public ListUpdated: BehaviorSubject<ShoppingCartItem[]> = new BehaviorSubject<ShoppingCartItem[]>(this.ShoppingList);


  constructor(http_route: ActivatedRoute,
    dataStore: DataCacheService,
    toast: MessageService) {
    super(http_route, dataStore, null, toast, "Shopping Cart");
    this.ngOnInit();
  }

  onAllDataReady() {
    this.loadSettings();
  }

  private loadSettings() {
    const foundTrades: { trade: TradeEquipmentEntry; quantity: number }[] = [];

    this.userData.shopping_cart.forEach(t => {
      const matchingEquipment = this.gameData.EquipmentKeyMap[t.equipment_key];
      const matchingTrade = matchingEquipment &&
                            matchingEquipment.TradeEntries &&
                            matchingEquipment.TradeEntries.find(te => te.Station.Key == t.location);

      if (matchingTrade) foundTrades.push({ trade: matchingTrade, quantity: t.quantity });
    });

    this.ShoppingList = foundTrades.map(t => new ShoppingCartItem(this, t.trade, t.quantity));
    this.notifyChanges(true);
  }

  private saveSettings(): void {
    this.userData.shopping_cart = this.ShoppingList.map(sl => {
      return {
        equipment_key: sl.Trade.Equipment.Key,
        location: sl.Trade.Station.Key,
        price: sl.UnitPrice,
        quantity: sl.Quantity
      };
    });
    this.dataStore.setUserDetails(this.userData, false);
  }

  public ClearCart(): void {
    this.ShoppingList = [];

    this.toast.add({ severity: 'success', summary: `Emptied shopping cart`});

    this.notifyChanges(true);
  }

  public AddCheapestTrade(Equipment: GameEquipment) {
    if (Equipment && Equipment.TradeEntries && Equipment.TradeEntries.length > 0)
      this.AddEquipmentTrade(Equipment.TradeEntries[0]);
  }

  public AddEquipmentTrade(EquipmentTrade: TradeEquipmentEntry) {
    const existingItem: ShoppingCartItem = this.ShoppingList.find(sce => sce.Trade.ID == EquipmentTrade.ID);

    if (existingItem) {
      existingItem.Quantity += 1;
    } else {
      this.ShoppingList.push(new ShoppingCartItem(this, EquipmentTrade));
    }

    this.toast.add({ severity: 'success', summary: `Added ${EquipmentTrade.Equipment.Name}`, detail: `Price ${fmtUEC(EquipmentTrade.BuyPrice, true, true)}` });

    this.notifyChanges(true);
  }

  public ChangeTrade(ExistingTrade: TradeEquipmentEntry, NewTrade: TradeEquipmentEntry) {
    const existingItem: ShoppingCartItem = this.ShoppingList.find(sce => sce.Trade.ID == ExistingTrade.ID);

    if (existingItem) {
      this.ChangeItem(existingItem, NewTrade);
    }
  }

  public ChangeItem(ExistingTrade: ShoppingCartItem, NewTrade: TradeEquipmentEntry) {
    const index = this.ShoppingList.indexOf(ExistingTrade);
    if (index === -1) return;

    ExistingTrade.Trade = NewTrade;
  }

  notifyChanges(itemsChanged = false, saveChanges = true): void {
    this.ItemCount = this.ShoppingList.reduce((p, c) => p + c.Quantity, 0);
    this.ListTotal = this.ShoppingList.reduce((p, c) => p + c.TotalPrice, 0);
    this.StationCount = new Set(this.ShoppingList.map(e => e.Trade.Station.Key)).size

    if (itemsChanged) this.ListUpdated.next(this.ShoppingList);
    if (saveChanges) this.saveSettings();
  }
}

export class ShoppingCartItem {

  private _quantity: number;
  private _trade: TradeEquipmentEntry;

  constructor(private Cart: ShoppingCartManagerService,
    Trade: TradeEquipmentEntry,
    Quantity: number = 1) {
    this._quantity = Quantity;
    this._trade = Trade;
  }

  public get TotalPrice(): number {
    return this.Trade.BuyPrice * this.Quantity;
  }

  public get UnitPrice(): number {
    return this.Trade.BuyPrice;
  }

  public set Quantity(value: number) {
    this._quantity = value;
    this.Cart.notifyChanges();
  }

  public get Quantity(): number {
    return this._quantity;
  }

  public set Trade(value: TradeEquipmentEntry) {
    this._trade = value;
    this.Cart.notifyChanges();
  }

  public get Trade(): TradeEquipmentEntry {
    return this._trade;
  }
}
