import { BaseLoop } from './client_models';
import { GameDatabaseFull } from './game_data';
import { SystemLocation, SystemBody, TradeShop, TradeTableEntry, ShopType, TradeItem, TradeType } from './generated_models';
import { ClientData } from './client_data';

export class CalculatedLoop {
  title: string;
  steps: CalculatedLoopStep[];
  resources: CalculatedLoopResource[];

  constructor(private gameData: GameDatabaseFull, private sourceLoop: BaseLoop) {
    this.getSteps();
    this.getResources();
  }

  public getSteps(): CalculatedLoopStep[] {
    if (!this.steps) {
      var bodies = this.sourceLoop.body_ids.map(bodyID => this.gameData.BodyMap[bodyID]);

      this.steps = bodies.map(body => {
        return new CalculatedLoopStep(this.gameData, this.sourceLoop.item_ids, this, body);
      });

      this.title = this.steps.map(s => s.destination.Name).join(", ");
    }

    return this.steps;
  }

  public getResources(): CalculatedLoopResource[] {
    if (!this.resources) {
      var items = this.sourceLoop.item_ids.map(itemID => this.gameData.ItemMap[itemID]);

      this.resources = items.map(item => {
        return new CalculatedLoopResource(this.gameData, this, item);
      });
    }

    return this.resources;
  }

}

export class CalculatedLoopStep {
  substeps: CalculatedLoopSubstep[];

  constructor(private gameData: GameDatabaseFull, private itemIDs: number[], public loop: CalculatedLoop, public destination: SystemBody) {
  }

  public getSubsteps(): CalculatedLoopSubstep[] {
    if (!this.substeps) {
      const visitShops = this.gameData.TradeShops
        .filter(ts => ts.Location.ParentBody == this.destination.SystemLocation)
        .filter(ts => ts.TradeEntries.filter(te => !te.BadTrade && this.itemIDs.includes(te.ItemID)).length > 0)
        .filter(ts => ClientData.TradeShopTypes.includes(ts.ShopType));

      this.substeps = visitShops.map(shop => {
        return new CalculatedLoopSubstep(this.gameData, this.itemIDs, this.loop, shop);
      });
    }

    return this.substeps;
  }
}

export class CalculatedLoopSubstep {
  trades: CalculatedSubstepEntry[];

  constructor(private gameData: GameDatabaseFull, private itemIDs: number[], public loop: CalculatedLoop, public destination: TradeShop) {  
  }

  public getEntries(): CalculatedSubstepEntry[] {
    if (!this.trades) {
      const entries = this.destination.TradeEntries.filter(te => !te.BadTrade && this.itemIDs.includes(te.ItemID));

      this.trades = entries.map(tradeEntry => {
        return new CalculatedSubstepEntry(this.gameData, this.loop, tradeEntry);
      });
    }

    return this.trades;
  }

  public getEntry(itemID: number): CalculatedSubstepEntry {
    return this.getEntries().find(e => e.trade.ItemID == itemID);
  }
}

export class CalculatedLoopResource {
  trades: CalculatedSubstepEntry[];

  min_buy: number = Number.MAX_VALUE;
  avg_buy: number = 0;
  max_buy: number = 0;
  min_sell: number = Number.MAX_VALUE;
  avg_sell: number = 0;
  max_sell: number = 0;

  unit_profit_avg: number = 0;
  unit_profit_max: number = 0;

  buy_rate: number = 0;
  sell_rate: number = 0;
  sale_ratio: number = 0;

  demand_ratio: number = 0;

  constructor(private gameData: GameDatabaseFull, public loop: CalculatedLoop, public item: TradeItem) {
    var foundTrades: CalculatedSubstepEntry[] = [];
    loop.steps.forEach(s => {
      s.getSubsteps().forEach(ss => {
        var entry = ss.getEntry(this.item.ID);
        if (entry) foundTrades.push(entry);
      });
    });
    this.trades = foundTrades;

    this.trades.forEach(t => {
      if (t.trade.Type == TradeType.Buy) {
        this.buy_rate += t.trade.UnitCapacityRate;
        this.min_buy = Math.min(this.min_buy, t.trade.MinUnitPrice);
        this.max_buy = Math.max(this.max_buy, t.trade.MaxUnitPrice);
      } else if (t.trade.Type == TradeType.Sell) {
        this.sell_rate += t.trade.UnitCapacityRate;
        this.min_sell = Math.min(this.min_sell, t.trade.MinUnitPrice);
        this.max_sell = Math.max(this.max_sell, t.trade.MaxUnitPrice);
      }
    });

    this.avg_buy = (this.min_buy + this.max_buy) / 2;
    this.avg_sell = (this.min_sell + this.max_sell) / 2;

    this.sale_ratio = this.sell_rate / (this.buy_rate + this.sell_rate);

    this.demand_ratio = this.sell_rate / this.buy_rate;

    this.unit_profit_avg = this.avg_sell - this.avg_buy;
    this.unit_profit_max = this.max_sell - this.min_buy;
  }
}

export class CalculatedSubstepEntry {

  constructor(private gameData: GameDatabaseFull, public loop: CalculatedLoop, public trade: TradeTableEntry) {
    
  }
}
