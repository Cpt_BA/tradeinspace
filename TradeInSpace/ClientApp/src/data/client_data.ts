import { PreferenceEnum, FilterParams, DamageMode, SlotDisplay, DisplayRoute } from './client_models';
import { ShopType, PadSize, ServiceFlags, LocationType } from './generated_models';
import { SelectItem } from 'primeng/api';

export class ClientData {

  public static RouteRankProperties: RankedProperty<DisplayRoute>[] = [
    {
      name: "max_unit_cost",
      fnValue: route => route.max_buy,
      fnMixColor: RedToGreen
    },
    {
      name: "avg_profit_pct",
      fnValue: route => route.avg_profit_pct,
      fnMixColor: RedToGreen
    },
    {
      name: "avg_profit",
      fnValue: route => route.avg_profit,
      fnMixColor: RedToGreen
    },
    {
      name: "buy_rate",
      fnValue: route => route.buy_rate,
      fnMixColor: RedToGreen
    },
    {
      name: "sell_rate",
      fnValue: route => route.sell_rate,
      fnMixColor: RedToGreen
    },
    {
      name: "total_time",
      fnValue: route => route.itinerary.totalDurationS,
      fnMixColor: GreenToRed
    },
    {
      name: "total_buy",
      fnValue: route => route.itinerary.totalBuyS,
      fnMixColor: GreenToRed
    },
    {
      name: "total_transit",
      fnValue: route => route.itinerary.totalTransitS,
      fnMixColor: GreenToRed
    },
    {
      name: "total_ship",
      fnValue: route => route.itinerary.totalShipS,
      fnMixColor: GreenToRed
    },
    {
      name: "total_sell",
      fnValue: route => route.itinerary.totalSellS,
      fnMixColor: GreenToRed
    },
    {
      name: "total_jumps",
      fnValue: route => route.itinerary.totalJumps,
      fnMixColor: GreenToRed
    },
    {
      name: "avg_hold_cost",
      fnValue: route => (route.cargo_units * route.avg_buy),
      fnMixColor: RedToGreen
    },
    {
      name: "avg_hold_profit",
      fnValue: route => (route.cargo_units * route.avg_profit),
      fnMixColor: RedToGreen
    },
    {
      name: "max_profit_rate",
      fnValue: route => (route.cargo_units * route.max_profit) / (route.itinerary.totalDurationS / 60),
      fnMixColor: RedToGreen
    }
  ];

  public static ModeOptions: SelectItem[] = [
    { label: "One-Way", value: false },
    { label: "Loop", value: true }
  ];

  public static SizeFilterOptions: SelectItem[] = [
    { label: "None", value: null },
    { label: "Any/Tiny", value: PadSize.Tiny },
    { label: "X-Small", value: PadSize.XSmall },
    { label: "Small", value: PadSize.Small },
    { label: "Medium", value: PadSize.Medium },
    { label: "Large", value: PadSize.Large },
    { label: "X-Large", value: PadSize.XLarge },
  ];
  public static SizeFilterOptionsSmall: SelectItem[] = [
    { label: "N/A", value: null },
    { label: "All", value: PadSize.Tiny },
    { label: "XS", value: PadSize.XSmall },
    { label: "S", value: PadSize.Small },
    { label: "M", value: PadSize.Medium },
    { label: "L", value: PadSize.Large },
    { label: "XL", value: PadSize.XLarge },
  ];
  public static SizeSelectOptions: SelectItem[] = [
    { label: "Tiny", value: PadSize.Tiny },
    { label: "X-Small", value: PadSize.XSmall },
    { label: "Small", value: PadSize.Small },
    { label: "Medium", value: PadSize.Medium },
    { label: "Large", value: PadSize.Large },
    { label: "X-Large", value: PadSize.XLarge },
  ];

  public static EquipmentSizes: SelectItem[] = [
    { label: "0", value: 0 },
    { label: "1", value: 1 },
    { label: "2", value: 2 },
    { label: "3", value: 3 },
    { label: "4", value: 4 },
    { label: "5", value: 5 },
    { label: "6", value: 6 },
    { label: "7", value: 7 },
    { label: "8", value: 8 },
    { label: "9", value: 9 },
    { label: "10", value: 10 }
  ];

  public static EquipmentGrades: SelectItem[] = [
    { label: "1", value: 1 },
    { label: "2", value: 2 },
    { label: "3", value: 3 },
    { label: "4", value: 4 },
  ];

  public static GunDamageModes: SelectItem[] = [
    { label: "Single Fire", value: DamageMode.Single },
    { label: "Charge", value: DamageMode.Charge },
    { label: "Automatic", value: DamageMode.Automatic },
    { label: "Scattergun", value: DamageMode.Scattergun }
  ];

  public static FoodTypes: SelectItem[] = [
    { label: "Food", value: "Food" },
    { label: "Drink", value: "Drink" },
  ];

  public static ClothingTypes: SelectItem[] = [
    { label: "Hat", value: "Char_Clothing_Hat" },
    { label: "Torso 1", value: "Char_Clothing_Torso_0" },
    { label: "Torso 2", value: "Char_Clothing_Torso_1" },
    { label: "Torso 3", value: "Char_Clothing_Torso_2" },
    { label: "Legs", value: "Char_Clothing_Legs" },
    { label: "Hands", value: "Char_Clothing_Hands" },
    { label: "Feet", value: "Char_Clothing_Feet" },
  ];

  public static ArmorTypes: SelectItem[] = [
    { label: "Helmet", value: "Char_Armor_Helmet" },
    { label: "Torso", value: "Char_Armor_Torso" },
    { label: "Arms", value: "Char_Armor_Arms" },
    { label: "Legs", value: "Char_Armor_Legs" },
  ];

  public static DamageTypes: SelectItem[] = [
    { label: "Physical", value: "Physical" },
    { label: "Energy", value: "Energy" },
    { label: "Distortion", value: "Distortion" },
    { label: "Thermal", value: "Thermal" },
    { label: "Biochemical", value: "Biochemical" },
    { label: "Stun", value: "Stun" },
  ];

  public static MissionTypes: SelectItem[] = [
    { label: "Appointment", value: "Appointment", icon: "nf-mdi-calendar_clock" },
    { label: "Bounty Hunter", value: "Bounty Hunter", icon: "nf-fa-user_times" },
    { label: "Delivery", value: "Delivery", icon: "nf-oct-package" },
    { label: "ECN Alert", value: "ECN Alert", icon: "nf-oct-alert" },
    //{ label: "Investigate", value: "Investigate", icon: "nf-oct-question" }, //Disabled for now, only one is a hidden mission
    { label: "Investigation", value: "Investigation", icon: "nf-oct-question" },
    { label: "Maintenance", value: "Maintenance", icon: "nf-fa-wrench" },
    { label: "Mercenary", value: "Mercenary", icon: "nf-mdi-target" },
    { label: "Priority", value: "Priority", icon: "nf-mdi-comment_alert" },
    { label: "Racing", value: "Racing", icon: "nf-mdi-car_sports" },
    { label: "Research", value: "Research", icon: "nf-oct-beaker" },
    { label: "Search", value: "Search", icon: "nf-fa-binoculars" },
    { label: "Service Beacons", value: "Service Beacons", icon: "nf-mdi-account_alert" }
  ];

  public static ServiceOptions: SelectItem[] = [
    { label: ClientData.GetServiceFlaNames(ServiceFlags.Fine), value: ServiceFlags.Fine },
    { label: ClientData.GetServiceFlaNames(ServiceFlags.FPSArmor), value: ServiceFlags.FPSArmor },
    { label: ClientData.GetServiceFlaNames(ServiceFlags.FPSClothing), value: ServiceFlags.FPSClothing },
    { label: ClientData.GetServiceFlaNames(ServiceFlags.FPSCrypto), value: ServiceFlags.FPSCrypto },
    { label: ClientData.GetServiceFlaNames(ServiceFlags.FPSEquipment), value: ServiceFlags.FPSEquipment },
    { label: ClientData.GetServiceFlaNames(ServiceFlags.FPSFood), value: ServiceFlags.FPSFood },
    { label: ClientData.GetServiceFlaNames(ServiceFlags.FPSRespawnPoint), value: ServiceFlags.FPSRespawnPoint },
    { label: ClientData.GetServiceFlaNames(ServiceFlags.FPSWeapon), value: ServiceFlags.FPSWeapon },
    { label: ClientData.GetServiceFlaNames(ServiceFlags.GreenZone), value: ServiceFlags.GreenZone },
    { label: ClientData.GetServiceFlaNames(ServiceFlags.MissionGiver), value: ServiceFlags.MissionGiver },
    { label: ClientData.GetServiceFlaNames(ServiceFlags.PackageTerminal), value: ServiceFlags.PackageTerminal },
    { label: ClientData.GetServiceFlaNames(ServiceFlags.Refinery), value: ServiceFlags.Refinery },
    { label: ClientData.GetServiceFlaNames(ServiceFlags.ShipClaim), value: ServiceFlags.ShipClaim },
    { label: ClientData.GetServiceFlaNames(ServiceFlags.ShipEquipment), value: ServiceFlags.ShipEquipment },
    { label: ClientData.GetServiceFlaNames(ServiceFlags.ShipLanding), value: ServiceFlags.ShipLanding },
    { label: ClientData.GetServiceFlaNames(ServiceFlags.ShipPurchase), value: ServiceFlags.ShipPurchase },
    { label: ClientData.GetServiceFlaNames(ServiceFlags.ShipRental), value: ServiceFlags.ShipRental },
    { label: ClientData.GetServiceFlaNames(ServiceFlags.Trade), value: ServiceFlags.Trade }
  ];

  public static TradeShopTypes: ShopType[] = [
    ShopType.AdminTerminal,
    ShopType.FenceTerminal,
    ShopType.CommExTransfers,
    ShopType.TDD,
    ShopType.CargoOffice
  ];

  public static SystemBodyLocationTypes: LocationType[] = [
    LocationType.Planet,
    LocationType.Moon,
    LocationType.LagrangePoint,
    LocationType.AsteroidField
  ];

  public static GetServiceFlaNames(flags: ServiceFlags): string {
    switch (flags) {
      case ServiceFlags.None:
        return "None";
      //Admin
      case ServiceFlags.Trade:
        return "Trade Terminal";
      case ServiceFlags.Refinery:
        return "Refinery Terminal";
      case ServiceFlags.Fine:
        return "Fine Payment Terminal";
      case ServiceFlags.PackageTerminal:
        return "Package Terminal";
      case ServiceFlags.MissionGiver:
        return "Mission Giver";
      case ServiceFlags.GreenZone:
        return "Green/Armistice Zone";
      //Ship
      case ServiceFlags.ShipLanding:
        return "Ship Landing";
      case ServiceFlags.ShipClaim:
        return "Ship Claiming & Customization";
      case ServiceFlags.ShipPurchase:
        return "Ship Purchasing";
      case ServiceFlags.ShipEquipment:
        return "Ship Equipment";
      case ServiceFlags.ShipRental:
        return "Ship Rental";

      case ServiceFlags.FPSArmor:
        return "FPS Armor";
      case ServiceFlags.FPSWeapon:
        return "FPS Weapons";
      case ServiceFlags.FPSEquipment:
        return "FPS Equipment/Utilities";
      case ServiceFlags.FPSClothing:
        return "FPS Clothing";
      case ServiceFlags.FPSRespawnPoint:
        return "FPS Respawn Point";
      case ServiceFlags.FPSCrypto:
        return "FPS Crypto Modules";

      case ServiceFlags.FPSFood:
        return "Food";
      case ServiceFlags.Hacking:
        return "Security Hacking";
    }

    return "Multiple";
  }

  public static EquipmentTypes = {
    QuantumDrive: "QuantumDrive",
    QTank: "QuantumFuelTank",
    HTank: "FuelTank",
    Shield: "Shield",
    Flight: "FlightController",
    Power: "PowerPlant",
    Cooler: "Cooler",
    Armor: "Armor",
    MiningLaser: "WeaponMining",
    WeaponShip: "WeaponGun",
    Vehicle: "NOITEM_Vehicle",
    Food: "Food",
    Drink: "Drink",
    Gadget: "Gadget",
    Missile: "Missile",
    Paint: "Paints",
    MiningModifier: "MiningModifier",
    SalvageModifier: "SalvageModifier",

    FPS_Undersuit: "Char_Armor_Undersuit",
    FPS_Helmet: "Char_Armor_Helmet",
    FPS_Torso: "Char_Armor_Torso",
    FPS_Arms: "Char_Armor_Arms",
    FPS_Legs: "Char_Armor_Legs",
    FPS_Weapon: "WeaponPersonal",
    FPS_Hacking: "RemovableChip",
    FPS_Consumable: "FPS_Consumable",

    Clothing_Hat: "Char_Clothing_Hat",
    Clothing_Torso0: "Char_Clothing_Torso_0",
    Clothing_Torso1: "Char_Clothing_Torso_1",
    Clothing_Torso2: "Char_Clothing_Torso_2",
    Clothing_Arms: "Char_Clothing_Arms",
    Clothing_Hands: "Char_Clothing_Hands",
    Clothing_Legs: "Char_Clothing_Legs",
    Clothing_Feet: "Char_Clothing_Feet"
  };

  public static EquipmentSubtypes = {
    VehicleSpaceship: "Vehicle_Spaceship",
    VehicleGround: "Vehicle_GroundVehicle"
  }

  public static FilterPresets: { [preset_name: string]: FilterParams } = {
    default: {
      city_preference: PreferenceEnum.Neurtral,
      hidden_preference: PreferenceEnum.Neurtral,
      quantum_preference: PreferenceEnum.Neurtral,
      hv_cargo_preference: PreferenceEnum.Neurtral,
      short_route_preference: PreferenceEnum.Neurtral,

      minimum_pad_size: null,
      looping: true,

      total_time_max: 0,
      buy_time_max: 0,
      transit_time_max: 0,
      ship_time_max: 0,
      sell_time_max: 0,

      item_keys: [],
      station_keys: [],

      sort_column: "rank",
      sort_direction: 1
    },
    pct_profit: {
      city_preference: PreferenceEnum.Neurtral,
      hidden_preference: PreferenceEnum.Neurtral,
      quantum_preference: PreferenceEnum.Neurtral,
      hv_cargo_preference: PreferenceEnum.Prefer,
      short_route_preference: PreferenceEnum.Neurtral,

      minimum_pad_size: null,
      looping: true,

      total_time_max: 0,
      buy_time_max: 0,
      transit_time_max: 0,
      ship_time_max: 0,
      sell_time_max: 0,

      item_keys: [],
      station_keys: [],

      sort_column: "avg_profit_pct",
      sort_direction: -1
    },
    uec_profit: {
      city_preference: PreferenceEnum.Neurtral,
      hidden_preference: PreferenceEnum.Neurtral,
      quantum_preference: PreferenceEnum.Neurtral,
      hv_cargo_preference: PreferenceEnum.Neurtral,
      short_route_preference: PreferenceEnum.Neurtral,

      minimum_pad_size: null,
      looping: true,

      total_time_max: 0,
      buy_time_max: 0,
      transit_time_max: 0,
      ship_time_max: 0,
      sell_time_max: 0,

      item_keys: [],
      station_keys: [],

      sort_column: "avg_profit",
      sort_direction: -1
    },
    speed: {
      city_preference: PreferenceEnum.Avoid,
      hidden_preference: PreferenceEnum.Avoid,
      quantum_preference: PreferenceEnum.Avoid,
      hv_cargo_preference: PreferenceEnum.Neurtral,
      short_route_preference: PreferenceEnum.Prefer,

      minimum_pad_size: null,
      looping: false,

      total_time_max: 30,
      buy_time_max: 10,
      transit_time_max: 0,
      ship_time_max: 0,
      sell_time_max: 10,

      item_keys: [],
      station_keys: [],

      sort_column: "total_time",
      sort_direction: 1
    },
    convenient: {
      city_preference: PreferenceEnum.Prefer,
      hidden_preference: PreferenceEnum.Avoid,
      quantum_preference: PreferenceEnum.Prefer,
      hv_cargo_preference: PreferenceEnum.Neurtral,
      short_route_preference: PreferenceEnum.Neurtral,

      minimum_pad_size: null,
      looping: false,

      total_time_max: 0,
      buy_time_max: 10,
      transit_time_max: 0,
      ship_time_max: 0,
      sell_time_max: 10,

      item_keys: [],
      station_keys: [],

      sort_column: "rank",
      sort_direction: 1
    },
    qt_distance: {
      city_preference: PreferenceEnum.Neurtral,
      hidden_preference: PreferenceEnum.Neurtral,
      quantum_preference: PreferenceEnum.Prefer,
      hv_cargo_preference: PreferenceEnum.Neurtral,
      short_route_preference: PreferenceEnum.Neurtral,

      minimum_pad_size: null,
      looping: true,

      total_time_max: 0,
      buy_time_max: 0,
      transit_time_max: 0,
      ship_time_max: 0,
      sell_time_max: 0,

      item_keys: [],
      station_keys: [],

      sort_column: "rank",
      sort_direction: 1
    }
  };

  public static slotDisplayClasses: Array<SlotDisplay> = [
    { order: 0, locked: false, title: "Weapon", class: "weapon", types: ["TurretBase", "Turret", "MissileLauncher", "Missile", "WeaponGun"] },
    { order: 1, locked: false, title: "System", class: "system", types: ["PowerPlant", "Cooler", "Shield", "QuantumDrive"] },
    { order: 2, locked: false, title: "Paint", class: "other", types: ["Paints"] },
    { order: 3, locked: true, title: "Aux", class: "auxilary", types: ["Armor", "FuelTank", "QuantumFuelTank", "QuantumInterdictionGenerator"] },
    { order: 4, locked: true, title: "Other", class: "other", types: ["Cargo", "Container", "Radar"] },
    { order: 5, locked: true, title: "Propulsion", class: "propulsion", types: ["MainThruster", "ManneuverThruster"] },
  ];
}

interface Color { r: number; g: number; b: number };
const RED: Color = { r: 255, g: 0, b: 0 };
const GREEN: Color = { r: 0, g: 255, b: 0 };
const BLUE: Color = { r: 0, g: 0, b: 255 };
const YELLOW: Color = { r: 255, g: 255, b: 0 };
const CYAN: Color = { r: 0, g: 255, b: 255 };
const PURPLE: Color = { r: 255, g: 0, b: 255 };

function mixColor(a: Color, b: Color, mix: number): string {
  mix = Math.min(Math.max(0, mix), 1);

  const color: Color = {
    r: (b.r * mix) + (a.r * (1.0 - mix)),
    g: (b.g * mix) + (a.g * (1.0 - mix)),
    b: (b.b * mix) + (a.b * (1.0 - mix)),
  };
  return `rgb(${color.r},${color.g},${color.b})`;
}
function RedToGreen(val: number): string { return mixColor(RED, GREEN, val); };
function GreenToRed(val: number): string { return mixColor(GREEN, RED, val); };
function GreenToYellow(val: number): string { return mixColor(GREEN, YELLOW, val); };

export interface PropertyMinMaxes {
  [propertyName: string]: MinMax;
}

export interface MinMax {
  min: number;
  max: number;
}

export interface RankedProperty<T> {
  name: string;
  fnValue: (val: T) => number;
  fnMixColor: (val: number) => string;
}
