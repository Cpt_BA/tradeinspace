"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var models_1 = require("./models");
function GameDatabaseLoaded(gameDB) {
    var fullDB = gameDB;
    console.log("== GameDB Loaded ==");
    fullDB.BodyMap = models_1.MapId(gameDB.SystemBodies);
    fullDB.BodyKeyMap = models_1.MapKey(gameDB.SystemBodies);
    fullDB.LocationMap = models_1.MapId(gameDB.SystemLocations);
    fullDB.LocationKeyMap = models_1.MapKey(gameDB.SystemLocations);
    fullDB.LandingZoneMap = models_1.MapId(gameDB.LandingZones);
    fullDB.LandingZoneKeyMap = models_1.MapKey(gameDB.LandingZones);
    fullDB.ShipMap = models_1.MapId(gameDB.Ships);
    fullDB.ShipKeyMap = models_1.MapKey(gameDB.Ships);
    fullDB.EquipmentMap = models_1.MapId(gameDB.Equipment);
    fullDB.EquipmentKeyMap = models_1.MapKey(gameDB.Equipment);
    fullDB.ItemMap = models_1.MapId(gameDB.TradeItems);
    fullDB.ItemKeyMap = models_1.MapKey(gameDB.TradeItems);
    fullDB.ShopMap = models_1.MapId(gameDB.TradeShops);
    fullDB.ShopKeyMap = models_1.MapKey(gameDB.TradeShops);
    gameDB.Ships.forEach(function (s) {
    });
    gameDB.Equipment.forEach(function (eq) {
        //Web service sends as JSON strings, just unpack them here
        eq.Properties = eq ? JSON.parse(eq.Properties) : {};
        eq.PortLayout = eq ? JSON.parse(eq.PortLayout) : {};
        eq.DefaultLoadout = eq ? JSON.parse(eq.DefaultLoadout) : {};
    });
    gameDB.TradeItems.forEach(function (ti) {
    });
    gameDB.TradeShops.forEach(function (s) {
        s.Location = fullDB.LocationMap[s.LocationID];
    });
    gameDB.LandingZones.forEach(function (lz) {
        lz.Location = fullDB.LocationMap[lz.LocationID];
    });
    gameDB.SystemBodies.forEach(function (b) {
        b.ParentBody = fullDB.BodyMap[b.ParentBodyID];
        b.SystemLocation = fullDB.LocationMap[b.SystemLocationID];
        b.ChildBodies = gameDB.SystemBodies.filter(function (sb) { return sb.ParentBodyID === b.ID; });
    });
    gameDB.SystemLocations.forEach(function (l) {
        l.ParentLocation = fullDB.LocationMap[l.ParentLocationID];
        l.ParentBody = fullDB.LocationMap[l.ParentBodyID];
        l.ChildLocations = gameDB.SystemLocations.filter(function (sl) { return sl.ParentLocationID === l.ID; });
        l.LandingZones = gameDB.LandingZones.filter(function (lz) { return lz.LocationID === l.ID; });
        l.Shops = gameDB.TradeShops.filter(function (s) { return s.LocationID === l.ID; });
    });
    return fullDB;
}
exports.GameDatabaseLoaded = GameDatabaseLoaded;
function TradeTableLoaded(gameDB, tradeTable) {
    console.log("== TradeTable Loaded ==");
    tradeTable.Entries.forEach(function (e) {
        e.Station = gameDB.ShopMap[e.StationID];
        e.Item = gameDB.ItemMap[e.ItemID];
        e.Table = tradeTable;
    });
    tradeTable.EquipmentEntries.forEach(function (ee) {
        ee.Station = gameDB.ShopMap[ee.StationID];
        ee.Equipment = gameDB.EquipmentMap[ee.EquipmentID];
        ee.Table = tradeTable;
    });
    gameDB.TradeShops.forEach(function (ts) {
        ts.TradeEntries = tradeTable.Entries.filter(function (e) { return e.StationID === ts.ID; });
        ts.EquipmentEntries = tradeTable.EquipmentEntries.filter(function (ee) { return ee.StationID === ts.ID; });
    });
    gameDB.Equipment.forEach(function (e) {
        e.TradeEntries = tradeTable.EquipmentEntries.filter(function (ee) { return ee.EquipmentID === e.ID && ee.BuyPrice > 0; }).sort(function (a, b) { return a.BuyPrice - b.BuyPrice; });
        if (e.TradeEntries.length > 0) {
            e.CheapestPrice = e.TradeEntries[0].BuyPrice;
        }
        else {
            e.CheapestPrice = null;
        }
    });
}
exports.TradeTableLoaded = TradeTableLoaded;
//# sourceMappingURL=game_data.js.map