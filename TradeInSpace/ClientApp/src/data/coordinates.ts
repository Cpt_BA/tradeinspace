import { root } from "rxjs/internal-compatibility";
import { LocationType, SystemBody } from "./generated_models";

export class Coordinate {
  public static ServerJulianTime: Date = new Date(Date.UTC(2020, 0, 1, 0, 0, 0));

  name: string;
  type: CoordinateType;

  globalPosition: Vector3;
  localPosition: Vector3;

  parentBody?: ObjectContainer;

  children: Coordinate[];

  timestamp: Date;

  private constructor() { }

  public static fromJulianCoordinates(name: string, type: CoordinateType, globalPosition: Vector3, parent?: ObjectContainer): Coordinate {
    const coord = new Coordinate();
    coord.globalPosition = globalPosition;
    coord.timestamp = this.ServerJulianTime;
    coord.setRelativeTo(parent);
    coord.name = name;
    coord.type = type;
    return coord;
  }

  public static fromGlobalAtTime(name: string, type: CoordinateType, globalPosition: Vector3, time?: Date, parent?: ObjectContainer): Coordinate {
    const coord = new Coordinate();
    coord.globalPosition = globalPosition;
    coord.timestamp = time || this.ServerJulianTime;
    coord.setRelativeTo(parent);
    coord.name = name;
    coord.type = type;
    return coord;
  }

  public static fromGlobalAtTimeWithLocate(name: string, type: CoordinateType, globalPosition: Vector3, time: Date, systemRoot: ObjectContainer): Coordinate {
    const coord = new Coordinate();
    coord.globalPosition = globalPosition;
    coord.timestamp = time;
    coord.name = name;
    coord.type = type;
    coord.findCoordinateOwner(systemRoot)
    return coord;
  }

  public isOnBody(): boolean {
    return this.parentBody && this.parentBody.bodyRadius != null;
  }

  private findCoordinateOwner(rootContainer: ObjectContainer) {
    const fnSearchContainer = function (target: Vector3, container: ObjectContainer): ObjectContainer {
      // TODO: Adjustable multiplier
      // TODO: Body radius won't work for stations, need OM distance
      const distanceTo = target.DistanceTo(container.position.globalPosition)
      const distanceFilter = container.omRadius == 0 ? 30000 : container.omRadius * 2;
      //console.log(`${this.name} => ${container.name} = ${distanceTo}`);
      if (distanceTo < distanceFilter) {
        return container
      }

      for (var idx in container.child_containers)
      {
        const result = fnSearchContainer(target, container.child_containers[idx])
        if (result)
          return result;
      }

      return null;
    }

    this.setRelativeTo(null);

    if (rootContainer) {
      const parentContainer = fnSearchContainer(this.globalPosition, rootContainer);
      this.setRelativeTo(parentContainer);
    }
  }

  setRelativeTo(parentContainer: ObjectContainer): void {
    this.parentBody = parentContainer;
    if (parentContainer) {
      this.localPosition = this.globalPosition.Subtract(this.parentBody.position.globalPosition);
    } else {
      this.localPosition = this.globalPosition;
    }
  }

  rotateToTime(newTime: Date): Coordinate {
    var dateDiff = newTime.getTime() - this.timestamp.getTime();
    var dateDiffDays = Math.abs(dateDiff / (24 * 60 * 60 * 1000));

    if (dateDiffDays == 0)
      return this;

    if (!this.isOnBody())
      return Coordinate.fromGlobalAtTime(this.name, this.type, this.globalPosition, newTime, this.parentBody);

    var LengthOfDayDecimal = this.parentBody.rotationSpeed.X * 3600.0 / 86400.0;
    
    var JulianDate = dateDiffDays;
    var TotalCycles = JulianDate / LengthOfDayDecimal;
    var CurrentCycleDez = TotalCycles % 1;
    var CurrentCycleDeg = CurrentCycleDez * 360;
    var CurrentCycleAngle = this.parentBody.rotationOffset.X + CurrentCycleDeg;
    if (dateDiff < 0)
      CurrentCycleAngle = 360 - CurrentCycleAngle;
    var CurrentCycleRadians = CurrentCycleAngle / 180 * Math.PI;

    var NewPosition = new Vector3(
      (this.localPosition.X * Math.cos(CurrentCycleRadians) - this.localPosition.Y * Math.sin(CurrentCycleRadians)),
      (this.localPosition.X * Math.sin(CurrentCycleRadians) + this.localPosition.Y * Math.cos(CurrentCycleRadians)),
      this.localPosition.Z);

    return Coordinate.fromGlobalAtTime(this.name, this.type, this.parentBody.position.localPosition.Add(NewPosition), newTime, this.parentBody);
  }
}

export class ObjectContainer {
  position: Coordinate;

  name: string;

  type: ContainerType;

  rotationSpeed: Vector3;
  rotationOffset: Vector3;

  omRadius: number;
  bodyRadius: number;

  dbBody: SystemBody;

  parent?: ObjectContainer;
  child_containers: ObjectContainer[];
  children: Coordinate[];

  orbitalMarkers: Coordinate[];

  private constructor() {
    this.children = [];
    this.child_containers = [];
    this.orbitalMarkers = [];
  }

  public static constructFromSystem(systemBody: SystemBody): ObjectContainer {
    const fnCreateContainer = function (body: SystemBody, parent?: ObjectContainer): ObjectContainer {

      const containerPos = body.AdditionalProperties.GlobalPosition ?
        new Vector3(
          body.AdditionalProperties.GlobalPosition.X || 0,
          body.AdditionalProperties.GlobalPosition.Y || 0,
          body.AdditionalProperties.GlobalPosition.Z || 0) :
        Vector3.Zero;
      const containerRot = body.AdditionalProperties.Rotation ?
        new Vector3(
          body.AdditionalProperties.Rotation.X || 0,
          body.AdditionalProperties.Rotation.Y || 0,
          body.AdditionalProperties.Rotation.Z || 0) :
        Vector3.Zero;

      var container: ObjectContainer = new ObjectContainer();

      container.dbBody = body;
      container.parent = parent;
      container.name = body.Name;
      container.bodyRadius = body.BodyRadius;
      container.omRadius = body.AdditionalProperties.OMRadius || 0;
      container.position = Coordinate.fromJulianCoordinates(container.name, CoordinateType.SpacePOI, containerPos, parent);
      container.rotationOffset = containerRot;
      container.rotationSpeed = new Vector3(body.AdditionalProperties.RotationSpeed, 0, 0);
      
      container.generateOrbitalMarkers();

      for (var idx in body.SystemLocation.ChildLocations) {
        var child = body.SystemLocation.ChildLocations[idx];
        if (child.LocationType == LocationType.Planet ||
          child.LocationType == LocationType.Moon)
          continue;
        if (child.AdditionalProperties) {
          var childPos = child.AdditionalProperties.GlobalPosition ?
            new Vector3(
              child.AdditionalProperties.GlobalPosition.X || 0,
              child.AdditionalProperties.GlobalPosition.Y || 0,
              child.AdditionalProperties.GlobalPosition.Z || 0) :
            Vector3.Zero;

          container.children.push(Coordinate.fromJulianCoordinates(child.Name, CoordinateType.PlanetPOI, childPos, container));
        }
      }
      
      for (var idx in body.ChildBodies) {
        const childBody = fnCreateContainer(body.ChildBodies[idx], container);
        container.child_containers.push(childBody);
      }

      return container;
    }

    return fnCreateContainer(systemBody);
  }

  findContainer(searchName: string, recusive: boolean): ObjectContainer | null {
    const fnSearch = function (container: ObjectContainer): ObjectContainer | null {
      if (container.name == searchName)
        return container;

      for (var idx in container.child_containers) {
        const match = fnSearch(container.child_containers[idx])
        if (match)
          return match;
      }
    }

    return fnSearch(this);
  }

  generateOrbitalMarkers(): void {
    if (this.omRadius) {
      this.orbitalMarkers.push(
        Coordinate.fromJulianCoordinates("OM1", CoordinateType.OrbitalMarker, Vector3.UnitZ.MultScalar(this.omRadius), this),
        Coordinate.fromJulianCoordinates("OM2", CoordinateType.OrbitalMarker, Vector3.UnitZ.MultScalar(-this.omRadius), this),
        Coordinate.fromJulianCoordinates("OM3", CoordinateType.OrbitalMarker, Vector3.UnitY.MultScalar(this.omRadius), this),
        Coordinate.fromJulianCoordinates("OM4", CoordinateType.OrbitalMarker, Vector3.UnitY.MultScalar(-this.omRadius), this),
        Coordinate.fromJulianCoordinates("OM5", CoordinateType.OrbitalMarker, Vector3.UnitX.MultScalar(this.omRadius), this),
        Coordinate.fromJulianCoordinates("OM6", CoordinateType.OrbitalMarker, Vector3.UnitX.MultScalar(-this.omRadius), this)
      )
    }
  }
}

export enum ContainerType {
  Planet,
  Space,
  Unknown
}

export enum CoordinateType {
  PlanetPOI,
  SpacePOI,
  OrbitalMarker,
  Player,
}


export class Vector3 {
  X: number;
  Y: number;
  Z: number;

  static Zero: Vector3 = new Vector3(0, 0, 0);
  static UnitX: Vector3 = new Vector3(1, 0, 0);
  static UnitY: Vector3 = new Vector3(0, 1, 0);
  static UnitZ: Vector3 = new Vector3(0, 0, 1);

  constructor(x: number, y: number, z: number) {
    this.X = x;
    this.Y = y;
    this.Z = z;
  }

  Length(): number {
    return this.DistanceTo(Vector3.Zero);
  }

  DistanceTo(other: Vector3): number {
    const diff = other.Subtract(this);
    return Math.sqrt(Math.pow(diff.X, 2) + Math.pow(diff.Y, 2) + Math.pow(diff.Z, 2));
  }

  Add(other: Vector3): Vector3 {
    return new Vector3(this.X + other.X, this.Y + other.Y, this.Z + other.Z);
  }

  Subtract(other: Vector3): Vector3 {
    return new Vector3(this.X - other.X, this.Y - other.Y, this.Z - other.Z);
  }

  Mult(other: Vector3): Vector3 {
    return new Vector3(this.X * other.X, this.Y * other.Y, this.Z * other.Z);
  }

  MultScalar(scalar: number): Vector3 {
    return new Vector3(this.X * scalar, this.Y * scalar, this.Z * scalar);
  }
}
