//Generated: 3/24/2023 10:21:00 PM
//C:\Users\benda\Documents\Visual Studio 2017\Projects\TradeInSpace\TradeInSpace\bin\Debug\netcoreapp2.1\TradeInSpace.Models.dll
import { KeyModel } from './models';
import { EquipmentProperties } from './client_models';
import { LoadoutSlotDTO, LoadoutEntryDTO } from './loadout';

export class GameComponent implements KeyModel {
  Name : string; //String
  Description : string; //String
  Manufacturer : string; //String
  Size : number; //Int32
  AdditionalProperties : string; //String
  GameDatabaseID : number; //Int32
  Key : string; //String
  SCRecordID? : any; //Guid
  ID : number; //Int32
}
export class GameDatabase  {
  VersionID? : number; //Int32
  Name : string; //String
  Description : string; //String
  Enabled : boolean; //Boolean
  DB_Version : string; //String
  DB_Build : number; //Int32
  DB_Channel : string; //String
  DB_BuildTime? : any; //DateTime
  FinishedImporting : boolean; //Boolean
  ImportTime? : any; //DateTime
  Version : Version; //Version
  Ships : GameShip[]; //GameShip
  Equipment : GameEquipment[]; //GameEquipment
  TradeItems : TradeItem[]; //TradeItem
  TradeShops : TradeShop[]; //TradeShop
  SystemBodies : SystemBody[]; //SystemBody
  SystemLocations : SystemLocation[]; //SystemLocation
  LandingZones : SystemLandingZone[]; //SystemLandingZone
  Manufacturers : Manufacturer[]; //Manufacturer
  MiningElements : MiningElement[]; //MiningElement
  HarvestableLocations : HarvestableLocation[]; //HarvestableLocation
  Harvestables : Harvestable[]; //Harvestable
  SubHarvestables : SubHarvestable[]; //SubHarvestable
  MiningCompositions : MiningComposition[]; //MiningComposition
  MiningCompositionEntries : MiningCompositionEntry[]; //MiningCompositionEntry
  MissionGivers : MissionGiver[]; //MissionGiver
  MissionGiverScopes : MissionGiverScope[]; //MissionGiverScope
  Missions : MissionGiverEntry[]; //MissionGiverEntry
  ID : number; //Int32
}
export class GameEquipment implements KeyModel {
  Name : string; //String
  Description : string; //String
  AttachmentType : string; //String
  AttachmentSubtype : string; //String
  Manufacturer : string; //String
  BaseEquipmentID? : number; //Int32
  ManufacturerID? : number; //Int32
  Size : number; //Int32
  Grade : number; //Int32
  Class : string; //String
  Tags : string; //String
  RequiredTags : string; //String
  Health? : number; //Single
  Mass? : number; //Single
  _Manufacturer : Manufacturer; //Manufacturer
  BaseEquipment : GameEquipment; //GameEquipment
  PortLayout : LoadoutSlotDTO; //String
  DefaultLoadout : LoadoutEntryDTO[]; //String
  Properties : EquipmentProperties; //String
  CheapestPrice? : number; //Single
  TradeEntries : TradeEquipmentEntry[]; //TradeEquipmentEntry
  DerivedEquipments : GameEquipment[]; //GameEquipment
  GameDatabaseID : number; //Int32
  Key : string; //String
  SCRecordID? : any; //Guid
  ID : number; //Int32
}
export class GameShip implements KeyModel {
  Name : string; //String
  Description : string; //String
  Manufacturer : string; //String
  CargoCapacity? : number; //Int32
  QuantumDriveSize? : number; //Int32
  Size? : PadSize; //PadSize
  ImageURL : string; //String
  ManufacturerID? : number; //Int32
  _Manufacturer : Manufacturer; //Manufacturer
  AdditionalProperties : any; //String
  GameDatabaseID : number; //Int32
  Key : string; //String
  SCRecordID? : any; //Guid
  ID : number; //Int32
}
export class Harvestable implements KeyModel {
  MiningElementID? : number; //Int32
  MiningCompositionID? : number; //Int32
  MinElevation : number; //Single
  MaxElevation : number; //Single
  RespawnTime : number; //Single
  SubHarvestSlots : number; //Int32
  SubHarvestableSlotChance : number; //Single
  ElementUnits : number; //Int32
  Element : MiningElement; //MiningElement
  Composition : MiningComposition; //MiningComposition
  Locations : HarvestableLocation[]; //HarvestableLocation
  SubHarvestables : SubHarvestable[]; //SubHarvestable
  ParentHarvestables : SubHarvestable[]; //SubHarvestable
  GameDatabaseID : number; //Int32
  Key : string; //String
  SCRecordID? : any; //Guid
  ID : number; //Int32
}
export class SubHarvestable implements KeyModel {
  ParentHarvestableID : number; //Int32
  ChildHarvestableID : number; //Int32
  RelativeProbability : number; //Single
  RespawnMultiplier : number; //Single
  ParentHarvestable : Harvestable; //Harvestable
  ChildHarvestable : Harvestable; //Harvestable
  GameDatabaseID : number; //Int32
  Key : string; //String
  SCRecordID? : any; //Guid
  ID : number; //Int32
}
export class HarvestableLocation implements KeyModel {
  ParentLocationID? : number; //Int32
  HarvestableID? : number; //Int32
  ParentLocation : SystemLocation; //SystemLocation
  Harvestable : Harvestable; //Harvestable
  Name : string; //String
  GameDatabaseID : number; //Int32
  Key : string; //String
  SCRecordID? : any; //Guid
  ID : number; //Int32
}
export class Manufacturer implements KeyModel {
  Name : string; //String
  Description : string; //String
  Code : string; //String
  LogoName : string; //String
  GameDatabaseID : number; //Int32
  Key : string; //String
  SCRecordID? : any; //Guid
  ID : number; //Int32
}
export class MiningComposition implements KeyModel {
  Name : string; //String
  MinimumDistinct : number; //Int32
  Entries : MiningCompositionEntry[]; //MiningCompositionEntry
  GameDatabaseID : number; //Int32
  Key : string; //String
  SCRecordID? : any; //Guid
  ID : number; //Int32
}
export class MiningCompositionEntry implements KeyModel {
  CompositionID? : number; //Int32
  MiningElementID : number; //Int32
  Probability : number; //Single
  MinPercentage : number; //Single
  MaxPercentage : number; //Single
  CurveExponent : number; //Single
  Element : MiningElement; //MiningElement
  Composition : MiningComposition; //MiningComposition
  GameDatabaseID : number; //Int32
  Key : string; //String
  SCRecordID? : any; //Guid
  ID : number; //Int32
}
export class MiningElement implements KeyModel {
  TradeItemID : number; //Int32
  RefinedTradeItemID? : number; //Int32
  Instability : number; //Single
  Resistance : number; //Single
  WindowMidpoint : number; //Single
  WindowMidpointRandomness : number; //Single
  WindowMidpointThinness : number; //Single
  ExplosionMultiplier : number; //Single
  ClusterFactor : number; //Single
  TradeItem : TradeItem; //TradeItem
  RefinedTradeItem : TradeItem; //TradeItem
  GameDatabaseID : number; //Int32
  Key : string; //String
  SCRecordID? : any; //Guid
  ID : number; //Int32
}
export class MissionGiver implements KeyModel {
  SystemLocationID? : number; //Int32
  Name : string; //String
  Description : string; //String
  InviteTimeout : number; //Int32
  VisitTimeout : number; //Int32
  ShortCooldown : number; //Int32
  MediumCooldown : number; //Int32
  LongCooldown : number; //Int32
  Icon : string; //String
  AdditionalProperties : any; //String
  SystemLocation : SystemLocation; //SystemLocation
  Missions : MissionGiverEntry[]; //MissionGiverEntry
  GameDatabaseID : number; //Int32
  Key : string; //String
  SCRecordID? : any; //Guid
  ID : number; //Int32
}
export class MissionGiverEntry implements KeyModel {
  GiverID? : number; //Int32
  AvailableLocationID? : number; //Int32
  SecretMission : boolean; //Boolean
  Title : string; //String
  Description : string; //String
  MissionType : string; //String
  Lawful : boolean; //Boolean
  GiverName : string; //String
  MinUEC : number; //Int32
  MaxUEC : number; //Int32
  PaysBonus : boolean; //Boolean
  BuyInUEC? : number; //Int32
  Requirements : MissionRequirementDTO[]; //String
  RewardSets : MissionRewardSetDTO[]; //String
  Notes : MissionNoteDTO[]; //String
  ReputationRequirement : MissionRequirementDTO; //MissionRequirementDTO
  RewardTypes : string; //String
  RewardTypesFull : string; //String
  Giver : MissionGiver; //MissionGiver
  AvailableLocation : SystemLocation; //SystemLocation
  GameDatabaseID : number; //Int32
  Key : string; //String
  SCRecordID? : any; //Guid
  ID : number; //Int32
}
export class MissionRequirementDTO  {
  Title : string; //String
  Type : string; //RequirementType
  Min? : number; //Single
  Max? : number; //Single
  MissionKeys : string[]; //String[]
  TargetGiverKey : string; //String
  RepScope : string; //String
  MinScope : MissionScopeRankDTO; //MissionScopeRankDTO
  MaxScope : MissionScopeRankDTO; //MissionScopeRankDTO
  TargetGiver : MissionGiver; //MissionGiver
  Missions : MissionGiverEntry[]; //MissionGiverEntry[]
}
export class MissionRewardSetDTO  {
  Title : string; //String
  Rewards : MissionRewardDTO[]; //MissionRewardDTO[]
}
export class MissionScopeRankDTO  {
  Title : string; //String
  Tier : number; //Int32
  Min : number; //Single
  Drift : number; //Single
  DriftHours : number; //Single
}
export class MissionRewardDTO  {
  Type : string; //RewardType
  Amount : number; //Single
  Bonus? : number; //Single
  TargetGiverKey : string; //String
  RepScope : string; //String
  TargetGiver : MissionGiver; //MissionGiver
  Scope : MissionGiverScope; //MissionGiverScope
}
export class MissionNoteDTO  {
  Type : string; //String
  Title : string; //String
}
export enum RequirementType {
  Wanted = 0,
  Virtue = 1,
  Reliability = 2,
  Standing = 3,
  MissionInvite = 4,
  MissionLinked = 5,
  MissionAll = 6,
  MissionAny = 7,
  MissionNone = 8,
  And = 9,
  Or = 10,
}
export enum RewardType {
  Wanted = 0,
  Virtue = 1,
  Reliability = 2,
  Standing = 3,
}
export class MissionGiverScope implements KeyModel {
  Name : string; //String
  Description : string; //String
  StandingInitial : number; //Double
  StandingMax : number; //Double
  IconSVG : string; //String
  AdditionalProperties : any; //String
  GameDatabaseID : number; //Int32
  Key : string; //String
  SCRecordID? : any; //Guid
  ID : number; //Int32
}
export class SystemBody implements KeyModel {
  ParentBodyID? : number; //Int32
  SystemLocationID? : number; //Int32
  Name : string; //String
  Description : string; //String
  OrbitRadius : number; //Single
  OrbitStart : number; //Single
  AtmosphereHeightKM? : number; //Single
  BodyRadius? : number; //Single
  Gravity? : number; //Single
  BodyType : BodyType; //BodyType
  AdditionalProperties : any; //String
  ParentBody : SystemBody; //SystemBody
  SystemLocation : SystemLocation; //SystemLocation
  ChildBodies : SystemBody[]; //SystemBody
  GameDatabaseID : number; //Int32
  Key : string; //String
  SCRecordID? : any; //Guid
  ID : number; //Int32
}
export class SystemLandingZone implements KeyModel {
  LocationID? : number; //Int32
  Name : string; //String
  Description : string; //String
  TPads : number; //Int32
  XSPads : number; //Int32
  SPads : number; //Int32
  MPads : number; //Int32
  LPads : number; //Int32
  XLPads : number; //Int32
  Location : SystemLocation; //SystemLocation
  GameDatabaseID : number; //Int32
  Key : string; //String
  SCRecordID? : any; //Guid
  ID : number; //Int32
}
export class SystemLocation implements KeyModel {
  ParentBodyID? : number; //Int32
  ParentLocationID? : number; //Int32
  Name : string; //String
  Description : string; //String
  AdditionalProperties : any; //String
  IsHidden : boolean; //Boolean
  QTDistance? : number; //Int32
  StarmapID? : any; //Guid
  LocationType : LocationType; //LocationType
  ServiceFlags : ServiceFlags; //ServiceFlags
  ParentBody : SystemLocation; //SystemLocation
  ParentLocation : SystemLocation; //SystemLocation
  ChildLocations : SystemLocation[]; //SystemLocation
  LandingZones : SystemLandingZone[]; //SystemLandingZone
  Harvestables : HarvestableLocation[]; //HarvestableLocation
  Shops : TradeShop[]; //TradeShop
  GameDatabaseID : number; //Int32
  Key : string; //String
  SCRecordID? : any; //Guid
  ID : number; //Int32
}
export class TradeEquipmentEntry  {
  TradeTableID : number; //Int32
  EquipmentID : number; //Int32
  StationID : number; //Int32
  BuyPrice : number; //Single
  SellPrice : number; //Single
  Quantity : number; //Single
  RentalDuration? : number; //Single
  Table : TradeTable; //TradeTable
  Equipment : GameEquipment; //GameEquipment
  Station : TradeShop; //TradeShop
  ID : number; //Int32
}
export class TradeItem implements KeyModel {
  Name : string; //String
  Description : string; //String
  Category : string; //String
  Mineable : boolean; //Boolean
  GameDatabaseID : number; //Int32
  Key : string; //String
  SCRecordID? : any; //Guid
  ID : number; //Int32
}
export class TradeShop implements KeyModel {
  LocationID? : number; //Int32
  Name : string; //String
  Description : string; //String
  ShopType : ShopType; //ShopType
  ClosestApproachKM : number; //Single
  TransitDurationS? : number; //Int32
  TransitNotes : string; //String
  TravelDurationS? : number; //Int32
  TravelNotes : string; //String
  RunningDurationS : number; //Int32
  Location : SystemLocation; //SystemLocation
  TradeEntries : TradeTableEntry[]; //TradeTableEntry
  EquipmentEntries : TradeEquipmentEntry[]; //TradeEquipmentEntry
  GameDatabaseID : number; //Int32
  Key : string; //String
  SCRecordID? : any; //Guid
  ID : number; //Int32
}
export class TradeTable  {
  GameDatabaseID : number; //Int32
  TableCreated : any; //DateTime
  TableUpdated : any; //DateTime
  RelatedSheetID : string; //String
  Name : string; //String
  Active : boolean; //Boolean
  GameDatabase : GameDatabase; //GameDatabase
  Entries : TradeTableEntry[]; //TradeTableEntry
  EquipmentEntries : TradeEquipmentEntry[]; //TradeEquipmentEntry
  ID : number; //Int32
}
export class TradeTableEntry  {
  TradeTableID : number; //Int32
  ItemID : number; //Int32
  StationID : number; //Int32
  Type : TradeType; //TradeType
  MinUnitPrice : number; //Single
  MaxUnitPrice : number; //Single
  UnitCapacityRate : number; //Single
  UnitCapacityMax : number; //Single
  BadTrade : boolean; //Boolean
  Table : TradeTable; //TradeTable
  Item : TradeItem; //TradeItem
  Station : TradeShop; //TradeShop
  ID : number; //Int32
}
export class Version  {
  GameDatabaseID? : number; //Int32
  Name : string; //String
  Description : string; //String
  Enabled : boolean; //Boolean
  Default : boolean; //Boolean
  ReleaseDate? : any; //DateTime
  UpdateDate? : any; //DateTime
  GameDatabases : GameDatabase[]; //GameDatabase
  ID : number; //Int32
}
export enum BodyType {
  Planet = 0,
  Moon = 1,
  AsteroidField = 2,
  Other = 3,
  Star = 4,
}
export enum ServiceFlags {
  None = 0,
  Equipment = 162560,
  Trade = 1,
  Refinery = 2,
  Fine = 4,
  PackageTerminal = 8,
  MissionGiver = 16,
  GreenZone = 32,
  ShipLanding = 64,
  ShipClaim = 128,
  ShipPurchase = 256,
  ShipEquipment = 512,
  ShipRental = 1024,
  FPSArmor = 2048,
  FPSWeapon = 4096,
  FPSEquipment = 8192,
  FPSClothing = 16384,
  FPSRespawnPoint = 32768,
  FPSFood = 65536,
  FPSCrypto = 131072,
  Harvestable = 262144,
  Hacking = 524288,
}
export enum ShopType {
  Other = 0,
  Ignore = 1,
  AdminTerminal = 2,
  AdminRefinery = 3,
  FenceTerminal = 4,
  CommExTransfers = 5,
  TDD = 6,
  Casaba = 7,
  Restaurant = 8,
  ConscientiousObjects = 9,
  AstroArmada = 10,
  VantageRentals = 11,
  PlatinumBay = 12,
  HDShowcase = 13,
  Rentals = 14,
  NewDeal = 15,
  TammanyAndSons = 16,
  BulwarkArmor = 17,
  DumpersDepot = 18,
  LiveFireWeapons = 19,
  GarrityDefense = 20,
  GrandBarter = 21,
  Technotic = 22,
  Teachs = 23,
  Centermass = 24,
  CubbyBlast = 25,
  Skutters = 26,
  TravelerRentals = 27,
  CongreveWeapons = 28,
  Cordrys = 29,
  KCTrending = 30,
  Aparelli = 31,
  OmegaPro = 32,
  RegalLuxury = 33,
  ShubinInterstellar = 34,
  FactoryLine = 35,
  GiftShops = 36,
  ConvenienceStore = 37,
  Bar = 38,
  BurritoBar = 39,
  BurritoCart = 40,
  CafeMusain = 41,
  CoffeToGo = 42,
  Ellroys = 43,
  GarciaGreens = 44,
  GLoc = 45,
  HotdogBar = 46,
  HotdogCart = 47,
  JuiceBar = 48,
  NoodleBar = 49,
  Old38 = 50,
  PizzaBar = 51,
  RacingBar = 52,
  Twyns = 53,
  Whammers = 54,
  Wallys = 55,
  MVBar = 56,
  KelTo = 57,
  CryAstro = 58,
  Covalex = 59,
  Stanton = 60,
  LandingServices = 61,
  ShipWeapons = 62,
  FPSArmor = 63,
  Food = 64,
  CargoOffice = 65,
  CargoOfficeRentals = 66,
  Pharmacy = 67,
  Makau = 68,
  CousinCrows = 69,
  CrusaderShowroom = 70,
}
export enum LocationType {
  Star = 0,
  Planet = 1,
  Moon = 2,
  Asteroid = 3,
  AsteroidField = 4,
  LagrangePoint = 5,
  LandingZone = 6,
  Outpost = 7,
  Manmade = 8,
  Other = 9,
  Store = 10,
  PointOfInterest = 11,
}
export enum EquipmentType {
  Ship = 0,
  GroundVehicle = 1,
}
export enum PadSize {
  Tiny = 1,
  XSmall = 2,
  Small = 3,
  Medium = 4,
  Large = 5,
  XLarge = 6,
}
export enum TradeType {
  Buy = 0,
  Sell = 1,
}
export enum ModifierActivation {
  Passive = 0,
  Active = 1,
}
export enum ModifierMode {
  AdditivePre = 0,
  AdditivePost = 1,
  MultiplicitivePre = 2,
  MultiplicitivePost = 3,
}
export enum ModifierType {
  Heat = 0,
  Signature = 1,
  WeaponBeamStrengthVFX = 2,
  MiningWindowLevel = 3,
  MiningResistance = 4,
  MiningInstability = 5,
  MiningOptimalWindowSize = 6,
  MiningShatterDamage = 7,
  MiningOptimalWindowRate = 8,
  MiningCatastrophicWindowRate = 9,
  WeaponProjectileSpeed = 10,
  WeaponFireRate = 11,
  WeaponSpreadDecy = 12,
  WeaponAmmoCost = 13,
  WeaponHeatGeneration = 14,
  WeaponSoundRadius = 15,
  WeaponRecoilDecay = 16,
  WeaponSpreadMin = 17,
  WeaponSpreadMax = 18,
  WeaponSpreadFirst = 19,
  WeaponSpreadAttack = 20,
  WeaponZoom = 21,
  WeaponDamage = 22,
  MiningFilter = 23,
  SalvageSpeed = 24,
  SalvageRadius = 25,
  SalvageEfficiency = 26,
}
//---End---
