"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Vector3 = exports.CoordinateType = exports.ContainerType = exports.ObjectContainer = exports.Coordinate = void 0;
var generated_models_1 = require("./generated_models");
var Coordinate = /** @class */ (function () {
    function Coordinate() {
    }
    Coordinate.fromJulianCoordinates = function (name, type, globalPosition, parent) {
        var coord = new Coordinate();
        coord.globalPosition = globalPosition;
        coord.timestamp = this.ServerJulianTime;
        coord.setRelativeTo(parent);
        coord.name = name;
        coord.type = type;
        return coord;
    };
    Coordinate.fromGlobalAtTime = function (name, type, globalPosition, time, parent) {
        var coord = new Coordinate();
        coord.globalPosition = globalPosition;
        coord.timestamp = time || this.ServerJulianTime;
        coord.setRelativeTo(parent);
        coord.name = name;
        coord.type = type;
        return coord;
    };
    Coordinate.fromGlobalAtTimeWithLocate = function (name, type, globalPosition, time, systemRoot) {
        var coord = new Coordinate();
        coord.globalPosition = globalPosition;
        coord.timestamp = time;
        coord.name = name;
        coord.type = type;
        coord.findCoordinateOwner(systemRoot);
        return coord;
    };
    Coordinate.prototype.isOnBody = function () {
        return this.parentBody && this.parentBody.bodyRadius != null;
    };
    Coordinate.prototype.findCoordinateOwner = function (rootContainer) {
        var fnSearchContainer = function (target, container) {
            // TODO: Adjustable multiplier
            // TODO: Body radius won't work for stations, need OM distance
            var distanceTo = target.DistanceTo(container.position.globalPosition);
            var distanceFilter = container.omRadius == 0 ? 30000 : container.omRadius * 2;
            //console.log(`${this.name} => ${container.name} = ${distanceTo}`);
            if (distanceTo < distanceFilter) {
                return container;
            }
            for (var idx in container.child_containers) {
                var result = fnSearchContainer(target, container.child_containers[idx]);
                if (result)
                    return result;
            }
            return null;
        };
        this.setRelativeTo(null);
        if (rootContainer) {
            var parentContainer = fnSearchContainer(this.globalPosition, rootContainer);
            this.setRelativeTo(parentContainer);
        }
    };
    Coordinate.prototype.setRelativeTo = function (parentContainer) {
        this.parentBody = parentContainer;
        if (parentContainer) {
            this.localPosition = this.globalPosition.Subtract(this.parentBody.position.globalPosition);
        }
        else {
            this.localPosition = this.globalPosition;
        }
    };
    Coordinate.prototype.rotateToTime = function (newTime) {
        var dateDiff = newTime.getTime() - this.timestamp.getTime();
        var dateDiffDays = Math.abs(dateDiff / (24 * 60 * 60 * 1000));
        if (dateDiffDays == 0)
            return this;
        if (!this.isOnBody())
            return Coordinate.fromGlobalAtTime(this.name, this.type, this.globalPosition, newTime, this.parentBody);
        var LengthOfDayDecimal = this.parentBody.rotationSpeed.X * 3600.0 / 86400.0;
        var JulianDate = dateDiffDays;
        var TotalCycles = JulianDate / LengthOfDayDecimal;
        var CurrentCycleDez = TotalCycles % 1;
        var CurrentCycleDeg = CurrentCycleDez * 360;
        var CurrentCycleAngle = this.parentBody.rotationOffset.X + CurrentCycleDeg;
        if (dateDiff < 0)
            CurrentCycleAngle = 360 - CurrentCycleAngle;
        var CurrentCycleRadians = CurrentCycleAngle / 180 * Math.PI;
        var NewPosition = new Vector3((this.localPosition.X * Math.cos(CurrentCycleRadians) - this.localPosition.Y * Math.sin(CurrentCycleRadians)), (this.localPosition.X * Math.sin(CurrentCycleRadians) + this.localPosition.Y * Math.cos(CurrentCycleRadians)), this.localPosition.Z);
        return Coordinate.fromGlobalAtTime(this.name, this.type, this.parentBody.position.localPosition.Add(NewPosition), newTime, this.parentBody);
    };
    Coordinate.ServerJulianTime = new Date(Date.UTC(2020, 0, 1, 0, 0, 0));
    return Coordinate;
}());
exports.Coordinate = Coordinate;
var ObjectContainer = /** @class */ (function () {
    function ObjectContainer() {
        this.children = [];
        this.child_containers = [];
        this.orbitalMarkers = [];
    }
    ObjectContainer.constructFromSystem = function (systemBody) {
        var fnCreateContainer = function (body, parent) {
            var containerPos = body.AdditionalProperties.GlobalPosition ?
                new Vector3(body.AdditionalProperties.GlobalPosition.X || 0, body.AdditionalProperties.GlobalPosition.Y || 0, body.AdditionalProperties.GlobalPosition.Z || 0) :
                Vector3.Zero;
            var containerRot = body.AdditionalProperties.Rotation ?
                new Vector3(body.AdditionalProperties.Rotation.X || 0, body.AdditionalProperties.Rotation.Y || 0, body.AdditionalProperties.Rotation.Z || 0) :
                Vector3.Zero;
            var container = new ObjectContainer();
            container.dbBody = body;
            container.parent = parent;
            container.name = body.Name;
            container.bodyRadius = body.BodyRadius;
            container.omRadius = body.AdditionalProperties.OMRadius || 0;
            container.position = Coordinate.fromJulianCoordinates(container.name, CoordinateType.SpacePOI, containerPos, parent);
            container.rotationOffset = containerRot;
            container.rotationSpeed = new Vector3(body.AdditionalProperties.RotationSpeed, 0, 0);
            container.generateOrbitalMarkers();
            for (var idx in body.SystemLocation.ChildLocations) {
                var child = body.SystemLocation.ChildLocations[idx];
                if (child.LocationType == generated_models_1.LocationType.Planet ||
                    child.LocationType == generated_models_1.LocationType.Moon)
                    continue;
                var childPos = child.AdditionalProperties.GlobalPosition ?
                    new Vector3(child.AdditionalProperties.GlobalPosition.X || 0, child.AdditionalProperties.GlobalPosition.Y || 0, child.AdditionalProperties.GlobalPosition.Z || 0) :
                    Vector3.Zero;
                container.children.push(Coordinate.fromJulianCoordinates(child.Name, CoordinateType.PlanetPOI, childPos, container));
            }
            for (var idx in body.ChildBodies) {
                var childBody = fnCreateContainer(body.ChildBodies[idx], container);
                container.child_containers.push(childBody);
            }
            return container;
        };
        return fnCreateContainer(systemBody);
    };
    ObjectContainer.prototype.findContainer = function (searchName, recusive) {
        var fnSearch = function (container) {
            if (container.name == searchName)
                return container;
            for (var idx in container.child_containers) {
                var match = fnSearch(container.child_containers[idx]);
                if (match)
                    return match;
            }
        };
        return fnSearch(this);
    };
    ObjectContainer.prototype.generateOrbitalMarkers = function () {
        if (this.omRadius) {
            this.orbitalMarkers.push(Coordinate.fromJulianCoordinates("OM1", CoordinateType.OrbitalMarker, Vector3.UnitZ.MultScalar(this.omRadius), this), Coordinate.fromJulianCoordinates("OM2", CoordinateType.OrbitalMarker, Vector3.UnitZ.MultScalar(-this.omRadius), this), Coordinate.fromJulianCoordinates("OM3", CoordinateType.OrbitalMarker, Vector3.UnitY.MultScalar(this.omRadius), this), Coordinate.fromJulianCoordinates("OM4", CoordinateType.OrbitalMarker, Vector3.UnitY.MultScalar(-this.omRadius), this), Coordinate.fromJulianCoordinates("OM5", CoordinateType.OrbitalMarker, Vector3.UnitX.MultScalar(this.omRadius), this), Coordinate.fromJulianCoordinates("OM6", CoordinateType.OrbitalMarker, Vector3.UnitX.MultScalar(-this.omRadius), this));
        }
    };
    return ObjectContainer;
}());
exports.ObjectContainer = ObjectContainer;
var ContainerType;
(function (ContainerType) {
    ContainerType[ContainerType["Planet"] = 0] = "Planet";
    ContainerType[ContainerType["Space"] = 1] = "Space";
    ContainerType[ContainerType["Unknown"] = 2] = "Unknown";
})(ContainerType = exports.ContainerType || (exports.ContainerType = {}));
var CoordinateType;
(function (CoordinateType) {
    CoordinateType[CoordinateType["PlanetPOI"] = 0] = "PlanetPOI";
    CoordinateType[CoordinateType["SpacePOI"] = 1] = "SpacePOI";
    CoordinateType[CoordinateType["OrbitalMarker"] = 2] = "OrbitalMarker";
    CoordinateType[CoordinateType["Player"] = 3] = "Player";
})(CoordinateType = exports.CoordinateType || (exports.CoordinateType = {}));
var Vector3 = /** @class */ (function () {
    function Vector3(x, y, z) {
        this.X = x;
        this.Y = y;
        this.Z = z;
    }
    Vector3.prototype.Length = function () {
        return this.DistanceTo(Vector3.Zero);
    };
    Vector3.prototype.DistanceTo = function (other) {
        var diff = other.Subtract(this);
        return Math.sqrt(Math.pow(diff.X, 2) + Math.pow(diff.Y, 2) + Math.pow(diff.Z, 2));
    };
    Vector3.prototype.Add = function (other) {
        return new Vector3(this.X + other.X, this.Y + other.Y, this.Z + other.Z);
    };
    Vector3.prototype.Subtract = function (other) {
        return new Vector3(this.X - other.X, this.Y - other.Y, this.Z - other.Z);
    };
    Vector3.prototype.Mult = function (other) {
        return new Vector3(this.X * other.X, this.Y * other.Y, this.Z * other.Z);
    };
    Vector3.prototype.MultScalar = function (scalar) {
        return new Vector3(this.X * scalar, this.Y * scalar, this.Z * scalar);
    };
    Vector3.Zero = new Vector3(0, 0, 0);
    Vector3.UnitX = new Vector3(1, 0, 0);
    Vector3.UnitY = new Vector3(0, 1, 0);
    Vector3.UnitZ = new Vector3(0, 0, 1);
    return Vector3;
}());
exports.Vector3 = Vector3;
//# sourceMappingURL=coordinates.js.map