import { SliceMaterial } from "../three_plugins/SliceMaterial";
import { SystemLocation, GameShip, TradeShop, TradeTableEntry, TradeItem, SystemBody, PadSize, GameEquipment, ModifierType, ModifierMode, ModifierActivation } from "./generated_models";

export enum PreferenceEnum {
  Prefer,
  Neurtral,
  Avoid,
  Ignore
};

export interface FilterParams {
  city_preference: PreferenceEnum;
  hidden_preference: PreferenceEnum;
  quantum_preference: PreferenceEnum;
  hv_cargo_preference: PreferenceEnum;
  short_route_preference: PreferenceEnum;

  minimum_pad_size: PadSize;
  looping: boolean;

  total_time_max: number;
  buy_time_max: number;
  transit_time_max: number;
  ship_time_max: number;
  sell_time_max: number;

  station_keys: string[];
  item_keys: string[];

  sort_column: string;
  sort_direction: number;
};

export interface LoopFilterParams {
  location_keys: string[];
  item_keys: string[];

  allow_planet: boolean;
  allow_hidden: boolean;
  inclusive_location: boolean;
  inclusive_item: boolean;

  minimum_pad_size: PadSize;

  buy_time_max: number;
  sell_time_max: number;
  transit_time_max: number;
};

export interface CalculatedRoute {
  from: TradeShop;
  to: TradeShop;
  item: TradeItem;
  buy_entry: TradeTableEntry;
  sell_entry: TradeTableEntry;

  from_name: string;
  to_name: string;
  item_name: string;

  avg_profit_pct: number;
  max_profit_pct: number;

  min_buy: number;
  avg_buy: number;
  max_buy: number;

  min_sell: number;
  avg_sell: number;
  max_sell: number;

  min_profit: number;
  avg_profit: number;
  max_profit: number;

  buy_rate: number;
  sell_rate: number;
  avg_rate: number;
};

export interface DisplayRoute extends CalculatedRoute {

  itinerary: Itinerary;

  rankProperties: PropertySet;

  score: number;
  rank: number;
  hidden: boolean;

  avg_profit_pct: number;
  total_time: number;
  total_buy: number;
  total_transit: number;
  total_quantum: number;
  total_sell: number;
  jumps: number;
  rate: number;

  cargo_units: number;

  //Calculated fields
  total_jumps: number;
  avg_hold_cost: number;
  avg_hold_profit: number;
};

export interface JumpDetails {
  total_time: number;
  total_disance_km: number;
  fuel_consumption: number;

  accel_time: number;
  cruise_time: number;
  decel_time: number;
}

export interface FullJumpDetails {
  duration: number;
  max_velocity: number;

  start: FullJumpDetailPoint;
  accel_finished: FullJumpDetailPoint;
  decel_started: FullJumpDetailPoint;
  end: FullJumpDetailPoint;
}

export interface FullJumpDetailPoint {
  description: string;
  velocity: number;
  distance_traveled: number;
  distance_remaining: number;
  time_elapsed: number;
  time_remaining: number;
  fuel_usage: number;
}

export interface BaseLoop {
  body_ids: number[];
  item_ids: number[];
};

export interface BodyTradeDetails {
  body: SystemBody;
  buy: TradeTableEntry[];
  sell: TradeTableEntry[];
};

export interface PropertySet {
  [propertyName: string]: PropertyRankValue;
};

export interface PropertyRankValue {
  value: number;
  percent: number;
  color: string;
};

export interface Itinerary {
  steps: ItineraryStep[];

  totalDurationS: number;
  totalBuyS: number;
  totalTransitS: number;
  totalShipS: number;
  totalSellS: number;

  totalJumps: number;
};

export interface ItineraryStep {
  title: string;
  stepType: ItineraryStepType;
  from: SystemLocation;
  to: SystemLocation;
  durationS: number;
};

export enum ItineraryStepType {
  QuantumJump,
  ShipFlight,
  Running,
  Transit,
  BuyGoods,
  SellGoods
};

export interface EquipmentProperties {

  //Any Heat/Power User
  Heat?: {
    HeatBase: number;
    HeatActive: number;
    TemperatureToIR: number;
    CoolingRate: number;
  }

  Energy?: {
    PowerBase: number;
    PowerActive: number;
    PowerToEM: number;
    DecayRateEM: number;
  }

  PowerPlant?: {
    PowerBase: number;
    PowerActive: number;
    PowerToEM: number;
    DecayRateEM: number;
  }

  //Cooler
  Cooler?: {
    CoolingRate: number;
  }

  //Quantum Drive
  QDrive?: {
    JumpRange: number;
    DriveSpeed: number;
    CooldownTime: number;
    S1AccelRate: number;
    S2AccelRate: number;
    EngageSpeed: number;
    SpoolTime: number;
    FuelConsumptionRate: number;
  }

  //Weapon
  Weapon?: {
    Actions: FiringAction[];
  }

  //Container
  Cargo?: {
    CrateGenRate: number;
    CrateMax: number;
    MiningOnly: boolean;
    DimX: number;
    DimY: number;
    DimZ: number;
    SCU: number;
  }

  //Shield
  Shield?: {
    MaxShield: number;
    MaxRegenRate: number;
    DownedRegenDelay: number;
    DamagedRegenDelay: number;
    HardenFactor: number;
    HardenDuration: number;
    HardenCooldown: number;
  }

  //Fuel
  FuelTank?: {
    FuelFillRate: number;
    FuelDrainRate: number;
    FuelCapacity: number;
  }

  //Quantum Fuel
  QuantumFuelTank?: {
    QuantumFuelFillRate: number;
    QuantumFuelDrainRate: number;
    QuantumFuelCapacity: number;
  }

  Attachable?: {
    ShouldAttach: boolean;
  }

  //AmmoContainer
  Ammo?: MagazineData;

  //Armor
  Armor?: {
    SignalInfrared: number;
    SignalElectromagnetic: number;
    SignalCrossSection: number;

    DamagePhysical: number;
    DamageEnergy: number;
    DamageDistortion: number;
    DamageThermal: number;
    DamageBiochemical: number;
    DamageStun: number;
  }

  Missile?: {
    TrackingData: TrackingData;
    MissileSpeed: number;
    ExplosionRadiusMin: number;
    ExplosionRadiusMax: number;

    ExplosionDamage: DamageInfo;
  }

  MiningLaser?: {
    ResistanceModifier: number;
    MUL_InstabilityModifier: number;
    MUL_WindowSizeModifier: number;
    MUL_ShatterDamageModifier: number;
    MUL_OptimalChargeRate: number;
    MUL_OverchargeRate: number;
  }

  IFCS?: {
    MaxSpeed: number;
    MaxBoostSpeed: number;
  }

  Food?: {
    Hunger: number;
    Thirst: number;
    Uses: number;
    Effects: FoodBuff[];
  }

  FPSArmor?: {
    Resistance: DamageInfo;
    ProtectedBodyParts: string[];
  }

  Chip?: {
    Duration: number;
    ErrorChance: number;
  }

  Modifier: {
    Charges: number;
    Activation: ModifierActivation;
    Interruptable: boolean;
    Modifiers: Modifier[];
  }
}

export interface Modifier {
  Type: ModifierType;
  Mode: ModifierMode;
  Value: number;
  Lifetime: number;
  Delay: number;
}

export interface FoodBuff {
  BuffType: string;
  Duration?: number;
  Value?: number;
}

export interface TrackingData {
  SignalType: string;
  MinimumValue: number;
  MaxDistance: number;
  Angle: number;
  LockTime: number;
}

export interface FiringAction {
  Type: string;
  ModeName: string;
  Name: string;

  //Sequenced
  FireActions?: FiringAction[];

  //Normal
  FireRate?: number;
  HeatPerShot?: number;
  PelletCount?: number;
  FireMissile?: boolean;
  ShotCount: number;

  //Charge
  ChargeTime?: number;
  CooldownTime?: number;
  ChargedMultiplier?: number;
}

export interface MagazineData {
  StartAmmo: number;
  MaxAmmo: number;
  AmmoData: AmmoData;
}

export interface AmmoData {
  Size: number;
  Lifetime: number;
  BulletType: number;
  Speed: number;
  HitType: string;
  MinRadius: number;
  MaxRadius: number;
  
  BaseDamage: DamageInfo;
  FalloffMinDistance: DamageInfo;
  FalloffPerMeter: DamageInfo;
  FalloffMinDamage: DamageInfo;
  ExplosionDamage: DamageInfo;
}

export interface DamageInfo {
  Physical: number;
  Energy: number;
  Distortion: number;
  Thermal: number;
  Biochemical: number;
  Stun: number;
}

export interface JumpProfile {
  AccelerationTime: number;
  TopSpeedTime: number;
  SlowdownTime: number;

  FuelUsage: number;
  DistanceTraveled: number;
}

export interface DamageSummary {
  volley: DamageInfo;
  volley_flat: number;
  sustain: DamageInfo;
  sustain_flat: number;
  missile: DamageInfo;
  missile_flat: number;
  missile_count: number;

  projectiles: number;
  effective_rate: number;
  cooldown_time: number;
  heat_generation: number;
  mode: DamageMode;
}

export enum DamageMode {
  Single,
  Burst,
  Automatic,
  Scattergun,
  Charge,
  Missile,
  Multiple
}

export const ZeroDamage: DamageInfo = { Physical: 0, Energy: 0, Distortion: 0, Thermal: 0, Biochemical: 0, Stun: 0 };

export function AddDamage(A: DamageInfo, B: DamageInfo): DamageInfo {
  return {
    Physical: A.Physical + B.Physical,
    Energy: A.Energy + B.Energy,
    Distortion: A.Distortion + B.Distortion,
    Thermal: A.Thermal + B.Thermal,
    Biochemical: A.Biochemical + B.Biochemical,
    Stun: A.Stun + B.Stun
  };
}

export function MultDamage(A: DamageInfo, amount: number): DamageInfo {
  return {
    Physical: A.Physical * amount,
    Energy: A.Energy * amount,
    Distortion: A.Distortion * amount,
    Thermal: A.Thermal * amount,
    Biochemical: A.Biochemical * amount,
    Stun: A.Stun * amount
  };
}

export function CollapseDamage(A: DamageInfo): number {
  return A ? (A.Physical + A.Energy + A.Distortion + A.Thermal + A.Biochemical + A.Stun) : 0;
}

export function CollapseResistance(A: DamageInfo): number {
  return CollapseDamage(A) / 6;
}

export interface SlotDisplay {
  order: number;
  class: string;
  title: string;
  locked: boolean;
  types: string[];
};

export interface LayerConfiguration {
  default: number,
  hull_pass: number,
  outline_pass: number,
  navigation_pass: number,
  gadgets_layer: number,

  base_material: SliceMaterial,
  equipment_material: SliceMaterial,
  traversal_material: SliceMaterial
}
