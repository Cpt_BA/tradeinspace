import { KeyModel, MapKey, KeyMapping, IdMapping, MapId } from "./models";
import { TradeItem, TradeShop, SystemBody, SystemLocation, GameDatabase, TradeTable, SystemLandingZone, GameShip, GameEquipment, Harvestable, SubHarvestable, MiningElement, MiningComposition, MiningCompositionEntry, HarvestableLocation, MissionGiver, MissionGiverEntry, MissionRequirementDTO, MissionGiverScope, Manufacturer } from "./generated_models"
import { ObjectContainer } from "./coordinates";

export function GameDatabaseLoaded(gameDB: GameDatabase): GameDatabaseFull {
  const fullDB = gameDB as GameDatabaseFull;
  console.log("== GameDB Loaded ==");

  fullDB.BodyMap = MapId(gameDB.SystemBodies);
  fullDB.BodyKeyMap = MapKey(gameDB.SystemBodies);
   
  fullDB.LocationMap = MapId(gameDB.SystemLocations);
  fullDB.LocationKeyMap = MapKey(gameDB.SystemLocations);
  fullDB.LandingZoneMap = MapId(gameDB.LandingZones);
  fullDB.LandingZoneKeyMap = MapKey(gameDB.LandingZones);

  fullDB.ShipMap = MapId(gameDB.Ships);
  fullDB.ShipKeyMap = MapKey(gameDB.Ships);
  fullDB.EquipmentMap = MapId(gameDB.Equipment);
  fullDB.EquipmentKeyMap = MapKey(gameDB.Equipment);

  fullDB.ItemMap = MapId(gameDB.TradeItems);
  fullDB.ItemKeyMap = MapKey(gameDB.TradeItems);
  fullDB.ShopMap = MapId(gameDB.TradeShops);
  fullDB.ShopKeyMap = MapKey(gameDB.TradeShops);
  fullDB.ManufacturerMap = MapId(gameDB.Manufacturers);
  fullDB.ManufacturerKeyMap = MapKey(gameDB.Manufacturers);

  fullDB.HarvestableMap = MapId(gameDB.Harvestables);
  fullDB.SubHarvestableMap = MapId(gameDB.SubHarvestables);
  fullDB.HarvestableLocationsMap = MapId(gameDB.HarvestableLocations);

  fullDB.MiningElementMap = MapId(gameDB.MiningElements);
  fullDB.MiningCompositionMap = MapId(gameDB.MiningCompositions);
  fullDB.MiningCompositionEntryMap = MapId(gameDB.MiningCompositionEntries);

  fullDB.MissionGiversMap = MapId(gameDB.MissionGivers);
  fullDB.MissionGiversKeyMap = MapKey(gameDB.MissionGivers);
  fullDB.MissionsMap = MapId(gameDB.Missions);
  fullDB.MissionsKeyMap = MapKey(gameDB.Missions);

  fullDB.ScopeKeyMap = MapKey(gameDB.MissionGiverScopes);

  gameDB.Ships.forEach(s => {
    s.AdditionalProperties = s.AdditionalProperties ? JSON.parse(s.AdditionalProperties) : [];

    s._Manufacturer = fullDB.ManufacturerMap[s.ManufacturerID];
  });

  gameDB.Equipment.forEach(eq => {
    //Web service sends as JSON strings, just unpack them here
    eq.Properties = eq ? JSON.parse(eq.Properties as any) : {};
    eq.PortLayout = eq ? JSON.parse(eq.PortLayout as any) : {};
    eq.DefaultLoadout = eq ? JSON.parse(eq.DefaultLoadout as any) : {};

    eq._Manufacturer = fullDB.ManufacturerMap[eq.ManufacturerID];
  });

  gameDB.TradeItems.forEach(ti => {
    
  });

  gameDB.TradeShops.forEach(s => {
    s.Location = fullDB.LocationMap[s.LocationID];
  });

  gameDB.LandingZones.forEach(lz => {
    lz.Location = fullDB.LocationMap[lz.LocationID];
  });

  gameDB.SystemBodies.forEach(b => {
    b.AdditionalProperties = b.AdditionalProperties ? JSON.parse(b.AdditionalProperties) : [];

    b.ParentBody = fullDB.BodyMap[b.ParentBodyID];
    b.SystemLocation = fullDB.LocationMap[b.SystemLocationID];

    b.ChildBodies = gameDB.SystemBodies.filter(sb => sb.ParentBodyID === b.ID);
  });

  gameDB.SystemLocations.forEach(l => {
    l.AdditionalProperties = l.AdditionalProperties ? JSON.parse(l.AdditionalProperties) : [];

    l.ParentLocation = fullDB.LocationMap[l.ParentLocationID];
    l.ParentBody = fullDB.LocationMap[l.ParentBodyID];

    l.ChildLocations = gameDB.SystemLocations.filter(sl => sl.ParentLocationID === l.ID);
    l.LandingZones = gameDB.LandingZones.filter(lz => lz.LocationID === l.ID);
    l.Shops = gameDB.TradeShops.filter(s => s.LocationID === l.ID)
    l.Harvestables = gameDB.HarvestableLocations.filter(hl => hl.ParentLocationID === l.ID);
  });

  gameDB.Harvestables.forEach(h => {
    h.Element = fullDB.MiningElements.find(me => me.ID === h.MiningElementID);
    h.Composition = fullDB.MiningCompositions.find(mc => mc.ID === h.MiningCompositionID);

    h.SubHarvestables = fullDB.SubHarvestables.filter(sh => sh.ParentHarvestableID === h.ID);
    h.ParentHarvestables = fullDB.SubHarvestables.filter(sh => sh.ChildHarvestableID === h.ID);
    h.Locations = fullDB.HarvestableLocations.filter(hl => hl.HarvestableID === h.ID);
  });

  gameDB.SubHarvestables.forEach(sh => {
    sh.ParentHarvestable = fullDB.HarvestableMap[sh.ParentHarvestableID];
    sh.ChildHarvestable = fullDB.HarvestableMap[sh.ChildHarvestableID];
  });

  gameDB.HarvestableLocations.forEach(hl => {
    hl.ParentLocation = fullDB.LocationMap[hl.ParentLocationID];
    hl.Harvestable = fullDB.HarvestableMap[hl.HarvestableID];
  });

  gameDB.MiningElements.forEach(me => {
    me.TradeItem = fullDB.ItemMap[me.TradeItemID];
  });

  gameDB.MiningCompositions.forEach(mc => {
    mc.Entries = fullDB.MiningCompositionEntries.filter(mce => mce.CompositionID === mc.ID);
  });

  gameDB.MiningCompositionEntries.forEach(mce => {
    mce.Composition = fullDB.MiningCompositionMap[mce.CompositionID];
    mce.Element = fullDB.MiningElementMap[mce.MiningElementID];
  });

  gameDB.MissionGiverScopes.forEach(mgs => {
    mgs.AdditionalProperties = mgs.AdditionalProperties ? JSON.parse(mgs.AdditionalProperties) : [];
  });

  gameDB.MissionGivers.forEach(mg => {
    mg.Missions = [];
    mg.SystemLocation = mg.SystemLocationID ? fullDB.LocationMap[mg.SystemLocationID] : null;
  });

  gameDB.Missions.forEach(m => {
    if (m.GiverID) {
      m.Giver = fullDB.MissionGiversMap[m.GiverID];
      m.Giver.Missions.push(m);
    }
    if (m.AvailableLocationID) {
      m.AvailableLocation = fullDB.LocationMap[m.AvailableLocationID];
    }
    m.Requirements = m.Requirements ? JSON.parse(m.Requirements as any) : [];
    m.RewardSets = m.RewardSets ? JSON.parse(m.RewardSets as any) : [];
    m.Notes = m.Notes ? JSON.parse(m.Notes as any) : [];

    if (!m.MaxUEC) {
      m.MaxUEC = m.MinUEC;
    }

    m.Requirements.forEach(r => {
      r.Missions = r.MissionKeys ? r.MissionKeys.map(mKey => fullDB.MissionsKeyMap[mKey]).filter(m => !m.SecretMission) : [];
      r.TargetGiver = r.TargetGiverKey ? fullDB.MissionGiversKeyMap[r.TargetGiverKey] : null;
      if (r.RepScope) {
        const rep_scope = fullDB.ScopeKeyMap[r.RepScope];
        const scope_ranks: any[] = rep_scope.AdditionalProperties.Ranks;
        if (r.Min !== undefined)
          r.MinScope = scope_ranks.find(rank => rank.Min >= r.Min)
        if (r.Max !== undefined)
          r.MaxScope = scope_ranks.find(rank => rank.Min >= r.Max)

        if (m.ReputationRequirement == null && !rep_scope.Name.includes("fired"))
          m.ReputationRequirement = r;
      }
    });

    m.RewardSets.forEach(rs => {
      rs.Rewards.forEach(r => {
        r.TargetGiver = r.TargetGiverKey ? fullDB.MissionGiversKeyMap[r.TargetGiverKey] : null;
        r.Scope = fullDB.ScopeKeyMap[r.RepScope];
      });
    });

    if (m.RewardSets && m.RewardSets[0]) {
      m.RewardTypes = m.RewardSets[0].Rewards
        .map(r => r.Type)
        .filter((value, index, self) => { return self.indexOf(value) === index; }) //Unique values only
        .join(", ");
      m.RewardTypesFull = m.RewardSets[0].Rewards
        .map(r => `${r.Type} (${r.Amount})`)
        .filter((value, index, self) => { return self.indexOf(value) === index; }) //Unique values only
        .join(", ");
    }
  });

  if ("stanton" in fullDB.BodyKeyMap)
    fullDB.SystemStructureRoot = ObjectContainer.constructFromSystem(fullDB.BodyKeyMap["stanton"]);

  return fullDB;
}

export function TradeTableLoaded(gameDB: GameDatabaseFull, tradeTable: TradeTable): void {
  console.log("== TradeTable Loaded ==");

  gameDB.TradeShops.forEach(ts => {
    //Both of these are added later when trades are iterated
    ts.TradeEntries = [];
    ts.EquipmentEntries = [];
  });

  gameDB.Equipment.forEach(e => {
    //Same thing here, too expensive to calculate here
    e.TradeEntries = [];
  });

  tradeTable.Entries.forEach(e => {
    e.Station = gameDB.ShopMap[e.StationID];
    e.Item = gameDB.ItemMap[e.ItemID];
    e.Table = tradeTable;

    e.Station.TradeEntries.push(e);
  });

  tradeTable.EquipmentEntries.forEach(ee => {
    ee.Station = gameDB.ShopMap[ee.StationID];
    ee.Equipment = gameDB.EquipmentMap[ee.EquipmentID];
    ee.Table = tradeTable;

    //Add to the equipment's list
    if (ee.BuyPrice > 0 && !ee.RentalDuration) {
      ee.Equipment.TradeEntries.push(ee);
    }

    ee.Station.EquipmentEntries.push(ee);
  });

  //Second-pass for equipment to sort the trades and calculate cheapest price
  gameDB.Equipment.forEach(e => {
    e.TradeEntries = e.TradeEntries.sort((a, b) => a.BuyPrice - b.BuyPrice);
    if (e.TradeEntries.length > 0) {
      e.CheapestPrice = e.TradeEntries[0].BuyPrice
    } else {
      e.CheapestPrice = null;
    }
  });
}

export interface GameDatabaseFull extends GameDatabase {
  BodyMap: IdMapping<SystemBody>;
  BodyKeyMap: KeyMapping<SystemBody>;
  LocationMap: IdMapping<SystemLocation>;
  LocationKeyMap: KeyMapping<SystemLocation>;
  LandingZoneMap: IdMapping<SystemLandingZone>;
  LandingZoneKeyMap: KeyMapping<SystemLandingZone>;

  ShipMap: IdMapping<GameShip>;
  ShipKeyMap: KeyMapping<GameShip>;
  EquipmentMap: IdMapping<GameEquipment>;
  EquipmentKeyMap: KeyMapping<GameEquipment>;

  ItemMap: IdMapping<TradeItem>;
  ItemKeyMap: KeyMapping<TradeItem>;
  ShopMap: IdMapping<TradeShop>;
  ShopKeyMap: KeyMapping<TradeShop>;
  ManufacturerMap: IdMapping<Manufacturer>;
  ManufacturerKeyMap: KeyMapping<Manufacturer>;

  HarvestableMap: IdMapping<Harvestable>;
  SubHarvestableMap: IdMapping<SubHarvestable>;
  HarvestableLocationsMap: IdMapping<HarvestableLocation>;

  MiningElementMap: IdMapping<MiningElement>;
  MiningCompositionMap: IdMapping<MiningComposition>;
  MiningCompositionEntryMap: IdMapping<MiningCompositionEntry>;

  MissionGiversMap: IdMapping<MissionGiver>;
  MissionGiversKeyMap: KeyMapping<MissionGiver>;
  MissionsMap: IdMapping<MissionGiverEntry>;
  MissionsKeyMap: KeyMapping<MissionGiverEntry>;

  ScopeKeyMap: KeyMapping<MissionGiverScope>;

  SystemStructureRoot: ObjectContainer;
}
