import { SystemBody, GameShip, TradeItem, TradeShop, SystemLocation, TradeTableEntry, TradeTable, TradeType, GameEquipment, LocationType } from "./generated_models";
import { Itinerary, ItineraryStep, ItineraryStepType, DisplayRoute, CalculatedRoute, FilterParams } from "./client_models";
import { BasicUserSettings } from "./models";
import { GameDatabase } from "./generated_models";
import { GameDatabaseFull } from './game_data';
import { ModelUtils } from '../utilities/model_utils';
import { fmtNumber } from '../pipes/scaleunit_pipe';

export class ItineraryBuilder {

  static CalculateForRoute(gameData: GameDatabaseFull, inShip: GameShip, withDrive: GameEquipment,
    route: DisplayRoute, userData: BasicUserSettings, filterSettings: FilterParams): Itinerary {
    const itinerary: Itinerary = {
      steps: [],
      totalDurationS: 0,
      totalBuyS: 0,
      totalTransitS: 0,
      totalShipS: 0,
      totalSellS: 0,
      totalJumps: 0
    };

    if (!filterSettings.looping) {
      if (userData.current_location && userData.use_location && userData.current_location !== route.from.Key) {
        this.TerminalToTerminal(gameData, inShip, withDrive, gameData.ShopKeyMap[userData.current_location], route.from, (newStep) => itinerary.steps.push(newStep));
      }
    }

    itinerary.steps.push(this.TradeTerminal(gameData, route.buy_entry, route.cargo_units));
    this.TerminalToTerminal(gameData, inShip, withDrive, route.from, route.to, (newStep) => itinerary.steps.push(newStep));
    itinerary.steps.push(this.TradeTerminal(gameData, route.sell_entry, route.cargo_units));

    if (filterSettings.looping) {
      this.TerminalToTerminal(gameData, inShip, withDrive, route.to, route.from, (newStep) => itinerary.steps.push(newStep));
    }

    itinerary.steps.forEach((step) => {
      itinerary.totalDurationS += step.durationS;
      switch (step.stepType) {
        case ItineraryStepType.BuyGoods:
          itinerary.totalBuyS += step.durationS;
          break;
        case ItineraryStepType.Running:
        case ItineraryStepType.Transit:
          itinerary.totalTransitS += step.durationS;
          break;
        case ItineraryStepType.ShipFlight:
          itinerary.totalShipS += step.durationS;
          break;
        case ItineraryStepType.QuantumJump:
          itinerary.totalShipS += step.durationS;
          itinerary.totalJumps += 1;
          break;
        case ItineraryStepType.SellGoods:
          itinerary.totalSellS += step.durationS;
          break;
      }
    });

    route.itinerary = itinerary;
    return itinerary;
  }

  static TerminalToTerminal(gameData: GameDatabaseFull, inShip: GameShip, withDrive: GameEquipment, from: TradeShop, to: TradeShop, fnAddElem: (step: ItineraryStep) => void): void {
    //const fromBody: SystemBody = gameData.SystemBodies.find(b => b.SystemLocation == from.Location.ParentBody);
    //const toBody: SystemBody = gameData.SystemBodies.find(b => b.SystemLocation == to.Location.ParentBody);

    const fromBody: SystemBody = gameData.BodyKeyMap[from.Location.ParentBody.Key];
    const toBody: SystemBody = gameData.BodyKeyMap[to.Location.ParentBody.Key];

    if (!fromBody) debugger;
    if (!toBody) debugger;

    const fromRoot = ModelUtils.getRootBody(gameData, fromBody);
    const toRoot = ModelUtils.getRootBody(gameData, toBody);

    let currentBody = fromBody;

    this.StationToOrbit(gameData, inShip, from, fnAddElem);

    if (fromRoot != toRoot) {
      fnAddElem(this.BodyToBody(gameData, withDrive, currentBody, toRoot));
      currentBody = toRoot;
    }

    if (currentBody != toBody) {
      fnAddElem(this.BodyToBody(gameData, withDrive, currentBody, toBody));
      currentBody = toBody;
    }

    this.OrbitToStation(gameData, inShip, withDrive, to, fnAddElem);
  }

  static TradeTerminal(gameData: GameDatabaseFull, entry: TradeTableEntry, units: number): ItineraryStep {
    const entryBody: SystemLocation = entry.Station.Location.ParentBody;
    return {
      stepType: entry.Type === TradeType.Sell ? ItineraryStepType.SellGoods : ItineraryStepType.BuyGoods,
      title: `${entry.Type === TradeType.Sell ? 'Sell' : 'Buy'} ${fmtNumber(units, true, " units", "", true)} of ${entry.Item.Name} @ ${fmtNumber(entry.UnitCapacityRate / 100, false, ' scu/min')}.`,
      from: entryBody,
      to: entryBody,
      durationS: (units / entry.UnitCapacityRate) * 60
    }
  }

  static BodyToBody(gameData: GameDatabaseFull, withDrive: GameEquipment, fromBody: SystemBody, toBody: SystemBody): ItineraryStep {
    const qProps = withDrive.Properties.QDrive;
    const jumpDetails = ModelUtils.calculateQuantumJump(fromBody, toBody, withDrive);

    return {
      stepType: ItineraryStepType.QuantumJump,
      title: `QT Jump from ${fromBody.Name} -> ${toBody.Name}. ${qProps.SpoolTime} Second Spool.`,
      from: fromBody.SystemLocation,
      to: toBody.SystemLocation,
      durationS: jumpDetails.total_time + qProps.SpoolTime
    };
  }

  static StationToOrbit(gameData: GameDatabaseFull, inShip: GameShip, station: TradeShop, fnAddElem: (step: ItineraryStep) => void): void {
    const stationBody: SystemLocation = station.Location.ParentBody;

    if (station.RunningDurationS && station.RunningDurationS !== 0) {
      fnAddElem({
        stepType: ItineraryStepType.Running,
        from: stationBody,
        to: station.Location,
        title: "Run from trade terminal.",
        durationS: station.RunningDurationS
      });
    }

    //We check !IsHidden, because if it is a hidden location, we assume all transit time is just finding the location, so theres no out-going transit time.
    if (!station.Location.IsHidden && station.TransitDurationS && station.TransitDurationS !== 0) {
      fnAddElem({
        stepType: ItineraryStepType.Transit,
        from: station.Location,
        to: stationBody,
        title: `Transit from station to spaceport.` + (station.TransitNotes ?`(${station.TransitNotes})` : ""),
        durationS: station.TransitDurationS
      });
    }

    fnAddElem({
      stepType: ItineraryStepType.ShipFlight,
      from: stationBody,
      to: stationBody,
      title: `Takeoff in ship`,
      durationS: 60 //TODO: Allow user to configure landing time
    });

    const leave = this.LeaveBody(gameData, inShip, stationBody);
    fnAddElem(leave);
  }

  static OrbitToStation(gameData: GameDatabaseFull, inShip: GameShip, withDrive: GameEquipment, station: TradeShop, fnAddElem: (step: ItineraryStep) => void): void {
    const qProps = withDrive.Properties.QDrive;
    const stationBody = station.Location.ParentBody;
    const stationOrbitalBody = gameData.BodyKeyMap[stationBody.Key];

    if (station.Location.IsHidden) {
      fnAddElem({
        stepType: ItineraryStepType.ShipFlight,
        from: stationBody,
        to: stationBody,
        title: `Travel to hidden location - ${station.Name}.` + (station.TravelNotes ? `(${station.TravelNotes})` : ""),
        durationS: 300//station.TravelDurationS //Hidden locations take a fixed 5 minutes for now
      })
    } else {
      fnAddElem({
        stepType: ItineraryStepType.QuantumJump,
        from: stationBody,
        to: stationBody,
        title: `QT To within ${station.ClosestApproachKM} KM.  ${qProps.SpoolTime} Second Spool Time and ${qProps.CooldownTime} Second Cooldown.`,
        durationS: qProps.SpoolTime + qProps.CooldownTime
      });

      //TODO: Account for atmospheric delay better.
      if (stationOrbitalBody && stationOrbitalBody.AtmosphereHeightKM > 0) {
        fnAddElem({
          stepType: ItineraryStepType.ShipFlight,
          from: stationBody,
          to: stationBody,
          title: `Fly in-atmosphere remaining ${station.ClosestApproachKM} KM to ${station.Name}`,
          durationS: station.ClosestApproachKM * 5
          //TODO: Account for ship speed
        });
      } else {
        fnAddElem({
          stepType: ItineraryStepType.ShipFlight,
          from: stationBody,
          to: stationBody,
          title: `Fly remaining ${station.ClosestApproachKM} KM to ${station.Name}`,
          durationS: station.ClosestApproachKM //most ships hit roughly 1 km/s
          //TODO: Account for ship speed
        });
      }
    }

    fnAddElem({
      stepType: ItineraryStepType.ShipFlight,
      from: stationBody,
      to: stationBody,
      title: `Land ship`,
      durationS: 60 //TODO: Allow user to configure landing time
    });

    if (station.TransitDurationS && station.TransitDurationS !== 0) {
      fnAddElem({
        stepType: ItineraryStepType.Transit,
        from: stationBody,
        to: stationBody,
        title: `Transit from spaceport to shop.` + (station.TransitNotes ? `(${station.TransitNotes})` : ""),
        durationS: station.TransitDurationS
      });
    }

    if (station.RunningDurationS && station.RunningDurationS !== 0) {
      fnAddElem({
        stepType: ItineraryStepType.Running,
        from: stationBody,
        to: stationBody,
        title: "Running to trade terminal.",
        durationS: station.RunningDurationS
      });
    }
    
  }

  static LeaveBody(gameData: GameDatabaseFull, inShip: GameShip, body: SystemLocation): ItineraryStep {
    const fullBody = gameData.SystemBodies.filter(b => b.SystemLocationID === body.ID)[0];
    if (!fullBody) debugger;

    if (fullBody.AtmosphereHeightKM && fullBody.AtmosphereHeightKM !== 0) {
      return {
        stepType: ItineraryStepType.ShipFlight,
        from: body,
        to: body,
        title: `Leave atmosphere @ ${fullBody.AtmosphereHeightKM.toFixed(0)} KM`,
        durationS: fullBody.AtmosphereHeightKM //most ships hit roughly 1 km/s
      };
    } else {
      return {
        stepType: ItineraryStepType.ShipFlight,
        from: body,
        to: body,
        title: "Get jump visibility to target.",
        durationS: 30
      };
    }
  }
}
