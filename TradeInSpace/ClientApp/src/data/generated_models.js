"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ModifierType = exports.ModifierMode = exports.ModifierActivation = exports.TradeType = exports.PadSize = exports.EquipmentType = exports.LocationType = exports.ShopType = exports.ServiceFlags = exports.BodyType = exports.Version = exports.TradeTableEntry = exports.TradeTable = exports.TradeShop = exports.TradeItem = exports.TradeEquipmentEntry = exports.SystemLocation = exports.SystemLandingZone = exports.SystemBody = exports.MissionGiverScope = exports.RewardType = exports.RequirementType = exports.MissionNoteDTO = exports.MissionRewardDTO = exports.MissionScopeRankDTO = exports.MissionRewardSetDTO = exports.MissionRequirementDTO = exports.MissionGiverEntry = exports.MissionGiver = exports.MiningElement = exports.MiningCompositionEntry = exports.MiningComposition = exports.Manufacturer = exports.HarvestableLocation = exports.SubHarvestable = exports.Harvestable = exports.GameShip = exports.GameEquipment = exports.GameDatabase = exports.GameComponent = void 0;
var GameComponent = /** @class */ (function () {
    function GameComponent() {
    }
    return GameComponent;
}());
exports.GameComponent = GameComponent;
var GameDatabase = /** @class */ (function () {
    function GameDatabase() {
    }
    return GameDatabase;
}());
exports.GameDatabase = GameDatabase;
var GameEquipment = /** @class */ (function () {
    function GameEquipment() {
    }
    return GameEquipment;
}());
exports.GameEquipment = GameEquipment;
var GameShip = /** @class */ (function () {
    function GameShip() {
    }
    return GameShip;
}());
exports.GameShip = GameShip;
var Harvestable = /** @class */ (function () {
    function Harvestable() {
    }
    return Harvestable;
}());
exports.Harvestable = Harvestable;
var SubHarvestable = /** @class */ (function () {
    function SubHarvestable() {
    }
    return SubHarvestable;
}());
exports.SubHarvestable = SubHarvestable;
var HarvestableLocation = /** @class */ (function () {
    function HarvestableLocation() {
    }
    return HarvestableLocation;
}());
exports.HarvestableLocation = HarvestableLocation;
var Manufacturer = /** @class */ (function () {
    function Manufacturer() {
    }
    return Manufacturer;
}());
exports.Manufacturer = Manufacturer;
var MiningComposition = /** @class */ (function () {
    function MiningComposition() {
    }
    return MiningComposition;
}());
exports.MiningComposition = MiningComposition;
var MiningCompositionEntry = /** @class */ (function () {
    function MiningCompositionEntry() {
    }
    return MiningCompositionEntry;
}());
exports.MiningCompositionEntry = MiningCompositionEntry;
var MiningElement = /** @class */ (function () {
    function MiningElement() {
    }
    return MiningElement;
}());
exports.MiningElement = MiningElement;
var MissionGiver = /** @class */ (function () {
    function MissionGiver() {
    }
    return MissionGiver;
}());
exports.MissionGiver = MissionGiver;
var MissionGiverEntry = /** @class */ (function () {
    function MissionGiverEntry() {
    }
    return MissionGiverEntry;
}());
exports.MissionGiverEntry = MissionGiverEntry;
var MissionRequirementDTO = /** @class */ (function () {
    function MissionRequirementDTO() {
    }
    return MissionRequirementDTO;
}());
exports.MissionRequirementDTO = MissionRequirementDTO;
var MissionRewardSetDTO = /** @class */ (function () {
    function MissionRewardSetDTO() {
    }
    return MissionRewardSetDTO;
}());
exports.MissionRewardSetDTO = MissionRewardSetDTO;
var MissionScopeRankDTO = /** @class */ (function () {
    function MissionScopeRankDTO() {
    }
    return MissionScopeRankDTO;
}());
exports.MissionScopeRankDTO = MissionScopeRankDTO;
var MissionRewardDTO = /** @class */ (function () {
    function MissionRewardDTO() {
    }
    return MissionRewardDTO;
}());
exports.MissionRewardDTO = MissionRewardDTO;
var MissionNoteDTO = /** @class */ (function () {
    function MissionNoteDTO() {
    }
    return MissionNoteDTO;
}());
exports.MissionNoteDTO = MissionNoteDTO;
var RequirementType;
(function (RequirementType) {
    RequirementType[RequirementType["Wanted"] = 0] = "Wanted";
    RequirementType[RequirementType["Virtue"] = 1] = "Virtue";
    RequirementType[RequirementType["Reliability"] = 2] = "Reliability";
    RequirementType[RequirementType["Standing"] = 3] = "Standing";
    RequirementType[RequirementType["MissionInvite"] = 4] = "MissionInvite";
    RequirementType[RequirementType["MissionLinked"] = 5] = "MissionLinked";
    RequirementType[RequirementType["MissionAll"] = 6] = "MissionAll";
    RequirementType[RequirementType["MissionAny"] = 7] = "MissionAny";
    RequirementType[RequirementType["MissionNone"] = 8] = "MissionNone";
    RequirementType[RequirementType["And"] = 9] = "And";
    RequirementType[RequirementType["Or"] = 10] = "Or";
})(RequirementType = exports.RequirementType || (exports.RequirementType = {}));
var RewardType;
(function (RewardType) {
    RewardType[RewardType["Wanted"] = 0] = "Wanted";
    RewardType[RewardType["Virtue"] = 1] = "Virtue";
    RewardType[RewardType["Reliability"] = 2] = "Reliability";
    RewardType[RewardType["Standing"] = 3] = "Standing";
})(RewardType = exports.RewardType || (exports.RewardType = {}));
var MissionGiverScope = /** @class */ (function () {
    function MissionGiverScope() {
    }
    return MissionGiverScope;
}());
exports.MissionGiverScope = MissionGiverScope;
var SystemBody = /** @class */ (function () {
    function SystemBody() {
    }
    return SystemBody;
}());
exports.SystemBody = SystemBody;
var SystemLandingZone = /** @class */ (function () {
    function SystemLandingZone() {
    }
    return SystemLandingZone;
}());
exports.SystemLandingZone = SystemLandingZone;
var SystemLocation = /** @class */ (function () {
    function SystemLocation() {
    }
    return SystemLocation;
}());
exports.SystemLocation = SystemLocation;
var TradeEquipmentEntry = /** @class */ (function () {
    function TradeEquipmentEntry() {
    }
    return TradeEquipmentEntry;
}());
exports.TradeEquipmentEntry = TradeEquipmentEntry;
var TradeItem = /** @class */ (function () {
    function TradeItem() {
    }
    return TradeItem;
}());
exports.TradeItem = TradeItem;
var TradeShop = /** @class */ (function () {
    function TradeShop() {
    }
    return TradeShop;
}());
exports.TradeShop = TradeShop;
var TradeTable = /** @class */ (function () {
    function TradeTable() {
    }
    return TradeTable;
}());
exports.TradeTable = TradeTable;
var TradeTableEntry = /** @class */ (function () {
    function TradeTableEntry() {
    }
    return TradeTableEntry;
}());
exports.TradeTableEntry = TradeTableEntry;
var Version = /** @class */ (function () {
    function Version() {
    }
    return Version;
}());
exports.Version = Version;
var BodyType;
(function (BodyType) {
    BodyType[BodyType["Planet"] = 0] = "Planet";
    BodyType[BodyType["Moon"] = 1] = "Moon";
    BodyType[BodyType["AsteroidField"] = 2] = "AsteroidField";
    BodyType[BodyType["Other"] = 3] = "Other";
    BodyType[BodyType["Star"] = 4] = "Star";
})(BodyType = exports.BodyType || (exports.BodyType = {}));
var ServiceFlags;
(function (ServiceFlags) {
    ServiceFlags[ServiceFlags["None"] = 0] = "None";
    ServiceFlags[ServiceFlags["Equipment"] = 162560] = "Equipment";
    ServiceFlags[ServiceFlags["Trade"] = 1] = "Trade";
    ServiceFlags[ServiceFlags["Refinery"] = 2] = "Refinery";
    ServiceFlags[ServiceFlags["Fine"] = 4] = "Fine";
    ServiceFlags[ServiceFlags["PackageTerminal"] = 8] = "PackageTerminal";
    ServiceFlags[ServiceFlags["MissionGiver"] = 16] = "MissionGiver";
    ServiceFlags[ServiceFlags["GreenZone"] = 32] = "GreenZone";
    ServiceFlags[ServiceFlags["ShipLanding"] = 64] = "ShipLanding";
    ServiceFlags[ServiceFlags["ShipClaim"] = 128] = "ShipClaim";
    ServiceFlags[ServiceFlags["ShipPurchase"] = 256] = "ShipPurchase";
    ServiceFlags[ServiceFlags["ShipEquipment"] = 512] = "ShipEquipment";
    ServiceFlags[ServiceFlags["ShipRental"] = 1024] = "ShipRental";
    ServiceFlags[ServiceFlags["FPSArmor"] = 2048] = "FPSArmor";
    ServiceFlags[ServiceFlags["FPSWeapon"] = 4096] = "FPSWeapon";
    ServiceFlags[ServiceFlags["FPSEquipment"] = 8192] = "FPSEquipment";
    ServiceFlags[ServiceFlags["FPSClothing"] = 16384] = "FPSClothing";
    ServiceFlags[ServiceFlags["FPSRespawnPoint"] = 32768] = "FPSRespawnPoint";
    ServiceFlags[ServiceFlags["FPSFood"] = 65536] = "FPSFood";
    ServiceFlags[ServiceFlags["FPSCrypto"] = 131072] = "FPSCrypto";
    ServiceFlags[ServiceFlags["Harvestable"] = 262144] = "Harvestable";
    ServiceFlags[ServiceFlags["Hacking"] = 524288] = "Hacking";
})(ServiceFlags = exports.ServiceFlags || (exports.ServiceFlags = {}));
var ShopType;
(function (ShopType) {
    ShopType[ShopType["Other"] = 0] = "Other";
    ShopType[ShopType["Ignore"] = 1] = "Ignore";
    ShopType[ShopType["AdminTerminal"] = 2] = "AdminTerminal";
    ShopType[ShopType["AdminRefinery"] = 3] = "AdminRefinery";
    ShopType[ShopType["FenceTerminal"] = 4] = "FenceTerminal";
    ShopType[ShopType["CommExTransfers"] = 5] = "CommExTransfers";
    ShopType[ShopType["TDD"] = 6] = "TDD";
    ShopType[ShopType["Casaba"] = 7] = "Casaba";
    ShopType[ShopType["Restaurant"] = 8] = "Restaurant";
    ShopType[ShopType["ConscientiousObjects"] = 9] = "ConscientiousObjects";
    ShopType[ShopType["AstroArmada"] = 10] = "AstroArmada";
    ShopType[ShopType["VantageRentals"] = 11] = "VantageRentals";
    ShopType[ShopType["PlatinumBay"] = 12] = "PlatinumBay";
    ShopType[ShopType["HDShowcase"] = 13] = "HDShowcase";
    ShopType[ShopType["Rentals"] = 14] = "Rentals";
    ShopType[ShopType["NewDeal"] = 15] = "NewDeal";
    ShopType[ShopType["TammanyAndSons"] = 16] = "TammanyAndSons";
    ShopType[ShopType["BulwarkArmor"] = 17] = "BulwarkArmor";
    ShopType[ShopType["DumpersDepot"] = 18] = "DumpersDepot";
    ShopType[ShopType["LiveFireWeapons"] = 19] = "LiveFireWeapons";
    ShopType[ShopType["GarrityDefense"] = 20] = "GarrityDefense";
    ShopType[ShopType["GrandBarter"] = 21] = "GrandBarter";
    ShopType[ShopType["Technotic"] = 22] = "Technotic";
    ShopType[ShopType["Teachs"] = 23] = "Teachs";
    ShopType[ShopType["Centermass"] = 24] = "Centermass";
    ShopType[ShopType["CubbyBlast"] = 25] = "CubbyBlast";
    ShopType[ShopType["Skutters"] = 26] = "Skutters";
    ShopType[ShopType["TravelerRentals"] = 27] = "TravelerRentals";
    ShopType[ShopType["CongreveWeapons"] = 28] = "CongreveWeapons";
    ShopType[ShopType["Cordrys"] = 29] = "Cordrys";
    ShopType[ShopType["KCTrending"] = 30] = "KCTrending";
    ShopType[ShopType["Aparelli"] = 31] = "Aparelli";
    ShopType[ShopType["OmegaPro"] = 32] = "OmegaPro";
    ShopType[ShopType["RegalLuxury"] = 33] = "RegalLuxury";
    ShopType[ShopType["ShubinInterstellar"] = 34] = "ShubinInterstellar";
    ShopType[ShopType["FactoryLine"] = 35] = "FactoryLine";
    ShopType[ShopType["GiftShops"] = 36] = "GiftShops";
    ShopType[ShopType["ConvenienceStore"] = 37] = "ConvenienceStore";
    ShopType[ShopType["Bar"] = 38] = "Bar";
    ShopType[ShopType["BurritoBar"] = 39] = "BurritoBar";
    ShopType[ShopType["BurritoCart"] = 40] = "BurritoCart";
    ShopType[ShopType["CafeMusain"] = 41] = "CafeMusain";
    ShopType[ShopType["CoffeToGo"] = 42] = "CoffeToGo";
    ShopType[ShopType["Ellroys"] = 43] = "Ellroys";
    ShopType[ShopType["GarciaGreens"] = 44] = "GarciaGreens";
    ShopType[ShopType["GLoc"] = 45] = "GLoc";
    ShopType[ShopType["HotdogBar"] = 46] = "HotdogBar";
    ShopType[ShopType["HotdogCart"] = 47] = "HotdogCart";
    ShopType[ShopType["JuiceBar"] = 48] = "JuiceBar";
    ShopType[ShopType["NoodleBar"] = 49] = "NoodleBar";
    ShopType[ShopType["Old38"] = 50] = "Old38";
    ShopType[ShopType["PizzaBar"] = 51] = "PizzaBar";
    ShopType[ShopType["RacingBar"] = 52] = "RacingBar";
    ShopType[ShopType["Twyns"] = 53] = "Twyns";
    ShopType[ShopType["Whammers"] = 54] = "Whammers";
    ShopType[ShopType["Wallys"] = 55] = "Wallys";
    ShopType[ShopType["MVBar"] = 56] = "MVBar";
    ShopType[ShopType["KelTo"] = 57] = "KelTo";
    ShopType[ShopType["CryAstro"] = 58] = "CryAstro";
    ShopType[ShopType["Covalex"] = 59] = "Covalex";
    ShopType[ShopType["Stanton"] = 60] = "Stanton";
    ShopType[ShopType["LandingServices"] = 61] = "LandingServices";
    ShopType[ShopType["ShipWeapons"] = 62] = "ShipWeapons";
    ShopType[ShopType["FPSArmor"] = 63] = "FPSArmor";
    ShopType[ShopType["Food"] = 64] = "Food";
    ShopType[ShopType["CargoOffice"] = 65] = "CargoOffice";
    ShopType[ShopType["CargoOfficeRentals"] = 66] = "CargoOfficeRentals";
    ShopType[ShopType["Pharmacy"] = 67] = "Pharmacy";
    ShopType[ShopType["Makau"] = 68] = "Makau";
    ShopType[ShopType["CousinCrows"] = 69] = "CousinCrows";
    ShopType[ShopType["CrusaderShowroom"] = 70] = "CrusaderShowroom";
})(ShopType = exports.ShopType || (exports.ShopType = {}));
var LocationType;
(function (LocationType) {
    LocationType[LocationType["Star"] = 0] = "Star";
    LocationType[LocationType["Planet"] = 1] = "Planet";
    LocationType[LocationType["Moon"] = 2] = "Moon";
    LocationType[LocationType["Asteroid"] = 3] = "Asteroid";
    LocationType[LocationType["AsteroidField"] = 4] = "AsteroidField";
    LocationType[LocationType["LagrangePoint"] = 5] = "LagrangePoint";
    LocationType[LocationType["LandingZone"] = 6] = "LandingZone";
    LocationType[LocationType["Outpost"] = 7] = "Outpost";
    LocationType[LocationType["Manmade"] = 8] = "Manmade";
    LocationType[LocationType["Other"] = 9] = "Other";
    LocationType[LocationType["Store"] = 10] = "Store";
    LocationType[LocationType["PointOfInterest"] = 11] = "PointOfInterest";
})(LocationType = exports.LocationType || (exports.LocationType = {}));
var EquipmentType;
(function (EquipmentType) {
    EquipmentType[EquipmentType["Ship"] = 0] = "Ship";
    EquipmentType[EquipmentType["GroundVehicle"] = 1] = "GroundVehicle";
})(EquipmentType = exports.EquipmentType || (exports.EquipmentType = {}));
var PadSize;
(function (PadSize) {
    PadSize[PadSize["Tiny"] = 1] = "Tiny";
    PadSize[PadSize["XSmall"] = 2] = "XSmall";
    PadSize[PadSize["Small"] = 3] = "Small";
    PadSize[PadSize["Medium"] = 4] = "Medium";
    PadSize[PadSize["Large"] = 5] = "Large";
    PadSize[PadSize["XLarge"] = 6] = "XLarge";
})(PadSize = exports.PadSize || (exports.PadSize = {}));
var TradeType;
(function (TradeType) {
    TradeType[TradeType["Buy"] = 0] = "Buy";
    TradeType[TradeType["Sell"] = 1] = "Sell";
})(TradeType = exports.TradeType || (exports.TradeType = {}));
var ModifierActivation;
(function (ModifierActivation) {
    ModifierActivation[ModifierActivation["Passive"] = 0] = "Passive";
    ModifierActivation[ModifierActivation["Active"] = 1] = "Active";
})(ModifierActivation = exports.ModifierActivation || (exports.ModifierActivation = {}));
var ModifierMode;
(function (ModifierMode) {
    ModifierMode[ModifierMode["AdditivePre"] = 0] = "AdditivePre";
    ModifierMode[ModifierMode["AdditivePost"] = 1] = "AdditivePost";
    ModifierMode[ModifierMode["MultiplicitivePre"] = 2] = "MultiplicitivePre";
    ModifierMode[ModifierMode["MultiplicitivePost"] = 3] = "MultiplicitivePost";
})(ModifierMode = exports.ModifierMode || (exports.ModifierMode = {}));
var ModifierType;
(function (ModifierType) {
    ModifierType[ModifierType["Heat"] = 0] = "Heat";
    ModifierType[ModifierType["Signature"] = 1] = "Signature";
    ModifierType[ModifierType["WeaponBeamStrengthVFX"] = 2] = "WeaponBeamStrengthVFX";
    ModifierType[ModifierType["MiningWindowLevel"] = 3] = "MiningWindowLevel";
    ModifierType[ModifierType["MiningResistance"] = 4] = "MiningResistance";
    ModifierType[ModifierType["MiningInstability"] = 5] = "MiningInstability";
    ModifierType[ModifierType["MiningOptimalWindowSize"] = 6] = "MiningOptimalWindowSize";
    ModifierType[ModifierType["MiningShatterDamage"] = 7] = "MiningShatterDamage";
    ModifierType[ModifierType["MiningOptimalWindowRate"] = 8] = "MiningOptimalWindowRate";
    ModifierType[ModifierType["MiningCatastrophicWindowRate"] = 9] = "MiningCatastrophicWindowRate";
    ModifierType[ModifierType["WeaponProjectileSpeed"] = 10] = "WeaponProjectileSpeed";
    ModifierType[ModifierType["WeaponFireRate"] = 11] = "WeaponFireRate";
    ModifierType[ModifierType["WeaponSpreadDecy"] = 12] = "WeaponSpreadDecy";
    ModifierType[ModifierType["WeaponAmmoCost"] = 13] = "WeaponAmmoCost";
    ModifierType[ModifierType["WeaponHeatGeneration"] = 14] = "WeaponHeatGeneration";
    ModifierType[ModifierType["WeaponSoundRadius"] = 15] = "WeaponSoundRadius";
    ModifierType[ModifierType["WeaponRecoilDecay"] = 16] = "WeaponRecoilDecay";
    ModifierType[ModifierType["WeaponSpreadMin"] = 17] = "WeaponSpreadMin";
    ModifierType[ModifierType["WeaponSpreadMax"] = 18] = "WeaponSpreadMax";
    ModifierType[ModifierType["WeaponSpreadFirst"] = 19] = "WeaponSpreadFirst";
    ModifierType[ModifierType["WeaponSpreadAttack"] = 20] = "WeaponSpreadAttack";
    ModifierType[ModifierType["WeaponZoom"] = 21] = "WeaponZoom";
    ModifierType[ModifierType["WeaponDamage"] = 22] = "WeaponDamage";
    ModifierType[ModifierType["MiningFilter"] = 23] = "MiningFilter";
})(ModifierType = exports.ModifierType || (exports.ModifierType = {}));
//---End---
//# sourceMappingURL=generated_models.js.map