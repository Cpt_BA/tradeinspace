import { Component, ViewChild, OnInit, ChangeDetectorRef, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DataCacheService } from '../../../data/data_cache';
import { SystemLocation, HarvestableLocation, TradeItem } from '../../../data/generated_models';
import { TreeNode, MessageService } from 'primeng/api';
import { TISBaseComponent } from '../../tis_base.component';
import { ModelUtils } from '../../../utilities/model_utils';
import { FullJumpDetailPoint, FullJumpDetails } from '../../../data/client_models';


@Component({
  selector: 'qd-calculator',
  templateUrl: './qd-calculator.component.html',
  styleUrls: ['./qd-calculator.component.css']
})
export class QDCalculatorComponent extends TISBaseComponent implements OnInit {

  public qd_data: any = {
    labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
    datasets: [
    ]
  };

  public qd_options: any = {
    scales: {
      xAxes: [
        {
          id: "x-axis",
          type: "linear",
          ticks: {
            beginAtZero: true
          },
          time: {
            unit: "second",
            displayFormats: {
              second: 'SS'
            }
          },
          scaleLabel: {
            display: true,
            labelString: "Time (Seconds)"
          }
        }
      ],
      yAxes: [
        {
          id: 'velocity', type: 'linear', position: 'left',
          ticks: {
            callback: (label, index, labels) => {
              return `${Number(label).toLocaleString()} km/s`;
            }
          },
          scaleLabel: {
            display: true,
            labelString: "Velocity"
          }
        },
        {
          id: 'distance', type: 'linear', position: 'right',
          ticks: {
            callback: (label, index, labels) => {
              return `${Number(label).toLocaleString()} km`;
            }
          },
          scaleLabel: {
            display: true,
            labelString: "Distance"
          }
        },
      ],
    },
  };

  constructor(httpRoute: ActivatedRoute,
    dataStore: DataCacheService,
    cdr: ChangeDetectorRef,
    toast: MessageService) {
    super(httpRoute, dataStore, cdr, toast);
  }

  onAllDataGameUpdated(): void {
    this.refreshData();
  }

  onAllDataUserUpdated(): void {
  }

  selectData(event: Event): void {
  }

  refreshData(): void {
    var from = this.gameData.BodyKeyMap["arc_l5"];
    var to = this.gameData.BodyKeyMap["hur_l3"];
    var drive = this.gameData.EquipmentKeyMap["qdrv_arcc_s01_rush_scitem"];

    var jumpDetails = ModelUtils.calculateFullQuantumJump(from, to, drive);

    function buildData(fnProperty: (item: FullJumpDetailPoint) => number): any[] {
      return [
        { x: jumpDetails.start.time_elapsed, y: fnProperty(jumpDetails.start) },
        { x: jumpDetails.accel_finished.time_elapsed, y: fnProperty(jumpDetails.accel_finished) },
        { x: jumpDetails.decel_started.time_elapsed, y: fnProperty(jumpDetails.decel_started) },
        { x: jumpDetails.end.time_elapsed, y: fnProperty(jumpDetails.end) }
      ]
    }

    this.qd_data = {
      datasets: [
        {
          label: "Velocity",
          yAxisID: 'velocity',
          lineTension: 0,
          data: buildData((p) => p.velocity)
        },
        {
          label: "Distance Remaining",
          yAxisID: 'distance',
          lineTension: 0,
          data: buildData((p) => p.distance_remaining)
        },
        {
          label: "Distance Traveled",
          yAxisID: 'distance',
          lineTension: 0,
          data: buildData((p) => p.distance_traveled)
        }
      ]
    }

    this.qd_options.scales.xAxes[0].max = jumpDetails.duration;
    //Velocity
    this.qd_options.scales.yAxes[0].max = jumpDetails.max_velocity;
    //Distance
    this.qd_options.scales.yAxes[1].max = jumpDetails.end.distance_traveled;

    console.log(this.qd_data);
  }
}
