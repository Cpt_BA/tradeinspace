import { Component, ViewChild, OnInit, ChangeDetectorRef, ViewEncapsulation, ElementRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DataCacheService } from '../../data/data_cache';
import { GameDatabase } from '../../data/generated_models';
import { BasicUserSettings } from '../../data/models';
import { GameDatabaseFull } from '../../data/game_data';
import { Loadout, LoadoutSlot } from '../../data/loadout';
import { TISBaseComponent } from '../tis_base.component';
import { MessageService } from 'primeng/api';
import { LoadoutManagerService } from '../../data/loadout_manager';
import { Subscription } from 'rxjs';


@Component({
  selector: 'loadout',
  templateUrl: './loadout.component.html',
  styleUrls: ['./loadout.component.css']
})
export class LoadoutComponent extends TISBaseComponent implements OnInit {

  public shipLoadout: Loadout;

  private loadoutChange: Subscription;

  @ViewChild('loadoutTop', { static: true }) loadoutTop: ElementRef;

  constructor(
    private loadoutMgr: LoadoutManagerService,
    private router: Router,
    http_route: ActivatedRoute,
    dataStore: DataCacheService,
    cdr: ChangeDetectorRef,
    toast: MessageService)
  {
    super(http_route, dataStore, cdr, toast);
  }

  private calculateLoadout(): void {
    if (this.urlParams.ship_key) {
      //if (this.shipLoadout && this.shipLoadout.equipment.Key === this.urlParams.ship_key)
      //  return;

      this.shipLoadout = this.loadoutMgr.getDefaultLoadout(this.urlParams.ship_key).Duplicate();
    } else if (this.urlParams.loadout_key) {
      const loadoutName = decodeURIComponent(this.urlParams.loadout_key)
      const customLoadout = this.loadoutMgr.getLoadoutWithName(loadoutName);
      if (customLoadout) {
        this.setLoadout(customLoadout);
      } else {
        this.toast.add({ summary: "Unable to find that loadout. Switching to active ship", severity: "error" });
        this.router.navigate(['/', 'loadout']);
        //Error message
      }
    } else if (this.loadoutMgr.activeLoadout.value) {
      //if (this.shipLoadout && this.shipLoadout.key === this.loadoutMgr.activeLoadout.value.key)
      //  return;

      this.setLoadout(this.loadoutMgr.activeLoadout.value.Duplicate());
    } else {
      this.failedLoad();
    }
  }

  private setLoadout(loadout: Loadout): void {
    this.shipLoadout = loadout;
    
  }

  ngAfterViewInit(): void {
    this.loadoutTop.nativeElement.scrollIntoView();
  }

  private failedLoad(): void {
    this.shipLoadout = null;
  }

  onAllDataUserUpdated(): void {
    if (!this.urlParams) return;

    //this.calculateLoadout();
  }

  onUrlParamsLoaded(): void {
    this.calculateLoadout();

    if (!this.loadoutChange) {
      this.loadoutChange = this.loadoutMgr.activeLoadout.subscribe(al => {
        if (al) this.calculateLoadout();
      });
    }
  }

  ngOnDestroy(): void {
    super.ngOnDestroy();
    if (this.loadoutChange) this.loadoutChange.unsubscribe();
  }

  public equipmentChanged(slot: LoadoutSlot): void {
    if (this.urlParams.loadout_key) {
      this.loadoutMgr.addCustomLoadout(this.shipLoadout, true);
    } else if (this.loadoutMgr.activeLoadout.value.key.startsWith("custom_")) {
      //TODO:Better way to mark custom loadouts vs default.
      this.loadoutMgr.addCustomLoadout(this.shipLoadout, true);
    }
  }
  
}
