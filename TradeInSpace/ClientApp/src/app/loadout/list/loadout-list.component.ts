import { Component, ViewChild, OnInit, ChangeDetectorRef, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TISBaseComponent } from '../../tis_base.component';
import { Loadout } from '../../../data/loadout';
import { LoadoutManagerService } from '../../../data/loadout_manager';
import { MessageService, ConfirmationService } from 'primeng/api';
import { DataCacheService } from '../../../data/data_cache';
import { DisplayShip } from '../../../data/display_ship';


@Component({
  selector: 'loadout-list',
  templateUrl: './loadout-list.component.html',
  styleUrls: ['./loadout-list.component.css']
})
export class LoadoutListComponent extends TISBaseComponent implements OnInit {

  public availableLoadouts: DisplayShip[];

  constructor(
    private loadoutMgr: LoadoutManagerService,
    private confirmSvc: ConfirmationService,
    private router: Router,
    http_route: ActivatedRoute,
    dataStore: DataCacheService,
    cdr: ChangeDetectorRef,
    toast: MessageService)
  {
    super(http_route, dataStore, cdr, toast);
  }

  onAllDataReady(): void {
    this.availableLoadouts = this.loadoutMgr.getCustomLoadouts().map(l => new DisplayShip(l, this.gameData.ShipKeyMap[l.equipment.Key]));
  }

  public deleteLoadout(ship: DisplayShip): void {
    this.confirmSvc.confirm({
      message: `Delete Loadout: ${ship.Name}. Are you sure? Like REEEEEEEALLY sure?`,
      accept: () => {
        console.log("Deleting loadout", ship);
        this.loadoutMgr.deleteCustomLoadout(ship.Key);
        this.onAllDataReady();
      }
    });
  }
}
