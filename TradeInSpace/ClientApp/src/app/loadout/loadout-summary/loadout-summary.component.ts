import { Component, OnInit, ChangeDetectorRef, Input, OnChanges, SimpleChanges, ViewChild, ElementRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LocationType, ServiceFlags, GameEquipment } from '../../../data/generated_models';
import { DataCacheService } from '../../../data/data_cache';
import { Loadout, LoadoutSlot, LoadoutEntryMirrorMode, NameGroupEntry } from '../../../data/loadout';
import { groupSet } from '../../../utilities/utilities';
import { environment } from '../../../environments/environment';
import { loadoutSlotTypeName } from '../../../pipes/display_text_pipe';
import { LoadoutManagerService } from '../../../data/loadout_manager';
import { MessageService } from 'primeng/api';


@Component({
  selector: 'loadout-summary',
  templateUrl: './loadout-summary.component.html',
  styleUrls: ['./loadout-summary.component.css']
})
export class LoadoutSummaryComponent implements OnInit, OnChanges {
  //Hack to make LocationType accesible to the html
  LocationType: typeof LocationType = LocationType;
  ServiceFlags: typeof ServiceFlags = ServiceFlags;
  Math: typeof Math = Math;

  public displayExport = false;
  public displaySave = false;
  public exportText: string;
  public saveAsName: string;

  @Input() public selected_loadout: Loadout;

  @ViewChild("exportTxt", { static: false }) exportTxtBox: ElementRef;

  constructor(private loadoutMgr: LoadoutManagerService,
    private router: Router,
    private http_route: ActivatedRoute,
    private data_store: DataCacheService,
    private cdr: ChangeDetectorRef,
    private toast: MessageService) {
  }

  ngOnInit(): void {
  }

  ngAfterViewInit(): void {
    this.data_store.gameData.subscribe(gameData => {
      if (gameData === null) return;

    });
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.selected_loadout && changes.selected_loadout.currentValue) {
      const newLoadout = changes.selected_loadout.currentValue as Loadout;

      
    }
  }

  public saveToHangar(): void {
    const savedLoadout = Loadout.fromSaved(this.loadoutMgr.gameData, this.selected_loadout.toSave());
    savedLoadout.name = this.saveAsName;

    this.loadoutMgr.addCustomLoadout(savedLoadout, true);
    this.router.navigate(['/', 'loadout', 'my', savedLoadout.name]);
  }

  public showSaveWindow(): void {
    if (!this.selected_loadout) return;

    this.saveAsName = this.selected_loadout.name;
    this.displaySave = true;
  }

  public showExportWindow(): void {
    if (!this.selected_loadout) return;

    this.exportText = this.selected_loadout.toExport(this.loadoutMgr, false);
    this.displayExport = true;

    this.cdr.detectChanges();
  }

  public copyText(): void {
    this.exportTxtBox.nativeElement.focus();
    this.exportTxtBox.nativeElement.select();
    document.execCommand("copy");
    this.exportTxtBox.nativeElement.setSelectionRange(0, 0);

    this.toast.add({ summary: "Copied to clipboard!", severity: "success" })
  }

  public doImport(): void {
    try {
      const imported = Loadout.fromImport(this.loadoutMgr.gameData, this.exportText);
      if (!imported || !imported.success) throw new Error(imported.error);

      console.log("Valid Loadout Imported: ", imported);
      this.loadoutMgr.addCustomLoadout(imported.loadout, true);
      //Do this little hack to support re-importing under the exact same name
      this.router
        .navigateByUrl("/", { skipLocationChange: true })
        .then(() => {
          this.router.navigate(['/', 'loadout', 'my', imported.loadout.name]);
        });
    this.displayExport = false;
    } catch(ex){
      this.toast.add({
        summary: ex.message,
        severity: "error"
      });
    }
  }
}
