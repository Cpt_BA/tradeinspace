import { Component, OnInit, ChangeDetectorRef, Input, OnChanges, SimpleChanges, EventEmitter, Output } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { LocationType, ServiceFlags, GameEquipment } from '../../../data/generated_models';
import { DataCacheService } from '../../../data/data_cache';
import { Loadout, LoadoutSlot, LoadoutEntryMirrorMode, NameGroupEntry } from '../../../data/loadout';
import { groupSet } from '../../../utilities/utilities';
import { environment } from '../../../environments/environment';
import { loadoutSlotTypeName } from '../../../pipes/display_text_pipe';
import { SlotDisplay } from '../../../data/client_models';


@Component({
  selector: 'loadout-editor',
  templateUrl: './loadout-editor.component.html',
  styleUrls: ['./loadout-editor.component.css']
})
export class LoadoutEditorComponent implements OnInit, OnChanges {
  //Hack to make LocationType accesible to the html
  LocationType: typeof LocationType = LocationType;
  ServiceFlags: typeof ServiceFlags = ServiceFlags;

  @Input() selected_loadout: Loadout;
  @Output() equipmentChanged: EventEmitter<LoadoutSlot> = new EventEmitter<LoadoutSlot>();

  display_slots: LoadoutGroupingDisplayModel[] = [];

  leftRightGroup: boolean = false;
  groupChildren: boolean = false;

  constructor(private http_route: ActivatedRoute,
    private data_store: DataCacheService,
    private cdr: ChangeDetectorRef) {
  }

  ngOnInit(): void {
  }

  ngAfterViewInit(): void {
    this.data_store.gameData.subscribe(gameData => {
      if (gameData == null) return;

    });
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.selected_loadout && changes.selected_loadout.currentValue) {
      var newLoadout = changes.selected_loadout.currentValue as Loadout;

      this.display_slots = LoadoutGroupingDisplayModel.FromLoadout(newLoadout).children;
    }
  }
}

export class LoadoutGroupingDisplayModel {

  //Parameters for grouping/dispaly
  display_name: string;
  mirror: LoadoutEntryMirrorMode;
  quantity: number = 1;
  types: string[];
  nameGroup: NameGroupEntry;
  display_params: SlotDisplay;

  public slot: LoadoutSlot;
  children: LoadoutGroupingDisplayModel[] = [];

  private constructor(
    public loadout: Loadout,
    public represents: LoadoutSlot[],
    public parent: LoadoutGroupingDisplayModel = null) {

    this.slot = this.represents[0];
    this.quantity = this.represents.length;

    //Convert names for diisplay
    this.types = this.slot.types ? this.slot.types.map(type => loadoutSlotTypeName(type)).sort() : [];

    this.display_name = this.slot.name_group.group_name;

    this.nameGroup = this.slot.name_group;
    this.mirror = this.slot.name_group.mode;

    this.display_params = this.slot.display_params;

    this.invalidateChildren();

    console.log(this);
  }

  private static ignoreChildrenTypes: string[] = []//"WeaponGun", "Missile", "Radar"];

  private invalidateChildren(): void {
    if (this.slot.attached_equipment &&
      LoadoutGroupingDisplayModel.ignoreChildrenTypes.includes(this.slot.attached_equipment.AttachmentType)) {
      this.children = [];
      return;
    }

    const displayChildren = this.slot.children;
    //This is the primary "grouper" for slots.
    //Using name over key because ships that have special left / right variants of equipment that wouldn't match
    const groupedChildren = groupSet(displayChildren,
      s => `${s.name_group.group_name}_${s.attached_equipment && s.attached_equipment.Name}`);

    const groups = Object.keys(groupedChildren);

    const raw_children = groups.map(group_name => {
      const slots: LoadoutSlot[] = groupedChildren[group_name];

      const newSlot = new LoadoutGroupingDisplayModel(this.loadout, slots, this);
      if (!newSlot.display_params) {
        //if (!environment.production) console.warn(`No display params for slot group '${slots[0].types}'`, slots);
        return null; //Skip it if we didn't find it
      }

      return newSlot;
    }).filter(g => !(!g));

    //Sort by the order propery
    this.children = raw_children.sort((a, b) => {
      return a.display_params.order - b.display_params.order;
    });
  }

  public static FromLoadout(loadout: Loadout): LoadoutGroupingDisplayModel {
    return new LoadoutGroupingDisplayModel(loadout, [loadout.root_slot]);
  }

  private duplicateChild(childSlot: LoadoutGroupingDisplayModel): void {
    this.represents.forEach(c => {
      if (c === childSlot.parent.slot) return;

      childSlot.represents.forEach(copySlot => {
        c.children.forEach(targetSlot => {
          if (copySlot.name_group.original === targetSlot.name_group.original) {
            targetSlot.attachComponent(copySlot.attached_equipment);
          }
        })
      })
    })
  }

  public applyEquipment(new_equipment: GameEquipment, invalidate = true): void {
    this.represents.forEach(slot => slot.attachComponent(new_equipment));
    if (this.parent) {
      this.parent.duplicateChild(this);
    }
    if (invalidate) {
      this.invalidateChildren();
      this.loadout.recalculate();
    }
  }

}




export interface LoadoutGroup {
  title: string;
  types: string[];
  slots?: LoadoutGroupingDisplayModel[];
}

