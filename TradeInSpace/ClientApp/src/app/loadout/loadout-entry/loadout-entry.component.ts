import { Component, OnInit, ChangeDetectorRef, Input, OnChanges, SimpleChanges, EventEmitter, Output } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { GameEquipment } from '../../../data/generated_models';
import { DataCacheService } from '../../../data/data_cache';
import { LoadoutEntryMirrorMode, LoadoutSlot } from '../../../data/loadout';
import { LoadoutGroupingDisplayModel } from '../loadout-editor/loadout-editor.component';
import { cleanCamelCase } from '../../../utilities/utilities';
import { ShowTooltipEvent } from '../../../directive/tooltip-attr.directive';
import { EquipmentListExpandedParams } from '../../components/equipment-selector/equipment-selector.component';
import { ShoppingCartManagerService } from '../../../data/shopping_cart_manager';


@Component({
  selector: 'loadout-entry',
  templateUrl: './loadout-entry.component.html',
  styleUrls: ['./loadout-entry.component.css']
})
export class LoadoutEntryComponent implements OnInit, OnChanges {
  Mirror: typeof LoadoutEntryMirrorMode = LoadoutEntryMirrorMode;
  JSON: typeof JSON = JSON;

  @Input() entry: LoadoutGroupingDisplayModel;
  @Input() depth: number = 0;
  @Output() equipmentChanged: EventEmitter<LoadoutSlot> = new EventEmitter<LoadoutSlot>();

  public equipment_options: GameEquipment[] = [];
  public slot_locked: boolean;

  constructor(private http_route: ActivatedRoute,
    private dataCache: DataCacheService,
    private cdr: ChangeDetectorRef,
    private cart: ShoppingCartManagerService) {
  }


  ngOnInit(): void {
  }

  ngAfterViewInit(): void {
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.entry && changes.entry.currentValue) {
      const display: LoadoutGroupingDisplayModel = changes.entry.currentValue;
      this.slot_locked = !display.slot.editable || display.display_params.locked
      if (display.slot) {
        this.equipment_options = [display.slot.attached_equipment];
      }
    }
  }

  public populateEquipmentList(event: EquipmentListExpandedParams): void {
    const gameDB = this.dataCache.gameDataCConfig.rawSubject.value;
    const slot = this.entry.slot;

    event.equipment = gameDB.Equipment.filter(eq => {
      let attachType = eq.AttachmentType;
      if (eq.AttachmentSubtype) attachType += `/${eq.AttachmentSubtype}`;

      const sizeMatch = eq.Size >= slot.min_size && eq.Size <= slot.max_size
      const typeMatch = slot.canFitType(attachType);

      return sizeMatch && typeMatch;
    });
  }

  public changeEquipment(equipment: GameEquipment): void {
    this.entry.applyEquipment(equipment);
    this.equipmentChanged.emit(this.entry.slot);
  }

  public purchase(): void {
    this.cart.AddCheapestTrade(this.entry.slot.attached_equipment);
  }

  getCollapsedIndicators(event: ShowTooltipEvent): void{
    event.content = this.entry.represents
      .sort((a, b) => a.name_group.indicator.localeCompare(b.name_group.indicator))
      .map(s => {
        return cleanCamelCase(s.name_group.indicator);
      }).join("<br/>");
  }
}

interface DisplayEntry {
  title: string;
  equipment: GameEquipment;
}
