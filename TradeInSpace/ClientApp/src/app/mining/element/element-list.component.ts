import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { DataCacheService } from '../../../data/data_cache';
import { GameDatabaseFull } from '../../../data/game_data';
import { GameEquipment, MiningElement, MiningComposition, MiningCompositionEntry, TradeType, ShopType, TradeTableEntry, HarvestableLocation, SystemLocation, LocationType } from '../../../data/generated_models';
import { ClientData } from '../../../data/client_data';
import { BasicUserSettings } from '../../../data/models';
import { TISBaseComponent } from '../../tis_base.component';
import { MessageService, MenuItem } from 'primeng/api';
import { ShowTooltipEvent } from '../../../directive/tooltip-attr.directive';
import { fmtMoney, fmtPercent } from '../../../pipes/scaleunit_pipe';
import { groupBy } from 'rxjs/operators';
import { groupSet } from '../../../utilities/utilities';


@Component({
  selector: 'element-list',
  templateUrl: './element-list.component.html',
  styles: [
`
.mini-bar {
  border-radius: 3px;
  overflow: hidden;
  width: 100%;
  height: 4px;
  background-color: black;
  margin-top: 2px;
}

.mini-bar .mini-bar-value {
  display: block;
  padding: 2px;
  width: 50%;
}

.col-right {
  float: right;
  margin-bottom: 2px;
}

.col-center {
  text-align: center;
}

.inline-title {
  float: left;
}

.hover-show {
  display: none;
}

td:hover .hover-show {
  display: contents;
}

@media (max-width: 1600px)
{
  .small-rotate {
    transform: rotate(-90deg);
	  display: inline-block;
	  transform-origin: 8px;
  }
}
`
  ]
})
export class ElementListComponent extends TISBaseComponent implements OnInit {
  public LocationType: typeof LocationType = LocationType;

  elements: ElementDetails[];
  asteroidCompositions: CompositionDetails[] = []; 
  depositCompositions: CompositionDetails[] = [];

  trades: TradeTableEntry[] = [];
  refined_trades: TradeTableEntry[] = [];
  locations: SystemLocation[] = [];

  displayPrices = false;
  displayLocations = false;

  displayElement: MiningElement;
  displayComposition: MiningComposition;

  ignore = ["Hadanite", "Aphorite", "Dolivine"];

  constructor(httpRoute: ActivatedRoute,
    dataStore: DataCacheService,
    cdr: ChangeDetectorRef,
    toast: MessageService) {
    super(httpRoute, dataStore, cdr, toast);
  }

  onAllDataTradeUpdated(): void {
    this.elements = this.gameData.MiningElements
      .filter(me => !this.ignore.includes(me.TradeItem.Name))
      .map(me => this.elementToItem(me));
    const compositionSet = new Set<number>()
    this.elements.forEach(e => {
      this.gameData.MiningCompositionEntries
        .filter(mce => !mce.Composition.Name.startsWith("Test")
          && !mce.Composition.Name.startsWith("FPS")
          && mce.MiningElementID === e.element.ID)
        .forEach(mce => {
        compositionSet.add(mce.CompositionID);
        e.availableLookup[mce.CompositionID] = mce;
      });
    })

    this.asteroidCompositions = [];
    this.depositCompositions = [];

    Array.from(compositionSet)
      .map(mcid => this.compositionToItem(this.gameData.MiningCompositionMap[mcid]))
      .forEach(c => {
        if (c.composition.Name.startsWith("Asteroid"))
          this.asteroidCompositions.push(c)
        else if (c.composition.Name.endsWith("Deposit"))
          this.depositCompositions.push(c);
      });
  }

  elementToItem(element: MiningElement): ElementDetails {
    const elementTrades = this.tradeData.Entries.filter(e =>
      e.ItemID === element.TradeItem.ID &&
      e.Type === TradeType.Sell);
    const refinedTrades = this.tradeData.Entries.filter(e =>
      e.ItemID === element.RefinedTradeItemID &&
      e.Type === TradeType.Sell);

    let minPrice = null, maxPrice = null;
    let minRefinedPrice = null, maxRefinedPrice = null;

    elementTrades.forEach(et => {
      if (et.MinUnitPrice < minPrice || !minPrice) minPrice = et.MinUnitPrice;
      if (et.MaxUnitPrice > maxPrice || !maxPrice) maxPrice = et.MaxUnitPrice;
    })
    refinedTrades.forEach(et => {
      if (et.MinUnitPrice < minRefinedPrice || !minRefinedPrice) minRefinedPrice = et.MinUnitPrice;
      if (et.MaxUnitPrice > maxRefinedPrice || !maxRefinedPrice) maxRefinedPrice = et.MaxUnitPrice;
    })

    return {
      minValue: minPrice,
      maxValue: maxPrice,
      minRefinedValue: minRefinedPrice,
      maxRefinedValue: maxRefinedPrice,
      element: element,
      availableLookup: {}
    }
  }

  compositionToItem(composition: MiningComposition): CompositionDetails {
    return {
      title: composition.Name.replace("Deposit", "").replace("Asteroid_", "").trim(),
      composition: composition
    }
  }

  onCompositionEntryTooltip(compEntry: MiningCompositionEntry, event: ShowTooltipEvent): void {

  }

  onCompositionTooltip(compDetails: CompositionDetails, event: ShowTooltipEvent): void {
    const composition = compDetails.composition;

    const lines = [
      `Will spawn at least <span style="font-weight: bold">${composition.MinimumDistinct}</span>`,
      ""
    ]

    composition.Entries.forEach(ce => {
      lines.push(`[${fmtPercent(ce.Probability)}] ${ce.Element.TradeItem.Name} @ ${fmtPercent(ce.MinPercentage)} - ${fmtPercent(ce.MaxPercentage)}`)
    });

    lines.push("");
    lines.push("Can be found at these locations:");

    const compositionLocations = this.gameData.HarvestableLocations
      .filter(hl => hl.Harvestable.MiningCompositionID === composition.ID)
      .map(hl => hl.ParentLocation)
      .filter((v, i, a) => a.indexOf(v)===i); //Distinct locations

    compositionLocations.forEach(cl => {
      lines.push(`${cl.Name} - ${cl.LocationType}`)
    });

    event.content = lines.join("<br/>");
  }

  onMineralTooltip(element: ElementDetails, event: ShowTooltipEvent): void {
    const lines = [
      `Mining Resistance: ${fmtPercent(element.element.Resistance, true)}`,
      `Mining Instability: ${fmtPercent(element.element.Instability, true)}`,
      "",
      `Explosion Multiplier: ${element.element.ExplosionMultiplier}x`,
      `Cluster Factor: ${fmtPercent(element.element.ClusterFactor, true)}`,
      "",
      `Window Midpoint: ${fmtPercent(element.element.WindowMidpoint, true)}`,
      `Midpoint Randomness: ${fmtPercent(element.element.WindowMidpointRandomness, true)}`,
      `Window "Thinness": ${fmtPercent(element.element.WindowMidpointThinness, true)}`,
    ]

    event.content = lines.join("<br/>");
  }

  showTradesWindow(element: ElementDetails): void {
    this.trades = this.tradeData.Entries.filter(te => te.ItemID === element.element.TradeItemID && te.Type === TradeType.Sell);
    this.refined_trades = this.tradeData.Entries.filter(te => te.ItemID === element.element.RefinedTradeItemID && te.Type === TradeType.Sell);
    this.displayPrices = true;
    this.displayElement = element.element;
  }

  showLocationsWindow(composition: CompositionDetails): void {
    this.locations = this.gameData.HarvestableLocations
      .filter(hl => hl.Harvestable.MiningCompositionID === composition.composition.ID)
      .map(hl => hl.ParentLocation)
      .filter((v, i, a) => a.indexOf(v) === i); //Distinct locations
    this.displayLocations = true;
    this.displayComposition = composition.composition;
  }
}

interface CompositionDetails {
  title: string;
  composition: MiningComposition;
}

interface ElementDetails {
  element: MiningElement;
  minValue?: number;
  maxValue?: number;
  minRefinedValue?: number;
  maxRefinedValue?: number;
  availableLookup: { [locationID: number]: MiningCompositionEntry };
}
