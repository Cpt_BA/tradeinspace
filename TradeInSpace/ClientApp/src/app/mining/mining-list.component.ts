import { Component, ViewChild, OnInit, ChangeDetectorRef, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DataCacheService } from '../../data/data_cache';
import { TradeTable, GameDatabase, SystemLocation, LocationType, ServiceFlags, PadSize, HarvestableLocation, MiningCompositionEntry, TradeItem } from '../../data/generated_models';
import { TreeNode, MessageService } from 'primeng/api';
import { TISBaseComponent } from '../tis_base.component';


@Component({
  selector: 'mining-list',
  templateUrl: './mining-list.component.html',
  styleUrls: ['./mining-list.component.css']
})
export class MiningListComponent extends TISBaseComponent implements OnInit {
  public console: Console = window.console;

  locations: TreeNode[];
  locationLookup: { [key: string]: TreeNode } = {};

  constructor(httpRoute: ActivatedRoute,
    dataStore: DataCacheService,
    cdr: ChangeDetectorRef,
    toast: MessageService) {
    super(httpRoute, dataStore, cdr, toast);
  }

  onAllDataGameUpdated(): void {
    this.refreshLocations();
  }

  onAllDataUserUpdated(): void {
  }

  locationToTree(location: SystemLocation): TreeNode {
    const children = location.Harvestables.map(h => this.harvestableToTree(h));
    children.push(...location.ChildLocations.map(c => this.locationToTree(c)));

    return {
      label: location.Name,
      key: location.Key,
      data: location,
      children: children
    };
  }

  harvestableToTree(location: HarvestableLocation): TreeNode {
    const children = [];
    if (location.Harvestable.Element)
      children.push(location.Harvestable.Element.TradeItem);
    if (location.Harvestable.Composition)
      children.push(...location.Harvestable.Composition.Entries.map(e => e.Element.TradeItem))

    return {
      label: location.Harvestable.Key,
      key: location.Key,
      data: location,
      children: children.map(c => this.compositionToTree(c))
    };
  }

  compositionToTree(item: TradeItem): TreeNode {
    return {
      label: item.Name,
      key: item.Key,
      data: item
    };
  }

  refreshLocations(): void {
    this.locations = this.gameData.SystemLocations
      .filter(sl => !sl.ParentLocation)
      .map(rl => {
        return this.locationToTree(rl);
      });
    console.log("Mining: ", this.gameData.HarvestableLocations, this.locations);
  }
}
