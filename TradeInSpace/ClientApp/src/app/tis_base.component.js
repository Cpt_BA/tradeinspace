"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.TISBaseComponent = void 0;
var client_data_1 = require("../data/client_data");
var ui_utils_1 = require("../utilities/ui_utils");
var client_models_1 = require("../data/client_models");
var TISBaseComponent = /** @class */ (function () {
    function TISBaseComponent(http_route, dataStore, cdr, toast, name) {
        if (name === void 0) { name = "..."; }
        this.http_route = http_route;
        this.dataStore = dataStore;
        this.cdr = cdr;
        this.toast = toast;
        this.name = name;
        this.ClientData = client_data_1.ClientData;
        this.UiUtils = ui_utils_1.UiUtils;
        this.DamageMode = client_models_1.DamageMode;
        //console.log(`TIS Component<${name}> - Start`);
    }
    //Probably a bit overkill, but explicit logic is nice and easy to debug :^)
    TISBaseComponent.prototype.ngOnInit = function () {
        //console.log(`TIS Component<${this.name}> - OnInit`)
        var _this = this;
        this.gameDataSub = this.dataStore.gameData.subscribe(function (gameData) {
            //console.log(`TIS Component<${this.name}> - GameData Loaded`, gameData);
            if (!gameData)
                return;
            var firstGameData = !_this.gameData;
            _this.gameData = gameData;
            _this.handleFirstData(firstGameData, false, false);
            _this.handleDataUpdate(true, false, false);
        });
        this.tradeDataSub = this.dataStore.tradeTable.subscribe(function (tradeData) {
            //console.log(`TIS Component<${this.name}> - TradeData Loaded`, tradeData);
            if (!tradeData)
                return;
            var firstTradeData = !_this.tradeData;
            _this.tradeData = tradeData;
            _this.handleFirstData(false, firstTradeData, false);
            _this.handleDataUpdate(false, true, false);
        });
        this.userDataSub = this.dataStore.userDetails.subscribe(function (userData) {
            //console.log(`TIS Component<${this.name}> - UserData Loaded`, userData);
            if (!userData)
                return;
            var firstUserData = !_this.userData;
            _this.userData = userData;
            _this.handleFirstData(false, false, firstUserData);
            _this.handleDataUpdate(false, false, true);
        });
    };
    TISBaseComponent.prototype.ngOnDestroy = function () {
        if (this.gameDataSub)
            this.gameDataSub.unsubscribe();
        if (this.tradeDataSub)
            this.tradeDataSub.unsubscribe();
        if (this.userDataSub)
            this.userDataSub.unsubscribe();
        if (this.urlParamSub)
            this.urlParamSub.unsubscribe();
        if (this.queryParamSub)
            this.queryParamSub.unsubscribe();
    };
    TISBaseComponent.prototype.setupURLParameters = function () {
        var _this = this;
        this.urlParamSub = this.http_route.params.subscribe(function (urlParams) {
            _this.urlParams = urlParams;
            _this.onUrlParamsLoaded();
        });
        this.queryParamSub = this.http_route.queryParams.subscribe(function (qParams) {
            _this.queryParams = qParams;
            _this.onQueryParamsLoaded();
        });
    };
    TISBaseComponent.prototype.handleFirstData = function (firstGame, firstTrade, firstUser) {
        if (firstGame)
            this.onFirstGameDataLoaded();
        if (firstTrade)
            this.onFirstTradeDataLoaded();
        if (firstUser)
            this.onFirstUserDataLoaded();
        var anyFirst = firstGame || firstTrade || firstUser;
        if ((firstGame || firstTrade)) {
            if (this.gameLoaded())
                this.onGameTradeDataReady();
        }
        if (this.allLoaded()) {
            if (anyFirst) {
                //console.log(`TIS Component<${name}> - All data ready");
                this.onAllDataReady();
                this.onAllDataGameUpdated();
                this.onAllDataTradeUpdated();
                this.onAllDataUserUpdated();
                //Do this last so any views that depend on data have it loaded already.
                //Not really needed before that
                if (this.http_route)
                    this.setupURLParameters();
            }
        }
    };
    TISBaseComponent.prototype.handleDataUpdate = function (newGame, newTrade, newUser) {
        if (newGame)
            this.onGameDataUpdated();
        if (newTrade)
            this.onTradeDataUpdated();
        if (newUser)
            this.onUserDataUpdated();
        if ((newGame || newTrade) && this.gameLoaded())
            this.onGameTradeUpdated();
        if (this.allLoaded()) {
            this.onAllDataUpdated();
            if (newGame)
                this.onAllDataGameUpdated();
            if (newTrade)
                this.onAllDataTradeUpdated();
            if (newUser)
                this.onAllDataUserUpdated();
        }
    };
    TISBaseComponent.prototype.gameLoaded = function () {
        return !(!this.gameData) && !(!this.tradeData);
    };
    TISBaseComponent.prototype.allLoaded = function () {
        return !(!this.gameData) && !(!this.tradeData) && !(!this.userData);
    };
    TISBaseComponent.prototype.onFirstGameDataLoaded = function () { };
    TISBaseComponent.prototype.onFirstTradeDataLoaded = function () { };
    TISBaseComponent.prototype.onFirstUserDataLoaded = function () { };
    TISBaseComponent.prototype.onGameDataUpdated = function () { };
    TISBaseComponent.prototype.onTradeDataUpdated = function () { };
    TISBaseComponent.prototype.onUserDataUpdated = function () { };
    TISBaseComponent.prototype.onGameTradeDataReady = function () { };
    TISBaseComponent.prototype.onGameTradeUpdated = function () { };
    TISBaseComponent.prototype.onAllDataReady = function () { };
    TISBaseComponent.prototype.onAllDataUpdated = function () { };
    TISBaseComponent.prototype.onUrlParamsLoaded = function () { };
    TISBaseComponent.prototype.onQueryParamsLoaded = function () { };
    TISBaseComponent.prototype.onAllDataGameUpdated = function () { };
    TISBaseComponent.prototype.onAllDataTradeUpdated = function () { };
    TISBaseComponent.prototype.onAllDataUserUpdated = function () { };
    return TISBaseComponent;
}());
exports.TISBaseComponent = TISBaseComponent;
//# sourceMappingURL=tis_base.component.js.map