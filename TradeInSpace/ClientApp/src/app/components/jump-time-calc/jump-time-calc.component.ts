import { Component, OnInit, Input, HostBinding, SimpleChanges, OnChanges, ChangeDetectorRef } from '@angular/core';
import { GameEquipment, SystemBody } from '../../../data/generated_models';
import { DataCacheService } from '../../../data/data_cache';
import { GameDatabaseFull } from '../../../data/game_data';
import { Subscription } from 'rxjs';
import { ModelUtils } from '../../../utilities/model_utils';
import { TISBaseComponent } from '../../tis_base.component';
import { ActivatedRoute } from '@angular/router';
import { MessageService } from 'primeng/api';

@Component({
  selector: 'jump-time-calc',
  template: `
<div class="ui-g">
  <div class="ui-g-12">
    <div class="ui-g-5">
      <p-dropdown [options]="sourceOptions" [(ngModel)]="Source" optionLabel="Name" (onChange)="recalc()"></p-dropdown>
    </div>
    <div class="ui-g-2">
      <span class="nf nf-fa-angle_double_right"></span>
    </div>
    <div class="ui-g-5">
      <p-dropdown [options]="destOptions" [(ngModel)]="Destination" optionLabel="Name" (onChange)="recalc()"></p-dropdown>
    </div>
  </div>
  <span class="ui-g-6">Distance: {{ calculatedDistance | fmtGeneric }}</span>
  <span class="ui-g-6">Time: {{ calculatedTime | timerange }}</span>
</div>
`
})
export class JumpTimeCalcComponent extends TISBaseComponent implements OnInit {
  @Input() public QuantumDrive: GameEquipment = null;
  @Input() public Source: SystemBody = null;
  @Input() public Destination: SystemBody = null;

  public sourceOptions: SystemBody[] = [];
  public destOptions: SystemBody[] = [];

  public calculatedDistance: number;
  public calculatedTime: number;

  constructor(httpRoute: ActivatedRoute,
    dataStore: DataCacheService,
    cdr: ChangeDetectorRef,
    toast: MessageService) {
    super(httpRoute, dataStore, cdr, toast);
  }

  onAllDataGameUpdated(): void {
    this.sourceOptions = this.gameData.SystemBodies;
    this.destOptions = this.gameData.SystemBodies;
  }

  recalc(): void {
    if (!this.Source || !this.Destination || !this.QuantumDrive) return;

    const jumpDetails = ModelUtils.calculateQuantumJump(this.Source, this.Destination, this.QuantumDrive);

    this.calculatedDistance = jumpDetails.total_disance_km;
    this.calculatedTime = jumpDetails.total_time;
  }

}

