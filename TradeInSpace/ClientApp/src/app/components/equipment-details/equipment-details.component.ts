import { Component, OnInit, Input, HostBinding, OnChanges, SimpleChanges } from '@angular/core';
import { GameEquipment } from '../../../data/generated_models';
import { EquipmentProperties, DamageInfo, DamageSummary, DamageMode } from '../../../data/client_models';
import { DataCacheService } from '../../../data/data_cache';
import { GameDatabaseFull } from '../../../data/game_data';
import { calculateDamage } from '../../../data/loadout';

@Component({
  selector: 'equipment-details',
  templateUrl: './equipment-details.component.html',
  styleUrls: ['./equipment-details.component.css']
})
export class EquipmentDetailsComponent implements OnChanges {
  public DamageMode: typeof DamageMode = DamageMode;

  @Input() quantity: number = 1;
  @Input() equipment: GameEquipment = null;
  equipment_props: EquipmentProperties = null;
  damage: DamageSummary = null;

  constructor(private dataCache: DataCacheService) {
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.equipment && changes.equipment.currentValue) {
      var newEq: GameEquipment = changes.equipment.currentValue;
      //Hack to get current instance of db
      //w/o registering an observable for every details component
      var gameDB = this.dataCache.gameDataCConfig.rawSubject.value;

      this.damage = calculateDamage(gameDB, newEq, newEq.Properties.Ammo);
      //console.log(newEq.Name, this.equipment_props, this.damage)
    }
  }

}
