import { Component, OnInit, Input, HostBinding, SimpleChanges, OnChanges, ChangeDetectorRef, SimpleChange, ViewChild, ElementRef } from '@angular/core';
import { DamageInfo } from '../../../data/client_models';
import { fmtDPS, fmtFlatDPS } from '../../../pipes/damage_pipe';
import { AppComponent } from '../../../app/app.component';
import { UIChart } from 'primeng/chart';
import { fmtPercent } from '../../../pipes/scaleunit_pipe';

@Component({
  selector: 'resist-display',
  template: `
<div class="ui-g">
  <div *ngIf="title" class="ui-g-12 center-col" style="padding: 0 0 5px 0">
    <span style="font-weight: bold">{{ title }}</span>
  </div>
  <div #outer class="{{title ? 'ui-g-12' : 'ui-g-6'}}" style="padding: 0">
    <p-chart #chart type="radar" [data]="resistData" [options]="options" [width]="width" [height]="height"></p-chart>
  </div>
</div>
`,
  styles: [`
.center-content {
  position: absolute;
  top: 50%;
  left: 0.5em;
  -webkit-transform: translateY(-50%);
  -ms-transform: translateY(-50%);
  transform: translateY(-50%);
}
`]
})
export class ResistDisplayComponent implements OnInit {
  @Input() public resistance: DamageInfo = null;
  @Input() public width = 32;
  @Input() public height = 32;
  @Input() public collapseDamage = true;
  @Input() public title: string = null;

  @ViewChild("outer", { static: true }) outer: ElementRef;
  @ViewChild("chart", { static: true }) chart: UIChart;

  public resistData: any;
  public options: any;

  constructor(private app: AppComponent) {
    this.options = {
      segmentShowStroke: false,
      legend: false,
      startAngle: 30,
      scale: {
        beginAtZero: false,
        gridLines: {
          color: "black"
        },
        angleLines: {
          color: "black"
        },
        pointLabels: {
          fontColor: [
            "#aaaaaa",
            "#ffbb82",
            "#b67dff",
            "#fb9090",
            "#aef279",
            "#a2fffd",
          ]
        },
        ticks: {
          fontColor: "white",
          showLabelBackdrop: false,
        }
      },
      title: { display: false },
      tooltips: {
        enabled: false,
        custom: (tooltipModel) => {
          if (tooltipModel.body) {
            this.app.showTooltipOn(this.outer.nativeElement, tooltipModel.body[0].lines.join("\r\n"));
          } else {
            this.app.hideTooltip();
          }
        },
        callbacks: {
          label: function (tooltipItem, data) {
            const dataset = data.datasets[tooltipItem.datasetIndex];
            if (!dataset) return '';

            const entry: number = dataset.data[tooltipItem.index];
            const label: string = data.labels[tooltipItem.index];

            //ex: -10% Energy Damage. Leading negative when zero to enphasize its a modifier
            return `${entry === 0 ? "-" : ""}${fmtPercent(entry, false)} ${label} Damage`;
          }
        }
      }
    }

    this.resistData = {
      labels: ["Physical", "Energy", "Distortion", "Thermal", "Bio.", "Stun"],
      datasets: [
        {
          legend: { display: false },
          data: [0, 0, 0, 0, 0, 0],
          fill: false,
          backgroundColor: "transparent",
          borderColor: "#DEDEDE",
          pointBackgroundColor: [
            "#aaaaaa",
            "#ffbb82",
            "#b67dff",
            "#fb9090",
            "#aef279",
            "#a2fffd",
          ],
          pointBorderWidth: 0,
          pointRadius: 6
        }
      ]
    }
  }

  ngOnInit(): void {
    this.update();
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.damage || changes.showLegend || changes.showValues) {
      this.update();
    }
  }

  update(): void {
    if (!this.resistance) return;

    this.resistData.datasets[0].data = [
      this.resistance.Physical * 100 - 100,
      this.resistance.Energy * 100 - 100,
      this.resistance.Distortion * 100 - 100,
      this.resistance.Thermal * 100 - 100,
      this.resistance.Biochemical * 100 - 100,
      this.resistance.Stun * 100 - 100
    ];
    this.chart.refresh();
  }

}

