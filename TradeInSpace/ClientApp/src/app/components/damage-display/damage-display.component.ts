import { Component, OnInit, Input, HostBinding, SimpleChanges, OnChanges, ChangeDetectorRef, SimpleChange, ViewChild, ElementRef } from '@angular/core';
import { DamageInfo } from '../../../data/client_models';
import { fmtDPS, fmtFlatDPS } from '../../../pipes/damage_pipe';
import { AppComponent } from '../../../app/app.component';
import { UIChart } from 'primeng/chart';

@Component({
  selector: 'damage-display',
  template: `
<div class="ui-g">
  <div *ngIf="!title" class="ui-g-6" style="padding: 0;position: relative">
    <span class="center-content">{{ damage | fmtDamage:collapseDamage }}</span>
  </div>
  <div *ngIf="title" class="ui-g-12 center-col" style="padding: 0 0 5px 0">
    <span style="font-weight: bold">{{ title }} - {{ damage | fmtDamage:false }} {{unit}}</span>
  </div>
  <div #outer class="{{title ? 'ui-g-12' : 'ui-g-6'}}" style="padding: 0">
    <p-chart #chart type="pie" [data]="damageData" [options]="options" [width]="width" [height]="height"></p-chart>
  </div>
</div>
`,
  styles: [`
.center-content {
  position: absolute;
  top: 50%;
  left: 0.5em;
  -webkit-transform: translateY(-50%);
  -ms-transform: translateY(-50%);
  transform: translateY(-50%);
}
`]
})
export class DamageDisplayComponent implements OnInit {
  @Input() public damage: DamageInfo = null;
  @Input() public width = 32;
  @Input() public height = 32;
  @Input() public unit = "DPS";
  @Input() public collapseDamage = true;
  @Input() public showLegend = false;
  @Input() public showValues = false;
  @Input() public title: string = null;

  @ViewChild("outer", { static: true }) outer: ElementRef;
  @ViewChild("chart", { static: true }) chart: UIChart;

  public damageData: any;
  public options: any;

  constructor(private app: AppComponent) {
    this.options = {
      segmentShowStroke: false,
      legend: {
        display: this.showLegend,
        animation: {
          duration: 0 // general animation time
        },
        hover: {
          animationDuration: 0 // duration of animations when hovering an item
        },
        responsiveAnimationDuration: 0,
        position: "left",
        align: "center",
        labels: {
          fontColor: "#AAAAAA",
          fontFamily: "Open Sans",
          padding: 5,
          generateLabels: (chart) => this.generateLabels(chart)
        }
      },
      title: { display: false },
      elements: {
        arc: {
          borderWidth: 0
        }
      },
      tooltips: {
        enabled: false,
        custom: (model) => this.showTooltip(model),
        callbacks: {
          label: (item, data) => this.getTooltipText(item, data)
        }
      },
      plugins: {
        datalabels: {
          display: true,
          anchor: 'center',
          fontColor: '#3C3C3C',
          fontFamily: "Share Tech Mono",
          fontStyle: "bold",
          formatter: (v) => {
            if (v === "0") return null;
            return fmtFlatDPS(v, true);
          }
        }
      }
    }

    this.damageData = {
      labels: ["Physical", "Energy", "Distortion", "Thermal", "Biochemical", "Stun"],
      datasets: [
        {
          data: [0, 0, 0, 0, 0, 0],
          backgroundColor: [
            "#aaaaaa",
            "#ffbb82",
            "#b67dff",
            "#fb9090",
            "#aef279",
            "#a2fffd",
          ],
          borderWidth: 0
        }
      ]
    }
  }

  ngOnInit(): void {
    this.update();
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.damage || changes.showLegend || changes.showValues) {
      this.update();
    }
  }

  generateLabels(chart): string[] {
    const data = chart.data;
    if (data.labels.length && data.datasets.length) {
      return data.labels.map((label, i) => {
        const meta = chart.getDatasetMeta(0);
        const ds = data.datasets[0];
        const arc = meta.data[i];
        const custom = arc && arc.custom || {};
        const getValueAtIndexOrDefault = (a: any[], i: number, d: any): any => {
          if (a && i < a.length) return a[i];
          else return d;
        };
        const arcOpts = chart.options.elements.arc;
        const fill = custom.backgroundColor ? custom.backgroundColor : getValueAtIndexOrDefault(ds.backgroundColor, i, arcOpts.backgroundColor);
        const stroke = custom.borderColor ? custom.borderColor : getValueAtIndexOrDefault(ds.borderColor, i, arcOpts.borderColor);
        const bw = custom.borderWidth ? custom.borderWidth : getValueAtIndexOrDefault(ds.borderWidth, i, arcOpts.borderWidth);
        return {
          // And finally :
          text: `${fmtFlatDPS(ds.data[i], false)} ${this.unit} - ${label}`,
          fillStyle: fill,
          strokeStyle: stroke,
          lineWidth: bw,
          hidden: isNaN(ds.data[i]) || meta.data[i].hidden,
          index: i
        };
      });
    } else {
      return [];
    }
  }

  getTooltipText(tooltipItem, data): string {
    const dataset = data.datasets[tooltipItem.datasetIndex];
    if (!dataset) return '';

    const entry: number = dataset.data[tooltipItem.index];
    const label: string = data.labels[tooltipItem.index];

    return `${label}: ${fmtFlatDPS(entry, false)} ${this.unit}`;
  }

  showTooltip(tooltipModel): void {
    if (tooltipModel.body) {
      this.app.showTooltipOn(this.outer.nativeElement, tooltipModel.body[0].lines.join("\r\n"));
    } else {
      this.app.hideTooltip();
    }
  }

  update(): void {
    if (!this.damage) return;

    this.options.legend.display = this.showLegend;
    this.options.plugins.datalabels.display = this.showValues;

    this.damageData.datasets[0].data = [
      this.damage.Physical,
      this.damage.Energy,
      this.damage.Distortion,
      this.damage.Thermal,
      this.damage.Biochemical,
      this.damage.Stun
    ];
    this.chart.refresh();
  }

}

