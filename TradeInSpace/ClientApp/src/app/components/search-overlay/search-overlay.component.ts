import { Component, ViewChild, ErrorHandler, ChangeDetectorRef, ElementRef } from '@angular/core';
import { GameEquipment, SystemBody, GameShip } from '../../../data/generated_models';
import { EquipmentProperties } from '../../../data/client_models';
import { DataCacheService } from '../../../data/data_cache';
import { GameDatabaseFull } from '../../../data/game_data';
import { LoadoutSlot } from '../../../data/loadout';
import { TISBaseComponent } from '../../tis_base.component';
import { InputText } from 'primeng/inputtext';
import { ActivatedRoute, Router } from '@angular/router';
import { Action } from 'rxjs/internal/scheduler/Action';
import { MenuItem, SelectItem } from 'primeng/api';
import { SelectButtonModule, SelectButton } from 'primeng/selectbutton';

@Component({
  selector: 'search-overlay',
  templateUrl: './search-overlay.component.html',
  styles: []
})
export class SearchOverlayComponent extends TISBaseComponent {

  searchText: string = "";
  @ViewChild("search_text", { static: false }) searchTextBox: InputText;

  selectedCategory: SelectItem = null;
  categories: SelectItem[] = [
    { label: 'Locations', value: { id: 1, name: 'New York', code: 'NY' } },
    { label: 'Personal Items', value: { id: 2, name: 'Rome', code: 'RM' } },
    { label: 'Vehicle Items', value: { id: 3, name: 'London', code: 'LDN' } },
    { label: 'Vehicles', value: { id: 4, name: 'Istanbul', code: 'IST' } },
    { label: 'Trade Items', value: { id: 5, name: 'Paris', code: 'PRS' } }
  ];
  searchResults: Array<SearchRecord> = []

  constructor(http_route: ActivatedRoute,
    dataStore: DataCacheService,
    cdr: ChangeDetectorRef,
    private router: Router) {
    super(http_route, dataStore, cdr, null);
  }

  searchChanged(e: Event): void {
    if (!this.gameData) return;
    if (this.searchText.length < 3) return;

    this.searchResults = this.doSearch(this.searchText)
  }

  doSearch(searchText: string) {
    const lowerSearch = searchText.toLowerCase()
    const searchBasic = (e: any) => e.Key.includes(lowerSearch)
    const searchNameDescription = (e: any) => (e.Name !== "<= PLACEHOLDER =>") &&
                                              (searchBasic(e) || e.Name.includes(lowerSearch) || e.Description.includes(lowerSearch))

    const bodyRoute = (e: SystemBody) => this.router.createUrlTree(["/expore", "system", e.Key]).toString();
    const equipRoute = (e: GameEquipment) => this.router.createUrlTree(["/equipment", e.AttachmentType, e.Key]).toString()
    const shipRoute = (e: GameShip) => this.router.createUrlTree(["/loadout", e.Key]).toString()

    const allResults = [].concat(
      this.searchCollection(searchText, this.gameData.SystemBodies, "system_body", bodyRoute, searchNameDescription),
      this.searchCollection(searchText, this.gameData.Equipment, "equipment", equipRoute, searchNameDescription),
      this.searchCollection(searchText, this.gameData.Ships, "ship", shipRoute, searchNameDescription)
    )
    console.log(`[${searchText}] - ${allResults.length} Results`)
    console.log(allResults)
    return allResults.slice(0, 25)
  }

  searchCollection<T>(searchText: string, collection: Array<T>, resultType: string,
          link: (value: T) => string, search: (value: T) => boolean = null): Array<SearchRecord> {
    if (!search)
      search = (e: any) => e.Key.includes(searchText.toLowerCase())
    return collection.filter(search).map(record => {
      return {
        "type": resultType,
        "record": record,
        "link": link(record)
      };
    })
  }
  
}

interface SearchRecord {
  "type": string,
  "record": any,
  "link": string
}
