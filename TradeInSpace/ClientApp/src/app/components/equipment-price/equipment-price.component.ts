import { Component, OnInit, Input, HostBinding, OnChanges, SimpleChanges, Output, ViewChild } from '@angular/core';
import { GameEquipment, TradeEquipmentEntry, TradeTable } from '../../../data/generated_models';
import { DataCacheService } from '../../../data/data_cache';
import { MenuItem } from 'primeng/api';
import { fmtUEC } from '../../../pipes/scaleunit_pipe';
import { ShoppingCartManagerService } from '../../../data/shopping_cart_manager';
import { EventEmitter } from 'events';
import { SplitButton } from 'primeng/splitbutton';

@Component({
  selector: 'equipment-price',
  templateUrl: './equipment-price.component.html',
  styles: [
`
  :host::ng-deep .ui-splitbutton span.ui-button-text {
    text-align: left;
  }
`
  ]
})
export class EquipmentPriceComponent implements OnChanges {

  @Input()
  public label: string = null;
  @Input()
  public icon: string = null;
  @Input()
  public clickPurchases: boolean = true;
  @Input()
  public clickExpands: boolean = false;
  @Input()
  public selectionMode: boolean = false;
  @Input()
  public styleClass: string = "ui-button-info";

  @Input()
  public equipment: GameEquipment = null;
  @Input()
  public equipmentTrade: TradeEquipmentEntry = null;


  public equipmentTrades: MenuItem[] = [];


  constructor(private dataCache: DataCacheService,
    private cart: ShoppingCartManagerService) {
  }

  optionClicked(event: { originalEvent: Event, item: MenuItem }) {
    if (event && event.item) {
      const eventTrade: TradeEquipmentEntry = (event.item as any).value;

      this.cart.AddEquipmentTrade(eventTrade)
    }
  }

  defaultClicked() {
    if (this.clickPurchases) {
      if (this.equipmentTrades.length > 0) {
        const cheapestTrade: TradeEquipmentEntry = (this.equipmentTrades[0] as any).value;
        this.cart.AddEquipmentTrade(cheapestTrade);
      }
    } else if (this.clickExpands) {
      
    }
  }

  tradeChanged(event: { originalEvent: Event, value: any }) {
    if (event && event.value) {
      const eventTrade: TradeEquipmentEntry = event.value as TradeEquipmentEntry;

      this.cart.ChangeTrade(this.equipmentTrade, eventTrade);
    }
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.equipment && changes.equipment.currentValue) {
      const newEq: GameEquipment = changes.equipment.currentValue;

      if (!newEq || !newEq.TradeEntries) {
        this.equipmentTrades = [];
        return;
      }
      this.equipmentTrades = newEq.TradeEntries
        .filter(te => te.BuyPrice > 0)
        .map<MenuItem>(te => {
        return {
          value: te,
          disabled: false,
          title: `${fmtUEC(te.BuyPrice, false, true)} - ${te.Station.Name}`,
          label: `${fmtUEC(te.BuyPrice, false, true)} - ${te.Station.Name}`,
          icon: "nf nf-fa-shopping_cart",
          command: (evt) => { this.optionClicked(evt); }
        }
      });
    }
  }

}
