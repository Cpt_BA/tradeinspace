import { Component, OnInit, Input, HostBinding, OnChanges, SimpleChanges, Output, EventEmitter } from '@angular/core';
import { GameEquipment } from '../../../data/generated_models';
import { EquipmentProperties } from '../../../data/client_models';
import { DataCacheService } from '../../../data/data_cache';
import { GameDatabaseFull } from '../../../data/game_data';
import { LoadoutSlot } from '../../../data/loadout';

@Component({
  selector: 'equipment-selector',
  templateUrl: './equipment-selector.component.html',
  styleUrls: ['./equipment-selector.component.css']
})
export class EquipmentSelectorComponent implements OnChanges {

  @Input() disabled: boolean = false;
  @Input() allow_empty: boolean = true;
  @Input() show_stats: boolean = true;

  @Output() populateEquipmentList: EventEmitter<EquipmentListExpandedParams> = new EventEmitter();

  @Input() selectedEquipment: GameEquipment = null;

  @Output() onChange: EventEmitter<GameEquipment> = new EventEmitter();

  public equipmentOptions: GameEquipment[];

  constructor(private dataCache: DataCacheService) {
  }

  listExpanded(event: Event): void {
    const expandArgs: EquipmentListExpandedParams = {
      show: true
    };

    this.populateEquipmentList.emit(expandArgs);

    if (expandArgs.show && expandArgs.equipment) {

      if (this.allow_empty) {
        expandArgs.equipment.splice(0, 0, null); //Insert null at the start
      }

      this.equipmentOptions = expandArgs.equipment;
    }
  }

  changeEquipment(event: Event): void {
    this.onChange.emit(this.selectedEquipment);
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.selectedEquipment && changes.selectedEquipment.currentValue) {
      const newEq: GameEquipment = changes.selectedEquipment.currentValue;
      if (!this.equipmentOptions) {
        this.equipmentOptions = [newEq];
      }
    }
  }

}

export interface EquipmentListExpandedParams {
  show: boolean;
  equipment?: GameEquipment[];
}
