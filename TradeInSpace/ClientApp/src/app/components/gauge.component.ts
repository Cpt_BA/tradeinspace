import { Component, OnInit, Input, HostBinding } from '@angular/core';

@Component({
  selector: 'gauge',
  templateUrl: './gauge.component.html',
  styleUrls: ['./gauge.component.css']
})
export class GaugeComponent {

  @Input() value: number = 0.4;
  @Input() size: number = 30;
  @Input() thickness: number = 10;

  constructor() {
  }

  containerStyle(): any {
    return {
      'width': `${this.size * 2}px`,
      'height': `${this.size}px`
    };
  }
  gaugeBackground(): any {
    return {
      'width': `${this.size * 2}px`,
      'height': `${this.size}px`,
      'border-radius': `${(this.size - this.thickness) * 2}px ${(this.size - this.thickness) * 2}px 0px 0px`
    };
  }
  gaugeCenter(): any {
    return {
      'width': `${(this.size - this.thickness) * 2}px`,
      'height': `${this.size - this.thickness}px`,
      'top': `${this.thickness}px`,
      'margin-left': `${this.thickness}px`,
      'border-radius': `${(this.size - this.thickness) * 2}px ${(this.size - this.thickness) * 2}px 0px 0px`
    };
  }
  gaugeMeter(): any {
    return {
      'width': `${this.size * 2}px`,
      'height': `${this.size}px`,
      'top': `${this.size}px`,
      'border-radius': `0px 0px ${this.size}px ${this.size}px`
    };
  }
  gaugeContent(): any {
    return {
      'width': `${this.size * 2}px`,
      'height': `${this.size}px`,
      'top': `${this.thickness * 2}px`,
      'font-size': `${this.thickness * 1.5}px`,
      'line-height': `${this.thickness / 3}px`
    };
  }
}

