import { Component, ViewChild, ElementRef, OnInit, Input, ChangeDetectorRef, OnChanges, SimpleChange, SimpleChanges } from '@angular/core';
import { DataCacheService } from '../../../data/data_cache';
import { ModelUtils } from '../../../utilities/model_utils';
import { BodyType, SystemBody } from '../../../data/generated_models';
import { TISBaseComponent } from '../../tis_base.component';
import { ActivatedRoute } from '@angular/router';
import { MessageService } from 'primeng/api';
import { map } from 'rxjs-compat/operator/map';

@Component({
  selector: 'system-display',
  template: `
    <canvas #canvas [width]="width" [height]="height"
      (mousedown)="canvasMouseDown($event)"
      (mousemove)="canvasMouseMove($event)"
      (mouseup)="canvasMouseUp($event)"
      (mouseleave)="canvasMouseOut($event)"
    ></canvas>
  `,
  styles: ['canvas { border-style: solid }']
})
export class SystemDisplayComponent extends TISBaseComponent implements OnInit {
  @ViewChild('canvas', { static: true })
  canvas: ElementRef<HTMLCanvasElement>;

  @Input() width = 800;
  @Input() height = 800;

  @Input() selectedBody: SystemBody;
  hoveredBody: MapBody;
  selectedMapBody: MapBody;

  mapBodies: MapBody[];
  mapGroups: PlanetGroup[];

  //Configuration Parameters
  minScale = .0000001;
  maxScale = 1;
  hoverRadiusViewport = 30;
  collapseRaduisViewport = 20;
  planetRadiusViewport = 5;

  //Display Parameters
  camera: CameraParams = {
    scale: .00001,
    globalCenter: { x: 0, y: 0 },
    viewportCenter: { x: 0, y: 0 }
  };

  mouse: MouseParams = {
    viewport: { x: 0, y: 0 },
    global: { x: 0, y: 0 },
    bounds: null,
    button: 0
  };

  private ctx: CanvasRenderingContext2D;

  constructor(http_route: ActivatedRoute,
    dataStore: DataCacheService,
    cdr: ChangeDetectorRef,
    toast: MessageService) {
    super(http_route, dataStore, cdr, toast);
  }

  drawEverything(): void {
    if (!this.ctx) return;
    if (!this.allLoaded()) return;

    this.ctx.clearRect(0, 0, this.width, this.height);

    this.ctx.strokeStyle = "black";

    this.ctx.fillRect(0, 0, 0.1, 0.1);

    //Draw orbirs first, so planets are always on top
    this.mapBodies.forEach(b => { this.drawOrbit(b); });
    //Drawn non-highlighted so it's always on top
/*
    this.mapBodies.forEach(b => { if (!b.highlighted) this.drawBody(b) });
    this.drawBody(this.hoveredBody);
*/

    this.mapGroups.forEach(g => { this.drawGroup(g); });

    this.ctx.resetTransform();
  }

  drawGroup(mapGroup: PlanetGroup): void {
    this.drawBody(mapGroup.mainBody, false);

    this.ctx.setLineDash([]);
    this.ctx.shadowColor = "white";
    this.ctx.shadowBlur = 3;
    
    this.ctx.strokeText(mapGroup.title, mapGroup.mainBody.viewportPosition.x + 15, mapGroup.mainBody.viewportPosition.y);

    this.ctx.shadowColor = "transparent";
  }

  drawOrbit(mapBody: MapBody): void {
    const screenDist = this.pointDistance({ x: 0, y: 0 }, this.camera.globalCenter);
    const screenSize = this.width / this.camera.scale;
    const bodyDist = this.pointDistance({ x: 0, y: 0 }, mapBody.globalPosition);

    if (bodyDist < screenDist - screenSize || bodyDist > screenDist + screenSize) {
      return;
    }

    const r = (mapBody.body.OrbitRadius / 1000) * this.camera.scale; //r in km

    const parentPosGlobal = this.globalToViewport(mapBody.orbitalCenter);

    switch (mapBody.body.BodyType) {
      case BodyType.Star: return;
      case BodyType.Moon:
        this.ctx.setLineDash([20, 5]);
        break;
      case BodyType.Planet:
        this.ctx.setLineDash([]);
        break;
      case BodyType.Other:
        this.ctx.setLineDash([3,3]);
        break;
      case BodyType.AsteroidField:
        this.ctx.setLineDash([10, 5]);
        break;
    }

    this.ctx.strokeStyle = "#111111";

    this.ctx.beginPath();
    this.ctx.ellipse(parentPosGlobal.x, parentPosGlobal.y, r, r, 0, 0, Math.PI * 2);
    this.ctx.stroke();
  }

  drawBody(mapBody: MapBody, drawName: boolean): void {
    //Basic culling
    if (!this.inFrame(mapBody.viewportPosition)) {
      return;
    }

    if (mapBody === this.selectedMapBody) {
      const oldStroke = this.ctx.strokeStyle;
      this.ctx.strokeStyle = "blue";
      this.ctx.beginPath();
      this.ctx.ellipse(mapBody.viewportPosition.x, mapBody.viewportPosition.y, this.planetRadiusViewport * 1.5, this.planetRadiusViewport * 1.5, 0, 0, Math.PI * 2);
      this.ctx.stroke();

      this.ctx.fillStyle = "green";
      this.ctx.strokeStyle = oldStroke;
    } else {
      this.ctx.fillStyle = mapBody.highlighted ? "blue" : "red";
    }

    this.ctx.beginPath();
    this.ctx.ellipse(mapBody.viewportPosition.x, mapBody.viewportPosition.y, this.planetRadiusViewport, this.planetRadiusViewport, 0, 0, Math.PI * 2);
    this.ctx.fill();

    if (drawName) {
      this.ctx.setLineDash([]);
      this.ctx.strokeText(mapBody.body.Name, mapBody.viewportPosition.x + (3 * this.planetRadiusViewport), mapBody.viewportPosition.y);
    }

  }

  calculatePlanetGroups(): void {
    this.mapGroups = [];
    this.mapBodies.forEach(checkBody => {
      checkBody.viewportPosition = this.globalToViewport(checkBody.globalPosition);

      const collideGroup = this.mapGroups.find(g => this.pointDistance(g.mainBody.viewportPosition, checkBody.viewportPosition) <= this.collapseRaduisViewport);
      if (collideGroup) {
        if (checkBody.depth < collideGroup.mainBody.depth || (checkBody.depth === collideGroup.mainBody.depth && checkBody.body.BodyType === BodyType.Planet)) {
          collideGroup.mainBody = checkBody;
        }
        collideGroup.groupedBodies.push(checkBody);
        collideGroup.title = `${collideGroup.mainBody.body.Name} (+${collideGroup.groupedBodies.length - 1})`;
      } else {
        this.mapGroups.push({
          mainBody: checkBody,
          title: checkBody.body.Name,
          groupedBodies: [checkBody]
        });
      }
    });
  }

  inFrame(point: Point): boolean {
    return (point.x > -this.planetRadiusViewport || point.y > -this.planetRadiusViewport ||
      point.x < this.width + this.planetRadiusViewport || point.y < this.height + this.planetRadiusViewport)
  }

  canvasMouseDown(mouse: MouseEvent): void {
    if (!this.gameData) return;
    this.move(mouse);
  }

  canvasMouseMove(mouse: MouseEvent): void {
    if (!this.gameData) return;
    this.move(mouse);
  }

  canvasMouseUp(mouse: MouseEvent): void {
    if (!this.gameData) return;
    this.move(mouse);
  }

  canvasMouseOut(mouse: MouseEvent): void {
    if (!this.gameData) return;
    this.move(mouse);
  }
  
  zoomed(number: number): number {
    return Math.floor(number * this.camera.scale);
  }

  move(event: MouseEvent): void {
    event.preventDefault();

    if (event.type === "mousedown") {
      this.mouse.button = 1;
    }
    else if (event.type === "mouseup" || event.type === "mouseout") {
      this.mouse.button = 0;
    }

    this.mouse.bounds = this.canvas.nativeElement.getBoundingClientRect();
    this.mouse.viewport = {
      x: event.clientX - this.mouse.bounds.left,
      y: event.clientY - this.mouse.bounds.top
    }
    const lastMouseGlobal = this.mouse.global; // get last real world pos of mouse

    this.mouse.global = this.viewportToGlobal(this.mouse.viewport); // get the mouse real world pos via inverse scale and translate

    //Update all planets' on-screen position in memory
    this.mapBodies.forEach(mb => {
      mb.viewportPosition = this.globalToViewport(mb.globalPosition);
    });
    //Determine which ones if any we are hovering
    const hoverGroup = this.mapGroups.find(g => {
      if (this.viewportDistance(g.mainBody.globalPosition, this.mouse.global) < this.hoverRadiusViewport) {
        return true;
      } else {
        return false;
      }
    });
    this.hoveredBody = hoverGroup ? hoverGroup.mainBody : null;
    this.mapBodies.forEach(mb => {
      mb.highlighted = (mb === this.hoveredBody);
    });

    if (this.mouse.button === 1) { // is mouse button down
      this.camera.globalCenter = {
        // move the world origin by the distance, moved in world coords
        x: this.camera.globalCenter.x - (this.mouse.global.x - lastMouseGlobal.x),
        y: this.camera.globalCenter.y - (this.mouse.global.y - lastMouseGlobal.y),
      }

      // recaculate mouse world
      this.mouse.global = this.viewportToGlobal(this.mouse.viewport);
    }

    this.drawEverything();
  }

  zoomGlobal(factor: number, globalCenter: Point, newCenterViewport: Point = null) {
    const newViewport = newCenterViewport || this.globalToViewport(globalCenter);

    this.camera.scale = Math.min(this.maxScale, Math.max(this.minScale, this.camera.scale * factor))
    this.camera.globalCenter = globalCenter; // set zoom center for calculations
    this.camera.viewportCenter = newViewport; // set screen origin

    //Recalculate mouse's global position
    this.mouse.global = this.viewportToGlobal(this.mouse.viewport);

    this.calculatePlanetGroups();

    this.drawEverything();
  }

  viewportToGlobal(position: Point): Point {
    return {
      x: Math.floor((position.x - this.camera.viewportCenter.x) * (1 / this.camera.scale) + this.camera.globalCenter.x),
      y: Math.floor((position.y - this.camera.viewportCenter.y) * (1 / this.camera.scale) + this.camera.globalCenter.y)
    }
  }

  globalToViewport(position: Point): Point {
    return {
      x: Math.floor((position.x - this.camera.globalCenter.x) * this.camera.scale + this.camera.viewportCenter.x),
      y: Math.floor((position.y - this.camera.globalCenter.y) * this.camera.scale + this.camera.viewportCenter.y)
    }
  }

  pointDistance(pointA: Point, pointB: Point): number {
    const pointDist = {
      x: pointA.x - pointB.x,
      y: pointA.y - pointB.y
    };
    return Math.sqrt(pointDist.x * pointDist.x + pointDist.y * pointDist.y);
  }

  viewportDistance(globalA: Point, globalB: Point): number {
    return this.camera.scale * this.pointDistance(globalA, globalB);
  }

  canvasMouseWheel(wheel: WheelEvent): void {
    let zoomAbout: Point = null;
    if (this.selectedMapBody) {
      zoomAbout = this.selectedMapBody.globalPosition;
    } else if (this.hoveredBody) {
      zoomAbout = this.hoveredBody.globalPosition;
    } else {
      zoomAbout = this.mouse.global;
    }

    if (wheel.deltaY < 0) {
      this.zoomGlobal(1.1, zoomAbout);
    } else {
      this.zoomGlobal(1 / 1.1, zoomAbout);
    }
  }

  canvasClick(mouse: MouseEvent) {
    mouse.preventDefault();
    if (mouse.detail === 1) {
      let newCenter: Point = null;
      if (this.hoveredBody) {
        newCenter = this.hoveredBody.globalPosition;
        this.selectedMapBody = this.hoveredBody;
      } else if (this.selectedMapBody) {
        this.selectedMapBody = null;
      } 

      if (newCenter) {
        this.zoomGlobal(1, newCenter, { x: this.width / 2, y: this.height / 2 });
      }
    }

    if (mouse.detail >= 2 && this.selectedMapBody) {
      this.zoomGlobal(4, this.selectedMapBody.globalPosition);
    }
  }

  ngAfterViewInit(): void {
    this.ctx = this.canvas.nativeElement.getContext('2d');

    this.camera.viewportCenter = {
      x: this.width / 2,
      y: this.height / 2
    };

    this.canvas.nativeElement.onwheel = (evt) => { this.canvasMouseWheel(evt) };
    this.canvas.nativeElement.onclick = (evt) => { this.canvasClick(evt); };

    this.drawEverything();
  }

  ngOnDestroy(): void {
    super.ngOnDestroy();

    
  }

  onAllDataGameUpdated(): void {
    this.mapBodies = this.gameData.SystemBodies
      //.filter(sb => sb.BodyType === BodyType.Star || sb.BodyType === BodyType.Planet || sb.BodyType === BodyType.Moon)
      .map(sb => {
        //Ugly but it works :^)
        let depth = 0;
        let depthTest = sb;
        while (depthTest) { depth++; depthTest = depthTest.ParentBody; }
        const pos = ModelUtils.calculateBodyPositionKM(sb);
        return {
          body: sb,
          globalPosition: pos,
          viewportPosition: this.globalToViewport(pos),
          orbitalCenter: sb.ParentBody ? ModelUtils.calculateBodyPositionKM(sb.ParentBody) : { x: 0, y: 0 },
          highlighted: false,
          selectable: true,
          depth: depth
        }
      })
      .sort((a, b) => { return b.depth - a.depth; }); //Sorted highest depth to lowest, how drawing happens.

    this.calculatePlanetGroups();
    this.drawEverything();
  }
  
}

interface PlanetGroup {
  mainBody: MapBody;
  groupedBodies: MapBody[];
  title: string;
}

interface MapBody {
  body: SystemBody;
  globalPosition: Point;
  viewportPosition: Point;
  orbitalCenter: Point;
  selectable: boolean;
  highlighted: boolean;
  depth: number;
}

interface CameraParams {
  scale: number;
  globalCenter: Point;
  viewportCenter: Point;
}

interface MouseParams {
  viewport: Point;
  global: Point;
  bounds: DOMRect | ClientRect;
  button: number;
}

interface Point {
  x: number;
  y: number;
}
