import { Component, OnInit, Input, OnChanges, SimpleChanges, Output, EventEmitter, ChangeDetectorRef, ViewChild } from '@angular/core';
import { SelectItemGroup, SelectItem, MessageService } from 'primeng/api';
import { GameEquipment, GameShip, Manufacturer, PadSize } from '../../../data/generated_models';
import { EquipmentProperties } from '../../../data/client_models';
import { DataCacheService } from '../../../data/data_cache';
import { GameDatabaseFull } from '../../../data/game_data';
import { Loadout, LoadoutSlot } from '../../../data/loadout';
import { ModelUtils } from '../../../utilities/model_utils';
import { Group, groupSet } from '../../../utilities/utilities';
import { TISBaseComponent } from '../../tis_base.component';
import { ActivatedRoute } from '@angular/router';
import { ClientData } from '../../../data/client_data';
import { LoadoutManagerService } from '../../../data/loadout_manager';
import { OverlayPanel } from 'primeng/overlaypanel';
import { ShowTooltipEvent } from '../../../directive/tooltip-attr.directive';
import { fmtUEC } from '../../../pipes/scaleunit_pipe';

@Component({
  selector: 'ship-select',
  templateUrl: './ship-select.component.html',
  styleUrls: ['./ship-select.component.css']
})
export class ShipSelectorComponent extends TISBaseComponent implements OnChanges {
  ClientData: typeof ClientData = ClientData;

  @Input() disabled: boolean = false;
  @Input() allow_empty: boolean = true;
  @Input() show_stats: boolean = true;

  @Output() populateShipList: EventEmitter<ShipListExpandedParams> = new EventEmitter();

  manufacturerOptions: SelectItem[];
  careerOptions: SelectItem[];
  roleOptions: SelectItem[];
  sizeOptions: SelectItem[];

  @Input() selectedLoadout: Loadout = null;

  filterText: string;
  selectedManufacturers: Manufacturer[];
  selectedCareers: string[];
  selectedRoles: string[];
  selectedSizes: string[];

  @Output() onChange: EventEmitter<Loadout> = new EventEmitter();

  @ViewChild("op", { static: true }) overlayPanel: OverlayPanel;

  public shipOptions: ShipSelectEntry[];
  public filteredShips: ShipSelectEntry[];

  constructor(httpRoute: ActivatedRoute,
    private loadouts: LoadoutManagerService,
    dataStore: DataCacheService,
    cdr: ChangeDetectorRef,
    toast: MessageService) {
    super(httpRoute, dataStore, cdr, toast);
  }

  onAllDataGameUpdated() {
    // Cache data?
  }

  panelExpanded(event: Event): void {
    const expandArgs: ShipListExpandedParams = {
      show: true
    };

    this.populateShipList.emit(expandArgs);

    if (expandArgs.show && expandArgs.loadouts) {

      var loadoutsByManufacturer = groupSet(expandArgs.loadouts, l => l.from_ship.Manufacturer);
      
      this.manufacturerOptions = Object
        .keys(loadoutsByManufacturer)
        .map((m) => {
          var manufacturer = loadoutsByManufacturer[m][0].from_ship._Manufacturer;
          return {
            label: m,
            value: manufacturer,
            icon: manufacturer.LogoName
          }
        });

      this.careerOptions = Array.from(new Set(expandArgs.loadouts.map(s => s.from_ship.AdditionalProperties.Career)))
        .map(career => {
          return {
            label: career,
            value: career
          }
        });

      this.roleOptions = Array.from(new Set(expandArgs.loadouts.map(s => s.from_ship.AdditionalProperties.Role)))
        .map(role => {
          return {
            label: role,
            value: role
          }
        });

      this.shipOptions = expandArgs.loadouts.map((loadout) => {
        var trades = this.tradeData.EquipmentEntries.filter((e) => e.Equipment.Key == loadout.from_ship.Key);
        var purchase = 0, rental = false
        for (var index in trades) {
          var trade = trades[index];
          
          if (trade.RentalDuration)
            rental = true;
          else if (trade.BuyPrice)
            purchase = trade.BuyPrice;
        }
        var sizeOption = ClientData.SizeSelectOptions.find(sfo => { return sfo.value == loadout.from_ship.Size });

        return {
          name: loadout.from_ship.Name,
          manufacturer_name: loadout.from_ship.Manufacturer,
          career: loadout.from_ship.AdditionalProperties["Career"],
          role: loadout.from_ship.AdditionalProperties["Role"],
          size: sizeOption ? sizeOption.label : "N/A",
          price: purchase,
          rental: rental,
          scu: loadout.from_ship.CargoCapacity,

          ship: loadout.from_ship,
          loadout: loadout,
          manufacturer: loadout.from_ship._Manufacturer
        }
      });

      this.filterUpdated(null);
    }
  }

  filterUpdated(event: Event): void {

    function fnIsEmpty(value: any) {
      return value == null || (Array.isArray(value) && value.length === 0) || value === "";
    }

    this.filteredShips = this.shipOptions.filter(so => {
      //Ignore any misses when manufacturers, careers, roles, or sizes are filled out
      if (!fnIsEmpty(this.selectedManufacturers) &&
        !this.selectedManufacturers.some(mfg => so.manufacturer_name.toLowerCase().includes(mfg.Name.toLowerCase())))
        return false;
      if (!fnIsEmpty(this.selectedCareers) &&
        !this.selectedCareers.some(career => so.career.toLowerCase().includes(career.toLowerCase())))
        return false;
      if (!fnIsEmpty(this.selectedRoles) &&
        !this.selectedRoles.some(role => so.role.toLowerCase().includes(role.toLowerCase())))
        return false;
      //Sizes are funny
      if (!fnIsEmpty(this.selectedSizes) &&
        !this.selectedSizes.some(size => {
          var selectedSize = so.size.toLowerCase();
          var matchingSize = ClientData.SizeSelectOptions.find(sfo => { return sfo.value == size });
          return selectedSize.includes(matchingSize.label.toLowerCase())
        }))
        return false;

      // Finally filter by text
      if (!fnIsEmpty(this.filterText) &&
        !(
          so.name.toLowerCase().includes(this.filterText.toLowerCase()) ||
          so.manufacturer_name.toLowerCase().includes(this.filterText.toLowerCase()) ||
          so.career.toLowerCase().includes(this.filterText.toLowerCase()) ||
          so.role.toLowerCase().includes(this.filterText.toLowerCase()) ||
          so.size.toLowerCase().includes(this.filterText.toLowerCase())
        ))
        return false;

      return true;
    });

  }

  selectLoadout(event: Event, loadout: Loadout): void {
    this.selectedLoadout = loadout;
    this.onChange.emit(loadout);
    this.overlayPanel.hide();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.selectedShip && changes.selectedShip.currentValue) {
      /*
      const newEq: GameEquipment = changes.selectedEquipment.currentValue;
      if (!this.equipmentOptions) {
        this.equipmentOptions = [newEq];
      }
      */
    }
  }

  public showShipLocationsTooltip(selection: ShipSelectEntry, event: ShowTooltipEvent): void {
    //Find trade
    var trades = this.tradeData.EquipmentEntries.filter((e) => e.Equipment.Key == selection.ship.Key && e.BuyPrice != 0 &&
      (e.RentalDuration == null || e.RentalDuration == 1));
    if (trades.length > 0) {
      var lines = trades
        .map(t =>
          `<tr>
<td>${t.Station.Name}</td>
<td>${t.Station.Location.ParentBody ? t.Station.Location.ParentBody.Name : ""}</td>
<td style='text-align: right'>${t.RentalDuration == 1 ? '[Rent] ' : ''}${fmtUEC(t.BuyPrice, true, true)} </td>
</tr>`)
        .join("");

      var final = `<table style='width:600px'>
<thead>
<tr>
<td style='text-align: center; width: 50%'>Store</td>
<td style='text-align: center; width: 25%'>Planet</td>
<td style='text-align: center; width: 25%'>Price</td></tr>
</thead>
<tbody>
${lines}
</tbody>
</table>`

      event.content = final;
    } else {
      event.show = false;
    }
  }

}

export interface ShipListExpandedParams {
  show: boolean;
  loadouts?: Loadout[];
}

interface ShipSelectEntry {
  name: string;
  price: number;
  rental: boolean;
  scu: number;
  career: string;
  role: string;
  size: string;
  manufacturer_name: string;

  ship: GameShip;
  loadout: Loadout;
  manufacturer: Manufacturer;
}
