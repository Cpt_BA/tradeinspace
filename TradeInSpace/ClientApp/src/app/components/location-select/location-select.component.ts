import { Component, OnInit, Input, OnChanges, SimpleChanges, Output, EventEmitter, ChangeDetectorRef, ViewChild } from '@angular/core';
import { TreeNode, MessageService, SelectItem } from 'primeng/api';
import { GameEquipment, GameShip, LocationType, Manufacturer, PadSize, ServiceFlags, SystemBody, SystemLocation } from '../../../data/generated_models';
import { EquipmentProperties } from '../../../data/client_models';
import { DataCacheService } from '../../../data/data_cache';
import { GameDatabaseFull } from '../../../data/game_data';
import { Loadout, LoadoutSlot } from '../../../data/loadout';
import { ModelUtils } from '../../../utilities/model_utils';
import { Group, groupSet } from '../../../utilities/utilities';
import { TISBaseComponent } from '../../tis_base.component';
import { ActivatedRoute } from '@angular/router';
import { ClientData } from '../../../data/client_data';
import { LoadoutManagerService } from '../../../data/loadout_manager';
import { OverlayPanel } from 'primeng/overlaypanel';
import { ServiceColumn, UiUtils } from '../../../utilities/ui_utils';

@Component({
  selector: 'location-select',
  templateUrl: './location-select.component.html',
  styleUrls: ['./location-select.component.css']
})
export class LocationSelectorComponent extends TISBaseComponent implements OnChanges {
  //Hack to make LocationType accesible to the html
  LocationType: typeof LocationType = LocationType;
  ServiceFlags: typeof ServiceFlags = ServiceFlags;
  ModelUtils: typeof ModelUtils = ModelUtils;
  ClientData: typeof ClientData = ClientData;
  UiUtils: typeof UiUtils = UiUtils;

  @Input() disabled: boolean = false;
  @Input() allow_empty: boolean = true;
  @Input() show_stats: boolean = true;

  selectedLocation: SystemLocation = null;

  allOptions: TreeNode[];

  locationOptions: TreeNode[];

  browsingNode: SystemLocation;

  displayServices: ServiceColumn[] = [
    { description: "Ship Landing", icon: "nf nf-mdi-airplane_landing", service: ServiceFlags.ShipLanding },
    { description: "Ship Claiming", icon: "nf nf-mdi-airplane_takeoff", service: ServiceFlags.ShipClaim },
    { description: "Ship Purchasing", icon: "nf nf-fa-space_shuttle", service: ServiceFlags.ShipPurchase },
    { description: "Ship Equipment", icon: "nf nf-fa-gears", service: ServiceFlags.ShipEquipment },
    { description: "Ship Rental", icon: "nf nf-mdi-clock_start", service: ServiceFlags.ShipRental },
    
    { description: "FPS Respawn", icon: "nf nf-fa-bed", service: ServiceFlags.FPSRespawnPoint },
    { description: "FPS Armor", icon: "nf nf-fae-layers", service: ServiceFlags.FPSArmor },
    { description: "FPS Weaponry", icon: "nf nf-fa-crosshairs", service: ServiceFlags.FPSWeapon },
    { description: "FPS Equipment", icon: "nf nf-fa-wrench", service: ServiceFlags.FPSEquipment },
    { description: "FPS Clothing", icon: "nf nf-fae-shirt", service: ServiceFlags.FPSClothing },

    { description: "Trading", icon: "nf nf-fa-exchange", service: ServiceFlags.Trade },
    { description: "Refining", icon: "nf nf-fa-recycle", service: ServiceFlags.Refinery },
    { description: "Fine Payment", icon: "nf nf-fa-dollar", service: ServiceFlags.Fine },
    { description: "Green/Armistice Zone", icon: "nf nf-mdi-yin_yang", service: ServiceFlags.GreenZone },
    { description: "Security Hacking", icon: "nf nf-fa-edit", service: ServiceFlags.Hacking },
  ];

  filterText: string;

  @Output() onChange: EventEmitter<SystemLocation> = new EventEmitter();

  @ViewChild("location_panel", { static: true }) overlayPanel: OverlayPanel;

  constructor(httpRoute: ActivatedRoute,
    private loadouts: LoadoutManagerService,
    dataStore: DataCacheService,
    cdr: ChangeDetectorRef,
    toast: MessageService) {
    super(httpRoute, dataStore, cdr, toast);
  }

  onAllDataGameUpdated() {
    // Cache data?
    this.browsingNode = this.gameData.LocationKeyMap[this.userData.current_location];
  }

  panelExpanded(event: Event): void {
    this.refreshLocations();
  }

  refreshLocations() {
    var current_loc: SystemLocation = this.browsingNode;
    var parent_loc = current_loc ? current_loc.ParentLocation : null;

    /*
    var rootNode = this.systemLocationToTree(parent_loc, null, false);
    var currentNode = this.systemLocationToTree(current_loc, rootNode, false);
    rootNode.children = [currentNode];
    currentNode.children = current_loc.ChildLocations.map((l) => this.systemLocationToTree(l, currentNode, false));

    this.allOptions = [
      rootNode
    ];
    */

    this.locationOptions = this.gameData.LocationKeyMap["stanton"].ChildLocations
      .filter(sl => sl.IsHidden == false && this.ClientData.SystemBodyLocationTypes.includes(sl.LocationType))
      .map<TreeNode>(sl => this.systemLocationToTree(sl, null, true));
  }

  systemLocationToTree(location: SystemLocation, parent?: TreeNode, recurse?: boolean): TreeNode {
    var children: TreeNode[] = [];
    var node: TreeNode = {
      label: location.Name,
      key: location.Key,
      data: location,
      expanded: false,
      icon: "nf nf-fa-shopping_cart",
      parent: parent
    };

    if (recurse) {
      var childlocations: SystemLocation[] = location.ChildLocations;

      if (location.LocationType == LocationType.Star) {
        // TODO: Group by Planet/Lagrange
      } else if (location.LocationType == LocationType.Planet ||
        location.LocationType == LocationType.Moon) {
      } else {
      }

      childlocations = childlocations.filter(l => l.IsHidden == false && l.LocationType != LocationType.Store);

      node.children = childlocations.map((l) => this.systemLocationToTree(l, node, recurse));
    }

    return node
  }

  bodyChanged(node: TreeNode, location: SystemLocation): void {
    //this.selectedLocation = location;
    console.log(this);
    this.onChange.emit(location);
  }

  onNodeSelect(event: Event | any): void {
    console.log(event);
    var node: TreeNode = event.node;
    var location: SystemLocation = node.data;
    this.browsingNode = location;
    this.refreshLocations();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.selectedShip && changes.selectedShip.currentValue) {
      /*
      const newEq: GameEquipment = changes.selectedEquipment.currentValue;
      if (!this.equipmentOptions) {
        this.equipmentOptions = [newEq];
      }
      */
    }
  }

}


