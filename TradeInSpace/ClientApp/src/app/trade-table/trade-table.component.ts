import { Component, ViewChild, OnInit, ChangeDetectorRef, ViewEncapsulation, ElementRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DataCacheService } from '../../data/data_cache';
import { TradeTable, GameDatabase, SystemLocation, LocationType, ServiceFlags, PadSize, TradeTableEntry, TradeShop, TradeItem, TradeType } from '../../data/generated_models';
import { TreeNode, SortMeta, SelectItem, MessageService } from 'primeng/api';
import { TreeTable } from 'primeng/treetable';
import { Subscription, combineLatest } from 'rxjs';
import { ShowTooltipEvent } from '../../directive/tooltip-attr.directive';
import { SelectButton } from 'primeng/selectbutton';
import { serviceFlagName } from '../../pipes/display_text_pipe';
import { GameDatabaseFull } from '../../data/game_data';
import { utils } from 'protractor';
import { groupSet } from '../../utilities/utilities';
import { Table } from 'primeng/table';
import { BasicUserSettings } from '../../data/models';
import { fmtMoney } from '../../pipes/scaleunit_pipe';
import { TISBaseComponent } from '../tis_base.component';


@Component({
  selector: 'loop-finder',
  templateUrl: './trade-table.component.html',
  styleUrls: ['./trade-table.component.css']
})
export class TradeTableComponent extends TISBaseComponent implements OnInit {
  TradeType: typeof TradeType = TradeType;

  @ViewChild("tradeTable", { static: false }) tradeTable: Table;
  @ViewChild("ttContainer", { static: false }) ttContainer: ElementRef;

  _rows: TTRowGroup[];
  _cols: TTColumnGroup[];
  _flatCols: TTColumnEntry[];
  _flatRows: TTRowEntry[];
  _currentLocation: TTRowEntry = null;


  _frozenCol: any[] = [{
    title: "Location",
    field: "title",
    data: null
  }];

  _colTotal: number;

  cell_width: number = 80;
  cell_height: number = 30;

  constructor(http_route: ActivatedRoute,
    dataStore: DataCacheService,
    cdr: ChangeDetectorRef,
    toast: MessageService) {
    super(http_route, dataStore, cdr, toast);
  }

  ngAfterViewInit(): void {
    window.addEventListener('resize', () => {
      this.fixTable();
    });
  }

  onAllDataTradeUpdated(): void {
    const distinctItems = Array.from(new Set(this.tradeData.Entries.map(tte => tte.Item)));
    const distinctShops = Array.from(new Set(this.tradeData.Entries.map(tte => tte.Station)));

    const groupedItems = groupSet(distinctItems, di => di.Category);
    const groupedShops = groupSet(distinctShops, ds => ds.Location.ParentBody.Key);

    const colTemp: TTColumnGroup[] = [];
    const rowTemp: TTRowGroup[] = [];

    const flatColsTemp: TTColumnEntry[] = [];
    const flatRowsTemp: TTRowEntry[] = [];

    Object.keys(groupedItems).forEach(group => {
      const groupItems: TradeItem[] = groupedItems[group];

      const cols: TTColumnEntry[] = [];
      groupItems.forEach(item => {
        const col: TTColumnEntry = {
          title: item.Name,
          data: item,
          field: item.ID
        };
        cols.push(col);
      });

      const colGrp: TTColumnGroup = {
        title: group,
        columns: cols
      };

      flatColsTemp.push(...cols);
      colTemp.push(colGrp);
    });

    Object.keys(groupedShops).forEach(group => {
      const groupShops: TradeShop[] = groupedShops[group];
      const groupBody = this.gameData.SystemBodies.find(b => b.Key == group);

      const rows: TTRowEntry[] = [];
      groupShops.forEach(shop => {

        const trades: { [id: number]: TradeTableEntry; } = {};
        shop.TradeEntries.forEach(te => trades[te.Item.ID] = te);

        rows.push({
          title: shop.Name,
          data: shop,
          cells: trades
        });

      });

      const row: TTRowGroup = {
        title: groupBody.Name,
        rows: rows
      }

      flatRowsTemp.push(...rows);
      rowTemp.push(row);
    });

    this._cols = colTemp;
    this._rows = rowTemp;
    this._flatCols = flatColsTemp;
    this._flatRows = flatRowsTemp;

    setTimeout(() => {
      this.fixTable();
    }, 10);

    this.cdr.detectChanges();
  }

  userDetailsUpdate(userDetails: BasicUserSettings): void {
    if (userDetails) {
      this._currentLocation = this._flatRows.find(r => r.data.Key == userDetails.current_location);

      this.cdr.detectChanges();
    }
  }

  fixTable(): void {
    if (this.ttContainer) {
      this.tradeTable.scrollHeight = `${this.ttContainer.nativeElement.offsetHeight}px`;
    }
  }

  public getTradeTableTooltip(ttEvent: ShowTooltipEvent, trade: TradeTableEntry): void {
    ttEvent.show = !(!trade);
    if (ttEvent.show) {
      const priceRangeSCU = `${fmtMoney(trade.MinUnitPrice * 100)} - ${fmtMoney(trade.MaxUnitPrice * 100)}/SCU`
      const priceRangeU = `${fmtMoney(trade.MinUnitPrice)} - ${fmtMoney(trade.MaxUnitPrice)}/unit`
      const shopCap = `${fmtMoney(trade.MinUnitPrice * trade.UnitCapacityRate)} - ${fmtMoney(trade.MaxUnitPrice * trade.UnitCapacityRate)} uec/min`

      switch (trade.Type) {
        case TradeType.Buy:
          ttEvent.content = `Buy @ ${priceRangeSCU}<br/>Unit: ${priceRangeU}<br/><br/>Supply Cap: ${shopCap}`
          break;
        case TradeType.Sell:
          ttEvent.content = `Sell @ ${priceRangeSCU}<br/>Unit: ${priceRangeU}<br/><br/>Demand Cap: ${shopCap}`
          break;
      }
    }
  }
}

interface TTColumnGroup {
  title: string;
  columns: TTColumnEntry[];
}

interface TTColumnEntry {
  title: string;
  field: any;
  data: TradeItem;
}

interface TTRowGroup {
  title: string;
  rows: TTRowEntry[];
}

interface TTRowEntry {
  title: string;
  data: TradeShop;
  cells: { [id: number]: TradeTableEntry; }
}

