import { Component, ViewChild, OnInit, ChangeDetectorRef, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { DataCacheService } from '../data/data_cache';
import { GameDatabase, TradeTable } from '../data/generated_models';
import { BasicUserSettings } from '../data/models';
import { GameDatabaseFull } from '../data/game_data';
import { Loadout } from '../data/loadout';
import { Subscription } from 'rxjs';
import { MessageService } from 'primeng/api';
import { ClientData } from '../data/client_data';
import { UiUtils } from '../utilities/ui_utils';
import { DamageMode } from '../data/client_models';


export abstract class TISBaseComponent implements OnInit {
  public gameData: GameDatabaseFull;
  public tradeData: TradeTable;
  public userData: BasicUserSettings;
  protected urlParams: Params;
  protected queryParams: Params;

  private gameDataSub: Subscription;
  private tradeDataSub: Subscription;
  private userDataSub: Subscription;
  private urlParamSub: Subscription;
  private queryParamSub: Subscription;

  public ClientData: typeof ClientData = ClientData;
  public UiUtils: typeof UiUtils = UiUtils;
  public DamageMode: typeof DamageMode = DamageMode;

  constructor(protected http_route: ActivatedRoute,
    protected dataStore: DataCacheService,
    protected cdr: ChangeDetectorRef,
    protected toast: MessageService,
    private name: string = "...") {
    //console.log(`TIS Component<${name}> - Start`);
  }

  //Probably a bit overkill, but explicit logic is nice and easy to debug :^)
  public ngOnInit(): void {
    //console.log(`TIS Component<${this.name}> - OnInit`)

    this.gameDataSub = this.dataStore.gameData.subscribe(gameData => {
      //console.log(`TIS Component<${this.name}> - GameData Loaded`, gameData);
      if (!gameData) return;
      const firstGameData = !this.gameData;

      this.gameData = gameData;

      this.handleFirstData(firstGameData, false, false);
      this.handleDataUpdate(true, false, false);
    });

    this.tradeDataSub = this.dataStore.tradeTable.subscribe(tradeData => {
      //console.log(`TIS Component<${this.name}> - TradeData Loaded`, tradeData);
      if (!tradeData) return;
      const firstTradeData = !this.tradeData;

      this.tradeData = tradeData;

      this.handleFirstData(false, firstTradeData, false);
      this.handleDataUpdate(false, true, false);
    });

    this.userDataSub = this.dataStore.userDetails.subscribe(userData => {
      //console.log(`TIS Component<${this.name}> - UserData Loaded`, userData);
      if (!userData) return;
      const firstUserData = !this.userData;

      this.userData = userData;

      this.handleFirstData(false, false, firstUserData);
      this.handleDataUpdate(false, false, true);
    });
  }

  public ngOnDestroy(): void {
    if (this.gameDataSub) this.gameDataSub.unsubscribe();
    if (this.tradeDataSub) this.tradeDataSub.unsubscribe();
    if (this.userDataSub) this.userDataSub.unsubscribe();
    if (this.urlParamSub) this.urlParamSub.unsubscribe();
    if (this.queryParamSub) this.queryParamSub.unsubscribe();
  }

  private setupURLParameters(): void {
    this.urlParamSub = this.http_route.params.subscribe(urlParams => {
      this.urlParams = urlParams;

      this.onUrlParamsLoaded();
    });

    this.queryParamSub = this.http_route.queryParams.subscribe(qParams => {
      this.queryParams = qParams;

      this.onQueryParamsLoaded();
    });
  }

  private handleFirstData(firstGame: boolean, firstTrade: boolean, firstUser: boolean): void {
    if (firstGame) this.onFirstGameDataLoaded();
    if (firstTrade) this.onFirstTradeDataLoaded();
    if (firstUser) this.onFirstUserDataLoaded();
    const anyFirst = firstGame || firstTrade || firstUser;

    if ((firstGame || firstTrade)) {
      if (this.gameLoaded()) this.onGameTradeDataReady();
    }

    if (this.allLoaded()) {
      if (anyFirst) {
        //console.log(`TIS Component<${name}> - All data ready");
        this.onAllDataReady();
        this.onAllDataGameUpdated();
        this.onAllDataTradeUpdated();
        this.onAllDataUserUpdated();
        //Do this last so any views that depend on data have it loaded already.
        //Not really needed before that
        if (this.http_route) this.setupURLParameters();
      }
    }
  }

  private handleDataUpdate(newGame: boolean, newTrade: boolean, newUser: boolean): void {
    if (newGame) this.onGameDataUpdated();
    if (newTrade) this.onTradeDataUpdated();
    if (newUser) this.onUserDataUpdated();

    if ((newGame || newTrade) && this.gameLoaded())
      this.onGameTradeUpdated();

    if (this.allLoaded()) {
      this.onAllDataUpdated();

      if(newGame) this.onAllDataGameUpdated();
      if(newTrade) this.onAllDataTradeUpdated();
      if(newUser) this.onAllDataUserUpdated();
    }
  }

  protected gameLoaded(): boolean {
    return !(!this.gameData) && !(!this.tradeData);
  }

  protected allLoaded(): boolean {
    return !(!this.gameData) && !(!this.tradeData) && !(!this.userData);
  }

  protected onFirstGameDataLoaded():void { }
  protected onFirstTradeDataLoaded(): void { }
  protected onFirstUserDataLoaded(): void { }

  protected onGameDataUpdated(): void { }
  protected onTradeDataUpdated(): void { }
  protected onUserDataUpdated(): void { }

  protected onGameTradeDataReady(): void { }
  protected onGameTradeUpdated(): void { }
  protected onAllDataReady(): void { }
  protected onAllDataUpdated(): void { }

  protected onUrlParamsLoaded(): void { }
  protected onQueryParamsLoaded(): void { }

  protected onAllDataGameUpdated(): void { }
  protected onAllDataTradeUpdated(): void { }
  protected onAllDataUserUpdated(): void { }
}
