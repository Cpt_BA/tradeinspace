import { Component,  Input, ViewEncapsulation, Output, EventEmitter, OnInit, ChangeDetectorRef } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { TradeShop, TradeItem, SystemBody, GameDatabase, PadSize } from '../../../data/generated_models';
import { CalculatedRoute, FilterParams, PreferenceEnum, LoopFilterParams } from '../../../data/client_models';
import { RouteCalculatorService } from '../../../data/route_calculator';
import { TreeNode, MenuItem, SelectItem, MessageService } from 'primeng/api'
import { Observable, BehaviorSubject } from 'rxjs';
import { KeyModel } from '../../../data/models';
import { findReadVarNames } from '@angular/compiler/src/output/output_ast';
import { DataCacheService } from '../../../data/data_cache';
import { groupSet, TreeNodeMaker, TreeGroup, treeFromGroupData, treeFromData, treeFromFlatData } from '../../../utilities/utilities';
import { Tree } from 'primeng/tree';
import { ClientData } from '../../../data/client_data';
import { UiUtils } from '../../../utilities/ui_utils';
import { GameDatabaseFull } from '../../../data/game_data';
import { TISBaseComponent } from '../../tis_base.component';
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'loop-filter',
  templateUrl: './loop-filter.component.html',
  styleUrls: ['./loop-filter.component.css']
})
export class LoopFilterComponent extends TISBaseComponent implements OnInit {
  UiUtils: typeof UiUtils = UiUtils;
  ClientData: typeof ClientData = ClientData;
  
  @Input() filter_settings: LoopFilterParams;

  @Output() filtersChanged: EventEmitter<LoopFilterParams> = new EventEmitter<LoopFilterParams>();
  filterThrottle: BehaviorSubject<LoopFilterParams>;

  all_item_keys: string[];
  all_station_keys: string[];

  filter_items: TreeNode[];
  filter_locations: TreeNode[];

  selected_items: TreeNode[] = [];
  selected_locations: TreeNode[] = [];

  make_item_category_treenode: TreeNodeMaker<TreeGroup<TradeItem>> = (has_children, item_group) => {
    return {
      label: item_group.group,
      collapsedIcon: "nf nf-fa-folder",
      expandedIcon: "nf nf-fa-folder_open"
    };
  };

  make_item_treenode: TreeNodeMaker<TradeItem> = (has_children, item) => {
    return {
      label: item.Name,
      data: item,
      collapsedIcon: "pi pi-shopping-cart",
      expandedIcon: "pi pi-shopping-cart"
    };
  }

  make_station_treenode: TreeNodeMaker<TradeShop> = (has_children, station) => {
    return {
      label: station.Name,
      data: station,
      collapsedIcon: "pi pi-home",
      expandedIcon: "pi pi-home"
    };
  };

  make_body_treenode: TreeNodeMaker<SystemBody> = (has_children, body) => {
    return {
      label: body.Name,
      data: body,
      collapsedIcon: "pi pi-globe",
      expandedIcon: "pi pi-globe"
    };
  };

  constructor(http_route: ActivatedRoute,
    dataStore: DataCacheService,
    cdr: ChangeDetectorRef,
    toast: MessageService) {
    super(http_route, dataStore, cdr, toast);

    this.filter_settings = {
      item_keys: [],
      location_keys: [],
      minimum_pad_size: null,

      allow_planet: true,
      allow_hidden: true,
      inclusive_item: true,
      inclusive_location: true,

      buy_time_max: 0,
      transit_time_max: 0,
      sell_time_max: 0
    }

    this.filterThrottle = new BehaviorSubject(this.filter_settings);

    this.filterThrottle
      .debounceTime(500)
      .subscribe(newSettings => {
        this.filtersChanged.emit(newSettings);
    });
  }

  onGameTradeUpdated(): void {
    this.all_item_keys = Array.from(new Set(this.tradeData.Entries.map(tte => tte.Item))).map(item => item.Key);
    this.all_station_keys = Array.from(new Set(this.tradeData.Entries.map(tte => tte.Station))).map(station => station.Key);

    this.filter_items = UiUtils.createItemTree(this.gameData, ti => this.all_item_keys.includes(ti.Key));
    this.filter_locations = UiUtils.createBodyTree(this.gameData, null, true);

    this.loadTreeFilters();
  }

  ngAfterViewInit(): void {
    this.updateTreeFilters();
  }

  loadTreeFilters(): void {
    this.selected_items = this.findRecursive(this.filter_items, (item: TreeNode) => item.data && this.filter_settings.item_keys.includes((<KeyModel>item.data).Key));
    this.selected_locations = this.findRecursive(this.filter_locations, (item: TreeNode) => item.data && this.filter_settings.location_keys.includes((<KeyModel>item.data).Key));

    this.updateTreeCheckmarks();
  }

  updateTreeCheckmarks(): void {
    this.filter_items.forEach(n => this.markDownwards(n, this.selected_items));
    this.filter_locations.forEach(n => this.markDownwards(n, this.selected_locations));
  }

  selectAll(tree: Tree, selected_list: TreeNode[]): void {
    selected_list.length = 0;
    tree.getRootNode().forEach(n => this.addRecurse(n, selected_list));

    this.updateTreeCheckmarks();
    this.updateTreeFilters();
  }

  unselectAll(tree: Tree, selected_list: TreeNode[]): void {
    selected_list.length = 0;
    tree.selection = [];

    this.updateTreeCheckmarks();
    this.updateTreeFilters();
  }

  private addRecurse(node: TreeNode, selectedNodes: TreeNode[]): void {
    selectedNodes.push(node);
    if(node.children) node.children.forEach(n => this.addRecurse(n, selectedNodes));
  }

  private markDownwards(node: TreeNode, selectedNodes: TreeNode[]): void {
    //Depth first
    node.children.forEach(n => {
      if (n.children && n.children.length > 0) {
        this.markDownwards(n, selectedNodes);
      }
    })

    var selectedChildren: number = node.children.filter(n => selectedNodes.includes(n)).length;
    var childCount = node.children.length;

    if (selectedChildren == 0) {
      //Nothing
    } else if (selectedChildren == childCount) {
      //Allchildren, add node itself
      selectedNodes.push(node);
    } else {
      //Partial
      node.partialSelected = true;
    }
  }

  private findRecursive(nodes: TreeNode[], fnCheckItem: (item: any) => boolean): TreeNode[] {
    var results:TreeNode[] = [];
    nodes.forEach(node => {
      if (fnCheckItem(node)) results.push(node);

      if (node.children) {
        this.findRecursive(node.children, fnCheckItem).forEach(c => results.push(c));
      }
    });
    return results;
  }

  updateTreeFilters(): void {
    this.filter_settings.item_keys = this.selected_items
      .filter(item => item.data)
      .map(item => (<KeyModel>item.data).Key);

    this.filter_settings.location_keys = this.selected_locations
      .filter(station => station.data)
      .map(station => (<KeyModel>station.data).Key);

    this.filterChanged();
  }

  filterChanged(): void {
    this.filterThrottle.next(this.filter_settings);
  }

  createPreferenceOptions(labels: string[], onClicked: (update: number) => void): MenuItem[] {
    return labels.map((val) => { return { label: val, command: (event: any) => { onClicked(event.index); } } });
  }


}
