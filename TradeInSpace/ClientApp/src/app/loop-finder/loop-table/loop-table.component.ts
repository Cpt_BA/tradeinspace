import { Component, Inject, Input, ElementRef, OnChanges, SimpleChanges, SimpleChange, OnInit, ViewChild, EventEmitter, Output } from '@angular/core';
import { CalculatedLoop } from '../../../data/loop_models';

@Component({
  selector: 'loop-table',
  templateUrl: './loop-table.component.html',
  styleUrls: ['./loop-table.component.css']
})
export class LoopTableComponent {

  @Input() available_loops: CalculatedLoop[];

  constructor() {
  }

  public updateFilters() {
  }

  ngAfterViewInit(): void {
  }

}
