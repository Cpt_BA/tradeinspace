import { Component, ViewEncapsulation, OnInit, ViewChild, Input } from '@angular/core';
import { Table } from 'primeng/table';
import { ServiceFlags, TradeType } from '../../../data/generated_models';
import { CalculatedLoop, CalculatedLoopSubstep, CalculatedSubstepEntry, CalculatedLoopResource } from '../../../data/loop_models';
import { ShowTooltipEvent } from '../../../directive/tooltip-attr.directive';
import { fmtMoney, fmtUnits } from '../../../pipes/scaleunit_pipe';


@Component({
  selector: 'loop-record',
  templateUrl: './loop-record.component.html',
  styleUrls: ['./loop-record.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class LoopRecordComponent implements OnInit {
  TradeType: typeof TradeType = TradeType;
  Math: typeof Math = Math;

  @Input() model: CalculatedLoop;

  @ViewChild("recordTable", { static: true }) dataTable: Table;

  ngOnInit(): void {
  }

  public getLoopResourceStepTooltip(ttEvent: ShowTooltipEvent, substep: CalculatedSubstepEntry): void {
    ttEvent.show = !(!substep);
    if (ttEvent.show) {
      const priceRangeSCU = `${fmtMoney(substep.trade.MinUnitPrice * 100)} - ${fmtMoney(substep.trade.MaxUnitPrice * 100)}/SCU`
      const priceRangeU = `${fmtMoney(substep.trade.MinUnitPrice)} - ${fmtMoney(substep.trade.MaxUnitPrice)}/unit`
      const shopCap = `${fmtMoney(substep.trade.MinUnitPrice * substep.trade.UnitCapacityRate)} - ${fmtMoney(substep.trade.MaxUnitPrice * substep.trade.UnitCapacityRate)} uec/min`

      switch (substep.trade.Type) {
        case TradeType.Buy:
          ttEvent.content = `Buy @ ${priceRangeSCU}<br/>Unit: ${priceRangeU}<br/><br/>Supply Cap: ${shopCap}`
          break;
        case TradeType.Sell:
          ttEvent.content = `Sell @ ${priceRangeSCU}<br/>Unit: ${priceRangeU}<br/><br/>Demand Cap: ${shopCap}`
          break;
      }
    }
  }

  public getLoopResourceTooltip(ttEvent: ShowTooltipEvent, resource: CalculatedLoopResource): void {
    ttEvent.show = !(!resource);
    if (ttEvent.show) {

      //TODO: Setting for slowest globally vs. slowest site of all
      const slowestRate = Math.min(resource.sell_rate, resource.buy_rate); //units/m

      const lines: string[] = [
        `Avg Buy @ ${fmtMoney(resource.avg_buy * 100)} uec/SCU`,
        `Supply Cap: ${fmtUnits(resource.buy_rate)}/M`,
        `Supply Cap: ${fmtMoney(resource.buy_rate * resource.avg_buy)} uec/M`,
        ``,
        `Avg Sell @ ${fmtMoney(resource.avg_sell * 100)} uec/SCU`,
        `Demand Cap: ${fmtUnits(resource.sell_rate)}/M`,
        `Demand Cap: ${fmtMoney(resource.sell_rate * resource.avg_sell)} uec/M`,
        ``,
        `Avg Profit @ ${fmtMoney(resource.unit_profit_avg * 100)} uec/SCU`,
        `Avg Profit Cap: ${fmtMoney(resource.unit_profit_avg * slowestRate)} uec/M`,
        `Max Profit Cap: ${fmtMoney(resource.unit_profit_max * slowestRate)} uec/M`
      ];

      ttEvent.content = lines.join("<br/>");
    }
  }

}
