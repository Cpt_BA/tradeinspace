import { Component, ViewChild, OnInit, ChangeDetectorRef } from '@angular/core';

import { ActivatedRoute } from '@angular/router';
import { RouteCalculatorService } from '../../data/route_calculator';
import { DataCacheService } from '../../data/data_cache';
import { GameDatabase, SystemBody, TradeTable, TradeTableEntry, TradeType, ShopType, TradeItem, BodyType } from '../../data/generated_models';
import { permuteOptions } from '../../utilities/utilities';
import { BaseLoop, BodyTradeDetails, LoopFilterParams } from '../../data/client_models';
import { LoopFilterComponent } from './loop-filter/loop-filter.component';
import { LoopTableComponent } from './loop-table/loop-table.component';
import { CalculatedLoop } from '../../data/loop_models';
import { GameDatabaseFull } from '../../data/game_data';
import { ClientData } from '../../data/client_data';
import { ModelUtils } from '../../utilities/model_utils';
import { TISBaseComponent } from '../tis_base.component';
import { MessageService } from 'primeng/api';

@Component({
  selector: 'loop-finder',
  templateUrl: './loop-finder.component.html',
  styleUrls: ['./loop-finder.component.css']
})
export class LoopFinderComponent extends TISBaseComponent implements OnInit {

  calculated_loops: CalculatedLoop[];

  settings: LoopFilterParams;

  @ViewChild("lFilter", { static: true }) loopFilter: LoopFilterComponent;
  @ViewChild("lTable", { static: true }) loopTable: LoopTableComponent;

  constructor(http_route: ActivatedRoute,
    dataStore: DataCacheService,
    cdr: ChangeDetectorRef,
    toast: MessageService) {
    super(http_route, dataStore, cdr, toast);
  }

  onAllDataTradeUpdated(): void {
    this.calculateLoops();
  }

  preferenceChanged(): void {

  }

  filterChanged(newSettings: LoopFilterParams): void {
    this.settings = newSettings;
    this.calculateLoops();
  }

  calculateLoops(): void {
    if (!this.settings || !this.gameData) return;
    this.calculated_loops = [];

    console.log("Loop - calculate", this.gameData);

    const fnFilterTrade = (trade: TradeTableEntry) => {
      if (this.settings.item_keys.length > 0) {
        if (this.settings.inclusive_item) {
          if (!this.settings.item_keys.includes(trade.Item.Key)) return false;
        }
      }

      if (this.settings.minimum_pad_size) {
        if (!ModelUtils.locationHasPad(trade.Station.Location, this.settings.minimum_pad_size)) {
          return false;
        }
      }

      return true;
    };

    const fnFilterBody = (body: SystemBody) => {
      if (!this.settings.allow_hidden && body.SystemLocation.IsHidden) {
        return false;
      }

      if (!this.settings.allow_planet) {
        if (body.BodyType == BodyType.Planet || body.BodyType == BodyType.Moon) {
          return false;
        }
      }

      if (this.settings.location_keys.length > 0) {
        if (this.settings.inclusive_location) {
          if (!this.settings.location_keys.includes(body.Key)) return false;
        }
      }

      return true;
    };

    //Find all buy/sell trades by system body.
    const bodyTrades: BodyTradeDetails[] = this.gameData.SystemBodies
      .filter(fnFilterBody)
      .map(b => {
        const allBodyTrades = this.tradeData.Entries
          .filter(td => !td.BadTrade)
          .filter(td => td.Station.Location.ParentBody == b.SystemLocation)
          .filter(td => ClientData.TradeShopTypes.includes(td.Station.ShopType));
      return {
        body: b,
        buy: allBodyTrades.filter(fnFilterTrade).filter(bt => bt.Type == TradeType.Buy),
        sell: allBodyTrades.filter(fnFilterTrade).filter(bt => bt.Type == TradeType.Sell)
      };
    }).filter(bt => bt.buy.length > 0 || bt.sell.length > 0);

    //Map them by body ID for efficient lookup
    const bodyLookup: { [bodyId: number]: BodyTradeDetails } = {};
    bodyTrades.forEach(bt => bodyLookup[bt.body.ID] = bt);

    const bodyOptions = bodyTrades.map(bt => bt.body.ID);

    console.log(`Calculating permutations for ${bodyOptions.length} bodies`);

    const testLoops: number[][] = [...permuteOptions(bodyOptions, 2)];
    const outLoops: BaseLoop[] = [];

    testLoops.forEach((planetIDs, idx) => {
      const availableBuys = {}, availableSells = {};
      let buys = 0, sells = 0;

      //Handling of non inclusive_location path
      if (!this.settings.inclusive_location && this.settings.location_keys.length > 0) {
        if (!planetIDs.some(p => this.settings.location_keys.includes(bodyLookup[p].body.Key))) {
          return;
        }
      }

      planetIDs.forEach((bodyID) => {
        const bodyTrades = bodyLookup[bodyID];
        bodyTrades.buy.forEach(t => availableBuys[t.ItemID] = (availableBuys[t.ItemID] || 0) + 1);
        bodyTrades.sell.forEach(t => availableSells[t.ItemID] = (availableSells[t.ItemID] || 0) + 1);
        buys += bodyTrades.buy.length;
        sells += bodyTrades.sell.length;
      });


      if (buys == 0 || sells == 0) return;

      //Filter only to items that have both buy and sell entries.
      const tradeableItemIDs = this.gameData.TradeItems.filter(item => availableBuys[item.ID] > 0 && availableSells[item.ID] > 0).map(item => item.ID);


      //Handling of non inclusive_item path
      if (!this.settings.inclusive_item && this.settings.item_keys.length > 0) {
        if (!tradeableItemIDs.some(itemID => {
          const item = this.gameData.ItemMap[itemID];
          return this.settings.item_keys.includes(item.Key);
        })) {
          return;
        }
      }

      if (tradeableItemIDs.length == 0) return;

      let buyTrades: TradeTableEntry[] = [], sellTrades: TradeTableEntry[] = [];
      let rates: { [id: string]: { buy_rate: number; buy_price: number; sell_rate: number; sell_price: number } } = {};

      tradeableItemIDs.forEach(ti => rates[ti] = { buy_rate: 0, buy_price: 0, sell_rate: 0, sell_price: 0 });

      let skip:boolean = false;

      planetIDs.forEach((bodyID) => {
        const bodyTrades = bodyLookup[bodyID];
        const bodyBuys = [].concat(...bodyTrades.buy.filter(b => tradeableItemIDs.includes(b.ItemID)))
        const bodySells = [].concat(...bodyTrades.sell.filter(b => tradeableItemIDs.includes(b.ItemID)));

        if ((bodyBuys.length == 0 && bodySells.length == 0) || skip) {
          skip = true;
          return;
        }

        buyTrades = buyTrades.concat(bodyBuys);
        sellTrades = sellTrades.concat(bodySells);
      });

      if (skip) return;

      buyTrades.forEach(bt => {
        rates[bt.ItemID].buy_rate += bt.UnitCapacityRate;
        rates[bt.ItemID].buy_price += ((bt.MinUnitPrice + bt.MaxUnitPrice) / 2) * bt.UnitCapacityRate;
      });
      sellTrades.forEach(st => {
        rates[st.ItemID].sell_rate += st.UnitCapacityRate;
        rates[st.ItemID].sell_price += ((st.MinUnitPrice + st.MaxUnitPrice) / 2) * st.UnitCapacityRate;
      });

      /*
        *  TODO: More pre-filtering here...
        */

      outLoops.push({
        body_ids: planetIDs,
        item_ids: tradeableItemIDs
      });
    });

    console.log(`${testLoops.length} body permutations. ${outLoops.length} Loops`);

    this.calculated_loops = outLoops.map(l => new CalculatedLoop(this.gameData, l));
  }
  
}


