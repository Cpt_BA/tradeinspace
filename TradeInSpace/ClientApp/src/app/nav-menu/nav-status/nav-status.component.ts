import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { DataCacheService } from '../../../data/data_cache';
import { GameDatabase, GameShip, TradeShop, SystemLocation, SystemBody } from '../../../data/generated_models';
import { BasicUserSettings } from '../../../data/models';
import { Observable, BehaviorSubject, Subject, Subscription } from 'rxjs-compat';
import { SelectItemGroup, SelectItem, MessageService } from 'primeng/api';
import { groupSet, Group, pointDistance, locationDistance } from '../../../utilities/utilities';
import { Spinner } from 'primeng/spinner';
import { ClientData } from '../../../data/client_data';
import { UiUtils } from '../../../utilities/ui_utils';
import { GameDatabaseFull } from '../../../data/game_data';
import { ShoppingCartManagerService } from '../../../data/shopping_cart_manager';
import { TISBaseComponent } from '../../tis_base.component';
import { ActivatedRoute, Router } from '@angular/router';
import { LoadoutManagerService } from '../../../data/loadout_manager';
import { Loadout } from '../../../data/loadout';
import { CoordinateResult, ModelUtils } from '../../../utilities/model_utils';
import { Coordinate, ObjectContainer } from "../../../data/coordinates";

@Component({
  selector: 'nav-status',
  templateUrl: './nav-status.component.html',
  styleUrls: ['./nav-status.component.css']
})
export class NavStatusComponent extends TISBaseComponent implements OnInit {

  public ships: GameShip[];
  public shops: TradeShop[];

  public allShipOptions: SelectItemGroup[];
  public allLocations: SelectItemGroup[];
  public myShipOptions: SelectItemGroup[];

  public selectedShip: DropdownShip;
  public selectedLocation: TradeShop;

  private balanceThrottle: Subject<number>;
  private activeLoadoutSub: Subscription;

  private dataError: Error = null;

  constructor(httpRoute: ActivatedRoute,
    dataStore: DataCacheService,
    toast: MessageService,
    public router: Router,
    public cart: ShoppingCartManagerService,
    public loadoutMgr: LoadoutManagerService)
  {
    super(httpRoute, dataStore, null, toast, "User Details");
    //Hack to throttle rate spinner triggers updates
    this.balanceThrottle = new Subject<number>();
    this.balanceThrottle.throttleTime(1000).subscribe(() => { this.triggerUpdate(); });
  }

  onAllDataReady(): void {
    this.activeLoadoutSub = this.loadoutMgr.activeLoadout.subscribe(al => {
      if (al)
        this.selectedShip = { key: al.key, ship: this.gameData.ShipKeyMap[al.equipment.Key], favorite: this.checkFavorite(al.key) }
      else
        this.selectedShip = null;
    });
  }

  onAllDataGameUpdated(): void {
    this.ships = this.gameData.Ships;
    this.shops = this.gameData.TradeShops;

    this.buildLocationOptions();
    this.buildShipOptions();
  }

  onAllDataUserUpdated(): void {
    this.selectedLocation = this.gameData.ShopKeyMap[this.userData.current_location];
  }

  buildShipOptions(): void {
    this.allShipOptions = [];

    const mfgGrouped = groupSet<GameShip>(this.ships.filter(s => s.CargoCapacity > 0), s => s.Manufacturer);

    if (this.userData) {
      if (this.userData.owned_ships && this.userData.owned_ships.length > 0) {
        const myFavorites = this.userData.owned_ships.map(s => {
          const customLoadout = this.loadoutMgr.getCustomLoadout(s);
          if (customLoadout) {
            return this.loadoutToComboItem(customLoadout);
          } else {
            const ship = this.gameData.ShipKeyMap[s];
            if (!ship) return null;
            return this.shipToComboItem(ship);
          }
        }).filter(s => !(!s));
        if (myFavorites.length > 0)
          this.allShipOptions.push({ label: "Favorite Ships", items: myFavorites });
      }

      const customShips = this.loadoutMgr.getCustomLoadouts();
      if (customShips.length > 0) {
        this.allShipOptions.push({ label: "My Hangar", items: customShips.map(l => this.loadoutToComboItem(l)) });
      }
    }

    Object.keys(mfgGrouped).forEach(mfg => {
      this.allShipOptions.push({ label: `${mfg}`, items: mfgGrouped[mfg].map(s => this.shipToComboItem(s)) });
    });
  }

  buildLocationOptions(): void {
    this.allLocations = [];

    const locationsGrouped = groupSet<TradeShop>(this.shops.filter(s => ClientData.TradeShopTypes.includes(s.ShopType)), s => s.Location.ParentBody.Key);

    const bodyKeys = Object.keys(locationsGrouped);
    const sortedKeys = bodyKeys.map(k => this.gameData.BodyKeyMap[k]).sort(UiUtils.bodySort).map(b => b.Key);

    sortedKeys.forEach(bodyLocationKey => {
      this.allLocations.push(this.locationToComboGroup(this.gameData.BodyKeyMap[bodyLocationKey], locationsGrouped[bodyLocationKey]));
    });
  }

  loadoutToComboItem(loadout: Loadout): SelectItem {
    const ship = this.gameData.ShipKeyMap[loadout.equipment.Key];
    return {
      label: `${loadout.name} [${loadout.equipment.Name}]`,
      value: { key: loadout.key, ship: ship, favorite: this.checkFavorite(loadout.key)},
      title: loadout.equipment.Name
    }
  }

  shipToComboItem(ship: GameShip): SelectItem {
    return {
      label: ship.Name,
      value: { key: ship.Key, ship: ship, favorite: this.checkFavorite(ship.Key) },
      title: ship.Description
    }
  }

  stationToComboItem(station: TradeShop): SelectItem | any {
    return {
      label: station.Name,
      value: station,
      title: station.Name,
      body_name: station.Location.ParentBody.Name
    }
  }

  locationToComboGroup(body: SystemBody, shops: TradeShop[]): SelectItemGroup {
    return {
      label: body.Name,
      value: body,
      items: shops.map(this.stationToComboItem)
    }
  }

  checkFavorite(loadoutKey: string): boolean {
    return this.userData.owned_ships.includes(loadoutKey);
  }

  toggleFavorite(shipItem: DropdownShip, event: Event): void {
    event.preventDefault();
    shipItem.favorite = !shipItem.favorite;
    if (this.userData.owned_ships.includes(shipItem.key))
      this.userData.owned_ships.splice(this.userData.owned_ships.indexOf(shipItem.key), 1)
    else
      this.userData.owned_ships.push(shipItem.key);
    this.buildShipOptions();
  }

  shipChanged(event: Event): void {
    const loadout: DropdownShip = (event as any).value;
    //Bit of a hack, TODO: Better solution
    this.userData.active_ship = loadout.key;
    this.genericChanged(event);
  }

  balanceChanged(event: Event): void {
    this.balanceThrottle.next(this.userData.balance);
  }

  locationChanged(event: Event): void {
    const eventLocation = (event as any).value as SystemLocation;
    this.userData.current_location = eventLocation.Key;
    this.genericChanged(event);
  }

  genericChanged(event: Event): void {
    this.triggerUpdate();
  }

  locatePlayer(event: Event): void {

    navigator.clipboard.readText()
      .then(text => {
        console.log("Process input: ", text);
        let results = ModelUtils.doLocate(this.gameData, text);
        if (results) {
          console.log("Result: ", results);
        }
      })
      .catch(err => {
        console.log("Clipboard read error", err);
      });
  }

  private triggerUpdate(): void {
    this.dataStore.setUserDetails(this.userData, true);
  }
}

interface DropdownGroup<T> {
  label: string;
  items: T[];
}

interface DropdownShip {
  key: string;
  ship: GameShip;
  favorite: boolean;
}
