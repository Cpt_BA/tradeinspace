import { Component, ChangeDetectorRef, OnInit } from '@angular/core';
import { MenuItem } from 'primeng/api';
import { Router, NavigationEnd } from '@angular/router';
import { environment } from '../../environments/environment';
import { SystemBody, BodyType } from '../../data/generated_models';
import { DataCacheService } from '../../data/data_cache';

@Component({
  selector: 'app-nav-menu',
  templateUrl: './nav-menu.component.html',
  styleUrls: ['./nav-menu.component.css']
})
export class NavMenuComponent implements OnInit {
  isExpanded = false;

  menu_items: MenuItem[] = [
    {
      label: "Trade Routes",
      icon: "nf nf-fa-exchange",
      items: [
        {
          label: "Trade Finder",
          icon: "nf nf-fa-send",
          expanded: true,
          items: [
            {
              label: "Highest UEC/m",
              icon: "nf nf-fa-hourglass_start",
              routerLink: ["/trade", "route-finder", "default"],
              routerLinkActiveOptions: { exact: true }
            },
            {
              label: "Best Profit %",
              icon: "nf nf-fa-signal",
              routerLink: ["/trade", "route-finder", "pct_profit"],
              routerLinkActiveOptions: { exact: true }
            },
            {
              label: "Most Profit Per-Run",
              icon: "nf nf-fa-usd",
              routerLink: ["/trade", "route-finder", "uec_profit"],
              routerLinkActiveOptions: { exact: true }
            },
            {
              label: "Fastest",
              icon: "nf nf-fa-clock_o",
              routerLink: ["/trade", "route-finder", "speed"],
              routerLinkActiveOptions: { exact: true }
            },
            {
              label: "Convenient",
              icon: "nf nf-fa-magnet",
              routerLink: ["/trade", "route-finder", "convenient"],
              routerLinkActiveOptions: { exact: true }
            },
            {
              label: "Long Haul",
              icon: "nf nf-mdi-airplane",
              routerLink: ["/trade", "route-finder", "qt_distance"],
              routerLinkActiveOptions: { exact: true }
            },
            {
              label: "Saved",
              icon: "nf nf-oct-settings",
              routerLink: ["/trade", "route-finder", "custom"],
              routerLinkActiveOptions: { exact: true }
            }
          ]
        },
        {
          label: "Trade Table",
          icon: "nf nf-fa-table",
          routerLink: ["/trade", "trade-table"],
          routerLinkActiveOptions: { exact: true }
        },
      ]
    },
    {
      label: "Maps",
      icon: "nf nf-fa-map",
      items: [],
      visible: true
    },
    {
      label: "Missions",
      icon: "nf nf-fa-group",
      items: [
        {
          label: "Missions",
          icon: "nf nf-dev-git",
          routerLink: ["/mission"],
          routerLinkActiveOptions: { exact: true }
        },
        {
          label: "Mission Givers",
          icon: "nf nf-custom-folder_git",
          routerLink: ["/giver"],
          routerLinkActiveOptions: { exact: true }
        },
      ]
    },
    {
      visible: false,
      label: "Tools",
      icon: "nf nf-mdi-wrench",
      items: [
        {
          label: "QD Calculator",
          icon: "nf nf-mdi-clock_start",
          routerLink: ["/tools", "qd"],
          routerLinkActiveOptions: { exact: true }
        }
      ]
    },
    {
      label: "Lists / Raw Data",
      icon: "nf nf-fa-th_list",
      items: [
        {
          label: "Trade Table",
          icon: "nf nf-fa-table",
          routerLink: ["/trade", "trade-table"],
          routerLinkActiveOptions: { exact: true },
          visible: false
        },
        {
          label: "Commodities",
          icon: "nf nf-fa-table",
          routerLink: ["/raw", "commodity"],
          routerLinkActiveOptions: { exact: true }
        },
        {
          label: "Items/Equipment",
          icon: "nf nf-fa-table",
          routerLink: ["/raw", "item"],
          routerLinkActiveOptions: { exact: true }
        },
        {
          label: "Locations",
          icon: "nf nf-fa-table",
          routerLink: ["/raw", "location"],
          routerLinkActiveOptions: { exact: true }
        },
        {
          label: "Routes",
          icon: "nf nf-fa-table",
          routerLink: ["/raw", "route"],
          routerLinkActiveOptions: { exact: true }
        },
        {
          label: "Vehicles",
          icon: "nf nf-fa-table",
          routerLink: ["/raw", "vehicle"],
          routerLinkActiveOptions: { exact: true }
        }
      ]
    },
    {
      label: "Ship Equipment",
      icon: "nf nf-fa-wrench",
      items: [
        {
          label: "Quantum Drive",
          icon: "nf nf-mdi-fast_forward_outline",
          routerLink: ["/equipment", "qdrive"],
          routerLinkActiveOptions: { exact: true }
        },
        {
          label: "Mining Laser",
          icon: "nf nf-oct-ruby",
          routerLink: ["/equipment", "mininglaser"],
          routerLinkActiveOptions: { exact: true }
        },
        {
          label: "Cooler",
          icon: "nf nf-mdi-snowflake",
          routerLink: ["/equipment", "cooler"],
          routerLinkActiveOptions: { exact: true }
        },
        {
          label: "Shield",
          icon: "nf nf-mdi-shield_outline",
          routerLink: ["/equipment", "shield"],
          routerLinkActiveOptions: { exact: true }
        },
        {
          label: "Weapon",
          icon: "nf nf-mdi-pistol",
          routerLink: ["/equipment", "weapon"],
          routerLinkActiveOptions: { exact: true }
        },
        {
          label: "Power Plant",
          icon: "nf nf-weather-lightning",
          routerLink: ["/equipment", "powerplant"],
          routerLinkActiveOptions: { exact: true }
        },
        {
          label: "Missiles",
          icon: "nf nf-oct-rocket",
          routerLink: ["/equipment", "missile"],
          routerLinkActiveOptions: { exact: true }
        },
        {
          label: "Paint",
          icon: "nf nf-oct-paintcan",
          routerLink: ["/equipment", "paint"],
          routerLinkActiveOptions: { exact: true }
        }
      ]
    },
    {
      label: "FPS Equipment",
      icon: "nf nf-oct-person",
      items: [
        {
          label: "Food & Drink",
          icon: "nf nf-fae-hamburger",
          routerLink: ["/fps_equipment", "food"],
          routerLinkActiveOptions: { exact: true }
        },
        {
          label: "Undersuit",
          icon: "nf nf-mdi-human_handsup",
          routerLink: ["/fps_equipment", "undersuit"],
          routerLinkActiveOptions: { exact: true }
        },
        {
          label: "Armor",
          icon: "nf nf-fae-layers",
          routerLink: ["/fps_equipment", "armor"],
          routerLinkActiveOptions: { exact: true }
        },
        {
          label: "Weapons",
          icon: "nf nf-fa-crosshairs",
          routerLink: ["/fps_equipment", "weapon"],
          routerLinkActiveOptions: { exact: true }
        },
        {
          label: "Clothing",
          icon: "nf nf-fae-shirt",
          routerLink: ["/fps_equipment", "clothing"],
          routerLinkActiveOptions: { exact: true }
        },
        {
          label: "Other",
          icon: "nf nf-mdi-dots_horizontal",
          routerLink: ["/fps_equipment", "other"],
          routerLinkActiveOptions: { exact: true }
        }
      ]
    },
    {
      label: "Ships",
      icon: "nf nf-mdi-rocket",
      items: [
        {
          label: "3D Maps (New!)",
          icon: 'nf nf-mdi-eye',
          routerLink: ["/ship_maps"],
          routerLinkActiveOptions: { exact: false }
        },
        {
          label: "Ship Rentals",
          icon: "nf nf-mdi-clock_start",
          routerLink: ["/equipment", "rentals"],
          routerLinkActiveOptions: { exact: true }
        },
        {
          label: "All Ships",
          icon: "nf nf-mdi-rocket",
          routerLink: ["/equipment", "ships"],
          routerLinkActiveOptions: { exact: true }
        },
        {
          label: "Loadout Editor",
          icon: "nf nf-fae-checklist_o",
          routerLink: ["/loadout"],
          routerLinkActiveOptions: { exact: false }
        },
        {
          label: "My Hangar",
          icon: "nf nf-mdi-garage",
          routerLink: ["/hangar"],
          routerLinkActiveOptions: { exact: false }
        },
      ]
    },
    {
      label: "Mining & Processing",
      icon: "nf nf-fa-cubes",
      items: [
        {
          label: "Elements",
          icon: "nf nf-oct-ruby",
          routerLink: ["/mining", "elements"],
          routerLinkActiveOptions: { exact: true }
        },
        {
          label: "Mining Modules",
          icon: "nf nf-mdi-shape",
          routerLink: ["/equipment", "miningmodule"],
          routerLinkActiveOptions: { exact: true }
        },
        {
          label: "Salvage Modules",
          icon: "nf nf-mdi-recycle",
          routerLink: ["/equipment", "salvagemodule"],
          routerLinkActiveOptions: { exact: true }
        },
        {
          label: "Gadgets",
          icon: "nf nf-oct-tools",
          routerLink: ["/equipment", "mininggadget"],
          routerLinkActiveOptions: { exact: true }
        }
      ]
    }
  ];

  constructor(private router: Router,
    protected dataStore: DataCacheService,
    private cdr: ChangeDetectorRef) {
  }

  private setActiveSection(sectionName: string): void {
    this.menu_items.forEach(mi => {
      mi.expanded = (mi.label === sectionName);
    });
    this.cdr.detectChanges();
  }

  private searchChildrean(section: string, urlParts: string[], items: MenuItem[], found: boolean): boolean {
    items.forEach(child_item => {
      if (found) return;

      if (child_item.routerLink)
      {
        if(child_item.routerLink.length != urlParts.length)
          return;

        found = true;
        for (var idx = 0; idx < urlParts.length; idx++) {
          if (
            (idx == 0 && child_item.routerLink[idx] != ("/" + urlParts[idx])) ||
            (idx >= 1 && child_item.routerLink[idx] != urlParts[idx]))
            found = false;
        }
      }

      if (found) {
        this.setActiveSection(section);
      } else if (child_item.items && child_item.items.length > 0) {
        found = this.searchChildrean(section, urlParts, <MenuItem[]>child_item.items, found);
      }
    });
    return found;
  }

  ngOnInit(): void {
    this.dataStore.gameData.subscribe((gd) => {
      const stanton = gd.SystemBodies.find(b => b.ParentBodyID === null);
      this.menu_items[1].items = this.makeMapMenuItems(stanton.ChildBodies)
    });
    this.router.events.subscribe((val) => {
      if (val instanceof NavigationEnd) {
        const urlParts = val.urlAfterRedirects.substr(1).split("/");
        var found: boolean = false;
        this.menu_items.forEach(root_item => {
          if (found) return;

          found = this.searchChildrean(root_item.label, urlParts, <MenuItem[]>root_item.items, found);
        });
      }
    });
  }

  private makeMapMenuItems(bodies: SystemBody[]): MenuItem[] {
    return bodies
      .filter(b => b.BodyType === BodyType.Moon || b.BodyType === BodyType.Planet)
      .map((b) => {
        const node: MenuItem = {
          label: b.Name,
          icon: "nf " + (b.BodyType === BodyType.Moon ? "nf-fa-moon_o" : "nf-mdi-earth"),
          routerLink: ["/maps", b.Key],
          routerLinkActiveOptions: { exact: false },
          expanded: true,
          disabled: true,
          styleClass: "hoverfix"
        }
        const children = (b.ChildBodies && b.ChildBodies.length > 0) ? this.makeMapMenuItems(b.ChildBodies) : [];
        if(children.length > 0)
          node.items = children;
        return node;
      });
  }

  collapse() {
    this.isExpanded = false;
  }

  toggle() {
    this.isExpanded = !this.isExpanded;
  }
}
