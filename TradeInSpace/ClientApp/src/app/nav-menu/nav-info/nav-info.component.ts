import { Component, ErrorHandler, Inject, OnInit, ChangeDetectorRef } from '@angular/core';
import { DataCacheService } from '../../../data/data_cache';
import { ActivatedRoute } from '@angular/router';
import { GameDatabase, TradeTable } from '../../../data/generated_models';
import { TypeofExpr } from '@angular/compiler';
import * as moment from 'moment';
import { TISErrorHandler } from '../../../utilities/error_handler';
import { TISBaseComponent } from '../../tis_base.component';
import { MessageService } from 'primeng/api';
import { ChangelogPage } from '../../about/changelog/changelog';

@Component({
  selector: 'app-nav-info',
  templateUrl: './nav-info.component.html',
  styleUrls: ['./nav-info.component.css']
})
export class NavInfoComponent extends TISBaseComponent implements OnInit {
  moment: typeof moment = moment;
  ChangelogPage: typeof ChangelogPage = ChangelogPage;

  tradeUpdated: Date;

  constructor(dataStore: DataCacheService,
    @Inject(ErrorHandler) public err_handler: TISErrorHandler) {
    super(null, dataStore, null, null, "Nav Info");
  }

  onGameDataUpdated(): void {

  }

  onTradeDataUpdated(): void {
    this.tradeUpdated = this.dataStore.tradeDataCConfig.cacheTime;
  }

}
