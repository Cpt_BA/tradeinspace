import { Component, ViewChild, OnInit, ChangeDetectorRef, ViewEncapsulation } from '@angular/core';
import { ShoppingCartManagerService, ShoppingCartItem } from '../../data/shopping_cart_manager';
import { groupSet, Group } from '../../utilities/utilities';


@Component({
  selector: 'shopping-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {

  public CartSections: DisplayCartSection[] = [];

  public confirmClear: boolean = false;

  constructor(public cart: ShoppingCartManagerService) {
    cart.ListUpdated.subscribe(list => {
      if (!list) return;

      const groups: Group<ShoppingCartItem> = groupSet(list, sci => sci.Trade.Equipment.AttachmentType);

      this.CartSections = Object.keys(groups).map(g => {
        return {
          name: g,
          items: groups[g]
        }
      });
    });
  }

  ngOnInit(): void {
  }

  ngAfterViewInit(): void {
  }

  clearCart(): void {
    if (this.confirmClear) {
      this.cart.ClearCart();
    } else {
      this.confirmClear = true;
    }
    
  }

  leaveClear(): void {
    this.confirmClear = false;
  }
}

interface DisplayCartSection {
  name: string;
  items: ShoppingCartItem[];
}
