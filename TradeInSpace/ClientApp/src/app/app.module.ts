import { BrowserModule } from '@angular/platform-browser';
import { NgModule, ErrorHandler } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { RouterModule, UrlSegment } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { StorageServiceModule, LOCAL_STORAGE } from 'angular-webstorage-service';
import { CardModule } from 'primeng/card';
import { MenuModule } from 'primeng/menu';
import { TreeModule } from 'primeng/tree';
import { ChartModule } from 'primeng/chart';
import { ToastModule } from 'primeng/toast';
import { PanelModule } from 'primeng/panel';
import { StepsModule } from 'primeng/steps';
import { TableModule } from 'primeng/table';
import { ButtonModule } from 'primeng/button';
import { SliderModule } from 'primeng/slider';
import { DialogModule } from 'primeng/dialog';
import { BlockUIModule } from 'primeng/blockui';
import { ListboxModule } from 'primeng/listbox';
import { SpinnerModule } from 'primeng/spinner';
import { TabViewModule } from 'primeng/tabview';
import { ToolbarModule } from 'primeng/toolbar';
import { TooltipModule } from 'primeng/tooltip';
import { FieldsetModule } from 'primeng/fieldset';
import { CheckboxModule } from 'primeng/checkbox';
import { DropdownModule } from 'primeng/dropdown';
import { DataViewModule } from 'primeng/dataview';
import { InputTextModule } from 'primeng/inputtext';
import { PanelMenuModule } from 'primeng/panelmenu';
import { TreeTableModule } from 'primeng/treetable';
import { ScrollPanelModule } from 'primeng/scrollpanel';
import { SplitButtonModule } from 'primeng/splitbutton';
import { ProgressBarModule } from 'primeng/progressbar';
import { InputSwitchModule } from 'primeng/inputswitch';
import { MultiSelectModule } from 'primeng/multiselect';
import { ToggleButtonModule } from 'primeng/togglebutton';
import { SelectButtonModule } from 'primeng/selectbutton';
import { OverlayPanelModule } from 'primeng/overlaypanel';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { OrganizationChartModule } from 'primeng/organizationchart';

import { TooltipAttr } from '../directive/tooltip-attr.directive';
import { VarDirective } from '../directive/ng-var.directive';

import { AppComponent } from './app.component';
import { NavMenuComponent } from './nav-menu/nav-menu.component';
import { NavInfoComponent } from './nav-menu/nav-info/nav-info.component'
import { HomeComponent } from './home/home.component';
import { ExploreComponent } from './explore/explore.component';
import { SystemMapComponent } from './system/map.component';
import { ShopBrandListComponent } from './explore/shops-brand/shops-brand.component';
import { ShopLocationListComponent } from './explore/shops-location/shops-location.component';
import { AboutPage } from './about/about-page';
import { ChangelogPage } from './about/changelog/changelog';

import { RentalListComponent } from './explore/rentals/rental-list.component';
import { NearMeComponent } from './explore/near-me/near-me.component';

import { QDriveListComponent } from './equipment/qdrive/qdrive-list.component';
import { WeaponListComponent } from './equipment/weapon/weapon-list.component';
import { MiningLaserListComponent } from './equipment/mininglaser/mininglaser-list.component';
import { MiningGadgetListComponent } from './equipment/mininggadget/mininggadget-list.component';
import { MiningModuleListComponent } from './equipment/miningmodule/miningmodule-list.component';
import { SalvageModuleListComponent } from './equipment/salvagemodule/salvagemodule-list.component';
import { CoolerListComponent } from './equipment/cooler/cooler-list.component';
import { ShieldListComponent } from './equipment/shield/shield-list.component';
import { PowerplantListComponent } from './equipment/powerplant/powerplant-list.component';
import { ShipListComponent } from './equipment/ship/ship-list.component';
import { MissileListComponent } from './equipment/missile/missile-list.component';
import { PaintListComponent } from './equipment/paint/paint-list.component';

import { FPSFoodListComponent } from './equipment/fps-food/fps-food-list.component';
import { FPSUndersuitListComponent } from './equipment/fps-undersuit/fps-undersuit-list.component';
import { FPSArmorListComponent } from './equipment/fps-armor/fps-armor-list.component';
import { FPSWeaponListComponent } from './equipment/fps-weapon/fps-weapon-list.component';
import { FPSClothingListComponent } from './equipment/fps-clothing/fps-clothing-list.component';
import { FPSOtherListComponent } from './equipment/fps-other/fps-other-list.component';

import { MiningListComponent } from './mining/mining-list.component';
import { ElementListComponent } from './mining/element/element-list.component';

import { RouteFinderComponent } from './route-finder/route-finder.component';
//import { RouteDetailsComponent } from './route-finder/route-details/route-details.component';
import { RouteFilterComponent } from './route-finder/route-filter/route-filter.component';
import { RouteTableComponent } from './route-finder/route-table/route-table.component';

import { UserDetailsComponent } from './account/user-details/user-details.component';
import { RouteDetailsComponent } from './route-finder/route-details/route-details.component';
import { TradeTableComponent } from './trade-table/trade-table.component';

import { LoopFinderComponent } from './loop-finder/loop-finder.component';
import { LoopFilterComponent } from './loop-finder/loop-filter/loop-filter.component';
import { LoopTableComponent } from './loop-finder/loop-table/loop-table.component';
import { LoopRecordComponent } from './loop-finder/loop-record/loop-record.component';

import { LoadoutComponent } from './loadout/loadout.component';
import { LoadoutListComponent } from './loadout/list/loadout-list.component';
import { LoadoutEntryComponent } from './loadout/loadout-entry/loadout-entry.component';
import { LoadoutEditorComponent } from './loadout/loadout-editor/loadout-editor.component';
import { LoadoutSummaryComponent } from './loadout/loadout-summary/loadout-summary.component';

import { MapsComponent } from './maps/maps.component';
import { CartComponent } from './cart/cart.component';
import { ShipMapsComponent } from './ship-maps/ship-maps.component';

import { MessageService, ConfirmationService } from 'primeng/api';
import { DataCacheService } from '../data/data_cache';
import { LoadoutManagerService } from '../data/loadout_manager';
import { RouteCalculatorService } from '../data/route_calculator';
import { ShoppingCartManagerService } from '../data/shopping_cart_manager';

import { WhereAmIComponent } from './explore/where-am-i/where-am-i.component';
import { QDCalculatorComponent } from './tools/qd-calculator/qd-calculator.component'

import { RawDataListComponent } from './raw-data/raw-data.component'
import { RawCommodityListComponent } from './raw-data/commodities.component'
import { RawLocationListComponent } from './raw-data/locations.component'
import { RawVehicleListComponent } from './raw-data/vehicles.component'
import { RawRouteListComponent } from './raw-data/routes.component'
import { RawItemListComponent } from './raw-data/items.component'

import { TimeRangePipe } from '../pipes/time_range_pipe';
import { StationPathPipe } from '../pipes/station_path_pipe';
import { ScaleUnitPipe, FormatGenericPipe, FormatMoneyPipe, FormatSCUPipe, FormatUECPipe, FormatUnitsPipe, FormatPercentPipe, FormatModifierPipe } from '../pipes/scaleunit_pipe';
import { LocationTypePipe, ServiceFlagPipe, PadSizePipe, LoadoutSlotTypePipe, DisplayNamePipe, MissionRewardPipe, MissionTitlePipe } from '../pipes/display_text_pipe';
import { SafePipe, NewLinePipe, ValuePipe } from '../pipes/html_safe_pipe';
import { TypeofPipe } from '../pipes/util_pipe';
import { LocationDisplayPipe, TradeShopDisplayPipe } from '../pipes/location_display';
import { DamagePipe, ResistancePipe } from '../pipes/damage_pipe';

import { GaugeComponent } from './components/gauge.component';
import { JumpTimeCalcComponent } from './components/jump-time-calc/jump-time-calc.component';
import { SystemDisplayComponent } from './components/system-display/system-display.component';
import { EquipmentDetailsComponent } from './components/equipment-details/equipment-details.component';
import { EquipmentSelectorComponent } from './components/equipment-selector/equipment-selector.component';
import { EquipmentPriceComponent } from './components/equipment-price/equipment-price.component';
import { DamageDisplayComponent } from './components/damage-display/damage-display.component';
import { ResistDisplayComponent } from './components/damage-display/resist-display.component';
import { SearchOverlayComponent } from './components/search-overlay/search-overlay.component';
import { ShipSelectorComponent } from './components/ship-select/ship-select.component';
import { LocationSelectorComponent } from './components/location-select/location-select.component';

import { MissionListComponent } from './missions/mission-list/mission-list.component';
import { MissionDetailsComponent } from './missions/mission-details/mission-details.component';
import { MissionGiverListComponent } from './missions/mission-giver-list/mission-giver-list.component';
import { MissionGiverDetailsComponent } from './missions/mission-giver-details/mission-giver-details.component';

import { TISErrorHandler } from '../utilities/error_handler';


export function mapsMatcher(testurl: UrlSegment[]) {
  if (testurl.length < 2) return null;
  if (testurl[0].path !== "maps") return null;

  if (testurl.length === 2)
    return {
      consumed: testurl,
      posParams: {
        map_key: testurl[1]
      }
    };
  else if (testurl.length === 5)
    return {
      consumed: testurl,
      posParams: {
        map_key: testurl[1],
        zoom: testurl[2],
        lat: testurl[3],
        lng: testurl[4]
      }
    };
  else return null;
}


@NgModule({
  declarations: [
    AppComponent,
    NavMenuComponent,
    NavInfoComponent,
    HomeComponent,
    ExploreComponent,
    SystemMapComponent,
    ShopBrandListComponent,
    ShopLocationListComponent,
    AboutPage,
    ChangelogPage,

    ShipSelectorComponent,
    LocationSelectorComponent,
    SearchOverlayComponent,
    RentalListComponent,

    WhereAmIComponent,
    QDCalculatorComponent,

    RawDataListComponent,
    RawCommodityListComponent,
    RawLocationListComponent,
    RawVehicleListComponent,
    RawRouteListComponent,
    RawItemListComponent,

    GaugeComponent,
    JumpTimeCalcComponent,
    SystemDisplayComponent,
    EquipmentDetailsComponent,
    EquipmentSelectorComponent,
    EquipmentPriceComponent,
    DamageDisplayComponent,
    ResistDisplayComponent,

    MissionListComponent,
    MissionDetailsComponent,
    MissionGiverListComponent,
    MissionGiverDetailsComponent,

    QDriveListComponent,
    WeaponListComponent,
    MiningLaserListComponent,
    MiningGadgetListComponent,
    MiningModuleListComponent,
    SalvageModuleListComponent,
    CoolerListComponent,
    ShieldListComponent,
    PowerplantListComponent,
    MissileListComponent,
    PaintListComponent,
    ShipListComponent,

    FPSFoodListComponent,
    FPSUndersuitListComponent,
    FPSArmorListComponent,
    FPSWeaponListComponent,
    FPSClothingListComponent,
    FPSOtherListComponent,

    UserDetailsComponent,
    RouteDetailsComponent,
    TradeTableComponent,
    NearMeComponent,

    MiningListComponent,
    ElementListComponent,

    RouteFinderComponent,
    RouteFilterComponent,
    RouteTableComponent,

    LoopFinderComponent,
    LoopFilterComponent,
    LoopTableComponent,
    LoopRecordComponent,

    LoadoutComponent,
    LoadoutListComponent,
    LoadoutEntryComponent,
    LoadoutEditorComponent,
    LoadoutSummaryComponent,

    MapsComponent,
    CartComponent,
    ShipMapsComponent,

    SafePipe,
    DamagePipe,
    PadSizePipe,
    TimeRangePipe,
    ResistancePipe,
    DisplayNamePipe,
    StationPathPipe,
    ServiceFlagPipe,
    LocationTypePipe,
    FormatPercentPipe,
    LocationDisplayPipe,
    LoadoutSlotTypePipe,
    TradeShopDisplayPipe,

    ScaleUnitPipe,
    FormatGenericPipe,
    FormatMoneyPipe,
    FormatSCUPipe,
    FormatUECPipe,
    FormatUnitsPipe,
    FormatModifierPipe,
    MissionRewardPipe,
    MissionTitlePipe,
    NewLinePipe,
    ValuePipe,
    TypeofPipe,

    TooltipAttr,
    VarDirective
  ],
  imports: [
    StorageServiceModule,
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    BrowserAnimationsModule,
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot([
      { path: '', redirectTo: '/trade/route-finder/default', pathMatch: 'full' },
      { path: 'trade/route-finder', redirectTo: '/trade/route-finder/default', pathMatch: 'full', },
      { path: 'trade/route-finder/:preset', component: RouteFinderComponent },
      { path: 'trade/loop-finder', component: LoopFinderComponent },
      { path: 'trade/trade-table', component: TradeTableComponent },

      { path: 'equipment/qdrive', component: QDriveListComponent },
      { path: 'equipment/weapon', component: WeaponListComponent },
      { path: 'equipment/mininglaser', component: MiningLaserListComponent },
      { path: 'equipment/mininggadget', component: MiningGadgetListComponent },
      { path: 'equipment/miningmodule', component: MiningModuleListComponent },
      { path: 'equipment/salvagemodule', component: SalvageModuleListComponent },
      { path: 'equipment/cooler', component: CoolerListComponent },
      { path: 'equipment/shield', component: ShieldListComponent },
      { path: 'equipment/powerplant', component: PowerplantListComponent },
      { path: 'equipment/missile', component: MissileListComponent },
      { path: 'equipment/paint', component: PaintListComponent },
      { path: 'equipment/rentals', component: RentalListComponent },

      { path: 'equipment/ships', component: ShipListComponent },

      { path: 'loadout', component: LoadoutComponent },
      { path: 'loadout/:ship_key', component: LoadoutComponent },
      { path: 'loadout/my/:loadout_key', component: LoadoutComponent },

      { path: 'tools/qd', component: QDCalculatorComponent },

      { path: 'raw/commodity', component: RawCommodityListComponent },
      { path: 'raw/location', component: RawLocationListComponent },
      { path: 'raw/vehicle', component: RawVehicleListComponent },
      { path: 'raw/route', component: RawRouteListComponent },
      { path: 'raw/item', component: RawItemListComponent },

      { path: 'hangar', component: LoadoutListComponent },

      { path: 'maps', component: MapsComponent },
      { path: 'ship_maps', component: ShipMapsComponent },
      { path: 'ship_maps/:ship_key', component: ShipMapsComponent },
      /*path: 'maps/:map_key',*/ { component: MapsComponent, matcher: mapsMatcher },

      { path: 'fps_equipment/food', component: FPSFoodListComponent },
      { path: 'fps_equipment/undersuit', component: FPSUndersuitListComponent },
      { path: 'fps_equipment/armor', component: FPSArmorListComponent },
      { path: 'fps_equipment/weapon', component: FPSWeaponListComponent },
      { path: 'fps_equipment/clothing', component: FPSClothingListComponent },
      { path: 'fps_equipment/other', component: FPSOtherListComponent },

      { path: 'mission', component: MissionListComponent },
      { path: 'mission/:key', component: MissionDetailsComponent },
      { path: 'giver', component: MissionGiverListComponent },
      { path: 'giver/:key', component: MissionGiverDetailsComponent },

      { path: 'explore/system', component: ExploreComponent },
      { path: 'explore/map', component: SystemMapComponent },
      { path: 'explore/shops-brand', component: ShopBrandListComponent },
      { path: 'explore/shops-location', component: ShopLocationListComponent },
      { path: 'explore/nearby', component: NearMeComponent },

      { path: 'mining/locations', component: MiningListComponent },
      { path: 'mining/elements', component: ElementListComponent },

      { path: 'cart', component: CartComponent },

      { path: 'about', component: AboutPage },
      { path: 'changelog', component: ChangelogPage },
    ], { useHash: true }),


    CardModule,
    MenuModule,
    TreeModule,
    ChartModule,
    TableModule,
    ToastModule,
    PanelModule,
    StepsModule,
    ButtonModule,
    SliderModule,
    DialogModule,
    BlockUIModule,
    ListboxModule,
    SpinnerModule,
    TabViewModule,
    ToolbarModule,
    TooltipModule,
    FieldsetModule,
    CheckboxModule,
    DropdownModule,
    DataViewModule,
    InputTextModule,
    PanelMenuModule,
    TreeTableModule,
    ScrollPanelModule,
    SplitButtonModule,
    ProgressBarModule,
    InputSwitchModule,
    MultiSelectModule,
    ToggleButtonModule,
    SelectButtonModule,
    OverlayPanelModule,
    ConfirmDialogModule,
    InputTextareaModule,
    OrganizationChartModule
  ],
  providers: [
    MessageService,
    ConfirmationService,

    DataCacheService,
    LoadoutManagerService,
    RouteCalculatorService,
    ShoppingCartManagerService,
    { provide: ErrorHandler, useClass: TISErrorHandler },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
