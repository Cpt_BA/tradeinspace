import { Component, ViewChild, OnInit, ChangeDetectorRef, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DataCacheService } from '../../../data/data_cache';
import { TISBaseComponent } from '../../tis_base.component';
import { LocationType, ServiceFlags, ShopType, TradeShop, MissionGiverEntry, MissionGiver } from '../../../data/generated_models';


@Component({
  selector: 'mission-giver-details',
  templateUrl: '../mission-list/mission-list.component.html',
  styleUrls: ['../mission-list/mission-list.component.css']
})
export class MissionGiverDetailsComponent extends TISBaseComponent implements OnInit {

  missions: MissionGiverEntry[];
  giver: MissionGiver;

  title = "";

  rewardDetails = false;
  rewardFilter: number = 0;
  rewardTimeout: any;

  public MissionTypeIconMap: { [key: string]: string } = {
    "Appointment": "nf-mdi-calendar_clock",
    "Bounty Hunter": "nf-fa-user_times",
    "Delivery": "nf-oct-package",
    "ECN Alert": "nf-oct-alert",
    "Investigate": "nf-oct-question",
    "Investigation": "nf-oct-question",
    "Maintenance": "nf-fa-wrench",
    "Mercenary": "nf-mdi-target",
    "Priority": "nf-mdi-comment_alert",
    "Racing": "nf-mdi-car_sports",
    "Research": "nf-oct-beaker",
    "Search": "nf-fa-binoculars",
    "Service Beacons": "nf-mdi-account_alert"
  };

  constructor(httpRoute: ActivatedRoute,
    dataStore: DataCacheService,
    cdr: ChangeDetectorRef) {
    super(httpRoute, dataStore, cdr, null);
  }

  onAllDataGameUpdated(): void {
  }

  onAllDataUserUpdated(): void {
    
  }

  onUrlParamsLoaded(): void {
    if (this.urlParams.key) {
      this.giver = this.gameData.MissionGiversKeyMap[this.urlParams.key];
      this.missions = this.giver.Missions;
      this.title = `Mission List for ${this.giver.Name}`;
    }
  }

  onRewardChange(event, dt) {
    if (this.rewardTimeout) {
      clearTimeout(this.rewardTimeout);
    }

    this.rewardTimeout = setTimeout(() => {
      dt.filter(event.value * 1000, 'MinUEC', 'gte');
    }, 250);
  }

}
