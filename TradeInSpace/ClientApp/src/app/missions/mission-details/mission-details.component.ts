import { Component, ViewChild, OnInit, ChangeDetectorRef, ViewEncapsulation, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DataCacheService } from '../../../data/data_cache';
import { ModelUtils } from '../../../utilities/model_utils';
import { ClientData } from '../../../data/client_data';
import { TISBaseComponent } from '../../tis_base.component';
import { LocationType, ServiceFlags, ShopType, TradeShop, MissionGiverEntry, MissionRequirementDTO } from '../../../data/generated_models';
import { groupSet } from '../../../utilities/utilities';


@Component({
  selector: 'mission-details',
  templateUrl: './mission-details.component.html',
  styleUrls: ['./mission-details.component.css']
})
export class MissionDetailsComponent extends TISBaseComponent implements OnInit {
  ShopType: typeof ShopType = ShopType;
  ServiceFlags: typeof ServiceFlags = ServiceFlags;
  ModelUtils: typeof ModelUtils = ModelUtils;
  ClientData: typeof ClientData = ClientData;

  @Input()
  public mission: MissionGiverEntry;

  public RelatedMissions: MissionGiverEntry[];
  public StandingRequired = false;
  public RepRequired = false;

  public NoteIconMap: { [noteType: string]: MessageNoteMapping } = {
    "Note": { icon: "nf-fa-sticky_note", color: "white" },
    "Warning": { icon: "nf-fa-warning", color: "yellow" },
    "Illegal": { icon: "nf-mdi-skull", color: "white" },
    "ArrestLicense": { icon: "nf-fa-chain", color: "white" },
    "BuyIn": { icon: "nf-mdi-coin", color: "red" },
    "Cooldown": { icon: "nf-mdi-clock", color: "white" }
  };

  ReputationTypes: string[] = [
    "Wanted", "Virtue", "Reliability"
  ];

  constructor(private httpRoute: ActivatedRoute,
    dataStore: DataCacheService,
    cdr: ChangeDetectorRef) {
    super(httpRoute, dataStore, cdr, null);
  }

  onAllDataGameUpdated(): void {
  }

  onAllDataUserUpdated(): void {

  }

  onUrlParamsLoaded(): void {
    if (this.urlParams.key) {
      this.mission = this.gameData.Missions.find(m => m.Key === this.urlParams.key);
      this.RelatedMissions = this.gameData.Missions.filter(m => m.Requirements.some(r => r.MissionKeys ? r.MissionKeys.includes(this.mission.Key) : false));

      this.StandingRequired = this.mission.Requirements.some(r => this.ReputationTypes.includes(r.Type));
      this.RepRequired = this.mission.Requirements.some(r => !this.ReputationTypes.includes(r.Type));
    }
  }

}

export interface MessageNoteMapping {
  icon: string;
  color: string;
}

interface MissionRequirementsExtra extends MissionRequirementDTO {
  scope_value_name: string;
}
