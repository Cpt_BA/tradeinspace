import { Component, ViewChild, OnInit, ChangeDetectorRef, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DataCacheService } from '../../../data/data_cache';
import { TISBaseComponent } from '../../tis_base.component';
import { MissionGiver } from '../../../data/generated_models';


@Component({
  selector: 'mission-giver-list',
  templateUrl: './mission-giver-list.component.html',
  styleUrls: ['./mission-giver-list.component.css']
})
export class MissionGiverListComponent extends TISBaseComponent implements OnInit {

  givers: MissionGiver[];

  constructor(httpRoute: ActivatedRoute,
    dataStore: DataCacheService,
    cdr: ChangeDetectorRef) {
    super(httpRoute, dataStore, cdr, null);
  }

  onAllDataGameUpdated(): void {
    this.givers = this.gameData.MissionGivers
      .sort((a, b) => a.Name.localeCompare(b.Name));
  }

  onAllDataUserUpdated(): void {

  }

}
