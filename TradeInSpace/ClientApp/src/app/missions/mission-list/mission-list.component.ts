import { Component, ViewChild, OnInit, ChangeDetectorRef, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TISBaseComponent } from '../../tis_base.component';
import { LocationType, ServiceFlags, ShopType, TradeShop, MissionGiverEntry } from '../../../data/generated_models';
import { DataCacheService } from '../../../data/data_cache';


@Component({
  selector: 'mission-list',
  templateUrl: './mission-list.component.html',
  styleUrls: ['./mission-list.component.css']
})
export class MissionListComponent extends TISBaseComponent implements OnInit {

  missions: MissionGiverEntry[];
  public title = "Mission List";

  rewardDetails = false;
  rewardFilter: number = 0;
  rewardTimeout: any;

  public MissionTypeIconMap: { [key: string]: string } = {
    "Appointment": "nf-mdi-calendar_clock",
    "Bounty Hunter": "nf-fa-user_times",
    "Delivery": "nf-oct-package",
    "ECN Alert": "nf-oct-alert",
    "Investigate": "nf-oct-question",
    "Investigation": "nf-oct-question",
    "Maintenance": "nf-fa-wrench",
    "Mercenary": "nf-mdi-target",
    "Priority": "nf-mdi-comment_alert",
    "Racing": "nf-mdi-car_sports",
    "Research": "nf-oct-beaker",
    "Search": "nf-fa-binoculars",
    "Service Beacons": "nf-mdi-account_alert"
  };

  constructor(httpRoute: ActivatedRoute,
    dataStore: DataCacheService,
    cdr: ChangeDetectorRef) {
    super(httpRoute, dataStore, cdr, null);
  }

  onAllDataGameUpdated(): void {
    this.missions = this.gameData.Missions
      .filter(m => !m.SecretMission) //Easter eggs abound :^)
      .sort((a, b) => a.MissionType.localeCompare(b.MissionType));
  }

  onAllDataUserUpdated(): void {

  }

  onRewardChange(event, dt) {
    if (this.rewardTimeout) {
      clearTimeout(this.rewardTimeout);
    }

    this.rewardTimeout = setTimeout(() => {
      dt.filter(event.value * 1000, 'MinUEC', 'gte');
    }, 250);
  }

}
