"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ShipLoadManager = void 0;
var FBXLoader_1 = require("../../three_plugins/FBXLoader");
var THREE = require("three");
var three_1 = require("three");
var ShipLoadManager = /** @class */ (function () {
    function ShipLoadManager(onLoadComplete) {
        var _this = this;
        if (onLoadComplete === void 0) { onLoadComplete = null; }
        this.loaderRunning = false;
        this.loaderQueue = [];
        this.loadingItem = null;
        this.loaderCache = {};
        var loadingManager = new THREE.LoadingManager(function () {
            //Request the next model if there is one
            _this.requestModel();
        });
        //this.loader = new ColladaLoader(loadingManager);
        this.loader = new FBXLoader_1.FBXLoader(loadingManager);
        this.onLoadComplete = onLoadComplete;
    }
    ShipLoadManager.prototype.startModelRequest = function (url, parent, loadedCallback) {
        this.loaderQueue.push({
            url: url,
            callback: loadedCallback,
            attach_to: parent
        });
        this.requestModel();
    };
    ShipLoadManager.prototype.requestModel = function () {
        var _this = this;
        if (this.loaderRunning)
            return;
        this.loaderRunning = true;
        var modelPath = "";
        while (!modelPath) {
            if (this.loaderQueue.length == 0) {
                if (this.onLoadComplete)
                    this.onLoadComplete();
                return;
            }
            //Dequeue elements until we have a model to load
            this.loadingItem = this.loaderQueue[0];
            this.loaderQueue.splice(0, 1);
            //Check for flag that this has already been loaded
            if (this.loadingItem.attach_to.userData.is_loaded) {
                console.warn("Attempt to double-load model: ", this.loadingItem);
                modelPath = null;
            }
            else {
                modelPath = this.loadingItem.url;
            }
        }
        var _FinalizeRecord = function (final_obj) {
            if (_this.loadingItem.attach_to) {
                _this.loadingItem.attach_to.userData.is_loaded = true;
                _this.loadingItem.attach_to.add(final_obj);
            }
            _this.loadingItem.callback(final_obj);
            _this.loaderRunning = false;
            _this.requestModel();
        };
        if (this.loadingItem.url in this.loaderCache) {
            if (this.loaderCache[this.loadingItem.url] != null) {
                //Model was already loaded, just clone and use
                var clonedScene = this.loaderCache[this.loadingItem.url].clone();
                _FinalizeRecord(clonedScene);
            }
            else {
                //Model was previously attempted and failed, simply cache the failure and move on.
                this.loaderRunning = false;
                this.requestModel();
            }
        }
        else {
            //Request that item
            this.loader.load(this.loadingItem.url, function (fbx_data) {
                //Compute normals, the models do not have them to reduce file size as much as possible.
                fbx_data.traverse(function (c) { if (c instanceof three_1.Mesh)
                    c.geometry.computeVertexNormals(); });
                _this.loaderCache[_this.loadingItem.url] = fbx_data;
                _FinalizeRecord(fbx_data);
            }, function (progress) {
                //console.log("fbx progress", progress);
            }, function (error) {
                console.error("File read error", error);
                _this.loaderCache[_this.loadingItem.url] = null;
                //Continue loading. TODO: Feature flag
                _this.loaderRunning = false;
                _this.requestModel();
            });
        }
    };
    return ShipLoadManager;
}());
exports.ShipLoadManager = ShipLoadManager;
//# sourceMappingURL=ship-loader.component.js.map