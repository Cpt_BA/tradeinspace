import { Component, ViewChild, OnInit, ChangeDetectorRef, ViewEncapsulation, ElementRef, ViewRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DataCacheService } from '../../data/data_cache';
import { TradeTable, GameDatabase, SystemLocation, LocationType, ServiceFlags, PadSize, GameShip, GameEquipment } from '../../data/generated_models';
import { TreeNode, SortMeta, SelectItem, MessageService, Message } from 'primeng/api';
import { TreeTable } from 'primeng/treetable';
import { Subscription } from 'rxjs';
import { ShowTooltipEvent } from '../../directive/tooltip-attr.directive';
import { SelectButton } from 'primeng/selectbutton';
import { loadoutSlotTypeName, serviceFlagName } from '../../pipes/display_text_pipe';
import { ModelUtils } from '../../utilities/model_utils';
import { ClientData } from '../../data/client_data';
import { TISBaseComponent } from '../tis_base.component';
import { SafeResourceUrl, DomSanitizer } from '@angular/platform-browser';
import { ColladaLoader } from '../../three_plugins/ColladaLoader';
import { OrbitControls } from '../../three_plugins/OrbitController';
import { LayerOutlinePass } from '../../three_plugins/LayerOutlineController';
import { SliceMaterial } from '../../three_plugins/SliceMaterial';
import { FBXLoader } from '../../three_plugins/FBXLoader'
import * as THREE from 'three';
import { CastExpr } from '@angular/compiler';
import { ArrowHelper, AxesHelper, LineBasicMaterial, Mesh, Object3D, OrthographicCamera, PerspectiveCamera, Quaternion, ShaderMaterial, Texture, Uniform, Vector3, WebGLRenderer } from 'three';
import { HttpClient } from '@angular/common/http';
import Stats from 'three/examples/jsm/libs/stats.module';
import GUI from 'three/examples/jsm/libs/lil-gui.module.min';
import { Loadout, LoadoutSlot } from '../../data/loadout';
import { EffectComposer, Pass } from 'three/examples/jsm/postprocessing/EffectComposer';
import { RenderPass } from 'three/examples/jsm/postprocessing/RenderPass.js';
import { ShaderPass } from 'three/examples/jsm/postprocessing/ShaderPass.js';
import { disposeObject, ShipLoadManager } from './ship-loader.component';
import { ShipModel } from './ship-model.component';
import { BoundLoadoutNode, LoadoutDisplayMode, LoadoutGroupingMode } from './ship-loadout.component';
import { FXAAShader } from 'three/examples/jsm/shaders/FXAAShader.js';
import { TexturePass } from 'three/examples/jsm/postprocessing/TexturePass';
import { EquipmentListExpandedParams } from '../components/equipment-selector/equipment-selector.component';
import { saveFile } from '../../utilities/utilities';
import { LayerConfiguration } from '../../data/client_models';
import { Tree } from 'primeng/tree';
import { expand } from 'rxjs/operators';

@Component({
  selector: 'ship-maps',
  templateUrl: 'ship-maps.component.html',
  styles: [
`
.camera-control {
  padding-left: 0.5em;
  cursor: pointer;
}
.rotate {
  transform: rotate(-90deg);
}
`
  ]
  })
export class ShipMapsComponent extends TISBaseComponent {
  public Math: typeof Math = Math;

  ///
  /// Variables & Config
  ///

  public tree_node: any[] = null;
  public tree_bound: any[] = null;
  public hierarchy_root: TreeNode[] = null;

  public placeholder: any[] = [{ label: "Loading..." }]
  public selected_bound: TreeNode;
  public selected_previous: TreeNode;
  public selected_hierarchy: any;

  loadNode(event) {
    if (event.node) {
      var obj: THREE.Object3D = event.node.data;
      event.node.children = obj.children.map(c => this.convertObject3d(c))
    }
  }

  boundSelect(event) {
    console.log(event.node.data);
    this.setSelectedNode(event.node);
    this.updateData();
  }

  setNestedHighlight(root: THREE.Object3D, enabled: boolean): void {

    var _CollectMeshes = (from_obj: THREE.Object3D): THREE.Mesh[] => {
      var all_meshes: THREE.Mesh[] = [];
      var _Search = (obj: THREE.Object3D): void => {
        if (obj instanceof THREE.Mesh)
          all_meshes.push(obj);
        obj.children.forEach(c => _Search(c));
      };
      _Search(from_obj);
      return all_meshes;
    }

    var meshes = _CollectMeshes(root);
    meshes.forEach(m => {
      if (enabled)
        m.layers.enable(this.layers.outline_pass)
      else
        m.layers.disable(this.layers.outline_pass)
    });
  }

  displaySelect(event) {
    console.log(event);
  }

  clearHelper() {

  }

  createHelper() {
    //this.selectHelper = new THREE.EdgesGeometry()
  }

  updateData(event?: { originalEvent: PointerEvent, node: any }) {
    //Bit of a hack, but weird behavior was abound...
    setTimeout(() => {
      if (this.cdr !== null && this.cdr !== undefined && !(this.cdr as ViewRef).destroyed) {
        this.cdr.detectChanges();
      }
    }, 1);
  }

  convertObject3d(item: THREE.Object3D): any {
    return {
      label: item.name || item.type,
      data: item,
      children: this.placeholder
    }
  }



  @ViewChild("canvas", { static: true }) canvasRef: ElementRef;
  @ViewChild("stats", { static: true }) statsRef: ElementRef;
  @ViewChild("loader", { static: true }) loaderRef: ElementRef;
  @ViewChild("shipgui", { static: true }) shipguiRef: ElementRef;
  @ViewChild("innerContainer", { static: true }) innerContainerRef: ElementRef;
  @ViewChild("navMenu", { static: true }) navMenuRef: ElementRef;

  private camera!: THREE.Camera;

  private get canvas(): HTMLCanvasElement {
    return this.canvasRef.nativeElement;
  }
  private get aspectRatio(): number {
    return this.canvas.clientWidth / this.canvas.clientHeight;
  }

  private loader!: ShipLoadManager;
  private loaded_ship!: ShipModel;
  public loading: boolean = false;
  public loader_completed: number = 0;
  public loader_total: number = 0;
  public loader_progress: number = 0;
  public loadout_visible: boolean = true;

  private gui: GUI;

  private animate_target: THREE.Vector3;
  private animate_origin: THREE.Vector3;
  private animate_camera: boolean = false;
  private animate_start: number = 0;
  private animate_duration: number = 0.25;

  private renderer!: THREE.WebGLRenderer;
  private outline!: LayerOutlinePass;
  private hull_pass: ShipHullRenderPass;
  private fxaa_pass: ShaderPass;
  public fullscreen: boolean = false;

  private composer!: EffectComposer;
  private scene!: THREE.Scene;
  private controls: OrbitControls;
  private stats: Stats;
  private clock: THREE.Clock = new THREE.Clock(true);
  private hitTest: THREE.Raycaster = new THREE.Raycaster();
  private dirLight: THREE.DirectionalLight;
  private grid: THREE.GridHelper;

  private sceneTransformRoot: THREE.Group;
  private cameraHelperRoot: THREE.Group;
  private focusHeightRoot: THREE.Group;
  private finalRoot: THREE.Group;

  private layers: LayerConfiguration = {
    default: 0,
    hull_pass: 1,
    outline_pass: 2,
    navigation_pass: 3,
    gadgets_layer: 4,

    base_material: null,
    equipment_material: null,
    traversal_material: null,
  }

  private params = {
    focusHeight: 0,
    focusTop: 2,
    focusBottom: 2,
    cameraDistance: 20, //Meters

    shipOptions: [],
    selectedShip: null,

    outlineThickness: 3,
    outlineColor: new THREE.Color(0xFF0000),

    gridEnabled: true,
    gridColor: new THREE.Color(0xFFFFFF),
    gridAlpha: 0.6,

    baseSlicing: false,
    baseColor: new THREE.Color(0x707070),
    baseAlpha: 1.0,

    equipmentSlicing: false,
    equipmentColor: new THREE.Color(0x754545),
    equipmentAlpha: 1.0,

    traversalSlicing: false,
    traversalColor: new THREE.Color(0x454575),
    traversalAlpha: 1.0,

    startLoad: () => {
      if (this.params.selectedShip) {

        if (this.loading)
          return;

        if (this.loaded_ship) {
          this.loaded_ship.dispose();
        }

        var ship: GameShip = this.gameData.ShipKeyMap[this.params.selectedShip];
        var loadout: Loadout = Loadout.FromShip(this.gameData, ship);
        this.loaded_ship = new ShipModel(loadout, this.loader, this.layers);

        this.loading = true;
        this.updateData(null);

        this.router.navigate(["/ship_maps", ship.Key], { replaceUrl: true });

        this.loaded_ship.startLoading(this.finalRoot, true, true,
          (completed: number, total: number, progress: number) => {
            //onModelLoadProgress
            //If progress is 1, the model count already includes that progress
            var safe_total = total == 0 ? 1 : total;
            var sub_progress = progress == 1 ? 0 : progress * 1 / safe_total;
            this.loader_completed = completed;
            this.loader_total = safe_total;
            this.loader_progress = completed / safe_total + sub_progress;

            this.updateData(null);
          },
          () => {
            //onModelFinished
            this.loading = false;
            this.loader_completed = 0;
            this.loader_total = 0;
            this.updateShipLoadout();
            this.updateData(null);
          },
          () => {
            //onSubModelLoaded
          });
      }
    }
  };


  private views: CameraView[] = [
    //Main Camera
    {
      left: 0,
      bottom: 0,
      width: 1.0,
      height: 1.0,
      background: new THREE.Color(0.5, 0.5, 0.7),
      eye: [-20, 20, -20],
      up: [0, 1, 0],
      fov: 60,
      camera: null,
      aspect: 0,
      updateCamera: (camera, scene, mouseX, mouseY) => {
        if (this.controls.focusHeight != this.params.focusHeight) {
          this.updateFocusHeightParam(this.controls.focusHeight);
        }
      }
    }
  ];

  constructor(
    dataStore: DataCacheService,
    cdr: ChangeDetectorRef,
    toast: MessageService,
    private httpRoute: ActivatedRoute,
    private router: Router,
    private sanitizer: DomSanitizer,
    private http: HttpClient) {
    super(httpRoute, dataStore, cdr, toast);
  }

  pickObject(offset_x: number, offset_y: number, layers: number[] = null): THREE.Intersection[] {
    var coords = {
      x: (offset_x / this.canvas.clientWidth) * 2 - 1,
      y: -(offset_y / this.canvas.clientHeight) * 2 + 1
    };
    var oldMask = this.hitTest.layers.mask;
    if (layers) {
      this.hitTest.layers.disableAll();
      layers.forEach(l => this.hitTest.layers.set(l));
    } else {
      this.hitTest.layers.enableAll();
    }
    this.hitTest.setFromCamera(coords, this.camera);
    var intersection = this.hitTest.intersectObject(this.finalRoot, true);
    this.hitTest.layers.mask = oldMask;
    return intersection;
  }

  ///
  /// Callbacks
  ///

  onUrlParamsLoaded(): void {
    if (this.urlParams.ship_key) {
      if (this.urlParams.ship_key in this.gameData.ShipKeyMap) {
        this.params.selectedShip = this.urlParams.ship_key;
        this.params.startLoad();
        this.updateGUI();
      } else {
        this.toast.add({ summary: "Unable to find that ship.", severity: "error" });
      }
    }
  }

  onClick = (event: MouseEvent) => {
    var intersections = this.pickObject(event.offsetX, event.offsetY);

    if (intersections.length == 0) {
      this.setSelectedNode(null);
      return;
    }

    var _CheckIntersection = (inter: THREE.Intersection): BoundLoadoutNode => {
      var search_object: THREE.Object3D = inter.object;

      //Search up the tree until we find something thats not a chunk.
      while (search_object && search_object.userData && search_object.userData.TIS_Type != "Equipment") {
        search_object = search_object.parent;
      }

      if (!search_object) return null;

      var boundData = this.loaded_ship.bound_loadout_root.searchObject(search_object);
      if (boundData) {
        return boundData;
      }
      return null;
    };

    var foundMatch: BoundLoadoutNode = null;
    var inersectionIndex = 0;
    for (inersectionIndex = 0; inersectionIndex < intersections.length && !foundMatch; inersectionIndex++) {
      foundMatch = _CheckIntersection(intersections[inersectionIndex]);
      //If the slot isn't editable, ignore and check next
      if (foundMatch && !foundMatch.isVisible(LoadoutDisplayMode.Editable))
        foundMatch = null;
    }

    if (foundMatch) {
      //First click, expand it
      if (event.detail == 1) {
        //Shenanigans
        this.expandTo(foundMatch);
        this.refreshShipLoadout();
      }
      //Only animate on a double-click or higher
      else if (event.detail >= 2) {
        console.log(foundMatch);
        this.animateTo(foundMatch.object.parent.getWorldPosition(foundMatch.object.position.clone()));
      }
    } else {
      this.setSelectedNode(null);
    }
    this.updateData();

  };

  clickPosition: { x: number, y: number } = null;

  onMouseDown = (event: MouseEvent) => {
    this.clickPosition = { x: event.offsetX, y: event.offsetY };
  };

  onMouseUp = (event: MouseEvent) => {
    if (!this.clickPosition) return;
    if (this.clickPosition.x == event.offsetX && this.clickPosition.y == event.offsetY) {
      this.onClick(event);
      this.clickPosition = null;
    }
  };

  onKeyDown = (event: KeyboardEvent) => {
    if (event.repeat) return;

    //WASD, QE, RF are all handled in camera
    //Only handle layer controls here.

    switch (event.code) {

      case 'Digit1':
        this.params.baseSlicing = !this.params.baseSlicing;
        this.layers.base_material.setSliceEnabled(this.params.baseSlicing);
        this.updateGUI();
        break;

      case 'Digit2':
        this.params.equipmentSlicing = !this.params.equipmentSlicing;
        this.layers.equipment_material.setSliceEnabled(this.params.equipmentSlicing);
        this.updateGUI();
        break;

      case 'Digit3':
        this.params.traversalSlicing = !this.params.traversalSlicing;
        this.layers.traversal_material.setSliceEnabled(this.params.traversalSlicing);
        this.updateGUI();
        break;

      case 'Digit0':
        this.hull_pass.material.hologramEnabled = !this.hull_pass.material.hologramEnabled;
        this.updateGUI();
        break;

      case 'Numpad1':
        this.moveCamera("backleft");
        break;
      case 'Numpad2':
        this.moveCamera("back");
        break;
      case 'Numpad3':
        this.moveCamera("backright");
        break;

      case 'Numpad4':
        this.moveCamera("left");
        break;
      case 'Numpad5':
        this.moveCamera("top");
        break;
      case 'Numpad6':
        this.moveCamera("right");
        break;

      case 'Numpad7':
        this.moveCamera("frontleft");
        break;
      case 'Numpad8':
        this.moveCamera("front");
        break;
      case 'Numpad9':
        this.moveCamera("frontright");
        break;

    }
  }

  onWindowResize = () => {
    this.resizeScreen();
  };

  onFullscreenChange = (event) => {
    this.fullscreen = this.isFullscreen();
    this.resizeScreen();
    this.updateData(null);
  };

  /*
  onMouseMove = (event: PointerEvent) => {
    //TODO: Feature flag/select tool
    var obj = this.pickObject(event.offsetX, event.offsetY);
    return null;
  };
  */

  public populateEquipmentList(node: BoundLoadoutNode, event: EquipmentListExpandedParams): void {
    const gameDB = this.gameData;
    const slot = node.slot;

    event.equipment = gameDB.Equipment.filter(eq => {
      let attachType = eq.AttachmentType;
      if (eq.AttachmentSubtype) attachType += `/${eq.AttachmentSubtype}`;

      const sizeMatch = eq.Size >= slot.min_size && eq.Size <= slot.max_size
      const typeMatch = slot.canFitType(attachType);

      return sizeMatch && typeMatch;
    });
  }

  public changeEquipment(node: BoundLoadoutNode, equipment: GameEquipment): void {
    node.changeComponent(equipment);
    this.loaded_ship.populateShipLoadoutSlot(node, (load: BoundLoadoutNode) => {
      if (load.equipment == equipment) {
        this.updateShipLoadout();
      }
    });
  }

  ///
  /// UI Callbacks
  ///

  public takeScreenshot(): void {
    var saveName = "TIS_Screenshot";

    if (this.loaded_ship)
      saveName += ("_" + this.loaded_ship.loadout.from_ship.Key);

    //For some reason need to render() twice to get the domElement to update properly with the hidden element
    this.camera.layers.disable(this.layers.gadgets_layer);
    this.composer.render();
    this.composer.render();
    
    this.renderer.domElement.toBlob((blob: Blob | null): void => {
      saveFile(saveName, blob);
      this.camera.layers.enable(this.layers.gadgets_layer);
    })
  }

  goFullscreen() {
    if (this.isFullscreen()) {
      RunPrefixMethod(document, "CancelFullScreen");
    }
    else {
      RunPrefixMethod(this.innerContainerRef.nativeElement, "RequestFullScreen");
    }
  }

  moveCamera(location: string) {

    var bounds: THREE.Box3;

    //TODO: Better isLoaded() mechanism
    if (this.loaded_ship && this.loaded_ship.bound_loadout_root) {
      bounds = new THREE.Box3().setFromObject(this.loaded_ship.ship_root);
    } else {
      bounds = new THREE.Box3(
        new Vector3(-this.params.cameraDistance, -this.params.cameraDistance , -this.params.cameraDistance),
        new Vector3(this.params.cameraDistance, this.params.cameraDistance , this.params.cameraDistance));
    }

    //NOTE: Ships are -z forward, so camera poisitions are all flipped to account for this

    switch (location) {
      case "frontleft":
        this.camera.position.set(-bounds.max.x * 2, 0, -bounds.max.z * 2);
        break;
      case "front":
        this.camera.position.set(0, 0, -bounds.max.z * 2);
        break;
      case "frontright":
        this.camera.position.set(-bounds.min.x * 2, 0, -bounds.max.z * 2);
        break;

      case "left":
        this.camera.position.set(-bounds.max.x * 2, 0, 0);
        break;
      case "top":
        //We use z (forward) here intentionally to allow for long ships to be fully in frame
        this.camera.position.set(0, bounds.max.z * 2, 0);
        break;
      case "right":
        this.camera.position.set(-bounds.min.x * 2, 0, 0);
        break;

      case "backleft":
        this.camera.position.set(-bounds.max.x * 2, 0, -bounds.min.z * 2);
        break;
      case "back":
        this.camera.position.set(0, 0, -bounds.min.z * 2);
        break;
      case "backright":
        this.camera.position.set(-bounds.min.x * 2, 0, -bounds.min.z * 2);
        break;
    }
    this.controls.target.set(0, 0, 0);
    this.updateFocusHeightParam(0);
  }

  ///
  /// Scene Setup
  ///

  private sceneSetup() {
    this.canvas.addEventListener("mousedown", this.onMouseDown);
    this.canvas.addEventListener("mouseup", this.onMouseUp);
    this.canvas.addEventListener("keydown", this.onKeyDown);
    //this.canvas.addEventListener("click", this.onClick);
    //Mouse move is a bit too expensive
    //this.canvas.addEventListener('pointermove', this.onMouseMove);

    window.addEventListener('resize', this.onWindowResize, false);
    window.addEventListener('webkitfullscreenchange', this.onFullscreenChange, false);
    window.addEventListener('mozfullscreenchange', this.onFullscreenChange, false);
    window.addEventListener('fullscreenchange', this.onFullscreenChange, false);
    window.addEventListener('MSFullscreenChange', this.onFullscreenChange, false);

    this.createScene();
    this.createCameras();

    this.startRender();
    this.addGUI();
  }

  public dispose() {
    this.canvas.removeEventListener("mousedown", this.onMouseDown);
    this.canvas.removeEventListener("mouseup", this.onMouseUp);
    this.canvas.removeEventListener("keydown", this.onKeyDown);
    window.removeEventListener("resize", this.onWindowResize);
    window.removeEventListener('webkitfullscreenchange', this.onFullscreenChange, false);
    window.removeEventListener('mozfullscreenchange', this.onFullscreenChange, false);
    window.removeEventListener('fullscreenchange', this.onFullscreenChange, false);
    window.removeEventListener('MSFullscreenChange', this.onFullscreenChange, false);
  }

  private createCameras() {

    var ii = 0;
    const view = this.views[ii];
    view.aspect = (this.canvas.clientWidth * view.width) / (this.canvas.clientHeight * view.height);

    var camera: THREE.Camera;
    if (ii == 0)
      camera = new THREE.PerspectiveCamera(view.fov, view.aspect, 1, 10000);
    else {
      camera = new THREE.OrthographicCamera(-10 * view.aspect, 10 * view.aspect, -10, 10, 1, 1000);
    }

    this.scene.add(camera);
    camera.position.fromArray(view.eye);
    camera.up.fromArray(view.up);
    view.camera = camera;

    this.camera = this.views[0].camera;
    this.camera.lookAt(0, 0, 0);
    this.camera.layers.enable(this.layers.gadgets_layer);

    this.dirLight = new THREE.DirectionalLight(0xFFFFFF, 0.4);
    this.dirLight.layers.enableAll();
    this.dirLight.target = this.cameraHelperRoot;
    this.camera.add(this.dirLight);

    //Setup camera controls
    this.controls = new OrbitControls(this.camera, this.canvas);
    this.controls.target_helper = this.cameraHelperRoot;
    this.controls.enabled = true;
    this.controls.enablePan = true;

    var axes = new AxesHelper(1);
    axes.layers.disableAll();
    axes.layers.enable(this.layers.gadgets_layer);
    this.cameraHelperRoot.add(axes);
  }

  private createScene() {
    //Scene, lights, camera
    this.scene = new THREE.Scene();
    this.scene.background = new THREE.Color(0x000000);

    this.stats = Stats();
    this.statsRef.nativeElement.appendChild(this.stats.dom);
    this.stats.domElement.style.removeProperty("position");

    this.layers.base_material = new SliceMaterial();
    this.layers.base_material.name = "BaseMaterial";
    this.layers.base_material.setSliceHeight(this.params.focusHeight);
    this.layers.base_material.setSliceTop(this.params.focusTop);
    this.layers.base_material.setSliceBottom(this.params.focusBottom);
    this.layers.equipment_material = new SliceMaterial();
    this.layers.equipment_material.name = "EquipmentMaterial";
    this.layers.equipment_material.setSliceHeight(this.params.focusHeight);
    this.layers.equipment_material.setSliceTop(this.params.focusTop);
    this.layers.equipment_material.setSliceBottom(this.params.focusBottom);
    this.layers.traversal_material = new SliceMaterial();
    this.layers.traversal_material.name = "TraversalMaterial";
    this.layers.traversal_material.setSliceHeight(this.params.focusHeight);
    this.layers.traversal_material.setSliceTop(this.params.focusTop);
    this.layers.traversal_material.setSliceBottom(this.params.focusBottom);
    this.updateLayerColors();

    const ambientLight = new THREE.AmbientLight(0x888888, 0.1);
    this.scene.add(ambientLight);

    this.sceneTransformRoot = new THREE.Group();
    this.sceneTransformRoot.rotation.x = -Math.PI / 2;
    //this.sceneTransformRoot.rotation.z = Math.PI;

    this.hitTest = new THREE.Raycaster();

    //Root that ship will load under. Pre-transform it.
    this.finalRoot = new THREE.Group();
    //this.finalRoot.scale.x = -1;
    //this.finalRoot.scale.z = -1;

    this.cameraHelperRoot = new THREE.Group();
    this.scene.add(this.cameraHelperRoot);

    this.focusHeightRoot = new THREE.Group();
    this.scene.add(this.focusHeightRoot);

    this.grid = new THREE.GridHelper(100, 20, this.params.gridColor, this.params.gridColor);
    (<THREE.Material>this.grid.material).opacity = this.params.gridAlpha;
    (<THREE.Material>this.grid.material).transparent = true;
    this.focusHeightRoot.add(this.grid);

    this.sceneTransformRoot.add(this.finalRoot);
    this.scene.add(this.sceneTransformRoot);

    //Loader
    this.loader = new ShipLoadManager();
  }

  private updateLayerColors() {
    this.layers.base_material.setColor(new THREE.Vector4(this.params.baseColor.r, this.params.baseColor.g, this.params.baseColor.b, this.params.baseAlpha));
    this.layers.equipment_material.setColor(new THREE.Vector4(this.params.equipmentColor.r, this.params.equipmentColor.g, this.params.equipmentColor.b, this.params.equipmentAlpha));
    this.layers.traversal_material.setColor(new THREE.Vector4(this.params.traversalColor.r, this.params.traversalColor.g, this.params.traversalColor.b, this.params.traversalAlpha));
  }

  ///
  /// Renderer Setup
  ///

  private startRender() {
    this.renderer = new THREE.WebGLRenderer({ canvas: this.canvas });
    this.renderer.setPixelRatio(this.aspectRatio);
    this.renderer.setSize(this.canvas.clientWidth, this.canvas.clientHeight);
    this.renderer.setClearColor(0x000000, 1);
    this.renderer.autoClear = false;
    this.renderer.localClippingEnabled = true;

    this.composer = new EffectComposer(this.renderer);

    this.hull_pass = new ShipHullRenderPass(this.scene, this.camera, this.layers.hull_pass);
    this.hull_pass.needsSwap = false;
    this.composer.addPass(this.hull_pass);

    this.outline = new LayerOutlinePass(new THREE.Vector2(this.canvas.clientWidth, this.canvas.clientHeight), this.scene, this.camera, this.layers.outline_pass);
    this.outline.edgeColor = this.params.outlineColor;
    this.outline.edgeThickness = this.params.outlineThickness;
    this.outline.needsSwap = false;
    this.composer.addPass(this.outline);

    this.fxaa_pass = new ShaderPass(FXAAShader);
    this.fxaa_pass.needsSwap = false;
    this.composer.addPass(this.fxaa_pass);

    this.resizeScreen();
    this.frameRender();

    //Total hack to re-layout after 200 and 500ms
    //This is because the header bar disappears on this page modifying tha layout
    setTimeout(() => this.resizeScreen(), 200);
    setTimeout(() => this.resizeScreen(), 500);
  }

  private frameRender() {
    //Queue up another frame
    requestAnimationFrame(() => this.frameRender());

    this.animate();

    const h = this.canvas.clientHeight,
      w = this.canvas.clientWidth,
      p = 10;

    this.renderer.clear();

    //Only one view now...
    const view = this.views[0];
    const camera = view.camera;

    view.updateCamera(camera, this.scene, 0, 0);

    const left = Math.floor(w * view.left);
    const bottom = Math.floor(h * view.bottom);
    const width = Math.floor(w * view.width);
    const height = Math.floor(h * view.height);

    this.renderer.setViewport(left, bottom, width, height);
    this.renderer.setScissor(left, bottom, width, height);
    this.renderer.setScissorTest(true);
    this.renderer.setClearColor(view.background);

    if (camera instanceof PerspectiveCamera) {
      camera.aspect = width / height;
      camera.updateProjectionMatrix();
    } else if (camera instanceof OrthographicCamera) {
        
    }
    
    //this.renderer.render(this.scene, camera);
    this.composer.render();
  }

  private resizeScreen() {
    var toolbarHeight = document.getElementById("toolbarRoot").offsetHeight;
    var navmenuWidth = (<HTMLElement>document.getElementsByClassName("main-nav")[0]).offsetWidth;

    //The loadout panel takes up 40% of our space, so we take 50%
    
    var finalSize: { x: number, y: number };
    if (!this.fullscreen) {
      finalSize = {
        x: window.innerWidth - navmenuWidth - 40,
        y: window.innerHeight - toolbarHeight - 20
      }
    } else {
      finalSize = { x: window.innerWidth, y: window.innerHeight }
    }
    

    this.innerContainerRef.nativeElement.style.width = finalSize.x + "px";
    this.innerContainerRef.nativeElement.style.height = finalSize.y + "px";
    this.canvasRef.nativeElement.style.width = finalSize.x + "px";
    this.canvasRef.nativeElement.style.height = finalSize.y + "px";

    this.renderer.setSize(finalSize.x, finalSize.y);
    this.composer.setSize(finalSize.x, finalSize.y);
    this.composer.passes.forEach(p => p.setSize(finalSize.x, finalSize.y));

    var pixelRatio = this.renderer.getPixelRatio();

    this.fxaa_pass.material.uniforms['resolution'].value.x = 1 / (finalSize.x * pixelRatio);
    this.fxaa_pass.material.uniforms['resolution'].value.y = 1 / (finalSize.y * pixelRatio);
  }

  private addGUI() {
    const gui = new GUI({ autoPlace: false });
    this.gui = gui;
    this.shipguiRef.nativeElement.appendChild(gui.domElement);

    gui.add(this.params, 'selectedShip', this.params.shipOptions).name("Ship");
    gui.add(this.params, 'startLoad').name("Start Load");

    gui.add(this.params, 'focusHeight', -30, 30).step(0.01).name('Slicing Height').onChange((value) => {
      this.updateFocusHeightParam(value);
    });

    gui.add(this.params, 'focusTop', 0, 10).step(0.01).name('Slice Above').onChange((value) => {
      this.layers.base_material.setSliceTop(value);
      this.layers.equipment_material.setSliceTop(value);
      this.layers.traversal_material.setSliceTop(value);
    });

    gui.add(this.params, 'focusBottom', 0, 10).step(0.01).name('Slice Below').onChange((value) => {
      this.layers.base_material.setSliceBottom(value);
      this.layers.equipment_material.setSliceBottom(value);
      this.layers.traversal_material.setSliceBottom(value);
    });

    var key_text = {
      "WASD": "Move",
      "Q/E": "Rotate",
      "R/F": "Up/Down",
      "1,2,3,0": "Toggle Slicing",
      "NUM 1-9": "Camera Angles"
    }
    var shortcuts = gui.addFolder("Keyboard Shortcuts");
    shortcuts.disabled = true;
    for (var key_name in key_text) {
      shortcuts.add(key_text, key_name).disable();
    }

    //Visual Settings
    var visuals_folder = gui.addFolder("Visuals");

    //Outline Settings
    var outlineFolder = visuals_folder.addFolder("Outline");
    outlineFolder.close();
    outlineFolder.add(this.params, "outlineThickness", 1, 10, 0.1).name("Thickness").onChange((value) => {
      this.outline.edgeThickness = value;
    });
    outlineFolder.addColor(this.params, "outlineColor").name("Color").onChange((value) => {
      this.outline.edgeColor = value;
    });

    //Helper Grid GUI
    var gridFolder = visuals_folder.addFolder("Grid");
    gridFolder.close();
    gridFolder.add(this.params, "gridEnabled").name("Enabled").onChange((value) => {
      this.grid.visible = value;
    });
    gridFolder.addColor(this.params, "gridColor").name("Color").onChange((value) => {
      //I don't love this, ut there isn't really a better way to go about this as colors are stored per-vertex
      if (this.grid) {
        disposeObject(this.grid);
      }
      this.grid = new THREE.GridHelper(100, 20, value, value);
      (<THREE.Material>this.grid.material).opacity = this.params.gridAlpha;
      (<THREE.Material>this.grid.material).transparent = true;
      this.focusHeightRoot.add(this.grid);
    });
    gridFolder.add(this.params, "gridAlpha", 0, 1, 0.001).name("Alpha").onChange((value) => {
      (<THREE.Material>this.grid.material).opacity = value;
    });

    //Gui for material layers
    var layerFolder = visuals_folder.addFolder("Layers");
    layerFolder.close();

    var _MakeLayer = (title: string, key: string, material: SliceMaterial) => {
      var _layer = layerFolder.addFolder(title);

      var colorProperty = title.toLowerCase() + "Color";
      var alphaProperty = title.toLowerCase() + "Alpha";
      var slicingProperty = title.toLowerCase() + "Slicing";

      _layer.add(this.params, slicingProperty).name(`Slicing [${key}]`).onChange((value) => {
        material.setSliceEnabled(value);
      });
      _layer.addColor(this.params, colorProperty).name("Color").onChange((value) => {
        this.updateLayerColors();
      });
      _layer.add(this.params, alphaProperty, 0, 1, 0.001).name("Alpha").onChange((value) => {
        this.updateLayerColors();
      })
    };

    var hull_layer = layerFolder.addFolder("Sliced Hologram");
    hull_layer.add(this.hull_pass.material, "hologramEnabled").name("Enabled [0]");
    hull_layer.addColor(this.hull_pass.material, "hologramColor").name("Color");
    hull_layer.add(this.hull_pass.material, "hologramAlpha", 0, 1, 0.001).name("Alpha");


    _MakeLayer("Base", "1", this.layers.base_material);
    _MakeLayer("Equipment", "2", this.layers.equipment_material);
    _MakeLayer("Traversal", "3", this.layers.traversal_material);
  }

  updateGUI() {
    if (this.gui) {
      var controllers: any[] = this.gui.controllersRecursive();
      controllers.forEach(c => c.updateDisplay());
    }
  }

  isFullscreen() {
    return RunPrefixMethod(document, "FullScreen") || RunPrefixMethod(document, "IsFullScreen");
  }

  ///
  /// Animation
  ///

  private animate() {
    if (this.animate_camera) {
      var elapsed = (new Date().getTime() - this.animate_start) / 1000; //seconds
      var new_target_world: THREE.Vector3;

      if (elapsed < this.animate_duration) {
        var diff = this.animate_target.clone().sub(this.animate_origin);
        var dir = diff.clone().normalize();
        var posLength = diff.length() * (elapsed / this.animate_duration);

        new_target_world = dir.multiplyScalar(posLength).add(this.animate_origin);
      } else {
        //Finished animating
        new_target_world = this.animate_target.clone();
        this.animate_camera = false;
      }

      var new_target_local = this.controls.target_helper.parent.worldToLocal(new_target_world);
      this.controls.target.set(new_target_local.x, new_target_local.y, new_target_local.z);
      this.updateFocusHeightParam(new_target_world.y);

      if (!this.animate_camera) {
      }
    } else {
      //Bit of a hack to not make camera directly control height
      var vertical_move = this.controls.animateFromKeys() / 100;
      this.updateFocusHeightParam(this.params.focusHeight + vertical_move);
    }

    this.stats.update();
  }

  private refreshShipLoadout() {
    if (!this.hierarchy_root) return;
    this.hierarchy_root = [...this.hierarchy_root];
  }

  private updateShipLoadout() {
    if (!this.loaded_ship.bound_loadout_root)
      return;

    this.hierarchy_root = this.loaded_ship.bound_loadout_root.buildLoadoutTree(LoadoutGroupingMode.Ship, LoadoutDisplayMode.Editable, this.hierarchy_root);

    this.updateData();
  }

  private updateFocusHeightParam(new_focus_height: number, update_gui: boolean = true) {
    this.layers.base_material.setSliceHeight(new_focus_height);
    this.layers.equipment_material.setSliceHeight(new_focus_height);
    this.layers.traversal_material.setSliceHeight(new_focus_height);

    var yDelta = this.focusHeightRoot.position.y - new_focus_height;
    this.focusHeightRoot.position.y = new_focus_height;
    this.controls.focusHeight = new_focus_height;
    this.camera.position.y -= yDelta;
    this.controls.update();
    this.params.focusHeight = new_focus_height;

    if (update_gui) {
      this.updateGUI();
    }
  }

  private animateTo(target_world_position: THREE.Vector3) {
    this.animate_origin = this.controls.target_helper.getWorldPosition(new Vector3(0, 0, 0)).clone();
    this.animate_target = target_world_position.clone();

    this.animate_start = new Date().getTime();
    this.animate_camera = true;
  }

  ///
  /// Helper Functions
  ///

  expandTo(node: BoundLoadoutNode) {
    var _searchHierarchy = (items: TreeNode[]): void => {
      if (!items) return;
      items.forEach(item => {
        if (result) return;
        if (item.data == node) {
          result = item;
        }
        if(!result)
          _searchHierarchy(item.children);
      });
    }

    var _doExpand = (_node: TreeNode): void => {
      //Expand recursively upwards by parents
      var expand_node = _node.parent;
      while (expand_node) {
        expand_node.expanded = true;
        expand_node = expand_node.parent;
      }
    }

    var result: TreeNode = null;
    _searchHierarchy(this.hierarchy_root);

    this.setSelectedNode(result);
    if (result) {
      _doExpand(result);
    }
  }

  setSelectedNode(node: TreeNode) {
    if (this.selected_previous && this.selected_previous.data) {
      var old_bound: BoundLoadoutNode = this.selected_previous.data;
      if (old_bound.equipment_object) {
        this.setNestedHighlight(old_bound.equipment_object, false);
      } else if (old_bound.object) {
        this.setNestedHighlight(old_bound.object, false);
      }
    }

    //This will usually be set by explicit UI interaction
    //But set explicity for programmatic udpates
    this.selected_bound = node;

    if (node && node.data) {
      var new_bound: BoundLoadoutNode = node.data;

      if (new_bound.equipment_object) {
        this.setNestedHighlight(new_bound.equipment_object, true);
      } else if (new_bound.object) {
        this.setNestedHighlight(new_bound.object, true);
      }
    }

    this.selected_previous = node;
  }

  onFirstGameDataLoaded() {

    var ship_keys_sorted = Object.keys(this.gameData.ShipKeyMap).sort();
    for (var i = 0; i < ship_keys_sorted.length; i++) {
      var key = ship_keys_sorted[i];
      this.params.shipOptions.push(key);
    }

    this.sceneSetup();

    this.tree_node = [this.convertObject3d(this.scene)];
  }

  ngAfterViewInit() {
  }
}

class ShipHullRenderPass extends Pass {
  public material: CustomMaterial = new CustomMaterial();

  constructor(private scene: THREE.Scene, private camera: THREE.Camera, public hull_layer: number) {
    super();
  }

  render(renderer: WebGLRenderer, inputBuffer, outputBuffer, deltaTime, stencilTest) {
    renderer.setRenderTarget(outputBuffer);
    renderer.setClearColor(0x000000, 1.0);
    renderer.clear();

    //Phase 1. Render hologram of ship extrior shape
    //Explicity clear the depth bufffer to allow the render to just draw on top
    if (this.material.hologramEnabled) {

      var old_camera_mask = this.camera.layers.mask;

      this.camera.layers.disableAll();
      this.camera.layers.enable(this.hull_layer);

      this.scene.overrideMaterial = this.material;

      renderer.render(this.scene, this.camera);
      renderer.clearDepth();

      this.scene.overrideMaterial = null;

      this.camera.layers.mask = old_camera_mask;
    }

    //Phase 2. Render primary scene on top of hull hologram.
    var oldBackground = this.scene.background;
    this.scene.background = null;

    renderer.render(this.scene, this.camera);

    this.scene.background = oldBackground;

    this.needsSwap = true;
  }
}

class CustomMaterial extends ShaderMaterial {

  get hologramEnabled(): boolean {
    return this.uniforms.enabled.value;
  }
  get hologramColor(): THREE.Color {
    return this.uniforms.color.value;
  }
  get hologramAlpha(): number {
    return this.uniforms.alpha.value;
  }

  set hologramEnabled(value: boolean) {
    this.uniforms.enabled.value = value;
  }
  set hologramColor(value: THREE.Color) {
    this.uniforms.color.value = value;
  }
  set hologramAlpha(value: number) {
    this.uniforms.alpha.value = value;
  }


  constructor() {
    super({
      uniforms: {
        weights: new Uniform(new Vector3()),
        enabled: { value: true },
        color: { value: new THREE.Color(0x0C2D48) },
        alpha: { value: 0.2 }
      },

      toneMapped: false,
      depthWrite: false,
      depthTest: false,
      transparent: true,

      vertexShader: `
uniform vec3 weights;


void main() {
      vec4 modelViewPosition = modelViewMatrix * vec4(position, 1.0);
      gl_Position = projectionMatrix * modelViewPosition; 

}`,
      fragmentShader: `
uniform vec3 color;
uniform float alpha;

void main() {
    gl_FragColor = vec4(color.rgb, alpha);

}`
    })
  }
}

interface CameraView {
  left: number;
  bottom: number;
  width: number;
  height: number;
  background: THREE.Color;
  eye: number[];
  up: number[];
  fov: number;
  aspect: number;
  camera: THREE.Camera;
  updateCamera(camera, scene, mouseX, mouseY): void;
}

//Stupid fullscreen shenanigans
var pfx = ["webkit", "moz", "ms", "o", ""];
function RunPrefixMethod(obj, method) {

  var p = 0, m, t;
  while (p < pfx.length && !obj[m]) {
    m = method;
    if (pfx[p] == "") {
      m = m.substr(0, 1).toLowerCase() + m.substr(1);
    }
    m = pfx[p] + m;
    t = typeof obj[m];
    if (t != "undefined") {
      pfx = [pfx[p]];
      return (t == "function" ? obj[m]() : obj[m]);
    }
    p++;
  }

}
