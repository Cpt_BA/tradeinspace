import { Loadout, LoadoutSlot } from '../../data/loadout';
import { TreeNode } from 'primeng/api';
import * as THREE from 'three';
import { applyMaterial, disposeObject } from './ship-loader.component';
import { GameDatabase, GameEquipment } from '../../data/generated_models';
import { flattenRecursive, Group, groupSet } from '../../utilities/utilities';
import { GameDatabaseFull } from '../../data/game_data';
import { LayerConfiguration } from '../../data/client_models';

export class BoundLoadoutNode {
  public children: BoundLoadoutNode[] = [];
  public hidden: boolean = false;
  public loaded: boolean = false;
  public loading: boolean = false;
  public equipment_object?: THREE.Object3D = null;
  public id: string;

  private ignored: boolean = false;

  constructor(
    public slot: LoadoutSlot,
    public name: string,
    public object: THREE.Object3D,
    public equipment?: GameEquipment) {
    //Save a bit of calculation when computing visibility groups
    this.calcVisible();
    this.id = object.uuid
  }

  static ignoreTypes: string[] = [
    "Paints", "Door", "Room/", "Door/", "Flair_Cockpit/Flair_Static",
    "Flair_Cockpit/Flair_Hanging", "TargetSelector/", "Display", "SeatDashboard",
    "SeatAccess", "Usable/", "Container"
  ];

  private calcVisible() {
    this.ignored = this.slot.types.some(t => BoundLoadoutNode.ignoreTypes.includes(t));
    if (this.equipment) {
      this.ignored = this.ignored || BoundLoadoutNode.ignoreTypes.includes(this.equipment.AttachmentType)
    }
  }

  public UpdateChildren() {
    this.children = [];

    if (this.equipment && this.equipment.PortLayout) {
      var equipment = new BoundLoadoutNode(this.slot.children[0], this.equipment.Name, this.object);
      equipment.UpdateChildren();
      this.children.push(equipment);
    } else {
      this.slot.children.forEach(slot_child => {
        var matching_obj: THREE.Object3D = BoundLoadoutNode.SearchForNode(slot_child.name, this.object, true);
        if (matching_obj) {
          this.children.push(BoundLoadoutNode.CreateFromSlot(slot_child, matching_obj));
        }
      });
    }
  }

  public searchObject(object: THREE.Object3D): BoundLoadoutNode {
    if (this.object == object || this.equipment_object == object)
      return this;

    for (var key in this.children) {
      var result = this.children[key].searchObject(object);
      if (result)
        return result;
    }

    return null;
  }

  public changeComponent(equipment: GameEquipment) {
    if (this.equipment_object) {
      //Clear loaded flag to make parent valid target to load again
      //At this level in the hierarchy, we are still part of the ship
      //just a single vertex essentially for the position / orientation of the hardpoint position
      //this.object.userData.is_loaded = false;
      //We can'e dispose of equipment_object, as that would invalidate it for cache usage later
      //So simply detach it from it's parent and dispose of it
      this.equipment_object.parent.userData.is_loaded = false;
      this.equipment_object.parent.remove(this.equipment_object);
      this.equipment_object.removeFromParent();
      this.equipment_object = null;
    }
    this.loaded = false;
    this.equipment = equipment;
    this.slot.attachComponent(equipment);
    this.calcVisible();
    this.UpdateChildren();
  }

  public applyLayers(layers: LayerConfiguration) {
    //NOTE: These are applying recusrively, which is a bit redundant and expensive
    //but the hierarchy here doesn't exactly match the ship hierarchy 1:1, so we leave recursive on (default)
    if (this.object) {
      if (this.equipment_object && this.isVisible(LoadoutDisplayMode.Editable)) {
        applyMaterial(this.equipment_object, layers.equipment_material);
      } else  if (this.equipment && this.equipment.AttachmentType == "Door") {
        applyMaterial(this.object, layers.traversal_material);
      } else {
        BoundLoadoutNode.applyLayersSimple(this.object, layers);
      }
    }

    this.children.forEach(c => c.applyLayers(layers));
  }

  public static applyLayersSimple(scene_object: THREE.Object3D, layers: LayerConfiguration) {
    var _CheckTraversalTag = (val: string): boolean => {
      var lower = val.toLowerCase();
      return lower.includes("traverse") || lower.includes("navlink")
    }



    scene_object.traverse(obj => {
      var tags: string = obj.userData.TIS_Tags ? obj.userData.TIS_Tags : "";
      if (_CheckTraversalTag(tags)) {
        applyMaterial(obj, layers.traversal_material);
      }
    });
  }

  static CreateRootFromEquipment(gameData: GameDatabaseFull, equipment: GameEquipment, object: THREE.Object3D) {
    if (!equipment.PortLayout)
      return null;

    var loadout = Loadout.FromEquipment(gameData, equipment);

    return BoundLoadoutNode.CreateFromSlot(loadout.root_slot, object);
  }

  static CreateFromSlot(slot_child: LoadoutSlot, scene_object: THREE.Object3D): BoundLoadoutNode {
    var child_node = new BoundLoadoutNode(slot_child, slot_child.name_group.group_name, scene_object, slot_child.attached_equipment);
    child_node.UpdateChildren();
    return child_node;
  }

  static SearchForNode(search_name: string, from_root: THREE.Object3D, allow_recursive: boolean = true): THREE.Object3D {
    var result: THREE.Object3D = null;
    var lower_search_name = search_name.toLowerCase();
    var _InnerSearch = (item: THREE.Object3D): void => {
      //console.log("Searching: ", item.children)
      var matches: THREE.Object3D[] = item.children.filter(c => c.name.toLowerCase() == lower_search_name);
      if (matches.length > 0) {
        result = matches[0];
        return;
      }
      if (allow_recursive) {
        item.children.forEach(c => {
          if (!result) {
            _InnerSearch(c);
          }
        });
      }
    }

    _InnerSearch(from_root);
    return result;
  }

  //Treenode building

  protected buildTreeNode(parent?: TreeNode): TreeNode {
    return {
      label: this.slot.name,
      data: this,
      children: [],
      parent: parent
    }
  }

  protected static buildDummyTreeNode(label: string, parent?: TreeNode): TreeNode {
    return {
      label: label,
      data: null,
      children: [],
      parent: parent
    }
  }

  protected static copyTreeVisibility(originalTree: TreeNode[], newTree: TreeNode[]) {
    var _CopySettings = (From: TreeNode, To: TreeNode) => {
      To.expanded = From.expanded;
    }

    var _SearchAndCopy = (A: TreeNode, B: TreeNode[]) => {
      for (var i in B) {
        if (B[i].label == A.label) {
          _CopySettings(A, B[i]);
          if (A.children.length > 0 && B[i].children.length > 0) {
            A.children.forEach(_a => _SearchAndCopy(_a, B[i].children))
          }
        }
      }
    }

    originalTree.forEach(original => _SearchAndCopy(original, newTree));
  }

  //TODO: Reference loaded root
  public buildLoadoutTree(grouping: LoadoutGroupingMode, display: LoadoutDisplayMode, original: TreeNode[] = null): TreeNode[] {
    var results: TreeNode[];
    switch (grouping) {
      case LoadoutGroupingMode.Ship:
        //Ignore top-level pseudo-node
        results = this.buildShipModelTree(display).children;
        break;
      case LoadoutGroupingMode.Flat:
        results = this.buildFlatLoadoutTree(display);
        break;
      case LoadoutGroupingMode.Type:
        results = this.buildTypeLoadoutTree(display);
        break;
    }
    if (original)
      BoundLoadoutNode.copyTreeVisibility(original, results);
    return results;
  }

  public isVisible(displayFilter: LoadoutDisplayMode): boolean {
    if (this.ignored) return false;

    //If slot has no types defined, we hide it. SUS!
    //We can't set ignore because then root would end up hard-ignored
    //Returning false here is a soft ignore that allows it to be in the UI if it has visible children
    if (this.slot.types.length == 0) {
      return false;
    }

    switch (displayFilter) {
      case LoadoutDisplayMode.All:
        return true;
      case LoadoutDisplayMode.Editable:
        return this.slot.editable;
      case LoadoutDisplayMode.Equipped:
        return this.slot.attached_equipment != null;
    }
  }

  protected filteredChildren(displayFilter: LoadoutDisplayMode): BoundLoadoutNode[] {
    return this.children.filter(c => {
      if (c.ignored)
        return false;

      if (c.isVisible(displayFilter))
        return true;

      var nestedChildren = c.filteredChildren(displayFilter)
      //Otherwise show if we have visible children
      return nestedChildren.length != 0;
    });
  }

  protected buildShipModelTree(displayFilter: LoadoutDisplayMode): TreeNode {
    var _MakeNode = (_node: BoundLoadoutNode, _parent: TreeNode = null) => {
      var element = _node.buildTreeNode(_parent);

      element.children = _node
        .filteredChildren(displayFilter)
        .map(c => _MakeNode(c, element));

      return element;
    }

    return _MakeNode(this);
  }

  private buildFlatLoadoutTree(displayFilter: LoadoutDisplayMode) : TreeNode[] {
    var flatChildren = flattenRecursive<BoundLoadoutNode>(this, (c: BoundLoadoutNode) => c.filteredChildren(displayFilter));
    console.log(flatChildren);
    return flatChildren.map(c => c.buildTreeNode());
  }

  private buildTypeLoadoutTree(displayFilter: LoadoutDisplayMode): TreeNode[] {
    var flatChildren = flattenRecursive<BoundLoadoutNode>(this, (c: BoundLoadoutNode) => c.filteredChildren(displayFilter));
    console.log(flatChildren);
    var typeGroups = groupSet(flatChildren, c => c.slot.types.join("|"))
    var typeNodes = [];

    for (var typeName in typeGroups) {
      //Dummy node for equipment root
      var typeDummy = BoundLoadoutNode.buildDummyTreeNode(typeName);
      var group: BoundLoadoutNode[] = typeGroups[typeName];

      group.forEach(node => {
        typeDummy.children.push(node.buildTreeNode(typeDummy));
      });

      typeNodes.push(typeDummy);
    }

    return typeNodes;
  }
}

export enum LoadoutGroupingMode {
  //Native ship-based hierarchy
  Ship,
  //Flattened ship hierarchy
  Flat,
  //Group loadout slots by slot type
  Type,
  //Group components by floor of the ship.
  //This one is tricky because we have to convert floors to Y-coordiante ranges
  Floor,
  //Group components by room
  //Also tricky because we have to test ship room areas containing components
  Room
}

export enum LoadoutDisplayMode {
  //Only loadout slots that are editable are visible
  Editable,
  //Only those with equipped items are visible
  Equipped,
  //EVERYTHING is visible
  All
}
