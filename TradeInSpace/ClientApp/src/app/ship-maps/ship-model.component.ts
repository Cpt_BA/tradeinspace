import * as THREE from 'three';
import { TreeNode } from 'primeng/api';

import { GameEquipment, GameShip } from '../../data/generated_models';
import { Loadout, LoadoutSlot } from '../../data/loadout'
import { SliceMaterial } from '../../three_plugins/SliceMaterial';
import { applyMaterial, disposeObject, ShipLoadManager } from './ship-loader.component';
import { BoundLoadoutNode } from './ship-loadout.component';
import { BufferGeometry } from 'three';
import { LayerConfiguration } from '../../data/client_models';

export class ShipModel {

  private parent_container: THREE.Object3D;
  private parent_attach: boolean = false;

  private onModelLoadProgress: (loaded: number, total: number, progress: number) => void = null;
  private onFinishedLoading: () => void = null;
  private onSubModelLoaded: () => void = null;

  public ship_root!: THREE.Object3D;
  public entityCache: Record<string, THREE.Object3D> = {};
  public bound_loadout_root: BoundLoadoutNode;

  constructor(public loadout: Loadout,
    private load_manager: ShipLoadManager,
    public layer_config: LayerConfiguration) {
  }

  //
  // Public Functions
  //

  startLoading(attach_root: THREE.Object3D,
    immediate_attach: boolean = true,
    equipment_attach: boolean = true,
    onModelLoadProgress: (loaded: number, total: number, progress: number) => void = null,
    onFinishedLoading: () => void = null,
    onSubModelLoaded: () => void = null)
  {
    this.parent_container = attach_root;
    this.parent_attach = immediate_attach;
    this.onModelLoadProgress = onModelLoadProgress;
    this.onFinishedLoading = onFinishedLoading;
    this.onSubModelLoaded = onSubModelLoaded;

    this.load_manager.resetLoader();
    this.load_manager.onLoadComplete = () => {
      if (this.onModelFinished) {
        this.onModelFinished();
      }
    };
    this.load_manager.onItemFinished = (loaded, total, error) => {
      if (this.onModelLoadProgress) {
        this.onModelLoadProgress(loaded, total, 1);
      };
    }
    this.load_manager.onItemProgress = (loaded, total, error, progress) => {
      if (this.onModelLoadProgress) {
        this.onModelLoadProgress(loaded, total, progress);
      }
    }

    this.loadShipLoadout(equipment_attach)
  }

  dispose() {
    //for (var key in this.loader.loaderCache)
    //  this.disposeObject(this.loader.loaderCache[key]);

    for (var key in this.entityCache)
      disposeObject(this.entityCache[key]);

    this.ship_root.children.forEach(c => { disposeObject(c); })
    disposeObject(this.ship_root);

    //this.loader.loaderCache = {};
    this.entityCache = {};
    if (this.parent_container.userData.is_loaded) {
      delete this.parent_container.userData.is_loaded;
    }
  }

  //
  // Callbacks
  //

  private onModelFinished(): void {
    //console.log("Model Finished!", this.entityCache, this.bound_loadout_root);

    this.hullPass(this.ship_root);

    applyMaterial(this.bound_loadout_root.object, this.layer_config.base_material);
    this.bound_loadout_root.applyLayers(this.layer_config);

    for (var ent_guid in this.entityCache) {
      var entity_obj = this.entityCache[ent_guid];
      if (entity_obj.userData.TIS_TransitStops) {

      }
    }

    if (this.onFinishedLoading)
      this.onFinishedLoading();

    this.load_manager.onLoadComplete = null;
    this.load_manager.onItemFinished = null;
  }

  private onMainModelLoaded(modelObject: THREE.Object3D, applyEquipment: boolean): void {
    console.log("Post-Bind: ", this.bound_loadout_root);

    //If we load the model for the root object, update loadout to reflect
    this.bound_loadout_root.UpdateChildren();
    applyMaterial(this.bound_loadout_root.object, this.layer_config.base_material);
    this.bound_loadout_root.applyLayers(this.layer_config);

    if (applyEquipment) {
      this.populateShipLoadoutSlot(this.bound_loadout_root, (load) => {
        //load.UpdateChildren();
      })
    }
  }

  private loadShipLoadout(applyEquipment: boolean = true): void {
    //NOTE: There is a bit of a loss of information between the model structure and loadout hierarchy
    //This is fine since the model is quite verbose anyway.
    this.loadVehicleSkeleton(this.loadout.from_ship, null, (obj) => {
      this.ship_root = obj;
      //There should be a single node with type model at the root of the loaded skeleton
      var model_root = obj.children.filter(c => c.userData.TIS_Type == "Model");
      if (model_root.length != 1 || !model_root[0].userData.TIS_Type) {
        console.error("Invalid Model Loaded");
        return;
      }
      
      this.bound_loadout_root = new BoundLoadoutNode(this.loadout.root_slot, this.loadout.name, model_root[0], this.loadout.equipment);


      if (this.parent_attach && this.parent_container)
        this.parent_container.add(this.ship_root)

      //Load root equipment (main ship hull) explicitly
      this.loadEquipmentModel(this.bound_loadout_root, (_loadedModel) => {
        this.onMainModelLoaded(_loadedModel, applyEquipment);
        this.exploreModel(_loadedModel, (_path, _obj) => {
          //We target parent here, because what our loaded object is attached to contains metadata for us
          BoundLoadoutNode.applyLayersSimple(_obj.parent, this.layer_config);
        });
      });

      //Handle the fact that the ship itself will be bound to root
      if (this.bound_loadout_root.equipment) {
        this.bound_loadout_root.equipment = null;
        model_root[0].userData.model_path = null;
      }

      //Explore the skeleton model for model path cues
      //Make sure to start exploration at the root of the loaded object.
      this.exploreModel(obj, (_path, _obj) => {
        //This is a bit of a hack to apply material configurations to embedded references
        //We target parent here, because what our loaded object is attached to contains metadata for us
        BoundLoadoutNode.applyLayersSimple(_obj.parent, this.layer_config);
      });
    });
  }

  public populateShipLoadoutSlot(bound: BoundLoadoutNode, onModelLoaded: (loadedBound: BoundLoadoutNode) => void = null): void {
    //Avoid loading ship root model overtop. Since the skeleton references it already, it is already queued
    var _InnperPopulateSlot = (_bound: BoundLoadoutNode) => {
      //console.log("Populate: ", _bound);
      if (_bound.equipment) {
        var doMount = true;

        if (_bound.loaded == true || _bound.loading == true) {
          //console.log("Skipping populate of already loaded node: ", _bound);
          doMount = false;
        }

        if (_bound.equipment.SCRecordID == this.loadout.from_ship.SCRecordID) {
          //console.log("Ignoring ship root equipment.");
          doMount = false;
        }

        if (!_bound.slot.visible) {
          doMount = false;
        }

        if (doMount) {
          this.loadEquipmentModel(_bound, (obj) => {
            if (onModelLoaded)
              onModelLoaded(_bound);
            //This will ignore the already-loaded element and search children
            _InnperPopulateSlot(_bound);
          });
        }
      }

      _bound.children.forEach(c => _InnperPopulateSlot(c));
    }

    _InnperPopulateSlot(bound);
  }

  private generateAreaObject(node: THREE.Object3D) {
    var bottom: number = parseFloat(node.userData.TIS_Area_Bottom);
    var height: number = 0.5; //parseFloat(node.userData.TIS_Area_Height);
    var points: number[] = node.userData.TIS_Area_Points.a;

    var shape = new THREE.Shape();

    if (points.length >= 4) {
      var vec_pts: THREE.Vector2[] = [];
      for (var i = 0; i < points.length; i += 2) {
        vec_pts.push(new THREE.Vector2(points[i], points[i + 1]));
      }
      shape.setFromPoints(vec_pts);

      var geometry = new THREE.ExtrudeGeometry(shape, {
        depth: height,
        bevelEnabled: false
      })

      var area_mesh = new THREE.Mesh(geometry, this.layer_config.traversal_material);
      area_mesh.position.z = bottom;
      area_mesh.layers.disableAll();
      area_mesh.layers.enable(this.layer_config.navigation_pass);
      node.add(area_mesh);
    }
  }

  static cdnRoot: string = "https://tis-models.b-cdn.net";

  private loadEquipmentModel(bound: BoundLoadoutNode, loaded: (obj: THREE.Object3D) => void = null) {
    var equipment: GameEquipment = bound.equipment;
    var parent: THREE.Object3D = bound.object;
    
    bound.loading = true;
    this.load_manager.startModelRequest(`${ShipModel.cdnRoot}/equipment/${equipment.SCRecordID}.fbx`,
      parent,
      (loaded_obj: THREE.Group) => {
        bound.loading = false;
        bound.loaded = true;
        bound.equipment_object = loaded_obj;
        bound.UpdateChildren();
        bound.applyLayers(this.layer_config);

        //Execute callback with the new model
        if (loaded) loaded(loaded_obj);

        //NOTE: exploreModel() isn't needed as equipment models *should* be fully "flattened" with no references
        this.hullPass(loaded_obj);
      });
  }

  private loadVehicleSkeleton(ship: GameShip, parent: THREE.Object3D, loaded: (obj: THREE.Object3D) => void = null) {
    //Load ship data
    console.log("Loading Ship: ", ship.Name);

    this.load_manager.startModelRequest(`${ShipModel.cdnRoot}/vehicles/${ship.SCRecordID}.fbx`,
      parent,
      (loaded_obj: THREE.Group) => {
        console.log("Skeleton-Model Loaded: ", loaded_obj);

        this.ship_root = loaded_obj;

        applyMaterial(loaded_obj, this.layer_config.base_material);
        loaded_obj.traverse(obj_child => {
          //Entities get registered for later referencing
          if ("TIS_EntityID" in obj_child.userData)
            this.entityCache[obj_child.userData["TIS_EntityID"]] = obj_child;

          //
          if ("TIS_Area_Bottom" in obj_child.userData &&
            "TIS_Area_Height" in obj_child.userData &&
            "TIS_Area_Points" in obj_child.userData) {
            this.generateAreaObject(obj_child);
          }
        });

        if (loaded) loaded(loaded_obj);
      });
  }

  private loadModel(modelPath: string, parent: THREE.Object3D, loaded: (obj: THREE.Object3D) => void = null) {
    modelPath = modelPath.replace(".cga", ".fbx").replace(".cgf", ".fbx");

    this.load_manager.startModelRequest(`${ShipModel.cdnRoot}/model/${escape(modelPath)}`,
      parent,
      (loaded_obj: THREE.Group) => {
        applyMaterial(loaded_obj, this.layer_config.base_material);

        if (loaded) loaded(loaded_obj);
      });
  }

  private exploreModel(loaded_node: THREE.Object3D, loaded: (model_path: string, obj: THREE.Object3D) => void = null): void {
    var check_object = (obj: THREE.Object3D) => {
      if (obj.userData.model_path) {
        var model_path: string = obj.userData.model_path;
        if (obj.children.length != 0) {
          console.warn("Loading model on root with children already!");
        }

        this.loadModel(model_path, obj, (_obj) => { if (loaded) loaded(model_path, _obj); });
      }

      obj.children.forEach(child => check_object(child));
    }

    check_object(loaded_node);
  }

  private hullPass(object: THREE.Object3D) {
    //Ignore hardpoints (equipment) and Area nodes
    if (object.userData.node_type != "Hardpoint" && !("TIS_Area_Points" in object.userData)) {

      //Enable the layer for the hull hologram render pass
      object.layers.enable(this.layer_config.hull_pass);
      object.children.forEach(c => this.hullPass(c));
    }
  }

}


