import { FBXLoader } from "../../three_plugins/FBXLoader";
import * as THREE from 'three';
import { Mesh } from "three";
import { EventEmitter } from "@angular/core";

export class ShipLoadManager {

  private loader: FBXLoader;
  private loaderRunning: boolean = false;
  private loaderQueue: LoaderQueueEntry[] = [];
  private loadingItem: LoaderQueueEntry = null;

  private loadedCount: number = 0;
  private errorCount: number = 0;
  private downloadedCount: number;

  public loaderCache: Record<string, THREE.Object3D> = {};
  public onLoadComplete!: () => void;
  public onItemFinished!: (loaded: number, total: number, error: number) => void;
  public onItemProgress!: (loaded: number, total: number, error: number, progress: number) => void;

  public constructor(
    onLoadComplete: () => void = null,
    onItemFinished: (loaded: number, total: number) => void = null,
    onItemProgress: (loaded: number, total: number, progress: number) => void = null)
  {
    const loadingManager = new THREE.LoadingManager(() => {
      //Request the next model if there is one
      this.requestModel();
    });
    //this.loader = new ColladaLoader(loadingManager);
    this.loader = new FBXLoader(loadingManager);
    this.onLoadComplete = onLoadComplete;
    this.onItemFinished = onItemFinished;
    this.onItemProgress = onItemProgress;
  }

  startModelRequest(url: string, parent: THREE.Object3D, loadedCallback: (obj: THREE.Object3D) => void): void {
    this.loaderQueue.push({
      url: url,
      callback: loadedCallback,
      attach_to: parent
    });
    this.requestModel();
  }

  resetLoader(clear_cache: boolean = true): void {
    this.loadedCount = 0;
    this.errorCount = 0;
    if (clear_cache) {
      for (var key in this.loaderCache) {
        disposeObject(this.loaderCache[key]);
      }
      this.loaderCache = {};
    }
  }

  private updateItemCount(): void {
    if (this.onItemFinished)
      this.onItemFinished(this.loadedCount, this.loadedCount + this.loaderQueue.length, this.errorCount);
  }

  private updateItemProgress(percent: number): void {
    if (this.onItemProgress)
      this.onItemProgress(this.loadedCount, this.loadedCount + this.loaderQueue.length, this.errorCount, percent);
  }

  //Main loader loop

  private requestModel(): void {
    if (this.loaderRunning)
      return;
    this.loaderRunning = true;

    var modelPath = "";

    while (!modelPath) {
      if (this.loaderQueue.length == 0) {
        if (this.onLoadComplete) this.onLoadComplete();
        this.loaderRunning = false;
        return;
      }

      //Dequeue elements until we have a model to load
      this.loadingItem = this.loaderQueue[0];
      this.loaderQueue.splice(0, 1);

      //Check for flag that this has already been loaded
      if (this.loadingItem.attach_to &&
        this.loadingItem.attach_to.userData.is_loaded) {
        console.warn("Attempt to double-load model: ", this.loadingItem);
        modelPath = null;
      } else {
        modelPath = this.loadingItem.url;
      }
    }

    this.updateItemProgress(0);

    var _FinalizeRecord = (final_obj: THREE.Object3D): void => {
      if (this.loadingItem.attach_to) {
        this.loadingItem.attach_to.userData.is_loaded = true;
        this.loadingItem.attach_to.add(final_obj);
      }
      this.loadingItem.callback(final_obj);
      this.loaderRunning = false;
      this.requestModel();
    };

    if (this.loadingItem.url in this.loaderCache) {
      this.loadedCount++;
      this.updateItemCount();

      if (this.loaderCache[this.loadingItem.url] != null) {
        //Model was already loaded, just clone and use
        var clonedScene = this.loaderCache[this.loadingItem.url].clone();
        _FinalizeRecord(clonedScene);
      } else {
        //Model was previously attempted and failed, simply cache the failure and move on.
        this.loaderRunning = false;
        this.requestModel();
      }
    } else {
      //Request that item
      this.loader.load(this.loadingItem.url,
        (fbx_data: THREE.Object3D) => {
          this.loadedCount++;
          this.downloadedCount++;
          this.updateItemCount();

          //Compute normals, the models do not have them to reduce file size as much as possible.
          fbx_data.traverse(c => { if (c instanceof Mesh) c.geometry.computeVertexNormals(); })

          this.loaderCache[this.loadingItem.url] = fbx_data;
          _FinalizeRecord(fbx_data);
        },
        (progress: ProgressEvent) => {
          if (progress.loaded != progress.total) {
            this.updateItemProgress(progress.loaded / progress.total);
          }
        },
        (error) => {
          console.error("File read error", error);
          this.errorCount++;
          this.updateItemCount();

          this.loaderCache[this.loadingItem.url] = null;
          //Continue loading. TODO: Feature flag
          this.loaderRunning = false;
          this.requestModel();
        }
      );
    }
  }
}

export interface LoaderQueueEntry {
  url: string;
  callback: (obj: THREE.Object3D) => void;
  attach_to: THREE.Object3D;
}

export function disposeObject(obj: THREE.Object3D | THREE.Mesh): void {
  if (obj.children.length > 0) {
    for (var x = obj.children.length - 1; x >= 0; x--) {
      disposeObject(obj.children[x]);
    }
  }

  obj.removeFromParent();
}

export function applyMaterial(object: THREE.Object3D, material: THREE.Material, recursive: boolean = true): void {
  if (object instanceof THREE.Mesh) {
    object.material = material;
  }
  if (recursive) {
    object.children.forEach(c => applyMaterial(c, material, recursive));
  }
}
