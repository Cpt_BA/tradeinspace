import { Component, OnInit, Input, OnChanges, SimpleChanges, Output, EventEmitter, ChangeDetectorRef, ViewChild } from '@angular/core';
import { SelectItemGroup, SelectItem, MessageService } from 'primeng/api';
import { GameEquipment, GameShip, LocationType, Manufacturer, PadSize, SystemLocation } from '../../../data/generated_models';
import { EquipmentProperties } from '../../../data/client_models';
import { DataCacheService } from '../../../data/data_cache';
import { GameDatabaseFull } from '../../../data/game_data';
import { Loadout, LoadoutSlot } from '../../../data/loadout';
import { CoordinateResult, ModelUtils } from '../../../utilities/model_utils';
import { Group, groupSet } from '../../../utilities/utilities';
import { TISBaseComponent } from '../../tis_base.component';
import { ActivatedRoute } from '@angular/router';
import { ClientData } from '../../../data/client_data';
import { LoadoutManagerService } from '../../../data/loadout_manager';
import { OverlayPanel } from 'primeng/overlaypanel';
import { Coordinate, Vector3 } from '../../../data/coordinates';

@Component({
  selector: 'where-am-i',
  templateUrl: './where-am-i.component.html',
  styleUrls: ['./where-am-i.component.css']
})
export class WhereAmIComponent extends TISBaseComponent implements OnChanges {
  ClientData: typeof ClientData = ClientData;

  has_coordinate: boolean = false;
  has_error: boolean = false;
  last_coordinate: string = null;

  global_julian: Vector3 = Vector3.Zero;
  global_current: Vector3 = Vector3.Zero;

  local_julian: Vector3 = Vector3.Zero;
  local_current: Vector3 = Vector3.Zero;

  altitude: number = 0;
  latitude: number = 0;
  longitude: number = 0;

  local_body: SystemLocation = null;
  nearest_poi: SystemLocation = null;

  body_distance: number = 0;
  poi_distance: number = 0;

  constructor(httpRoute: ActivatedRoute,
    private loadouts: LoadoutManagerService,
    dataStore: DataCacheService,
    cdr: ChangeDetectorRef,
    toast: MessageService) {
    super(httpRoute, dataStore, cdr, toast);
  }

  fnPageVisChange: EventListenerObject;

  onFirstGameDataLoaded() {
    this.fnPageVisChange = this.pageVisChange.bind(this);
    document.addEventListener("visibilitychange", this.fnPageVisChange);
    window.addEventListener("focus", this.fnPageVisChange);
  }

  ngOnDestroy() {
    document.removeEventListener("visibilitychange", this.fnPageVisChange);
    window.removeEventListener("focus", this.fnPageVisChange);
  }

  pageVisChange(event) {
    console.log(event)
    if (event.type == "visibilitychange") {
      if (!document.hidden) {
        this.checkCoordinates();
      }
    } else if (event.type == "focus") {
      this.checkCoordinates();
    }
  }

  onAllDataGameUpdated() {
    this.checkCoordinates();
  }

  panelExpanded(event: Event): void {
    this.checkCoordinates();
  }

  checkCoordinates(): void {
    this.has_coordinate = this.last_coordinate != null;
    this.has_error = false;

    if (!this.gameLoaded())
      return;

    navigator.clipboard.readText()
      .then(text => {
        if (text != this.last_coordinate) {
          let results = ModelUtils.doLocate(this.gameData, text);
          if (results) {
            this.coordinateUpdated(results);
            this.has_coordinate = true;
            this.last_coordinate = text;
          } else {
            this.last_coordinate = null;
          }
        }
      })
      .catch((err: Error) => {
        console.log("Clipboard read error", err);
        this.has_error = true;
        this.last_coordinate = null;
      });
  }

  coordinateUpdated(results: CoordinateResult): void {
    this.global_julian = results.position.globalPosition;
    this.global_current = results.position_now.globalPosition;

    this.local_julian = results.position.localPosition;
    this.local_current = results.position_now.localPosition;

    this.altitude = results.distance;
    this.latitude = results.latitude;
    this.longitude = results.longitude;

    if (results.position_now.isOnBody()) {
      this.local_body = results.body_location;
    } else {
      this.local_body = null;
    }

    var poisByDistance = this.gameData.SystemLocations
      .filter(l => !ClientData.SystemBodyLocationTypes.includes(l.LocationType))
      .sort((a, b) => {
        return this.global_julian.DistanceTo(ModelUtils.calculateLocationPositionM(a)) - this.global_julian.DistanceTo(ModelUtils.calculateLocationPositionM(b));
      });

    this.nearest_poi = poisByDistance.length > 0 ? poisByDistance[0] : null;

    console.log(poisByDistance);

    this.body_distance = this.local_body ? this.global_julian.DistanceTo(ModelUtils.calculateLocationPositionM(this.local_body)) : 0;
    this.poi_distance = this.nearest_poi ? this.global_julian.DistanceTo(ModelUtils.calculateLocationPositionM(this.nearest_poi)) : 0;
  }

  setLocation(location: SystemLocation, event: Event) {
    this.userData.current_location = location.Key;
    this.dataStore.setUserDetails(this.userData, true);
    event.preventDefault();
  }

  filterUpdated(event: Event): void {
  }

  ngOnChanges(changes: SimpleChanges): void {
  }

}
