import { Component, ViewChild, OnInit, ChangeDetectorRef, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DataCacheService } from '../../data/data_cache';
import { TradeTable, GameDatabase, SystemLocation, LocationType, ServiceFlags, PadSize } from '../../data/generated_models';
import { TreeNode, SortMeta, SelectItem, MessageService } from 'primeng/api';
import { TreeTable } from 'primeng/treetable';
import { Subscription } from 'rxjs';
import { ShowTooltipEvent } from '../../directive/tooltip-attr.directive';
import { SelectButton } from 'primeng/selectbutton';
import { serviceFlagName } from '../../pipes/display_text_pipe';
import { ModelUtils } from '../../utilities/model_utils';
import { ClientData } from '../../data/client_data';
import { TISBaseComponent } from '../tis_base.component';
import { ServiceColumn, ServiceGroup, UiUtils } from '../../utilities/ui_utils';


@Component({
  selector: 'loop-finder',
  templateUrl: './explore.component.html',
  styleUrls: ['./explore.component.css']
})
export class ExploreComponent extends TISBaseComponent implements OnInit {
  //Hack to make LocationType accesible to the html
  LocationType: typeof LocationType = LocationType;
  ServiceFlags: typeof ServiceFlags = ServiceFlags;
  ModelUtils: typeof ModelUtils = ModelUtils;
  ClientData: typeof ClientData = ClientData;
  UiUtils: typeof UiUtils = UiUtils;

  locations: TreeNode[];
  locationLookup: { [key: string]: TreeNode } = {};

  filterAllServices = false;
  filterServices: ServiceFlags = ServiceFlags.None;
  filterPadSize: PadSize = null;

  selectedLocation: TreeNode;

  @ViewChild("locationTree", { static: true }) locationTree: TreeTable;
  @ViewChild("padFilterBtn", { static: true }) padFilterButonn: SelectButton;

  serviceFilterOptions: SelectItem[];
  selectedFilterOptions: ServiceFlags[];

  sortColumns: any[] = [
    { field: 'Name', header: 'Name' },
    { field: 'LocationType', header: 'Type' }
  ];

  multiSortMeta: SortMeta[] = [
    { field: 'LocationType', order: 1 },
    { field: 'Name', order: 1 }
  ];

  serviceGroups: ServiceGroup[] = [
    {
      header: "Ships",
      description: "Various services for ships.",
      icon: "nf nf-mdi-airplane_takeoff",
      groupServices: ServiceFlags.ShipLanding | ServiceFlags.ShipClaim | ServiceFlags.ShipPurchase | ServiceFlags.ShipEquipment | ServiceFlags.ShipRental,
      services: [
        { description: "Ship Landing", icon: "nf nf-mdi-airplane_landing", service: ServiceFlags.ShipLanding },
        { description: "Ship Claiming", icon: "nf nf-mdi-airplane_takeoff", service: ServiceFlags.ShipClaim },
        { description: "Ship Purchasing", icon: "nf nf-fa-space_shuttle", service: ServiceFlags.ShipPurchase },
        { description: "Ship Equipment", icon: "nf nf-fa-gears", service: ServiceFlags.ShipEquipment },
        { description: "Ship Rental", icon: "nf nf-mdi-clock_start", service: ServiceFlags.ShipRental }
      ]
    },
    {
      header: "FPS",
      description: "Various services for your player.",
      icon: "nf nf-fa-crosshairs",
      groupServices: ServiceFlags.FPSRespawnPoint | ServiceFlags.FPSArmor | ServiceFlags.FPSWeapon | ServiceFlags.FPSEquipment | ServiceFlags.FPSClothing,
      services: [
        { description: "FPS Respawn", icon: "nf nf-fa-bed", service: ServiceFlags.FPSRespawnPoint },
        { description: "FPS Armor", icon: "nf nf-fae-layers", service: ServiceFlags.FPSArmor },
        { description: "FPS Weaponry", icon: "nf nf-fa-crosshairs", service: ServiceFlags.FPSWeapon },
        { description: "FPS Equipment", icon: "nf nf-fa-wrench", service: ServiceFlags.FPSEquipment },
        { description: "FPS Clothing", icon: "nf nf-fae-shirt", service: ServiceFlags.FPSClothing }
      ]
    },
    {
      header: "Admin",
      description: "Administrative services",
      icon: "nf nf-oct-person",
      groupServices: ServiceFlags.Trade | ServiceFlags.Refinery | ServiceFlags.Fine | ServiceFlags.GreenZone | ServiceFlags.Hacking,
      services: [
        { description: "Trading", icon: "nf nf-fa-exchange", service: ServiceFlags.Trade },
        { description: "Refining", icon: "nf nf-fa-recycle", service: ServiceFlags.Refinery },
        { description: "Fine Payment", icon: "nf nf-fa-dollar", service: ServiceFlags.Fine },
        { description: "Green/Armistice Zone", icon: "nf nf-mdi-yin_yang", service: ServiceFlags.GreenZone },
        { description: "Security Hacking", icon: "nf nf-fa-edit", service: ServiceFlags.Hacking },
      ]
    }
  ];

  constructor(httpRoute: ActivatedRoute,
    dataStore: DataCacheService,
    cdr: ChangeDetectorRef,
    toast: MessageService) {
    super(httpRoute, dataStore, cdr, toast);

    this.serviceFilterOptions = [];

    this.serviceGroups.forEach(sg => {
      sg.services.forEach(s => {
        this.serviceFilterOptions.push({
          icon: s.icon,
          value: s.service,
          label: s.description
        })
      })
    });
  }

  onAllDataGameUpdated(): void {
    this.refreshLocations();
  }

  onAllDataUserUpdated(): void {
    const currentShop = this.gameData.TradeShops
      .find(ts => ts.Key === this.userData.current_location);
    if (currentShop) {
      const shopTreeNode = this.locationLookup[currentShop.Location.ID];
      this.expandUp(shopTreeNode, true);
      this.locationTree.selection = shopTreeNode;
    }

    if (this.userData.active_ship) {
      const ship = this.gameData.ShipKeyMap[this.userData.active_ship];
      this.filterPadSize = ship.Size;
    }
  }

  public getLocationTypeName(type: LocationType): string {
    return LocationType[type];
  }

  public refreshFilter(): void {
    this.updateFilters();
    this.processFilter();
  }

  updateFilters(): void {
    this.filterPadSize = this.padFilterButonn.value;
    this.filterServices = ServiceFlags.None;
    if (this.selectedFilterOptions) {
      this.selectedFilterOptions.forEach(sfo => this.filterServices |= sfo);
    }
  }

  processFilter(): void {
    const allLocations = Object.keys(this.locationLookup).map(k => this.locationLookup[k]);

    allLocations.forEach(l => {
      l.data.keep = this.processFilterItem(l.data);
    });

    this.recurseCheckHide(this.locations);
  }

  recurseCheckHide(nodes: TreeNode[]): void {
    nodes.forEach(n => {
      if (n.children.length !== 0) {
        this.recurseCheckHide(n.children);

        n.children.forEach(c => n.data.keep |= c.data.keep);
      }
    });
  }

  processFilterItem(location: SystemLocation): boolean {
    if (this.filterPadSize) {
      const padAvailable = ModelUtils.locationHasPad(location, this.filterPadSize);

      if (!padAvailable) return false;
    }

    if (this.filterAllServices) {
      return ModelUtils.hasServices(location.ServiceFlags, this.filterServices);
    } else {
      return ModelUtils.hasAnyService(location.ServiceFlags, this.filterServices);
    }
  }

  expandUp(node: TreeNode, expand: boolean = true): void {
    node.expanded = expand;
    if (node.parent) this.expandUp(node.parent, expand);
  }

  refreshLocations(): void {
    const rootLocations = this.gameData.SystemLocations.filter(l => l.ParentLocation == null);

    //Only applies to root level.
    this.locations = rootLocations.map(rl => {
      const locNode = this.locationToTree(rl);
      locNode.expanded = true;
      return locNode;
    });
  }

  locationToTree(location: SystemLocation, parent?: TreeNode, depth: number = 0): TreeNode {
    const node: TreeNode = {
      data: location,
      parent: parent,
      key: location.Key
    }

    //Use this to filter things ourselves.
    node.data.keep = true;
    //CSS class to apply to the row based on tree depth
    node.data.class = "row-depth-" + depth;

    node.children = location.ChildLocations.map(cl => this.locationToTree(cl, node, depth + 1));
    this.locationLookup[location.ID] = node;
    return node;
  }
}
