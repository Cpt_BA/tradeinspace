import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { DataCacheService } from '../../../data/data_cache';
import { GameDatabaseFull } from '../../../data/game_data';
import { GameEquipment, GameShip, TradeShop, LocationType } from '../../../data/generated_models';
import { ClientData } from '../../../data/client_data';
import { BasicUserSettings } from '../../../data/models';
import { TISBaseComponent } from '../../tis_base.component';
import { MessageService, SortMeta } from 'primeng/api';
import { groupSet } from '../../../utilities/utilities';


@Component({
  selector: 'rental-list',
  templateUrl: './rental-list.component.html'
})
export class RentalListComponent extends TISBaseComponent implements OnInit {

  sortField: string;
  rentals: RentalDisplay[];
  rowGroupMetadata: any;

  multiSortMeta: SortMeta[] = [
    { field: 'ship.Name', order: 1 },
    { field: 'price1Day', order: 1 }
  ];

  constructor(httpRoute: ActivatedRoute,
    dataStore: DataCacheService,
    cdr: ChangeDetectorRef,
    toast: MessageService) {
    super(httpRoute, dataStore, cdr, toast);
  }

  onAllDataTradeUpdated(): void {
    this.updateRentals();
  }

  updateRentals() {
    const rentalTrades = this.tradeData.EquipmentEntries.filter(ee => ee.RentalDuration !== null)
    const shipsGrouped = groupSet(rentalTrades, ee => ee.Equipment.Key);
    const shipKeys = Object.keys(shipsGrouped);

    const allRentals: RentalDisplay[] = [];
    shipKeys.forEach(sk => {
      const shipRentals = shipsGrouped[sk];
      const ship = this.gameData.ShipKeyMap[sk];
      const shipRentalsGroups = groupSet(shipRentals, sr => sr.Station.Key)
      const locationKeys = Object.keys(shipRentalsGroups)

      locationKeys.forEach(lk => {
        const stationShipRentals = shipRentalsGroups[lk]
        const location = stationShipRentals[0].Station;
        const trade1Day = stationShipRentals.find(sr => sr.RentalDuration === 1);
        const trade3Day = stationShipRentals.find(sr => sr.RentalDuration === 3);
        const trade7Day = stationShipRentals.find(sr => sr.RentalDuration === 7);
        const trade30Day = stationShipRentals.find(sr => sr.RentalDuration === 30);

        allRentals.push({
          ship: ship,
          location: location,
          price1Day: trade1Day.BuyPrice,
          price3Day: trade3Day.BuyPrice,
          price7Day: trade7Day.BuyPrice,
          price30Day: trade30Day.BuyPrice
        });
      })
    });
    console.log(allRentals);
    this.rentals = allRentals;
  }
}

export interface RentalDisplay {
  ship: GameShip;
  location: TradeShop;
  price1Day: number;
  price3Day: number;
  price7Day: number;
  price30Day: number;
}

