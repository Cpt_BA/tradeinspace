import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { DataCacheService } from '../../../data/data_cache';
import { GameDatabaseFull } from '../../../data/game_data';
import { GameEquipment, GameShip, TradeShop, LocationType, MissionGiverEntry, SystemLocation } from '../../../data/generated_models';
import { ClientData } from '../../../data/client_data';
import { BasicUserSettings } from '../../../data/models';
import { TISBaseComponent } from '../../tis_base.component';
import { MessageService, SortMeta } from 'primeng/api';
import { groupSet } from '../../../utilities/utilities';
import { CalculatedRoute, DisplayRoute, FilterParams } from '../../../data/client_models';
import { RouteCalculatorService } from '../../../data/route_calculator';
import { RouteTableComponent } from '../../route-finder/route-table/route-table.component';
import { ViewChild } from '@angular/core';
import { Observable } from 'rxjs';
import { RouteUtils } from '../../../utilities/route_utils';
import { LoadoutManagerService } from '../../../data/loadout_manager';


@Component({
  selector: 'near-me',
  templateUrl: './near-me.component.html'
})
export class NearMeComponent extends TISBaseComponent implements OnInit {
  @ViewChild("tblRoutesNearby", { static: false }) routeTable: RouteTableComponent;

  private searchLocation: SystemLocation;

  private allRoutes: CalculatedRoute[];
  public nearbyRoutes: DisplayRoute[];
  public nearbyMissions: MissionGiverEntry[];
  public nearbyShops: TradeShop[];

  private routeObserve: Subscription;

  constructor(httpRoute: ActivatedRoute,
    dataStore: DataCacheService,
    cdr: ChangeDetectorRef,
    toast: MessageService,
    private loadoutMgr: LoadoutManagerService,
    private route_calc: RouteCalculatorService) {
    super(httpRoute, dataStore, cdr, toast);
  }

  onAllDataTradeUpdated(): void {
    console.log("All Data Updated")

    this.routeObserve = this.route_calc.available_routes.subscribe((routes) => {
      this.allRoutes = routes;
      this.onRoutesUpdated()
    });
    this.updateNearMe();
  }

  ngOnDestroy(): void {
    console.log("Destroy")
    this.routeObserve.unsubscribe();
  }

  ngAfterViewInit(): void {
    this.routeTable.selectHeaders(["rank", "to_name", "total_time", "total_jumps", "avg_hold_cost", "avg_hold_profit", "max_profit_rate"])

    console.log("After View Init", this.allRoutes)
    this.onRoutesUpdated();

  }

  onRoutesUpdated(): void {
    if (!this.routeTable)
      return; //View not initialized yet
    if (!this.allRoutes)
      return
    console.log("On Updated", this.allRoutes);

    var inRoutes = this.allRoutes.filter(r => this.isNearLocation(r.from.Location, SearchScope.Location))

    var loadout = this.loadoutMgr.activeLoadout.value;
    var filter: FilterParams = ClientData.FilterPresets.default;

    var outRoutes = RouteUtils.calculateDisplayRoutes(this.gameData, inRoutes, loadout, this.userData, filter)

    RouteUtils.calculateRouteScores(outRoutes, filter);
    RouteUtils.calculateVisibility(outRoutes, filter);

    // Only show the top 10 routes
    
    outRoutes = (outRoutes
      .sort((a, b) => a.rank - b.rank)
      .slice(0, 10));

    this.nearbyRoutes = outRoutes;

    console.log("Got new routes", this.nearbyRoutes, this.routeTable);
    this.routeTable.refreshTable();
  }

  isNearLocation(location: SystemLocation, scope: SearchScope = SearchScope.Location): boolean {
    switch (scope) {
      case SearchScope.Immediate:
        return location == this.searchLocation;
      case SearchScope.Location:
        return location == this.searchLocation || location == this.searchLocation.ParentLocation || location.ParentLocation == this.searchLocation;
      case SearchScope.Body:
        return location.ParentBody == this.searchLocation.ParentBody;
      case SearchScope.System:
        return true;
    }
  }

  updateNearMe() {
    if (this.userData.current_location) {
      this.searchLocation = this.gameData.LocationKeyMap[this.userData.current_location];


    }
  }
}


enum SearchScope {
  Immediate,
  Location,
  Body,
  System
}
