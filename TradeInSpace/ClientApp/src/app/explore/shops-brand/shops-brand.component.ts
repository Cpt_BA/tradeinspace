import { Component, ViewChild, OnInit, ChangeDetectorRef, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DataCacheService } from '../../../data/data_cache';
import { ModelUtils } from '../../../utilities/model_utils';
import { ClientData } from '../../../data/client_data';
import { TISBaseComponent } from '../../tis_base.component';
import { LocationType, ServiceFlags, ShopType, TradeShop } from '../../../data/generated_models';
import { groupSet } from '../../../utilities/utilities';


@Component({
  selector: 'shop-brand-list',
  templateUrl: './shops-brand.component.html',
  styleUrls: ['./shops-brand.component.css']
})
export class ShopBrandListComponent extends TISBaseComponent implements OnInit {
  ShopType: typeof ShopType = ShopType;
  ServiceFlags: typeof ServiceFlags = ServiceFlags;
  ModelUtils: typeof ModelUtils = ModelUtils;
  ClientData: typeof ClientData = ClientData;

  public stores: TradeShop[];
  rowGroupMetadata: { [idx: string]: BrandGroup };

  constructor(httpRoute: ActivatedRoute,
    dataStore: DataCacheService,
    cdr: ChangeDetectorRef) {
    super(httpRoute, dataStore, cdr, null);
  }

  onAllDataGameUpdated(): void {
    //const shopGroups = groupSet(this.gameData.TradeShops, shop => ShopType[shop.ShopType]);
    //const shopNames = Object.keys(shopGroups);
    //const services = shops.reduce((p, c) => p | c.Location.ServiceFlags, ServiceFlags.None);

    this.stores = this.gameData.TradeShops.sort((a, b) => a.ShopType - b.ShopType);
    this.updateRowGroupMetaData();
  }

  onAllDataUserUpdated(): void {

  }

  filterUpdated(): void {
    console.log("Filter Udpated");
    this.updateRowGroupMetaData();
  }

  updateRowGroupMetaData() {
    this.rowGroupMetadata = {};
    if (this.stores) {
      for (let i = 0; i < this.stores.length; i++) {
        const rowData = this.stores[i];
        const shopType = ShopType[rowData.ShopType];
        const shopSvcs = rowData.Location.ServiceFlags;

        if (i === 0) {
          this.rowGroupMetadata[shopType] = { index: 0, size: 1, services: shopSvcs };
        }
        else {
          const previousRowData = this.stores[i - 1];
          const previousRowGroup = ShopType[previousRowData.ShopType];
          if (shopType === previousRowGroup) {
            this.rowGroupMetadata[shopType].size++;
            this.rowGroupMetadata[shopType].services |= shopSvcs;
          } else {
            this.rowGroupMetadata[shopType] = { index: i, size: 1, services: shopSvcs };
          }
        }
      }
    }
  }

}

interface BrandGroup {
  index: number;
  size: number;
  services: ServiceFlags;
}
