import { Component, ViewChild, OnInit, ChangeDetectorRef, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TISBaseComponent } from '../tis_base.component';
import { LocationType, ServiceFlags, ShopType, TradeShop, MissionGiverEntry, TradeItem, SystemLocation, GameShip } from '../../data/generated_models';
import { DataCacheService } from '../../data/data_cache';
import { Dictionary, saveCSV, saveFile } from '../../utilities/utilities';


@Component({
  selector: 'raw-vehicle-list',
  template: `
<raw-data [headers]="headers" [records]="records" title="Raw Vehicle List" filename="vehicles.csv">
</raw-data>
`,
  styles: [
  ]
})
export class RawVehicleListComponent extends TISBaseComponent implements OnInit {
  public records: GameShip[];
  public headers: Dictionary<string> = {
    "ID": "ID",
    "Key": "Key",
    "Name": "Name",
    "Description": "Description",
    "Manufacturer": "Manufacturer",
    "Size": "Size",
    "CargoCapacity": "CargoCapacity",
    "QuantumDriveSize": "QuantumDriveSize",
    "AdditionalProperties": "AdditionalProperties"
  };

  constructor(httpRoute: ActivatedRoute,
    dataStore: DataCacheService,
    cdr: ChangeDetectorRef) {
    super(httpRoute, dataStore, cdr, null);
  }

  onAllDataGameUpdated(): void {
    this.records = this.gameData.Ships
      .sort((a, b) => a.Name.localeCompare(b.Name));
  }

}
