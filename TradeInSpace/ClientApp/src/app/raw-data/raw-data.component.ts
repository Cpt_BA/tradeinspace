import { Component, Input } from '@angular/core';
import { OverlayPanel } from 'primeng/overlaypanel';
import { Dictionary, saveCSV, saveFile } from '../../utilities/utilities';


@Component({
  selector: 'raw-data',
  templateUrl: './raw-data.component.html',
  styles: [
  ]
})
export class RawDataListComponent {
  public Object: typeof Object = Object;

  @Input() public headers: Dictionary<string>;
  @Input() public title: string;
  @Input() public records: any[];
  @Input() public filename: string;

  selectedObjectJSON: string = null;

  doDownload(): void {
    saveCSV(this.filename, this.records, this.headers);
  }

  showDetails(event: any, dataObject: Object, panel: OverlayPanel): void {
    this.selectedObjectJSON = JSON.stringify(dataObject, null, 4);
    panel.toggle(event);
  }
}
