import { Component, ViewChild, OnInit, ChangeDetectorRef, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TISBaseComponent } from '../tis_base.component';
import { LocationType, ServiceFlags, ShopType, TradeShop, MissionGiverEntry, TradeItem, GameEquipment } from '../../data/generated_models';
import { DataCacheService } from '../../data/data_cache';
import { Dictionary, saveCSV, saveFile } from '../../utilities/utilities';


@Component({
  selector: 'raw-item-list',
  template: `
<raw-data [headers]="headers" [records]="records" title="Raw Item List" filename="items.csv">
</raw-data>
`,
  styles: [
  ]
})
export class RawItemListComponent extends TISBaseComponent implements OnInit {
  public records: GameEquipment[];
  public headers: Dictionary<string> = {
    "ID": "ID",
    "Key": "Key",
    "Name": "Name",
    "Description": "Description",
    "AttachmentType": "AttachmentType",
    "AttachmentSubtype": "AttachmentSubtype",
    "Manufacturer": "Manufacturer",
    "Size": "Size",
    "Grade": "Grade",
    "Class": "Class",
    "Tags": "Tags",
    "RequiredTags": "RequiredTags",
    "Health": "Health",
    "Mass": "Mass",
    "PortLayout": "PortLayout",
    "DefaultLoadout": "DefaultLoadout",
    "Properties": "Propeties"
  };

  constructor(httpRoute: ActivatedRoute,
    dataStore: DataCacheService,
    cdr: ChangeDetectorRef) {
    super(httpRoute, dataStore, cdr, null);
  }

  onAllDataGameUpdated(): void {
    this.records = this.gameData.Equipment
      .sort((a, b) => a.Name.localeCompare(b.Name));
  }

}
