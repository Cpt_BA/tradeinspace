import { Component, ViewChild, OnInit, ChangeDetectorRef, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TISBaseComponent } from '../tis_base.component';
import { LocationType, ServiceFlags, ShopType, TradeShop, MissionGiverEntry, TradeItem, SystemLocation } from '../../data/generated_models';
import { DataCacheService } from '../../data/data_cache';
import { Dictionary, saveCSV, saveFile } from '../../utilities/utilities';


@Component({
  selector: 'raw-location-list',
  template: `
<raw-data [headers]="headers" [records]="records" title="Raw Location List" filename="locations.csv">
</raw-data>
`,
  styles: [
  ]
})
export class RawLocationListComponent extends TISBaseComponent implements OnInit {
  public Object: typeof Object = Object;

  public records: SystemLocation[];
  public headers: Dictionary<string> = {
    "ID": "ID",
    "ParentLocationID": "ParentID",
    "ParentBodyID": "BodyID",
    "Key": "Key",
    "Name": "Name",
    "Description": "Description",
    "LocationType": "LocationType",
    "IsHidden": "IsHidden",
    "QTDistance": "QTDistance",
    "ServiceFlags": "ServiceFlags",
    "AdditionalProperties": "AdditionalProperties"
  };
  public title = "Raw Location List";

  constructor(httpRoute: ActivatedRoute,
    dataStore: DataCacheService,
    cdr: ChangeDetectorRef) {
    super(httpRoute, dataStore, cdr, null);
  }

  onAllDataGameUpdated(): void {
    this.records = this.gameData.SystemLocations
      .sort((a, b) => a.Name.localeCompare(b.Name));
  }

}
