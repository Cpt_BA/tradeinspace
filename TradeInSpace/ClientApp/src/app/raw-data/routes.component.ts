import { Component, ViewChild, OnInit, ChangeDetectorRef, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TISBaseComponent } from '../tis_base.component';
import { LocationType, ServiceFlags, ShopType, TradeShop, MissionGiverEntry, TradeItem, SystemLocation } from '../../data/generated_models';
import { DataCacheService } from '../../data/data_cache';
import { Dictionary, saveCSV, saveFile } from '../../utilities/utilities';
import { RouteCalculatorService } from '../../data/route_calculator';
import { CalculatedRoute } from '../../data/client_models';


@Component({
  selector: 'raw-route-list',
  template: `
<raw-data [headers]="headers" [records]="records" title="Raw Route List" filename="routes.csv">
</raw-data>
`,
  styles: [
  ]
})
export class RawRouteListComponent extends TISBaseComponent implements OnInit {
  public Object: typeof Object = Object;

  public records: RawRouteDisplay[];
  public headers: Dictionary<string> = {
    "from_id": "From ID",
    "to_id": "To ID",
    "from_name": "From",
    "to_name": "To",

    "commodity_key": "Commodity Key",
    "commodity_name": "Commodity Name",

    "min_buy": "Min Buy",
    "max_buy": "Max Buy",
    "min_sell": "Min Sell",
    "max_sell": "Max Sell",
    "min_profit": "Min Profit",
    "max_profit": "Max Profit",
    "buy_rate": "Buy Rate",
    "sell_rate": "Sell Rate"
  };

  constructor(httpRoute: ActivatedRoute,
    dataStore: DataCacheService,
    private routeCalc: RouteCalculatorService,
    cdr: ChangeDetectorRef) {
    super(httpRoute, dataStore, cdr, null);
    
  }

  onAllDataGameUpdated(): void {
    this.routeCalc.available_routes.subscribe(routes => {
      console.log(routes);
      this.records = routes.map(r => {
        return {
          from_id: r.from.ID,
          from_name: r.from_name,
          to_id: r.to.ID,
          to_name: r.to_name,
          commodity_key: r.item.Key,
          commodity_name: r.item_name,
          min_buy: r.min_buy,
          max_buy: r.max_buy,
          min_sell: r.min_sell,
          max_sell: r.max_sell,
          min_profit: r.min_profit,
          max_profit: r.max_profit,
          buy_rate: r.buy_rate,
          sell_rate: r.sell_rate
        }
      });
    });
  }

}

export interface RawRouteDisplay {
  from_id: number;
  to_id: number;

  from_name: string;
  to_name: string;

  commodity_key: string;
  commodity_name: string;

  min_buy: number;
  max_buy: number;

  min_sell: number;
  max_sell: number;

  min_profit: number;
  max_profit: number;

  buy_rate: number;
  sell_rate: number;
}
