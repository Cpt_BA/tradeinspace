import { Component, ViewChild, OnInit, ChangeDetectorRef, ViewEncapsulation, ElementRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DataCacheService } from '../../data/data_cache';
import { TradeTable, GameDatabase, SystemLocation, LocationType, ServiceFlags, PadSize } from '../../data/generated_models';
import { TreeNode, SortMeta, SelectItem, MessageService, Message } from 'primeng/api';
import { TreeTable } from 'primeng/treetable';
import { Subscription } from 'rxjs';
import { ShowTooltipEvent } from '../../directive/tooltip-attr.directive';
import { SelectButton } from 'primeng/selectbutton';
import { serviceFlagName } from '../../pipes/display_text_pipe';
import { ModelUtils } from '../../utilities/model_utils';
import { ClientData } from '../../data/client_data';
import { TISBaseComponent } from '../tis_base.component';
import { SafeResourceUrl, DomSanitizer } from '@angular/platform-browser';


@Component({
  selector: 'maps',
  templateUrl: './maps.component.html',
  styleUrls: ['./maps.component.css']
})
export class MapsComponent extends TISBaseComponent {

  mapKey = "";
  mapParams = "";
  mapError: string = null;

  viewer_frame_source: SafeResourceUrl;
  @ViewChild("map_container", { static: true }) map_container: ElementRef;
  paramListener: Subscription;

  constructor(
    dataStore: DataCacheService,
    cdr: ChangeDetectorRef,
    toast: MessageService,
    private httpRoute: ActivatedRoute,
    private router: Router,
    private sanitizer: DomSanitizer) {
    super(httpRoute, dataStore, cdr, toast);
  }

  onFirstGameDataLoaded(): void {
    this.paramListener = this.httpRoute.params.subscribe((p) => this.onParams(p));
    window.addEventListener("message", (message) => this.frameUpdated(message));
  }

  onAllDataReady(): void {
  }

  onParams(p) {
    let newArgs = null;
    if (p && p.zoom && p.lat && p.lng) {
      newArgs = ([p.zoom, p.lat, p.lng].join("/"))
    }
    if (!p.map_key) {
      this.mapError = "No Map Specified";
      return;
    }

    if (!this.gameData.BodyKeyMap[p.map_key]) {
      this.mapError = "Invalid Map Specified";
      return;
    }

    if (p.map_key !== this.mapKey) {
      this.mapKey = p.map_key;
      this.mapParams = newArgs;
      //Changed map. Must update the iframe source
      this.viewer_frame_source = this.sanitizer.bypassSecurityTrustResourceUrl(`/mapviewer/${p.map_key}#${newArgs}`);
    }

    if (newArgs !== this.mapParams) {
      this.mapParams = newArgs;
      //Args were updated with authority. Either initial navigate or user manually editing.
      //Must notify the inner panel what we intend to be the extents of view.
      const msg = {
        type: "boundschange",
        data: newArgs
      };
      this.map_container.nativeElement.contentWindow.postMessage(JSON.stringify(msg), "*");
    }

    this.mapError = null;
  }

  public frameUpdated(message: MessageEvent) {
    try {
      const msg: MessageData = JSON.parse(message.data);
      //console.log(msg);
      try {
        switch (msg.type) {
          case "hashchange":
            this.mapParams = msg.value;
            this.router.navigateByUrl(`/maps/${this.mapKey}/${this.mapParams}`, { replaceUrl: true })
            break;
        }
      } catch (ex) {
        console.error(ex);
      }
    } catch (ex) {
      //Liekly due to plugins
    }
  }

  public pageChanged(e: Event): void {
    
  }
}

interface MessageData {
  type: string;
  value: any;
}

