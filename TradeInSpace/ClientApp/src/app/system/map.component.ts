import { Component, OnInit, Input, HostBinding, OnChanges, SimpleChanges, Output, ViewChild, ChangeDetectorRef } from '@angular/core';
import { MenuItem, MessageService } from 'primeng/api';
import { ActivatedRoute } from '@angular/router';
import { TISBaseComponent } from '../tis_base.component';
import { DataCacheService } from '../../data/data_cache';

@Component({
  selector: 'system-map',
  template: `
<h1 class="header-font" style="text-align: center;">System Map</h1>

<div class="ui-g">
  <div class="ui-g-12">
    <system-display width="800" height="800">
    </system-display>
  </div>
</div>
`
})
export class SystemMapComponent extends TISBaseComponent implements OnInit {

  constructor(httpRoute: ActivatedRoute,
    dataStore: DataCacheService,
    cdr: ChangeDetectorRef,
    toast: MessageService) {
    super(httpRoute, dataStore, cdr, toast);
  }

}
