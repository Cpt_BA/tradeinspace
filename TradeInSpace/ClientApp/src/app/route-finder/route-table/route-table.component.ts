import { Component, Inject, Input, ElementRef, OnChanges, SimpleChanges, SimpleChange, OnInit, ViewChild, EventEmitter, Output } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { TradeShop, SystemLocation, ShopType } from '../../../data/generated_models';
import { CalculatedRoute, DisplayRoute, PropertyRankValue } from '../../../data/client_models';
import { Observable } from 'rxjs';
import { FilterMetadata, SortEvent } from 'primeng/api';
import { Table } from 'primeng/table';
import { stationpath } from '../../../pipes/station_path_pipe';
import { timerange } from '../../../pipes/time_range_pipe';
import { OverlayPanel } from 'primeng/overlaypanel';
import { Element } from '@angular/compiler';
import { tradeshopdisplay } from '../../../pipes/location_display';
import { fmtNumber, fmtUEC, fmtUnits, fmtMoney } from '../../../pipes/scaleunit_pipe';
import { Header } from 'primeng/components/common/shared';

@Component({
  selector: 'route-table',
  templateUrl: './route-table.component.html',
  styleUrls: ['./route-table.component.css']
})
export class RouteTableComponent implements OnChanges {

  @Input() available_routes: DisplayRoute[];

  @Input() sort_column: string;

  @Input() sort_direction: number;

  @ViewChild("dt", { static: true }) dataTable: Table;
  @ViewChild("tooltip_overlay", { static: true }) detailTooltip: OverlayPanel;

  @Output() routeCompleted = new EventEmitter<DisplayRoute>();  

  tooltip_content: string = "";

  grid_columns: Array<GridColumn> = [
    {
      header: "#",
      property: "rank",
      searchable: false,
      width: "50px",
      class: "col-high-priority",
      definition: "Rank/score according to preferences.",

      getRowContent: (route) => `${route.rank ? route.rank : ''}`,
      createTooltipContent: (route) => { return `Score: ${route.score}`; }
    },
    {
      header: "From",
      property: "from_name",
      searchable: true,
      width: "10%",
      class: "col-high-priority",

      getRowContent: (route) => tradeshopdisplay(route.from),
      createTooltipContent: (route) => stationpath(this.buildPath(route.from), true)
    },
    {
      header: "To",
      property: "to_name",
      searchable: true,
      width: "10%",
      class: "col-high-priority",

      getRowContent: (route) => tradeshopdisplay(route.to),
      createTooltipContent: (route) => stationpath(this.buildPath(route.to), true)
    },
    {
      header: "Item",
      property: "item_name",
      searchable: true,
      width: "100px",
      class: "col-high-priority",

      getRowContent: (route) => route.item.Name
    },

    {
      header: "uec/SCU",
      property: "avg_profit",
      searchable: false,
      width: "80px",
      class: "col-low-priority",
      definition: "Average profit per SCU. Helpful for small ships",

      getRowContent: (route) => `${fmtUEC(route.avg_profit)}`,
      createTooltipContent: (route) => {
        return [
          `Profit per SCU`,
          `Min - ${fmtUEC(route.min_profit)}`,
          `Avg - ${fmtUEC(route.avg_profit)}`,
          `Max - ${fmtUEC(route.max_profit)}`
        ].join("<br/>")
      },
      barSettings: (route) => route.rankProperties.avg_profit
    },
    {
      header: "Total",
      property: "total_time",
      searchable: false,
      width: "70px",
      class: "col-high-priority",
      definition: "Estimated total duration.",

      getRowContent: (route) => timerange(route.itinerary.totalDurationS),
      createTooltipContent: (route) => this.getItineraryContent(route),
      barSettings: (route) => route.rankProperties.total_time
    },
    {
      header: "Buy",
      property: "total_buy",
      searchable: false,
      width: "70px",
      class: "col-low-priority",
      definition: "Estimated duration to buy resources.",

      getRowContent: (route) => timerange(route.itinerary.totalBuyS),
      createTooltipContent: (route) => `Buy  @ ${fmtNumber(route.buy_rate / 100, false, ' SCU/min', '')}<br/>Sell @ ${fmtNumber(route.sell_rate / 100, false, ' SCU/min', '')}`,
      barSettings: (route) => route.rankProperties.total_buy
    },
    {
      header: "Transit",
      property: "total_transit",
      searchable: false,
      width: "70px",
      class: "col-low-priority",
      definition: "Estimated duration of tansit (trains, tram, running, etc.)",

      getRowContent: (route) => timerange(route.itinerary.totalTransitS),
      barSettings: (route) => route.rankProperties.total_transit
    },
    {
      header: "Ship",
      property: "total_ship",
      searchable: false,
      width: "70px",
      class: "col-low-priority",
      definition: "Estimated duration to travel from station to station. Includes ship flight along with QT travel and spooling",

      getRowContent: (route) => timerange(route.itinerary.totalShipS),
      barSettings: (route) => route.rankProperties.total_ship
    },
    {
      header: "Sell",
      property: "total_sell",
      searchable: false,
      width: "70px",
      class: "col-low-priority",
      definition: "Estimated duration to sell resources.",

      getRowContent: (route) => timerange(route.itinerary.totalSellS),
      createTooltipContent: (route) => `Buy  @ ${fmtNumber(route.buy_rate / 100, false, ' SCU/min', '')}<br/>Sell @ ${fmtNumber(route.sell_rate / 100, false, ' SCU/min', '')}`,
      barSettings: (route) => route.rankProperties.total_sell
    },
    {
      header: "QJ",
      property: "total_jumps",
      searchable: false,
      width: "50px",
      class: "col-med-priority",
      definition: "Number of Quantum Jumps.",

      getRowContent: (route) => `${route.total_jumps}`
    },
    {
      header: "SCU",
      property: "cargo_units",
      searchable: false,
      width: "60px",
      class: "col-low-priority",
      definition: "SCU to transport.",

      getRowContent: (route) => `${fmtUnits(route.cargo_units)}`
    },
    {
      header: "Cost",
      property: "avg_hold_cost",
      searchable: false,
      width: "70px",
      class: "col-high-priority",
      definition: "Average UEC cost to fill hold to maximum.",

      getRowContent: (route) => `${fmtMoney(route.avg_hold_cost)}`,
      createTooltipContent: (route) => `Buying ${fmtUnits(route.cargo_units)} @ ${fmtNumber(route.avg_buy / 100, false, ' uec', '$')}`
    },
    {
      header: "Profit",
      property: "avg_hold_profit",
      searchable: false,
      width: "70px",
      class: "col-high-priority",
      definition: "Average UEC profit from an entire run.",

      getRowContent: (route) => `${fmtMoney(route.avg_hold_profit)}`
    },
    {
      header: "%",
      property: "avg_profit_pct",
      searchable: false,
      width: "60px",
      class: "col-med-priority",
      definition: "Average profit percentage.",

      getRowContent: (route) => `${fmtNumber(route.avg_profit_pct, false, '%', '')}`,
      createTooltipContent: (route) => `Buy  @ ${fmtMoney(route.min_buy)} - ${fmtUEC(route.max_buy)}<br/>Sell @ ${fmtMoney(route.min_sell)} - ${fmtUEC(route.max_sell)}`,
      barSettings: (route) => route.rankProperties.avg_profit_pct
    },
    {
      header: "Rate",
      property: "max_profit_rate",
      searchable: false,
      width: "100px",
      class: "col-high-priority",
      definition: "*Maximum* potential rate of income.",

      getRowContent: (route) => `${fmtNumber(route.rankProperties.max_profit_rate.value, false, ' uec/min')}`,
      barSettings: (route) => route.rankProperties.max_profit_rate
    }
  ];
  public selected_columns: Array<GridColumn> = this.grid_columns;

  preset_filters: { [propName: string]: FilterMetadata } =
    {
      hidden: { value: false, matchMode: "equals" }
    };

  rowTrack = (index, item: DisplayRoute) => {
    return item.rank;
  }

  constructor() {
  }

  public refreshTable() {
    console.log(this.dataTable);
    if (this.dataTable) {
      this.dataTable.filter("false", "hidden", "equals");
    }
  }

  public newDataLoaded() {

  }

  public selectHeaders(headerKeys: string[]): void {
    this.selected_columns = this.grid_columns.filter(gc => {
      return headerKeys.some(key => gc.property == key);
    });
  }

  ngAfterViewInit(): void {
  }

  ngOnChanges(changes: SimpleChanges): void {
    var routeChanges = changes["available_routes"];
    console.log("On Changes!", changes, routeChanges);
    if (routeChanges && routeChanges.currentValue) {
      console.log("Route Changes!");
      this.refreshTable();
    }
  }

  customSort(event: SortEvent) {
    //event.data = Data to sort
    //event.mode = 'single' or 'multiple' sort mode
    //event.field = Sort field in single sort
    //event.order = Sort order in single sort
    //event.multiSortMeta = SortMeta array in multiple sort

    event.data = event.data.sort((data1, data2) => {
      let value1 = data1[event.field];
      let value2 = data2[event.field];
      let result = null;

      if (value1 == null && value2 != null)
        result = -1;
      else if (value1 != null && value2 == null)
        result = 1;
      else if (value1 == null && value2 == null)
        result = 0;
      else if (typeof value1 === 'string' && typeof value2 === 'string')
        result = value1.localeCompare(value2);
      else
        result = (value1 < value2) ? -1 : (value1 > value2) ? 1 : 0;

      return (event.order * result);
    });
  }

  cellClicked(event: Event, column: GridColumn, route: DisplayRoute) {
    var targetElement = <any>event.target;

    if (targetElement.classList.contains("route-complete")) {
      this.routeCompleted.next(route);
    }
  }

  getItineraryContent(route: DisplayRoute): string {
    const lines = [
      `[${timerange(route.itinerary.totalBuyS)}] Buy ${fmtUnits(route.cargo_units)} @ ${fmtNumber(route.buy_rate / 100, false, ' scu/min', '')}`,
      `[${timerange(route.itinerary.totalTransitS + route.itinerary.totalShipS)}] QT, Flight, Transit, and Running.`,
      `[${timerange(route.itinerary.totalSellS)}] Sell ${fmtUnits(route.cargo_units)} @ ${fmtNumber(route.sell_rate / 100, false, ' scu/min', '')}`,
      "",
      "Route:"
    ];
    route.itinerary.steps.forEach(s => {
      lines.push(`[${timerange(s.durationS)}] ${s.title}`);
    })
    return lines.join("<br/>");
  }

  showTooltip(event: Event, column: GridColumn, route: DisplayRoute): void {
    if (column.createTooltipContent) {
      this.tooltip_content = column.createTooltipContent(route);
      this.detailTooltip.show(event);
    }
  }

  showTooltipDef(event: Event, content: string): void {
    if (content) {
      this.tooltip_content = content;
      this.detailTooltip.show(event);
    }
  }

  hideTooltip(event): void {
    if (event.toElement) {
      let testElem = event.toElement;
      do {
        if (testElem.classList.contains("ui-overlaypanel-content")) return;
        testElem = testElem.parentElement;
      } while (testElem);
    }

    this.detailTooltip.hide();
  }

  buildPath(forStation: TradeShop): string[] {
    var fullPath = [];
    fullPath.push(ShopType[forStation.ShopType]);
    var pathLocation: SystemLocation = forStation.Location.ParentLocation;

    while (pathLocation) {
      //Avoid duplicate nested locations such as grim hex
      if (pathLocation.Name != fullPath[fullPath.length - 1]) {
        fullPath.push(pathLocation.Name);
      }
      pathLocation = pathLocation.ParentLocation;
    } 

    return fullPath.reverse();
  }
}

interface GridColumn {
  header: string;
  property: string;
  searchable: boolean;
  width: string;
  class: string;
  definition?: string;

  getRowContent: (route: DisplayRoute) => string;
  createTooltipContent?: (route: DisplayRoute) => string;
  barSettings?: (route: DisplayRoute) => PropertyRankValue;
}
