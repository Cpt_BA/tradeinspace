import { Component, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'route-details',
  templateUrl: './route-details.component.html'
})
export class RouteDetailsComponent {

  constructor(http: HttpClient, @Inject('BASE_URL') baseUrl: string) {
  }
}

