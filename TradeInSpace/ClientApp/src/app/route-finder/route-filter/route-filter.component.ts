import { ChangeDetectorRef, Component, EventEmitter, OnInit, Output, ViewEncapsulation, Input, OnChanges, SimpleChanges } from '@angular/core';
import { MenuItem, TreeNode, MessageService } from 'primeng/api';
import { Tree } from 'primeng/tree';
import { BehaviorSubject, combineLatest, Subscription, Observable } from 'rxjs';
import { ClientData } from '../../../data/client_data';
import { FilterParams, PreferenceEnum } from '../../../data/client_models';
import { DataCacheService } from '../../../data/data_cache';
import { GameDatabaseFull } from '../../../data/game_data';
import { SystemBody, TradeItem, TradeShop, TradeTable } from '../../../data/generated_models';
import { BasicUserSettings, KeyModel } from '../../../data/models';
import { RouteCalculatorService } from '../../../data/route_calculator';
import { locationdisplay, tradeshopdisplay } from '../../../pipes/location_display';
import { treeFromData, treeFromFlatData, treeFromGroupData } from '../../../utilities/utilities';
import { UiUtils } from '../../../utilities/ui_utils';
import { TISBaseComponent } from '../../tis_base.component';
import { ActivatedRoute } from '@angular/router';

type TreeNodeMaker<T> = (has_children: boolean, data: T) => TreeNode;
type TreeGroup<T> = { group: string, records: T[] };

@Component({
  selector: 'route-filter',
  templateUrl: './route-filter.component.html',
  styleUrls: ['./route-filter.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class RouteFilterComponent extends TISBaseComponent implements OnInit, OnChanges {
  ClientData: typeof ClientData = ClientData;
  UiUtils: typeof UiUtils = UiUtils;

  @Input() filter_settings: FilterParams;

  @Output() preferencesChanged: EventEmitter<FilterParams> = new EventEmitter<FilterParams>();
  @Output() filtersChanged: EventEmitter<FilterParams> = new EventEmitter<FilterParams>();

  filter_throttle: BehaviorSubject<FilterParams>;

  all_item_keys: string[];
  all_station_keys: string[];

  filter_items: TreeNode[];
  filter_stations: TreeNode[];

  selected_items: TreeNode[] = [];
  selected_stations: TreeNode[] = [];

  city_pref_options: MenuItem[] = this.createPreferenceOptions(["Prefer", "Neutral", "Avoid", "Ignore"], (val) => {
    return this.filter_settings.city_preference = <PreferenceEnum>val
  });
  hidden_pref_options: MenuItem[] = this.createPreferenceOptions(["Prefer", "Neutral", "Avoid", "Ignore"], (val) => {
    return this.filter_settings.hidden_preference = <PreferenceEnum>val
  });
  qt_pref_options: MenuItem[] = this.createPreferenceOptions(["Prefer", "Neutral", "Avoid", "Ignore"], (val) => {
    return this.filter_settings.quantum_preference = <PreferenceEnum>val
  });

  cargo_pref_options: MenuItem[] = this.createPreferenceOptions(["High Value", "Neutral", "Low Value"], (val) => {
    return this.filter_settings.hv_cargo_preference = <PreferenceEnum>val
  });

  short_pref_options: MenuItem[] = this.createPreferenceOptions(["Prefer shorter", "Don't Care"], (val) => {
    return this.filter_settings.short_route_preference = <PreferenceEnum>val
  });

  constructor(http_route: ActivatedRoute,
    dataStore: DataCacheService,
    toast: MessageService) {
    super(http_route, dataStore, null, toast);
  }


  // ****** Init and data loading ******
  ngOnInit(): void {
    super.ngOnInit();

    this.filter_throttle = new BehaviorSubject<FilterParams>(null);
    this.filter_throttle.debounceTime(250).subscribe(new_settings => {
      this.filtersChanged.emit(new_settings);
    });
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (!this.gameData) return;

    if (changes.filter_settings && changes.filter_settings.currentValue) {
      this.settingsLoaded(changes["filter_settings"].currentValue);
    }
  }

  onUserDataUpdated(): void {
    if (this.filter_settings) {
      this.settingsLoaded(this.filter_settings);
    }
  }

  onAllDataGameUpdated(): void {
    var validRouteTrades = this.tradeData.Entries.filter(s => ClientData.TradeShopTypes.includes(s.Station.ShopType));

    this.all_item_keys = Array.from(new Set(validRouteTrades.map(tte => tte.Item))).map(item => item.Key);
    this.all_station_keys = Array.from(new Set(validRouteTrades.map(tte => tte.Station))).map(station => station.Key);

    this.filter_items = UiUtils.createItemTree(this.gameData, ti => this.all_item_keys.includes(ti.Key));
    this.filter_stations = UiUtils.createStationTree(this.gameData, ts => this.all_station_keys.includes(ts.Key), true);
  }


  // ****** Settings saving/loading ******



  settingsLoaded(new_settings: FilterParams): void {
    this.loadTreeSelections(new_settings);
  }

  preferenceChanged(): void {
    this.preferencesChanged.emit(this.filter_settings);
  }

  filterChanged(): void {
    //this.filtersChanged.emit(this.filter_settings);
    this.filter_throttle.next(this.filter_settings);
  }


  // ****** UI Updaters/helpers ******



  updateTreeCheckmarks(): void {
    this.filter_items.forEach(n => UiUtils.markDownwards(n, this.selected_items));
    this.filter_stations.forEach(n => UiUtils.markDownwards(n, this.selected_stations));
  }

  loadTreeSelections(settings: FilterParams): void {
    this.selected_items = UiUtils.findRecursive(this.filter_items, (item: TreeNode) => {
      return item.data && settings.item_keys.includes((<KeyModel>item.data).Key)
    });

    this.selected_stations = UiUtils.findRecursive(this.filter_stations, (item: TreeNode) => {
      return item.data && settings.station_keys.includes((<KeyModel>item.data).Key)
    });

    this.updateTreeCheckmarks();
  }

  writeTreeSelections(): void {
    this.filter_settings.item_keys = this.selected_items
      .filter(item => item.data)
      .map(item => (<KeyModel>item.data).Key);

    this.filter_settings.station_keys = this.selected_stations
      .filter(station => station.data)
      .map(station => (<KeyModel>station.data).Key);

    this.filterChanged();
  }

  createPreferenceOptions(labels: string[], onClicked: (update: number) => void): MenuItem[] {
    return labels.map((val) => { return { label: val, command: (event: any) => { onClicked(event.index); } } });
  }

}


