import { Location } from '@angular/common';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { ClientData } from '../../data/client_data';
import { CalculatedRoute, DisplayRoute, FilterParams, Itinerary, PreferenceEnum } from '../../data/client_models';
import { DataCacheService } from '../../data/data_cache';
import { SystemBody } from '../../data/generated_models';
import { RouteCalculatorService } from '../../data/route_calculator';
import { RouteTableComponent } from './route-table/route-table.component';
import { RouteFilterComponent } from './route-filter/route-filter.component';
import { RouteUtils } from '../../utilities/route_utils';
import { TISBaseComponent } from '../tis_base.component';
import { MessageService } from 'primeng/api';
import { LoadoutManagerService } from '../../data/loadout_manager';


@Component({
  selector: 'route-finder',
  templateUrl: './route-finder.component.html',
  styleUrls: ['./route-finder.component.css']
})
export class RouteFinderComponent extends TISBaseComponent implements OnInit {

  @ViewChild("rFilter", { static: false }) routeFilter: RouteFilterComponent;
  @ViewChild("rTable", { static: false }) routeTable: RouteTableComponent;

  public filter_settings: FilterParams;
  public available_routes: DisplayRoute[];
  public route_legs: CalculatedRoute[];
  public calculated_itineraries: Itinerary[];

  preset_loaded: boolean;

  constructor(
    route: ActivatedRoute,
    dataStore: DataCacheService,
    //cdr: ChangeDetectorRef,
    toast: MessageService,
    private loadoutMgr: LoadoutManagerService,
    private routeCalc: RouteCalculatorService,
    private router: Router,
    private location: Location) {
    super(route, dataStore, null, toast);
  }

  onAllDataReady(): void {
    this.routeCalc.available_routes.subscribe(routes => {
      this.route_legs = routes;

      this.everythingFinallyLoaded();
    });
  }

  onUrlParamsLoaded(): void {
    if (this.urlParams.preset) {
      const presetName = this.urlParams.preset;

      if (presetName === "custom") {
        this.preset_loaded = false;

        //If we have URL parameters to expand
        //Should do some validation checks first, since this ends up overwriting your local lastSettings data
        if (Object.keys(this.queryParams).length > 0) {
          console.log("Expanding from URL parameters.");
          this.filter_settings = this.expandArgs(this.queryParams as TinyRouteArgs);
          //Otherwise just load from localData as normal
        } else {
          console.log("No URL Settings. Loading from user settings");

          this.loadFromUserSettings();
          this.updateURLArgs();
        }
      } else if (ClientData.FilterPresets[presetName]) {
        this.preset_loaded = true;
        this.filter_settings = ClientData.FilterPresets[presetName];
      }

    } else {
      this.preset_loaded = false;
      console.log("Fallback, loading from settings");
      this.loadFromUserSettings();
      this.updateURLArgs();
    }

    this.everythingFinallyLoaded();
  }

  loadFromUserSettings(): void {
    if (this.userData && this.userData.last_filter) {
      this.filter_settings = this.userData.last_filter;
    } else {
      this.filter_settings = ClientData.FilterPresets.default;
    }
  }

  onQueryParamsLoaded(): void {
    this.parametersChanged();
  }

  onAllDataUserUpdated(): void {
    this.everythingFinallyLoaded();
  }

  ngAfterViewInit(): void {
  }

  parametersChanged(): void {
    this.everythingFinallyLoaded();
  }

  everythingFinallyLoaded(): void {
    if (!this.urlParams || !this.queryParams) return;
    if (!this.filter_settings) { debugger; return; }

    this.routesUpdated(true, true, true);

    if (this.routeTable) this.routeTable.refreshTable();
  }

  routeCompleted(route: DisplayRoute): void {
    this.userData.balance += Math.round(route.avg_profit * route.cargo_units * 100) / 100;
    this.userData.current_location = route.to.Key;
    this.updateUserDetails();
  }

  preferenceChanged(settings: FilterParams): void {
    this.filter_settings = settings;

    if (!this.available_routes) return;
    this.routesUpdated(false, true, true);

    this.updateUserDetails();

    if (this.routeTable) this.routeTable.refreshTable();
  }

  filterChanged(settings: FilterParams): void {
    if (!settings) return;
    this.filter_settings = settings;

    if (!this.available_routes) return;
    this.routesUpdated(false, true, false);

    this.updateUserDetails();

    if (this.routeTable) this.routeTable.refreshTable();
  }

  updateURLArgs(): void {
    if (this.preset_loaded) return;

    var tiny = this.shrinkArgs();
    var url = this.router.createUrlTree(['route-finder', 'custom'], { queryParams: tiny }).toString();

    this.location.go(url);
  }

  updateUserDetails(): void {
    this.updateURLArgs();
    this.dataStore.setUserDetails(this.userData, true);
  }

  routesUpdated(calculateDisplay: boolean = true, calculateVisibility: boolean = true, calculateScores: boolean = true): void {
    var displayRoutes: DisplayRoute[];

    //Use previously calculated display routes when possible
    if (calculateDisplay)
      displayRoutes = RouteUtils.calculateDisplayRoutes(this.gameData, this.route_legs, this.loadoutMgr.activeLoadout.value, this.userData, this.filter_settings);
    else
      displayRoutes = this.available_routes;

    if (calculateVisibility)
      RouteUtils.calculateVisibility(displayRoutes, this.filter_settings);
    if (calculateScores)
      RouteUtils.calculateRouteScores(displayRoutes, this.filter_settings);
    this.available_routes = displayRoutes;
  }

  compressStations(selectedKeys: string[]): string {
    var finalKeys: string[] = [];

    var stations = this.gameData.TradeShops.filter(ts => selectedKeys.includes(ts.Key));
    var bodies = this.gameData.SystemBodies.filter(sl => selectedKeys.includes(sl.Key));

    bodies.forEach(b => {
      if (!bodies.includes(b.ParentBody)) {
        finalKeys.push(b.Key);
      }
    });
    stations.forEach(s => {
      if (!bodies.some(b => b.SystemLocation == s.Location.ParentBody)) {
        finalKeys.push(s.Key);
      }
    });

    //Remove duplicates w/ Set
    return Array.from(new Set(finalKeys)).join(",");
  }

  compressItems(selectedKeys: string[]): string {
    var finalKeys: string[] = [];

    var items = this.gameData.TradeItems.filter(ti => selectedKeys.includes(ti.Key));
    var categories = selectedKeys.filter(sk => items.every(i => i.Key != sk));
    var leafItems = items.filter(i => !categories.includes(i.Category));

    finalKeys.push(...categories);
    finalKeys.push(...leafItems.map(i => i.Key));

    //Remove duplicates w/ Set
    return Array.from(new Set(finalKeys)).join(",");
  }

  expandStations(compressedSelected: string): string[] {
    var compressedKeys: string[] = compressedSelected.split(",");
    var finalKeys: string[] = [];

    var stations = this.gameData.TradeShops.filter(ts => compressedKeys.includes(ts.Key));
    var bodies = this.gameData.SystemBodies.filter(sb => compressedKeys.includes(sb.Key));

    var fnAddRecurse = (body: SystemBody): void => {
      finalKeys.push(body.Key);
      body.ChildBodies.forEach(cb => fnAddRecurse(cb));
      var bodyShops = this.gameData.TradeShops.filter(s => s.Location.ParentBody === body.SystemLocation);
      finalKeys.push(...bodyShops.map(s => s.Key));
    }

    bodies.forEach(b => {
      fnAddRecurse(b);
    });
    stations.forEach(s => finalKeys.push(s.Key));

    return finalKeys;
  }

  expandItems(compressedSelected: string): string[] {
    var compressedKeys: string[] = compressedSelected.split(",");
    var finalKeys: string[] = [];

    var items = this.gameData.TradeItems.filter(ti => compressedKeys.includes(ti.Key));
    var groupItems = this.gameData.TradeItems.filter(ti => compressedKeys.includes(ti.Category));
    var groups = Array.from(new Set(groupItems.map(gi => gi.Category)));

    finalKeys.push(...items.map(i => i.Key));
    finalKeys.push(...groups);
    finalKeys.push(...groupItems.map(gi => gi.Key));


    return finalKeys;
  }

  public shrinkArgs(): TinyRouteArgs {
    const fullTiny: TinyRouteArgs = {
      city: this.filter_settings.city_preference.toString(),
      hidden: this.filter_settings.hidden_preference.toString(),
      quantum: this.filter_settings.quantum_preference.toString(),
      hv: this.filter_settings.hv_cargo_preference.toString(),
      short: this.filter_settings.short_route_preference.toString(),

      size: this.filter_settings.minimum_pad_size.toString(),
      loop: this.filter_settings.looping.toString(),

      t_total: this.filter_settings.total_time_max.toString(),
      t_buy: this.filter_settings.buy_time_max.toString(),
      t_transit: this.filter_settings.transit_time_max.toString(),
      t_ship: this.filter_settings.ship_time_max.toString(),
      t_sell: this.filter_settings.sell_time_max.toString(),

      stations: this.compressStations(this.filter_settings.station_keys),
      items: this.compressItems(this.filter_settings.item_keys),

      sort: this.filter_settings.sort_column,
      sort_dir: this.filter_settings.sort_direction
    };

    const preset = ClientData.FilterPresets["default"];

    if (fullTiny.city == preset.city_preference.toString()) delete fullTiny.city;
    if (fullTiny.hidden == preset.hidden_preference.toString()) delete fullTiny.hidden;
    if (fullTiny.quantum == preset.quantum_preference.toString()) delete fullTiny.quantum;
    if (fullTiny.hv == preset.hv_cargo_preference.toString()) delete fullTiny.hv;
    if (fullTiny.short == preset.short_route_preference.toString()) delete fullTiny.short;

    if (fullTiny.size == preset.minimum_pad_size.toString()) delete fullTiny.size;
    if (fullTiny.loop == preset.looping.toString()) delete fullTiny.loop;

    if (fullTiny.t_total == preset.total_time_max.toString()) delete fullTiny.t_total;
    if (fullTiny.t_buy == preset.buy_time_max.toString()) delete fullTiny.t_buy;
    if (fullTiny.t_transit == preset.transit_time_max.toString()) delete fullTiny.t_transit;
    if (fullTiny.t_ship == preset.ship_time_max.toString()) delete fullTiny.t_ship;
    if (fullTiny.t_sell == preset.sell_time_max.toString()) delete fullTiny.t_sell;

    if (fullTiny.stations === "") delete fullTiny.stations;
    if (fullTiny.items === "") delete fullTiny.items;

    if (fullTiny.sort == preset.sort_column) delete fullTiny.sort;
    if (fullTiny.sort_dir == preset.sort_direction) delete fullTiny.sort_dir;

    return fullTiny;
  }

  public expandArgs(tinyArgs: TinyRouteArgs): FilterParams {
    const defaults = ClientData.FilterPresets["default"];

    return {
      city_preference: parseInt(tinyArgs.city) || defaults.city_preference,
      hidden_preference: parseInt(tinyArgs.hidden) || defaults.hidden_preference,
      quantum_preference: parseInt(tinyArgs.quantum) || defaults.quantum_preference,
      hv_cargo_preference: parseInt(tinyArgs.hv) || defaults.hv_cargo_preference,
      short_route_preference: parseInt(tinyArgs.short) || defaults.short_route_preference,

      minimum_pad_size: parseInt(tinyArgs.size) || defaults.minimum_pad_size,
      looping: new Boolean(tinyArgs.loop).valueOf() || defaults.looping,

      total_time_max: parseInt(tinyArgs.t_total) || defaults.total_time_max,
      buy_time_max: parseInt(tinyArgs.t_buy) || defaults.buy_time_max,
      transit_time_max: parseInt(tinyArgs.t_transit) || defaults.transit_time_max,
      ship_time_max: parseInt(tinyArgs.t_ship) || defaults.ship_time_max,
      sell_time_max: parseInt(tinyArgs.t_sell) || defaults.sell_time_max,

      station_keys: tinyArgs.stations ? this.expandStations(tinyArgs.stations) : defaults.station_keys,
      item_keys: tinyArgs.items ? this.expandItems(tinyArgs.items) : defaults.item_keys,

      sort_column: tinyArgs.sort || defaults.sort_column,
      sort_direction: tinyArgs.sort_dir || defaults.sort_direction
    }
  }
}

interface ColumnDetails {
  name: string;
  header: string;
  sort_property: string;
  searchable: boolean;
  enabled: boolean;
}

export interface TinyRouteArgs {
  city: string;
  hidden: string;
  quantum: string;
  hv: string;
  short: string;

  size: string;
  loop: string;

  t_total: string;
  t_buy: string;
  t_transit: string;
  t_ship: string;
  t_sell: string;

  stations: string;
  items: string;

  sort: string;
  sort_dir: number;
}
