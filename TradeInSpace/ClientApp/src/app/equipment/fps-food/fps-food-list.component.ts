import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { DataCacheService } from '../../../data/data_cache';
import { GameDatabaseFull } from '../../../data/game_data';
import { GameEquipment } from '../../../data/generated_models';
import { ClientData } from '../../../data/client_data';
import { BasicUserSettings } from '../../../data/models';
import { TISBaseComponent } from '../../tis_base.component';
import { MessageService } from 'primeng/api';


@Component({
  selector: 'fps-food-list',
  templateUrl: './fps-food-list.component.html'
})
export class FPSFoodListComponent extends TISBaseComponent implements OnInit {

  foodList: FoodDisplay[];

  constructor(httpRoute: ActivatedRoute,
    dataStore: DataCacheService,
    cdr: ChangeDetectorRef,
    toast: MessageService) {
    super(httpRoute, dataStore, cdr, toast);
  }

  onAllDataGameUpdated(): void {
    this.foodList = this.gameData.Equipment
      .filter(d =>
        (d.AttachmentType == ClientData.EquipmentTypes.Food ||
          d.AttachmentType == ClientData.EquipmentTypes.Drink) &&
        d.Properties.Food)
      .map(f => new FoodDisplay(this.gameData, f));
  }
}

export class FoodDisplay {
  public Effects: string;

  public constructor(private Game_Data: GameDatabaseFull, public Food: GameEquipment) {
    this.Effects = (Food.Properties.Food.Effects || [])
      .map(e => (e.Duration === 0) ? `${e.BuffType} (${e.Duration}s)` : `${e.BuffType}`)
      .join(", ");
  }
}
