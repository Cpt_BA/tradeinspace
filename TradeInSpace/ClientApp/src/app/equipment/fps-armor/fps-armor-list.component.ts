import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { DataCacheService } from '../../../data/data_cache';
import { GameDatabaseFull } from '../../../data/game_data';
import { GameEquipment } from '../../../data/generated_models';
import { ClientData } from '../../../data/client_data';
import { BasicUserSettings } from '../../../data/models';
import { MessageService } from 'primeng/api';
import { TISBaseComponent } from '../../tis_base.component';


@Component({
  selector: 'fps-armor-list',
  templateUrl: './fps-armor-list.component.html'
})
export class FPSArmorListComponent extends TISBaseComponent implements OnInit {

  armorList : GameEquipment[];

  constructor(httpRoute: ActivatedRoute,
    dataStore: DataCacheService,
    cdr: ChangeDetectorRef,
    toast: MessageService) {
    super(httpRoute, dataStore, cdr, toast);
  }

  onAllDataGameUpdated(): void {
    this.armorList = this.gameData.Equipment
      .filter(d =>
        (d.AttachmentType == ClientData.EquipmentTypes.FPS_Helmet ||
          d.AttachmentType == ClientData.EquipmentTypes.FPS_Torso ||
          d.AttachmentType == ClientData.EquipmentTypes.FPS_Arms ||
          d.AttachmentType == ClientData.EquipmentTypes.FPS_Legs) &&
        d.Properties.FPSArmor);
  }
}

