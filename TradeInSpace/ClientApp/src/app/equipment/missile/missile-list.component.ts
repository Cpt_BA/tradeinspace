import { Component, OnInit, ChangeDetectorRef, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { DataCacheService } from '../../../data/data_cache';
import { GameDatabaseFull } from '../../../data/game_data';
import { GameEquipment } from '../../../data/generated_models';
import { ClientData } from '../../../data/client_data';
import { BasicUserSettings } from '../../../data/models';
import { TISBaseComponent } from '../../tis_base.component';
import { Table } from 'primeng/table';
import { MessageService } from 'primeng/api';


@Component({
  templateUrl: './missile-list.component.html'
})
export class MissileListComponent extends TISBaseComponent implements OnInit {

  missileList: GameEquipment[];

  constructor(http_route: ActivatedRoute,
    dataStore: DataCacheService,
    cdr: ChangeDetectorRef,
    toast: MessageService) {
    super(http_route, dataStore, cdr, toast);
  }

  onAllDataGameUpdated(): void {
    this.missileList = this.gameData.Equipment.filter(d => d.AttachmentType === ClientData.EquipmentTypes.Missile);
  }
}

