import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { DataCacheService } from '../../../data/data_cache';
import { GameDatabaseFull } from '../../../data/game_data';
import { GameEquipment } from '../../../data/generated_models';
import { ClientData } from '../../../data/client_data';
import { BasicUserSettings } from '../../../data/models';
import { TISBaseComponent } from '../../tis_base.component';
import { MessageService } from 'primeng/api';


@Component({
  selector: 'fps-other-list',
  templateUrl: './fps-other-list.component.html'
})
export class FPSOtherListComponent extends TISBaseComponent implements OnInit {

  utilityList: GameEquipment[];
  hackingList: GameEquipment[];
  gadgetList: GameEquipment[];

  constructor(httpRoute: ActivatedRoute,
    dataStore: DataCacheService,
    cdr: ChangeDetectorRef,
    toast: MessageService) {
    super(httpRoute, dataStore, cdr, toast);
  }

  onAllDataGameUpdated(): void {
    this.utilityList = this.gameData.Equipment.filter(d =>
      d.AttachmentType === ClientData.EquipmentTypes.FPS_Consumable && d.AttachmentSubtype !== "Hacking");

    this.hackingList = this.gameData.Equipment.filter(d =>
      d.AttachmentType === ClientData.EquipmentTypes.FPS_Consumable && d.AttachmentSubtype === "Hacking");

    this.gadgetList = this.gameData.Equipment.filter(d =>
      d.AttachmentType === ClientData.EquipmentTypes.FPS_Weapon && d.AttachmentSubtype === "Gadget" && d.Manufacturer);

  }
}

