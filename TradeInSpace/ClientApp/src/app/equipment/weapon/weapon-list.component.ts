import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { DataCacheService } from '../../../data/data_cache';
import { GameDatabaseFull } from '../../../data/game_data';
import { GameEquipment } from '../../../data/generated_models';
import { ClientData } from '../../../data/client_data';
import { BasicUserSettings } from '../../../data/models';
import { calculateDamage } from '../../../data/loadout';
import { ModelUtils } from '../../../utilities/model_utils';
import { DamageMode, DamageSummary } from '../../../data/client_models';
import { TISBaseComponent } from '../../tis_base.component';
import { MessageService } from 'primeng/api';


@Component({
  selector: 'weapon-list',
  templateUrl: './weapon-list.component.html',
  styles: [
`
.name-col {
	font-size: 0.8em;
	color: gray;
}
`
  ]
})
export class WeaponListComponent extends TISBaseComponent implements OnInit {

  weaponList: WeaponDisplay[];

  constructor(http_route: ActivatedRoute,
    dataStore: DataCacheService,
    cdr: ChangeDetectorRef,
    toast: MessageService) {
    super(http_route, dataStore, cdr, toast);
  }

  onAllDataGameUpdated(): void {
    this.weaponList = this.gameData.Equipment
      .filter(d => d.AttachmentType === ClientData.EquipmentTypes.WeaponShip && d.Properties.Heat && d.Properties.Energy)
      .map<WeaponDisplay>(w => {
        const weaponAmmo = ModelUtils.getEquipmentAmmo(this.gameData, w);
        return {
          equipment: w,
          damage: calculateDamage(this.gameData, w, weaponAmmo)
        }
      });
  }
}

interface WeaponDisplay {
  equipment: GameEquipment;
  damage: DamageSummary;
}
