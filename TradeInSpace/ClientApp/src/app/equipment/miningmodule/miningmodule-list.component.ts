import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { DataCacheService } from '../../../data/data_cache';
import { GameDatabaseFull } from '../../../data/game_data';
import { GameDatabase, GameEquipment, ModifierActivation, ModifierType } from '../../../data/generated_models';
import { ClientData } from '../../../data/client_data';
import { BasicUserSettings } from '../../../data/models';
import { TISBaseComponent } from '../../tis_base.component';
import { MessageService } from 'primeng/api';


@Component({
  selector: 'miningmodule-list',
  styles: [
`
.module-mode {
  font-size: 0.8em;
  color: grey;
}
`
  ],
  templateUrl: './miningmodule-list.component.html'
})
export class MiningModuleListComponent extends TISBaseComponent implements OnInit {
  public ModifierActivation:typeof ModifierActivation = ModifierActivation;

  moduleList: DisplayMiningModule[];

  constructor(httpRoute: ActivatedRoute,
    dataStore: DataCacheService,
    cdr: ChangeDetectorRef,
    toast: MessageService) {
    super(httpRoute, dataStore, cdr, toast);
  }

  onAllDataGameUpdated(): void {

    function searchPropertyValue(equipment: GameEquipment, type: ModifierType): number {
      const mod = equipment.Properties.Modifier.Modifiers.find(m => m.Type == type);
      if (mod != undefined) {
        return mod.Value;
      }
      return 1;
    }

    this.moduleList = this.gameData.Equipment.filter(d =>
      d.AttachmentType === ClientData.EquipmentTypes.MiningModifier &&
      d.Tags.includes("miningConsumable") &&
      d.Properties &&
      d.Properties.Modifier &&
      d.Properties.Modifier.Modifiers.length > 0)
      .map(eq => {

        return {
          Equipment: eq,
          Charges: eq.Properties.Modifier.Charges,
          Activation: eq.Properties.Modifier.Activation,
          Heat: searchPropertyValue(eq, ModifierType.Heat),
          Signature: searchPropertyValue(eq, ModifierType.Signature),
          Filter: searchPropertyValue(eq, ModifierType.MiningFilter),
          Power: searchPropertyValue(eq, ModifierType.WeaponDamage),
          WindowLevel: searchPropertyValue(eq, ModifierType.MiningWindowLevel),
          Resistance: searchPropertyValue(eq, ModifierType.MiningResistance),
          Instability: searchPropertyValue(eq, ModifierType.MiningInstability),
          OptimalWindowSize: searchPropertyValue(eq, ModifierType.MiningOptimalWindowSize),
          ShatterDamage: searchPropertyValue(eq, ModifierType.MiningShatterDamage),
          OptimalWindowRate: searchPropertyValue(eq, ModifierType.MiningOptimalWindowRate),
          CatastrophicWindowRate: searchPropertyValue(eq, ModifierType.MiningCatastrophicWindowRate),
        }
      });
  }
}



export interface DisplayMiningModule {
  Equipment: GameEquipment;

  Charges: number;
  Activation: ModifierActivation;

  Heat: number;
  Signature: number;

  Filter: number;
  Power: number;
  WindowLevel: number;
  Resistance: number;
  Instability: number;
  OptimalWindowSize: number;
  ShatterDamage: number;
  OptimalWindowRate: number;
  CatastrophicWindowRate: number;
}
