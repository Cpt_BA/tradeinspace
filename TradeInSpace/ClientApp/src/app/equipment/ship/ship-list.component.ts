import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DataCacheService } from '../../../data/data_cache';
import { TISBaseComponent } from '../../tis_base.component';
import { MessageService } from 'primeng/api';
import { LoadoutManagerService } from '../../../data/loadout_manager';
import { DisplayShip } from '../../../data/display_ship';


@Component({
  selector: 'ship-list',
  templateUrl: './ship-list.component.html',
  styles: [
    ".full-width { width: 100% }"
  ]
})
export class ShipListComponent extends TISBaseComponent implements OnInit {

  shipList: DisplayShip[];

  constructor(private loadoutMgr: LoadoutManagerService,
    httpRoute: ActivatedRoute,
    dataStore: DataCacheService,
    cdr: ChangeDetectorRef,
    toast: MessageService) {
    super(httpRoute, dataStore, cdr, toast, "Ship List");
  }

  onAllDataGameUpdated(): void {
    console.log(this.gameData.Ships)
    this.shipList = this.gameData.Ships.map(s => {
      const loadout = this.loadoutMgr.getDefaultLoadout(s.Key);
      if (s.Key === "argo_mole") {
        console.log(s)
        console.log(loadout)
      }
      if (loadout)
        return new DisplayShip(loadout, s);
      else
        return null;
    }).filter(ds => !(!ds));
  }
}
