import { Component, ViewChild, OnInit, ChangeDetectorRef, ViewEncapsulation, ElementRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription, combineLatest } from 'rxjs';
import { DataCacheService } from '../../../data/data_cache';
import { GameDatabaseFull } from '../../../data/game_data';
import { GameEquipment, SystemBody } from '../../../data/generated_models';
import { ClientData } from '../../../data/client_data';
import { BasicUserSettings } from '../../../data/models';
import { Column } from 'primeng/components/common/shared';
import { JumpDetails } from '../../../data/client_models';
import { ModelUtils } from '../../../utilities/model_utils';
import { TISBaseComponent } from '../../tis_base.component';
import { MessageService } from 'primeng/api';


@Component({
  selector: 'qdrive-list',
  templateUrl: './qdrive-list.component.html',
  styles: [`
    .progress-label {
      color: var(--neg-txt-color);
      font-weight: bold;
      margin: 0 0 0 20px;
      position: absolute;
      top: 50%;
      -ms-transform: translateY(-50%);
      transform: translateY(-50%);
    }

    .center-col {
      text-align: center;
    }

    @media screen and (max-width: 40em)
    {
      tr > th.mobile-show-qdcalc {
        display: inline-block !important;
        width: 100%;
      }
    }
  `]
})
export class QDriveListComponent extends TISBaseComponent implements OnInit {

  qdriveList: GameEquipment[];

  public previewSource: SystemBody = null;
  public previewDestination: SystemBody = null;

  public sourceOptions: SystemBody[] = [];
  public destOptions: SystemBody[] = [];

  public calculatedDist: number;
  public calculatedTimes: QDriveDisplay[] = [];

  public longestTime: number;
  public mostFuel: number;

  constructor(httpRoute: ActivatedRoute,
    dataStore: DataCacheService,
    cdr: ChangeDetectorRef,
    toast: MessageService) {
    super(httpRoute, dataStore, cdr, toast);
  }

  onAllDataReady() {
    this.previewDestination = this.gameData.BodyKeyMap["port_olisar"];
  }

  onAllDataGameUpdated() {
    this.destOptions = this.sourceOptions = this.gameData.SystemBodies;
    this.qdriveList = this.gameData.Equipment.filter(d => d.AttachmentType === ClientData.EquipmentTypes.QuantumDrive);

    this.recalc();
  }

  onAllDataUserUpdated() {
    if (this.userData.current_location) {
      const tradeLocation = this.gameData.ShopKeyMap[this.userData.current_location];
      const bodyLocation = tradeLocation && tradeLocation.Location.ParentBody;
      if (bodyLocation) {
        //TODO: Better mechanism to lookup body to a location
        this.previewSource = this.gameData.BodyKeyMap[bodyLocation.Key];

        this.recalc();
      }
    }
  }

  public recalc(): void {
    this.longestTime = 0;
    this.mostFuel = 0;

    if (!this.previewSource || !this.previewDestination) {
      this.calculatedTimes = this.qdriveList.map<QDriveDisplay>(drive => {
        return { drive: drive, jump: ModelUtils.EmptyJump };
      });
    }
    else {
      this.calculatedDist = ModelUtils.calculateBodyDistanceKM(this.previewSource, this.previewDestination);

      this.calculatedTimes = this.qdriveList.map(drive => {
        const jumpStats = ModelUtils.calculateQuantumJump(this.previewSource, this.previewDestination, drive);

        this.longestTime = Math.max(this.longestTime, jumpStats.total_time);
        this.mostFuel = Math.max(this.mostFuel, jumpStats.fuel_consumption);

        return {
          drive: drive,
          jump: jumpStats
        };
      });
    }
  }
}

interface QDriveDisplay {
  drive: GameEquipment;
  jump: JumpDetails;
}
