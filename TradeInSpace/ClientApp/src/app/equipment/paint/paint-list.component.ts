import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { DataCacheService } from '../../../data/data_cache';
import { GameDatabaseFull } from '../../../data/game_data';
import { GameEquipment } from '../../../data/generated_models';
import { ClientData } from '../../../data/client_data';
import { BasicUserSettings } from '../../../data/models';
import { TISBaseComponent } from '../../tis_base.component';
import { MessageService } from 'primeng/api';
import { UiUtils } from '../../../utilities/ui_utils';
import { Table } from 'primeng/table';


@Component({
  selector: 'paint-list',
  templateUrl: './paint-list.component.html'
})
export class PaintListComponent extends TISBaseComponent implements OnInit {

  paintList: GameEquipment[];

  public returnSelf(obj: any): any {
    console.log(obj);
    return obj;
  }

  constructor(http_route: ActivatedRoute,
    dataStore: DataCacheService,
    cdr: ChangeDetectorRef,
    toast: MessageService) {
    super(http_route, dataStore, cdr, toast);
  }

  onAllDataGameUpdated(): void {
    //Explicity put buyable filter here b/c many ships have placeholder names for default skins
    this.paintList = this.gameData.Equipment.filter(d => d.AttachmentType === ClientData.EquipmentTypes.Paint).filter(e => e.CheapestPrice > 0);
  }

  public buyableFilter(table: Table, event): void {
    table.filter(event, (o) => o, 'isBuyable')
  }
}

