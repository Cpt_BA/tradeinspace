import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { DataCacheService } from '../../../data/data_cache';
import { GameDatabaseFull } from '../../../data/game_data';
import { GameEquipment } from '../../../data/generated_models';
import { ClientData } from '../../../data/client_data';
import { BasicUserSettings } from '../../../data/models';
import { MessageService } from 'primeng/api';
import { TISBaseComponent } from '../../tis_base.component';


@Component({
  selector: 'fps-undersuit-list',
  templateUrl: './fps-undersuit-list.component.html'
})
export class FPSUndersuitListComponent extends TISBaseComponent implements OnInit {

  undersuitList: GameEquipment[];

  constructor(httpRoute: ActivatedRoute,
    dataStore: DataCacheService,
    cdr: ChangeDetectorRef,
    toast: MessageService) {
    super(httpRoute, dataStore, cdr, toast);
  }

  onAllDataGameUpdated(): void {
    this.undersuitList = this.gameData.Equipment.filter(d => d.AttachmentType === ClientData.EquipmentTypes.FPS_Undersuit);
  }
}

