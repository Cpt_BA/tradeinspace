import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { DataCacheService } from '../../../data/data_cache';
import { GameDatabaseFull } from '../../../data/game_data';
import { GameEquipment } from '../../../data/generated_models';
import { ClientData } from '../../../data/client_data';
import { BasicUserSettings } from '../../../data/models';
import { DamageInfo } from '../../../data/client_models';
import { Loadout } from '../../../data/loadout';
import { SelectItem, MessageService } from 'primeng/api';
import { UiUtils } from '../../../utilities/ui_utils';
import { TISBaseComponent } from '../../tis_base.component';


@Component({
  selector: 'fps-weapon-list',
  templateUrl: './fps-weapon-list.component.html'
})
export class FPSWeaponListComponent extends TISBaseComponent implements OnInit {

  manufacturerList: SelectItem[];
  weaponList: FPSWeaponDisplay[];

  constructor(httpRoute: ActivatedRoute,
    dataStore: DataCacheService,
    cdr: ChangeDetectorRef,
    toast: MessageService) {
    super(httpRoute, dataStore, cdr, toast);
  }

  onAllDataGameUpdated(): void {
    this.weaponList = this.gameData.Equipment
      .filter(d =>
        (d.AttachmentType == ClientData.EquipmentTypes.FPS_Weapon) &&
        d.AttachmentSubtype != "Gadget" &&
        d.Properties.Weapon &&
        !d.Key.includes("template") &&
        !d.Key.includes("_tow")) //Theaters of war? :D
      .map(weapon => new FPSWeaponDisplay(this.gameData, weapon));


    this.manufacturerList = UiUtils.distinctOptionList(this.weaponList, (w) => w.Weapon.Manufacturer)
  }
}

export class FPSWeaponDisplay {
  public DPS: DamageInfo;
  public FlatDPS: number;
  public Burst: DamageInfo;
  public FlatBurst: number;
  public FireRate: number;
  public Projectiles: number;

  public AmmoSpeed: number;
  public AmmoCap: number;

  public AttachSlots: string;
  public Ammo: GameEquipment;

  private WeaponLoadout: Loadout;

  constructor(private GameDB: GameDatabaseFull, public Weapon: GameEquipment) {
    this.WeaponLoadout = Loadout.FromEquipment(this.GameDB, this.Weapon);

    var ammoData = this.WeaponLoadout.all_slots.find(s => s.using_ammo).using_ammo

    this.AmmoSpeed = ammoData.AmmoData.Speed;
    this.AmmoCap = ammoData.MaxAmmo;

    this.DPS = this.WeaponLoadout.total_sustain;
    this.FlatDPS = this.WeaponLoadout.total_sustain_dps;
    this.Burst = this.WeaponLoadout.total_volley;
    this.FlatBurst = this.WeaponLoadout.total_volley_dps;

    this.FireRate = this.WeaponLoadout.total_fire_rate;
    this.Projectiles = this.WeaponLoadout.total_projectiles;

    var attachments: string[] = [];

    this.WeaponLoadout.all_slots.forEach(s => {
      switch (s.name) {
        case "optics_attach": attachments.push("Optics"); break;
        case "barrel_attach": attachments.push("Barrel"); break;
        case "underbarrel_attach": attachments.push("Underbarrel"); break;
      }
    });

    this.AttachSlots = attachments.join(", ");
  }
}
