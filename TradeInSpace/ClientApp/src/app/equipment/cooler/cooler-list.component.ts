import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { DataCacheService } from '../../../data/data_cache';
import { GameDatabaseFull } from '../../../data/game_data';
import { GameEquipment } from '../../../data/generated_models';
import { ClientData } from '../../../data/client_data';
import { BasicUserSettings } from '../../../data/models';
import { MessageService } from 'primeng/api';
import { TISBaseComponent } from '../../tis_base.component';


@Component({
  selector: 'cooler-list',
  templateUrl: './cooler-list.component.html'
})
export class CoolerListComponent extends TISBaseComponent implements OnInit {

  coolerList: GameEquipment[];

  constructor(httpRoute: ActivatedRoute,
    dataStore: DataCacheService,
    cdr: ChangeDetectorRef,
    toast: MessageService) {
    super(httpRoute, dataStore, cdr, toast);
  }

  onAllDataGameUpdated(): void {
    this.coolerList = this.gameData.Equipment.filter(d => d.AttachmentType === ClientData.EquipmentTypes.Cooler);
  }
}

