import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { DataCacheService } from '../../../data/data_cache';
import { GameDatabaseFull } from '../../../data/game_data';
import { GameEquipment } from '../../../data/generated_models';
import { ClientData } from '../../../data/client_data';
import { BasicUserSettings } from '../../../data/models';
import { TISBaseComponent } from '../../tis_base.component';
import { MessageService } from 'primeng/api';


@Component({
  selector: 'fps-clothing-list',
  templateUrl: './fps-clothing-list.component.html'
})
export class FPSClothingListComponent extends TISBaseComponent implements OnInit {

  clothingList: GameEquipment[];

  clothingTypes: string[] = [
    ClientData.EquipmentTypes.Clothing_Hat,
    ClientData.EquipmentTypes.Clothing_Torso0,
    ClientData.EquipmentTypes.Clothing_Torso1,
    ClientData.EquipmentTypes.Clothing_Torso2,
    ClientData.EquipmentTypes.Clothing_Hands,
    ClientData.EquipmentTypes.Clothing_Legs,
    ClientData.EquipmentTypes.Clothing_Feet,
  ]

  constructor(httpRoute: ActivatedRoute,
    dataStore: DataCacheService,
    cdr: ChangeDetectorRef,
    toast: MessageService) {
    super(httpRoute, dataStore, cdr, toast);
  }

  onAllDataGameUpdated(): void {
      this.clothingList = this.gameData.Equipment.filter(d => this.clothingTypes.some(ct => d.AttachmentType == ct) &&
        !d.Key.includes("template"));
  }
}

