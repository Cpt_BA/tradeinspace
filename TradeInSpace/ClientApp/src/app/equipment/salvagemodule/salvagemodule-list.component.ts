import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { DataCacheService } from '../../../data/data_cache';
import { GameDatabaseFull } from '../../../data/game_data';
import { GameDatabase, GameEquipment, ModifierActivation, ModifierType } from '../../../data/generated_models';
import { ClientData } from '../../../data/client_data';
import { BasicUserSettings } from '../../../data/models';
import { TISBaseComponent } from '../../tis_base.component';
import { MessageService } from 'primeng/api';
import { Client } from '_debugger';


@Component({
  selector: 'salvagemodule-list',
  styles: [
`
.module-mode {
  font-size: 0.8em;
  color: grey;
}
`
  ],
  templateUrl: './salvagemodule-list.component.html'
})
export class SalvageModuleListComponent extends TISBaseComponent implements OnInit {
  public ModifierActivation:typeof ModifierActivation = ModifierActivation;

  moduleList: DisplaySalvageModule[];

  constructor(httpRoute: ActivatedRoute,
    dataStore: DataCacheService,
    cdr: ChangeDetectorRef,
    toast: MessageService) {
    super(httpRoute, dataStore, cdr, toast);
  }

  onAllDataGameUpdated(): void {

    function searchPropertyValue(equipment: GameEquipment, type: ModifierType): number {
      const mod = equipment.Properties.Modifier.Modifiers.find(m => m.Type == type);
      if (mod != undefined) {
        return mod.Value;
      }
      return 1;
    }

    this.moduleList = this.gameData.Equipment.filter(d =>
      d.AttachmentType === ClientData.EquipmentTypes.SalvageModifier &&
      d.Properties &&
      d.Properties.Modifier &&
      d.Properties.Modifier.Modifiers.length > 0)
      .map(eq => {

        return {
          Equipment: eq,

          Speed: searchPropertyValue(eq, ModifierType.SalvageSpeed),
          Radius: searchPropertyValue(eq, ModifierType.SalvageRadius),
          Efficiency: searchPropertyValue(eq, ModifierType.SalvageEfficiency)
        }
      });

    console.log(this.moduleList);
    console.log(this.gameData.Equipment.length);
    console.log(this.gameData.Equipment.filter(d => d.AttachmentType == ClientData.EquipmentTypes.SalvageModifier));
  }
}



export interface DisplaySalvageModule {
  Equipment: GameEquipment;

  Speed: number;
  Radius: number;
  Efficiency: number;
}
