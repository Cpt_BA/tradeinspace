import { Component, NgZone } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DataCacheService } from '../../../data/data_cache';
import * as moment from 'moment';

@Component({
  selector: 'changelog',
  templateUrl: './changelog.html',
  
})
export class ChangelogPage {
  public moment: typeof moment = moment;

  public static CurrentVersion = "2.2.1";

  //Note: All months are set back by one b/c javascript dates are dum
  entries: LogEntry[] = [
    {
      date: new Date(2023, 2, 24),
      header: "v2.2.1 - Some Simple Salvaging",
      notes: [
        {
          header: "Salvage modules have now been added!",
          lines: [
            "Additionally, ship capacities have been fixed along with some other minor bugs."
          ]
        }
      ]
    },
    {
      date: new Date(2023, 2, 19),
      header: "v2.2 - A New Frame of Reference",
      notes: [
        {
          header: "The site is now open source!",
          lines: [
            "Full source code for the entire project is now publicly available!",
            "This includes the website, database models, import utility, and general-purpose data-scraping desktop application.",
          ],
          link: {
            title: "Bitbucket Repository",
            url: "https://bitbucket.org/Cpt_BA/tradeinspace/src/master/"
          }
        },
        {
          header: "A couple updates to the planetary maps for the site as well.",
          lines: [
            "Planet maps have gotten a bit of a touch-up, and now have a rendered surface for the whole planet.",
            "This texture is rendered based on raw values in game data, and reverse-engineering the process as best as I can understand it.",
            "While fairly accurate from a distance, locations up-close can be quite inaccurate in terms of terrain and color.",
            "These maps serve as a general guide to help navigation."
          ]
        },
        {
          header: "Additionally, all planetary information is now using NASA-Style coordinates",
          lines: [
            "This means coordinates have a longitudinal range of 0/360º instead of -180º[W]/180º[E]. Latitude remains with a range of -90º/90º"
          ]
        }
      ]
    },
    {
      date: new Date(2022, 10, 4),
      header: "v2.1 - A Peek Inside",
      notes: [
        { header: "My biggest new feature since adding maps, MORE MAPS!" },
        {
          lines: [
            "I've now added fully 3D, interactive maps of all avialable ship interiors/exteriors.",
            "This tool allows you to see inside ships, and get an intuitive idea of how they are laid out.",
            "Notable featuers include:"
          ],
          points: [
            "Full resolution Exterior + Interior models.",
            "Loadout is fully editable",
            "Slice horizontally at any height to get a cross-section of a single floor.",
            "Doors and editable hardpoints are highlighted, for better clarity.",
            "View in fullscreen, or take pictures to reference later.",
            "Select equipment by clicking on them, for easy editing.",
            "Keybord controls for camera and slicing.",
            "Link ships directly to other people.",
            "Added links on ship and loadout lists, going direct to the model."
          ]
        },
        {
          header: "Keyboard Shortcuts",
          points: [
            "WASD: Move camera",
            "Q/E: Rotate camera left/right",
            "R/F: Raise/lower camera",
            "1: Toggle hull slicing",
            "2: Toggle equipment slicing",
            "3: Toggle traversal (door) slicing",
            "0: Toggle hologram when slicing",
            "NUMPAD [1-9]: Camera preset"
          ]
        },
        {
          lines: [
            "I hope you all enjoy this new tool, and find it useful in your journey through the 'verse!",
            "Some ships stil have some glitches here and there, but most ships should be fairly accurate.",
            "Please let me know in the spectrum forums if you find any issues with specific ships, so I can fix them.",
            "",
            "As much as I would love to add textures, it would add considerable size/time/weight to the assets, making ships unable to load as quickly as they otherwise could.",
            "For these reasons and more, it is unlikely that I will add textures to these models.",
            "",
            "As always, I thank you for continued interest/use of this site <3."
          ]
        }
      ]
    },
    {
      date: new Date(2022, 7, 29),
      header: "v2.0 - 3.17.2",
      notes: [
        { header: "Finally! An Update!" },
        {
          lines: [
            "Lots of new stuff for once!",
            "Additionally, all site data was updated for 3.17.2, including maps."
          ],
          points: [
            "New site layout for clearer picture of important options.",
            "Better organized navigation menu.",
            "New and improved ship selector.",
            "New and improved location selector.",
            "Mining modules and gadgets.",
            "Raw data lists"
          ]
        },
        {
          header: "New Ship/Location Selector",
          lines: [
            "Much nicer ship selection dialog, with many useful filters, and manufacturer icons.",
            "This display will also show you the purchase price of the ship, locations to purchase, and available rentals as well.",
            "Location selector was also overhauled, to make finding what you are looking for much easier, as well as supporting a bit of exploration."
          ]
        },
        {
          header: "Raw Data Lists",
          lines: [
            "These lists are almost direct views of the data the site uses to consutuct all of the screens.",
            "Most tables have a set of JSON properties that can be expanded, which contain context-specific information such as orbit radius, or position.",
            "These tables also allow you to save directly to a .csv file if you would like to do your own data manipulation.",
            "Current Tables Include:"
          ],
          points: [
            "Commodities",
            "Items & Equipment",
            "Locations",
            "Routes",
            "Vehicles"
          ]
        }
      ]
    },
    {
      date: new Date(2022, 0, 30),
      header: "v1.6.1 - 3.16.1",
      notes: [
        { header: "Updated maps, and some minor updates." },
        {
          lines: [
            "Maps have been updated for 3.16.1, including the new derelicts that can be found on most moon surfaces.",
            "Additionally, the map viewer was updated to add heading information when planning routes,",
            "The map viewer has also been given a placehold search feature, to be replaced with a better version integrated into the site later.",
            "this enables you to actually track an accurate heading and distance in-game to find an unmarked location on the surface from a known location!",
          ]
        },
        {
          lines: [
            "Additionally, the mission givers page has been updated to readily show much more relevant information.",
            "Changes:"
          ],
          points: [
            "Rank requirements now shows the name of the closest rank, as well as it's tier (ie: T1, T2, etc.)",
            "Mission list now displays rank requirements",
            "Mission list now displays rep rewards, with an option to show exact values",
            "Fixed bug with max mission payout display."
          ]
        }
      ]
    },
    {
      date: new Date(2022, 0, 7),
      header: "v1.6 - 3.16 and MAPS!",
      notes: [
        { header: "Now with maps!" },
        {
          lines: [
            "The biggest addition to the site yet!",
            "I'm pleased to announce that I've added browsable maps for all planets and moons to the site.",
            "Feel free to embed these maps into your own website to serve as reference for players",
            "Site data was also updated to reflect the 3.16 LIVE Update",
            "",
            "Maps are being rendered by a server in my basement, so please be a bit patient if they are a bit slow to render sometimes :)",
            "",
            "Some highlight features of the maps:"
          ],
          points: [
            "Heightmaps extracted from planets",
            "All POI's marked on planets surface.",
            "Deep linking support, easily send/recieve links direct to a camera view.",
            "Accurately measure distances, accounting for different size planets, and planet curvature.",
            "Individually toggle waypoint types for basic searching.",
            "Clustering and expansion of waypoints to avoid clutter.",
            "Link to the site, containing the corresponding version",
            "Maps are 100% auto-generated, allowing for easy future updates.",
            "Supports a full-frame viewer directly for embedding purposes.",
            "NOTE: Heightmaps are likely not positioned accurately relative to ground structures."
          ]
        },
        {
          header: "Embedding"
        },
        {
          lines: [
            "Embedding the viewer is simple, just use an iframe pointing to the mapviewer you want to show.",
            "Example for yela:",
            "",
            "<iframe src='https://tradein.space/mapviewer/yela'></iframe>",
            "",
            "Thats it! Simply substitue any planet name as desired.",
            "The viewer will take up all 100% of the iframe, and can be directly linked to a location via deep linking.",
            "Example deep linking (Navigates direct to jumptown):",
            "",
            "<iframe src='https://tradein.space/mapviewer/yela#17/13.81216/28.21386'></iframe>",
            "",
            "NOTE: Make sure not to use the 'https://tradein.space/#/maps' path that the main website uses.",
          ],
        },
        {
          header: "Future Plans"
        },
        {
          lines: [
            "There are still a lot of features I'd like to add to the site and viewer.",
            "I hope to get some of these added in the coming months.",
            "Some examples:"
          ],
          points: [
            "Integrated trade/items viwer",
            "Transit loop information (cycle times, etc)",
            "In-game triangulation. (Enter distances to waypoints, get point on map)",
            "In-game geolocation. (Select coordinate, get distances to waypoints)",
            "Better site integration",
            "Embed parameters (hide layer control, tools, etc.)",
            "Ship size reference utility (preview S/M/L/XL/etc. bounds in-map)"
          ]
        }
      ]
    },
    {
      date: new Date(2021, 11, 3),
      header: "v1.5.3 - 3.15 time",
      notes: [
        { header: "Update to 3.15 data!" },
        {
          lines: [
            "Again, apologies for the long time between udpates, life just getting in the way...",
            "Added a couple quick things, as well as fixing a few change data points with the 3.15 update."
          ],
          points: [
            "Added refined mineral prices and locations to mining elements screen.",
            "Added trader rep scopes, only available in raw data at the moment.",
            "Fixed foods not showing any stats due to new metadata.",
            "Fixed mission list reputation reward scope issues."
          ]
        }
      ]
    },
    {
      date: new Date(2021, 7, 21),
      header: "v1.5.2 - What year is it?",
      notes: [
        { header: "Finally an update!" },
        {
          lines: [
            "Finally got around to supporting the latest and greatest 3.14 build from CIG.",
            "I have some new features planned soon(tm), but I wanted to at least update to the latest data."
          ]
        }
      ]
    },
    {
      date: new Date(2021, 1, 11),
      header: "v1.5.1 - Missing Shops",
      notes: [
        {
          lines: [
            "Fixed issue with missing shops that are now considered part of the Cargo Station now. Hint of things to come? ;)"
          ]
        }
      ]
    },
    {
      date: new Date(2021, 1, 3),
      header: "v1.5 - On a Mission",
      notes: [
        { header: "Mission data now available!" },
        {
          lines: [
            "New mission giver and mission lists have been added to help plan your next course of action in SC.",
            "The mission details page contains many useful pieces of information to assist you in your next adventure in the verse."
          ],
          points: [
            "Show UEC reward ranges, as well as possible reputation outcomes.",
            "Show mission prerequisites, including location, reputation, buy-in, required missions, and more.",
            "Displays any helpful notes available for the missions so you aren't left out to dry.",
            "Related missions view to find other related missions that might be available."
          ]
        },
        { header: "Performance improvements" },
        {
          lines: [
            "Drastically reduced initial page startup time."
          ]
        }
      ]
    },
    {
      date: new Date(2021, 0, 17),
      header: "v1.4 - Back in Action",
      notes: [
        { header: "Data loaded correctly again." },
        {
          lines: [
            "I'm sorry for the delays with getting the site properly udpated for both versions 3.11 and 3.12.",
            "There were some fundamental changes on data I was relying on, and I've been extremely busy with work, unable to find much time for working on the site.",
            "I'm glad to say I've resolved the issues and all the stores seem to be coming in correctly now. I will be doing some spot-checks to verify everything that was imported."
          ]
        },
        { header: "Upcomming Features" },
        {
          lines: [
            "I may not have been actively working on the website, but I have been thinking quite a bit about what would be nice additions to the site.",
            "Here are a few of my various ideas, send a reply in Spectrum to let me know which is your favorite!",
            "I don't plan on adding all of these featues, but I would love to hear which you like the most."
          ],
          points: [
            "Interactive System/Planet Map - Draggable map for viewing the whole system, or basic layouts on a planet.",
            "Mission Database - List of available missions, payouts, requirements, etc.",
            "Basic Ship/Fleet Combat Sim - Simple calculator for comparing head-to-head performance of different loadouts.",
            "Mining Simulator & Cart - Tool to simulte rock compositions/tool compatability, and pricing of different mineral loads.",
            "Route Planner - Ability to generate and plan efficient routes taking into account the ship loadout. Including for picking up all items in the shopping cart."
          ]
        }
      ]
    },
    {
      date: new Date(2020, 9, 8),
      header: "v1.3.2 - Initial 3.11 LIVE/PTU Support",
      notes: [
        { header: "Initial update to import 3.11 data." },
        {
          lines: [
            "Note: There are some major changes involved in this update, without much time to prepare on PTU this time around.",
            "There are some known issues including missing stations that I am working on fixing.",
            "In addition I will have some new features coming online with the full update",
            "I hope to have it finished within the next week. Thank you for your understanding."
          ]
        }
      ]
    },
    {
      date: new Date(2020, 7, 16),
      header: "v1.3.1 - Route Looping",
      notes: [
        { header: "Option to calculate routes as one-way or looping." },
        {
          lines: [
            "Previously, routes were calculated as only the time to go from the 'From' location to the 'To' location",
            "Now there is a choice between 'One-Way' and 'Looping' for calculating route timing."
          ],
          points: [
            "In 'One-Way' mode, the time is as before, only the time it takes for 'From' -> 'To'",
            "In 'Looping' mode, the calculator will factor in the time it takes to return to the 'From' location after your sale.",
            "This can have a large impact on hidden locations as they take a long time to locate but are instant to leave."
          ]
        }
      ]
    },
    {
      date: new Date(2020, 7, 16),
      header: "v1.3 - Mining Information",
      notes: [
        { header: "Mining data now available." },
        {
          lines: [
            "New mining page has been added to help plan for the most profitable mining route.",
          ],
          points: [
            "Shows which deposits hold which mineable resources, the chances of the deposits, and potential relative densities.",
            "Various stats for each mineable resource including resistance, instability, window modifiers, and more.",
            "Value of resources, and stations available for selling easily accessible.",
            "Locations where deposits can be found is easily visible and searchable."
          ]
        },
        {
          header: "Ship Rentals",
          lines: [
            "New page to show rentals available for all ships."
          ]
        },
        {
          header: "Additional Features",
          lines: [
            "Added full itinerary view to 'Total' column of the route table",
            "Improved classification of location services to be based on specific available inventory.",
          ]
        }
      ]
    },
    {
      date: new Date(2020, 7, 10),
      header: "v1.2.2 - Trade Data Overhaul",
      notes: [
        { header: "Trade data now pulled entirely from game data files." },
        {
          lines: [
            "All ship, commodity, equipment, and item trades are now pulled entirely from the Subsumption data stored in local game files.",
            "This allows for immediate, accurate updating of trade information when new game udpates are released.",
            "In the future I still plan on supporting user-submitted trade values for realtime value fluctuations, but most screens are setup to accomodate prices ranges currently as-is.",
            "",
            "I would one again like to thank Mr. Deschain for providing the original source of data prior to this.",
            "You can support him any time via his referral code: STAR-VFY7-VM75"
          ],
          link: {
            title: "Or by visiting his youtube channel",
            url: "https://www.youtube.com/channel/UC8LhugUyvlqq9L7WZSVLG6w/featured"
          }
        },
        { header: "Added ship paint page with prices." },
        {
          lines: [
            "Fixed search issues on some pages."
          ]
        }
      ]
    },
    {
      date: new Date(2020, 6, 29),
      header: "v1.2.1 - PTU  Support",
      notes: [
        { header: "Support for separate LIVE/PTU datasets." },
        {
          header: "LIVE Data available via",
          link: {
            title: "https://tradein.space",
            url: "https://tradein.space"
          }
        },
        {
          header: "PTU Data available via",
          link: {
            title: "https://ptu.tradein.space",
            url: "https://ptu.tradein.space"
          }
        }
      ]
    },
    {
      date: new Date(2020, 6, 25),
      header: "v1.2 - Personal Hangar and Loadout Improvements",
      notes: [
        { header: "Many improvements to ship building capabilities." },
        {
          lines: [
            "This update focused on improving the accuracy of loadout calculation, as well as persistence (heh) to ship loadouts.",
            "TIS is now at parity (or better) with other tools, and I will seek to increase it's accuracy more.",
            "Notable features include:"
          ]
        },
        {
          header: "Personal Hangar",
          points: [
            "Save named and customized loadouts to your personal hangar, which maintains persistant local storage!",
            "Quickly see stats of all available ships in one place, and easily edit when needed.",
            "Imported loadouts end up in your personal hangar.",
            "Ships in your hangar show up in the active ship list, and can be favorited for quick access."
          ]
        },
        {
          header: "Loadouts",
          points: [
            "Complete overhaul of summary panel. Now has detailed breakout charts for weapon and missile damage as well as armor resistances.",
            "Additionally, all helpful stats are brought to the summary, including calculation of dynamic overheat time.",
            "Loadouts can now be Exported and Imported! This is useful for backing them up or sharing with other citizens",
            "Note: Export/Import format is experimental at the moment and is potentially subject to change.",
            "Calculation accuracy of dps, power, heat, emissive, and other stats greatly increased. Removing the [WIP] status :^)",
            "Note: Numbers here may vary from other community resources due to the number of hidden and non-essential components taken into account during calculations."
          ]
        },
        {
          header: "Better Mobile Support",
          points: [
            "Did a pass to essentially all screens to make them much more useable on a mobile device.",
            "Loadout Summary is now significantly more readable",
            "Shopping cart got a summary section on mobile.",
            "All equipment pages (ship and personal) got basic cleanup and formatting for mobile.",
            "Only a couple screens don't have proper mobile support, namely the Trade Table.",
          ]
        },
        {
          header: "Changelog",
          points: [
            "Added a changelog for the website...Which you are reading now...Meta."
          ]
        },
      ]
    },
    {
      date: new Date(2020, 5, 30),
      header: "v1.1.1 - Cart & Equipment Prices",
      notes: [
        { header: "Implemented equipment prices from MrDeschain's spreadsheet." },
        {
          lines: [
            "TIS now has prices for ships and equipment, adding to the utility of the site.",
            "Along with prices a shopping cart was added to help keep track of your intended purchases.",
            "Notable features include:"
          ]
        },
        {
          header: "Equipment Pages",
          points: [
            "Prices added to all forms of available equiment",
            "Ability to add items to cart from any available location."
          ]
        },
        {
          header: "Shopping Cart",
          points: [
            "View all your added items grouped by type.",
            "Quickly see which items are available at which locations, and the ability to switch if needed."
          ]
        },
      ]
    },
    {
      date: new Date(2020, 5, 16),
      header: "v1.1 - Ships & Equipment",
      notes: [
        { header: "First major update for the website!" },
        {
          lines: [
            "Now offering much more in the way of helpful information.",
            "Also pairing with MrDeschain for the first time for an accurate user-collected data source.",
            "Notable features include:"
          ]
        },
        {
          header: "[WIP] Loadout Edtior",
          points: [
            "First iteration of a loadout editor for ships and equipment.",
            "Power and heat values are calculated roughly at the moment, and are likely not entirely accurate at this point.",
            "Ability to switch equipments in available slots, and available equipments are properly restricted to slot size/type.",
          ]
        },
        {
          header: "Ship list",
          points: [
            "Provides high-level information about all ships available in the game.",
            "Key stats including Max Speed, QT Speed, DPS, and more."
          ]
        },
        {
          header: "Ship equipment listing pages",
          points: [
            "Quantum Drives (Speed, Accel, Cooldown, Spool, Economy, and Calculator)",
            "Mining Lasers (Power, Stat Modifiers)",
            "Coolers (Power and Cooling)",
            "Shields (Heat, Power, Health, Delay, and Hardening)",
            "Weapons (Name, Manufacturer, Heat, Power, Firing Mode, DPS, and Fire Rate)",
            "Power Plants (Heat and Power)"
          ]
        },
        {
          header: "FPS equipment listing pages",
          points: [
            "Food & Drink (Hunger, Hydration, and Effect stats)",
            "Undersuits (Temp Rating and Damage Resistance)",
            "Armor (Temp Rating and Damage Resistance)",
            "Weapons (Manufacturer, Size, Projectile Speed, DPS, and Attachments)",
            "Clothing (Type and Temp Rating)",
            "Other (Hacking Tools and Utilities)"
          ]
        },
      ]
    },
    {
      date: new Date(2020, 2, 9),
      header: "v1.0 - Initial Launch",
      notes: [
        { header: "Initial launch of the TradeInSpace website!" },
        { lines: ["Features include:"] },
        { header: "Trade Route Finder",
          points: [
            "uec/SCU, uec/min., Profit%",
            "Route ranking",
            "Filtering by time, station, item, hidden and more.",
            "Presets",
            "Saved Preset(shareable)"
          ]
        },
        {
          header: "Loop Finder (WIP): Find multi-step routes with compatible pickup/dropoff combos",
          points: [
            "Filtering by item or station",
            "Demand ratio calculation",
            "Maximum potential profit cap",
            "Pad size filtering"
          ]
        },
        {
          header: "Trade Table Viewer: Just give me the numbers!",
          points: [
            "See price range and SCU rates per location per item."
          ]
        },
        {
          header: "Exploration View: See services per location",
          points: [
            "Pad Filtering & counts",
            "Services display: Pads, Trade, Admin, FPS, Ship, Etc."
          ]
        }
      ]
    }
  ]

  constructor(private http_route: ActivatedRoute, private data_cache: DataCacheService, private zone: NgZone) {
  
  }
}

interface LogEntry {
  date: Date;
  header: string;
  notes: LogNotes[];
}

interface LogNotes {
  points?: string[];
  lines?: string[];
  header?: string;
  link?: LogLink;
}

interface LogLink {
  title: string;
  url: string;
}
