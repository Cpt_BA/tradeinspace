import { Component, NgZone } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DataCacheService } from '../../data/data_cache';

@Component({
  selector: 'about-page',
  templateUrl: './about-page.html',
  styleUrls: ['./about-page.css']
})
export class AboutPage {


  constructor(private http_route: ActivatedRoute, private data_cache: DataCacheService, private zone: NgZone) {
  
  }

  public reset(): void {
    if (confirm("Are you sure?")) {
      this.data_cache.clearCache(false);
      this.refreshPage();
    }
  }

  public resetAll(): void {
    if (confirm("Are you sure?")) {
      this.data_cache.clearCache(true);
      this.refreshPage();
    }
  }

  refreshPage(): void {
    this.zone.runOutsideAngular(() => {
      location.reload();
    });
  }

}
