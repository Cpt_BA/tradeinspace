import { Component, ViewChild, ErrorHandler, OnInit, ElementRef } from '@angular/core';
import { OverlayPanel } from 'primeng/overlaypanel';
import { Router } from '@angular/router';
import { Title } from '@angular/platform-browser';
import { TISErrorHandler } from '../utilities/error_handler';
import { FilterUtils } from 'primeng/api';
import { Chart } from 'chart.js';
import { LoadoutManagerService } from '../data/loadout_manager';
import { ServiceFlags } from '../data/generated_models';
import { InputText } from 'primeng/inputtext';
import { animate, state, style, transition, trigger } from '@angular/animations';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  animations: [
    trigger('panelInOut', [
      state('hidden', style({ display: 'none' })),
      state('visible', style({ display: '' })),
      transition('hidden => visible', [
        animate(200, style({ transform: 'translateY(-100%)' }))
      ]),
      transition('visible => hidden', [
        animate(200, style({ transform: 'translateY(-100%)' }))
      ])
    ])
  ]
})
export class AppComponent implements OnInit {
  title = 'app';

  tisError: TISErrorHandler;

  toolbar_hidden: boolean = false;

  searchBlock = false
  globalTooltipContent: string = "";
  @ViewChild("global_tooltip", { static: false }) globalTooltip: OverlayPanel;

  title_map: { [path: string]: string } = {
    "/": "Trade in Space",
    "/trade/route-finder/default": "Route Finder",
    "/trade/route-finder/pct_profit": "Route Finder: % Profit",
    "/trade/route-finder/uec_profit": "Route Finder: uec Profit",
    "/trade/route-finder/speed": "Route Finder: Speed",
    "/trade/route-finder/convenient": "Route Finder: Convenience",
    "/trade/route-finder/qt_distance": "Route Finder: QT Time",
    "/trade/route-finder/custom": "Route Finder: Saved Profile",
    "/trade/loop-finder": "Loop Finder",
    "/trade/trade-table": "Trade Table",
    
    
    "/equipment/qdrive": "Equipment - QDrives",
    "/equipment/weapon": "Equipment - Weapons",
    "/equipment/mininglaser": "Equipment - Mining Lasers",
    "/equipment/mininggadget": "Equipment - Mining Gadgets",
    "/equipment/miningmodule": "Equipment - Mining Modules",
    "/equipment/salvagemodule": "Equipment - Salvage Modules",
    "/equipment/cooler": "Equipment - Coolers",
    "/equipment/shield": "Equipment - Shields",
    "/equipment/powerplant": "Equipment - Power Plants",
    "/equipment/missile": "Equipment - Missiles",
    "/equipment/paint": "Equipment - Paint",
    "/equipment/ships": "Ships",
    "/equipment/rentals": "Ship Rentals",

    "/loadout": "Loadout Editor",

    "/raw/commodity": "Raw Commodity List",
    "/raw/location": "Raw Location List",
    "/raw/vehicle": "Raw Vehicle List",
    "/raw/route": "Raw Route List",
    "/raw/item": "Raw Item List",

    "/hangar": "Personal Hangar",
    //TODO: Support params better?

    "/maps": "Maps",
    "/ship_maps": "3D Ship Maps",
    
    "/fps_equipment/food": "Equipment - Food",
    "/fps_equipment/undersuit": "Equipment - Undesruit",
    "/fps_equipment/armor": "Equipment - FPS Armor",
    "/fps_equipment/weapon": "Equipment - FPS Weapon",
    "/fps_equipment/clothing": "Equipment - Clothing",
    "/fps_equipment/other": "Equipment - FPS Other",

    "/mission": "Missions",
    "/giver": "Mission Givers",

    "/explore/system": "Traverse the verse!",
    "/explore/map": "System Map",
    "/explore/shops": "Shops",

    "/mining/locations": "Mining Locations",
    "/mining/elements": "Elements",

    "/cart": "Shopping Cart",

    "/about": "About",
    "/changelog": "Changelog"
  }

  default_settings: PageSettings = {
    hide_settings_bar: false
  };

  page_settings: Record<string, PageSettings> = {
    "/ship_maps": { hide_settings_bar: true }
  };

  applySettings(settings: PageSettings) {
    this.toolbar_hidden = settings.hide_settings_bar;
  }

  constructor(router: Router, title: Title, error: ErrorHandler, loadouts: LoadoutManagerService) {
    this.tisError = error as TISErrorHandler;
    router.events.subscribe((event) => { //fires on every URL change
      title.setTitle(this.getTitleFor(router.url));

      if (router.url in this.page_settings) {
        this.applySettings(this.page_settings[router.url]);
      } else {
        var found = false;

        for (var key in this.page_settings) {
          if (router.url.startsWith(key)) {
            this.applySettings(this.page_settings[key]);
            found = true;
          }
        }

        if(!found)
          this.applySettings(this.default_settings);
      }
    });

    //Custom filters for data tables.
    FilterUtils['isBuyable'] = (value, filter): boolean => {
      if (filter === undefined || filter === null) {
        return true;
      }

      if (value === undefined || value === null) {
        return false;
      }

      return (filter === true) ? (value.TradeEntries.length > 0) : true;
    }
    FilterUtils['service'] = (value: ServiceFlags, filter: ServiceFlags): boolean => {
      console.log(`Service Flags: ${(value & filter) !== ServiceFlags.None}`);

      if (filter === undefined || filter === null) {
        return true;
      }

      if (value === undefined || value === null) {
        return false;
      }

      return (value & filter) !== ServiceFlags.None;
    }

    //Charjs plugin for simple value rendering
    Chart.plugins.register({
      afterDatasetsDraw: function (chartInstance, easing) {
        const opt =
          chartInstance &&
          chartInstance.chart &&
          chartInstance.chart.options &&
          chartInstance.chart.options.plugins &&
          chartInstance.chart.options.plugins.datalabels;
        if (!opt || !opt.display) return;

        // To only draw at the end of animation, check for easing === 1
        const ctx = chartInstance.chart.ctx;
        chartInstance.data.datasets.forEach(function (dataset, i) {
          const meta = chartInstance.getDatasetMeta(i);
          if (!meta.hidden) {
            meta.data.forEach(function (element, index) {
              // Draw the text in black, with the specified font
              ctx.fillStyle = opt.fontColor || 'black';
              const fontSize =  opt.fontSize || 16;
              const fontStyle = opt.fontStyle || 'normal';
              const fontFamily = opt.fontFamily || 'Helvetica Neue';
              ctx.font = Chart.helpers.fontString(fontSize, fontStyle, fontFamily);
              // Just naively convert to string for now
              let dataString = dataset.data[index].toString();
              if (opt.formatter) {
                dataString = opt.formatter(dataString);
                if (dataString === null) return; //skip ones that return null
              }
              // Make sure alignment settings are correct
              ctx.textBaseline = opt.position || 'middle';
              ctx.textAlign = opt.align || 'center';
              const position = element.tooltipPosition();

              ctx.fillText(dataString, position.x, position.y);
            });
          }
        });
      }
    });
  }

  ngOnInit() {
  }

  getTitleFor(url: string): string {
    const title = this.title_map[url];

    if (title) {
      return `TIS - ${title}`;
    } else if (url.startsWith("/loadout")) {
      return `TIS - Loadout Editor`;
    } else if (url.startsWith("/mission/")) {
      return `TIS - Mission Details`;
    } else if (url.startsWith("/maps/")) {
      return `TIS - Map Viewer`;
    } else if (url.startsWith("/ship_maps")) {
      return `TIS - Ship Viewer`;
    } else if (url.startsWith("/giver/")) {
      return `TIS - Mission Giver Details`;
    } else {
      console.warn(`Unknown path: ${url}`);
      return "TIS -  o7";
    }
  }

  showTooltip(e: Event, content: string): void {
    this.globalTooltipContent = content;
    this.globalTooltip.show(e);
  }

  showTooltipOn(target: any, content: string): void {
    this.globalTooltipContent = content;
    this.globalTooltip.show(null, target);
  }

  hideTooltip(): void {
    this.globalTooltip.hide();
  }

  showSearch(e: Event): void {
    e.preventDefault();
    this.searchBlock = true;
  }

  hideSearch(e: Event): void {
    e.preventDefault();
    this.searchBlock = false;
  }
}

export interface PageSettings {
  hide_settings_bar: boolean
}
