import { NgModule, Directive, ElementRef, AfterViewInit, OnDestroy, Input, NgZone, ViewChild, Renderer2, EventEmitter, Output } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppModule } from '../app/app.module';
import { AppComponent } from '../app/app.component';
import { OverlayPanel } from 'primeng/overlaypanel';

@Directive({
    selector: '[tisTooltip]',
})
export class TooltipAttr implements AfterViewInit {

  @Output() onShowTooltip: EventEmitter<ShowTooltipEvent> = new EventEmitter();

  events: (() => void)[];

  @Input("tooltipContent") content: string;

  constructor(public el: ElementRef, public zone: NgZone, public app: AppComponent, public renderer: Renderer2) {
  }

  ngAfterViewInit() {
    this.renderer.addClass(this.el.nativeElement, "tooltip-info");
    this.events = [
      this.renderer.listen(this.el.nativeElement, "mouseenter", (e) => this.onMouseEnter(e)),
      this.renderer.listen(this.el.nativeElement, "mouseleave", (e) => this.onMouseLeave(e))
    ];
  }

  onMouseEnter(e: Event) {
    const showEvent: ShowTooltipEvent = {
      event: e,
      content: this.content ? this.content : "",
      show: true
    };

    this.onShowTooltip.emit(showEvent);

    if (showEvent.show && showEvent.content && showEvent.content !== "") {
      this.app.showTooltip(e, showEvent.content);
    }
  }
    
  onMouseLeave(e: Event) {
    this.app.hideTooltip();
  }

  ngOnDestroy() {
    this.onMouseLeave(null);
    this.events.forEach(e => e());
  }
}

export interface ShowTooltipEvent {
  event: Event,
  content: string,
  show: boolean
}
