"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.LayerOutlinePass = void 0;
var three_1 = require("three");
var Pass_js_1 = require("three/examples/jsm/postprocessing/Pass.js");
var CopyShader_js_1 = require("three/examples/jsm/shaders/CopyShader.js");
var LayerOutlinePass = /** @class */ (function (_super) {
    __extends(LayerOutlinePass, _super);
    function LayerOutlinePass(resolution, scene, camera, objectLayer) {
        if (objectLayer === void 0) { objectLayer = 2; }
        var _this = _super.call(this) || this;
        _this.renderScene = scene;
        _this.renderCamera = camera;
        _this.objectLayer = objectLayer;
        _this.edgeColor = new three_1.Color(1, 1, 1);
        _this.edgeThickness = 1.0;
        _this.downSampleRatio = 2;
        _this.pulsePeriod = 0;
        _this.resolution = (resolution !== undefined) ? new three_1.Vector2(resolution.x, resolution.y) : new three_1.Vector2(256, 256);
        var pars = { minFilter: three_1.LinearFilter, magFilter: three_1.LinearFilter, format: three_1.RGBAFormat };
        var resx = Math.round(_this.resolution.x / _this.downSampleRatio);
        var resy = Math.round(_this.resolution.y / _this.downSampleRatio);
        _this.maskBufferMaterial = new three_1.MeshBasicMaterial({ color: 0xffffff });
        _this.maskBufferMaterial.side = three_1.DoubleSide;
        _this.renderTargetMaskBuffer = new three_1.WebGLRenderTarget(_this.resolution.x, _this.resolution.y, pars);
        _this.renderTargetMaskBuffer.texture.name = 'OutlinePass.mask';
        _this.renderTargetMaskBuffer.texture.generateMipmaps = false;
        _this.depthMaterial = new three_1.MeshDepthMaterial();
        _this.depthMaterial.side = three_1.DoubleSide;
        _this.depthMaterial.depthPacking = three_1.RGBADepthPacking;
        _this.depthMaterial.blending = three_1.NoBlending;
        _this.prepareMaskMaterial = new three_1.MeshBasicMaterial({ color: new three_1.Color(0x000000) });
        _this.prepareMaskMaterial.side = three_1.DoubleSide;
        _this.renderTargetDepthBuffer = new three_1.WebGLRenderTarget(_this.resolution.x, _this.resolution.y, pars);
        _this.renderTargetDepthBuffer.texture.name = 'OutlinePass.depth';
        _this.renderTargetDepthBuffer.texture.generateMipmaps = false;
        _this.renderTargetMaskDownSampleBuffer = new three_1.WebGLRenderTarget(resx, resy, pars);
        _this.renderTargetMaskDownSampleBuffer.texture.name = 'OutlinePass.depthDownSample';
        _this.renderTargetMaskDownSampleBuffer.texture.generateMipmaps = false;
        _this.edgeDetectionMaterial = _this.getEdgeDetectionMaterial();
        _this.renderTargetEdgeBuffer = new three_1.WebGLRenderTarget(resx, resy, pars);
        _this.renderTargetEdgeBuffer.texture.name = 'OutlinePass.edge1';
        _this.renderTargetEdgeBuffer.texture.generateMipmaps = false;
        // Overlay material
        _this.overlayMaterial = _this.getOverlayMaterial();
        // copy material
        if (CopyShader_js_1.CopyShader === undefined)
            console.error('THREE.OutlinePass relies on CopyShader');
        var copyShader = CopyShader_js_1.CopyShader;
        _this.copyUniforms = three_1.UniformsUtils.clone(copyShader.uniforms);
        _this.copyUniforms['opacity'].value = 1.0;
        _this.materialCopy = new three_1.ShaderMaterial({
            uniforms: _this.copyUniforms,
            vertexShader: copyShader.vertexShader,
            fragmentShader: copyShader.fragmentShader,
            blending: three_1.NoBlending,
            depthTest: false,
            depthWrite: false,
            transparent: true
        });
        _this.enabled = true;
        _this.needsSwap = false;
        _this._oldClearColor = new three_1.Color();
        _this.oldClearAlpha = 1;
        _this.fsQuad = new Pass_js_1.FullScreenQuad(null);
        _this.tempPulseColor = new three_1.Color();
        _this.textureMatrix = new three_1.Matrix4();
        function replaceDepthToViewZ(string, camera) {
            var type = camera.isPerspectiveCamera ? 'perspective' : 'orthographic';
            return string.replace(/DEPTH_TO_VIEW_Z/g, type + 'DepthToViewZ');
        }
        return _this;
    }
    LayerOutlinePass.prototype.dispose = function () {
        this.renderTargetMaskBuffer.dispose();
        this.renderTargetDepthBuffer.dispose();
        this.renderTargetMaskDownSampleBuffer.dispose();
        this.renderTargetEdgeBuffer.dispose();
    };
    LayerOutlinePass.prototype.setSize = function (width, height) {
        this.renderTargetMaskBuffer.setSize(width, height);
        this.renderTargetDepthBuffer.setSize(width, height);
        var resx = Math.round(width / this.downSampleRatio);
        var resy = Math.round(height / this.downSampleRatio);
        this.renderTargetMaskDownSampleBuffer.setSize(resx, resy);
        this.renderTargetEdgeBuffer.setSize(resx, resy);
    };
    LayerOutlinePass.prototype.updateTextureMatrix = function () {
        this.textureMatrix.set(0.5, 0.0, 0.0, 0.5, 0.0, 0.5, 0.0, 0.5, 0.0, 0.0, 0.5, 0.5, 0.0, 0.0, 0.0, 1.0);
        this.textureMatrix.multiply(this.renderCamera.projectionMatrix);
        this.textureMatrix.multiply(this.renderCamera.matrixWorldInverse);
    };
    LayerOutlinePass.prototype.render = function (renderer, writeBuffer, readBuffer, deltaTime, maskActive) {
        var _this = this;
        var someVisible = false;
        this.renderScene.traverseVisible(function (obj) {
            if (obj.layers.isEnabled(_this.objectLayer)) {
                someVisible = true;
            }
        });
        if (someVisible) {
            renderer.getClearColor(this._oldClearColor);
            this.oldClearAlpha = renderer.getClearAlpha();
            var oldAutoClear = renderer.autoClear;
            renderer.autoClear = false;
            if (maskActive)
                renderer.state.buffers.stencil.setTest(false);
            renderer.setClearColor(0xffffff, 1);
            // Make only selected objects visible
            var camera_mask_old = this.renderCamera.layers.mask;
            this.renderCamera.layers.disableAll();
            this.renderCamera.layers.enable(this.objectLayer);
            var currentBackground = this.renderScene.background;
            this.renderScene.background = null;
            // 1. Render mask for background objects.
            this.renderScene.overrideMaterial = this.prepareMaskMaterial;
            renderer.setRenderTarget(this.renderTargetMaskBuffer);
            renderer.clear();
            renderer.render(this.renderScene, this.renderCamera);
            this.renderScene.overrideMaterial = null;
            this.renderScene.background = currentBackground;
            // 2. Downsample to Half resolution
            this.fsQuad.material = this.materialCopy;
            this.copyUniforms['tDiffuse'].value = this.renderTargetMaskBuffer.texture;
            renderer.setRenderTarget(this.renderTargetMaskDownSampleBuffer);
            renderer.clear();
            this.fsQuad.render(renderer);
            // .. Update pulse colors
            this.tempPulseColor.copy(this.edgeColor);
            if (this.pulsePeriod > 0) {
                var scalar = (1 + 0.25) / 2 + Math.cos(performance.now() * 0.01 / this.pulsePeriod) * (1.0 - 0.25) / 2;
                this.tempPulseColor.multiplyScalar(scalar);
            }
            // 3. Apply Edge Detection Pass
            this.fsQuad.material = this.edgeDetectionMaterial;
            this.edgeDetectionMaterial.uniforms['maskTexture'].value = this.renderTargetMaskDownSampleBuffer.texture;
            this.edgeDetectionMaterial.uniforms['texSize'].value.set(this.renderTargetMaskDownSampleBuffer.width, this.renderTargetMaskDownSampleBuffer.height);
            this.edgeDetectionMaterial.uniforms['edgeColor'].value = this.tempPulseColor;
            this.edgeDetectionMaterial.uniforms['edgeThickness'].value = this.edgeThickness;
            renderer.setRenderTarget(this.renderTargetEdgeBuffer);
            renderer.clear();
            this.fsQuad.render(renderer);
            // Blend it additively over the input texture
            this.fsQuad.material = this.overlayMaterial;
            this.overlayMaterial.uniforms['maskTexture'].value = this.renderTargetMaskBuffer.texture;
            this.overlayMaterial.uniforms['edgeTexture'].value = this.renderTargetEdgeBuffer.texture;
            if (maskActive)
                renderer.state.buffers.stencil.setTest(true);
            renderer.setRenderTarget(writeBuffer);
            this.fsQuad.render(renderer);
            renderer.setClearColor(this._oldClearColor, this.oldClearAlpha);
            renderer.autoClear = oldAutoClear;
            this.renderCamera.layers.mask = camera_mask_old;
        }
    };
    LayerOutlinePass.prototype.getPrepareMaskMaterial = function () {
        return new three_1.ShaderMaterial({
            uniforms: {
                'depthTexture': { value: null },
                'cameraNearFar': { value: new three_1.Vector2(0.5, 0.5) },
                'textureMatrix': { value: null }
            },
            vertexShader: "#include <morphtarget_pars_vertex>\n\t\t\t\t#include <skinning_pars_vertex>\n\n\t\t\t\tvarying vec4 projTexCoord;\n\t\t\t\tvarying vec4 vPosition;\n\t\t\t\tuniform mat4 textureMatrix;\n\n\t\t\t\tvoid main() {\n\n\t\t\t\t\t#include <skinbase_vertex>\n\t\t\t\t\t#include <begin_vertex>\n\t\t\t\t\t#include <morphtarget_vertex>\n\t\t\t\t\t#include <skinning_vertex>\n\t\t\t\t\t#include <project_vertex>\n\n\t\t\t\t\tvPosition = mvPosition;\n\t\t\t\t\tvec4 worldPosition = modelMatrix * vec4( transformed, 1.0 );\n\t\t\t\t\tprojTexCoord = textureMatrix * worldPosition;\n\n\t\t\t\t}",
            fragmentShader: "#include <packing>\n\t\t\t\tvarying vec4 vPosition;\n\t\t\t\tvarying vec4 projTexCoord;\n\t\t\t\tuniform sampler2D depthTexture;\n\t\t\t\tuniform vec2 cameraNearFar;\n\n\t\t\t\tvoid main() {\n\n\t\t\t\t\tfloat depth = unpackRGBAToDepth(texture2DProj( depthTexture, projTexCoord ));\n\t\t\t\t\tfloat viewZ = - DEPTH_TO_VIEW_Z( depth, cameraNearFar.x, cameraNearFar.y );\n\t\t\t\t\tfloat depthTest = (-vPosition.z > viewZ) ? 1.0 : 0.0;\n\t\t\t\t\tgl_FragColor = vec4(0.0, depthTest, 1.0, 1.0);\n\n\t\t\t\t}"
        });
    };
    LayerOutlinePass.prototype.getEdgeDetectionMaterial = function () {
        return new three_1.ShaderMaterial({
            uniforms: {
                'maskTexture': { value: null },
                'texSize': { value: new three_1.Vector2(0.5, 0.5) },
                'edgeColor': { value: new three_1.Vector3(1.0, 1.0, 1.0) },
                'edgeThickness': { value: 5 }
            },
            vertexShader: "varying vec2 vUv;\n\n\t\t\t\tvoid main() {\n\t\t\t\t\tvUv = uv;\n\t\t\t\t\tgl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );\n\t\t\t\t}",
            fragmentShader: "varying vec2 vUv;\n\n\t\t\t\tuniform sampler2D maskTexture;\n\t\t\t\tuniform vec2 texSize;\n\t\t\t\tuniform vec3 edgeColor;\n                uniform float edgeThickness;\n\n\t\t\t\tvoid main() {\n\t\t\t\t\tvec2 invSize = 1.0 / texSize;\n\t\t\t\t\tvec4 uvOffset = vec4(edgeThickness, -edgeThickness, 0.0, -edgeThickness) * vec4(invSize, invSize);\n\t\t\t\t\tvec4 c1 = texture2D( maskTexture, vUv + uvOffset.xy);\n\t\t\t\t\tvec4 c2 = texture2D( maskTexture, vUv - uvOffset.xy);\n\t\t\t\t\tvec4 c3 = texture2D( maskTexture, vUv + uvOffset.yw);\n\t\t\t\t\tvec4 c4 = texture2D( maskTexture, vUv - uvOffset.yw);\n                    vec4 c5 = texture2D( maskTexture, vUv + uvOffset.xz);\n                    vec4 c6 = texture2D( maskTexture, vUv - uvOffset.xz);\n                    vec4 c7 = texture2D( maskTexture, vUv + uvOffset.zx);\n                    vec4 c8 = texture2D( maskTexture, vUv - uvOffset.zx);\n\t\t\t\t\tfloat diff1 = (c1.r - c2.r)*0.5;\n\t\t\t\t\tfloat diff2 = (c3.r - c4.r)*0.5;\n\t\t\t\t\tfloat diff3 = (c5.r - c6.r)*0.5;\n\t\t\t\t\tfloat diff4 = (c7.r - c8.r)*0.5;\n\t\t\t\t\tfloat d = length( vec2(diff1, diff2) ) + length(vec2(diff3, diff4));\n                    if(d > 1.0) d = 1.0;\n\t\t\t\t\tgl_FragColor = vec4(edgeColor, 1.0) * vec4(d);\n\t\t\t\t}"
        });
    };
    LayerOutlinePass.prototype.getOverlayMaterial = function () {
        return new three_1.ShaderMaterial({
            uniforms: {
                'maskTexture': { value: null },
                'edgeTexture': { value: null },
            },
            vertexShader: "varying vec2 vUv;\n\n\t\t\t\tvoid main() {\n\t\t\t\t\tvUv = uv;\n\t\t\t\t\tgl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );\n\t\t\t\t}",
            fragmentShader: "varying vec2 vUv;\n\n\t\t\t\tuniform sampler2D maskTexture;\n\t\t\t\tuniform sampler2D edgeTexture;\n\n\t\t\t\tvoid main() {\n\t\t\t\t\tvec4 edgeValue = texture2D(edgeTexture, vUv);\n\t\t\t\t\tvec4 maskColor = texture2D(maskTexture, vUv);\n\t\t\t\t\tfloat visibilityFactor = maskColor.r > 0.0 ? 1.0 : 0.0;\n                    //Jenky but it works :)\n\t\t\t\t\tvec4 finalColor = 3.0 * visibilityFactor * edgeValue;\n\t\t\t\t\tgl_FragColor = finalColor;\n\t\t\t\t}",
            blending: three_1.CustomBlending,
            blendEquation: three_1.AddEquation,
            blendSrc: three_1.SrcAlphaFactor,
            blendDst: three_1.OneMinusSrcAlphaFactor,
            depthTest: false,
            depthWrite: false,
            transparent: true
        });
    };
    return LayerOutlinePass;
}(Pass_js_1.Pass));
exports.LayerOutlinePass = LayerOutlinePass;
//# sourceMappingURL=LayerOutlineController.js.map