
import * as THREE from 'three';
import { UniformsLib, UniformsUtils } from 'three';

export class SliceMaterial extends THREE.ShaderMaterial
{

  constructor() {
    super({
      uniforms: UniformsUtils.merge([
        UniformsLib.lights,
        UniformsUtils.clone(SliceMaterial.base_uniforms)
      ]),
        
      
      vertexShader: SliceMaterial.vertex_text,
      fragmentShader: SliceMaterial.fragment_text,

      lights: true,
      side: THREE.FrontSide,
      transparent: true,
    })
  }

  public setColor(color: THREE.Vector4) {
    this.uniforms.u_color.value = color;
  }
  public setSliceEnabled(enabled: boolean) {
    this.uniforms.v_focus_enable.value = enabled;
  }
  public setSliceHeight(height: number) {
    this.uniforms.v_focus_height.value = height;
  }
  public setSliceTop(top: number) {
    this.uniforms.v_focus_top.value = top;
  }
  public setSliceBottom(bottom: number) {
    this.uniforms.v_focus_bottom.value = bottom;
  }

  private static base_uniforms = {
    u_color: {
      value: new THREE.Vector4(1.0, 1.0, 1.0, 1.0)
    },
    offset: {
      type: 'f',
      value: 1
    },

    v_focus_enable: {
      type: 'b',
      value: false
    },
    v_focus_height: {
      type: 'f',
      value: 2
    },
    v_focus_top: {
      type: 'f',
      value: 0.5
    },
    v_focus_bottom: {
      type: 'f',
      value: 0.5
    }
  };

  static vertex_text: string = `
#define GLSLIFY 1
// Common varyings
varying vec3 v_position;
varying vec3 v_normal;
varying vec4 v_position_world;
varying vec3 v_refract;

uniform float offset;

/*
 * The main program
 */
void main() {
    #include <begin_vertex>

    // Save the varyings
    v_position = position;
    v_normal = normalize(normalMatrix * normal);

    vec4 pos = modelViewMatrix * vec4( v_position.xyz + normal.xyz * offset, 1.0 );

    // Vertex shader output
    gl_Position = projectionMatrix * modelViewMatrix * pos;//vec4(pos, 1.0);

    #include <project_vertex>

    v_position_world = modelMatrix * vec4( position, 1.0 );

	vec3 worldNormal = normalize ( mat3( modelMatrix[0].xyz, modelMatrix[1].xyz, modelMatrix[2].xyz ) * normal );
	vec3 I = v_position_world.xyz - cameraPosition;
	v_refract = refract( normalize( I ), worldNormal, 1.02 );

}`;
  static fragment_text: string = `
#define GLSLIFY 1
// Common uniforms

#include <common>

uniform bool receiveShadow;
uniform vec3 ambientLightColor;
uniform vec3 lightProbe[ 9 ];

vec3 shGetIrradianceAt( in vec3 normal, in vec3 shCoefficients[ 9 ] ) {

	// normal is assumed to have unit length

	float x = normal.x, y = normal.y, z = normal.z;

	// band 0
	vec3 result = shCoefficients[ 0 ] * 0.886227;

	// band 1
	result += shCoefficients[ 1 ] * 2.0 * 0.511664 * y;
	result += shCoefficients[ 2 ] * 2.0 * 0.511664 * z;
	result += shCoefficients[ 3 ] * 2.0 * 0.511664 * x;

	// band 2
	result += shCoefficients[ 4 ] * 2.0 * 0.429043 * x * y;
	result += shCoefficients[ 5 ] * 2.0 * 0.429043 * y * z;
	result += shCoefficients[ 6 ] * ( 0.743125 * z * z - 0.247708 );
	result += shCoefficients[ 7 ] * 2.0 * 0.429043 * x * z;
	result += shCoefficients[ 8 ] * 0.429043 * ( x * x - y * y );

	return result;

}

vec3 getLightProbeIrradiance( const in vec3 lightProbe[ 9 ], const in vec3 normal ) {

	vec3 worldNormal = inverseTransformDirection( normal, viewMatrix );

	vec3 irradiance = shGetIrradianceAt( worldNormal, lightProbe );

	return irradiance;

}

vec3 getAmbientLightIrradiance( const in vec3 ambientLightColor ) {

	vec3 irradiance = ambientLightColor;

	return irradiance;

}

float getDistanceAttenuation( const in float lightDistance, const in float cutoffDistance, const in float decayExponent ) {

	#if defined ( PHYSICALLY_CORRECT_LIGHTS )

		// based upon Frostbite 3 Moving to Physically-based Rendering
		// page 32, equation 26: E[window1]
		// https://seblagarde.files.wordpress.com/2015/07/course_notes_moving_frostbite_to_pbr_v32.pdf
		float distanceFalloff = 1.0 / max( pow( lightDistance, decayExponent ), 0.01 );

		if ( cutoffDistance > 0.0 ) {

			distanceFalloff *= pow2( saturate( 1.0 - pow4( lightDistance / cutoffDistance ) ) );

		}

		return distanceFalloff;

	#else

		if ( cutoffDistance > 0.0 && decayExponent > 0.0 ) {

			return pow( saturate( - lightDistance / cutoffDistance + 1.0 ), decayExponent );

		}

		return 1.0;

	#endif

}

float getSpotAttenuation( const in float coneCosine, const in float penumbraCosine, const in float angleCosine ) {

	return smoothstep( coneCosine, penumbraCosine, angleCosine );

}


struct DirectionalLight {
	vec3 direction;
	vec3 color;
};

uniform DirectionalLight directionalLights[ NUM_DIR_LIGHTS ];

void getDirectionalLightInfo( const in DirectionalLight directionalLight, const in GeometricContext geometry, out IncidentLight light ) {

	light.color = directionalLight.color;
	light.direction = directionalLight.direction;
	light.visible = true;

}


uniform vec4 u_color;

uniform bool v_focus_enable;
uniform float v_focus_height;
uniform float v_focus_top;
uniform float v_focus_bottom;

// Common varyings
varying vec3 v_position;
varying vec3 v_normal;
varying vec4 v_position_world;
varying vec3 v_refract;

float falloff_distance = 0.25;


/*
 *  Calculates the diffuse factor produced by the light illumination
 */
float diffuseFactor(vec3 normal, vec3 light_direction) {
    float df = dot(normalize(normal), normalize(light_direction));

    if (gl_FrontFacing) {
        df = -df;
    }

    return max(0.0, df);
}

/*
 * The main program
 */
void main() {

    float alpha = 1.0;

    if(v_focus_enable) {
      float height_off = v_position_world.y - v_focus_height;
      if(height_off > v_focus_top || height_off < -v_focus_bottom) {
        //Outside of f_focus_top/bottom, ignore
        discard;
      }else if(height_off > v_focus_top - falloff_distance) {
        alpha = 1.0 - ((height_off - (v_focus_top - falloff_distance)) / falloff_distance);
      }else if(height_off < -(v_focus_bottom - falloff_distance)) {
        alpha = 1.0 - ((height_off + (v_focus_bottom - falloff_distance)) / -falloff_distance);
      }
    }

	float directionalLightWeighting = max( dot( normalize( v_normal ), directionalLights[0].direction ), 0.0);
	vec3 lightWeighting = ambientLightColor + directionalLights[0].color * directionalLightWeighting;

	float intensity = smoothstep( - 0.5, 1.0, pow( length(lightWeighting), 20.0 ) );
	intensity += length(lightWeighting) * 0.2;

    gl_FragColor = u_color * vec4(lightWeighting, alpha);
}`;
} 
