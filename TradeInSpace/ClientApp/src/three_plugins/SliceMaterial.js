"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.SliceMaterial = void 0;
var THREE = require("three");
var three_1 = require("three");
var SliceMaterial = /** @class */ (function (_super) {
    __extends(SliceMaterial, _super);
    function SliceMaterial() {
        return _super.call(this, {
            uniforms: three_1.UniformsUtils.merge([
                three_1.UniformsLib.lights,
                three_1.UniformsUtils.clone(SliceMaterial.base_uniforms)
            ]),
            vertexShader: SliceMaterial.vertex_text,
            fragmentShader: SliceMaterial.fragment_text,
            lights: true,
            side: THREE.FrontSide,
            transparent: true,
        }) || this;
    }
    SliceMaterial.prototype.setColor = function (color) {
        this.uniforms.u_color.value = color;
    };
    SliceMaterial.prototype.setSliceEnabled = function (enabled) {
        this.uniforms.v_focus_enable.value = enabled;
    };
    SliceMaterial.prototype.setSliceHeight = function (height) {
        this.uniforms.v_focus_height.value = height;
    };
    SliceMaterial.prototype.setSliceTop = function (top) {
        this.uniforms.v_focus_top.value = top;
    };
    SliceMaterial.prototype.setSliceBottom = function (bottom) {
        this.uniforms.v_focus_bottom.value = bottom;
    };
    SliceMaterial.base_uniforms = {
        u_color: {
            value: new THREE.Vector4(1.0, 1.0, 1.0, 1.0)
        },
        offset: {
            type: 'f',
            value: 1
        },
        v_focus_enable: {
            type: 'b',
            value: false
        },
        v_focus_height: {
            type: 'f',
            value: 2
        },
        v_focus_top: {
            type: 'f',
            value: 0.5
        },
        v_focus_bottom: {
            type: 'f',
            value: 0.5
        }
    };
    SliceMaterial.vertex_text = "\n#define GLSLIFY 1\n// Common varyings\nvarying vec3 v_position;\nvarying vec3 v_normal;\nvarying vec4 v_position_world;\nvarying vec3 v_refract;\n\nuniform float offset;\n\n/*\n * The main program\n */\nvoid main() {\n    #include <begin_vertex>\n\n    // Save the varyings\n    v_position = position;\n    v_normal = normalize(normalMatrix * normal);\n\n    vec4 pos = modelViewMatrix * vec4( v_position.xyz + normal.xyz * offset, 1.0 );\n\n    // Vertex shader output\n    gl_Position = projectionMatrix * modelViewMatrix * pos;//vec4(pos, 1.0);\n\n    #include <project_vertex>\n\n    v_position_world = modelMatrix * vec4( position, 1.0 );\n\n\tvec3 worldNormal = normalize ( mat3( modelMatrix[0].xyz, modelMatrix[1].xyz, modelMatrix[2].xyz ) * normal );\n\tvec3 I = v_position_world.xyz - cameraPosition;\n\tv_refract = refract( normalize( I ), worldNormal, 1.02 );\n\n}";
    SliceMaterial.fragment_text = "\n#define GLSLIFY 1\n// Common uniforms\n\n#include <common>\n\nuniform bool receiveShadow;\nuniform vec3 ambientLightColor;\nuniform vec3 lightProbe[ 9 ];\n\nvec3 shGetIrradianceAt( in vec3 normal, in vec3 shCoefficients[ 9 ] ) {\n\n\t// normal is assumed to have unit length\n\n\tfloat x = normal.x, y = normal.y, z = normal.z;\n\n\t// band 0\n\tvec3 result = shCoefficients[ 0 ] * 0.886227;\n\n\t// band 1\n\tresult += shCoefficients[ 1 ] * 2.0 * 0.511664 * y;\n\tresult += shCoefficients[ 2 ] * 2.0 * 0.511664 * z;\n\tresult += shCoefficients[ 3 ] * 2.0 * 0.511664 * x;\n\n\t// band 2\n\tresult += shCoefficients[ 4 ] * 2.0 * 0.429043 * x * y;\n\tresult += shCoefficients[ 5 ] * 2.0 * 0.429043 * y * z;\n\tresult += shCoefficients[ 6 ] * ( 0.743125 * z * z - 0.247708 );\n\tresult += shCoefficients[ 7 ] * 2.0 * 0.429043 * x * z;\n\tresult += shCoefficients[ 8 ] * 0.429043 * ( x * x - y * y );\n\n\treturn result;\n\n}\n\nvec3 getLightProbeIrradiance( const in vec3 lightProbe[ 9 ], const in vec3 normal ) {\n\n\tvec3 worldNormal = inverseTransformDirection( normal, viewMatrix );\n\n\tvec3 irradiance = shGetIrradianceAt( worldNormal, lightProbe );\n\n\treturn irradiance;\n\n}\n\nvec3 getAmbientLightIrradiance( const in vec3 ambientLightColor ) {\n\n\tvec3 irradiance = ambientLightColor;\n\n\treturn irradiance;\n\n}\n\nfloat getDistanceAttenuation( const in float lightDistance, const in float cutoffDistance, const in float decayExponent ) {\n\n\t#if defined ( PHYSICALLY_CORRECT_LIGHTS )\n\n\t\t// based upon Frostbite 3 Moving to Physically-based Rendering\n\t\t// page 32, equation 26: E[window1]\n\t\t// https://seblagarde.files.wordpress.com/2015/07/course_notes_moving_frostbite_to_pbr_v32.pdf\n\t\tfloat distanceFalloff = 1.0 / max( pow( lightDistance, decayExponent ), 0.01 );\n\n\t\tif ( cutoffDistance > 0.0 ) {\n\n\t\t\tdistanceFalloff *= pow2( saturate( 1.0 - pow4( lightDistance / cutoffDistance ) ) );\n\n\t\t}\n\n\t\treturn distanceFalloff;\n\n\t#else\n\n\t\tif ( cutoffDistance > 0.0 && decayExponent > 0.0 ) {\n\n\t\t\treturn pow( saturate( - lightDistance / cutoffDistance + 1.0 ), decayExponent );\n\n\t\t}\n\n\t\treturn 1.0;\n\n\t#endif\n\n}\n\nfloat getSpotAttenuation( const in float coneCosine, const in float penumbraCosine, const in float angleCosine ) {\n\n\treturn smoothstep( coneCosine, penumbraCosine, angleCosine );\n\n}\n\n\nstruct DirectionalLight {\n\tvec3 direction;\n\tvec3 color;\n};\n\nuniform DirectionalLight directionalLights[ NUM_DIR_LIGHTS ];\n\nvoid getDirectionalLightInfo( const in DirectionalLight directionalLight, const in GeometricContext geometry, out IncidentLight light ) {\n\n\tlight.color = directionalLight.color;\n\tlight.direction = directionalLight.direction;\n\tlight.visible = true;\n\n}\n\n\nuniform vec4 u_color;\n\nuniform bool v_focus_enable;\nuniform float v_focus_height;\nuniform float v_focus_top;\nuniform float v_focus_bottom;\n\n// Common varyings\nvarying vec3 v_position;\nvarying vec3 v_normal;\nvarying vec4 v_position_world;\nvarying vec3 v_refract;\n\nfloat falloff_distance = 0.25;\n\n\n/*\n *  Calculates the diffuse factor produced by the light illumination\n */\nfloat diffuseFactor(vec3 normal, vec3 light_direction) {\n    float df = dot(normalize(normal), normalize(light_direction));\n\n    if (gl_FrontFacing) {\n        df = -df;\n    }\n\n    return max(0.0, df);\n}\n\n/*\n * The main program\n */\nvoid main() {\n\n    float alpha = 1.0;\n\n    if(v_focus_enable) {\n      float height_off = v_position_world.y - v_focus_height;\n      if(height_off > v_focus_top || height_off < -v_focus_bottom) {\n        //Outside of f_focus_top/bottom, ignore\n        discard;\n      }else if(height_off > v_focus_top - falloff_distance) {\n        alpha = 1.0 - ((height_off - (v_focus_top - falloff_distance)) / falloff_distance);\n      }else if(height_off < -(v_focus_bottom - falloff_distance)) {\n        alpha = 1.0 - ((height_off + (v_focus_bottom - falloff_distance)) / -falloff_distance);\n      }\n    }\n\n\tfloat directionalLightWeighting = max( dot( normalize( v_normal ), directionalLights[0].direction ), 0.0);\n\tvec3 lightWeighting = ambientLightColor + directionalLights[0].color * directionalLightWeighting;\n\n\tfloat intensity = smoothstep( - 0.5, 1.0, pow( length(lightWeighting), 20.0 ) );\n\tintensity += length(lightWeighting) * 0.2;\n\n    gl_FragColor = u_color * vec4(lightWeighting, alpha);\n}";
    return SliceMaterial;
}(THREE.ShaderMaterial));
exports.SliceMaterial = SliceMaterial;
//# sourceMappingURL=SliceMaterial.js.map