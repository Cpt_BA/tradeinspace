"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.STATE = exports.MapControls = exports.OrbitControls = void 0;
var THREE = require("three");
var three_1 = require("three");
// Unlike TrackballControls, it maintains the "up" direction object.up (+Y by default).
//
//    Orbit - left mouse / touch: one-finger move
//    Zoom - middle mouse, or mousewheel / touch: two-finger spread or squish
//    Pan - right mouse, or left mouse + ctrl/meta/shiftKey, or arrow keys / touch: two-finger move
var _changeEvent = {
    type: 'change'
};
var _startEvent = {
    type: 'start'
};
var _endEvent = {
    type: 'end'
};
var OrbitControls = /** @class */ (function (_super) {
    __extends(OrbitControls, _super);
    function OrbitControls(object, domElement) {
        var _this = _super.call(this) || this;
        _this.focusHeight = 0;
        _this.target_helper = null;
        _this.target = new THREE.Vector3(0, 0, 0);
        _this.minDistance = 0;
        _this.maxDistance = Infinity;
        _this.minZoom = 0;
        _this.maxZoom = Infinity;
        _this.minPolarAngle = 0;
        _this.maxPolarAngle = Math.PI;
        _this.minAzimuthAngle = -Infinity;
        _this.maxAzimuthAngle = Infinity;
        _this.enableDamping = false;
        _this.dampingFactor = 0.05;
        _this.enableZoom = true;
        _this.zoomSpeed = 1.0;
        _this.enableRotate = true;
        _this.rotateSpeed = 1.0;
        _this.enablePan = true;
        _this.panSpeed = 1.0;
        _this.screenSpacePanning = false;
        _this.keyPanSpeed = 7.0;
        _this.autoRotate = false;
        _this.autoRotateSpeed = 2.0;
        _this.keys = {
            LEFT: 'ArrowLeft',
            UP: 'ArrowUp',
            RIGHT: 'ArrowRight',
            BOTTOM: 'ArrowDown'
        }; // Mouse buttons
        _this.mouseButtons = {
            LEFT: THREE.MOUSE.ROTATE,
            MIDDLE: THREE.MOUSE.DOLLY,
            RIGHT: THREE.MOUSE.PAN
        }; // Touch fingers
        _this.touches = {
            ONE: THREE.TOUCH.ROTATE,
            TWO: THREE.TOUCH.DOLLY_PAN
        }; // for reset
        _this.state = STATE.NONE;
        _this.scale = 1;
        _this.zoomChanged = false;
        _this.EPS = 0.000001; // current position in spherical coordinates
        _this.spherical = new THREE.Spherical();
        _this.sphericalDelta = new THREE.Spherical();
        _this.panOffset = new THREE.Vector3();
        _this.rotateStart = new THREE.Vector2();
        _this.rotateEnd = new THREE.Vector2();
        _this.rotateDelta = new THREE.Vector2();
        _this.panStart = new THREE.Vector2();
        _this.panEnd = new THREE.Vector2();
        _this.panDelta = new THREE.Vector2();
        _this.dollyStart = new THREE.Vector2();
        _this.dollyEnd = new THREE.Vector2();
        _this.dollyDelta = new THREE.Vector2();
        _this.pointers = [];
        _this.pointerPositions = {};
        // event handlers - FSM: listen for events and reset state
        //
        _this.onPointerDown = function (event) {
            if (_this.enabled === false)
                return;
            if (_this.pointers.length === 0) {
                _this.domElement.setPointerCapture(event.pointerId);
                _this.domElement.addEventListener('pointermove', _this.onPointerMove);
                _this.domElement.addEventListener('pointerup', _this.onPointerUp);
            } //
            _this.addPointer(event);
            if (event.pointerType === 'touch') {
                _this.onTouchStart(event);
            }
            else {
                _this.onMouseDown(event);
            }
        };
        _this.onPointerMove = function (event) {
            if (_this.enabled === false)
                return;
            if (event.pointerType === 'touch') {
                _this.onTouchMove(event);
            }
            else {
                _this.onMouseMove(event);
            }
        };
        _this.onPointerUp = function (event) {
            _this.removePointer(event);
            if (_this.pointers.length === 0) {
                _this.domElement.releasePointerCapture(event.pointerId);
                _this.domElement.removeEventListener('pointermove', _this.onPointerMove);
                _this.domElement.removeEventListener('pointerup', _this.onPointerUp);
            }
            _this.dispatchEvent(_endEvent);
            _this.state = STATE.NONE;
        };
        _this.onPointerCancel = function (event) {
            _this.removePointer(event);
        };
        _this.onMouseDown = function (event) {
            var mouseAction;
            switch (event.button) {
                case 0:
                    mouseAction = _this.mouseButtons.LEFT;
                    break;
                case 1:
                    mouseAction = _this.mouseButtons.MIDDLE;
                    break;
                case 2:
                    mouseAction = _this.mouseButtons.RIGHT;
                    break;
                default:
                    mouseAction = -1;
            }
            switch (mouseAction) {
                case THREE.MOUSE.DOLLY:
                    if (_this.enableZoom === false)
                        return;
                    _this.handleMouseDownDolly(event);
                    _this.state = STATE.DOLLY;
                    break;
                case THREE.MOUSE.ROTATE:
                    if (event.ctrlKey || event.metaKey || event.shiftKey) {
                        if (_this.enablePan === false)
                            return;
                        _this.handleMouseDownPan(event);
                        _this.state = STATE.PAN;
                    }
                    else {
                        if (_this.enableRotate === false)
                            return;
                        _this.handleMouseDownRotate(event);
                        _this.state = STATE.ROTATE;
                    }
                    break;
                case THREE.MOUSE.PAN:
                    if (event.ctrlKey || event.metaKey || event.shiftKey) {
                        if (_this.enableRotate === false)
                            return;
                        _this.handleMouseDownRotate(event);
                        _this.state = STATE.ROTATE;
                    }
                    else {
                        if (_this.enablePan === false)
                            return;
                        _this.handleMouseDownPan(event);
                        _this.state = STATE.PAN;
                    }
                    break;
                default:
                    _this.state = STATE.NONE;
            }
            if (_this.state !== STATE.NONE) {
                _this.dispatchEvent(_startEvent);
            }
        };
        _this.onMouseMove = function (event) {
            switch (_this.state) {
                case STATE.ROTATE:
                    if (_this.enableRotate === false)
                        return;
                    _this.handleMouseMoveRotate(event);
                    break;
                case STATE.DOLLY:
                    if (_this.enableZoom === false)
                        return;
                    _this.handleMouseMoveDolly(event);
                    break;
                case STATE.PAN:
                    if (_this.enablePan === false)
                        return;
                    _this.handleMouseMovePan(event);
                    break;
            }
        };
        _this.onMouseWheel = function (event) {
            if (_this.enabled === false || _this.enableZoom === false || _this.state !== STATE.NONE)
                return;
            event.preventDefault();
            _this.dispatchEvent(_startEvent);
            _this.handleMouseWheel(event);
            _this.dispatchEvent(_endEvent);
        };
        _this.onKeyDown = function (event) {
            if (_this.enabled === false || _this.enablePan === false)
                return;
            _this.handleKeyDown(event);
        };
        _this.onTouchStart = function (event) {
            _this.trackPointer(event);
            switch (_this.pointers.length) {
                case 1:
                    switch (_this.touches.ONE) {
                        case THREE.TOUCH.ROTATE:
                            if (_this.enableRotate === false)
                                return;
                            _this.handleTouchStartRotate();
                            _this.state = STATE.TOUCH_ROTATE;
                            break;
                        case THREE.TOUCH.PAN:
                            if (_this.enablePan === false)
                                return;
                            _this.handleTouchStartPan();
                            _this.state = STATE.TOUCH_PAN;
                            break;
                        default:
                            _this.state = STATE.NONE;
                    }
                    break;
                case 2:
                    switch (_this.touches.TWO) {
                        case THREE.TOUCH.DOLLY_PAN:
                            if (_this.enableZoom === false && _this.enablePan === false)
                                return;
                            _this.handleTouchStartDollyPan();
                            _this.state = STATE.TOUCH_DOLLY_PAN;
                            break;
                        case THREE.TOUCH.DOLLY_ROTATE:
                            if (_this.enableZoom === false && _this.enableRotate === false)
                                return;
                            _this.handleTouchStartDollyRotate();
                            _this.state = STATE.TOUCH_DOLLY_ROTATE;
                            break;
                        default:
                            _this.state = STATE.NONE;
                    }
                    break;
                default:
                    _this.state = STATE.NONE;
            }
            if (_this.state !== STATE.NONE) {
                _this.dispatchEvent(_startEvent);
            }
        };
        _this.onTouchMove = function (event) {
            _this.trackPointer(event);
            switch (_this.state) {
                case STATE.TOUCH_ROTATE:
                    if (_this.enableRotate === false)
                        return;
                    _this.handleTouchMoveRotate(event);
                    _this.update();
                    break;
                case STATE.TOUCH_PAN:
                    if (_this.enablePan === false)
                        return;
                    _this.handleTouchMovePan(event);
                    _this.update();
                    break;
                case STATE.TOUCH_DOLLY_PAN:
                    if (_this.enableZoom === false && _this.enablePan === false)
                        return;
                    _this.handleTouchMoveDollyPan(event);
                    _this.update();
                    break;
                case STATE.TOUCH_DOLLY_ROTATE:
                    if (_this.enableZoom === false && _this.enableRotate === false)
                        return;
                    _this.handleTouchMoveDollyRotate(event);
                    _this.update();
                    break;
                default:
                    _this.state = STATE.NONE;
            }
        };
        _this.onContextMenu = function (event) {
            if (_this.enabled === false)
                return;
            event.preventDefault();
        };
        _this.addPointer = function (event) {
            _this.pointers.push(event);
        };
        _this.removePointer = function (event) {
            delete _this.pointerPositions[event.pointerId];
            for (var i = 0; i < _this.pointers.length; i++) {
                if (_this.pointers[i].pointerId == event.pointerId) {
                    _this.pointers.splice(i, 1);
                    return;
                }
            }
        };
        _this.trackPointer = function (event) {
            var position = _this.pointerPositions[event.pointerId];
            if (position === undefined) {
                position = new THREE.Vector2();
                _this.pointerPositions[event.pointerId] = position;
            }
            position.set(event.pageX, event.pageY);
        };
        _this.getSecondPointerPosition = function (event) {
            var pointer = event.pointerId === _this.pointers[0].pointerId ?
                _this.pointers[1] : _this.pointers[0];
            return _this.pointerPositions[pointer.pointerId];
        }; //
        _this.object = object;
        _this.domElement = domElement;
        _this.target0 = _this.target.clone();
        _this.position0 = _this.object.position.clone();
        //this.zoom0 = this.object; // the target DOM element for key events
        _this._domElementKeyEvents = null; //
        _this.domElement.addEventListener('contextmenu', _this.onContextMenu);
        _this.domElement.addEventListener('pointerdown', _this.onPointerDown);
        _this.domElement.addEventListener('pointercancel', _this.onPointerCancel);
        _this.domElement.addEventListener('wheel', _this.onMouseWheel, {
            passive: false
        });
        // force an update at start
        _this.listenToKeyEvents(_this.domElement);
        return _this;
        //this.update();
    }
    OrbitControls.prototype.getPolarAngle = function () {
        return this.spherical.phi;
    };
    ;
    OrbitControls.prototype.getAzimuthalAngle = function () {
        return this.spherical.theta;
    };
    ;
    OrbitControls.prototype.getDistance = function () {
        return this.object.position.distanceTo(this.target);
    };
    ;
    OrbitControls.prototype.listenToKeyEvents = function (domElement) {
        domElement.addEventListener('keydown', this.onKeyDown);
        this._domElementKeyEvents = domElement;
    };
    ;
    /*
    saveState () {
      this.target0.copy(this.target);
      this.position0.copy(this.object.position);
      this.zoom0 = this.object.zoom;
    };
  
    reset () {
      this.target.copy(this.target0);
      this.object.position.copy(this.position0);
      this.object.zoom = this.zoom0;
      this.object.updateProjectionMatrix();
      this.dispatchEvent(_changeEvent);
      this.update();
      state = STATE.NONE;
    }; // this method is exposed, but perhaps it would be better if we can make it private...
    */
    OrbitControls.prototype.update = function () {
        this.panOffset.y = 0;
        this.target.y = this.focusHeight;
        var offset = new THREE.Vector3(); // so camera.up is the orbit axis
        var quat = new THREE.Quaternion().setFromUnitVectors(this.object.up, new THREE.Vector3(0, 1, 0));
        var quatInverse = quat.clone().invert();
        var lastPosition = new THREE.Vector3();
        var lastQuaternion = new THREE.Quaternion();
        var twoPI = 2 * Math.PI;
        var position = this.object.position;
        offset.copy(position).sub(this.target); // rotate offset to "y-axis-is-up" space
        offset.applyQuaternion(quat); // angle from z-axis around y-axis
        this.spherical.setFromVector3(offset);
        if (this.autoRotate && this.state === STATE.NONE) {
            this.rotateLeft(this.getAutoRotationAngle());
        }
        if (this.enableDamping) {
            this.spherical.theta += this.sphericalDelta.theta * this.dampingFactor;
            this.spherical.phi += this.sphericalDelta.phi * this.dampingFactor;
        }
        else {
            this.spherical.theta += this.sphericalDelta.theta;
            this.spherical.phi += this.sphericalDelta.phi;
        } // restrict theta to be between desired limits
        var min = this.minAzimuthAngle;
        var max = this.maxAzimuthAngle;
        if (isFinite(min) && isFinite(max)) {
            if (min < -Math.PI)
                min += twoPI;
            else if (min > Math.PI)
                min -= twoPI;
            if (max < -Math.PI)
                max += twoPI;
            else if (max > Math.PI)
                max -= twoPI;
            if (min <= max) {
                this.spherical.theta = Math.max(min, Math.min(max, this.spherical.theta));
            }
            else {
                this.spherical.theta = this.spherical.theta > (min + max) / 2 ? Math.max(min, this.spherical.theta) : Math.min(max, this.spherical.theta);
            }
        } // restrict phi to be between desired limits
        this.spherical.phi = Math.max(this.minPolarAngle, Math.min(this.maxPolarAngle, this.spherical.phi));
        this.spherical.makeSafe();
        this.spherical.radius *= this.scale; // restrict radius to be between desired limits
        this.spherical.radius = Math.max(this.minDistance, Math.min(this.maxDistance, this.spherical.radius)); // move target to panned location
        if (this.enableDamping === true) {
            this.target.addScaledVector(this.panOffset, this.dampingFactor);
        }
        else {
            this.target.add(this.panOffset);
        }
        offset.setFromSpherical(this.spherical); // rotate offset back to "camera-up-vector-is-up" space
        offset.applyQuaternion(quatInverse);
        position.copy(this.target).add(offset);
        this.object.lookAt(this.target);
        if (this.target_helper)
            this.target_helper.position.set(this.target.x, this.target.y, this.target.z);
        if (this.enableDamping === true) {
            this.sphericalDelta.theta *= 1 - this.dampingFactor;
            this.sphericalDelta.phi *= 1 - this.dampingFactor;
            this.panOffset.multiplyScalar(1 - this.dampingFactor);
        }
        else {
            this.sphericalDelta.set(0, 0, 0);
            this.panOffset.set(0, 0, 0);
        }
        this.scale = 1; // update condition is:
        // min(camera displacement, camera rotation in radians)^2 > EPS
        // using small-angle approximation cos(x/2) = 1 - x^2 / 8
        if (this.zoomChanged || lastPosition.distanceToSquared(this.object.position) > this.EPS ||
            8 * (1 - lastQuaternion.dot(this.object.quaternion)) > this.EPS) {
            this.dispatchEvent(_changeEvent);
            lastPosition.copy(this.object.position);
            lastQuaternion.copy(this.object.quaternion);
            this.zoomChanged = false;
            return true;
        }
        return false;
    };
    OrbitControls.prototype.dispose = function () {
        this.domElement.removeEventListener('contextmenu', this.onContextMenu);
        this.domElement.removeEventListener('pointerdown', this.onPointerDown);
        this.domElement.removeEventListener('pointercancel', this.onPointerCancel);
        this.domElement.removeEventListener('wheel', this.onMouseWheel);
        this.domElement.removeEventListener('pointermove', this.onPointerMove);
        this.domElement.removeEventListener('pointerup', this.onPointerUp);
        if (this._domElementKeyEvents !== null) {
            this._domElementKeyEvents.removeEventListener('keydown', this.onKeyDown);
        } //this.dispatchEvent( { type: 'dispose' } ); // should this be added here?
    };
    ;
    OrbitControls.prototype.getAutoRotationAngle = function () {
        return 2 * Math.PI / 60 / 60 * this.autoRotateSpeed;
    };
    OrbitControls.prototype.getZoomScale = function () {
        return Math.pow(0.95, this.zoomSpeed);
    };
    OrbitControls.prototype.rotateLeft = function (angle) {
        this.sphericalDelta.theta -= angle;
    };
    OrbitControls.prototype.rotateUp = function (angle) {
        this.sphericalDelta.phi -= angle;
    };
    OrbitControls.prototype.panLeft = function (distance, objectMatrix) {
        var v = new THREE.Vector3();
        v.setFromMatrixColumn(objectMatrix, 0); // get X column of objectMatrix
        v.multiplyScalar(-distance);
        this.panOffset.add(v);
    };
    OrbitControls.prototype.panUp = function (distance, objectMatrix) {
        var v = new THREE.Vector3();
        if (this.screenSpacePanning === true) {
            v.setFromMatrixColumn(objectMatrix, 1);
        }
        else {
            v.setFromMatrixColumn(objectMatrix, 0);
            v.crossVectors(this.object.up, v);
        }
        v.multiplyScalar(distance);
        this.panOffset.add(v);
    }; // deltaX and deltaY are in pixels; right and down are positive
    OrbitControls.prototype.pan = function (deltaX, deltaY) {
        var offset = new THREE.Vector3();
        var element = this.domElement;
        if (this.object instanceof THREE.PerspectiveCamera) {
            // perspective
            var position = this.object.position;
            offset.copy(position).sub(this.target);
            var targetDistance = offset.length(); // half of the fov is center to top of screen
            targetDistance *= Math.tan(this.object.fov / 2 * Math.PI / 180.0); // we use only clientHeight here so aspect ratio does not distort speed
            this.panLeft(2 * deltaX * targetDistance / element.clientHeight, this.object.matrix);
            this.panUp(2 * deltaY * targetDistance / element.clientHeight, this.object.matrix);
        }
        else if (this.object instanceof THREE.OrthographicCamera) {
            // orthographic
            this.panLeft(deltaX * (this.object.right - this.object.left) / this.object.zoom / element.clientWidth, this.object.matrix);
            this.panUp(deltaY * (this.object.top - this.object.bottom) / this.object.zoom / element.clientHeight, this.object.matrix);
        }
        else {
            // camera neither orthographic nor perspective
            console.warn('WARNING: OrbitControls.js encountered an unknown camera type - pan disabled.');
            this.enablePan = false;
        }
    };
    OrbitControls.prototype.dollyOut = function (dollyScale) {
        if (this.object instanceof THREE.PerspectiveCamera) {
            this.scale /= dollyScale;
        }
        else if (this.object instanceof THREE.OrthographicCamera) {
            this.object.zoom = Math.max(this.minZoom, Math.min(this.maxZoom, this.object.zoom * dollyScale));
            this.object.updateProjectionMatrix();
            this.zoomChanged = true;
        }
        else {
            console.warn('WARNING: OrbitControls.js encountered an unknown camera type - dolly/zoom disabled.');
            this.enableZoom = false;
        }
    };
    OrbitControls.prototype.dollyIn = function (dollyScale) {
        if (this.object instanceof three_1.PerspectiveCamera) {
            this.scale *= dollyScale;
        }
        else if (this.object instanceof three_1.OrthographicCamera) {
            this.object.zoom = Math.max(this.minZoom, Math.min(this.maxZoom, this.object.zoom / dollyScale));
            this.object.updateProjectionMatrix();
            this.zoomChanged = true;
        }
        else {
            console.warn('WARNING: OrbitControls.js encountered an unknown camera type - dolly/zoom disabled.');
            this.enableZoom = false;
        }
    };
    //
    // event callbacks - update the object state
    //
    OrbitControls.prototype.handleMouseDownRotate = function (event) {
        this.rotateStart.set(event.clientX, event.clientY);
    };
    OrbitControls.prototype.handleMouseDownDolly = function (event) {
        this.dollyStart.set(event.clientX, event.clientY);
    };
    OrbitControls.prototype.handleMouseDownPan = function (event) {
        this.panStart.set(event.clientX, event.clientY);
    };
    OrbitControls.prototype.handleMouseMoveRotate = function (event) {
        this.rotateEnd.set(event.clientX, event.clientY);
        this.rotateDelta.subVectors(this.rotateEnd, this.rotateStart).multiplyScalar(this.rotateSpeed);
        var element = this.domElement;
        this.rotateLeft(2 * Math.PI * this.rotateDelta.x / element.clientHeight); // yes, height
        this.rotateUp(2 * Math.PI * this.rotateDelta.y / element.clientHeight);
        this.rotateStart.copy(this.rotateEnd);
        this.update();
    };
    OrbitControls.prototype.handleMouseMoveDolly = function (event) {
        this.dollyEnd.set(event.clientX, event.clientY);
        this.dollyDelta.subVectors(this.dollyEnd, this.dollyStart);
        if (this.dollyDelta.y > 0) {
            this.dollyOut(this.getZoomScale());
        }
        else if (this.dollyDelta.y < 0) {
            this.dollyIn(this.getZoomScale());
        }
        this.dollyStart.copy(this.dollyEnd);
        this.update();
    };
    OrbitControls.prototype.handleMouseMovePan = function (event) {
        this.panEnd.set(event.clientX, event.clientY);
        this.panDelta.subVectors(this.panEnd, this.panStart).multiplyScalar(this.panSpeed);
        this.pan(this.panDelta.x, this.panDelta.y);
        this.panStart.copy(this.panEnd);
        this.update();
    };
    OrbitControls.prototype.handleMouseWheel = function (event) {
        if (event.shiftKey) {
            this.focusHeight += event.deltaY / -500;
        }
        else {
            if (event.deltaY < 0) {
                this.dollyIn(this.getZoomScale());
            }
            else if (event.deltaY > 0) {
                this.dollyOut(this.getZoomScale());
            }
        }
        this.update();
    };
    OrbitControls.prototype.handleKeyDown = function (event) {
        var needsUpdate = false;
        switch (event.code) {
            case this.keys.UP:
                this.pan(0, this.keyPanSpeed);
                needsUpdate = true;
                break;
            case this.keys.BOTTOM:
                this.pan(0, -this.keyPanSpeed);
                needsUpdate = true;
                break;
            case this.keys.LEFT:
                this.pan(this.keyPanSpeed, 0);
                needsUpdate = true;
                break;
            case this.keys.RIGHT:
                this.pan(-this.keyPanSpeed, 0);
                needsUpdate = true;
                break;
        }
        if (needsUpdate) {
            // prevent the browser from scrolling on cursor keys
            event.preventDefault();
            this.update();
        }
    };
    OrbitControls.prototype.handleTouchStartRotate = function () {
        if (this.pointers.length === 1) {
            this.rotateStart.set(this.pointers[0].pageX, this.pointers[0].pageY);
        }
        else {
            var x = 0.5 * (this.pointers[0].pageX + this.pointers[1].pageX);
            var y = 0.5 * (this.pointers[0].pageY + this.pointers[1].pageY);
            this.rotateStart.set(x, y);
        }
    };
    OrbitControls.prototype.handleTouchStartPan = function () {
        if (this.pointers.length === 1) {
            this.panStart.set(this.pointers[0].pageX, this.pointers[0].pageY);
        }
        else {
            var x = 0.5 * (this.pointers[0].pageX + this.pointers[1].pageX);
            var y = 0.5 * (this.pointers[0].pageY + this.pointers[1].pageY);
            this.panStart.set(x, y);
        }
    };
    OrbitControls.prototype.handleTouchStartDolly = function () {
        var dx = this.pointers[0].pageX - this.pointers[1].pageX;
        var dy = this.pointers[0].pageY - this.pointers[1].pageY;
        var distance = Math.sqrt(dx * dx + dy * dy);
        this.dollyStart.set(0, distance);
    };
    OrbitControls.prototype.handleTouchStartDollyPan = function () {
        if (this.enableZoom)
            this.handleTouchStartDolly();
        if (this.enablePan)
            this.handleTouchStartPan();
    };
    OrbitControls.prototype.handleTouchStartDollyRotate = function () {
        if (this.enableZoom)
            this.handleTouchStartDolly();
        if (this.enableRotate)
            this.handleTouchStartRotate();
    };
    OrbitControls.prototype.handleTouchMoveRotate = function (event) {
        if (this.pointers.length == 1) {
            this.rotateEnd.set(event.pageX, event.pageY);
        }
        else {
            var position = this.getSecondPointerPosition(event);
            var x = 0.5 * (event.pageX + position.x);
            var y = 0.5 * (event.pageY + position.y);
            this.rotateEnd.set(x, y);
        }
        this.rotateDelta.subVectors(this.rotateEnd, this.rotateStart).multiplyScalar(this.rotateSpeed);
        var element = this.domElement;
        this.rotateLeft(2 * Math.PI * this.rotateDelta.x / element.clientHeight); // yes, height
        this.rotateUp(2 * Math.PI * this.rotateDelta.y / element.clientHeight);
        this.rotateStart.copy(this.rotateEnd);
    };
    OrbitControls.prototype.handleTouchMovePan = function (event) {
        if (this.pointers.length === 1) {
            this.panEnd.set(event.pageX, event.pageY);
        }
        else {
            var position = this.getSecondPointerPosition(event);
            var x = 0.5 * (event.pageX + position.x);
            var y = 0.5 * (event.pageY + position.y);
            this.panEnd.set(x, y);
        }
        this.panDelta.subVectors(this.panEnd, this.panStart).multiplyScalar(this.panSpeed);
        this.pan(this.panDelta.x, this.panDelta.y);
        this.panStart.copy(this.panEnd);
    };
    OrbitControls.prototype.handleTouchMoveDolly = function (event) {
        var position = this.getSecondPointerPosition(event);
        var dx = event.pageX - position.x;
        var dy = event.pageY - position.y;
        var distance = Math.sqrt(dx * dx + dy * dy);
        this.dollyEnd.set(0, distance);
        this.dollyDelta.set(0, Math.pow(this.dollyEnd.y / this.dollyStart.y, this.zoomSpeed));
        this.dollyOut(this.dollyDelta.y);
        this.dollyStart.copy(this.dollyEnd);
    };
    OrbitControls.prototype.handleTouchMoveDollyPan = function (event) {
        if (this.enableZoom)
            this.handleTouchMoveDolly(event);
        if (this.enablePan)
            this.handleTouchMovePan(event);
    };
    OrbitControls.prototype.handleTouchMoveDollyRotate = function (event) {
        if (this.enableZoom)
            this.handleTouchMoveDolly(event);
        if (this.enableRotate)
            this.handleTouchMoveRotate(event);
    }; //
    return OrbitControls;
}(THREE.EventDispatcher));
exports.OrbitControls = OrbitControls;
// This set of controls performs orbiting, dollying (zooming), and panning.
// Unlike TrackballControls, it maintains the "up" direction object.up (+Y by default).
// This is very similar to OrbitControls, another set of touch behavior
//
//    Orbit - right mouse, or left mouse + ctrl/meta/shiftKey / touch: two-finger rotate
//    Zoom - middle mouse, or mousewheel / touch: two-finger spread or squish
//    Pan - left mouse, or arrow keys / touch: one-finger move
var MapControls = /** @class */ (function (_super) {
    __extends(MapControls, _super);
    function MapControls(object, domElement) {
        var _this = _super.call(this, object, domElement) || this;
        _this.screenSpacePanning = false; // pan orthogonal to world-space direction camera.up
        _this.mouseButtons.LEFT = THREE.MOUSE.PAN;
        _this.mouseButtons.RIGHT = THREE.MOUSE.ROTATE;
        _this.touches.ONE = THREE.TOUCH.PAN;
        _this.touches.TWO = THREE.TOUCH.DOLLY_ROTATE;
        return _this;
    }
    return MapControls;
}(OrbitControls));
exports.MapControls = MapControls;
var STATE;
(function (STATE) {
    STATE[STATE["NONE"] = -1] = "NONE";
    STATE[STATE["ROTATE"] = 0] = "ROTATE";
    STATE[STATE["DOLLY"] = 1] = "DOLLY";
    STATE[STATE["PAN"] = 2] = "PAN";
    STATE[STATE["TOUCH_ROTATE"] = 3] = "TOUCH_ROTATE";
    STATE[STATE["TOUCH_PAN"] = 4] = "TOUCH_PAN";
    STATE[STATE["TOUCH_DOLLY_PAN"] = 5] = "TOUCH_DOLLY_PAN";
    STATE[STATE["TOUCH_DOLLY_ROTATE"] = 6] = "TOUCH_DOLLY_ROTATE";
})(STATE = exports.STATE || (exports.STATE = {}));
;
//# sourceMappingURL=OrbitController.js.map