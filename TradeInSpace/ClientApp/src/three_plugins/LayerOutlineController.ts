import {
  AddEquation,
  AdditiveBlending,
  Color,
  CustomBlending,
  DoubleSide,
  LinearFilter,
  Material,
  Matrix4,
  MeshBasicMaterial,
  MeshDepthMaterial,
  MultiplyBlending,
  NoBlending,
  NormalBlending,
  OneMinusSrcAlphaFactor,
  OrthographicCamera,
  PerspectiveCamera,
  RGBADepthPacking,
  RGBAFormat,
  Shader,
  ShaderMaterial,
  SrcAlphaFactor,
  Texture,
  UniformsUtils,
  Vector2,
  Vector3,
  WebGLRenderTarget
} from 'three';
import { Pass, FullScreenQuad } from 'three/examples/jsm/postprocessing/Pass.js'
import { CopyShader } from 'three/examples/jsm/shaders/CopyShader.js';

export class LayerOutlinePass extends Pass {

  renderScene: THREE.Scene;
  renderCamera: THREE.Camera;

  objectLayer: number;

  edgeColor: Color;
  edgeThickness: number;
  downSampleRatio: number;
  pulsePeriod: number;

  resolution: Vector2;

  maskBufferMaterial: MeshBasicMaterial;
  renderTargetMaskBuffer: WebGLRenderTarget;
  depthMaterial: MeshDepthMaterial;
  prepareMaskMaterial: Material;
  edgeDetectionMaterial: ShaderMaterial;
  overlayMaterial: ShaderMaterial;
  materialCopy: ShaderMaterial;

  fsQuad: FullScreenQuad;

  renderTargetMaskDownSampleBuffer: WebGLRenderTarget;
  renderTargetDepthBuffer: WebGLRenderTarget;
  renderTargetEdgeBuffer: WebGLRenderTarget;

  copyUniforms: any;
  _oldClearColor: Color;
  oldClearAlpha: number;
  tempPulseColor: Color;
  textureMatrix: Matrix4;

  constructor(resolution: THREE.Vector2, scene: THREE.Scene, camera: THREE.Camera, objectLayer: number = 2) {

    super();

    this.renderScene = scene;
    this.renderCamera = camera;
    this.objectLayer = objectLayer;
    this.edgeColor = new Color(1, 1, 1);
    this.edgeThickness = 1.0;
    this.downSampleRatio = 2;
    this.pulsePeriod = 0;

    this.resolution = (resolution !== undefined) ? new Vector2(resolution.x, resolution.y) : new Vector2(256, 256);

    const pars = { minFilter: LinearFilter, magFilter: LinearFilter, format: RGBAFormat };

    const resx = Math.round(this.resolution.x / this.downSampleRatio);
    const resy = Math.round(this.resolution.y / this.downSampleRatio);

    this.maskBufferMaterial = new MeshBasicMaterial({ color: 0xffffff });
    this.maskBufferMaterial.side = DoubleSide;
    this.renderTargetMaskBuffer = new WebGLRenderTarget(this.resolution.x, this.resolution.y, pars);
    this.renderTargetMaskBuffer.texture.name = 'OutlinePass.mask';
    this.renderTargetMaskBuffer.texture.generateMipmaps = false;

    this.depthMaterial = new MeshDepthMaterial();
    this.depthMaterial.side = DoubleSide;
    this.depthMaterial.depthPacking = RGBADepthPacking;
    this.depthMaterial.blending = NoBlending;

    this.prepareMaskMaterial = new MeshBasicMaterial({ color: new Color(0x000000) });
    this.prepareMaskMaterial.side = DoubleSide;

    this.renderTargetDepthBuffer = new WebGLRenderTarget(this.resolution.x, this.resolution.y, pars);
    this.renderTargetDepthBuffer.texture.name = 'OutlinePass.depth';
    this.renderTargetDepthBuffer.texture.generateMipmaps = false;

    this.renderTargetMaskDownSampleBuffer = new WebGLRenderTarget(resx, resy, pars);
    this.renderTargetMaskDownSampleBuffer.texture.name = 'OutlinePass.depthDownSample';
    this.renderTargetMaskDownSampleBuffer.texture.generateMipmaps = false;

    this.edgeDetectionMaterial = this.getEdgeDetectionMaterial();
    this.renderTargetEdgeBuffer = new WebGLRenderTarget(resx, resy, pars);
    this.renderTargetEdgeBuffer.texture.name = 'OutlinePass.edge1';
    this.renderTargetEdgeBuffer.texture.generateMipmaps = false;

    // Overlay material
    this.overlayMaterial = this.getOverlayMaterial();

    // copy material
    if (CopyShader === undefined) console.error('THREE.OutlinePass relies on CopyShader');

    const copyShader = CopyShader;

    this.copyUniforms = UniformsUtils.clone(copyShader.uniforms);
    this.copyUniforms['opacity'].value = 1.0;

    this.materialCopy = new ShaderMaterial({
      uniforms: this.copyUniforms,
      vertexShader: copyShader.vertexShader,
      fragmentShader: copyShader.fragmentShader,
      blending: NoBlending,
      depthTest: false,
      depthWrite: false,
      transparent: true
    });

    this.enabled = true;
    this.needsSwap = false;

    this._oldClearColor = new Color();
    this.oldClearAlpha = 1;

    this.fsQuad = new FullScreenQuad(null);

    this.tempPulseColor = new Color();
    this.textureMatrix = new Matrix4();

    function replaceDepthToViewZ(string, camera) {

      var type = camera.isPerspectiveCamera ? 'perspective' : 'orthographic';

      return string.replace(/DEPTH_TO_VIEW_Z/g, type + 'DepthToViewZ');

    }

  }

  dispose() {

    this.renderTargetMaskBuffer.dispose();
    this.renderTargetDepthBuffer.dispose();
    this.renderTargetMaskDownSampleBuffer.dispose();
    this.renderTargetEdgeBuffer.dispose();

  }

  setSize(width, height) {

    this.renderTargetMaskBuffer.setSize(width, height);
    this.renderTargetDepthBuffer.setSize(width, height);

    let resx = Math.round(width / this.downSampleRatio);
    let resy = Math.round(height / this.downSampleRatio);
    this.renderTargetMaskDownSampleBuffer.setSize(resx, resy);
    this.renderTargetEdgeBuffer.setSize(resx, resy);

  }


  updateTextureMatrix() {

    this.textureMatrix.set(0.5, 0.0, 0.0, 0.5,
      0.0, 0.5, 0.0, 0.5,
      0.0, 0.0, 0.5, 0.5,
      0.0, 0.0, 0.0, 1.0);
    this.textureMatrix.multiply(this.renderCamera.projectionMatrix);
    this.textureMatrix.multiply(this.renderCamera.matrixWorldInverse);

  }



  render(renderer, writeBuffer, readBuffer, deltaTime, maskActive) {
    var someVisible = false;

    this.renderScene.traverseVisible(obj => {
      if (obj.layers.isEnabled(this.objectLayer)) {
        someVisible = true;
      }
    });

    if (someVisible) {

      renderer.getClearColor(this._oldClearColor);
      this.oldClearAlpha = renderer.getClearAlpha();
      const oldAutoClear = renderer.autoClear;

      renderer.autoClear = false;

      if (maskActive) renderer.state.buffers.stencil.setTest(false);

      renderer.setClearColor(0xffffff, 1);

      // Make only selected objects visible
      var camera_mask_old = this.renderCamera.layers.mask;
      this.renderCamera.layers.disableAll();
      this.renderCamera.layers.enable(this.objectLayer);

      const currentBackground = this.renderScene.background;
      this.renderScene.background = null;

      // 1. Render mask for background objects.
      this.renderScene.overrideMaterial = this.prepareMaskMaterial;
      renderer.setRenderTarget(this.renderTargetMaskBuffer);
      renderer.clear();
      renderer.render(this.renderScene, this.renderCamera);
      this.renderScene.overrideMaterial = null;

      this.renderScene.background = currentBackground;

      // 2. Downsample to Half resolution
      this.fsQuad.material = this.materialCopy;
      this.copyUniforms['tDiffuse'].value = this.renderTargetMaskBuffer.texture;
      renderer.setRenderTarget(this.renderTargetMaskDownSampleBuffer);
      renderer.clear();
      this.fsQuad.render(renderer);

      // .. Update pulse colors
      this.tempPulseColor.copy(this.edgeColor);

      if (this.pulsePeriod > 0) {

        const scalar = (1 + 0.25) / 2 + Math.cos(performance.now() * 0.01 / this.pulsePeriod) * (1.0 - 0.25) / 2;
        this.tempPulseColor.multiplyScalar(scalar);

      }

      // 3. Apply Edge Detection Pass
      this.fsQuad.material = this.edgeDetectionMaterial;
      this.edgeDetectionMaterial.uniforms['maskTexture'].value = this.renderTargetMaskDownSampleBuffer.texture;
      this.edgeDetectionMaterial.uniforms['texSize'].value.set(this.renderTargetMaskDownSampleBuffer.width, this.renderTargetMaskDownSampleBuffer.height);
      this.edgeDetectionMaterial.uniforms['edgeColor'].value = this.tempPulseColor;
      this.edgeDetectionMaterial.uniforms['edgeThickness'].value = this.edgeThickness;
      renderer.setRenderTarget(this.renderTargetEdgeBuffer);
      renderer.clear();
      this.fsQuad.render(renderer);

      // Blend it additively over the input texture
      this.fsQuad.material = this.overlayMaterial;
      this.overlayMaterial.uniforms['maskTexture'].value = this.renderTargetMaskBuffer.texture;
      this.overlayMaterial.uniforms['edgeTexture'].value = this.renderTargetEdgeBuffer.texture;


      if (maskActive) renderer.state.buffers.stencil.setTest(true);

      renderer.setRenderTarget(writeBuffer);
      this.fsQuad.render(renderer);

      renderer.setClearColor(this._oldClearColor, this.oldClearAlpha);
      renderer.autoClear = oldAutoClear;

      this.renderCamera.layers.mask = camera_mask_old;
    }
  }

  getPrepareMaskMaterial() {

    return new ShaderMaterial({

      uniforms: {
        'depthTexture': { value: null },
        'cameraNearFar': { value: new Vector2(0.5, 0.5) },
        'textureMatrix': { value: null }
      },

      vertexShader:
        `#include <morphtarget_pars_vertex>
				#include <skinning_pars_vertex>

				varying vec4 projTexCoord;
				varying vec4 vPosition;
				uniform mat4 textureMatrix;

				void main() {

					#include <skinbase_vertex>
					#include <begin_vertex>
					#include <morphtarget_vertex>
					#include <skinning_vertex>
					#include <project_vertex>

					vPosition = mvPosition;
					vec4 worldPosition = modelMatrix * vec4( transformed, 1.0 );
					projTexCoord = textureMatrix * worldPosition;

				}`,

      fragmentShader:
        `#include <packing>
				varying vec4 vPosition;
				varying vec4 projTexCoord;
				uniform sampler2D depthTexture;
				uniform vec2 cameraNearFar;

				void main() {

					float depth = unpackRGBAToDepth(texture2DProj( depthTexture, projTexCoord ));
					float viewZ = - DEPTH_TO_VIEW_Z( depth, cameraNearFar.x, cameraNearFar.y );
					float depthTest = (-vPosition.z > viewZ) ? 1.0 : 0.0;
					gl_FragColor = vec4(0.0, depthTest, 1.0, 1.0);

				}`

    });

  }

  getEdgeDetectionMaterial() {

    return new ShaderMaterial({

      uniforms: {
        'maskTexture': { value: null },
        'texSize': { value: new Vector2(0.5, 0.5) },
        'edgeColor': { value: new Vector3(1.0, 1.0, 1.0) },
        'edgeThickness': { value: 5 }
      },

      vertexShader:
        `varying vec2 vUv;

				void main() {
					vUv = uv;
					gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );
				}`,

      fragmentShader:
        `varying vec2 vUv;

				uniform sampler2D maskTexture;
				uniform vec2 texSize;
				uniform vec3 edgeColor;
                uniform float edgeThickness;

				void main() {
					vec2 invSize = 1.0 / texSize;
					vec4 uvOffset = vec4(edgeThickness, -edgeThickness, 0.0, -edgeThickness) * vec4(invSize, invSize);
					vec4 c1 = texture2D( maskTexture, vUv + uvOffset.xy);
					vec4 c2 = texture2D( maskTexture, vUv - uvOffset.xy);
					vec4 c3 = texture2D( maskTexture, vUv + uvOffset.yw);
					vec4 c4 = texture2D( maskTexture, vUv - uvOffset.yw);
                    vec4 c5 = texture2D( maskTexture, vUv + uvOffset.xz);
                    vec4 c6 = texture2D( maskTexture, vUv - uvOffset.xz);
                    vec4 c7 = texture2D( maskTexture, vUv + uvOffset.zx);
                    vec4 c8 = texture2D( maskTexture, vUv - uvOffset.zx);
					float diff1 = (c1.r - c2.r)*0.5;
					float diff2 = (c3.r - c4.r)*0.5;
					float diff3 = (c5.r - c6.r)*0.5;
					float diff4 = (c7.r - c8.r)*0.5;
					float d = length( vec2(diff1, diff2) ) + length(vec2(diff3, diff4));
                    if(d > 1.0) d = 1.0;
					gl_FragColor = vec4(edgeColor, 1.0) * vec4(d);
				}`
    });

  }

  getOverlayMaterial() {

    return new ShaderMaterial({

      uniforms: {
        'maskTexture': { value: null },
        'edgeTexture': { value: null },
      },

      vertexShader:
        `varying vec2 vUv;

				void main() {
					vUv = uv;
					gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );
				}`,

      fragmentShader:
        `varying vec2 vUv;

				uniform sampler2D maskTexture;
				uniform sampler2D edgeTexture;

				void main() {
					vec4 edgeValue = texture2D(edgeTexture, vUv);
					vec4 maskColor = texture2D(maskTexture, vUv);
					float visibilityFactor = maskColor.r > 0.0 ? 1.0 : 0.0;
                    //Jenky but it works :)
					vec4 finalColor = 3.0 * visibilityFactor * edgeValue;
					gl_FragColor = finalColor;
				}`,
      blending: CustomBlending,
      blendEquation: AddEquation,
      blendSrc: SrcAlphaFactor,
      blendDst: OneMinusSrcAlphaFactor,
      depthTest: false,
      depthWrite: false,
      transparent: true
    });

  }

}
