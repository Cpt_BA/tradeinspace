import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer, SafeHtml, SafeStyle, SafeScript, SafeUrl, SafeResourceUrl } from '@angular/platform-browser';
import { isNumeric } from 'rxjs/internal-compatibility';


export function CleanHTML(HTML: string): string {
  const entityMap = {
    "&": "&amp;",
    "<": "&lt;",
    ">": "&gt;",
    '"': '&quot;',
    "'": '&#39;',
    "/": '&#x2F;'
  };
  return HTML.replace(/[&<>"'\/]/g, function (s) {
    return entityMap[s];
  })
}

@Pipe({
  name: 'safe'
})
export class SafePipe implements PipeTransform {

  constructor(protected sanitizer: DomSanitizer) {}
 
 public transform(value: any, type: string): SafeHtml | SafeStyle | SafeScript | SafeUrl | SafeResourceUrl {
    switch (type) {
			case 'html': return this.sanitizer.bypassSecurityTrustHtml(value);
			case 'style': return this.sanitizer.bypassSecurityTrustStyle(value);
			case 'script': return this.sanitizer.bypassSecurityTrustScript(value);
			case 'url': return this.sanitizer.bypassSecurityTrustUrl(value);
			case 'resourceUrl': return this.sanitizer.bypassSecurityTrustResourceUrl(value);
			default: throw new Error(`Invalid safe type specified: ${type}`);
		}
  }
}

@Pipe({
  name: 'newlines'
})
export class NewLinePipe implements PipeTransform {

  constructor(protected sanitizer: DomSanitizer) { }

  public transform(value: string): SafeHtml {
    return CleanHTML(value).replace(/\r/gi, "").replace(/\\r/gi, "").replace(/\\n/gi, "\n").split("\n").join("<br/>");
  }

}

@Pipe({
  name: 'value'
})
export class ValuePipe implements PipeTransform {

  constructor(protected sanitizer: DomSanitizer) { }

  public transform(value: string | number, invert = false): SafeHtml {
    const numVal: number = isNumeric(value) ? (value as number) : parseFloat(value as string);
    let valueClass = "";
    if (numVal < 0 && !invert) valueClass = "alert-danger";
    if (numVal > 0 && !invert) valueClass = "alert-success";

    const outHTML = "<div class='alert alert-small " + valueClass + "' style='font-weight: bold'>" + numVal + "<span>";
    return this.sanitizer.bypassSecurityTrustHtml(outHTML);
  }

}

