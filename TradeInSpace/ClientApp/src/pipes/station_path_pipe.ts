import { CommonModule } from "@angular/common";
import { Pipe, PipeTransform } from "@angular/core";
import * as moment from 'moment';

@Pipe({
  name: "stationpath",
  pure: true
})
export class StationPathPipe implements PipeTransform {
  static transform(value: string[], asHtml: boolean = false): string {
    if (asHtml) {
      return value.map((v, i) => {
        var indent: string = "";
        if (i > 0) {
          indent = `<div style="width: ${(i -1) * 16}px" class="indent-space"></div><i class="pi pi-reply indent-arrow"></i>`;
        }
        return `${indent}<span style='float: left'>${v}</span>`;
      }).join('<br/>');
    } else {
      return value.map((v, i) => {
        var indent: string = "".padStart(i, '\t');
        return `${indent}${v}`;
      }).join('\n');
    }
  }

  transform(value: string[], asHtml: boolean = false): string {
    return StationPathPipe.transform(value, asHtml);
  }
}

export function stationpath(value: string[], asHtml: boolean = false): string {
  return StationPathPipe.transform(value, asHtml);
}
