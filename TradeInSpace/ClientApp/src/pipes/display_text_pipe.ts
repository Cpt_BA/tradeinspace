import { Pipe, PipeTransform } from "@angular/core";
import { LocationType, ServiceFlags, PadSize, MissionGiverEntry } from '../data/generated_models';
import { KeyModel } from '../data/models';
import { ClientData } from '../data/client_data';
import { Client } from '_debugger';
import { formatCurrency } from '@angular/common';
import { FormatUECPipe, fmtUEC } from './scaleunit_pipe';

@Pipe({
  name: "locationType",
  pure: true
})
export class LocationTypePipe implements PipeTransform {
  static transform(locationType: LocationType): string {
    switch (locationType) {
      case LocationType.AsteroidField:
        return "Asteroid Field";
      case LocationType.LandingZone:
        return "Landing Zone";
      case LocationType.Manmade:
        return "Manmade/Station";
      case LocationType.Moon:
        return "Moon";
      case LocationType.Outpost:
        return "Outpost";
      case LocationType.Planet:
        return "Planet";
      case LocationType.Star:
        return "Star";
      case LocationType.Store:
        return "Store";
      case LocationType.Other:
        return "Other";
    }

    return "Unknown";
  }

  transform(locationType: LocationType): string {
    return LocationTypePipe.transform(locationType);
  }
}

export function locationTypeName(locationType: LocationType): string {
  return LocationTypePipe.transform(locationType);
}


@Pipe({
  name: "serviceflag",
  pure: true
})
export class ServiceFlagPipe implements PipeTransform {
  static transform(flags: ServiceFlags, multiple = false): string {
    if (multiple) {
      const allServices: string[] = [];
      for (let i = 0; i < 32; i++) {
        const testSvc: ServiceFlags = (1 << i) as ServiceFlags;
        if ((flags & testSvc) !== ServiceFlags.None) {
          allServices.push(this.transform(testSvc, false));
        }
      }
      return allServices.join(", ");
    } else {
      return ClientData.GetServiceFlaNames(flags);
    }
  }

  transform(flags: ServiceFlags, multiple = false): string {
    return ServiceFlagPipe.transform(flags, multiple);
  }
}

export function serviceFlagName(flags: ServiceFlags, multiple = false): string {
  return ServiceFlagPipe.transform(flags, multiple);
}


@Pipe({
  name: "padsize",
  pure: true
})
export class PadSizePipe implements PipeTransform {
  static transform(size?: PadSize, short: boolean = true): string {
    if (size) {
      switch (size) {
        case PadSize.Tiny:
          return short ? "T" : "Tiny";
        case PadSize.XSmall:
          return short ? "XS" : "X-Small";
        case PadSize.Small:
          return short ? "S" : "Small";
        case PadSize.Medium:
          return short ? "M" : "Medium";
        case PadSize.Large:
          return short ? "L" : "Large";
        case PadSize.XLarge:
          return short ? "XL" : "X-Large";
      }
    } else {
      return "N/A";
    }
  }

  transform(size?: PadSize, short: boolean = true): string {
    return PadSizePipe.transform(size, short);
  }
}

export function padSizeName(size?: PadSize, short:boolean = true): string {
  return PadSizePipe.transform(size, short);
}

@Pipe({
  name: "loadoutslottype",
  pure: true
})
export class LoadoutSlotTypePipe implements PipeTransform {
  static transform(type: string): string {
    switch (type) {
      //Full list of all seen port types.
      case "Cooler":
      case "Avionics":
      case "Radar":
      case "EMP":
      case "Shield":
      case "Interior":
      case "Container":
      case "Ping":
      case "Room":
      case "Scanner":
      case "Cargo":
      case "Armor":
      case "Light":
      case "Flair_Surface":
      case "Turret":
        return type;

      case "MainThruster":
      case "ManneuverThruster":
        return "Thruster"

      case "GravityGenerator":
        return "Grav. Gen."
      case "ShieldController":
        return "Shield Cont."
      case "FuelTank":
        return "Fuel Tank"
      case "TurretBase":
        return "Turret Base"
      case "WeaponController":
        return "Weapon Cont."
      case "LifeSupport":
        return "Life Support"
      case "WeaponGun":
        return "Gun"
      case "SelfDestruct":
        return "Self Destruct"
      case "CoolerController":
        return "Cooler Cont."
      case "WeaponDefensive":
        return "Countermeasure"
      case "QuantumFuelTank":
        return "QFuel Tank"
      case "FlightController":
        return "Flight Cont."
      case "QuantumDrive":
        return "QDrive"
      case "MiningController":
        return "Mining Cont."
      case "MultiLight":
        return "Multi Light"
      case "MissileLauncher":
        return "Missile Launcher"
      case "MiningArm":
        return "Mining Arm"
      case "FuelIntake":
        return "Fuel Intake"
      case "TargetSelector":
        return "Tgt. Selector";
      case "QuantumInterdictionGenerator":
        return "Interdictor";
      case ClientData.EquipmentTypes.Power:
        return "Power Plant";
      case ClientData.EquipmentTypes.MiningLaser:
        return "Mining Laser";

      case ClientData.EquipmentTypes.Vehicle:
        return "Vehicle";

      case ClientData.EquipmentTypes.FPS_Undersuit:
        return "Undersuit";
      case ClientData.EquipmentTypes.FPS_Helmet:
        return "Helmet";
      case ClientData.EquipmentTypes.FPS_Torso:
        return "Torso";
      case ClientData.EquipmentTypes.FPS_Arms:
        return "Arms";
      case ClientData.EquipmentTypes.FPS_Legs:
        return "Legs";


      case ClientData.EquipmentTypes.Clothing_Hat:
        return "Hat";
      case ClientData.EquipmentTypes.Clothing_Torso0:
        return "Torso 1";
      case ClientData.EquipmentTypes.Clothing_Torso1:
        return "Torso 2";
      case ClientData.EquipmentTypes.Clothing_Torso2:
        return "Toso 3";
      case ClientData.EquipmentTypes.Clothing_Legs:
        return "Legs";
      case ClientData.EquipmentTypes.Clothing_Hands:
        return "Hands";
      case ClientData.EquipmentTypes.Clothing_Feet:
        return "Feet";
    }
    return type;
  }

  transform(type: string): string {
    return LoadoutSlotTypePipe.transform(type);
  }
}

export function loadoutSlotTypeName(type: string): string {
  return LoadoutSlotTypePipe.transform(type);
}

@Pipe({
  name: "displayname",
  pure: true
})
export class DisplayNamePipe implements PipeTransform {
  static transform(dbEnt: any): string {
    if (dbEnt.Name == "<= PLACEHOLDER =>") {
      return dbEnt.Key.split(/[\s_]/)
        .map(c => c.charAt(0).toUpperCase() + c.slice(1))
        .join(" ");
    }

    return dbEnt.Name;
  }

  transform(dbEnt: any): string {
    return DisplayNamePipe.transform(dbEnt);
  }
}

export function displayName(dbEnt: any): string {
  return DisplayNamePipe.transform(dbEnt);
}


@Pipe({
  name: "missionreward",
  pure: true
})
export class MissionRewardPipe implements PipeTransform {
  static transform(mission: MissionGiverEntry, truncate = true): string {
    const bonuses = mission.PaysBonus ? " +Bonuses" : "";
    if (mission.MaxUEC && mission.MaxUEC !== mission.MinUEC) {
      return mission.MinUEC + " - " + fmtUEC(mission.MaxUEC, !truncate, !truncate) + bonuses;
    } else {
      return fmtUEC(mission.MinUEC, !truncate, !truncate) + bonuses;
    }
  }

  transform(mission: MissionGiverEntry, truncate = true): string {
    return MissionRewardPipe.transform(mission, truncate);
  }
}

export function missionRewardText(mission: MissionGiverEntry, truncate = true): string {
  return MissionRewardPipe.transform(mission, truncate);
}

@Pipe({
  name: "missiontitle",
  pure: true
})
export class MissionTitlePipe implements PipeTransform {
  static transform(mission: MissionGiverEntry, truncate = true): string {
    if (mission.AvailableLocation) {
      return mission.Title + " - [" + mission.AvailableLocation.Name + "]";
    } else {
      return mission.Title;
    }
  }

  transform(mission: MissionGiverEntry, truncate = true): string {
    return MissionTitlePipe.transform(mission, truncate);
  }
}

export function missionTitleText(mission: MissionGiverEntry, truncate = true): string {
  return MissionTitlePipe.transform(mission, truncate);
}
