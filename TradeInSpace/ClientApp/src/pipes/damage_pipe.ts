import { CommonModule } from "@angular/common";
import { Pipe, PipeTransform } from "@angular/core";
import * as moment from 'moment';
import { CollapseDamage, DamageInfo, CollapseResistance } from '../data/client_models';
import { fmtGeneric, fmtPercent } from './scaleunit_pipe';

@Pipe({
  name: "fmtDamage",
  pure: true
})
export class DamagePipe implements PipeTransform {
  static transform(dps: number, collapsed = true): string {
    //TODO: Fancy html for hover details and all that jazz
    return fmtGeneric(dps, !collapsed, !collapsed);
  }

  transform(value: DamageInfo, collapsed = true): string {
    return DamagePipe.transform(CollapseDamage(value), collapsed);
  }
}

export function fmtDPS(value: DamageInfo, collapsed = true): string {
  return DamagePipe.transform(CollapseDamage(value), collapsed);
}

export function fmtFlatDPS(dps: number, collapsed = true): string {
  return DamagePipe.transform(dps, collapsed);
}


@Pipe({
  name: "fmtResistance",
  pure: true
})
export class ResistancePipe implements PipeTransform {
  static transform(value: DamageInfo, inlineTable = false, label = false): string {
    if (!value) return "";
    //TODO: Fancy html for hover details and all that jazz
    if (inlineTable) {
      return [
        fmtPercent(value.Physical - 1, true),
        (label ? " Phy / " : " / "),

        fmtPercent(value.Energy - 1, true),
        (label ? " Ener / " : " / "),

        fmtPercent(value.Distortion - 1, true),
        (label ? " Dist" : ""),

        "<br/>",

        fmtPercent(value.Thermal - 1, true),
        (label ? " Ther / " : " / "),

        fmtPercent(value.Biochemical - 1, true),
        (label ? " Bio / " : " / "),

        fmtPercent(value.Stun - 1, true),
        (label ? " Stun" : ""),
      ].join("")
    } else {
      return fmtPercent(CollapseResistance(value) - 1, true);
    }
  }

  transform(value: DamageInfo, inlineTable = false, label = false): string {
    return ResistancePipe.transform(value, inlineTable, label);
  }
}

export function fmtResistance(value: DamageInfo, inlineTable = false, label = false): string {
  return ResistancePipe.transform(value, inlineTable, label);
}
