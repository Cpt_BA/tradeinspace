import { CommonModule } from "@angular/common";
import { Pipe, PipeTransform } from "@angular/core";
import * as moment from 'moment';

@Pipe({
  name: "timerange",
  pure: true
})
export class TimeRangePipe implements PipeTransform {
  static transform(value: any, args?: any): string {
    var dur: moment.Duration = moment.duration(value, "seconds");
    return `${dur.hours().toString().padStart(1, '0')}:${dur.minutes().toString().padStart(2, '0')}:${dur.seconds().toString().padStart(2, '0')}s`;
  }

  transform(value: any, args?: any): string {
    return TimeRangePipe.transform(value, args);
  }
}

export function timerange(value: any, args?: any): string {
  return TimeRangePipe.transform(value, args);
}
