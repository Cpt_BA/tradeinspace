import { CommonModule } from "@angular/common";
import { Pipe, PipeTransform } from "@angular/core";
import * as moment from 'moment';

export function fmtNumber(value: number, noDecimals: boolean = false, unit: string = "", symbol: string = "", onlyFormat: boolean = false): string {
  if (!value && value !== 0) {
    return "0";
  }

  const originalSign = Math.sign(value);
  if (onlyFormat) {

    const numberFormatted = Number((value).toFixed(noDecimals ? 0 : 2)).toLocaleString()

    return `${symbol}${numberFormatted}${unit}`;
  } else {
    value = Math.abs(value);
    const powerGroup = Math.max(Math.floor(Math.log10(value) / 3), 0);
    const groupValue = value / Math.pow(10, powerGroup * 3);
    const groupUnits = Math.max(Math.floor(Math.log10(groupValue) + 1), 1);

    const prefixes: string[] = ["", "K", "M", "B", "T"];
    const suffix = (powerGroup < prefixes.length) ? prefixes[powerGroup] : `x10^${powerGroup * 3}`;

    let valueText = `${originalSign === -1 ? "-" : ""}${groupValue.toFixed(noDecimals ? 0 : (3 - groupUnits))}`

    //TODO: Fix this another way...
    if (valueText.endsWith(".0")) valueText = valueText.replace(".0", "");
    if (valueText.endsWith(".00")) valueText = valueText.replace(".00", "");
    if (valueText.endsWith(".000")) valueText = valueText.replace(".000", "");

    return `${symbol}${valueText}${suffix}${unit}`;
  }
}

@Pipe({ name: "fmtUEC" })
export class FormatUECPipe implements PipeTransform { transform(): string { return fmtUEC.apply(null, arguments); } }
export function fmtUEC(value: number, noDecimals: boolean = false, onlyFormat: boolean = false): string {
  return fmtNumber(value, noDecimals, " uec", "", onlyFormat);
}


@Pipe({ name: "fmtMoney" })
export class FormatMoneyPipe implements PipeTransform { transform(): string { return fmtMoney.apply(null, arguments); } }
export function fmtMoney(value: number, noDecimals: boolean = false): string {
  return fmtNumber(value, noDecimals, "", "$");
}


@Pipe({ name: "fmtGeneric" })
export class FormatGenericPipe implements PipeTransform { transform(): string { return fmtGeneric.apply(null, arguments); } }
export function fmtGeneric(value: number, noDecimals: boolean = false, onlyFormat: boolean = false, unit: string = ""): string {
  return fmtNumber(value, noDecimals, unit, "", onlyFormat);
}


@Pipe({ name: "fmtUnits" })
export class FormatUnitsPipe implements PipeTransform { transform(): string { return fmtUnits.apply(null, arguments); } }
export function fmtUnits(units: number): string {
  return fmtNumber(units / 100, true, " SCU", "");
}


@Pipe({ name: "fmtSCU" })
export class FormatSCUPipe implements PipeTransform { transform(): string { return fmtSCU.apply(null, arguments); } }
export function fmtSCU(scu: number): string {
  return fmtNumber(scu, true, " SCU", "");
}


@Pipe({ name: "fmtPercent" })
export class FormatPercentPipe implements PipeTransform { transform(): string { return fmtPercent.apply(null, arguments); } }
export function fmtPercent(number: number, scale: boolean = false, difference: boolean = false): string {
  var finalNumber = scale ? (number * 100) : number;
  var symbol = "";
  if (difference) {
    finalNumber -= 100;
    //Force a plus symbol when positive in difference mode
    if (finalNumber > 0) symbol = "+";
  }
  return fmtNumber(finalNumber, true, "%", symbol, true);
}


@Pipe({ name: "fmtMod" })
export class FormatModifierPipe implements PipeTransform { transform(): string { return fmtModifier.apply(null, arguments); } }
export function fmtModifier(number: number): string {
  return fmtNumber(number, true, "%", number > 0 ? "+" : "", true);
}

@Pipe({
  name: "scaleunit",
  pure: true
})
export class ScaleUnitPipe implements PipeTransform {
  transform(value: number, noDecimals: boolean = false, unit: string = "", symbol: string = "$"): string {
    return fmtNumber(value, noDecimals, unit, symbol);
  }
}

