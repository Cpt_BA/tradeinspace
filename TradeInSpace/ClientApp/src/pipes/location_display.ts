import { Pipe, PipeTransform } from "@angular/core";
import { SystemLocation, LocationType, TradeShop, ShopType } from '../data/generated_models';

@Pipe({
  name: "location",
  pure: true
})
export class LocationDisplayPipe implements PipeTransform {
  static transform(value: SystemLocation, type: string = null): string {
    switch (type) {
      default:
        return value.Name;
    }
  }

  transform(value: SystemLocation, type: string = null): string {
    return LocationDisplayPipe.transform(value, type);
  }
}

export function locationdisplay(value: SystemLocation, type: string = null): string {
  return LocationDisplayPipe.transform(value, type);
}

@Pipe({
  name: "tradeshop",
  pure: true
})
export class TradeShopDisplayPipe implements PipeTransform {
  static transform(value: TradeShop, type: string = null): string {
    switch (type) {
      case "shop":
        switch (value.ShopType) {
          case ShopType.AdminTerminal: return "Admin";
          case ShopType.CommExTransfers: return "CommEx";
          case ShopType.TDD: return "TDD";
          default: return ShopType[value.ShopType];
        }
      default:
        switch (value.ShopType) {
          case ShopType.AdminTerminal:
            return value.Location.ParentLocation.Name;
          case ShopType.TDD:
            return `${value.Location.ParentLocation.Name} - TDD`;
          case ShopType.CommExTransfers:
            return `${value.Location.ParentLocation.Name} - CommEx`;
          default:
            return value.Name;
        }
    }
  }

  transform(value: TradeShop, type: string = null): string {
    return TradeShopDisplayPipe.transform(value, type);
  }
}

export function tradeshopdisplay(value: TradeShop, type: string = null): string {
  return TradeShopDisplayPipe.transform(value, type);
}
