import { SystemLocation, PadSize, ServiceFlags, SystemBody, GameDatabase, GameEquipment } from "../data/generated_models";
import { ClientData } from '../data/client_data';
import { FullJumpDetailPoint, FullJumpDetails, JumpDetails, MagazineData } from '../data/client_models';
import { GameDatabaseFull } from '../data/game_data';
import { deepCopy, locationDistance } from "./utilities";
import { Coordinate, CoordinateType, ObjectContainer, Vector3 } from "../data/coordinates";
import { max } from "rxjs-compat/operator/max";

export class ModelUtils {


  public static locationHasPad(location: SystemLocation, searchSize: PadSize, exploreUp = true): boolean {
    let padAvailable = false;
    if (location.LandingZones.length > 0) {
      location.LandingZones.forEach(lz => {
        const xlAvail = lz.XLPads > 0;
        const lAvail = xlAvail || (lz.LPads > 0);
        const mAvail = lAvail || (lz.MPads > 0);
        const sAvail = mAvail || (lz.SPads > 0);
        const xsAvail = sAvail || (lz.XSPads > 0);
        const tAvail = xsAvail || (lz.TPads > 0);

        switch (searchSize) {
          case PadSize.Tiny: padAvailable = padAvailable || tAvail; break;
          case PadSize.XSmall: padAvailable = padAvailable || xsAvail; break;
          case PadSize.Small: padAvailable = padAvailable || sAvail; break;
          case PadSize.Medium: padAvailable = padAvailable || mAvail; break;
          case PadSize.Large: padAvailable = padAvailable || lAvail; break;
          case PadSize.XLarge: padAvailable = padAvailable || xlAvail; break;
        }
      });
    }

    if (!padAvailable && exploreUp && location.ParentLocation) {
      return this.locationHasPad(location.ParentLocation, searchSize, exploreUp);
    } else {
      return padAvailable;
    }
  }

  public static hasServices(serviceFlags: ServiceFlags, checkService: ServiceFlags) {
    if (checkService === ServiceFlags.None) return true;
    return (serviceFlags & checkService) == checkService;
  }

  public static hasAnyService(serviceFlags: ServiceFlags, checkService: ServiceFlags) {
    if (checkService === ServiceFlags.None) return true;
    return !!(serviceFlags & checkService);
  }

  public static calculateBodyDistanceKM(fromBody: SystemBody, toBody: SystemBody): number {
    const fromXY = this.calculateBodyPositionKM(fromBody);
    const toXY = this.calculateBodyPositionKM(toBody);

    return Math.sqrt(Math.pow(fromXY.x - toXY.x, 2) + Math.pow(fromXY.y - toXY.y, 2));
  }

  public static locateCoordinates(systemData: ObjectContainer, coordinates: LocationPosition, coordinateDate: Date): CoordinateResult | null
  {
    const position: Vector3 = new Vector3(coordinates.LocationX, coordinates.LocationY, coordinates.LocationZ);
    const coord: Coordinate = Coordinate.fromGlobalAtTimeWithLocate("Player", CoordinateType.Player, position, coordinateDate, systemData);
    const coordJulian: Coordinate = coord.rotateToTime(Coordinate.ServerJulianTime);
    const planetLocation = this.calculatePlanetLatLonAlt(coordJulian.localPosition);

    const result: CoordinateResult = {
      position: coordJulian,
      position_now: coord,
      location: null,
      body_location: null,
      latitude: planetLocation.lat,
      longitude: planetLocation.lon,
      distance: planetLocation.alt
    };

    if (coordJulian.parentBody) {
      result.body_location = coordJulian.parentBody.dbBody.SystemLocation;
    }

    return result;
  }

  public static calculateLocationDistanceM(fromLocation: SystemLocation, toLocation: SystemLocation): number {
    return this.calculateLocationPositionM(fromLocation).DistanceTo(this.calculateLocationPositionM(toLocation));
  }

  public static calculateLocationPositionM(location: SystemLocation): Vector3 {
    return new Vector3(
      location.AdditionalProperties.GlobalPosition.X,
      location.AdditionalProperties.GlobalPosition.Y,
      location.AdditionalProperties.GlobalPosition.Z);
  }

  public static calculatePlanetLatLonAlt(localPosition: Vector3): { lat: number, lon: number, alt: number } {
    const xyLen = Math.sqrt(localPosition.X * localPosition.X + localPosition.Y * localPosition.Y);
    const longitude = Math.atan2(-localPosition.X, localPosition.Y) * (180 / Math.PI);
    const latitude = Math.atan2(localPosition.Z, xyLen) * (180 / Math.PI);
    return {
      lat: latitude,
      lon: longitude,
      alt: localPosition.Length()
    }
  }

  public static calculateBodyPositionKM(planetBody: SystemBody): { x: number, y: number } {
    const myLocation = {
      x: Math.cos(planetBody.OrbitStart / 180 * Math.PI) * (planetBody.OrbitRadius / 1000),
      y: -Math.sin(planetBody.OrbitStart / 180 * Math.PI) * (planetBody.OrbitRadius / 1000)
    }

    if (planetBody.ParentBody) {
      const parentLoc = this.calculateBodyPositionKM(planetBody.ParentBody);
      myLocation.x += parentLoc.x;
      myLocation.y += parentLoc.y;
    }

    return myLocation;
  }

  public static getRootBody(gameData: GameDatabase, planetBody: SystemBody): SystemBody {
    return planetBody.ParentBody ? this.getRootBody(gameData, planetBody.ParentBody) : planetBody;
  }

  public static calculateQuantumJump(fromBody: SystemBody, toBody: SystemBody, drive: GameEquipment): JumpDetails {
    if (!drive || drive.AttachmentType != ClientData.EquipmentTypes.QuantumDrive) return null;

    const totalM = this.calculateBodyDistanceKM(fromBody, toBody) * 1000;
    const vMax = drive.Properties.QDrive.DriveSpeed;

    //SUPER Naive calculation, TODO: Make better
    const accelTime = vMax / (drive.Properties.QDrive.S2AccelRate * 0.38);
    const fullSpeedTime = totalM / vMax;
    return {
      total_time: fullSpeedTime + accelTime,
      total_disance_km: totalM / 1000,
      fuel_consumption: totalM / (1000 * 1000) * drive.Properties.QDrive.FuelConsumptionRate,

      accel_time: accelTime / 2,
      cruise_time: fullSpeedTime,
      decel_time: accelTime / 2,
    };
  }

  public static calculateFullQuantumJump(fromBody: SystemBody, toBody: SystemBody, drive: GameEquipment): FullJumpDetails {
    if (!drive || drive.AttachmentType != ClientData.EquipmentTypes.QuantumDrive) return null;

    const totalM = this.calculateBodyDistanceKM(fromBody, toBody) * 1000;
    const vMax = drive.Properties.QDrive.DriveSpeed;

    //SUPER Naive calculation, TODO: Make better

    // dist = (initial vel * time) + (1/2 * accel * time ^ 2)

    const adjustedAccel = drive.Properties.QDrive.S2AccelRate * 0.38
    var accelTime = vMax / adjustedAccel;
    var accelDist = (adjustedAccel * accelTime) / 2;
    var fullSpeedTime = (totalM - 2 * accelDist) / vMax;
    var maxVelocity = vMax;
    var fuelPerMeter = 1 / (1000 * 1000 * drive.Properties.QDrive.FuelConsumptionRate);

    //We can't reach max speed before having to start decelerating
    if (accelDist * 2 > totalM) {
      console.log("**WARN** Drive does not reach max-speed before decel.");
      fullSpeedTime = 0;
      accelDist = totalM / 2;
      accelTime = Math.sqrt(accelDist * 2 / adjustedAccel)
      maxVelocity = adjustedAccel * accelTime
    }

    var totalTime = fullSpeedTime + accelTime * 2;

    //Ignoring S1 accel for simplicities sake
    //Start Point
    //S2 Accel Complete (Max Speed)
    //S2 Decel Start
    //Arrive

    return {
      duration: totalTime,
      max_velocity: maxVelocity,

      start: {
        description: "Start",
        distance_traveled: 0,
        distance_remaining: totalM,
        fuel_usage: 0,
        time_elapsed: 0,
        time_remaining: totalTime,
        velocity: 0
      },
      accel_finished: {
        description: "Max Velocity Reached",
        distance_traveled: accelDist,
        distance_remaining: totalM - accelDist,
        fuel_usage: accelDist * fuelPerMeter,
        time_elapsed: accelTime,
        time_remaining: totalTime - accelTime,
        velocity: maxVelocity
      },
      decel_started: {
        description: "Final Approach Started",
        distance_traveled: totalM - accelDist,
        distance_remaining: accelDist,
        fuel_usage: (totalM - accelDist) * fuelPerMeter,
        time_elapsed: totalTime - accelTime,
        time_remaining: accelTime,
        velocity: maxVelocity
      },
      end: {
        description: "Arrived",
        distance_traveled: totalM,
        distance_remaining: 0,
        fuel_usage: totalM * fuelPerMeter,
        time_elapsed: totalTime,
        time_remaining: 0,
        velocity: 0
      }
    }
  }

  public static getEquipmentAmmo(gameData: GameDatabaseFull, weapon: GameEquipment): MagazineData {
    let usingAmmo = weapon.Properties.Ammo;
    
    if (!usingAmmo) {
      const defaultMagazine = weapon.DefaultLoadout &&
        weapon.DefaultLoadout.find(ls => ls.PortName == "magazine_attach")
      if (defaultMagazine) {
        const magazineEntity = gameData.EquipmentKeyMap[defaultMagazine.ComponentKey];
        usingAmmo = magazineEntity.Properties.Ammo;
      }
    }

    return usingAmmo;
  }

  public static doLocate(gameData: GameDatabaseFull, coordinateString: string): CoordinateResult | null {
    const coordCheck = /Coordinates: x:([-\.\d]+) y:([-\.\d]+) z:([-\.\d]+)/;

    if (coordCheck.test(coordinateString)) {
      const matches = coordinateString.match(coordCheck);
      const playerPosition = {
        LocationX: parseFloat(matches[1]),
        LocationY: parseFloat(matches[2]),
        LocationZ: parseFloat(matches[3]),
      };

      const closestPOI = ModelUtils.locateCoordinates(gameData.SystemStructureRoot, playerPosition, new Date());
      

      //const closestPOI = ModelUtils.locateCoordinates(this.gameData, playerPosition);


      //this.router.navigate(['/', 'maps', closestPOI.body_location.Key, 12, closestPOI.latitude, closestPOI.longitude]);

      return closestPOI;
    } else return null;
  }



  public static EmptyJump: JumpDetails = {
    total_time: 0,
    total_disance_km: 0,
    fuel_consumption: 0,

    accel_time: 0,
    cruise_time: 0,
    decel_time: 0
  };

}

export interface CoordinateResult {
  location: SystemLocation,
  body_location: SystemLocation,

  distance: number,
  latitude: number,
  longitude: number,

  position: Coordinate,
  position_now: Coordinate
}

export interface LocationPosition {
  LocationX: number;
  LocationY: number;
  LocationZ: number;
}

