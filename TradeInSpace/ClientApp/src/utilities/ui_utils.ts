import { GameDatabaseFull } from '../data/game_data';
import { TreeNode, SelectItem } from 'primeng/api';
import { treeFromGroupData, TreeNodeMaker, TreeGroup, treeFromData, treeFromFlatData } from './utilities';
import { TradeItem, TradeShop, SystemBody, SystemLocation, ServiceFlags } from '../data/generated_models';
import { tradeshopdisplay, locationdisplay } from '../pipes/location_display';
import { Tree } from 'primeng/tree';
import { Table } from 'primeng/table';
import { ShowTooltipEvent } from '../directive/tooltip-attr.directive';
import { serviceFlagName } from '../pipes/display_text_pipe';

export class UiUtils {
  public static createStationTree(gameData: GameDatabaseFull, testCreate: (shop: TradeShop) => boolean = null, skipRoot: boolean = true): TreeNode[] {
    var shops = (testCreate) ? gameData.TradeShops.filter(testCreate) : gameData.TradeShops;

    var rootBodies = gameData.SystemBodies.filter(b => {
      if (skipRoot) {
        return b.ParentBody != null && b.ParentBody.ParentBody == null
      } else {
        return b.ParentBody == null;
      }
    });

    return treeFromData(
      rootBodies.sort(this.bodySort),
      //Get the children based on parent ID's
      body => { return gameData.SystemBodies.filter(b => b.SystemLocation.ParentBody == body.SystemLocation).sort(this.bodySort); },
      (has_children: boolean, body: SystemBody) => {
        var station_node = this.make_body_treenode(has_children, body);
        //Populate each body with the stations on that body
        station_node.children = treeFromFlatData(
          shops.filter(s => s.Location.ParentBody === body.SystemLocation),
          this.make_station_treenode);
        return station_node;
      });
  }

  public static createBodyTree(gameData: GameDatabaseFull, testCreate: (body: SystemBody) => boolean = null, skipRoot: boolean = true): TreeNode[] {
    var bodies = (testCreate) ? gameData.SystemBodies.filter(testCreate) : gameData.SystemBodies;

    var rootBodies = gameData.SystemBodies.filter(b => {
      if (skipRoot) {
        return b.ParentBody != null && b.ParentBody.ParentBody == null
      } else {
        return b.ParentBody == null;
      }
    });

    return treeFromData(
      //Start with the free-orbiting bodies (should parent a star)
      rootBodies.sort(this.bodySort),
      //Children of the element, look in our master list
      body => body.ChildBodies.filter(b => bodies.includes(b)).sort(this.bodySort),
      (has_children: boolean, body: SystemBody) => {
        return this.make_body_treenode(has_children, body);
      }, false);
  }

  public static createItemTree(gameData: GameDatabaseFull, testCreate: (item: TradeItem) => boolean = null): TreeNode[] {
    var items = (testCreate) ? gameData.TradeItems.filter(testCreate) : gameData.TradeItems;

    return treeFromGroupData(
      items,
      [(i) => i.Category],
      (has_child, data) => {
        if (has_child) {
          return this.make_item_category_treenode(has_child, data);
        } else {
          return this.make_item_treenode(has_child, data);
        }
      }
    );
  }

  public static selectAll(tree: Tree, selected_list: TreeNode[]): void {
    selected_list.length = 0;
    tree.getRootNode().forEach(n => this.addRecurse(n, selected_list));

    selected_list.forEach(n => this.markDownwards(n, selected_list));
  }

  public static unselectAll(tree: Tree, selected_list: TreeNode[]): void {
    selected_list.length = 0;
    tree.selection = [];

    selected_list.forEach(n => this.markDownwards(n, selected_list));
  }

  public static addRecurse(node: TreeNode, selectedNodes: TreeNode[]): void {
    selectedNodes.push(node);
    if (node.children) node.children.forEach(n => this.addRecurse(n, selectedNodes));
  }

  public static markDownwards(node: TreeNode, selectedNodes: TreeNode[]): void {
    //Depth first
    node.children.forEach(n => {
      if (n.children && n.children.length > 0) {
        this.markDownwards(n, selectedNodes);
      }
    })

    var selectedChildren: number = node.children.filter(n => selectedNodes.includes(n)).length;
    var childCount = node.children.length;

    if (selectedChildren == 0) {
      //Nothing
    } else if (selectedChildren == childCount) {
      //Allchildren, add node itself
      if (!selectedNodes.includes(node)) selectedNodes.push(node);
    } else {
      //Partial
      node.partialSelected = true;
    }
  }

  public static  findRecursive(nodes: TreeNode[], fnCheckItem: (item: any) => boolean): TreeNode[] {
    if (!nodes) return;

    var results: TreeNode[] = [];
    nodes.forEach(node => {
      if (fnCheckItem(node)) results.push(node);

      if (node.children) {
        this.findRecursive(node.children, fnCheckItem).forEach(c => results.push(c));
      }
    });
    return results;
  }

  public static bodySort(bodyA: SystemBody, bodyB: SystemBody): number {
    var childDiff = (bodyB.ChildBodies.length) - (bodyA.ChildBodies.length);
    if (childDiff != 0) return childDiff;

    var nameDiff = (bodyB.Name.localeCompare(bodyA.Name));
    return nameDiff;
  }

  public static distinctOptionList<T>(entries: T[], evaluator: (item: T) => string): SelectItem[] {
    return entries
      .map(evaluator)
      .filter((a, b, c) => c.indexOf(a) == b)
      .map(v => {
        return {
          value: v,
          title: v,
          label: v
        }
      });
  }

  public static listServicesTooltip(rowData: SystemLocation, group: ServiceGroup, $event: ShowTooltipEvent): void {
    let serviceList = "Services Available:<br/>";

    group.services.forEach(s => {
      serviceList += `<span style='font-weight: bold'>${s.description}</span><br/>`;
    });

    switch (group.header) {
      case "Ships":
        serviceList += this.getLandingTooltip(rowData);
        break;
      case "FPS":
      case "Admin":
        break;
    }

    $event.content = serviceList;
  }

  public static showServiceTooltip(rowData: SystemLocation, service: ServiceColumn, $event: ShowTooltipEvent): void {
    switch (service.service) {
      case ServiceFlags.ShipLanding:
      case ServiceFlags.ShipClaim:
        $event.content = this.getLandingTooltip(rowData);
        break;

      default:
        $event.content = serviceFlagName(service.service);
        break;

    }
  }

  public static getLandingTooltip(location: SystemLocation): string {
    if (location.LandingZones.length < 1) return "No pads directly at this location.";

    const padDetails = location.LandingZones[0]; //TODO: More

    const lines = [
      location.Name,
      "Pads available:",
      `X-Small: ${padDetails.XSPads}`,
      `Small: ${padDetails.SPads}`,
      `Tiny: ${padDetails.TPads}`,
      `Medium: ${padDetails.MPads}`,
      `Large: ${padDetails.LPads}`,
      `X-Large: ${padDetails.XLPads}`,
    ]

    return lines.join("<br/>");
  }

  static make_item_category_treenode: TreeNodeMaker<TreeGroup<TradeItem>> = (has_children, item_group) => {
    return {
      label: item_group.group,
      data: { Key: item_group.group },
      collapsedIcon: "pi pi-folder",
      expandedIcon: "pi pi-folder-open"
    };
  };

  static make_item_treenode: TreeNodeMaker<TradeItem> = (has_children, item) => {
    return {
      label: item.Name,
      data: item,
      collapsedIcon: "pi pi-shopping-cart",
      expandedIcon: "pi pi-shopping-cart"
    };
  }

  static make_station_treenode: TreeNodeMaker<TradeShop> = (has_children, station) => {
    return {
      label: tradeshopdisplay(station),
      data: station,
      collapsedIcon: "pi pi-home",
      expandedIcon: "pi pi-home"
    };
  };

  static make_body_treenode: TreeNodeMaker<SystemBody> = (has_children, body) => {
    return {
      label: locationdisplay(body.SystemLocation),
      data: body,
      collapsedIcon: "pi pi-globe",
      expandedIcon: "pi pi-globe"
    };
  };

  static buyableFilter(table: Table, event: boolean): void {
    if (event) {
      table.filter(0, 'TradeEntries.length', 'gt');
    } else {
      table.filter(0, 'TradeEntries.length', 'gte')
    }
  }
}

export interface ServiceColumn {
  icon: string;
  description: string;
  service: ServiceFlags;
}

export interface ServiceGroup {
  icon: string;
  header: string;
  description: string;
  groupServices: ServiceFlags;
  services: ServiceColumn[];
}
