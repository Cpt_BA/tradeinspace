import { Injectable, ErrorHandler } from '@angular/core';
import { DataCacheService } from '../data/data_cache';
import { environment } from '../environments/environment';
 
@Injectable()
export class TISErrorHandler implements ErrorHandler {

  public fatal_error: boolean = false;

  constructor() {
  }

  public handleError(err: Error) {
    //General application errors go here immediately
    //Currently don't handle, but eventuall should do better :)
    if (environment.production) {
    } else {
      console.error(err);
    }
  }

  public handleDataError(err: Error) {
    //Hey, at least I'm handling this...
    console.error(err);
    this.fatal_error = true;
  }

  public clearDataError() {
    this.fatal_error = false;
  }
}
