import { TreeNode } from 'primeng/api';
import { Observable } from 'rxjs';
import { saveAs } from 'file-saver';

export function groupSet<T>(flatData: T[], groupFunction: (item: T) => string): Group<T> {
  var result: Group<T> = {};

  flatData.forEach((record) => {
    var group_key = groupFunction(record);
    if (!result[group_key]) {
      result[group_key] = new Array<T>();
    }
    result[group_key].push(record);
  });

  return result;
}

export function deepCopy<T>(obj: T): T {
  return (JSON.parse(JSON.stringify(obj)) as T);
}

export function vector3Distance(vec1: { x: number, y: number, z: number }, vec2: { x: number, y: number, z: number }): number {
  return Math.sqrt(Math.pow(vec1.x - vec2.x, 2) + Math.pow(vec1.y - vec2.y, 2) + Math.pow(vec1.z - vec2.z, 2));
}

export function pointDistance(x1: number, y1: number, z1: number, x2: number, y2: number, z2: number): number {
  return vector3Distance({ x: x1, y: y1, z: z1 }, { x: x2, y: y2, z: z2 });
}

export function locationDistance(loc1: { LocationX: number, LocationY: number, LocationZ: number }, loc2: { LocationX: number, LocationY: number, LocationZ: number }): number {
  return vector3Distance({ x: loc1.LocationX, y: loc1.LocationY, z: loc1.LocationZ }, { x: loc2.LocationX, y: loc2.LocationY, z: loc2.LocationZ });
}

export function cleanCamelCase(camelString: string): string {
  return camelString
    .split(/[\s_]/)
    .map(c => c.charAt(0).toUpperCase() + c.slice(1))
    .join(" ");
} 

export function flattenRecursive<T>(root: T, children: (item: T) => T[]): T[] {
  var all = [];

  var _Insert = (item: T) => {
    if (item) {
      all.push(item);
      var childs = children(item);
      if (childs) {
        childs.forEach(c => _Insert(c));
      }
    }
  }
  _Insert(root);

  return all;
}

export interface Group<T> {
  [name: string]: T[];
}

export type TreeNodeMaker<T> = (has_children: boolean, data: T) => TreeNode;
export type TreeGroup<T> = { group: string, records: T[] };

export function treeFromData<T>(rootSet: T[], childFunction: (entry: T) => T[], make_node: TreeNodeMaker < T >, hide_empty: boolean = true, parent: TreeNode = null): TreeNode[] {
  var result: Array<TreeNode> = new Array<TreeNode>();
  rootSet.forEach(item => {
    var children = childFunction(item);
    var treeItem = make_node(children.length > 0, item);
    treeItem.children = treeItem.children ? treeItem.children : [];
    treeItem.parent = parent;
    var newChildren = treeFromData(children, childFunction, make_node, hide_empty, treeItem);
    newChildren.forEach(c => treeItem.children.push(c));
    if (treeItem.children.length == 0 && hide_empty) {
      //If no "actual" children, don't add this as an option
    } else {
      result.push(treeItem);
    }
  });
  return result;
}

export function treeFromFlatData<T>(flatData: T[], make_node: TreeNodeMaker < T >, parent_node: TreeNode = null): TreeNode[] {
  var result: Array<TreeNode> = new Array<TreeNode>();
  flatData.forEach((record: any) => {
    var newNode = make_node(false, record);
    newNode.parent = parent_node;
    result.push(newNode);
  });
  return result;
}

export function treeFromGroupData<T>(flatData: T[], groupFunctions: ((elem: T) => string)[], make_node: TreeNodeMaker<any>, parent_node: TreeNode = null): TreeNode[] {
  if (groupFunctions.length == 0) {
    return treeFromFlatData(flatData, make_node, parent_node);
  } else {
    var groupedRecords = groupSet(flatData, groupFunctions[0]);

    var result: Array<TreeNode> = new Array<TreeNode>();
    var sortedKeys = Object.keys(groupedRecords).sort();

    sortedKeys.forEach((group_key) => {
      var group = groupedRecords[group_key];
      var newCell = make_node(true, { group: group_key, records: group });
      newCell.parent = parent_node;
      newCell.children = treeFromGroupData<T>(group, groupFunctions.slice(1), make_node, newCell);
      result.push(newCell);
    });
    return result;
  }
}

export function* permuteOptions<T>(options: T[], targetDepth: number, current: T[] = [], depth: number = 0): IterableIterator<T[]> {
  for (var i = 0; i < options.length; i++) {
    var current_option = options[i];
    if (!current.includes(current_option)) {
      current[depth] = current_option;
      if (depth == targetDepth - 1) {
        yield current.map(c => c);
        current[depth] = null;
      } else {
        yield* permuteOptions(options, targetDepth, current, depth + 1);
      }
    }
  }
  current[depth] = null;
}

export interface Dictionary<TValue> {
  [Key: string]: TValue;
}

export function saveCSV(fileName: string, records: any[], headers: Dictionary<string> = {}) {
  var finalHeaders: Dictionary<string>;
  var finalContent: string = "";

  if (headers) {
    //If we specified headers, use only those
    finalHeaders = deepCopy(headers);
  } else {
    //Enumerate all keys on all records
    records.forEach((r, idx) => {
      for (var key in r) {
        if (!(key in finalHeaders))
          finalHeaders[key] = key;
      }
    });
  }

  //Write Headers
  var headerRow: string = "";
  Object.keys(finalHeaders).forEach((key, idx) => {
    headerRow += `${idx == 0 ? '' : ','}${finalHeaders[key]}`
  });
  finalContent += `${headerRow}\r\n`

  //Write Rows
  records.forEach((r) => {
    var rowContent: string = "";
    Object.keys(finalHeaders).forEach((key, idx) => {
      var columnData = key in r ? r[key] : '';
      //Handle object blobs by converting to JSON
      if (columnData instanceof Object)
        columnData = JSON.stringify(columnData)
      var columnString: string = `${columnData}`;
      if (!columnString)
        columnString = '';
      if (columnString.includes('"'))
        //Escape double quotes specifically
        columnString = columnString.replace(/"/g, '\\"');
      if (columnString.includes(',') ||
        columnString.includes("\n") ||
        columnString.includes("\\") ||
        columnString.includes('"') ||
        columnString.includes("'"))
        //Escape when containing the above chars
        columnString = `"${columnString}"`

      rowContent += `${idx == 0 ? '' : ','}${columnString}`
    });
    finalContent += `${rowContent}\r\n`
  });

  //Save the file
  saveTextFile(fileName, finalContent);
}

export function saveTextFile(fileName: string, contents: string, filetype: string = 'text/plain;charset=utf-8') {
  var data = new Blob([contents], { type: filetype });
  saveFile(fileName, data);
}

export function saveFile(fileName: string, blobContents: Blob) {
  saveAs(blobContents, fileName);
}
