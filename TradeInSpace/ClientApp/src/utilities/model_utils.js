"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var generated_models_1 = require("../data/generated_models");
var client_data_1 = require("../data/client_data");
var ModelUtils = /** @class */ (function () {
    function ModelUtils() {
    }
    ModelUtils.locationHasPad = function (location, searchSize, exploreUp) {
        if (exploreUp === void 0) { exploreUp = false; }
        var padAvailable = false;
        if (location.LandingZones.length > 0) {
            location.LandingZones.forEach(function (lz) {
                var xlAvail = lz.XLPads > 0;
                var lAvail = xlAvail || (lz.LPads > 0);
                var mAvail = lAvail || (lz.MPads > 0);
                var sAvail = mAvail || (lz.SPads > 0);
                var xsAvail = sAvail || (lz.XSPads > 0);
                var tAvail = xsAvail || (lz.TPads > 0);
                switch (searchSize) {
                    case generated_models_1.PadSize.Tiny:
                        padAvailable = padAvailable || tAvail;
                        break;
                    case generated_models_1.PadSize.XSmall:
                        padAvailable = padAvailable || xsAvail;
                        break;
                    case generated_models_1.PadSize.Small:
                        padAvailable = padAvailable || sAvail;
                        break;
                    case generated_models_1.PadSize.Medium:
                        padAvailable = padAvailable || mAvail;
                        break;
                    case generated_models_1.PadSize.Large:
                        padAvailable = padAvailable || lAvail;
                        break;
                    case generated_models_1.PadSize.XLarge:
                        padAvailable = padAvailable || xlAvail;
                        break;
                }
            });
        }
        if (!padAvailable && exploreUp && location.ParentLocation) {
            return this.locationHasPad(location.ParentLocation, searchSize, exploreUp);
        }
        else {
            return padAvailable;
        }
    };
    ModelUtils.hasServices = function (serviceFlags, checkService) {
        if (checkService == generated_models_1.ServiceFlags.None)
            return true;
        return (serviceFlags & checkService) == checkService;
    };
    ModelUtils.hasAnyService = function (serviceFlags, checkService) {
        if (checkService == generated_models_1.ServiceFlags.None)
            return true;
        return !!(serviceFlags & checkService);
    };
    ModelUtils.calculateBodyDistanceKM = function (fromBody, toBody) {
        var fromXY = this.calculateBodyPositionKM(fromBody);
        var toXY = this.calculateBodyPositionKM(toBody);
        return Math.sqrt(Math.pow(fromXY.x - toXY.x, 2) + Math.pow(fromXY.y - toXY.y, 2));
    };
    ModelUtils.calculateBodyPositionKM = function (planetBody) {
        var myLocation = {
            x: Math.cos(planetBody.OrbitStart / 180 * Math.PI) * (planetBody.OrbitRadius / 1000),
            y: Math.sin(planetBody.OrbitStart / 180 * Math.PI) * (planetBody.OrbitRadius / 1000)
        };
        if (planetBody.ParentBody) {
            var parentLoc = this.calculateBodyPositionKM(planetBody.ParentBody);
            myLocation.x += parentLoc.x;
            myLocation.y += parentLoc.y;
        }
        return myLocation;
    };
    ModelUtils.getRootBody = function (gameData, planetBody) {
        return planetBody.ParentBody ? this.getRootBody(gameData, planetBody.ParentBody) : planetBody;
    };
    ModelUtils.calculateQuantumJump = function (fromBody, toBody, drive) {
        if (!drive || drive.AttachmentType != client_data_1.ClientData.EquipmentTypes.QuantumDrive)
            return null;
        var totalM = this.calculateBodyDistanceKM(fromBody, toBody) * 1000;
        var vMax = drive.Properties.QDrive.DriveSpeed;
        //SUPER Naive calculation, TODO: Make better
        var accelTime = vMax / (drive.Properties.QDrive.S2AccelRate * 0.38);
        var fullSpeedTime = totalM / vMax;
        return {
            total_time: fullSpeedTime + accelTime,
            total_disance_km: totalM / 1000,
            fuel_consumption: totalM / (1000 * 1000) * drive.Properties.QDrive.FuelConsumptionRate,
            accel_time: accelTime / 2,
            cruise_time: fullSpeedTime,
            decel_time: accelTime / 2,
        };
    };
    ModelUtils.EmptyJump = {
        total_time: 0,
        total_disance_km: 0,
        fuel_consumption: 0,
        accel_time: 0,
        cruise_time: 0,
        decel_time: 0
    };
    return ModelUtils;
}());
exports.ModelUtils = ModelUtils;
//# sourceMappingURL=model_utils.js.map