"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function groupSet(flatData, groupFunction) {
    var result = {};
    flatData.forEach(function (record) {
        var group_key = groupFunction(record);
        if (!result[group_key])
            result[group_key] = new Array();
        result[group_key].push(record);
    });
    return result;
}
exports.groupSet = groupSet;
function treeFromData(rootSet, childFunction, make_node, parent) {
    if (parent === void 0) { parent = null; }
    var result = new Array();
    rootSet.forEach(function (item) {
        var children = childFunction(item);
        var treeItem = make_node(children.length > 0, item);
        treeItem.children = treeItem.children ? treeItem.children : [];
        treeItem.parent = parent;
        var newChildren = treeFromData(children, childFunction, make_node, treeItem);
        newChildren.forEach(function (c) { return treeItem.children.push(c); });
        if (treeItem.children.length == 0) {
            //If no "actual" children, don't add this as an option
        }
        else {
            result.push(treeItem);
        }
    });
    return result;
}
exports.treeFromData = treeFromData;
function treeFromFlatData(flatData, make_node, parent_node) {
    if (parent_node === void 0) { parent_node = null; }
    var result = new Array();
    flatData.forEach(function (record) {
        var newNode = make_node(false, record);
        newNode.parent = parent_node;
        result.push(newNode);
    });
    return result;
}
exports.treeFromFlatData = treeFromFlatData;
function treeFromGroupData(flatData, groupFunctions, make_node, parent_node) {
    if (parent_node === void 0) { parent_node = null; }
    if (groupFunctions.length == 0) {
        return treeFromFlatData(flatData, make_node, parent_node);
    }
    else {
        var groupedRecords = groupSet(flatData, groupFunctions[0]);
        var result = new Array();
        Object.keys(groupedRecords).forEach(function (group_key) {
            var group = groupedRecords[group_key];
            var newCell = make_node(true, { group: group_key, records: group });
            newCell.parent = parent_node;
            newCell.children = treeFromGroupData(group, groupFunctions.slice(1), make_node, newCell);
            result.push(newCell);
        });
        return result;
    }
}
exports.treeFromGroupData = treeFromGroupData;
//# sourceMappingURL=utilities.js.map