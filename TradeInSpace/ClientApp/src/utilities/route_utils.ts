import { SystemLocation, PadSize, ServiceFlags, SystemBody, GameDatabase, GameEquipment, BodyType, LocationType, GameShip } from "../data/generated_models";
import { ClientData, MinMax, PropertyMinMaxes, RankedProperty } from '../data/client_data';
import { JumpDetails, AmmoData, MagazineData, DisplayRoute, CalculatedRoute, FilterParams, PreferenceEnum } from '../data/client_models';
import { GameDatabaseFull } from '../data/game_data';
import { deepCopy, locationDistance } from "./utilities";
import { Coordinate, CoordinateType, ObjectContainer, Vector3 } from "../data/coordinates";
import { Loadout } from "../data/loadout";
import { ItineraryBuilder } from "../data/itinerary_builder";
import { BasicUserSettings } from "../data/models";
import { ModelUtils } from "./model_utils";

export class RouteUtils {
  public static calculateDisplayRoutes(gameData: GameDatabaseFull, inRoutes: CalculatedRoute[], loadout: Loadout, userData: BasicUserSettings, filterSettings: FilterParams,
    fnBeforeCalculate: (DisplayRoute) => void = null, fnAfterCalculate: (DisplayRoute) => void = null): DisplayRoute[] {
    const newRoutes = inRoutes;
    const displayRoutes: DisplayRoute[] = [];
    const currentShip = loadout;
    const ship: GameShip = gameData.ShipKeyMap[currentShip.equipment.Key];
    const drives = currentShip.find_slots_equipment_type(ClientData.EquipmentTypes.QuantumDrive);
    const drive: GameEquipment = drives.length > 0 ? drives[0].attached_equipment : null;

    //TODO: Allow no drive if no jumps needed
    if (!ship || !drive) {
      return [];
    }

    newRoutes.forEach(route => {
      var fullRoute = <DisplayRoute>route;
      fullRoute.rankProperties = {};

      if (fnBeforeCalculate) fnBeforeCalculate(fullRoute);

      fullRoute.cargo_units = (userData.use_capacity ? userData.custom_capacity : ship.CargoCapacity) * 100;
      if (userData.use_balance) {
        fullRoute.cargo_units = Math.min(fullRoute.cargo_units, Math.floor(userData.balance / route.avg_buy));
      }

      fullRoute.itinerary = ItineraryBuilder.CalculateForRoute(gameData, ship, drive, fullRoute, userData, filterSettings);

      displayRoutes.push(fullRoute);
    });

    this.calculateMinMaxes(displayRoutes);

    displayRoutes.forEach(route => {
      //HACK, TODO: properly store these for direct reference by sorting
      //Or make a type
      //Or modify grid code to handle them better
      ClientData.RouteRankProperties.forEach(rankProp => {
        route[rankProp.name] = rankProp.fnValue(route);
      });

      if (fnAfterCalculate) fnAfterCalculate(route);
    });

    return displayRoutes;
  }

  public static calculateVisibility(routes: DisplayRoute[], filterSettings: FilterParams): void {
    routes.forEach(route => {
      route.hidden = false;

      if (filterSettings.minimum_pad_size) {
        if (!ModelUtils.locationHasPad(route.from.Location, filterSettings.minimum_pad_size) ||
          !ModelUtils.locationHasPad(route.to.Location, filterSettings.minimum_pad_size)) {
          route.hidden = true;
        }
      }

      if (filterSettings.total_time_max != 0) {
        if (route.itinerary.totalDurationS > filterSettings.total_time_max * 60) route.hidden = true;
      }

      if (filterSettings.buy_time_max != 0) {
        if (route.itinerary.totalBuyS > filterSettings.buy_time_max * 60) route.hidden = true;
      }

      if (filterSettings.transit_time_max != 0) {
        if (route.itinerary.totalTransitS > filterSettings.transit_time_max * 60) route.hidden = true;
      }

      if (filterSettings.ship_time_max != 0) {
        if (route.itinerary.totalShipS > filterSettings.ship_time_max * 60) route.hidden = true;
      }

      if (filterSettings.sell_time_max != 0) {
        if (route.itinerary.totalSellS > filterSettings.sell_time_max * 60) route.hidden = true;
      }

      if (filterSettings.item_keys.length > 0) {
        if (!filterSettings.item_keys.includes(route.item.Key)) route.hidden = true;
      }

      if (filterSettings.station_keys.length > 0) {
        if (!filterSettings.station_keys.includes(route.from.Key) &&
          !filterSettings.station_keys.includes(route.to.Key)) route.hidden = true;
      }
    });
  }

  public static calculateRouteScores(routes: DisplayRoute[], filterSettings: FilterParams): void {
    routes.forEach(route => {
      route.score = this.calculateRouteScore(route, filterSettings);

      if (route.score < 0) route.hidden = true;
    });

    //Map duplicates the array, so we aren't influencing other things using available_routes
    var sortedRoutes = routes.map(r => r).sort((a, b) => { return a.score - b.score; });
    for (var rank = 0; rank < sortedRoutes.length; rank++) {
      sortedRoutes[rank].rank = sortedRoutes.length - rank;
    }
  }

  private static calculateRouteScore(route: DisplayRoute, filterSettings: FilterParams): number {
    var fromHidden = route.from.Location.IsHidden;
    var toHidden = route.to.Location.IsHidden;

    var fromCity = ClientData.TradeShopTypes.includes(route.from.ShopType);
    var toCity = ClientData.TradeShopTypes.includes(route.to.ShopType);

    if (filterSettings.hidden_preference == PreferenceEnum.Ignore && (fromHidden || toHidden)) {
      return -1000;
    }

    if (filterSettings.city_preference == PreferenceEnum.Ignore && (fromCity || toCity)) {
      return -1000;
    }

    if (filterSettings.quantum_preference == PreferenceEnum.Ignore && route.itinerary.totalJumps > 1) {
      return -1000;
    }

    var finalScore: number = 0;

    //TODO: More complexity
    finalScore += 50;
    switch (filterSettings.city_preference) {
      case PreferenceEnum.Prefer:
        finalScore += fromCity ? 25 : 0;
        finalScore += toCity ? 25 : 0;
        break;
      case PreferenceEnum.Ignore:
        finalScore -= fromCity ? 25 : 0;
        finalScore -= toCity ? 25 : 0;
        break;
    }

    finalScore += 50;
    switch (filterSettings.hidden_preference) {
      case PreferenceEnum.Prefer:
        finalScore += fromHidden ? 25 : 0;
        finalScore += toHidden ? 25 : 0;
        break;
      case PreferenceEnum.Ignore:
        finalScore -= fromHidden ? 25 : 0;
        finalScore -= toHidden ? 25 : 0;
        break;
    }

    switch (filterSettings.quantum_preference) {
      case PreferenceEnum.Prefer:
        finalScore += Math.round(route.rankProperties.total_ship.percent * 100);
        break;
      case PreferenceEnum.Ignore:
        finalScore += Math.round(1 - route.rankProperties.total_ship.percent * 100);
        break;
    }

    switch (filterSettings.hv_cargo_preference) {
      case PreferenceEnum.Prefer:
        finalScore += Math.round(route.rankProperties.max_unit_cost.percent * 100);
        break;
      case PreferenceEnum.Ignore:
        finalScore += Math.round((1 - route.rankProperties.max_unit_cost.percent) * 100);
        break;
    }

    switch (filterSettings.short_route_preference) {
      case PreferenceEnum.Prefer:
        finalScore += Math.round((1 - route.rankProperties.total_time.percent) * 100);
        break;
    }

    finalScore += Math.round(route.rankProperties.max_profit_rate.percent * 150);

    return finalScore;
  }

  private static calculateMinMaxes(displayRoutes: DisplayRoute[]) {
    var valueRanges: PropertyMinMaxes = {};
    ClientData.RouteRankProperties.forEach(prop => {
      valueRanges[prop.name] = this.calculateMinMax(displayRoutes, prop);
    });
    ClientData.RouteRankProperties.forEach(prop => {
      displayRoutes.forEach(route => {
        var val = prop.fnValue(route);
        var scale = this.scaleValue(prop.name, val, valueRanges);
        route.rankProperties[prop.name] = {
          value: val,
          percent: scale,
          color: prop.fnMixColor(scale)
        }
      });
    });
  }

  private static calculateMinMax(displayRoutes: DisplayRoute[], property: RankedProperty<DisplayRoute>): MinMax {
    var result: MinMax = { min: 1000000, max: -1000000 };
    displayRoutes.forEach(route => {
      var val = property.fnValue(route);
      if (val === Infinity || val === -Infinity) return;

      if (val > result.max) result.max = val;
      if (val < result.min) result.min = val;
    });
    return result;
  }

  private static scaleValue(propertyName: string, value: number, valueRanges: PropertyMinMaxes): number {
    var range = valueRanges[propertyName];
    if (value === Infinity) return 1.0;
    if (value === -Infinity) return -1.0;
    return (Math.min(Math.max(value, range.min), range.max) - range.min) / (range.max - range.min);
  }
}

