using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Cors.Infrastructure;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.ResponseCompression;
using Microsoft.AspNetCore.SpaServices.AngularCli;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using TradeInSpace.Data;

namespace TradeInSpace
{
    public class Startup
    {
        string CorsPolicyName = "CORS_All";

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

#if RELEASE
            // Configure Compression level
            //services.Configure<GzipCompressionProviderOptions>(options => options.Level = CompressionLevel.Fastest);
            // Add Response compression services
            services.AddResponseCompression(options =>
            {
                options.Providers.Add<GzipCompressionProvider>();
                options.EnableForHttps = true;
            });
#endif


            services.AddCors(options => {
                options.AddPolicy(CorsPolicyName, builder => {
                    builder.AllowAnyOrigin().WithMethods("GET");
                });
            });

            services.AddMvc((opt) =>
            {
                opt.RespectBrowserAcceptHeader = true;
            })
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_1)
                .AddJsonOptions((opt) =>
            {
                opt.SerializerSettings.ContractResolver = new DefaultContractResolver()
                {
                    NamingStrategy = new DefaultNamingStrategy()
                };
            });

            // In production, the Angular files will be served from this directory
            services.AddSpaStaticFiles(configuration =>
            {
                configuration.RootPath = "ClientApp/dist";
            });

            var connectionString = Configuration.GetConnectionString("TradeInSpace");

            //services.AddDbContext<TISContext>(opts => opts.UseMySQL(connectionString));
            services.AddDbContext<TISContext>(opts => opts.UseSqlServer(connectionString));
#if RELEASE && (!STAGING)
            services.AddApplicationInsightsTelemetry();
#endif
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseSpaStaticFiles();

#if RELEASE
            app.UseResponseCompression();
#endif

            app.UseCors(CorsPolicyName);
            
            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "mapviewer",
                    template: "{controller=Maps}/{action=Index}");

                routes.MapRoute(
                    name: "default",
                    template: "{controller}/{action=Index}/{id?}");
            });
            
            app.UseSpa(spa =>
            {
                // To learn more about options for serving an Angular SPA from ASP.NET Core,
                // see https://go.microsoft.com/fwlink/?linkid=864501

                spa.Options.SourcePath = "ClientApp";

                if (env.IsDevelopment())
                {
                    spa.UseAngularCliServer(npmScript: "start");
                }
            });
        }
    }
}
