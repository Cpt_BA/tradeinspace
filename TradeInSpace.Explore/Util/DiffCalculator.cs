﻿using ICSharpCode.SharpZipLib.Zip;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using TradeInSpace.Explore.DataForge;

namespace TradeInSpace.Explore.Util
{
    public class DiffCalculator
    {
        public static DiffResultSet BuildDiff(
            IPackedFileReader OldSource, IPackedFileReader NewSource)
        {
            Dictionary<string, (long CRC, long Size)> GetZipHashes(IEnumerable<ZipEntry> Archive)
            {
                Dictionary<string, (long CRC, long Size)> crcResults = new Dictionary<string, (long CRC, long Size)>();

                foreach (ZipEntry entry in Archive)
                {
                    crcResults.Add(entry.Name, (entry.Crc, entry.Size));
                }

                return crcResults;
            };

            var oldDetails = GetZipHashes(OldSource.ZipChildren());
            var newDetails = GetZipHashes(NewSource.ZipChildren());

            List<string> adds = new List<string>();
            List<string> changes = new List<string>();
            List<string> removals = new List<string>();

            var removedFiles = oldDetails.Where(on => !newDetails.ContainsKey(on.Key));
            var addedFiles = newDetails.Where(nn => !oldDetails.ContainsKey(nn.Key));
            var changedFiles = newDetails.Where(nn => oldDetails.ContainsKey(nn.Key) && oldDetails[nn.Key].CRC != newDetails[nn.Key].CRC);

            var results = new DiffResultSet(OldSource, NewSource, DiffSource.Archive);

            foreach (var added in addedFiles)
                results.Added.Add(new DiffResult()
                {
                    FilePath = added.Key,
                    Name = System.IO.Path.GetFileName(added.Key),
                    NewHash = newDetails[added.Key].CRC,
                    Status = DiffStatus.Added,
                    Diff = results,
                    ExtraDetails = $"[ + {Utilities.FormatBytes(newDetails[added.Key].Size)}]"
                });

            foreach (var modified in changedFiles)
                results.Modified.Add(new DiffResult()
                {
                    FilePath = modified.Key,
                    Name = System.IO.Path.GetFileName(modified.Key),
                    PreviousHash = oldDetails[modified.Key].CRC,
                    NewHash = newDetails[modified.Key].CRC,
                    Status = DiffStatus.Modified,
                    Diff = results,
                    ExtraDetails = $"[{Utilities.FormatBytes(oldDetails[modified.Key].Size)} => {Utilities.FormatBytes(newDetails[modified.Key].Size)}]"
                });

            foreach (var removed in removedFiles)
                results.Removed.Add(new DiffResult()
                {
                    FilePath = removed.Key,
                    Name = System.IO.Path.GetFileName(removed.Key),
                    PreviousHash = oldDetails[removed.Key].CRC,
                    Status = DiffStatus.Removed,
                    Diff = results,
                    ExtraDetails = $"[ - {Utilities.FormatBytes(oldDetails[removed.Key].Size)}]"
                });

            results.RootAdded       = Utilities.PathToTree(results.Added,    r => r.FilePath, r => r.Children, DiffResult.Dummy).ToList();
            results.RootModified    = Utilities.PathToTree(results.Modified, r => r.FilePath, r => r.Children, DiffResult.Dummy).ToList();
            results.RootRemoved     = Utilities.PathToTree(results.Removed,  r => r.FilePath, r => r.Children, DiffResult.Dummy).ToList();

            return results;
        }

        public static DiffResultSet BuildDCBDiff(IPackedFileReader OldSource, IPackedFileReader NewSource, 
            out Dictionary<Guid, XmlElement> oldDCBElements, out Dictionary<Guid, XmlElement> newDCBElements)
        {
            unforge.DataForge oldDF, newDF;

            var oldDCB = OldSource.ReadFileBinary(DFDatabase.GameDCBPath);
            var newDCB = NewSource.ReadFileBinary(DFDatabase.GameDCBPath);

            using (var oldMS = new MemoryStream(oldDCB))
            using (var oldBR = new BinaryReader(oldMS))
                oldDF = new unforge.DataForge(oldBR);

            using (var newMS = new MemoryStream(newDCB))
            using (var newBR = new BinaryReader(newMS))
                newDF = new unforge.DataForge(newBR);

            oldDCBElements = new Dictionary<Guid, XmlElement>();
            newDCBElements = new Dictionary<Guid, XmlElement>();

            foreach (XmlElement oldDCBElement in oldDF.FullDocument.SelectSingleNode("/DataForge").ChildNodes.OfType<XmlElement>())
            {
                Guid id = Guid.Parse(oldDCBElement.GetAttribute("__ref"));
                oldDCBElements[id] = oldDCBElement;
            }

            foreach (XmlElement newDCBElement in newDF.FullDocument.SelectSingleNode("/DataForge").ChildNodes.OfType<XmlElement>())
            {
                Guid id = Guid.Parse(newDCBElement.GetAttribute("__ref"));
                newDCBElements[id] = newDCBElement;
            }

            var addedRecords = newDCBElements.Keys.Except(oldDCBElements.Keys).ToList();
            var removedRecords = oldDCBElements.Keys.Except(newDCBElements.Keys).ToList();
            var possible_modify = newDCBElements.Keys.Where(oldDCBElements.ContainsKey).ToList();
            List<Guid> modifiedRecords = new List<Guid>();

            foreach (var guid in possible_modify)
            {
                var oldEntry = oldDCBElements[guid];
                var newEntry = newDCBElements[guid];
                if (oldEntry == null || newEntry == null) continue;
                if (oldEntry.OuterXml != newEntry.OuterXml)
                    modifiedRecords.Add(guid);
            }

            var results = new DiffResultSet(OldSource, NewSource, DiffSource.DCB);

            foreach (var added in addedRecords)
            {
                var addedElement = newDCBElements[added];
                results.Added.Add(new DiffResult()
                {
                    FilePath = addedElement.GetAttribute("__path"),
                    Name = addedElement.Name,
                    NewHash = addedElement.OuterXml.ToString().GetHashCode(),
                    Status = DiffStatus.Added,
                    Diff = results,
                    RecordGUID = added
                });
            }

            foreach (var modified in modifiedRecords)
            {
                var oldElement = oldDCBElements[modified];
                var newElement = newDCBElements[modified];
                results.Modified.Add(new DiffResult()
                {
                    FilePath = newElement.GetAttribute("__path"),
                    Name = newElement.Name,
                    PreviousHash = oldElement.OuterXml.ToString().GetHashCode(),
                    NewHash = newElement.OuterXml.ToString().GetHashCode(),
                    Status = DiffStatus.Modified,
                    Diff = results,
                    RecordGUID = modified
                });
            }

            foreach (var removed in removedRecords)
            {
                var removedElement = oldDCBElements[removed];
                results.Removed.Add(new DiffResult()
                {
                    FilePath = removedElement.GetAttribute("__path"),
                    Name = removedElement.Name,
                    PreviousHash = removedElement.OuterXml.ToString().GetHashCode(),
                    Status = DiffStatus.Removed,
                    Diff = results,
                    RecordGUID = removed
                });
            }

            results.RootAdded = Utilities.PathToTree(results.Added, r => r.FilePath, r => r.Children, DiffResult.Dummy).ToList();
            results.RootModified = Utilities.PathToTree(results.Modified, r => r.FilePath, r => r.Children, DiffResult.Dummy).ToList();
            results.RootRemoved = Utilities.PathToTree(results.Removed, r => r.FilePath, r => r.Children, DiffResult.Dummy).ToList();

            return results;
        }
    }

    public class DiffResultSet
    {
        public DiffResultSet(IPackedFileReader oldSource, IPackedFileReader newSource, DiffSource source)
        {
            OldSource = oldSource;
            NewSource = newSource;
            Source = source;
        }

        public List<DiffResult> Added { get; internal set; } = new List<DiffResult>();
        public List<DiffResult> Modified { get; internal set; } = new List<DiffResult>();
        public List<DiffResult> Removed { get; internal set; } = new List<DiffResult>();

        public List<DiffResult> RootAdded { get; internal set; }
        public List<DiffResult> RootModified { get; internal set; }
        public List<DiffResult> RootRemoved { get; internal set; }

        public DiffSource Source { get; internal set; }

        // These properties are for tracking either a DFDatabase or SOC_Archive object.
        public IPackedFileReader OldSource { get; set; }
        public IPackedFileReader NewSource { get; set; }
    }

    public class DiffResult
    {
        public string FilePath { get; internal set; }
        public string Name { get; internal set; }
        public string ExtraDetails { get; set; }

        public DiffStatus Status { get; internal set; }
        public long PreviousHash { get; internal set; } = 1;
        public long NewHash { get; internal set; } = -1;
        public DiffResultSet Diff { get; internal set; }

        public Guid? RecordGUID { get; internal set; }

        //Used for hierarchical grouping
        public ObservableCollection<DiffResult> Children { get; set; } = new ObservableCollection<DiffResult>();

        public static DiffResult Dummy(string Path)
        {
            return new DiffResult()
            {
                FilePath = Path,
                Status = DiffStatus.Dummy,
                Name = System.IO.Path.GetFileName(Path)
            };
        }
    }

    public enum DiffSource
    {
        Archive,
        DCB
    }

    public enum DiffStatus
    {
        Removed,
        Added,
        Modified,
        Dummy
    }
}
