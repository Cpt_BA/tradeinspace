﻿using Serilog;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using TradeInSpace.Explore;

namespace TradeInSpace.Explore.Subsumption
{
    public class LayerZero
    {
        public XmlElement LayerZeroElement { get; set; }

        public List<ShopLayoutMapping> LayoutMap { get; set; } = new List<ShopLayoutMapping>();

        private LayerZero() { }

        internal static LayerZero LoadFromGameDB(DataForge.DFDatabase rawGameData)
        {
            var layerZero = rawGameData.ReadPackedFileCryXML(@"Data\Libs\Subsumption\Platforms\PU\system\Stanton\stantonsystem\Layer0.xml");

            if(layerZero == null)
            {
                //Game no longer contains raw trade data :c
                var fakeDoc = new XmlDocument();
                fakeDoc.AppendChild(fakeDoc.CreateElement("dummy"));
                return new LayerZero()
                {
                    LayerZeroElement = fakeDoc.DocumentElement,
                };
            }

            Log.Information($"[SUB] Loaded Layer0 data (shop profile mapping).");

            return new LayerZero()
            {
                LayerZeroElement = layerZero.DocumentElement
            };
        }

        internal void LoadLayoutMappings(Func<ShopLayoutMapping, bool> TestLayout)
        {
            LayoutMap.Clear();
            LayoutMap.AddRange(
                LayerZeroElement
                    .SelectNodes("//Variable")
                    .DeserializeBulk<ShopLayoutMapping>("Variable")
                    .Where(slm => slm != null && TestLayout(slm))
            );
        }
    }

    public class ShopLayoutMapping
    {
        [XmlAttribute("ID")]
        public string ShopSuperGUID { get; set; }
        [XmlAttribute("DefaultValue")]
        public Guid LayoutGUID { get; set; }
    }
}
