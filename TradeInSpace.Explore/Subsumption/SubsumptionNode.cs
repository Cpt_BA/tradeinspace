﻿using CgfConverter.Structs;
using Serilog;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using TradeInSpace.Explore.DataForge;
using TradeInSpace.Explore.Socpak;


namespace TradeInSpace.Explore.Subsumption
{
    using DN = System.DoubleNumerics;

    [DebuggerDisplay("[{GUID}] - {Name} {Type} ({Filename})")]
    public class SubsumptionNode
    {
        public SubsumptionNode() { }


        [XmlAttribute("ID")]
        public string SuperGUID { get; set; }
        [XmlAttribute("Name")]
        public string Name { get; set; }
        [XmlAttribute("Type")]
        public string Type { get; set; }
        [XmlAttribute("VariableTypeID")]
        public string TypeID { get; set; }
        [XmlAttribute("DataSource")]
        public string DataSource { get; set; }
        [XmlAttribute("Filename")]
        public string Filename { get; set; }


        [XmlElement(ElementName = "Position")]
        public Vector3 LocalPosition { get; set; }
        [XmlElement(ElementName = "Rotation")]
        public Vector4 LocalRotation { get; set; }

        [XmlArray("Variables")]
        [XmlArrayItem(ElementName = "Variable")]
        public List<SubsumptionNode> Children { get; set; }

        [XmlElement("DataClassName")]
        public DataElement DataClassName { get; set; }
        [XmlElement("Data")]
        public DataElement Data { get; set; }



        [XmlIgnore]
        public SOC_Entry SOC_Entry { get; set; }
        [XmlIgnore]
        public XmlElement OriginalElement { get; internal set; }

        [XmlIgnore]
        public bool IsContainerLink { get { return Type == "PlatformObjectContainerLink"; } }
        [XmlIgnore]
        public string GUID { get { return SuperGUID.Split(',').Last(); } }
        //[XmlIgnore]
        //public DN.Matrix4x4 Transform { get; set; }
        //[XmlIgnore]
        //public Vector3 GlobalPosition { get; set; }
        //[XmlIgnore]
        //public Vector4 GlobalRotation { get; set; }
    }

    public class SubsumptionRoot
    {
        [XmlAttribute("ID")]
        public string ID { get; set; }
        [XmlAttribute("Name")]
        public string Name { get; set; }
        [XmlAttribute("SolarSystem")]
        public string SolarSystem { get; set; }
        public bool IsSolarSystem => SolarSystem.Equals("true", StringComparison.OrdinalIgnoreCase);
        [XmlElement("DataFile")]
        public DataElement DataFile { get; set; }


        [XmlArray("Variables")]
        [XmlArrayItem(ElementName = "Variable")]
        public List<SubsumptionNode> Children { get; set; }

        [XmlIgnore]
        public Dictionary<string, SubsumptionNode> AllChildren { get; set; }
        [XmlIgnore]
        public XmlElement OriginalElement { get; set; }

        internal static SubsumptionRoot LoadFromGameDB(string SubsumptionFilePath, DataForge.DFDatabase rawGameData, bool PreserveOriginalElement = true)
        {
            var systemTree = rawGameData.ReadPackedFileCryXML(SubsumptionFilePath);

            if(systemTree == null)
            {
                return null;
            }

            var platformElement = systemTree.SelectSingleNode("//Platform");
            var platform = platformElement.Deserialize<SubsumptionRoot>("Platform");


            //Recursively flatten all children of this root
            platform.AllChildren = platform.Children
                .SelectMany(sn => Utilities.FlattenRecursive(sn, sc => sc.Children))
                .ToDictionary(fc => fc.SuperGUID, fc => fc);

            if (PreserveOriginalElement)
            {
                platform.OriginalElement = platformElement as XmlElement;

                Action<SubsumptionNode, XmlElement> fnSetOriginalElement = null;
                fnSetOriginalElement = (subNode, parentElem) =>
                {
                    subNode.OriginalElement = parentElem.SelectSingleNode($"//Variable[@ID='{subNode.SuperGUID}']") as XmlElement;

                    if (subNode?.Children != null)
                        subNode.Children.ForEach((c) => fnSetOriginalElement(c, subNode.OriginalElement));
                };

                platform.Children.ForEach((c) => fnSetOriginalElement(c, platform.OriginalElement));
            }

            return platform;
        }
    }

    public class DataElement
    {
        [XmlText]
        public string Data { get; set; }
    }
}
