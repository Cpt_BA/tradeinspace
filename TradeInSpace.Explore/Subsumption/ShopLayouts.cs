﻿using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using TradeInSpace.Models;

namespace TradeInSpace.Explore.Subsumption
{
    public class ShopLayouts
    {
        public List<SubsumptionShop> Shops { get; set; } = new List<SubsumptionShop>();
        public ShopLayoutNode RootShop { get; set; }

        private ShopLayouts() { }

        internal static ShopLayouts LoadFromGameDB(DataForge.DFDatabase rawGameData)
        {
            var shopLayouts = rawGameData.ReadPackedFileCryXML(@"Data\Libs\Subsumption\Shops\ShopLayouts.xml");

            if (shopLayouts == null)
                return new ShopLayouts();

            var shops = shopLayouts.Deserialize<ShopLayoutNode>("Subsumption");
            var flatShops = Utilities.FlattenRecursive(shops, s => s.ShopLayoutNodes);

            foreach (var parentShop in flatShops)
                foreach (var child in parentShop.ShopLayoutNodes)
                    child.Parent = parentShop;

            List<SubsumptionShop> allShops = new List<SubsumptionShop>();
            List<ShopLayoutNode> missingTypes = new List<ShopLayoutNode>();

            List<ShopLayoutNode> categories = flatShops.Where(s => s.ShopLayoutNodes.Length > 0).ToList();

            foreach(var category in categories)
            {
                Log.Information($"Category: [{category.ID}] - {category.Name}");
            }

            foreach(var shop in flatShops)
            {
                if (shop.Parent == null) continue;
                if (shop.ShopInventoryNodes.Length == 0) continue; //Only actually look at stores with inventories
                if (IgnoreShops.Contains(shop.Name) || IgnoreShops.Contains(shop.Parent?.Name)) continue;

                Enums.ShopType? shopType = null;

                foreach (var nameMap in ShopNameMapping)
                {
                    if (shop.Name.StartsWith(nameMap.Key))
                    {
                        shopType = nameMap.Value;
                        break;
                    }
                }

                if (!shopType.HasValue && ShopLayoutNodeMapping.ContainsKey(shop.Parent.ID))
                {
                    shopType = ShopLayoutNodeMapping[shop.Parent.ID];
                }

                if (shopType.HasValue)
                {
                    if(shopType.Value != Enums.ShopType.Ignore)
                        allShops.Add(new SubsumptionShop(shopType.Value, shop));
                }
                else
                    missingTypes.Add(shop);
            }

            if(missingTypes.Count > 0)
            {
                Log.Error($"The following shop names are missing / unmapped.");
                foreach(var missingShop in missingTypes)
                {
                    Log.Error($"{missingShop.ID} - {missingShop.Name} - {missingShop.Parent.ID}");
                }
            }

            Log.Information($"[SUB] Loaded {allShops.Count} shop nodes.");

            return new ShopLayouts()
            {
                Shops = allShops,
                RootShop = shops
            };
        }

        //Static data

        static List<string> IgnoreShops = new List<string>()
        {
            "AC_Inventory",
            "AnniversarySales",
            "NovemberAnniversarySale2018",
            "NovemberAnniversarySale2019",
            "ExpoHall_Lobby",
            "TestInventories",
            "FeatureTest_Shopping",
            "Prisons",
            "IAE",
            "Xenothreat-SecurityStation"
        };

        static Dictionary<string, Enums.ShopType> ShopNameMapping = new Dictionary<string, Enums.ShopType>()
        {
            { "Skutters_Armor_Weap", Enums.ShopType.Skutters},
            { "Skutters_Food", Enums.ShopType.Skutters },
            { "Market_ClothingStand", Enums.ShopType.GrandBarter},
            { "Bazaar_GadgetStand", Enums.ShopType.GrandBarter },

            { "CasabaOutlet_", Enums.ShopType.Casaba },
            { "LiveFireWeapons_", Enums.ShopType.LiveFireWeapons },
            { "RS_RefineryStore_", Enums.ShopType.AdminRefinery },
            { "MiningKiosks_", Enums.ShopType.AdminTerminal },
            { "PlatinumBay_", Enums.ShopType.PlatinumBay },
            { "FactoryLine_", Enums.ShopType.FactoryLine },
            { "GiftShop_NewBabbage", Enums.ShopType.GiftShops },
            { "NewBabbage_Spaceport", Enums.ShopType.GiftShops },
            { "Orison_CrusaderTour", Enums.ShopType.GiftShops },
            { "Orison_Spaceport", Enums.ShopType.GiftShops },
            { "RestStop_Bars", Enums.ShopType.Bar },
            { "Voyager", Enums.ShopType.Stanton },

            { "Aparelli_", Enums.ShopType.Aparelli },
            { "TammanyAndSons_", Enums.ShopType.TammanyAndSons },
            { "KCTrending_", Enums.ShopType.KCTrending },
            { "GrandBarterBazaar_", Enums.ShopType.GrandBarter },
            { "Centermass_", Enums.ShopType.Centermass },
            { "ConscientiousObjects_", Enums.ShopType.ConscientiousObjects },
            { "Cordrys_", Enums.ShopType.Cordrys },
            { "Skutters_", Enums.ShopType.Skutters },
            { "CubbyBlast_", Enums.ShopType.CubbyBlast },
            { "GarrityDefense_", Enums.ShopType.GarrityDefense },
            { "OmegaPro_", Enums.ShopType.OmegaPro },
            { "DumpersDepot_", Enums.ShopType.DumpersDepot },
            { "CousinCrows_", Enums.ShopType.CousinCrows },

            { "NewDeal", Enums.ShopType.NewDeal },
            { "AstroArmada", Enums.ShopType.AstroArmada },
            { "TeachsShipShop", Enums.ShopType.Teachs},

            { "ShipWeapons", Enums.ShopType.ShipWeapons },
            { "HDShowcase", Enums.ShopType.HDShowcase },
            { "CargoOffice", Enums.ShopType.CargoOffice },
            { "Technotic", Enums.ShopType.Technotic },

            { "CafeMusain", Enums.ShopType.CafeMusain },
            { "G-Loc", Enums.ShopType.GLoc },
            { "Wallys", Enums.ShopType.Wallys },
            { "Old38", Enums.ShopType.Old38},
            { "MVBar", Enums.ShopType.MVBar },
            { "Twyns", Enums.ShopType.Twyns },
            { "Ellroys", Enums.ShopType.Ellroys },
            { "Whammers", Enums.ShopType.Whammers },
            { "GarciaGreens", Enums.ShopType.GarciaGreens },
            { "Survival", Enums.ShopType.Food },
            { "BurritoBar", Enums.ShopType.BurritoBar },
            { "HotDogBar", Enums.ShopType.HotdogBar },
            { "NoodleBar", Enums.ShopType.NoodleBar },
            { "PizzaBar", Enums.ShopType.PizzaBar },
            { "RaceBar", Enums.ShopType.RacingBar },
            { "CoffeeStand", Enums.ShopType.CoffeToGo },
            { "Makau", Enums.ShopType.Makau },

            { "JuiceBar", Enums.ShopType.JuiceBar },
            { "Generic_FPSArmor", Enums.ShopType.FPSArmor },
            { "AdminOffice", Enums.ShopType.CargoOffice },
            { "_Hospital", Enums.ShopType.Pharmacy },
            { "KelTo_Food", Enums.ShopType.KelTo },
            { "Covalex-", Enums.ShopType.Covalex },
        };

        static Dictionary<Guid, Enums.ShopType> ShopLayoutNodeMapping = new Dictionary<Guid, Enums.ShopType>()
        {
            //Stores
            {new Guid("e24f84ae-53d7-4951-8f19-53ea3f3a241c"), Enums.ShopType.Casaba},
            {new Guid("2481a219-7334-4234-8724-49183ccbc350"), Enums.ShopType.DumpersDepot},
            {new Guid("8bd04065-f941-4a7d-b83a-ab3ed0059747"), Enums.ShopType.LiveFireWeapons},
            {new Guid("89b98102-1623-4ae9-860c-dd4989c910b9"), Enums.ShopType.GarrityDefense},
            {new Guid("594d2c53-7b39-48d0-9c4d-61d2d90cce35"), Enums.ShopType.KCTrending},
            {new Guid("e16c30f8-f109-4a97-838b-f39284f76a1c"), Enums.ShopType.CubbyBlast},
            {new Guid("6f898c56-e2e7-4431-8671-8c5cc88de92a"), Enums.ShopType.Cordrys},
            {new Guid("4d755298-4336-49d0-be27-9f21e437907d"), Enums.ShopType.Skutters},
            {new Guid("bb55f657-791b-4123-8da7-8624e52a673b"), Enums.ShopType.ConscientiousObjects},
            {new Guid("760e5ab9-2324-4102-88b5-de67c37a453f"), Enums.ShopType.CryAstro},
            {new Guid("9760e13a-b79e-4216-b5cb-33250b02d2d5"), Enums.ShopType.PlatinumBay},
            {new Guid("3ec4cc25-d189-4c57-9d5a-083a27d90549"), Enums.ShopType.Centermass},
            {new Guid("084b5abe-ff45-4b64-9d69-a6bac12c7a5d"), Enums.ShopType.TammanyAndSons},
            {new Guid("defdc55a-8c3c-467e-b6df-2257b6781606"), Enums.ShopType.CommExTransfers},
            {new Guid("9b10eafe-aa46-4159-855a-0c686abf6596"), Enums.ShopType.OmegaPro},
            {new Guid("8fb83a0a-2d13-4210-b934-75d22e4bb30c"), Enums.ShopType.ShubinInterstellar},
            {new Guid("552aa705-0caa-4f5c-93b1-92af32ce1589"), Enums.ShopType.ShipWeapons},//Station ship weapons


            //Unknown
            {new Guid("de926c7f-acca-488d-a453-a98cd036c647"), Enums.ShopType.Other }, //OrisonProvidenceSurplus?
            {new Guid("71a61e24-8272-425e-8713-a49c4273b617"), Enums.ShopType.CrusaderShowroom }, //CrusaderShowroom
            {new Guid("51d4de0c-1c81-401e-86c7-4a408411edb2"), Enums.ShopType.CrusaderShowroom }, //CrusaderShowroom_Orison
            {new Guid("f0f830dd-4e72-4de6-a5a6-bea74f758181"), Enums.ShopType.CrusaderShowroom}, //CrusaderShowroomWeaponry_Orison
            {new Guid("3e994d9d-53c5-4708-bd1c-6fbc90279381"), Enums.ShopType.Ignore }, //BestInShow_Kiosk


            //Other
            {new Guid("1fe30b00-44e6-4e01-aac8-06ae0c53cbcf"), Enums.ShopType.GrandBarter},
            {new Guid("04b1098e-5d92-46d2-a268-f545639eea5b"), Enums.ShopType.Technotic},
            {new Guid("188b2eff-dbd9-4b40-b4e0-c2233f5a5d0c"), Enums.ShopType.LandingServices},

            //Terminals
            {new Guid("f8e36354-cf43-4944-99e0-7841dbc398cb"), Enums.ShopType.AdminTerminal}, //Admin Office
            {new Guid("76f9f50c-dc05-454c-8a8b-2c3b177d6f1d"), Enums.ShopType.AdminTerminal}, //Storage
            {new Guid("84df979f-ddd2-4593-be4a-9df6a46f4e63"), Enums.ShopType.AdminTerminal}, //Mining
            {new Guid("d266ef13-d2ce-4e24-a3d0-85617125a220"), Enums.ShopType.AdminTerminal}, //Farming
            {new Guid("81e8613f-abdf-4761-ac32-c05cbe98e1c5"), Enums.ShopType.AdminTerminal}, //Research
            {new Guid("48eba9f2-986e-4de8-a235-9c7bcff742e4"), Enums.ShopType.AdminRefinery}, //Refinery
            {new Guid("667c1cc4-fa33-4071-9475-ff7cf7ca08ff"), Enums.ShopType.FenceTerminal}, //Drugs
            {new Guid("948147bc-a886-43b6-b739-aab76f4dbb9a"), Enums.ShopType.FenceTerminal}, //Fence
            
            //Ships
            {new Guid("826f79d4-e8a5-4509-a1cf-6355d752822a"), Enums.ShopType.TravelerRentals},
            {new Guid("b998f9a9-8eb4-40fe-955a-b7b6729205c7"), Enums.ShopType.VantageRentals},
            {new Guid("27b1e354-838f-43af-a9f0-ab410ebfc02b"), Enums.ShopType.RegalLuxury},
            {new Guid("40ebd0cb-8fbf-4a5a-9033-a127352ffd08"), Enums.ShopType.FPSArmor},
            {new Guid("43e85efc-8736-4441-a9ba-e9c50a9931f3"), Enums.ShopType.Aparelli},

            //Food
            {new Guid("12d2b6dc-4b09-4df2-8369-f796340a9df6"), Enums.ShopType.BurritoBar},
            {new Guid("98cf6436-09f7-46d0-b6a2-642b1c12337b"), Enums.ShopType.HotdogBar},
            {new Guid("380c2951-c9d1-4fdc-a7a9-c5499f5b10d8"), Enums.ShopType.Bar},
            {new Guid("84c2be5f-a3e4-4713-9939-26bfa902a9ac"), Enums.ShopType.KelTo},
            {new Guid("27296b85-f987-4a2e-a260-696c2db3b918"), Enums.ShopType.Food},
            {new Guid("f7306af5-d479-49bc-9398-6c9415e0658f"), Enums.ShopType.ConvenienceStore},
            {new Guid("355e1333-0ae4-42ae-b6c6-f3627a093af7"), Enums.ShopType.Pharmacy}
        };
    }

    public class SubsumptionShop
    {
        internal SubsumptionShop(Enums.ShopType ShopType, ShopLayoutNode Shop)
        {
            this.ShopType = ShopType;
            this.Shop = Shop;
        }

        public Enums.ShopType ShopType { get; }
        public ShopLayoutNode Shop { get; }
    }

    public class ShopLayoutNode
    {
        [XmlAttribute]
        public Guid ID { get; set; }
        [XmlAttribute]
        public int Index { get; set; }
        [XmlAttribute]
        public string Name { get; set; }
        [XmlAttribute]
        public string AutoGenerated { get; set; }

        public ShopLayoutNode Parent { get; set; }
        public ShopLayoutNode[] ShopLayoutNodes { get; set; }
        public ShopInventoryNode[] ShopInventoryNodes { get; set; }
    }

    public class ShopInventoryNode
    {
        [XmlAttribute]
        public Guid ID { get; set; }
        [XmlAttribute]
        public int Index { get; set; }
        [XmlAttribute]
        public string Name { get; set; }
        [XmlAttribute]
        public string AutoGenerated { get; set; }
        [XmlAttribute]
        public Guid InventoryID { get; set; }
        [XmlAttribute]
        public float BasePriceOffsetPercentage { get; set; }
        [XmlAttribute]
        public float MaxDiscountPercentage { get; set; }
        [XmlAttribute]
        public float MaxPremiumPercentage { get; set; }
        [XmlAttribute]
        public float Inventory { get; set; }
        [XmlAttribute]
        public float OptimalInventoryLevel { get; set; }
        [XmlAttribute]
        public string AutoRestock { get; set; }
        [XmlAttribute]
        public string AutoConsume { get; set; }
        [XmlAttribute]
        public float MaxInventory { get; set; }
        [XmlAttribute]
        public float RefreshRatePercentagePerMinute { get; set; }

        public string TransactionType { get { return TransactionTypes.FirstOrDefault()?.Data; } }

        [XmlArray(ElementName = "TransactionTypes")]
        [XmlArrayItem(ElementName = "TransactionType")]
        public XMLTransactionType[] TransactionTypes { get; set; }

        [XmlArrayItem("ID")]
        public RentalTemplate[] RentalTemplates { get; set; }

        public class RentalTemplate
        {
            [XmlText]
            public string Data { get; set; }
        }

        public class XMLTransactionType
        {
            [XmlText]
            public string Data { get; set; }
        }
    }
}
