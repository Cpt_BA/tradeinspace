﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Text;
using System.Xml.Serialization;
using TradeInSpace.Explore.Subsumption;

namespace TradeInSpace.Explore
{
    using DN = System.DoubleNumerics;

    [DebuggerDisplay("Vec3: ({X}, {Y}, {Z})")]
    [Serializable]
    public class Vector3
    {
        public Vector3()
        {
            X = Y = Z = 0;
        }
        public Vector3(double X, double Y, double Z)
        {
            this.X = X;
            this.Y = Y;
            this.Z = Z;
        }

        public Vector3((double X, double Y, double Z) pos)
        {
            this.X = pos.X;
            this.Y = pos.Y;
            this.Z = pos.Z;
        }

        public Vector3((float X, float Y, float Z) pos)
        {
            this.X = pos.X;
            this.Y = pos.Y;
            this.Z = pos.Z;
        }

        [XmlAttribute("X")]
        public double X { get; set; }
        [XmlAttribute("Y")]
        public double Y { get; set; }
        [XmlAttribute("Z")]
        public double Z { get; set; }

        public static readonly Vector3 Zero = new Vector3();
        public static readonly Vector3 One = new Vector3(1, 1, 1);
        public static readonly Vector3 UnitX = new Vector3(1, 0, 0);
        public static readonly Vector3 UnitY = new Vector3(0, 1, 0);
        public static readonly Vector3 UnitZ = new Vector3(0, 0, 1);

        public static Vector3 operator +(Vector3 a, Vector3 b) => Add(a, b);
        public static Vector3 operator -(Vector3 a, Vector3 b) => Subtract(a, b);
        public static Vector3 operator *(Vector3 a, Vector3 b) => Multiply(a, b);
        public static Vector3 operator *(Vector3 a, double value) => new Vector3(a.X * value, a.Y * value, a.Z * value);
        public static Vector3 operator /(Vector3 a, double value) => new Vector3(a.X / value, a.Y / value, a.Z / value);

        public Vector3 Abs()
        {
            return new Vector3(
                Math.Abs(X),
                Math.Abs(Y),
                Math.Abs(Z));
        }

        public static Vector3 Min(Vector3 A, Vector3 B)
        {
            return new Vector3(
                Math.Min(A.X, B.X),
                Math.Min(A.Y, B.Y),
                Math.Min(A.Z, B.Z));
        }

        public static Vector3 Max(Vector3 A, Vector3 B)
        {
            return new Vector3(
                Math.Max(A.X, B.X),
                Math.Max(A.Y, B.Y),
                Math.Max(A.Z, B.Z));
        }

        public static Vector3 Add(Vector3 A, Vector3 B)
        {
            return new Vector3(A.X + B.X, A.Y + B.Y, A.Z + B.Z);
        }

        public static Vector3 Subtract(Vector3 A, Vector3 B)
        {
            return new Vector3(A.X - B.X, A.Y - B.Y, A.Z - B.Z);
        }

        public static Vector3 Multiply(Vector3 A, Vector3 B)
        {
            return new Vector3(A.X * B.X, A.Y * B.Y, A.Z * B.Z);
        }

        public static implicit operator Vector3((double X, double Y, double Z) a) => new Vector3(a.X, a.Y, a.Z);
        public static implicit operator Vector3(DN.Vector3 a) => new Vector3(a.X, a.Y, a.Z);
        public static explicit operator DN.Vector3(Vector3 a) => new DN.Vector3(a.X, a.Y, a.Z);


        public static Vector3 FromString(string posString, Vector3 fallback = null)
        {
            if (string.IsNullOrWhiteSpace(posString)) return (fallback ?? Vector3.Zero);
            var parts = posString.Split(',');
            if (parts.Length != 3) return (fallback ?? Vector3.Zero);
            return new Vector3(
                double.Parse(parts[0], CultureInfo.InvariantCulture), 
                double.Parse(parts[1], CultureInfo.InvariantCulture), 
                double.Parse(parts[2], CultureInfo.InvariantCulture));
        }
        public static Vector3 FromTuple((double X, double Y, double Z) pos)
        {
            return new Vector3(pos);
        }

        public override string ToString()
        {
            return FormattableString.Invariant($"{X},{Y},{Z}");
        }
    }



    public class AARect3
    {
        public Vector3 Min { get; set; } = null;
        public Vector3 Max { get; set; } = null;

        public AARect3()
        {
        }

        public AARect3(Vector3 Center)
        {
            Min = Max = Center;
        }

        public void Ensure()
        {
            if (Min == null) Min = Vector3.Zero;
            if (Max == null) Max = Vector3.Zero;
        }

        public void AddPoint(Vector3 position)
        {
            if (position == null) return;

            Min = Vector3.Min(Min ?? position, position);
            Max = Vector3.Max(Max ?? position, position);
        }

        public void AddBounds(AARect3 bounds)
        {
            AddPoint(bounds.Min);
            AddPoint(bounds.Max);
        }
    }
}
