﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Xml.Serialization;

namespace TradeInSpace.Explore
{
    using DN = System.DoubleNumerics;

    [DebuggerDisplay("Vec4: ({X}, {Y}, {Z}, {W})")]
    [Serializable]
    public class Vector4
    {
        public static readonly Vector4 Identity = new Vector4(0, 0, 0, 1);

        public Vector4()
        {
            X = Y = Z = 0;
            W = 1;
        }
        public Vector4(double X, double Y, double Z, double W)
        {
            this.X = X;
            this.Y = Y;
            this.Z = Z;
            this.W = W;
        }

        [XmlAttribute("X")]
        public double X { get; set; }
        [XmlAttribute("Y")]
        public double Y { get; set; }
        [XmlAttribute("Z")]
        public double Z { get; set; }
        [XmlAttribute("W")]
        public double W { get; set; }

        public static implicit operator Vector4(DN.Quaternion a) => new Vector4(a.X, a.Y, a.Z, a.W);
        public static explicit operator DN.Quaternion(Vector4 a) => new DN.Quaternion(a.X, a.Y, a.Z, a.W);
        public static explicit operator Vector4(DN.Vector4 v) => new Vector4(v.X, v.Y, v.Z, v.W);

        public static Vector4 FromString(string rotString, bool WFirst = true)
        {
            if (rotString == null) return Vector4.Identity;
            var parts = rotString.Split(',');
            if (parts.Length != 4) return Vector4.Identity;
            if (WFirst)
                return new Vector4(double.Parse(parts[1]), double.Parse(parts[2]), double.Parse(parts[3]), double.Parse(parts[0]));
            else
                return new Vector4(double.Parse(parts[0]), double.Parse(parts[1]), double.Parse(parts[2]), double.Parse(parts[3]));
        }

        public override string ToString()
        {
            return $"{X},{Y},{Z},{W}";
        }

        public Vector4((float, float, float, float, float, float, float, float, float) RotationMatrix)
        {
            float trace = RotationMatrix.Item1 + RotationMatrix.Item5 + RotationMatrix.Item9; // I removed + 1.0f; see discussion with Ethan
            if (trace > 0)
            {// I changed M_EPSILON to 0
                float s = (float)(0.5f / Math.Sqrt(trace + 1.0f));
                W = 0.25f / s;
                X = (RotationMatrix.Item8 - RotationMatrix.Item6) * s;
                Y = (RotationMatrix.Item3 - RotationMatrix.Item7) * s;
                Z = (RotationMatrix.Item4 - RotationMatrix.Item2) * s;
            }
            else
            {
                if (RotationMatrix.Item1 > RotationMatrix.Item5 && RotationMatrix.Item1 > RotationMatrix.Item9)
                {
                    float s = (float)(2.0f * Math.Sqrt(1.0f + RotationMatrix.Item1 - RotationMatrix.Item5 - RotationMatrix.Item9));
                    W = (RotationMatrix.Item8 - RotationMatrix.Item6) / s;
                    X = 0.25f * s;
                    Y = (RotationMatrix.Item2 + RotationMatrix.Item4) / s;
                    Z = (RotationMatrix.Item3 + RotationMatrix.Item7) / s;
                }
                else if (RotationMatrix.Item5 > RotationMatrix.Item9)
                {
                    float s = (float)(2.0f * Math.Sqrt(1.0f + RotationMatrix.Item5 - RotationMatrix.Item1 - RotationMatrix.Item9));
                    W = (RotationMatrix.Item3 - RotationMatrix.Item7) / s;
                    X = (RotationMatrix.Item2 + RotationMatrix.Item4) / s;
                    Y = 0.25f * s;
                    Z = (RotationMatrix.Item6 + RotationMatrix.Item8) / s;
                }
                else
                {
                    float s = (float)(2.0f * Math.Sqrt(1.0f + RotationMatrix.Item9 - RotationMatrix.Item1 - RotationMatrix.Item5));
                    W = (RotationMatrix.Item4 - RotationMatrix.Item2) / s;
                    X = (RotationMatrix.Item3 + RotationMatrix.Item7) / s;
                    Y = (RotationMatrix.Item6 + RotationMatrix.Item8) / s;
                    Z = 0.25f * s;
                }
            }
        }

        public Vector4((double, double, double, double, double, double, double, double, double) RotationMatrix)
        {
            double trace = RotationMatrix.Item1 + RotationMatrix.Item5 + RotationMatrix.Item9; // I removed + 1.0f; see discussion with Ethan
            if (trace > 0)
            {// I changed M_EPSILON to 0
                double s = (0.5 / Math.Sqrt(trace + 1.0));
                W = 0.25 / s;
                X = (RotationMatrix.Item8 - RotationMatrix.Item6) * s;
                Y = (RotationMatrix.Item3 - RotationMatrix.Item7) * s;
                Z = (RotationMatrix.Item4 - RotationMatrix.Item2) * s;
            }
            else
            {
                if (RotationMatrix.Item1 > RotationMatrix.Item5 && RotationMatrix.Item1 > RotationMatrix.Item9)
                {
                    double s = 2.0 * Math.Sqrt(1.0 + RotationMatrix.Item1 - RotationMatrix.Item5 - RotationMatrix.Item9);
                    W = (RotationMatrix.Item8 - RotationMatrix.Item6) / s;
                    X = 0.25 * s;
                    Y = (RotationMatrix.Item2 + RotationMatrix.Item4) / s;
                    Z = (RotationMatrix.Item3 + RotationMatrix.Item7) / s;
                }
                else if (RotationMatrix.Item5 > RotationMatrix.Item9)
                {
                    double s = 2.0 * Math.Sqrt(1.0 + RotationMatrix.Item5 - RotationMatrix.Item1 - RotationMatrix.Item9);
                    W = (RotationMatrix.Item3 - RotationMatrix.Item7) / s;
                    X = (RotationMatrix.Item2 + RotationMatrix.Item4) / s;
                    Y = 0.25 * s;
                    Z = (RotationMatrix.Item6 + RotationMatrix.Item8) / s;
                }
                else
                {
                    double s = 2.0 * Math.Sqrt(1.0 + RotationMatrix.Item9 - RotationMatrix.Item1 - RotationMatrix.Item5);
                    W = (RotationMatrix.Item4 - RotationMatrix.Item2) / s;
                    X = (RotationMatrix.Item3 + RotationMatrix.Item7) / s;
                    Y = (RotationMatrix.Item6 + RotationMatrix.Item8) / s;
                    Z = 0.25 * s;
                }
            }
        }
    }
}
