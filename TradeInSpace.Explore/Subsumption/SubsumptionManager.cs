﻿using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using TradeInSpace.Explore.DataForge;
using TradeInSpace.Explore.Socpak;

namespace TradeInSpace.Explore.Subsumption
{
    public class SubsumptionManager
    {
        public ShopLayouts ShopInventories { get; set; } = null;
        public LayerZero LayerZero { get; set; } = null;
        public DFDatabase RawGameData { get; private set; }
        public List<SubsumptionRoot> Systems { get; private set; }
        public List<SubsumptionNode> AllNodes { get; set; } = new List<SubsumptionNode>();

        public SOC_Entry MasterRoot { get; private set; }


        public static readonly string[] SystemArchives = new[]
        {
            @"Data\Libs\Subsumption\Platforms\PU\System\Stanton\stantonsystem.xml"
        };

        public SubsumptionManager()
        {
            MasterRoot = SOC_Entry.CreateRoot("TIS-Parse_Root");
        }

        public void Attach(DFDatabase rawGameDB)
        {
            RawGameData = rawGameDB;

            if(ShopInventories == null)
            {
                ShopInventories = ShopLayouts.LoadFromGameDB(RawGameData);
                LayerZero = LayerZero.LoadFromGameDB(RawGameData);
            }
        }

        public void LoadAllSystems()
        {
            if (Systems != null) return;

            Systems = SystemArchives
                .Select(a => SubsumptionRoot.LoadFromGameDB(a, RawGameData, false))
                .Where(s_root => s_root != null)
                .ToList();
        }

        public void LoadFrom(SubsumptionNode subNode, bool PreloadDetails = true, bool PreloadEntities = true)
        {
            HandleChild(subNode, null, 
                PreloadDetails: PreloadDetails, PreloadEntities: PreloadEntities);
        }

        public void LoadAllChildren(bool PreloadDetails = true, bool PreloadEntities = true, bool PreloadArchives = true, bool LoadChildren = true, Func<SubsumptionNode, int, bool> fnShouldExplore = null)
        {
            if (AllNodes.Count() > 0 || Systems == null) return;

            foreach (var system in Systems)
            {
                var systemArchive = SOC_Archive.LoadFromGameDBSocPak(RawGameData, $"ObjectContainers/{system.DataFile.Data}", LoadChildren: LoadChildren, Parent: MasterRoot);
                if (systemArchive == null)
                    throw new Exception("Archive is null.");

                foreach(var child in system.Children)
                {
                    AllNodes.AddRange(HandleChild(child, systemArchive.RootEntry, parentDepth: 0,
                        PreloadDetails: PreloadDetails, PreloadEntities: PreloadEntities, PreloadArchives: PreloadArchives, 
                        fnShouldExplore: fnShouldExplore));
                }
            }

            LayerZero.LoadLayoutMappings((slm) =>
            {
                return ShopInventories.Shops.Any(s => s.Shop.ID == slm.LayoutGUID);// && AllNodes.Any(n => n.SuperGUID == slm.ShopSuperGUID);
            });
        }

        private IEnumerable<SubsumptionNode> HandleMultipleChildren(IEnumerable<SubsumptionNode> subNodes, SOC_Entry parentEntry,
            int parentDepth = 0, bool parallel = false, bool PreloadDetails = true, bool PreloadEntities = true, bool PreloadArchives = true)
        {
            if (!parallel)
            {
                foreach (var child in subNodes)
                {
                    foreach (var subChild in HandleChild(child, parentEntry,
                        parentDepth: parentDepth, PreloadDetails: PreloadDetails, PreloadEntities: PreloadEntities, PreloadArchives: PreloadArchives))
                    {
                        yield return subChild;
                    }
                }
            }
            else
            {
                Dictionary<long, List<SubsumptionNode>> nodeResults = new Dictionary<long, List<SubsumptionNode>>(subNodes.Count());

                Parallel.ForEach(subNodes, (child, _, i) =>
                {
                    nodeResults[i] = HandleChild(child, parentEntry,
                        parentDepth: parentDepth, PreloadDetails: PreloadDetails, PreloadEntities: PreloadEntities, PreloadArchives: PreloadArchives).ToList();
                });

                for (int i = 0; i < nodeResults.Count; i++)
                    foreach (var result in nodeResults[i])
                        yield return result;
            }
        }

        private IEnumerable<SubsumptionNode> HandleChild(SubsumptionNode node, SOC_Entry parentEntry, int parentDepth = 0,
            bool PreloadDetails = true, bool PreloadEntities = true, bool PreloadArchives = true, Func<SubsumptionNode, int, bool> fnShouldExplore = null)
        {
            if (parentEntry != null)
            {
                Log.Verbose($"Exploring {node.Name} [{node.Type}]");

                Func<SOC_Entry, bool> fnSearchNode = (c) => c.GUID.ToString() == node.GUID && node.SuperGUID.EndsWith(c.GUID.ToString());

                if(PreloadDetails)
                    parentEntry.LoadFullDetails();
                if(PreloadEntities)
                    parentEntry.LoadAllEntities(false);

                if (node.IsContainerLink)// && node.Filename != null)
                {
                    //Primary search for object containers from subsumption to socpak hierarchy
                    //Name and GUID match
                    var matchingContainer = parentEntry.Children.SingleOrDefault(fnSearchNode);

                    if (matchingContainer == null)
                    {
                        Log.Warning($"Performing recursive search for matching SOC Entry.");
                        matchingContainer = Utilities.FlattenRecursive(parentEntry, e => e.Children).SingleOrDefault(fnSearchNode);
                    }

                    if (matchingContainer == null && PreloadArchives)
                    {

                        //Load directly from archive when unable to find the appropriate link.
                        Log.Warning($"Unable to find socpac child {node.Name} in parent {parentEntry?.Name}. Loading directly from archive {node.Filename}");

                        SOC_Archive childArchive = SOC_Archive.LoadFromSubsubmption(RawGameData, node, parentEntry);
                        if (childArchive == null)
                        {
                            //Log.Warning($"Child archive is null. - {node.Filename}");
                            yield break;
                        }
                        node.SOC_Entry = childArchive.RootEntry;
                    }


                    node.SOC_Entry = matchingContainer;
                }
                else
                {

                    switch (node.Type)
                    {
                        case "PlatformObjectContainerLink":
                        case "Object"://Root-level object
                        case "ProceduralEntity":
                        case "InteractiveObject":
                        case "Usable - MissionGiver":
                        case "Usable - Guard":
                        case "Usable - Counter":
                        case "Usable - Table":
                        case "Usable - Seat":
                        case "Usable - Bed":
                        case "Usable - NPC":
                        case "Usable - TEMP_Archetype":
                        case "ActionArea":
                        case "TrackView":
                        case "NPC":
                        case "Shop"://TODO: anything?
                                    //TODO: Relate to entity from parent
                        case "Refinery":
                            break;
                        default:
                            Log.Warning($"Unknown type: {node.Type} on child {node.Name}");
                            //Still process
                            break;
                    }

                    node.SOC_Entry = parentEntry.Children.SingleOrDefault(c => node.SuperGUID.EndsWith(c.GUID.ToString()));

                }
            }

            //Actually return self and children for enumeration
            yield return node;

            //(Optionally) Determine if we need to continue exploring recursively for this node
            if (fnShouldExplore != null && fnShouldExplore(node, parentDepth + 1) == false)
                yield break;

            foreach (var child in HandleMultipleChildren(node.Children, node.SOC_Entry, 
                parentDepth: parentDepth + 1, PreloadDetails: PreloadDetails, PreloadEntities: PreloadEntities, PreloadArchives: PreloadArchives))
            {
                yield return child;
            }
        }


        public SubsumptionShop GetShopLayout(Guid ID)
        {
            return ShopInventories.Shops.SingleOrDefault(s => s.Shop.ID == ID);
        }

        public ShopLayoutMapping LookupShopProfile(SOC_Entry Parent_SOC, FullEntity ShopEntity)
        {
            var matchingSubNode = AllNodes.SingleOrDefault(n => n.SOC_Entry == ShopEntity.Owner);
            if (matchingSubNode == null) return null;

            var flatNodes = Utilities.FlattenRecursive(matchingSubNode, n => n.Children);
            var subsumpMatches = flatNodes.Where(c => c.GUID == ShopEntity.GUID.ToString()).ToList();
            if (subsumpMatches.Count == 0) return null;

            var subsumpShop = subsumpMatches.First();
            var shopMatch = LayerZero.LayoutMap.SingleOrDefault(lm => lm.ShopSuperGUID == subsumpShop.SuperGUID);

            //var parentSub = string.Join(",", subsumpShop.SuperGUID.Split(',').Reverse().Skip(1).Reverse());
            //var parentTest = LayerZero.LayoutMap.Where(lm => lm.ShopSuperGUID.StartsWith(parentSub)).ToList();
            
            return shopMatch;
        }

        public ShopLayoutMapping LookupShopProfile(SOC_Entry MatchEntry)
        {
            //Second level of matching??
            var directMatch = LayerZero.LayoutMap.SingleOrDefault(lm => lm.ShopSuperGUID == MatchEntry.SuperGUID);
            if (directMatch != null) return directMatch;

            var matchingSubNode = AllNodes.SingleOrDefault(n => n.SOC_Entry == MatchEntry);
            if (matchingSubNode == null) return null;

            var indirectMatch = LayerZero.LayoutMap.SingleOrDefault(lm => lm.ShopSuperGUID == matchingSubNode.SuperGUID);
            return indirectMatch;
        }

        public ShopLayoutMapping LookupShopProfileExact(string ShopEntitySuperGUID)
        {
            return LayerZero.LayoutMap.SingleOrDefault(lm => lm.ShopSuperGUID == ShopEntitySuperGUID);
        }
    }
}