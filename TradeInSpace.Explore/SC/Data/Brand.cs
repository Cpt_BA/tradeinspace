﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using TradeInSpace.Explore.DataForge;

namespace TradeInSpace.Explore.SC.Data
{
    [DFEntry("ItemKioskBrand")]
    public class Brand : DFEntry
    {
        public string Name { get; private set; }
        public string LogoPath { get; private set; }

        public Brand(XmlElement fromElement, DFDatabase gameDatabase) : base(fromElement, gameDatabase)
        {
            Name = fromElement.GetAttribute("name");
            LogoPath = fromElement.GetAttribute("logoPath");
        }
    }
}
