﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using TradeInSpace.Explore.DataForge;

namespace TradeInSpace.Explore.SC.Data
{
    [DFEntry("ShipInsuranceRecord")]
    public class ShipInsurance : DFEntry
    {
        public List<ShipInsuranceRecord> Ships = new List<ShipInsuranceRecord>();

        public ShipInsurance(XmlElement fromElement, DFDatabase gameDatabase) : base(fromElement, gameDatabase)
        {
            Ships = fromElement.SelectNodes("./allShips/ShipInsuranceParams")
                .OfType<XmlElement>()
                .Select(e => ShipInsuranceRecord.FromXML(e))
                .ToList();
        }
    }

    public class ShipInsuranceRecord
    {
        public string ShipClass { get; private set; }
        public float BaseWaitTimeMinutes { get; private set; }
        public float ExpeditedWaitTimeMinutes { get; private set; }
        public int ExpediteFee { get; private set; }

        public static ShipInsuranceRecord FromXML(XmlElement element)
        {
            return new ShipInsuranceRecord()
            {
                ShipClass = element.GetAttribute("shipEntityClassName"),
                BaseWaitTimeMinutes = float.Parse(element.GetAttribute("baseWaitTimeMinutes")),
                ExpeditedWaitTimeMinutes = float.Parse(element.GetAttribute("mandatoryWaitTimeMinutes")),
                ExpediteFee = int.Parse(element.GetAttribute("baseExpeditingFee"))
            };
        }
    }
}
