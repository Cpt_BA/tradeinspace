﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Xml;
using TradeInSpace.Explore.DataForge;
using TradeInSpace.Explore.SC.Components;

namespace TradeInSpace.Explore.SC.Data
{
    [DFEntry("ResourceTypeGroup")]
    public class ResourceTypeGroup : DFEntry
    {
        public string Name { get; private set; }
        public string Description { get; private set; }

        public List<Guid> GroupIDs { get; set; }
        public List<Guid> TypeIDs { get; set; }
        public List<ResourceTypeGroup> Groups { get; set; }
        public List<ResourceType> Resources { get; set; }

        public ResourceTypeGroup(XmlElement fromElement, DFDatabase gameDatabase) : base(fromElement, gameDatabase)
        {
            Name = gameDatabase.Localize(fromElement.GetAttribute("displayName"));
            Description = gameDatabase.Localize(fromElement.GetAttribute("description"));

            GroupIDs = fromElement["groups"].ChildNodes
                .Cast<XmlElement>()
                .Select(xe => Guid.Parse(xe.GetAttribute("value")))
                .ToList();
            TypeIDs = fromElement["resources"].ChildNodes
                .Cast<XmlElement>()
                .Select(xe => Guid.Parse(xe.GetAttribute("value")))
                .ToList();
        }

        internal override void OnDFEntriesCompleted()
        {
            Groups = GroupIDs.Select(g => GameDatabase.GetEntry<ResourceTypeGroup>(g)).ToList();
            Resources = TypeIDs.Select(t => GameDatabase.GetEntry<ResourceType>(t)).ToList();
            Resources.ForEach(r => r.Category = this);

            base.OnDFEntriesCompleted();
        }
    }

    [DFEntry("ResourceType")]
    public class ResourceType : DFEntry
    {
        public string Name { get; private set; }
        public string Description { get; private set; }
        public Guid RefinesToGUID { get; private set; }

        public SCCommodity CommodityEntity { get; private set; }
        public ResourceTypeGroup Category { get; set; }
        public ResourceType RefinesTo { get; private set; } = null;

        public ResourceType(XmlElement fromElement, DFDatabase gameDatabase) : base(fromElement, gameDatabase)
        {
            Name = gameDatabase.Localize(fromElement.GetAttribute("displayName"));
            Description = gameDatabase.Localize(fromElement.GetAttribute("description"));
            RefinesToGUID = Guid.Parse(fromElement.GetAttribute("refinedVersion"));
            //TODO: Add support for enumerating box sizes available, 1/8-32 SCU
        }

        internal override void OnDFEntriesCompleted()
        {
            base.OnDFEntriesCompleted();
            CommodityEntity = GameDatabase.GetComponentsOnEntities<SCCommodity>()
                //Look for the "pure" commodity based on the one with only 2 components (CommodityComponentParams/SCItemPurchasableParams)
                .Where(ie => ie.ParentEntity.Components.Count == 2 && ie.ParentEntity.Components.Any(c => c.Type == "SCItemPurchasableParams"))
                .SingleOrDefault(ie => ie.SubtypeID == this.ID);

            if (RefinesToGUID != Guid.Empty)
                RefinesTo = GameDatabase.GetEntry<ResourceType>(RefinesToGUID);
        }
    }

    [DFEntry("ResourceTypeDatabase")]
    public class ResourceDatabase : DFEntry
    {
        public List<Guid> GroupIDs { get; set; }
        public List<ResourceTypeGroup> Groups { get; set; }

        public List<ResourceType> AllResources { get; set; }

        public ResourceDatabase(XmlElement fromElement, DFDatabase gameDatabase) : base(fromElement, gameDatabase)
        {
            GroupIDs = fromElement["groups"].ChildNodes
                .Cast<XmlElement>()
                .Select(xe => Guid.Parse(xe.GetAttribute("value")))
                .ToList();
        }

        internal override void OnDFEntriesCompleted()
        {
            Groups = GroupIDs.Select(g => GameDatabase.GetEntry<ResourceTypeGroup>(g)).ToList();
            
            var resources = new HashSet<ResourceType>();
            void EnumerateGroup(ResourceTypeGroup g)
            {
                foreach (var c in g.Groups) EnumerateGroup(c);
                foreach (var r in g.Resources) resources.Add(r);
            };
            foreach(var group in Groups) EnumerateGroup(group);

            AllResources = new List<ResourceType>(resources);

            base.OnDFEntriesCompleted();
        }
    }
}
