﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using TradeInSpace.Explore.DataForge;

namespace TradeInSpace.Explore.SC.Data
{
    [DFEntry("InventoryContainer")]
    [JsonObject(MemberSerialization.OptIn)]
    public class InventoryContainer : DFEntry
    {
        [JsonProperty]
        public Vector3 Interior { get; set; }
        [JsonProperty]
        public bool IsOpen { get; set; }

        //Open-Only properties
        [JsonProperty]
        public float GridSize { get; set; }
        [JsonProperty]
        public bool IsExternal { get; set; }
        [JsonProperty]
        public float MaxPercentageDestroyed { get; set; }
        [JsonProperty]
        public Vector3 MinSize { get; set; }
        [JsonProperty]
        public Vector3 MaxSize { get; set; }
        [JsonProperty]
        public float uSCUCapacity { get; set; }


        public InventoryContainer(XmlElement fromElement, DFDatabase gameDatabase)
            : base(fromElement, gameDatabase)
        {
        }

        internal override void OnDFEntriesCompleted()
        {
            try
            {
                Interior = SCUtils.ReadVector3(Element.SelectSingleNode("./interiorDimensions") as XmlElement);

                var inventory = Element.SelectSingleNode("./inventoryType/*") as XmlElement;

                if (inventory.Name == "InventoryOpenContainerType" ||
                    inventory.Name == "InventoryOpenAlwaysContainerType")
                {
                    IsOpen = true;

                    GridSize = float.Parse(inventory.GetAttribute("gridCellSize"));
                    IsExternal = inventory.GetAttribute("isExternalContainer") == "1";
                    MaxPercentageDestroyed = float.Parse(inventory.GetAttribute("maxPercentageErasedOnParentDestruction")) / 100.0f;

                    MinSize = SCUtils.ReadVector3(inventory.SelectSingleNode("./minPermittedItemSize") as XmlElement);
                    MaxSize = SCUtils.ReadVector3(inventory.SelectSingleNode("./maxPermittedItemSize") as XmlElement);

                    uSCUCapacity = SCUtils.CalculateCargoGridSCU(Interior, GridSize) * (int)CommoditySizes_uSCU.SCU;
                }
                else if (inventory.Name == "InventoryClosedContainerType")
                {
                    IsOpen = false;

                    uSCUCapacity = (int)SCUtils.ReadCargoUnitAsMicroSCU(inventory.SelectSingleNode("./capacity/*") as XmlElement);
                }
                else
                {
                    throw new NotSupportedException($"Unknown InventoryContainer type: {inventory.Name}");
                }
            }
            catch (Exception ex)
            {

            }
        }
    }
}
