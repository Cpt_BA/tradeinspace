﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using TradeInSpace.Explore.DataForge;

namespace TradeInSpace.Explore.SC.Data
{
    [DFEntry("AmmoParams")]
    [JsonObject(MemberSerialization.OptIn)]
    public class AmmoParams : DFEntry
    {
        [JsonProperty]
        public int Size { get; private set; }
        [JsonProperty]
        public float Lifetime { get; private set; }
        [JsonProperty]
        public int BulletType { get; private set; }
        [JsonProperty]
        public float Speed { get; private set; }
        [JsonProperty]
        public string HitType { get; private set; }
        [JsonProperty]
        public float MinRadius { get; private set; }
        [JsonProperty]
        public float MaxRadius { get; private set; }

        [JsonProperty]
        public DamageInfo BaseDamage { get; private set; }
        [JsonProperty]
        public DamageInfo FalloffMinDistance { get; private set; }
        [JsonProperty]
        public DamageInfo FalloffPerMeter { get; private set; }
        [JsonProperty]
        public DamageInfo FalloffMinDamage { get; private set; }

        [JsonProperty]
        public DamageInfo ExplosionDamage { get; private set; }

        public AmmoParams(XmlElement fromElement, DFDatabase gameDatabase) : base(fromElement, gameDatabase)
        {
            Size = int.Parse(fromElement.GetAttribute("size"));
            Lifetime = float.Parse(fromElement.GetAttribute("lifetime"));
            BulletType = int.Parse(fromElement.GetAttribute("bulletType"));
            Speed = int.Parse(fromElement.GetAttribute("speed"));

            var bulletParams = fromElement.SelectSingleNode("./projectileParams/BulletProjectileParams") as XmlElement;
            if (bulletParams != null)
            {
                MinRadius = float.Parse(bulletParams.GetAttribute("minImpactRadius"));
                MaxRadius = float.Parse(bulletParams.GetAttribute("impactRadius"));
                HitType = bulletParams.GetAttribute("hitType");

                BaseDamage = bulletParams.SelectSingleNode("./damage/DamageInfo").Deserialize<DamageInfo>();


                var dropInfo = bulletParams.SelectSingleNode("./damageDropParams/BulletDamageDropParams");
                if (dropInfo != null)
                {
                    FalloffMinDistance = dropInfo.SelectSingleNode("./damageDropMinDistance/DamageInfo").Deserialize<DamageInfo>();
                    FalloffPerMeter = dropInfo.SelectSingleNode("./damageDropPerMeter/DamageInfo").Deserialize<DamageInfo>();
                    FalloffMinDamage = dropInfo.SelectSingleNode("./damageDropMinDamage/DamageInfo").Deserialize<DamageInfo>();
                }

                
                ExplosionDamage = bulletParams.SelectSingleNode("./detonationParams/ProjectileDetonationParams/explosionParams/damage/DamageInfo")?.Deserialize<DamageInfo>();
            }

        }

        internal override void OnDFEntriesCompleted()
        {
            base.OnDFEntriesCompleted();

            var tachyonParams = Element.SelectSingleNode("./projectileParams/TachyonProjectileParams") as XmlElement;

            if (tachyonParams != null)
            {
                //These dont *100%* map correctly, but it works...
                MinRadius = float.Parse(tachyonParams.GetAttribute("fullDamageRange"));
                MaxRadius = float.Parse(tachyonParams.GetAttribute("zeroDamageRange"));
                HitType = tachyonParams.GetAttribute("hitType");

                var tachyonDamage = tachyonParams["damage"]["DamageParams"];

                var macroTotal = float.Parse(tachyonDamage.GetAttribute("damageTotal"));
                var macroID = Guid.Parse(tachyonDamage.GetAttribute("damageMacro"));

                BaseDamage = this.GameDatabase.GetEntry<DamageMacro>(macroID).Damage.Multiply(macroTotal);
            }
        }
    }

    [JsonObject(MemberSerialization.OptIn)]
    public class DamageInfo
    {
        [XmlAttribute("DamagePhysical")]
        [JsonProperty]
        public float Physical { get; set; }
        [XmlAttribute("DamageEnergy")]
        [JsonProperty]
        public float Energy { get; set; }
        [XmlAttribute("DamageDistortion")]
        [JsonProperty]
        public float Distortion { get; set; }
        [XmlAttribute("DamageThermal")]
        [JsonProperty]
        public float Thermal { get; set; }
        [XmlAttribute("DamageBiochemical")]
        [JsonProperty]
        public float Biochemical { get; set; }
        [XmlAttribute("DamageStun")]
        [JsonProperty]
        public float Stun { get; set; }

        public static DamageInfo ResistanceFromXML(XmlElement DamageResistanceXML)
        {
            return new DamageInfo()
            {
                Physical = float.Parse(DamageResistanceXML.SelectSingleNode("PhysicalResistance/@Multiplier").Value),
                Energy= float.Parse(DamageResistanceXML.SelectSingleNode("EnergyResistance/@Multiplier").Value),
                Distortion = float.Parse(DamageResistanceXML.SelectSingleNode("DistortionResistance/@Multiplier").Value),
                Thermal = float.Parse(DamageResistanceXML.SelectSingleNode("ThermalResistance/@Multiplier").Value),
                Biochemical = float.Parse(DamageResistanceXML.SelectSingleNode("BiochemicalResistance/@Multiplier").Value),
                Stun = float.Parse(DamageResistanceXML.SelectSingleNode("StunResistance/@Multiplier").Value),
            };
        }

        public static DamageInfo FromMacroXML(XmlElement InfoElement)
        {
            return new DamageInfo()
            {
                Physical = float.Parse(InfoElement.SelectSingleNode("damageInfo/@DamagePhysical").Value) / 100.0f,
                Energy = float.Parse(InfoElement.SelectSingleNode("damageInfo/@DamageEnergy").Value) / 100.0f,
                Distortion = float.Parse(InfoElement.SelectSingleNode("damageInfo/@DamageDistortion").Value) / 100.0f,
                Thermal = float.Parse(InfoElement.SelectSingleNode("damageInfo/@DamageThermal").Value) / 100.0f,
                Biochemical = float.Parse(InfoElement.SelectSingleNode("damageInfo/@DamageBiochemical").Value) / 100.0f,
                Stun = float.Parse(InfoElement.SelectSingleNode("damageInfo/@DamageStun").Value) / 100.0f,
            };
        }

        public DamageInfo Multiply(float Amount)
        {
            return new DamageInfo()
            {
                Physical = Physical * Amount,
                Energy = Energy * Amount,
                Distortion = Distortion * Amount,
                Thermal = Thermal * Amount,
                Biochemical = Biochemical * Amount,
                Stun = Stun * Amount
            };
        }
    }
}
