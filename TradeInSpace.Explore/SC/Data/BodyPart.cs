﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using TradeInSpace.Explore.DataForge;

namespace TradeInSpace.Explore.SC.Data
{
    [DFEntry("BodyPart")]
    public class BodyPart : DFEntry
    {
        public string Name { get; private set; }

        public BodyPart(XmlElement fromElement, DFDatabase gameDatabase) : base(fromElement, gameDatabase)
        {
            switch (this.InstanceName)
            {
                case "head": Name = "Head"; break;
                case "leftArm": Name = "Left Arm"; break;
                case "leftLeg": Name = "Left Leg"; break;
                case "rightArm": Name = "Right Arm"; break;
                case "rightLeg": Name = "Right Leg"; break;
                case "torso": Name = "Torso"; break;
            }
        }
    }
}
