﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using TradeInSpace.Explore.DataForge;

namespace TradeInSpace.Explore.SC.Data
{
    //SReputationMissionRewardListParams


    [DFEntry("MissionOrganization", ParsePriority = 90)]
    public class MissionOrganization : DFEntry
    {
        public List<Guid> Tags { get; set; } = new List<Guid>();

        public Dictionary<string, List<string>> StringVariants { get; set; } = new Dictionary<string, List<string>>();

        public MissionOrganization(XmlElement fromElement, DFDatabase gameDatabase) : base(fromElement, gameDatabase)
        {
            foreach (XmlAttribute tag in Element.SelectNodes("./organizationTags/MissionLocationTags/tags/Reference/@value"))
            {
                Tags.Add(Guid.Parse(tag.Value));
            }
        }

        internal override void OnDFEntriesCompleted()
        {
            foreach (XmlElement stringVariant in Element.SelectNodes("./stringVariants/variants/MissionStringVariant"))
            {
                var variantName = GameDatabase.ConvertTags(stringVariant.GetAttribute("tag"));
                var value = GameDatabase.Localize(stringVariant.GetAttribute("string"));

                if (!StringVariants.ContainsKey(variantName))
                    StringVariants[variantName] = new List<string>();

                StringVariants[variantName].Add(value);
            }
        }
    }

    [DFEntry("ReputationValueSetting", ParsePriority = 90)]
    public class ReputationValueSetting : DFEntry
    {
        public string Name { get; set; }
        public float Value { get; set; }

        public ReputationValueSetting(XmlElement fromElement, DFDatabase gameDatabase) : base(fromElement, gameDatabase)
        {
            Name = fromElement.GetAttribute("name");
            Value = float.Parse(fromElement.GetAttribute("value"));
        }
    }

    [DFEntry("SReputationScopeParams", ParsePriority = 90)]
    public class SReputationScopeParams : DFEntry
    {
        public string Name { get; set; }
        public string DisplayName { get; set; }
        public string Description { get; set; }

        public float StartingValue { get; set; }
        public float MaxValue { get; set; }

        public string IconPath { get; set; }

        public List<SReputationStandingParams> StandingLevels { get; set; }

        List<Guid> _standingLevels;

        public SReputationScopeParams(XmlElement fromElement, DFDatabase gameDatabase) : base(fromElement, gameDatabase)
        {
            Name = fromElement.GetAttribute("scopeName");
            DisplayName = gameDatabase.Localize(fromElement.GetAttribute("displayName"));
            Description = gameDatabase.Localize(fromElement.GetAttribute("description"));
            IconPath = fromElement.GetAttribute("icon");

            var standingMap = Element["standingMap"];

            StartingValue = float.Parse(standingMap.GetAttribute("initialReputation"));
            MaxValue = float.Parse(standingMap.GetAttribute("reputationCeiling"));
            
            _standingLevels = standingMap
                .SelectNodes("./standings/Reference/@value")
                .OfType<XmlAttribute>()
                .Select(a => Guid.Parse(a.Value))
                .ToList();
        }

        internal override void OnDFEntriesCompleted()
        {
            StandingLevels = _standingLevels
                .Select(sl => this.GameDatabase.GetEntry<SReputationStandingParams>(sl))
                .ToList();
        }
    }

    [DFEntry("SReputationStandingParams", ParsePriority = 90)]
    public class SReputationStandingParams : DFEntry
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string DisplayName { get; set; }
        public string DisplayDescription { get; set; }
        public float MinReputation { get; set; }
        public float DriftReputation { get; set; }
        public float DriftHours { get; set; }
        public bool Gated { get; set; }


        public SReputationStandingParams(XmlElement fromElement, DFDatabase gameDatabase) : base(fromElement, gameDatabase)
        {
            Name = Element.GetAttribute("name");
            Description = Element.GetAttribute("description");
            DisplayName = GameDatabase.Localize(Element.GetAttribute("displayName"));
            DisplayDescription = GameDatabase.Localize(Element.GetAttribute("displayDescription"));

            MinReputation = float.Parse(Element.GetAttribute("minReputation"));
            DriftReputation = float.Parse(Element.GetAttribute("driftReputation"));
            DriftHours = float.Parse(Element.GetAttribute("driftTimeHours"));

            Gated = Element.GetAttribute("gated") == "1";
        }
    }

    [DFEntry("SReputationRewardAmount", ParsePriority = 90)]
    public class SReputationRewardAmountParams : DFEntry
    {
        public string Name { get; set; }
        public float Amount { get; set; }

        public SReputationRewardAmountParams(XmlElement fromElement, DFDatabase gameDatabase) : base(fromElement, gameDatabase)
        {
            Name = Element.GetAttribute("editorName");
            Amount = float.Parse(Element.GetAttribute("reputationAmount"));
        }
    }

    [DFEntry("SReputationContextUI", ParsePriority = 90)]
    public class SReputationContextUIParams : DFEntry
    {
        public string Name { get; set; }
        public SReputationScopeParams PrimaryScope { get; set; }
        public List<SReputationScopeParams> ListedScopes { get; set; }

        public SReputationContextUIParams(XmlElement fromElement, DFDatabase gameDatabase) : base(fromElement, gameDatabase)
        {
            Name = Element.GetAttribute("editorName");
        }

        internal override void OnDFEntriesCompleted()
        {
            var primaryScopeGUID = Guid.Parse(Element.SelectSingleNode("./primaryScopeContext/@scope").Value);
            PrimaryScope = GameDatabase.GetEntry<SReputationScopeParams>(primaryScopeGUID);

            ListedScopes = Element.SelectNodes("./scopeContextList/SReputationScopeContextUI/@scope")
                .OfType<XmlAttribute>()
                .Select(rs => GameDatabase.GetEntry<SReputationScopeParams>(Guid.Parse(rs.Value)))
                .ToList();
        }
    }

    [DFEntry("SReputationMissionAmountsListParams", ParsePriority = 90)]
    public class SReputationMissionAmountsListParams : DFEntry
    {
        public List<SReputationMissionGiverAmountListParams> ReputationAmounts { get; set; } = new List<SReputationMissionGiverAmountListParams>();

        public SReputationMissionAmountsListParams(XmlElement fromElement, DFDatabase gameDatabase) : base(fromElement, gameDatabase)
        {
            ReputationAmounts = fromElement
                .SelectNodes("./missionGiverAmounts/SReputationMissionGiverAmountListParams")
                .DeserializeBulk<SReputationMissionGiverAmountListParams>()
                .ToList();
        }

        internal override void OnDFEntriesCompleted()
        {
            foreach(var repAmount in ReputationAmounts)
            {
                repAmount.Entity = GameDatabase.GetEntity(repAmount.EntityGUID);

                foreach(var repRecipient in repAmount.RewardTargets)
                {
                    repRecipient.Entity = GameDatabase.GetEntity(repRecipient.EntityGUID);
                    repRecipient.Scope = GameDatabase.GetEntry<SReputationScopeParams>(repRecipient.ReputationScopeID);
                }
            }
        }
    }

    [DFEntry("SReputationMissionRewardListParams", ParsePriority = 90)]
    public class SReputationMissionRewardListParams : DFEntry
    {
        public List<SReputationMissionAmountsListParams> RewardAmounts { get; set; }

        public SReputationMissionRewardListParams(XmlElement fromElement, DFDatabase gameDatabase) : base(fromElement, gameDatabase)
        {
            
        }

        internal override void OnDFEntriesCompleted()
        {
            var rewardIDs = Element.SelectNodes("./missionResultReputationRewards/Reference/@value")
                .OfType<XmlAttribute>().Select(xa => Guid.Parse(xa.Value));

            RewardAmounts = rewardIDs.Select(rewardID => GameDatabase.GetEntry<SReputationMissionAmountsListParams>(rewardID)).ToList();
        }
    }

    public class SReputationMissionGiverAmountListParams
    {
        [XmlAttribute("missionGiverEntityClass")]
        public Guid EntityGUID { get; set; }
        [XmlArray(ElementName = "reputationAmounts")]
        [XmlArrayItem(ElementName = "SReputationAmountParams")]
        public SReputationAmountParams[] RewardTargets { get; set; }

        [XmlIgnore]
        public SCEntityClassDef Entity { get; set; }
    }

    public class SReputationAmountParams
    {
        [XmlAttribute("uniqueEntityClass")]
        public Guid EntityGUID { get; set; }
        [XmlAttribute("reputationScope")]
        public Guid ReputationScopeID { get; set; }
        [XmlAttribute("reputationAmount")]
        public float Amount { get; set; }

        [XmlIgnore]
        public SCEntityClassDef Entity { get; set; }
        [XmlIgnore]
        public SReputationScopeParams Scope { get; set; }
    }
}
