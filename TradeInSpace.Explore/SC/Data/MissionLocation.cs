﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using TradeInSpace.Explore.DataForge;
using TradeInSpace.Explore.SC.Params;

namespace TradeInSpace.Explore.SC.Data
{
    [DFEntry("MissionLocation", ParsePriority = 50)]
    public class MissionLocation : DFEntry
    {
        public MissionLocationParams LocationParams { get; private set; }

        public MissionLocation(XmlElement fromElement, DFDatabase gameDatabase)
            : base(fromElement, gameDatabase)
        {
        }

        internal override void OnDFEntriesCompleted()
        {
            LocationParams = MissionLocationParams.FromXML(Element, GameDatabase);
        }
    }
}
