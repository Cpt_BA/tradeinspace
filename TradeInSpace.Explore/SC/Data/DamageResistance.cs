﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using TradeInSpace.Explore.DataForge;

namespace TradeInSpace.Explore.SC.Data
{
    [DFEntry("DamageResistanceMacro")]
    public class DamageResistance : DFEntry
    {
        public DamageInfo Resistance { get; private set; }

        public DamageResistance(XmlElement fromElement, DFDatabase gameDatabase) : base(fromElement, gameDatabase)
        {
            Resistance = DamageInfo.ResistanceFromXML(fromElement["damageResistance"]);
        }
    }
}
