﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Xml;
using TradeInSpace.Explore.DataForge;

namespace TradeInSpace.Explore.SC.Data
{
    public class DFEntry : DFBase
    {
        internal DFEntry(XmlElement fromElement, DFDatabase gameDatabase) : base(fromElement)
        {
            ID = Guid.Parse(fromElement.GetAttribute("__ref"));
            InstanceName = fromElement.Name.Replace(this.Type, "").TrimStart('.');
            GameDatabase = gameDatabase;
        }

        public Guid ID { get; set; }
        public string InstanceName { get; set; }
        public DFDatabase GameDatabase { get; }
    }

    [AttributeUsage(AttributeTargets.Class)]
    public class DFEntryAttribute : Attribute
    {
        public string TypeName { get; set; }
        public int ParsePriority { get; set; } = 100;

        private static List<(Type Class, DFEntryAttribute Entry)> LoadedEntries;

        public DFEntryAttribute(string typeName)
        {
            TypeName = typeName;
        }

        public static IEnumerable<(Type Class, DFEntryAttribute Entry)> FindAllLoaded()
        {
            if (LoadedEntries == null)
            {
                LoadedEntries = new List<(Type Class, DFEntryAttribute Entry)>();
                var domainTypes = AppDomain.CurrentDomain.GetAssemblies().SelectMany(a =>
                {
                    try
                    {
                        return a.GetTypes();
                    }
                    catch (Exception ex)
                    {
                        return new Type[] { };
                    }
                });
                foreach (Type type in domainTypes)
                {
                    var matchingAttr = type.GetCustomAttribute(typeof(DFEntryAttribute), true) as DFEntryAttribute;
                    if (matchingAttr != null)
                    {
                        LoadedEntries.Add((type, matchingAttr));
                    }
                }
            }

            return LoadedEntries;
        }
    }
}
