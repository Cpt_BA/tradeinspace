﻿using Serilog;
using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using TradeInSpace.Explore.DataForge;
using TradeInSpace.Models;

namespace TradeInSpace.Explore.SC.Data
{
    [DFEntry("StarMapObject")]
    public class StarmapObject : DFEntry
    {
        public string Name { get; private set; }
        public string Description { get; private set; }
        public Guid AffiliationGUID { get; private set; }
        public Guid JurisdictionGUID { get; private set; }
        public Guid TypeGUID { get; private set; }
        public string NavIcon { get; private set; }
        public bool IsScanable { get; private set; }
        public string Size { get; private set; }
        public bool HiddenInStarmap { get; private set; }
        public bool ShowOrbit { get; private set; }
        public bool HasQuantum { get; private set; }
        public float ObstructionRadius { get; private set; }
        public float ArrivalRadius { get; private set; }
        public float AcceptableTravelRadius { get; private set; }
        public float ArrivalPointDetectionOffset { get; private set; }
        public float AdoptionRadius { get; private set; }

        public StarmapObjectType ObjectType { get; private set; }

        public StarmapObject(XmlElement fromElement, DFDatabase gameDatabase) : base(fromElement, gameDatabase)
        {
            AffiliationGUID = Guid.Parse(fromElement.GetAttribute("affiliation"));
            JurisdictionGUID = Guid.Parse(fromElement.GetAttribute("jurisdiction"));
            TypeGUID = Guid.Parse(fromElement.GetAttribute("type"));

            NavIcon = fromElement.GetAttribute("navIcon");
            IsScanable = fromElement.GetAttribute("isScannable") == "1";
            Size = fromElement.GetAttribute("size");
            HiddenInStarmap = fromElement.GetAttribute("hideInStarmap") == "1";
            ShowOrbit = fromElement.GetAttribute("showOrbitLine") == "1";

            var qtData = fromElement["quantumTravelData"]["StarMapQuantumTravelDataParams"];
            HasQuantum = qtData != null;
            if (HasQuantum)
            {
                ObstructionRadius = float.Parse(qtData.GetAttribute("obstructionRadius"));
                ArrivalRadius = float.Parse(qtData.GetAttribute("arrivalRadius"));
                AcceptableTravelRadius = float.Parse(string.IsNullOrEmpty(qtData.GetAttribute("acceptableTravelRadius")) ? "0" : qtData.GetAttribute("acceptableTravelRadius"));
                ArrivalPointDetectionOffset = float.Parse(qtData.GetAttribute("arrivalPointDetectionOffset"));
                AdoptionRadius = float.Parse(qtData.GetAttribute("adoptionRadius"));
            }

            //This is here because hidden locations have no name entries...
            //even though there are entries in the translation file

            var rawName = fromElement.GetAttribute("name");
            var rawDescription = fromElement.GetAttribute("description");

            if (rawName == "@LOC_UNINITIALIZED")
            {
                switch (InstanceName)
                {
                    //Hurston
                    case "Stanton1_JunkSite_ReclamationAndDisposalOrinth":
                        rawName = "@Stanton1_SalvageYard_001"; //Reclamation And Disposal Orinth
                        rawDescription = "@Stanton1_SalvageYard_001_desc";
                        break;

                    //Orison
                    case "Stanton2_Orison":
                        rawName = "@Stanton2_Orison";
                        rawDescription = "@Stanton2_Orison_Desc";
                        break;

                    case "CargoDepot_Stanton2_LEO":
                        rawName = "@stanton2_Shipping_Covalex";
                        rawDescription = "";
                        break;

                    //Crusader
                    //Cellin
                    case "Stanton2a_Stash_Unknown_001":
                        rawName = "@Stanton2a_Stash_001"; //Private Property
                        rawDescription = "@Stanton2a_Stash_001_desc";
                        break;
                    case "Stanton2a_AbandonedOutpost_Unknown_001":
                        //Actually unknown, no translation record for this place
                        break;
                    //Daymar
                    case "Stanton2b_JunkSite_BriosBreakerYard":
                        rawName = "@Stanton2b_SalvageYard_001"; //Brios Breaker
                        rawDescription = "@Stanton2b_SalvageYard_001_desc";
                        break;
                    case "Stanton2b_Stash_NuenWasteManagement":
                        rawName = "@Stanton2b_Stash_001"; //Nuen Waste
                        rawDescription = "@Stanton2b_Stash_001_desc";
                        break;
                    case "Stanton2b_AbandonedOutpost_Unknown_001":
                        //Actually unknown, no translation record for this place
                        break;
                    //Yela
                    case "Stanton2c_BennyHenge":
                        //yayayayayaya
                        break;
                    case "Stanton2c_DrugLab_Jumptown":
                        rawName = "@Stanton2c_DrugLab_001"; //Jumptown
                        rawDescription = "@Stanton2c_DrugLab_001_desc";
                        break;
                    case "Stanton2c_Stash_Unknown_001":
                        rawName = "@Stanton2c_Stash_001"; //NT-999-XX
                        rawDescription = "@Stanton2c_Stash_001_desc";
                        break;
                    case "Stanton2c_AbandonedOutpost_Unknown_001":
                        //Actually unknown, no translation record for this place
                        break;


                    //ArcCorp
                    //Lyria
                    case "Stanton3a_DrugLab_ParadiseCove":
                        rawName = "@Stanton3a_DrugLab_001"; //Paradise Cove
                        rawDescription = "@Stanton3a_DrugLab_001_desc";
                        break;
                    case "Stanton3a_Stash_TheOrphanage":
                        rawName = "@Stanton3a_Stash_001"; //The Orphanage
                        rawDescription = "@Stanton3a_Stash_001_desc";
                        break;
                    //Wala
                    case "Stanton3b_JunkSite_SamsonSonsSalvageCenter":
                        rawName = "@Stanton3b_SalvageYard_001"; //Samson Sons Salvage
                        rawDescription = "@Stanton3b_SalvageYard_001_desc";
                        break;
                    case var rr when rr.StartsWith("RR_"):
                    case var t when t.EndsWith("_TEMPLATE"):
                    case var m when m.StartsWith("MISSION_"):
                    case "YouAreHereMarker":
                    case "SpecialEvent":
                        break;
                    default:
                        break;
                }
            }

            if(rawName == "@LOC_UNINITIALIZED")
            {
                Log.Warning($"Name unintialized for starmap instance name {InstanceName}, reverting to instance name.");
                Name = InstanceName;
                Description = GameDatabase.Localize(rawDescription);
            }
            else
            {
                Name = GameDatabase.Localize(rawName);
                Description = GameDatabase.Localize(rawDescription);
            }
        }

        internal override void OnDFEntriesCompleted()
        {
            base.OnDFEntriesCompleted();

            ObjectType = GameDatabase.GetEntry<StarmapObjectType>(this.TypeGUID);
        }
    }

    [DFEntry("StarMapObjectType")]
    public class StarmapObjectType : DFEntry
    {
        public string Name { get; private set; }
        //public Enums.LocationType LocationType { get; private set; }

        public StarmapObjectType(XmlElement fromElement, DFDatabase gameDatabase) : base(fromElement, gameDatabase)
        {
            Name = fromElement.GetAttribute("name");
            //LocationType = (Enums.LocationType)Enum.Parse(typeof(Enums.LocationType), Name);
        }
    }
}
