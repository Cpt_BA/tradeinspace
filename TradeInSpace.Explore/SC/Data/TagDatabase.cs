﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using TradeInSpace.Explore.DataForge;

namespace TradeInSpace.Explore.SC.Data
{
    [DFEntry("TagDatabase", ParsePriority = 1)]
    public class TagDatabase : DFEntry
    {
        public List<Guid> TagIDs { get; private set; }
        public List<Tag> Tags { get; private set; }
        public List<Tag> RootTags { get; private set; }

        public TagDatabase(XmlElement fromElement, DFDatabase gameDatabase) : base(fromElement, gameDatabase)
        {
        }

        internal override void OnDFEntriesCompleted()
        {
            //Would use the actual values supplied but they are hierarchical.
            //So let the tag class link that up, and just enumarate all tags here.
            Tags = GameDatabase.GetEntries<Tag>().ToList();
            TagIDs = Tags.Select(t => t.ID).ToList();

            RootTags = Element
                .SelectNodes("./tags/Reference/@value")
                .Cast<XmlAttribute>()
                .Select(xa => GameDatabase.GetEntry<Tag>(Guid.Parse(xa.Value)))
                .ToList();

            GameDatabase.Tags = this;
        }

        public Guid GetTagID(string TagName, string ParentTag = null)
        {
            if(ParentTag != null)
                return Tags.SingleOrDefault(t => t.TagName == TagName && t.Parent?.TagName == ParentTag)?.ID ?? Guid.Empty;
            else
                return Tags.SingleOrDefault(t => t.TagName == TagName)?.ID ?? Guid.Empty;
        }

        public Tag GetTag(Guid Tag)
        {
            return Tags.SingleOrDefault(t => t.ID == Tag);
        }

        public Tag GetTag(string TagGUID)
        {
            return GetTag(Guid.Parse(TagGUID));
        }

        public string ConvertTags(string tagString)
        {
            var matchingTags = tagString
                .Split(new char[] { ',', ';' }, StringSplitOptions.RemoveEmptyEntries)
                .Select(t => GetTag(t)?.TagName ?? t);

            return string.Join(",", matchingTags);
        }
    }

    [DFEntry("Tag")]
    public class Tag : DFEntry
    {
        public string TagName { get; set; }
        public List<Tag> Children { get; set; }
        public Tag Parent { get; set; }

        public Tag(XmlElement fromElement, DFDatabase gameDatabase)
            : base(fromElement, gameDatabase)
        {
            TagName = fromElement.GetAttribute("tagName");
        }

        internal override void OnDFEntriesCompleted()
        {
            Children = Element
                .SelectNodes("./children/Reference/@value")
                .Cast<XmlAttribute>()
                .Select(xa => GameDatabase.GetEntry<Tag>(Guid.Parse(xa.Value)))
                .ToList();

            Children.ForEach(c => c.Parent = this);
        }
    }
}
