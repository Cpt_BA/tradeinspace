﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using TradeInSpace.Explore.DataForge;
using TradeInSpace.Explore.SC.Components;

namespace TradeInSpace.Explore.SC.Data
{
    [DFEntry("MissionItem", ParsePriority = 50)]
    public class MissionItem : DFEntry
    {
        public ResourceType Commodity { get; private set; }

        public Guid ItemEntityGUID { get; private set; }
        public SCEntityClassDef ItemEntitiy { get; private set; }
        public List<Guid> Tags { get; set; } = new List<Guid>();
        public Dictionary<string, List<string>> StringVariants { get; private set; } = new Dictionary<string, List<string>>();

        public MissionItem(XmlElement fromElement, DFDatabase gameDatabase) : base(fromElement, gameDatabase)
        {
            ItemEntityGUID = Guid.Parse(Element.GetAttribute("entityClass"));
            Tags.AddRange(Element.SelectNodes("./tags/Reference/@value")
                .OfType<XmlAttribute>().Select(e => Guid.Parse(e.Value)));
        }

        internal override void OnDFEntriesCompleted()
        {
            ItemEntitiy = GameDatabase.GetEntity(ItemEntityGUID);

            if (ItemEntitiy != null && ItemEntitiy.HasComponent<SCCommodity>())
            {
                var commodityComponent = ItemEntitiy.GetComponent<SCCommodity>();
                Commodity = commodityComponent.Commodity;
            }

            foreach (XmlElement stringVariant in Element.SelectNodes("./stringVariants/variants/MissionStringVariant"))
            {
                var variantName = GameDatabase.ConvertTags(stringVariant.GetAttribute("tag"));
                var value = GameDatabase.Localize(stringVariant.GetAttribute("string"));

                if (!StringVariants.ContainsKey(variantName))
                    StringVariants[variantName] = new List<string>();

                StringVariants[variantName].Add(value);
            }
        }
    }
}
