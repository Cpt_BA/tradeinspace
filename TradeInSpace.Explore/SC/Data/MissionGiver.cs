﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Xml;
using TradeInSpace.Explore.DataForge;

namespace TradeInSpace.Explore.SC.Data
{
    [DFEntry("MissionGiver")]
    public class MissionGiver : DFEntry
    {
        public Guid EntityGUID { get; set; }

        public string Name { get; set; }
        public string Description { get; set; }

        public byte[] IconData { get; set; }

        public int InviteTimeout { get; set; }
        public int VisitTimeout { get; set; }
        public int ShortCooldown { get; set; }
        public int MediumCooldown { get; set; }
        public int LongCooldown { get; set; }

        public List<string> ListedScopes { get; set; }

        public MissionGiver(XmlElement fromElement, DFDatabase gameDatabase) : base(fromElement, gameDatabase)
        {
            EntityGUID = Guid.Parse(Element.GetAttribute("entityClass"));

            Name = gameDatabase.Localize(Element.GetAttribute("displayName"));
            Description = gameDatabase.Localize(Element.GetAttribute("description"));

            InviteTimeout = int.Parse(Element.GetAttribute("invitationTimeout"));
            VisitTimeout = int.Parse(Element.GetAttribute("visitTimeout"));
            ShortCooldown = int.Parse(Element.GetAttribute("shortCooldown")); 
            MediumCooldown = int.Parse(Element.GetAttribute("mediumCooldown"));
            LongCooldown = int.Parse(Element.GetAttribute("longCooldown"));
        }

        internal override void OnDFEntriesCompleted()
        {
            var entity = GameDatabase.GetEntity(this.EntityGUID);
            var entityData = entity.Element.SelectSingleNode("./StaticEntityClassData");
            
            var entityOrgData = entityData.SelectSingleNode("./SOrganizationProperties") as XmlElement; //Optional
            var entityUIDataGUID = Guid.Parse(entityData.SelectSingleNode("./SReputationContextPropertiesUI/@context").Value); //Should always be present

            if (entityOrgData != null)
            {
                Name = GameDatabase.Localize(entityOrgData.GetAttribute("DisplayName"));
                Description = GameDatabase.Localize(entityOrgData.GetAttribute("Description"));
                IconData = GameDatabase.ReadPackedFileBinary(entityOrgData.GetAttribute("Logo"));
            }

            var uiParams = GameDatabase.GetEntry<SReputationContextUIParams>(entityUIDataGUID);
            if(uiParams.ListedScopes.Count > 0)
            {
                ListedScopes = uiParams.ListedScopes
                    .Select(s => s.Name)
                    .ToList();
            }
        }
    }
}
