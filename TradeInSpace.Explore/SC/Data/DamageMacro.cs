﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using TradeInSpace.Explore.DataForge;

namespace TradeInSpace.Explore.SC.Data
{
    [DFEntry("DamageMacro")]
    public class DamageMacro : DFEntry
    {
        public DamageInfo Damage { get; private set; }

        public DamageMacro(XmlElement fromElement, DFDatabase gameDatabase) : base(fromElement, gameDatabase)
        {
            Damage = DamageInfo.FromMacroXML(Element);
        }
    }
}
