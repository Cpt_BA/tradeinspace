﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml;
using TradeInSpace.Explore.DataForge;

namespace TradeInSpace.Explore.SC.Data
{
    using DB = TradeInSpace.Models;

    [DFEntry("MissionBrokerEntry")]
    public class MissionGiverEntry : DFEntry
    {
        public bool SecretMission { get; set; }
        public string Title { get; set; }
        public string FullTitle { get; set; }
        public string Description { get; set; }
        public string FullDescription { get; set; }
        public Guid MissionTypeGUID { get; set; }
        public Guid LocationMissionAvailableGUID { get; set; }
        public string MissionGiverName { get; set; }

        public bool NotifyOnAvailable { get; set; }
        public bool ShowAsOffer { get; set; }
        public int MissionBuyInAmount { get; set; }
        public bool RefundBuyInOnWithdraw { get; set; }
        public bool HasCompleteButton { get; set; }
        public bool HandlesAbandonRequest { get; set; }
        public int MissionModulePerPlayer { get; set; }
        public int MaxInstances { get; set; }
        public int MaxPlayersPerInstance { get; set; }
        public int MaxInstancesPerPlayer { get; set; }
        public bool CanBeShared { get; set; }
        public bool OnceOnly { get; set; }
        public bool GrantsArrestLicense { get; set; }
        public bool AvailableInPrison { get; set; }
        public bool FailIfSentToPrison { get; set; }
        public bool RequestOnly { get; set; }
        public float RespawnTime { get; set; }
        public float RespawnTimeVariation { get; set; }
        public bool InstanceHasLifeTime { get; set; }
        public bool ShowLifeTimeInMobiGlas { get; set; }
        public float InstanceLifeTime { get; set; }
        public float InstanceLifeTimeVariation { get; set; }
        public bool CanReacceptAfterAbandoning { get; set; }
        public float AbandonedCooldownTime { get; set; }
        public float AbandonedCooldownTimeVariation { get; set; }
        public bool HasPersonalCooldown { get; set; }
        public float PersonalCooldownTime { get; set; }
        public float PersonalCooldownTimeVariation { get; set; }
        public Guid LinkedMissionGUID { get; set; }
        public bool LawfulMission { get; set; }
        public Guid MissionGiverGUID { get; set; }
        public Guid InvitationMissionGUID { get; set; }
        public string MissionGiverFragmentTags { get; set; }


        public int MinUECReward { get; set; }
        public int MaxUECReward { get; set; }
        public bool BonusReward { get; set; }

        public List<MissionRequirement> Requirements { get; set; } =
            new List<MissionRequirement>();
        public List<MissionRewardSet> RewardSets { get; set; } =
            new List<MissionRewardSet>();

        public List<Guid> MissionTags { get; set; }
        public List<Guid> CompletionTags { get; set; }

        public string MissionType { get; set; }
        public MissionGiverEntry LinkedMission { get; set; }
        public MissionGiver MissionGiver { get; set; }
        public MissionGiverEntry InvitationMission { get; set; }
        public HashSet<MissionOrganization> Organizations { get; set; } = new HashSet<MissionOrganization>();

        public Dictionary<string, List<string>> TextTokens { get; set; } = 
            new Dictionary<string, List<string>>(StringComparer.OrdinalIgnoreCase);

        public Dictionary<string, HashSet<MissionLocation>> RegisteredLocations { get; set; } = 
            new Dictionary<string, HashSet<MissionLocation>>(StringComparer.OrdinalIgnoreCase);

        public Dictionary<string, HashSet<MissionItem>> RegisteredItems { get; set; } = 
            new Dictionary<string, HashSet<MissionItem>>(StringComparer.OrdinalIgnoreCase);

        public Dictionary<string, Dictionary<string, List<string>>> RegisteredTokens { get; set; } = 
            new Dictionary<string, Dictionary<string, List<string>>>(StringComparer.OrdinalIgnoreCase);

        public MissionGiverEntry(XmlElement fromElement, DFDatabase gameDatabase) : base(fromElement, gameDatabase)
        {
            SecretMission = fromElement.GetAttribute("notForRelease") == "1";

            Title = gameDatabase.Localize(fromElement.GetAttribute("title"));
            Description = gameDatabase.Localize(fromElement.GetAttribute("description"));
            MissionTypeGUID = Guid.Parse(fromElement.GetAttribute("type"));
            LocationMissionAvailableGUID = Guid.Parse(fromElement.GetAttribute("locationMissionAvailable"));
            MissionGiverName = gameDatabase.Localize(fromElement.GetAttribute("missionGiver"));

            NotifyOnAvailable = fromElement.GetAttribute("notifyOnAvailable") == "1";
            ShowAsOffer = fromElement.GetAttribute("showAsOffer") == "1";
            MissionBuyInAmount = int.Parse(fromElement.GetAttribute("missionBuyInAmount"));
            RefundBuyInOnWithdraw = fromElement.GetAttribute("refundBuyInOnWithdraw") == "1";
            HasCompleteButton = fromElement.GetAttribute("hasCompleteButton") == "1";
            HandlesAbandonRequest = fromElement.GetAttribute("handlesAbandonRequest") == "1";
            MissionModulePerPlayer = int.Parse(fromElement.GetAttribute("missionModulePerPlayer"));

            MaxInstances = int.Parse(fromElement.GetAttribute("maxInstances"));
            MaxPlayersPerInstance = int.Parse(fromElement.GetAttribute("maxPlayersPerInstance"));
            MaxInstancesPerPlayer = int.Parse(fromElement.GetAttribute("maxInstancesPerPlayer"));
            CanBeShared = fromElement.GetAttribute("canBeShared") == "1";
            OnceOnly = fromElement.GetAttribute("onceOnly") == "1";
            GrantsArrestLicense = fromElement.GetAttribute("grantsArrestLicense") == "1";
            AvailableInPrison = fromElement.GetAttribute("availableInPrison") == "1";
            FailIfSentToPrison = fromElement.GetAttribute("failIfSentToPrison") == "1";
            RequestOnly = fromElement.GetAttribute("requestOnly") == "1";
            RespawnTime = float.Parse(fromElement.GetAttribute("respawnTime"));
            RespawnTimeVariation = float.Parse(fromElement.GetAttribute("respawnTimeVariation"));
            InstanceHasLifeTime = fromElement.GetAttribute("instanceHasLifeTime") == "1";
            ShowLifeTimeInMobiGlas = fromElement.GetAttribute("showLifeTimeInMobiGlas") == "1";
            InstanceLifeTime = float.Parse(fromElement.GetAttribute("instanceLifeTime"));
            InstanceLifeTimeVariation = float.Parse(fromElement.GetAttribute("instanceLifeTimeVariation"));
            CanReacceptAfterAbandoning = fromElement.GetAttribute("canReacceptAfterAbandoning") == "1";
            AbandonedCooldownTime = float.Parse(fromElement.GetAttribute("abandonedCooldownTime"));
            AbandonedCooldownTimeVariation = float.Parse(fromElement.GetAttribute("abandonedCooldownTimeVariation"));
            HasPersonalCooldown = fromElement.GetAttribute("hasPersonalCooldown") == "1";
            PersonalCooldownTime = float.Parse(fromElement.GetAttribute("personalCooldownTime"));
            PersonalCooldownTimeVariation = float.Parse(fromElement.GetAttribute("personalCooldownTimeVariation"));
            LinkedMissionGUID = Guid.Parse(fromElement.GetAttribute("linkedMission"));
            LawfulMission = fromElement.GetAttribute("lawfulMission") == "1";
            MissionGiverGUID = Guid.Parse(fromElement.GetAttribute("missionGiverRecord"));
            InvitationMissionGUID = Guid.Parse(fromElement.GetAttribute("invitationMission"));
            MissionGiverFragmentTags = fromElement.GetAttribute("missionGiverFragmentTags");

            MinUECReward = int.Parse(fromElement.SelectSingleNode("./missionReward/@reward").Value);
            MaxUECReward = int.Parse(fromElement.SelectSingleNode("./missionReward/@max").Value);
            BonusReward = fromElement.SelectSingleNode("./missionReward/@plusBonuses").Value == "1";

            MissionTags = fromElement.SelectNodes("./missionTags/Reference/@value")
                .OfType<XmlAttribute>().Select(a => Guid.Parse(a.Value)).ToList();
            CompletionTags = fromElement.SelectNodes("./completionTags/tags/Reference/@value")
                .OfType<XmlAttribute>().Select(a => Guid.Parse(a.Value)).ToList();
        }

        internal override void OnDFEntriesCompleted()
        {
            MissionType = GameDatabase.GetEntry<MissionType>(MissionTypeGUID, true)?.Name ?? "Unknown";

            LinkedMission = GameDatabase.GetEntry<MissionGiverEntry>(LinkedMissionGUID, true);
            MissionGiver = GameDatabase.GetEntry<MissionGiver>(MissionGiverGUID, true);
            InvitationMission = GameDatabase.GetEntry<MissionGiverEntry>(InvitationMissionGUID, true);

            CalculateTextTokens();

            FullTitle = FormatMissionString(Title);
            FullDescription = FormatMissionString(Description);
            MissionGiverName = FormatMissionString(MissionGiverName);

            var wantedReq = Element.SelectSingleNode("./reputationPrerequisites/wantedLevel") as XmlElement;
            if (wantedReq != null)
                Requirements.Add(new MissionRequirement()
                {
                    Type = DB.RequirementType.Wanted,
                    Title = "Wanted",
                    Min = float.Parse(wantedReq.GetAttribute("minValue")),
                    Max = float.Parse(wantedReq.GetAttribute("maxValue")),
                });

            var virtueReq = Element.SelectSingleNode("./reputationPrerequisites/virtue") as XmlElement;
            if (virtueReq != null)
                Requirements.Add(new MissionRequirement()
                {
                    Type = DB.RequirementType.Virtue,
                    Title = "Virtue",
                    Min = float.Parse(virtueReq.GetAttribute("minValue")),
                    Max = float.Parse(virtueReq.GetAttribute("maxValue")),
                });

            var reliabilityReq = Element.SelectSingleNode("./reputationPrerequisites/reliability") as XmlElement;
            if (reliabilityReq != null)
                Requirements.Add(new MissionRequirement()
                {
                    Type = DB.RequirementType.Reliability,
                    Title = "Reliability",
                    Min = float.Parse(reliabilityReq.GetAttribute("minValue")),
                    Max = float.Parse(reliabilityReq.GetAttribute("maxValue")),
                });

            if(InvitationMission != null)
            {
                Requirements.Add(new MissionRequirement()
                {
                    Type = DB.RequirementType.MissionInvite,
                    Title = "Mission Invite",
                    Missions = new List<MissionGiverEntry>() { InvitationMission }
                });
            }

            if (LinkedMission != null)
            {
                Requirements.Add(new MissionRequirement()
                {
                    Type = DB.RequirementType.MissionLinked,
                    Title = "Mission Link",
                    Missions = new List<MissionGiverEntry>() { LinkedMission }
                });
            }

            var missionReqA = Element.SelectSingleNode("requiredMissions") as XmlElement;
            if (missionReqA?.HasChildNodes ?? false)
            {
                var idSet = missionReqA.SelectNodes("./Reference/@value")
                    .OfType<XmlAttribute>().Select(a => Guid.Parse(a.Value));

                var matchingMissions = GameDatabase.GetEntries<MissionGiverEntry>()
                    .Where(m => idSet.Contains(m.ID)).ToList();

                if (matchingMissions.Count > 0)
                {
                    Requirements.Add(new MissionRequirement()
                    {
                        Type = DB.RequirementType.MissionAll,
                        Title = "Missions",
                        Missions = matchingMissions
                    });
                }
            }

            var missionReqB = Element.SelectSingleNode("./requiredCompletedMissionTags") as XmlElement;
            if (missionReqB?.HasChildNodes ?? false)
            {
                var positiveTags = missionReqB.SelectNodes("./TagSearchTerm/positiveTags/Reference/@value")
                    .OfType<XmlAttribute>().Select(a => Guid.Parse(a.Value)).ToList();
                var negativeTags = missionReqB.SelectNodes("./TagSearchTerm/negativeTags/Reference/@value")
                    .OfType<XmlAttribute>().Select(a => Guid.Parse(a.Value)).ToList();

                foreach(var positiveTag in positiveTags)
                {
                    var matchingPositive = GameDatabase.GetEntries<MissionGiverEntry>().Where(mge => mge.CompletionTags.Contains(positiveTag)).ToList();
                    var title = GameDatabase.ConvertTags(positiveTag.ToString()) ?? "Missions";
                    Requirements.Add(new MissionRequirement()
                    {
                        Type = DB.RequirementType.MissionAny,
                        Title = title,
                        Missions = matchingPositive
                    });
                }

                foreach(var negativeTag in negativeTags)
                {
                    var matchingNegative = GameDatabase.GetEntries<MissionGiverEntry>().Where(mge => mge.CompletionTags.Contains(negativeTag)).ToList();
                    var title = GameDatabase.ConvertTags(negativeTag.ToString()) ?? "Missions";
                    Requirements.Add(new MissionRequirement()
                    {
                        Type = DB.RequirementType.MissionNone,
                        Title = title,
                        Missions = matchingNegative
                    });
                }
            }

            var reputationReqExpression = Element.SelectSingleNode("./reputationRequirements/SReputationMissionRequirementsParams/expression");
            if(reputationReqExpression?.HasChildNodes ?? false)
            {
                var elements = reputationReqExpression.SelectNodes("./*");
                foreach(XmlElement xmlExpressionElement in elements)
                {
                    switch (xmlExpressionElement.LocalName)
                    {
                        case "SReputationMissionGiverRequirementParams":
                            var targetGUID = Guid.Parse(xmlExpressionElement.GetAttribute("missionGiverEntityClass"));
                            var targetScopeGUID = Guid.Parse(xmlExpressionElement.GetAttribute("reputationScope"));
                            var standingGUID = Guid.Parse(xmlExpressionElement.GetAttribute("standing"));

                            var targetEntity = GameDatabase.GetEntries<MissionGiver>().SingleOrDefault(mg => mg.EntityGUID == targetGUID);
                            var targetScope = GameDatabase.GetEntry<SReputationScopeParams>(targetScopeGUID);
                            var targetStanding = GameDatabase.GetEntry<SReputationStandingParams>(standingGUID);

                            float standingValue = targetStanding.MinReputation;
                            float? minValue = null, maxValue = null;
                            string repScopeName = "Reputation";

                            switch (xmlExpressionElement.GetAttribute("comparison"))
                            {
                                case "GreaterThan":
                                case "GreaterThanOrEqualTo": //ya ya, sue me.
                                    minValue = standingValue; break;
                                case "LesserThan":
                                case "LesserThanOrEqualTo":
                                    maxValue = standingValue; break;
                                case "EqualTo":
                                    minValue = maxValue = standingValue; break;
                                default: 
                                    break;
                            }

                            Requirements.Add(new MissionRequirement()
                            {
                                Type = DB.RequirementType.Standing,
                                Title = repScopeName,
                                Min = minValue,
                                Max = maxValue,
                                TargetGiver = targetEntity,
                                RepScope = Utilities.CleanKey(targetScope.Name)
                            });
                            break;
                        case "SReputationMissionRequirementExpression_And":
                            //repElements.Add(new ReputationRequirementElement(RepRequirementType.And));
                            Requirements.Add(new MissionRequirement()
                            {
                                Type = DB.RequirementType.And
                            });
                            break;
                        case "SReputationMissionRequirementExpression_Or":
                            //repElements.Add(new ReputationRequirementElement(RepRequirementType.Or));
                            Requirements.Add(new MissionRequirement()
                            {
                                Type = DB.RequirementType.Or
                            });
                            break;
                        default:
                            break;
                    }
                }


            }


            RewardSets.Clear();
            RewardSets.Add(new MissionRewardSet() { Title = "Mission Success" });
            RewardSets.Add(new MissionRewardSet() { Title = "Outcome #2" });
            RewardSets.Add(new MissionRewardSet() { Title = "Outcome #3" });

            var repRewards = Element.SelectSingleNode("./reputationRewards") as XmlElement;
            if (repRewards?.HasChildNodes ?? false)
            {
                var rewardElements = repRewards.SelectNodes("./MissionReputationRewards");

                if (rewardElements.Count == 3)
                {
                    for (int i = 0; i < 3; i++)
                    {
                        XmlElement repReward = rewardElements[i] as XmlElement;
                        var virtChange = GameDatabase.GetEntry<ReputationValueSetting>
                            (Guid.Parse(repReward.GetAttribute("virtueChange")), true);
                        var reliabilityChange = GameDatabase.GetEntry<ReputationValueSetting>
                            (Guid.Parse(repReward.GetAttribute("reliabilityChange")), true);

                        if (virtChange != null)
                            RewardSets[i].Rewards.Add(new MissionReward()
                            {
                                Type = DB.RewardType.Virtue,
                                Amount = virtChange?.Value ?? 0
                            });

                        if (virtChange != null)
                            RewardSets[i].Rewards.Add(new MissionReward()
                            {
                                Type = DB.RewardType.Reliability,
                                Amount = reliabilityChange?.Value ?? 0
                            });
                    }
                }
                else
                {

                }
            }

            var missionGiverRep = Element.HasAttribute("missionGiverReputationRewards") ? 
                Guid.Parse(Element.GetAttribute("missionGiverReputationRewards")) : Guid.Empty;

            if (missionGiverRep != Guid.Empty && MissionGiver != null)
            {
                var giverReputation = GameDatabase.GetEntry<SReputationMissionRewardListParams>(missionGiverRep);
                if (giverReputation.RewardAmounts.Count == 3)
                {
                    for (int i = 0; i < 3; i++)
                    {
                        var repAmount = giverReputation.RewardAmounts[i];
                        var giverScope = repAmount.ReputationAmounts.SingleOrDefault(r => r.EntityGUID == MissionGiver.EntityGUID);

                        if (giverScope != null)
                        {
                            foreach (var target in giverScope.RewardTargets)
                            {
                                string rewardScope = null;

                                var rewardTarget = GameDatabase.GetEntries<MissionGiver>().SingleOrDefault(mg => mg.EntityGUID == target.EntityGUID);

                                RewardSets[i].Rewards.Add(new MissionReward()
                                {
                                    Type = DB.RewardType.Standing,
                                    Amount = target.Amount,
                                    TargetEntity = rewardTarget,
                                    RepScope = Utilities.CleanKey(rewardScope)
                                });
                            }
                        }
                        else
                        {
                            //Expected to find a scope but didn't
                        }
                    }
                }
                else
                {
                    //Not 3 mission entries
                }
            }
            else
            {
                var repResultElements = Element
                    .SelectNodes("./missionResultReputationRewards/SReputationAmountListParams")
                    .OfType<XmlElement>()
                    .ToList();

                if(repResultElements?.Count == 3)
                {
                    try
                    {
                        for (int i = 0; i < 3; i++)
                        {
                            XmlElement reputationResults = repResultElements[i];

                            var repResult = reputationResults
                                .SelectNodes("./reputationAmounts/SReputationAmountParams")
                                .OfType<XmlElement>()
                                .ToList();

                            foreach (XmlElement result in repResult)
                            {
                                var targetEntityGUID = Guid.Parse(result.GetAttribute("uniqueEntityClass"));
                                var repScopeGUID = Guid.Parse(result.GetAttribute("reputationScope"));
                                var rewardGUID = Guid.Parse(result.GetAttribute("reward"));

                                var targetGiver = GameDatabase.GetEntries<MissionGiver>().SingleOrDefault(mg => mg.EntityGUID == targetEntityGUID);
                                var repScope = GameDatabase.GetEntry<SReputationScopeParams>(repScopeGUID);
                                var reward = GameDatabase.GetEntry<SReputationRewardAmountParams>(rewardGUID);

                                RewardSets[i].Rewards.Add(new MissionReward()
                                {
                                    Type = DB.RewardType.Standing,
                                    Amount = reward.Amount,
                                    TargetEntity = targetGiver,
                                    RepScope = Utilities.CleanKey(repScope.Name)
                                });
                            }
                        }
                    }
                    catch (Exception ex)
                    {

                    }
                }
                else
                {

                }
            }


            //Remove any reward sets that don't have any rewards
            for(int i = 2; i >= 0; i--)
                if (RewardSets[i].Rewards.Count == 0)
                    RewardSets.RemoveAt(i);
        }

        private void CalculateTextTokens()
        {
            TextTokens.Clear();
            RegisteredTokens.Clear();

            foreach (XmlElement textTokenProperty in 
                Element.SelectNodes("./properties/MissionProperty"))
            {
                var variableName = textTokenProperty.GetAttribute("missionVariableName");
                var textToken = textTokenProperty.GetAttribute("extendedTextToken");

                var tokenName = string.IsNullOrEmpty(textToken) ? variableName : textToken;
                var tokenValue = textTokenProperty.SelectSingleNode("./value/*") as XmlElement;

                if (!TextTokens.ContainsKey(tokenName))
                    TextTokens[tokenName] = new List<string>();
                if (!RegisteredTokens.ContainsKey(tokenName))
                    RegisteredTokens[tokenName] = new Dictionary<string, List<string>>(StringComparer.OrdinalIgnoreCase);
                if (!RegisteredLocations.ContainsKey(tokenName))
                    RegisteredLocations[tokenName] = new HashSet<MissionLocation>();
                if (!RegisteredItems.ContainsKey(tokenName))
                    RegisteredItems[tokenName] = new HashSet<MissionItem>();

                switch (tokenValue?.LocalName)
                {
                    case "MissionPropertyValue_StringHash":
                        foreach (XmlElement option in tokenValue.SelectNodes("./options/*"))
                        {
                            switch (option.LocalName)
                            {
                                case "MissionPropertyValueOption_StringHash":
                                    var textID = option.GetAttribute("textId");
                                    TextTokens[tokenName].Add(GameDatabase.Localize(textID));
                                    break;
                                default:
                                    break;
                            }
                        }

                        break;
                    case "MissionPropertyValue_AIName":
                        if (tokenValue.GetAttribute("randomName") == "1")
                        {
                            TextTokens[tokenName].Add("<Random AI>");
                            AddTokens(tokenName, "", (_) =>
                            {
                                return new Dictionary<string, List<string>>()
                                {
                                    { "First", new List<string>() { "<NPC>" } },
                                    { "Last", new List<string>() { "<NPC>" } },
                                    { "Nick", new List<string>() { "<NPC>" } },
                                    { "NickOrFirst", new List<string>() { "<NPC>" } }
                                };
                            });
                        }
                        else
                        {
                            var firstName = GameDatabase.Localize(tokenValue.GetAttribute("characterGivenName"), UninitBlank: true);
                            var nickName = GameDatabase.Localize(tokenValue.GetAttribute("characterGivenNickName"), UninitBlank: true);
                            var lastName = GameDatabase.Localize(tokenValue.GetAttribute("characterGivenLastName"), UninitBlank: true);
                            var fullName = firstName;
                            if (!string.IsNullOrEmpty(nickName)) fullName += $" '{nickName}'";
                            if (!string.IsNullOrEmpty(lastName)) fullName += $" {lastName}";

                            TextTokens[tokenName].Add(fullName);
                            AddTokens(tokenName, "", (_) =>
                            {
                                return new Dictionary<string, List<string>>()
                                {
                                    { "First", new List<string>() { firstName } },
                                    { "Last", new List<string>() { lastName } },
                                    { "Nick", new List<string>() { nickName } },
                                    { "NickOrFirst", new List<string>() { "<NPC>" } }
                                };
                            });
                        }
                        break;
                    case "MissionPropertyValue_Integer":
                    case "MissionPropertyValue_Float":
                        foreach (XmlElement option in tokenValue.SelectNodes("./options/*"))
                        {
                            switch (option.LocalName)
                            {
                                case "MissionPropertyValueOption_Integer":
                                    var variation = int.Parse(option.GetAttribute("variation"));
                                    var value = int.Parse(option.GetAttribute("value"));
                                    if (variation == 0)
                                        TextTokens[tokenName].Add($"{value}");
                                    else
                                        TextTokens[tokenName].Add($"{value} +/- {variation}");
                                    break;
                                case "MissionPropertyValueOption_Float":
                                    var floatvariation = float.Parse(option.GetAttribute("variation"));
                                    var floatvalue = float.Parse(option.GetAttribute("value"));
                                    if (floatvariation == 0)
                                        TextTokens[tokenName].Add($"{floatvalue}");
                                    else
                                        TextTokens[tokenName].Add($"{floatvalue} +/- {floatvariation}");
                                    break;
                                default:
                                    break;
                            }
                        }
                        break;
                    case "MissionPropertyValue_Organization":
                        var matchConditions = tokenValue.SelectNodes("./matchConditions/*");
                        foreach (XmlElement condition in matchConditions)
                        {
                            switch (condition.LocalName)
                            {
                                case "DataSetMatchCondition_TagSearch":
                                    var searchElem = condition.SelectSingleNode("./tagSearch/TagSearchTerm") as XmlElement;
                                    var matchingOrgs = TagSearch<MissionOrganization>(searchElem, e => e.Tags);

                                    foreach (var org in matchingOrgs)
                                        Organizations.Add(org);

                                    break;
                                case "DataSetMatchCondition_SpecificOrganizationsDef":
                                    foreach (var org in DataMatch_Organizations(condition))
                                        Organizations.Add(org);
                                    break;
                                default:
                                    break;
                            }
                        }
                        TextTokens[tokenName].Add("<Mission Contractor>");
                        break;
                    case "MissionPropertyValue_Tags":
                        foreach (XmlElement tagSet in tokenValue.SelectNodes("./tags"))
                        {
                            var setTags = tagSet.SelectNodes("./tags/Reference/@value")
                                .OfType<XmlAttribute>().Select(e => Guid.Parse(e.Value));

                            var matchingLocations = GameDatabase.GetEntries<MissionLocation>()
                                .Where(ml => setTags.Any(t => ml.LocationParams.Tags.Contains(t)));

                            foreach (var location in matchingLocations)
                                TextTokens[tokenName].Add(location.InstanceName);
                        }
                        break;
                    case "MissionPropertyValue_MissionItem":
                        var allItems = GameDatabase.GetEntries<MissionItem>();
                        var matchingItems = allItems.Where(item =>
                                tokenValue.SelectNodes("./matchConditions/*").OfType<XmlElement>().All((missionLocationSearch) =>
                                {
                                    switch (missionLocationSearch.LocalName)
                                    {
                                        case "DataSetMatchCondition_SpecificItemsDef":
                                            var specificIDs = missionLocationSearch.SelectNodes("./items/Reference/@value")
                                                .OfType<XmlAttribute>().Select(a => Guid.Parse(a.Value));
                                            return specificIDs.Contains(item.ID);
                                        case "DataSetMatchCondition_TagSearch":
                                            return true;
                                        case "DataSetMatchCondition_ExcludeProperty":
                                            break;
                                        case "DataSetMatchCondition_ExcludeNearbyLocationsDef":
                                            break;
                                        case "DataSetMatchCondition_PropertyTags":
                                            var searchIDs = missionLocationSearch.SelectNodes("./tagFilters/Reference/@value")
                                                .OfType<XmlAttribute>().Select(a => Guid.Parse(a.Value));
                                            return searchIDs.All(id => item.Tags.Contains(id));
                                        case "DataSetMatchCondition_SpecificOrganizationsDef":
                                            return true; //TODO: Implement
                                        default:
                                            return true;
                                    }

                                    return false;
                                }));

                        TextTokens[tokenName].Add("<Mission Item>");
                        //AddTokens(tokenName, matchingItems, (i) => i.StringVariants);
                        AddTokens(tokenName, "", (_) => new Dictionary<string, List<string>>()
                            {
                                { "Title", new List<string>(){ "<Mission Item>" } },
                                { "SerialNumber", new List<string>(){ "####" } }
                            });
                        break;
                    case "MissionPropertyValue_Location":
                        var allLocations = GameDatabase.GetEntries<MissionLocation>();
                        var matchingMissions = allLocations.Where(ml =>
                                tokenValue.SelectNodes("./matchConditions/*").OfType<XmlElement>().All((missionLocationSearch) =>
                                {
                                    switch (missionLocationSearch.LocalName)
                                    {
                                        case "DataSetMatchCondition_SpecificLocationDef":
                                            var specificIDs = missionLocationSearch.SelectNodes("./locations/Reference/@value")
                                                .OfType<XmlAttribute>().Select(a => Guid.Parse(a.Value));
                                            return specificIDs.Contains(ml.ID);
                                        case "DataSetMatchCondition_TagSearch":
                                            return true;
                                        case "DataSetMatchCondition_ExcludeProperty":
                                            break;
                                        case "DataSetMatchCondition_ExcludeNearbyLocationsDef":
                                            break;
                                        case "DataSetMatchCondition_PropertyTags":
                                            break;
                                        case "DataSetMatchCondition_SpecificOrganizationsDef":
                                            return true; //TODO: Implement
                                        default:
                                            return true;
                                    }

                                    return false;
                                }));

                        TextTokens[tokenName].Add("<Mission Location>");
                        AddTokens(tokenName, "", (_) => new Dictionary<string, List<string>>()
                            {
                                { "Address", new List<string>(){ "<Mission Location>" } }
                            });
                        break;
                    case "MissionPropertyValue_CombinedDataSetEntries":
                        //TextTokens[tokenName].Add($"<CMB:{tokenName}>");
                        var varName = textTokenProperty.GetAttribute("missionVariableName");
                        switch (varName)
                        {
                            case "PickupArray":
                            case "PickupTarget":
                                TextTokens[tokenName].Add("<Mission Pickup Location>");
                                AddTokens(tokenName, "", (_) => new Dictionary<string, List<string>>()
                                    {
                                        { "Address", new List<string>(){ "<Mission Pickup Locations>" } }
                                    });
                                break;
                            case "DropoffArray":
                            case "DropoffTarget":
                            case "MultipleMissionLocations":
                                TextTokens[tokenName].Add("<Mission Dropoff Location>");
                                AddTokens(tokenName, "", (_) => new Dictionary<string, List<string>>()
                                    {
                                        { "Address", new List<string>(){ "<Mission Dropoff Locations>" } }
                                    });
                                break;
                            case "ItemArray":
                                TextTokens[tokenName].Add("<Mission Items>");
                                AddTokens(tokenName, "", (_) => new Dictionary<string, List<string>>()
                                    {
                                        { "SerialNumber", new List<string>(){ "<Mission Items>" } }
                                    });
                                break;
                            default:

                                break;
                        }
                        break;
                    case "MissionPropertyValue_Object":
                        switch(tokenName)
                        {
                            case "TargetName":
                                TextTokens[tokenName].Add("<Mission Target>");
                                break;
                            case "CurrentPlayer":
                                TextTokens[tokenName].Add($"<Player>");
                                break;
                            case "TopPlayer":
                                TextTokens[tokenName].Add($"<Top Player>");
                                break;
                            case "CheckpointWinningPlayer":
                                TextTokens[tokenName].Add($"<Winning Player>");
                                break;
                            case "SubObjective_PickAndDestroyRacer":
                            case "EliminateSpecificRacer":
                                TextTokens[tokenName].Add($"<Target Racer>");
                                break;
                            case "ARacerSurvivesRace":
                                TextTokens[tokenName].Add($"<Player Survives Race>");
                                break;
                            case var raceTarget when raceTarget.StartsWith("XDamageToRacerTarget"):
                                TextTokens[tokenName].Add($"<Target Racer Damage>");
                                break;
                            default:
                                TextTokens[tokenName].Add($"<OBJ:{tokenName}>");
                                break;
                        }
                        break;
                    case "MissionPropertyValue_Reward":
                        TextTokens[tokenName].Add($"<Mission Reward>");
                        break;
                    case null:
                        //case where there is just an empty <value> in the element with no children
                        TextTokens[tokenName].Add(tokenName);
                        break;
                    default:
                        break;
                }


                if (TextTokens[tokenName].Count == 0)
                {
                    switch (tokenName)
                    {
                        case "Danger":
                            TextTokens[tokenName].Add("<Mission Danger>");
                            break;
                        case "Reward":
                            TextTokens[tokenName].Add("<Mission Reward>");
                            break;
                        case "Timed":
                            TextTokens[tokenName].Add("<Mission Timed>");
                            break;
                        case "Location":
                            TextTokens[tokenName].Add("<Mission Location>");
                            break;
                        case "Title":
                            TextTokens[tokenName].Add("<Mission Title>");
                            break;
                        case "Contractor":
                            TextTokens[tokenName].Add("<Mission Contractor>");
                            break;
                        case "Client":
                            TextTokens[tokenName].Add("<Mission Client>");
                            break;
                        case "Item":
                            TextTokens[tokenName].Add("<Mission Item>");
                            break;
                        case "Description":
                            TextTokens[tokenName].Add("<Mission Description>");
                            break;
                        case "CombatShips":
                            TextTokens[tokenName].Add("<Mission Combat Ships>");
                            break;
                        case var tgt when tgt.StartsWith("Mission_TargetTags"):
                            TextTokens[tokenName].Add("<Mission Target>");
                            break;
                        case var reinf1 when reinf1.StartsWith("Mission_Reinforcement"):
                        case var reinf2 when reinf2.StartsWith("Mission_Reinforcment"):
                            TextTokens[tokenName].Add("<Mission Reinforcements>");
                            break;
                        case "Mission_Assassination_Type":
                            TextTokens[tokenName].Add("<Assassination Type>");
                            break;
                        case "MissionType":
                        case "Mission_Type":
                            TextTokens[tokenName].Add("<Mission Type>");
                            break;
                        case "Mission_TargetSpread":
                        case "Mission_Sentries":
                        case "Jingo": //WTF???
                        case "SH_MissionVariant":
                        case "AmbushShips":
                        case "":
                            break;
                        default:
                            break;
                    }
                }

                //Need to handle this eventually....
                if(Organizations.Count == 0)
                {

                }

                //Last check for a failsafe
                if (TextTokens[tokenName].Count == 0)
                    TextTokens[tokenName].Add($"<Skipped:{tokenName}>");
            }
        }

        private void AddTokens<T>(string PropertyKey, T Entry, Func<T, Dictionary<string, List<string>>> GetTextVariants)
        {
            var entryVariants = GetTextVariants(Entry);
            var propertyValues = RegisteredTokens[PropertyKey];

            foreach (var propertyVariant in entryVariants)
            {
                if (!propertyValues.ContainsKey(propertyVariant.Key))
                    propertyValues.Add(propertyVariant.Key, new List<string>());

                foreach (var propertyValue in propertyVariant.Value)
                    propertyValues[propertyVariant.Key].Add(propertyValue);
            }
        }

        private void AddTokens<T>(string PropertyKey, IEnumerable<T> Entries, Func<T, Dictionary<string, List<string>>> GetTextVariants)
        {
            foreach(var entry in Entries)
            {
                AddTokens(PropertyKey, entry, GetTextVariants);
            }
        }

        private IEnumerable<MissionOrganization> DataMatch_Organizations(XmlElement orgSearch)
        {
            foreach (XmlElement org in orgSearch.SelectNodes("./organizations/Reference"))
            {
                var orgGuid = Guid.Parse(org.GetAttribute("value"));
                yield return GameDatabase.GetEntry<MissionOrganization>(orgGuid);
            }
        }

        internal string FormatMissionString(string inputString)
        {
            var fixedText = inputString;

            int replacementsMade;

            do
            {
                replacementsMade = 0;

                //mission names look like this ~mission(Contractor|Title) => Red Wind Delivery
                foreach (Match nameReplace in Regex.Matches(fixedText, @"~mission\((.+?)\)"))
                {
                    replacementsMade++;
                    var nameKey = nameReplace.Groups[1].Value;
                    var fixedName = nameKey;

                    //break apart on the pipe
                    if (nameKey.Contains("|"))
                    {
                        var nameParts = nameKey.Split('|');
                        var nameParent = nameParts[0];
                        var nameChild = nameParts[1];

                        switch (nameParent)
                        {
                            case "Contractor":
                                var org = Organizations.FirstOrDefault();
                                if (org == null) return "<Mission Contractor>";

                                if (org.StringVariants.ContainsKey(nameChild))
                                    fixedName = GameDatabase.Localize(org.StringVariants[nameChild].First()); //TODO: kepp all choices everywhere
                                else
                                    fixedName = "<Mission Contractor>";
                                break;
                            case "RaceType":
                                fixedName = "<Race Objective>";
                                break;
                            default:
                                if (RegisteredTokens.ContainsKey(nameParent))
                                {
                                    var tokenValues = RegisteredTokens[nameParent];

                                    if (RegisteredTokens[nameParent].ContainsKey(nameChild))
                                    {
                                        var tokenVariants = RegisteredTokens[nameParent][nameChild];
                                        fixedName = tokenVariants.First();
                                    }
                                    else
                                    {
                                        fixedName = "";
                                        Log.Error($"No registered tokens under sub-name [{nameParent}]/[{nameChild}]");
                                    }
                                }
                                else
                                {
                                    fixedName = "";
                                    Log.Error($"No registered tokens under parent name [{nameParent}]");
                                }

                                break;
                        }
                    }
                    else
                    {
                        if (TextTokens.ContainsKey(nameKey))
                        {
                            fixedName = TextTokens[nameKey].First();
                        }
                        else if (RegisteredTokens.ContainsKey(nameKey))
                        {
                            fixedName = RegisteredTokens[nameKey].FirstOrDefault().Value?.FirstOrDefault() ?? "";
                        }
                        else if (nameKey == "Reward")       { fixedName = $"{this.MinUECReward}UEC"; }
                        else if (nameKey == "Danger")       { fixedName = $"Danger"; }
                        else if (nameKey == "Stops")        { fixedName = $"<# of Stops>"; }
                        else if (nameKey == "Timed")        { fixedName = $"<Timed Mission>"; }
                        else if (nameKey == "MultiPlayer")  { fixedName = $"MultiPlayer"; }
                        else if (nameKey == "Client")       { fixedName = $"<Mission Client>"; }
                        else if (nameKey == "Complexity")   { fixedName = $"<Difficulty>"; } //HACK: Not a great fix for this. Where would you get this??
                        else
                        {
                            fixedName = $"<Missing:{nameKey}>";
                        }
                    }

                    //Last pass for token strings
                    fixedName = GameDatabase.Localize(fixedName);

                    fixedText = fixedText.Replace(nameReplace.Value, fixedName);
                }
            } while (replacementsMade > 0);

            return fixedText;
        }

        internal IEnumerable<T> PropertyTagSearch<T>(XmlElement element, Func<T, Dictionary<string, List<Guid>>> propertyFunc) where T : DFEntry
        {
            var anyTag = element.GetAttribute("searchForAnyTag") == "1";
            var allRecords = GameDatabase.GetEntries<T>();

            return allRecords.Where(r =>
            {
                var recordProps = propertyFunc(r);
                var searchProperty = element.GetAttribute("searchProperty");
                var propertyTagType = element.GetAttribute("propertyTagType");
                var targetTagType = element.GetAttribute("targetTagType");

                if (anyTag)
                {
                    if (element.HasChildNodes) { }

                    return recordProps.ContainsKey(targetTagType);
                }
                else
                {
                    if (!recordProps.ContainsKey(targetTagType))
                        return false;

                    var tagProps = recordProps[targetTagType];

                    var filterValues = element.SelectNodes("./tagFilters/Reference/@value")
                        .OfType<XmlAttribute>().Select(a => Guid.Parse(a.Value));

                    return filterValues.All(fv => tagProps.Contains(fv));
                }
            });
        }

        internal IEnumerable<T> TagSearch<T>(XmlElement element, Func<T, IEnumerable<Guid>> tagFunc) 
            where T : DFEntry
        {
            var matchingElements = GameDatabase.GetEntries<T>();

            if (element != null)
            {
                var positiveTags = element.SelectNodes("./positiveTags/Reference/@value")
                    .OfType<XmlAttribute>().Select(a => Guid.Parse(a.Value)).ToList();
                var negativeTags = element.SelectNodes("./negativeTags/Reference/@value")
                    .OfType<XmlAttribute>().Select(a => Guid.Parse(a.Value)).ToList();

                matchingElements = matchingElements.Where(o => {
                    var tags = tagFunc(o).ToList();
                    //If we don't have tags in either situation, just ignore everything
                    if (positiveTags.Count == 0 && negativeTags.Count == 0) return false;
                    //This isn't exactly correct.....
                    return positiveTags.All(t => tags.Contains(t)) && !negativeTags.Any(t => tags.Contains(t));
                });
            }

            return matchingElements;
        }
    }

    [DFEntry("MissionType")]
    public class MissionType : DFEntry
    {
        public string Name { get; set; }

        public MissionType(XmlElement fromElement, DFDatabase gameDatabase) : base(fromElement, gameDatabase)
        {
            Name = gameDatabase.Localize(fromElement.GetAttribute("LocalisedTypeName"));
        }
    }

    public class MissionRequirement
    {
        public string Title { get; internal set; }
        public DB.RequirementType Type { get; internal set; }
        public float? Min { get; internal set; }
        public float? Max { get; internal set; }
        public List<MissionGiverEntry> Missions { get; internal set; }
        public MissionGiver TargetGiver { get; internal set; }
        public string RepScope { get; internal set; }
    }

    public class MissionRewardSet
    {
        public string Title { get; internal set; }
        public List<MissionReward> Rewards { get; internal set; } = new List<MissionReward>();
    }

    public class MissionReward
    {
        public DB.RewardType Type { get; internal set; }
        public float Amount { get; internal set; }
        public string RepScope { get; internal set; }
        public MissionGiver TargetEntity { get; internal set; }
    }
}
