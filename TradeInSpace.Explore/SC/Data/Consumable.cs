﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using TradeInSpace.Explore.DataForge;

namespace TradeInSpace.Explore.SC.Data
{
    [DFEntry("ConsumableSubtype")]
    public class Consumable : DFEntry
    {
        public float Hunger { get; set; }
        public float Thirst { get; set; }
        public List<ConsumableEffect> Effects { get; set; }

        public Consumable(XmlElement fromElement, DFDatabase gameDatabase) : base(fromElement, gameDatabase)
        {
            var hungerStat = fromElement.SelectSingleNode("./effectsPerMicroSCU/ConsumableEffectModifyActorStatus[@statType='Hunger']/@statPointChange");
            var thirstStat = fromElement.SelectSingleNode("./effectsPerMicroSCU/ConsumableEffectModifyActorStatus[@statType='Thirst']/@statPointChange");

            if (hungerStat != null)
                Hunger = float.Parse(hungerStat.Value);

            if (thirstStat != null)
                Thirst = float.Parse(thirstStat.Value);

            Effects = fromElement.SelectNodes("./effectsPerMicroSCU/ConsumableEffectAddBuffEffect")
                .OfType<XmlElement>()
                .Select(e => ConsumableEffect.FromElement(e))
                .ToList();
        }
    }

    public class ConsumableEffect
    {
        public string BuffType { get; set; }
        public float? Duration { get; set; }
        public float? Value { get; set; }

        public static ConsumableEffect FromElement(XmlElement fromElement)
        {
            var buffType = fromElement.GetAttribute("buffType");
            var buffDurationOverride = fromElement.SelectSingleNode("./buffDurationOverride/BuffDurationOverride/@durationOverride")?.Value;
            var buffValueOverride = fromElement.SelectSingleNode("./buffValueOverride/BuffValueOverride/@valueOverride")?.Value;
            return new ConsumableEffect()
            {
                BuffType = buffType,
                Duration = buffDurationOverride != null ? (float?)float.Parse(buffDurationOverride) : null,
                Value = buffValueOverride != null ? (float?)float.Parse(buffValueOverride) : null
            };
        }
    }
}
