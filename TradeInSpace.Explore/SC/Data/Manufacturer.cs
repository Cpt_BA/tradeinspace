﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using TradeInSpace.Explore.DataForge;

namespace TradeInSpace.Explore.SC.Data
{
    [DFEntry("SCItemManufacturer")]
    public class Manufacturer : DFEntry
    {
        public string Name { get; private set; }
        public string Description { get; private set; }
        public string Code { get; private set; }
        public string IconPath { get; private set; }

        public Manufacturer(XmlElement fromElement, DFDatabase gameDatabase) : base(fromElement, gameDatabase)
        {
            Name = gameDatabase.Localize(fromElement["Localization"].GetAttribute("Name"));
            Description = gameDatabase.Localize(fromElement["Localization"].GetAttribute("Description"));
            Code = fromElement.GetAttribute("Code");
            IconPath = fromElement.GetAttribute("Logo");
        }
    }
}
