﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using TradeInSpace.Explore.DataForge;
using TradeInSpace.Explore.SC.Components;

namespace TradeInSpace.Explore.SC.Data
{
    [DFEntry("HarvestableProviderPreset")]
    public class HarvestablePresetProvider : DFEntry
    {
        public List<HarvestablePresetEntry> Harvestables { get; private set; } = new List<HarvestablePresetEntry>();

        public HarvestablePresetProvider(XmlElement fromElement, DFDatabase gameDatabase) 
            : base(fromElement, gameDatabase)
        {
        }

        internal override void OnDFEntriesCompleted()
        {
            Harvestables = Element
                .SelectNodes("./harvestableGroups/HarvestableElementGroup/harvestables/HarvestableElement")
                .OfType<XmlElement>()
                .Select(xe => HarvestablePresetEntry.FromHarvestableXML(xe, GameDatabase))
                .ToList();

            /*
            Harvestables.AddRange(
                Harvestables
                    .Where(h => h.Preset?.SubHarvestables?.Any() ?? false)
                    .SelectMany(h => h.Preset.SubHarvestables)
                    .ToList());
            */
        }
    }

    [DFEntry("HarvestablePreset")]
    public class HarvestablePreset : DFEntry
    {
        public Guid EntityGUID { get; private set; }
        public SCEntityClassDef HarvestableEntity { get; private set; }
        public SCCommodity HarvestableCommodity { get; private set; }

        public List<HarvestablePresetEntry> SubHarvestables { get; private set; } = new List<HarvestablePresetEntry>();

        public float RespawnTime { get; private set; }
        public float MinElevation { get; private set; }
        public float MaxElevation { get; private set; }
        public int SubHarvestableSlots { get; private set; } = 0;
        public float SubHarvestableChance { get; private set; } = 0;

        public HarvestablePreset(XmlElement fromElement, DFDatabase gameDatabase)
            : base(fromElement, gameDatabase)
        {
            EntityGUID = Guid.Parse(fromElement.GetAttribute("entityClass"));

            RespawnTime = float.Parse(fromElement.GetAttribute("respawnInSlotTime"));
            MinElevation = float.Parse(fromElement["transformParams"].GetAttribute("minElevation"));
            MaxElevation = float.Parse(fromElement["transformParams"].GetAttribute("maxElevation"));

        }

        internal override void OnDFEntriesCompleted()
        {
            HarvestableEntity = GameDatabase.GetEntity(EntityGUID);
            HarvestableCommodity = HarvestableEntity.GetComponent<SCCommodity>();

            var subHarvest = Element.SelectSingleNode("subConfigBase/SubHarvestableConfigManual/subConfigManual") as XmlElement;

            if(subHarvest != null)
            {
                SubHarvestableChance = float.Parse(subHarvest.GetAttribute("initialSlotsProbability"));

                SubHarvestables = subHarvest
                    .SelectNodes("subHarvestables/SubHarvestableSlot")
                    .OfType<XmlElement>()
                    .Select(xe => HarvestablePresetEntry.FromSubharvestableXML(xe, GameDatabase))
                    .ToList();

                if (this.HarvestableEntity.HasComponent<SCItem>())
                {
                    var harvestableLayout = this.HarvestableEntity.GetComponent<SCItem>();
                    this.SubHarvestableSlots = harvestableLayout.ItemPorts.Count();
                }
            }
        }
    }

    public class HarvestablePresetEntry
    {
        public HarvestablePreset Preset { get; private set; }
        public float Probability { get; private set; }
        public float RespawnMultiplier { get; private set; }

        private HarvestablePresetEntry() { }

        public static HarvestablePresetEntry FromHarvestableXML(XmlElement fromElement, DFDatabase database)
        {
            var harvestableGUID = Guid.Parse(fromElement.GetAttribute("harvestable"));

            return new HarvestablePresetEntry()
            {
                Preset = database.GetEntry<HarvestablePreset>(harvestableGUID),
                Probability = float.Parse(fromElement.GetAttribute("relativeProbability")),
                RespawnMultiplier = 1
            };
        }

        public static HarvestablePresetEntry FromSubharvestableXML(XmlElement fromElement, DFDatabase database)
        {
            var harvestableGUID = Guid.Parse(fromElement.GetAttribute("harvestable"));

            return new HarvestablePresetEntry()
            {
                Preset = database.GetEntry<HarvestablePreset>(harvestableGUID),
                Probability = float.Parse(fromElement.GetAttribute("relativeProbability")),
                RespawnMultiplier = float.Parse(fromElement.GetAttribute("harvestableRespawnTimeMultiplier"))
            };
        }
    }
}
