﻿using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using TradeInSpace.Explore.DataForge;

namespace TradeInSpace.Explore.SC.Data
{
    [DFEntry("MineableElement")]
    public class MineableElement : DFEntry
    {
        public ResourceType Resource { get; private set; }

        public Guid ResourceTypeID { get; private set; }
        public float Instability { get; private set; }
        public float Resistance { get; private set; }
        public float WindowMidpoint { get; private set; }
        public float WindowMidpointRandomness { get; private set; }
        public float WindowMidpointThinness { get; private set; }
        public float ExplosionMultiplier { get; private set; }
        public float ClusterFactor { get; private set; }

        public MineableElement(XmlElement fromElement, DFDatabase gameDatabase) : base(fromElement, gameDatabase)
        {
            ResourceTypeID = Guid.Parse(fromElement.GetAttribute("resourceType"));
            Instability = float.Parse(fromElement.GetAttribute("elementInstability"));
            Resistance = float.Parse(fromElement.GetAttribute("elementResistance"));
            WindowMidpoint = float.Parse(fromElement.GetAttribute("elementOptimalWindowMidpoint"));
            WindowMidpointRandomness = float.Parse(fromElement.GetAttribute("elementOptimalWindowMidpointRandomness"));
            WindowMidpointThinness = float.Parse(fromElement.GetAttribute("elementOptimalWindowThinness"));
            ExplosionMultiplier = float.Parse(fromElement.GetAttribute("elementExplosionMultiplier"));
            ClusterFactor = float.Parse(fromElement.GetAttribute("elementClusterFactor"));
        }

        internal override void OnDFEntriesCompleted()
        {
            Resource = GameDatabase.GetEntry<ResourceType>(ResourceTypeID);
        }
    }

    [DFEntry("MineableComposition")]
    public class MineableComposition : DFEntry
    {
        public string CompositionName { get; set; }
        public int MinimumDistinctElements { get; set; }
        public List<MineableCompositionEntry> Entries { get; set; }

        public MineableComposition(XmlElement fromElement, DFDatabase gameDatabase)
            : base(fromElement, gameDatabase)
        {
            CompositionName = gameDatabase.Localize(fromElement.GetAttribute("depositName"));
            MinimumDistinctElements = int.Parse(fromElement.GetAttribute("minimumDistinctElements"));

            Entries = fromElement
                .SelectNodes("./compositionArray/MineableCompositionPart")
                .OfType<XmlElement>()
                .Select(n => MineableCompositionEntry.FromXML(n))
                .ToList();
        }

        internal override void OnDFEntriesCompleted()
        {
            foreach (var entry in Entries)
                entry.Element = GameDatabase.GetEntry<MineableElement>(entry.ElementID);
        }
    }

    public class MineableCompositionEntry
    {
        public Guid ElementID { get; set; }
        public float MinPercentage { get; set; }
        public float MaxPercentage { get; set; }
        public float Probability { get; set; }
        public float CurveExponent { get; set; }

        public MineableElement Element { get; set; }

        private MineableCompositionEntry() { }

        public static MineableCompositionEntry FromXML(XmlElement fromElement)
        {
            return new MineableCompositionEntry()
            {
                ElementID = Guid.Parse(fromElement.GetAttribute("mineableElement")),
                MinPercentage = float.Parse(fromElement.GetAttribute("minPercentage")),
                MaxPercentage = float.Parse(fromElement.GetAttribute("maxPercentage")),
                Probability = float.Parse(fromElement.GetAttribute("probability")),
                CurveExponent = float.Parse(fromElement.GetAttribute("curveExponent")),
            };
        }
    }
}
