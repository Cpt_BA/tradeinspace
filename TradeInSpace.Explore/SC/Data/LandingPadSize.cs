﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using TradeInSpace.Explore.DataForge;
using static TradeInSpace.Models.Enums;

namespace TradeInSpace.Explore.SC.Data
{
    [DFEntry("LandingPadSize")]
    public class LandingPadSize : DFEntry
    {
        public LandingPadSizeEntry ShipSize { get; set; }
        public LandingPadSizeEntry VehicleSize { get; set; }


        public LandingPadSize(XmlElement fromElement, DFDatabase gameDatabase) : base(fromElement, gameDatabase)
        {
            var shipElement = fromElement["shipSize"];
            var groundVehicleElement = fromElement["groundVehicleSize"];

            if (shipElement != null)
            {
                ShipSize = new LandingPadSizeEntry(
                        (PadSize)Enum.Parse(typeof(PadSize), InstanceName, true),
                        LandingPadType.Ship,
                        int.Parse(shipElement.GetAttribute("x")),
                        int.Parse(shipElement.GetAttribute("y")),
                        int.Parse(shipElement.GetAttribute("z"))
                    );
            }

            if (groundVehicleElement != null)
            {
                VehicleSize = new LandingPadSizeEntry(
                        (PadSize)Enum.Parse(typeof(PadSize), InstanceName, true),
                        LandingPadType.GroundVehicle,
                        int.Parse(groundVehicleElement.GetAttribute("x")),
                        int.Parse(groundVehicleElement.GetAttribute("y")),
                        int.Parse(groundVehicleElement.GetAttribute("z"))
                    );
            }
        }
    }

    public class LandingPadSizeEntry
    {
        public PadSize Size { get; set; }
        public LandingPadType Type { get; set; }
        public int X { get; set; }
        public int Y { get; set; }
        public int Z { get; set; }

        public LandingPadSizeEntry(PadSize size, LandingPadType type, int x, int y, int z)
        {
            Size = size;
            Type = type;
            X = x;
            Y = y;
            Z = z;
        }
    }

    public enum LandingPadType {
        Ship,
        GroundVehicle
    }
}
