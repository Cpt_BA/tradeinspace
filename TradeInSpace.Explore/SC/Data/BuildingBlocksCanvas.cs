﻿using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using TradeInSpace.Explore.DataForge;

namespace TradeInSpace.Explore.SC.Data
{
    [DFEntry("BuildingBlocks_Canvas")]
    public class BuildingBlocksCanvas : DFEntry
    {
        public Vector3 Size { get; set; }
        public List<BBCanvasWidgetBase> Blocks { get; set; }

        public BuildingBlocksCanvas(XmlElement fromElement, DFDatabase gameDatabase)
            : base(fromElement, gameDatabase)
        {
            Size = fromElement["size"].Deserialize<Vector3>("size");
            Blocks = fromElement.SelectNodes(".//scene/*")
                .OfType<XmlElement>()
                .Select(BBCanvasWidgetBase.FromXMLElement)
                .Where(bb => bb != null)
                .ToList();
        }

        internal override void OnDFEntriesCompleted()
        {

        }
    }

    public class BBCanvasWidgetBase
    {
        [XmlIgnore]
        public XmlElement Element { get; private set; }

        [XmlAttribute("name")]
        public string Name { get; set; }
        [XmlAttribute("layer")]
        public int Layer { get; set; }
        [XmlAttribute("isActive")]
        public int IsActive { get; set; }
        [XmlAttribute("alpha")]
        public float Alpha { get; set; }

        [XmlElement(ElementName = "position")]
        public XMLVec3 Position { get; set; }
        [XmlElement(ElementName = "positionOffset")]
        public XMLVec3 PositionOffset { get; set; }
        [XmlElement(ElementName = "orientation")]
        public XMLVec3 Orientation { get; set; }
        [XmlElement(ElementName = "orientationOffset")]
        public XMLVec3 OrientationOffset { get; set; }
        [XmlElement(ElementName = "scale")]
        public XMLVec3 Scale { get; set; }
        [XmlElement(ElementName = "pivot")]
        public XMLVec3 Pivot { get; set; }
        [XmlElement(ElementName = "anchor")]
        public XMLVec3 Anchor { get; set; }


        [XmlElement(ElementName = "sizing")]
        public SizingRules Sizing { get; set; }
        [XmlElement(ElementName = "padding")]
        public DirectionalValues Padding { get; set; }
        [XmlElement(ElementName = "margin")]
        public DirectionalValues Margin { get; set; }

        public static BBCanvasWidgetBase FromXMLElement(XmlElement element)
        {
            BBCanvasWidgetBase result;

            switch (element.Name)
            {
                case "BuildingBlocks_ComponentBadgeListItem":
                case "BuildingBlocks_ComponentCheckboxButton":
                case "BuildingBlocks_ComponentCheckboxListItem":
                case "BuildingBlocks_ComponentCustomButtonSecondary":
                case "BuildingBlocks_ComponentCustomDropDownMenu":
                case "BuildingBlocks_ComponentCustomListItem":
                case "BuildingBlocks_ComponentGeneralButton":
                case "BuildingBlocks_ComponentGeneralButtonLarge":
                case "BuildingBlocks_ComponentGeneralButtonSecondary":
                case "BuildingBlocks_ComponentGeneralDropDownMenu":
                case "BuildingBlocks_ComponentGeneralListItem":
                case "BuildingBlocks_ComponentLabelCaptionPair":
                case "BuildingBlocks_ComponentLinearRangeSlider":
                case "BuildingBlocks_ComponentMinMaxRangeSlider":
                case "BuildingBlocks_ComponentNavigationTabList":
                case "BuildingBlocks_ComponentNotification":
                case "BuildingBlocks_ComponentRadioButton":
                case "BuildingBlocks_ComponentRadioListItem":
                case "BuildingBlocks_ComponentScrollBar":
                case "BuildingBlocks_ComponentToggleButton":
                case "BuildingBlocks_ComponentToggleListItem":
                    result = element.Deserialize<BBComponentBase>(element.Name);
                    break;


                case "BuildingBlocks_ActorEntityWidget":
                case "BuildingBlocks_DisplayWidget":
                case "BuildingBlocks_GeneralEntityWidget":
                case "BuildingBlocks_GeometryEntityWidget":
                case "BuildingBlocks_VehicleEntityWidget":
                    result = element.Deserialize<BBCanvasWidgetBase>(element.Name);
                    break;


                case "BuildingBlocks_WidgetBadge":
                case "BuildingBlocks_WidgetBase":
                case "BuildingBlocks_WidgetBodyBackground":
                case "BuildingBlocks_WidgetCanvas":
                case "BuildingBlocks_WidgetCheckboxControl":
                case "BuildingBlocks_WidgetCinematicLabel":
                case "BuildingBlocks_WidgetCircle":
                case "BuildingBlocks_WidgetClone":
                case "BuildingBlocks_WidgetCustomShape":
                case "BuildingBlocks_WidgetEditBox":
                case "BuildingBlocks_WidgetEnvironmentProbe":
                case "BuildingBlocks_WidgetIcon":
                case "BuildingBlocks_WidgetImage":
                case "BuildingBlocks_WidgetLight":
                case "BuildingBlocks_WidgetLighting":
                case "BuildingBlocks_WidgetLine":
                case "BuildingBlocks_WidgetLinearProgressMeter":
                case "BuildingBlocks_WidgetList":
                case "BuildingBlocks_WidgetLoadingIndicator":
                case "BuildingBlocks_WidgetManufacturerLogo":
                case "BuildingBlocks_WidgetMovie":
                case "BuildingBlocks_WidgetParticleEffect":
                case "BuildingBlocks_WidgetPolygon":
                case "BuildingBlocks_WidgetPolymorphic":
                case "BuildingBlocks_WidgetRadialProgressMeter":
                case "BuildingBlocks_WidgetRadioButton":
                case "BuildingBlocks_WidgetRadioControl":
                case "BuildingBlocks_WidgetRuntimeImage":
                case "BuildingBlocks_WidgetSeparator":
                case "BuildingBlocks_WidgetSlice":
                case "BuildingBlocks_WidgetSlider":
                case "BuildingBlocks_WidgetStrip":
                case "BuildingBlocks_WidgetTextField":
                case "BuildingBlocks_WidgetTickBox":
                case "BuildingBlocks_WidgetToggleControl":
                case "BuildingBlocks_WidgetWindow":
                    result = element.Deserialize<BBWidget>(element.Name);
                    break;


                case "BuildingBlocks_WidgetText":
                    result = element.Deserialize<BBCanvasText>(element.Name);
                    break;


                default:
                    Log.Information($"Unknown BB widget type: {element.Name}");
                    return null;
            }

            result.Element = element;

            return result;
        }

        public class SizingRules
        {
            [XmlAttribute("enableMinWidth")]
            public double WnableMinWidth { get; set; }
            [XmlAttribute("enableMinHeight")]
            public double WnableMinHeight { get; set; }
            [XmlAttribute("enableMaxWidth")]
            public double WnableMaxWidth { get; set; }
            [XmlAttribute("enableMaxHeight")]
            public double WnableMaxHeight { get; set; }

            [XmlElement(ElementName = "width")]
            public SizingBehavior Width { get; set; }
            [XmlElement(ElementName = "height")]
            public SizingBehavior Height { get; set; }
            [XmlElement(ElementName = "depth")]
            public SizingBehavior Depth { get; set; }
            [XmlElement(ElementName = "minWidth")]
            public SizingBehavior MinWidth { get; set; }
            [XmlElement(ElementName = "minHeight")]
            public SizingBehavior MinHeight { get; set; }
            [XmlElement(ElementName = "maxWidth")]
            public SizingBehavior MaxWidth { get; set; }
            [XmlElement(ElementName = "maxHeight")]
            public SizingBehavior MaxHeight { get; set; }
        }

        public class SizingBehavior
        {
            [XmlAttribute("value")]
            public double Value { get; set; }

            [XmlAttribute("behavior")]
            public string Behavior { get; set; }
        }

        public class DirectionalValues
        {
            [XmlAttribute("left")]
            public double Left { get; set; }
            [XmlAttribute("right")]
            public double Right { get; set; }
            [XmlAttribute("bottom")]
            public double Bottom { get; set; }
            [XmlAttribute("top")]
            public double Top { get; set; }
        }

        public class XMLVec3
        {
            [XmlAttribute("x")]
            public double X { get; set; }
            [XmlAttribute("y")]
            public double Y { get; set; }
            [XmlAttribute("z")]
            public double Z { get; set; }

            public Vector3 ToVector3()
            {
                return new Vector3(X, Y, Z);
            }
        }
    }

    public class BBWidget : BBCanvasWidgetBase
    {
    }

    public class BBComponentBase : BBCanvasWidgetBase
    {
    }

    public class BBCanvasText : BBCanvasWidgetBase
    {
        [XmlAttribute("locString")]
        public string LocalString { get; set; }
        [XmlAttribute("textAlignment")]
        public string TextAlignment { get; set; }
        [XmlAttribute("verticalAlignment")]
        public string VerticalAlignment { get; set; }
        [XmlAttribute("fontSize")]
        public float FontSize { get; set; }
        [XmlAttribute("bold")]
        public int Bold { get; set; }
        [XmlAttribute("italic")]
        public int Italic { get; set; }
        [XmlAttribute("underline")]
        public int Underline { get; set; }
    }
}
