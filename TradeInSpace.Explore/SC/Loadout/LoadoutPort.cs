﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using TradeInSpace.Explore.SC.Components;

namespace TradeInSpace.Explore.SC.Loadout
{
    public class LoadoutPort
    {
        public LoadoutEntry ParentEntry { get; private set; }
        public XmlElement Element { get; private set; }

        public SCEntityClassDef AttachedComponent { get; private set; }
        public LoadoutTree ComponentLoadout { get; private set; }

        public string DisplayName { get; private set; }
        public string ID { get; private set; }
        public int? MinSize { get; internal set; } = null;
        public int? MaxSize { get; internal set; } = null;

        public bool Editable { get; private set; } = true;
        public bool Visible { get; private set; } = true;

        public List<PortType> PortTypes { get; private set; } = new List<PortType>();
        public List<PortConnection> Connections { get; private set; } = new List<PortConnection>();
        public List<string> UsableTypes { get; private set; } = new List<string>();
        public List<string> Tags { get; internal set; } = new List<string>();
        public List<string> RequiredTags { get; internal set; } = new List<string>();
        public List<string> Flags { get; internal set; } = new List<string>();

        public static LoadoutPort FromVehiclePart
            (LoadoutTree loadout, XmlElement element, LoadoutEntry parent = null)
        {
            var port = new LoadoutPort()
            {
                Element = element,
                DisplayName = element.GetAttribute("display_name").ToLower(),
                ParentEntry = parent,
                Flags = element.GetAttribute("flags").Split(' ').ToList(),
                Tags = element.GetAttribute("tags")?.Split(' ')?.ToList(),
                RequiredTags = element.GetAttribute("requiredTags")?.Split(' ')?.ToList()
            };

            if (element.HasAttribute("id"))
                port.ID = element.GetAttribute("id");
            if (element.HasAttribute("minsize"))
                port.MinSize = int.Parse(element.GetAttribute("minsize"));
            if (element.HasAttribute("maxsize"))
                port.MaxSize = int.Parse(element.GetAttribute("maxsize"));

            if (element["Types"] != null)
            {
                foreach (var portType in element["Types"].OfType<XmlElement>())
                {
                    //This is just weird....
                    if(portType.HasAttribute("id"))
                    {
                        if (port.ID == null)
                        {
                            port.ID = portType.GetAttribute("id");
                        }
                        else
                        {

                        }
                    }

                    port.PortTypes.Add(new PortType()
                    {
                        Type = portType.GetAttribute("type"),
                        Subtypes = portType.HasAttribute("subtypes") ? portType.GetAttribute("subtypes").Split(',') : new string[] { }
                    });
                }
            }
            if (element["Connections"] != null)
            {
                port.Connections = element["Connections"]
                    .SelectNodes("./Connection")
                    .DeserializeBulk<PortConnection>(RootElementName: "Connection")
                    .ToList();
            }

            port.Editable = (!port.Flags.Contains("uneditable")) && (!port.Flags.Contains("$uneditable"));
            port.Visible = !port.Flags.Contains("invisible");

            return port;
        }

        public static LoadoutPort EmptyPort(XmlElement element, LoadoutEntry parent)
        {
            return new LoadoutPort()
            {
                Element = element,
                DisplayName = element.GetAttribute("display_name").ToLower(),
                ParentEntry = parent,
                Flags = element.GetAttribute("flags").Split(' ').ToList(),
                Tags = element.GetAttribute("tags")?.Split(' ')?.ToList(),
                RequiredTags = element.GetAttribute("requiredTags")?.Split(' ')?.ToList()
            };
        }

        public static LoadoutPort FromComponentDefinition(XmlElement element, LoadoutEntry parent = null)
        {
            var port = new LoadoutPort()
            {
                Element = element,
                DisplayName = element.GetAttribute("DisplayName"),
                ParentEntry = parent,
                Flags = element.GetAttribute("Flags").Split(' ').ToList(),
                Tags = element.GetAttribute("PortTags").Split(' ').ToList(),
                RequiredTags = element.GetAttribute("RequiredPortTags").Split(' ').ToList()
            };

            if (element.HasAttribute("Id"))
                port.ID = element.GetAttribute("Id");
            if (element.HasAttribute("MinSize"))
                port.MinSize = int.Parse(element.GetAttribute("MinSize"));
            if (element.HasAttribute("MaxSize"))
                port.MaxSize = int.Parse(element.GetAttribute("MaxSize"));

            if(element["Types"] != null)
            {
                foreach(XmlElement typeEntry in element["Types"])
                {
                    var typeName = typeEntry.GetAttribute("Type");
                    var subTypes = typeEntry
                        .SelectNodes("./SubTypes/Enum/@value")
                        .OfType<XmlAttribute>()
                        .Select(a => a.Value);

                    port.PortTypes.Add(new PortType()
                    {
                        Type = typeName,
                        Subtypes = subTypes.ToArray()
                    });
                }
            }
            if (element["Connections"] != null)
            {
                port.Connections = element["Connections"]
                    .SelectNodes("./Connection")
                    .DeserializeBulk<PortConnection>(RootElementName: "Connection")
                    .ToList();
            }

            port.Editable = (!port.Flags.Contains("uneditable")) && (!port.Flags.Contains("$uneditable"));

            return port;
        }

        public LoadoutTree AttachComponent(SCEntityClassDef componentClass)
        {
            var itemComponent = componentClass.GetComponent<AttachableComponent>();
            if (itemComponent == null) throw new Exception($"Component supplied ({componentClass.InstanceName}) is not an attachable component.");

            LoadoutTree componentTree = LoadoutTree.CreateFromEntity(componentClass);

            AttachedComponent = componentClass;
            ComponentLoadout = componentTree;
            if(componentTree != null)
                ParentEntry.Children.Add(componentTree.Root);

            return componentTree;
        }
    }

    public class PortType
    {
        public string Type { get; set; }
        public string[] Subtypes { get; set; }

        public string[] ToStringArray()
        {
            if (Subtypes == null || Subtypes.Length == 0)
                return new string[] { Type };
            else
                return Subtypes.Select(st => $"{Type}/{st}").ToArray();
        }

        public bool Matches(string Name)
        {
            return Type == Name || ToStringArray().Contains(Name);
        }
    }

    public class PortConnection
    {
        [XmlAttribute("pipeClass")]
        public string PipeClass { get; set; }
        [XmlAttribute("pipe")]
        public string Name { get; set; }
    }

}
