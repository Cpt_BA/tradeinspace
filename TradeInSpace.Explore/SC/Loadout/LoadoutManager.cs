﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TradeInSpace.Explore.DataForge;
using TradeInSpace.Explore.SC.Components;

namespace TradeInSpace.Explore.SC.Loadout
{
    public class LoadoutManager
    {
        internal DFDatabase GameDatabase { get; }
        private List<LoadoutTree> Loadouts = new List<LoadoutTree>();

        public LoadoutManager(DFDatabase gameDatabase)
        {
            GameDatabase = gameDatabase;
        }

        public void PreloadLoadouts()
        {
            int vehicleCounter = 0;
            var allVehilcles = GameDatabase.GetComponentsOnEntities<Vehicle>().ToList();
            foreach (var vehicleComponent in allVehilcles)
            {
                var loadout = LoadoutTree.CreateFromVehicle(vehicleComponent);
                DFDatabase.UpdateProgress(this, ProgressPhase.Loadouts, vehicleCounter++, allVehilcles.Count);
                Loadouts.Add(loadout);
            }
        }

        public IEnumerable<LoadoutTree> AllLoadouts
        {
            get { return Loadouts; }
        }

    }
}
