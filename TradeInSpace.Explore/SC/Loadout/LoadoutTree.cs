﻿using CgfConverter;
using CgfConverter.CryEngineCore;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Serilog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml;
using TradeInSpace.Explore.SC.Components;

namespace TradeInSpace.Explore.SC.Loadout
{
    public class LoadoutTree
    {
        public SCEntityClassDef From { get; private set; }
        public Vehicle Vehicle { get; private set; }

        public LoadoutEntry Root { get; private set; }
        public LoadoutTree AttachedTree { get; private set; }
        public LoadoutTree RootTree
        {
            get
            {
                return AttachedTree ?? this;
            }
        }

        public XmlElement LoadoutDocument { get; private set; }

        private LoadoutTree() { }

        public static LoadoutTree CreateFromVehicle(Vehicle vehicle, bool loadDefaults = true)
        {
            if (vehicle == null) return null;

            var GameDatabase = vehicle.ParentEntity.GameDatabase;

            var vehicleEntClass = vehicle.ParentEntity;

            XmlDocument vehicleFile = null;
            try
            {
                vehicleFile = GameDatabase.ReadPackedFileCryXML(vehicle.DefinitionFile);
            }
            catch { }

            if (vehicleFile == null)
            {
                Log.Warning($"Vehicle {vehicle.Name} has no definition file.");
                return null;
            }

            var vehicleLoadoutTree = vehicleFile["Vehicle"];

            var partsEntry = vehicleLoadoutTree["Parts"];

            if(!string.IsNullOrEmpty(vehicle.DefinitionModification))
            {
                var defModification = vehicleLoadoutTree["Modifications"]?.SelectSingleNode($"./Modification[@name='{vehicle.DefinitionModification}']") as XmlElement;

                if(defModification != null)
                {
                    if (defModification.HasAttribute("patchFile") && !string.IsNullOrEmpty(defModification.GetAttribute("patchfile")))
                    {
                        //Vehicle contains a patched loadout tree.
                        var patchPath = Path.Combine(Path.GetDirectoryName(vehicle.DefinitionFile), defModification.GetAttribute("patchFile"));
                        var patchContents = GameDatabase.ReadPackedFileCryXML(Path.ChangeExtension(patchPath, "xml"));
                        //TODO: Better patch handling...
                        var patchRoot = patchContents?["Modifications"]?["Parts"];

                        if (patchRoot != null)
                        {
                            Log.Information($"Using modification patch {vehicle.DefinitionModification} for vehicle {vehicle.Name}");
                            partsEntry = patchRoot;
                        }
                    }
                }
                else
                {
                    Log.Warning($"Requested modification {vehicle.DefinitionModification} on ship {vehicle.Name} not found.");
                }
            }

            var loadout = new LoadoutTree()
            {
                From = vehicle.ParentEntity,
                Vehicle = vehicle,
                LoadoutDocument = partsEntry
            };

            loadout.Root = LoadoutEntry.FromAttachableXML(loadout, partsEntry["Part"]);

            if (!string.IsNullOrEmpty(vehicle.DefinitionModification))
            {
                var defModification = vehicleLoadoutTree["Modifications"]?.SelectSingleNode($"./Modification[@name='{vehicle.DefinitionModification}']") as XmlElement;

                if (defModification != null)
                {
                    loadout.ApplyModifications(defModification);
                }
                else
                {
                    Log.Warning($"Requested modification {vehicle.DefinitionModification} on ship {vehicle.Name} not found.");
                }
            }

            var defaultLoadout = vehicleEntClass.GetComponent<DefaultLoadoutParams>();
            if (loadDefaults && defaultLoadout != null)
            {
                //Applies recursively
                defaultLoadout.Loadout.PopulateLoadout(loadout);
            }

            return loadout;
        }

        private void ApplyModifications(XmlElement defModification)
        {
            var modElements = defModification.SelectNodes("./Elems/Elem").Cast<XmlElement>().ToList();

            foreach (var modification in modElements)
            {
                var modName = modification.GetAttribute("name");
                var modValue = modification.GetAttribute("value");
                var modID = modification.GetAttribute("idRef");

                switch (modID)
                {
                    case "modVehicle":
                        switch (modName)
                        {
                            case "name":
                                Log.Warning("Modification type - modVehicle - name not supported yet");
                                break;
                            case "displayname":
                                Log.Warning("Modification type - modVehicle - displayname not supported yet");
                                break;
                            case "landingOffset":
                                Log.Warning("Modification type - modVehicle - displayname not supported yet");
                                break;
                            case "crossSectionMultiplier":
                                Log.Warning("Modification type - modVehicle - crossSectionMultiplier not supported yet");
                                break;
                            case "HudPaletteScheme":
                                Log.Warning("Modification type - modVehicle - HudPaletteScheme not supported yet");
                                break;
                            default:
                                break;
                        }
                        break;
                    default:
                        var foundParts = Utilities.FlattenRecursive(this.Root, l => l.Children).Where(p => (p.ID ?? p.Port?.ID)?.Equals(modID) ?? false);
                        foreach(var foundPart in foundParts)
                        {
                            //Log.Information($"Applying patch for ship port {modID}. {modName} - {modValue}");
                            switch (modName)
                            {
                                case "minsize":
                                    foundPart.Port.MinSize = int.Parse(modValue);
                                    break;
                                case "maxsize":
                                    foundPart.Port.MaxSize = int.Parse(modValue);
                                    break;
                                case "damageMax":
                                    foundPart.Health = float.Parse(modValue);
                                    break;
                                case "mass":
                                    foundPart.Mass = float.Parse(Regex.Match(modValue, @"(\d*)").Groups[1].Value); //Fix for erroneous characters
                                    break;
                                case "flags":
                                    foundPart.Port.Flags = modValue.Split(' ').ToList();
                                    break;
                                case "tags":
                                    foundPart.Port.Tags = modValue.Split(' ').ToList();
                                    break;
                                case "requiredTags":
                                    foundPart.Port.RequiredTags = modValue.Split(' ').ToList();
                                    break;
                                case "type":
                                    foundPart.Port.PortTypes.Clear();
                                    foundPart.Port.PortTypes.Add(new PortType()
                                    {
                                        Type = modValue,
                                        Subtypes = null
                                    });
                                    break;
                                case "subtypes":
                                    if(foundPart.Port.PortTypes.Count == 0 && foundPart.Port.PortTypes[0].Subtypes == null)
                                    {
                                        foundPart.Port.PortTypes[0].Subtypes = modValue.Split(',');
                                    }
                                    break;
                                case "defaultWeaponGroup":
                                case "skippart":
                                case "skipPart":
                                case "name":
                                    break;
                                default:
                                    break;
                            }
                        }
                        
                        if(foundParts.Count() == 0)
                        {
                            switch (modID)
                            {
                                case "modMainPart":
                                case "modNoseVariantGeo":
                                case "modNode":
                                case "modBody":
                                case "modShip":
                                case "modShip2":
                                case "modAnim":
                                case "modLights":
                                case "modGrav":
                                case "modBridge":
                                case "modBridge1":
                                case "modBridge2":
                                case "modLower":
                                case "modUpper":
                                case "modTurretR":
                                case "modTurretL":
                                case "modSD_Front":
                                case "modSD_Rear":
                                case "modTD_Front":
                                case "modTD_Rear":
                                case "modMD_Front":
                                case "modMD_Rear":
                                case "modEngineering":
                                case "modCargo":
                                case "modHabitation":
                                case "modExt1":
                                case "modExt2":
                                case "modShield":
                                case "modPaint":
                                case "modRight_Wing":
                                case "modLeft_Wing":
                                case "modVehicleClass":
                                case "modShipGeometry":
                                case "modHoloGeometry_Default":
                                case "modHoloGeometry_OverDragging":
                                case "modHoloGeometry_ItemPreviewA":
                                case "modDeathMask":
                                case "modPilotEnterPosActivation":
                                case "modUpperTurretHUD":
                                case "modUpperTurretEnergyController":
                                case "modCoPilotEnterSeat":
                                case "modCoPilotSeat":
                                case "modRightLegPedal":
                                case "modLeftLegPedal":
                                case "modMannequin":
                                case "xmlMannequin":
                                case "modMissileRack":
                                    break;
                                default:
                                    break;
                            }
                        }
                        break;
                }
            }
        }

        public static LoadoutTree CreateFromEntity(SCEntityClassDef entityClass, bool loadDefaults = true, LoadoutEntry attachedTo = null)
        {
            var vehicle = entityClass.GetComponent<Vehicle>();

            if (vehicle != null)
            {
                return CreateFromVehicle(vehicle, loadDefaults);
            }
            else {
                var defaultsComponent = entityClass.GetComponent<DefaultLoadoutParams>();

                //TODO: Verify attachment parameters (size/type/connections/etc.)
                //Probably better to do client-side, but should warn here for sanity-checking

                var entityLoadout = new LoadoutTree()
                {
                    From = entityClass,
                    Vehicle = attachedTo?.Owner?.Vehicle,
                    LoadoutDocument = defaultsComponent?.Element
                };

                entityLoadout.Root = LoadoutEntry.FromEntity(entityLoadout, entityClass, attachedTo);

                if(entityLoadout.Root == null)
                {
                    return null;
                }

                if (defaultsComponent?.Loadout != null && loadDefaults)
                {
                    defaultsComponent.Loadout.PopulateLoadout(entityLoadout);
                }

                return entityLoadout;
            }
        }

        private LoadoutSlotDTO MakeStructure(IEnumerable<string> IgnoreTypes)
        {
            LoadoutSlotDTO _CreateSlotDTO(LoadoutEntry _slot)
            {
                var child_slots = _slot.Children
                    .Where(check_slot => !IgnoreTypes.Any(ig => check_slot.Port?.PortTypes?.Any(pt => pt.Matches(ig)) ?? false))
                    .ToList();

                if (_slot.Port?.ComponentLoadout != null)
                    child_slots.Add(_slot.Port.ComponentLoadout.Root);

                return new LoadoutSlotDTO()
                {
                    PortName = _slot.PortName,
                    Mass = _slot.Mass,
                    Health = _slot.Health,
                    Editable = _slot.Port?.Editable ?? false,
                    Visible = _slot.Port?.Visible ?? false,
                    Types = _slot.Port?.PortTypes?.SelectMany(pt => pt.ToStringArray())?.ToArray() ?? new string[] { },
                    Tags = _slot.Port?.Tags?.Where(t => !string.IsNullOrEmpty(t))?.ToArray() ?? new string[] { },
                    Flags = _slot.Port?.Flags?.Where(f => !string.IsNullOrEmpty(f))?.ToArray() ?? new string[] { },
                    RequiredTags = _slot.Port?.RequiredTags?.ToArray() ?? new string[] { },
                    MinSize = _slot.Port?.MinSize,
                    MaxSize = _slot.Port?.MaxSize,
                    Slots = child_slots.Select(_CreateSlotDTO).ToList(),
                    Connections = _slot.Port?.Connections ?? new List<PortConnection>()
                };
            }

            return _CreateSlotDTO(Root);
        }

        public string GetStructureJSON(IEnumerable<string> IgnoreTypes)
        {
            return JsonConvert.SerializeObject(MakeStructure(IgnoreTypes));
        }

        public static ChunkNode SearchModelForHardpoint(CryEngine shipModel, string HardpointName)
        {
            return shipModel.NodeMap.Values.FirstOrDefault(n => n.Name == HardpointName);
        }
    }

    public class LoadoutSlotDTO
    {
        public string PortName { get; set; }
        public bool Editable { get; set; }
        public bool Visible { get; set; }
        public string[] Types { get; set; }
        public string[] Tags { get; set; }
        public string[] Flags { get; set; }
        public string[] RequiredTags { get; set; }
        public int? MinSize { get; set; }
        public int? MaxSize { get; set; }
        public float Mass { get; set; }
        public float Health { get; set; }
        public List<LoadoutSlotDTO> Slots { get; set; }
        public List<PortConnection> Connections { get; set; }
    }

    public class LoadoutEntryDTO
    {
        public string PortName { get; set; }
        public string ComponentKey { get; set; }

        public List<LoadoutEntryDTO> Children { get; set; }
    }

}
