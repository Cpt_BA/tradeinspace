﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Xml;
using TradeInSpace.Explore.SC.Components;

namespace TradeInSpace.Explore.SC.Loadout
{
    [DebuggerDisplay("[{PortName} - {Class}] {ID} ({Mass} kg, {Health} HP)")]
    public class LoadoutEntry
    {
        public List<LoadoutEntry> Slots { get; private set; } = new List<LoadoutEntry>();

        public List<LoadoutEntry> Children { get; private set; } = new List<LoadoutEntry>();
        public IEnumerable<LoadoutEntry> DisplayChildren
        {
            get
            {
                return Slots.Where(c => 
                                (c.Port != null && c.Port.Editable) || 
                                c.DisplayChildren.Count() > 0);
            }
        }

        public LoadoutEntry Parent { get; private set; }
        public LoadoutTree Owner { get; private set; }

        //Bit of a hack, there is probably a better place for this.
        public List<(string, string)> SubPartModelPaths { get; internal set; } = new List<(string, string)>();

        public string ID { get; private set; }
        public string PortName { get; private set; }
        public string Class { get; private set; }
        public float Mass { get; internal set; }
        public float Health { get; internal set; }

        public LoadoutPort Port { get; private set; }
        
        public XmlElement EntryElement { get; private set; }

        //HACK FOR WPF: TODO: Remove
        public IEnumerable<LoadoutTree> AttachedTree
        {
            get
            {
                if (Port?.ComponentLoadout != null) yield return Port.ComponentLoadout;
                yield break;
            }
        }


        private LoadoutEntry()
        {

        }

        public static LoadoutEntry FromAttachableXML
            (LoadoutTree rootLoadout, XmlElement element, LoadoutEntry parent = null)
        {
            var entry = new LoadoutEntry()
            {
                Parent = parent,
                Owner = rootLoadout,

                PortName = element.GetAttribute("name"),
                Class = element.GetAttribute("class"),

                //Where do we put these additional weights?
                Mass = 0,
                Health = 0,

                EntryElement = element
            };

            if (element.HasAttribute("id")) entry.ID = element.GetAttribute("id");
            else if (element.HasAttribute("idRef")) entry.ID = element.GetAttribute("idRef");

            if (float.TryParse(element.GetAttribute("mass"), out var mass)) { entry.Mass = mass; }
            if (float.TryParse(element.GetAttribute("damageMax"), out var maxDmg)) { entry.Health = maxDmg; }

            parent?.Slots.Add(entry);
            parent?.Children?.Add(entry);

            var itemPort = element["ItemPort"];
            if (itemPort != null)
            {
                entry.Port = LoadoutPort.FromVehiclePart(rootLoadout, itemPort, entry);
            }
            else if(element.GetAttribute("class") == "ItemPort")
            {
                entry.Port = LoadoutPort.EmptyPort(element, entry);
            }

            //Get immediate children for some reason
            foreach (var childPart in element.SelectNodes("./Part | ./Parts/Part").OfType<XmlElement>())
            {
                var loadedPart = LoadoutEntry.FromAttachableXML(rootLoadout, childPart, entry);
                //FromVehiclePart will automatically add the child to the passed parent.
            }

            //<SubPart filename="objects/vehicles/drak/mule/DRAK_Mule_Wheel_Left.cga" geometryname="wheel_assembly_mesh_left" />
            foreach (var sub_model in element.SelectNodes("./SubPart").OfType<XmlElement>())
            {
                entry.SubPartModelPaths.Add((sub_model.GetAttribute("filename"), sub_model.GetAttribute("geometryname")));
            }

            return entry;
        }

        public static LoadoutEntry FromEntity
            (LoadoutTree loadout, SCEntityClassDef entity, LoadoutEntry attachedTo)
        {
            var itemDef = entity.GetComponent<SCItem>();
            var attachDef = entity.GetComponent<AttachableComponent>();
            var itemPortDef = entity.GetComponent("SItemPortContainerComponentParams");

            //If we don't have either, dont create a psuedo-root
            //Otherwise *every* piece of equipment gets un-needed ports.
            if (itemDef == null && itemPortDef == null)
                return null;

            var entry = new LoadoutEntry()
            {
                Owner = loadout,
                Parent = attachedTo,

                PortName = attachDef.Name,
                Class = "Item",

                EntryElement = entity.Element
            };
            attachedTo?.Children.Add(entry);

            var itemPort = entity.Element["ItemPort"];
            if(itemPort != null)
            {

            }
            else
            {

            }

            if (itemDef != null)
                entry.AddComponentPorts(itemDef, "./ItemPorts/SItemPortCoreParams/Ports/SItemPortDef");

            if(itemPortDef != null)
                entry.AddComponentPorts(itemPortDef, "./Ports/SItemPortDef");

            //TODO: furthur nesting evaluation?
            return entry;
        }

        private void AddComponentPorts(SCComponent component, string XPath)
        {
            var portElements = component.Element.SelectNodes(XPath).OfType<XmlElement>();

            foreach (var portChild in portElements)
            {
                var portEntry = new LoadoutEntry()
                {
                    Parent = this,
                    Owner = Owner,

                    PortName = portChild.GetAttribute("Name"),
                    Class = "ItemPort",

                    EntryElement = portChild
                };

                portEntry.Port = LoadoutPort.FromComponentDefinition(portChild, portEntry);

                Slots.Add(portEntry);
                Children.Add(portEntry);
            }
        }
    }
}
