﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Xml;
using TradeInSpace.Explore.DataForge;
using TradeInSpace.Explore.SC.Data;

namespace TradeInSpace.Explore.SC
{
    [DebuggerDisplay("SCEntityClass: {InstanceName}")]
    [DFEntry("EntityClassDefinition", ParsePriority = 10)]
    public class SCEntityClassDef : DFEntry
    {
        public SCEntityClassDef(XmlElement fromElement, DFDatabase gameDatabase) : base(fromElement, gameDatabase)
        {
            CreateComponents();
        }

        public SCComponent GetComponent(string ComponentName)
        {
            return Components.SingleOrDefault(c => c.Type == ComponentName);
        }

        public List<SCComponent> Components { get; set; }

        internal override void OnDFEntriesCompleted()
        {
            foreach (var component in Components)
            {
                component.OnDFEntriesCompleted();
            }

            base.OnDFEntriesCompleted();
        }

        private void CreateComponents()
        {
            Components = new List<SCComponent>();

            foreach (XmlElement component in this.Element["Components"])
            {
                var availableComponents = SCEntityComponentAttribute.FindAllLoaded();
                var elementType = component.GetAttribute("__polymorphicType");
                var matchingComponent = availableComponents.Where(c => c.Component.PolymorphicTypes.Contains(elementType));

                if (matchingComponent.Count() > 1)
                {
                    var names = string.Join(",", matchingComponent.Select(c => c.Class.Name));
                    throw new Exception($"Multiple components defined under same name: {elementType}. ({names})");
                }
                else if (matchingComponent.Count() == 1)
                {
                    var compInstance = Activator.CreateInstance(matchingComponent.First().Class, new object[] { component, this }) as SCComponent;
                    Components.Add(compInstance);
                }
                else
                {
                    Components.Add(new SCComponent(component, this));
                }
            }
        }

        public bool HasComponent<T>()
            where T : SCComponent
        {
            return Components.OfType<T>().Count() > 0;
        }

        public T GetComponent<T>()
            where T : SCComponent
        {
            return Components.OfType<T>().FirstOrDefault();
        }
    }

    [AttributeUsage(AttributeTargets.Class)]
    public class SCEntityComponentAttribute : Attribute
    {
        public string[] PolymorphicTypes { get; set; }

        private static List<(Type Class, SCEntityComponentAttribute Component)> LoadedComponents;

        public SCEntityComponentAttribute(string polymorphicType)
        {
            PolymorphicTypes = new string[] { polymorphicType };
        }

        public SCEntityComponentAttribute(params string[] polymorphicTypes)
        {
            PolymorphicTypes = polymorphicTypes;
        }

        public static IEnumerable<(Type Class, SCEntityComponentAttribute Component)> FindAllLoaded()
        {
            if (LoadedComponents == null)
            {
                LoadedComponents = new List<(Type Class, SCEntityComponentAttribute Component)>();
                var domainTypes = AppDomain.CurrentDomain.GetAssemblies().SelectMany(a => a.GetTypes());

                foreach (Type type in domainTypes)
                {
                    var matchingAttr = type.GetCustomAttribute(typeof(SCEntityComponentAttribute), true) as SCEntityComponentAttribute;
                    if (matchingAttr != null)
                    {
                        LoadedComponents.Add((type, matchingAttr));
                    }
                }
            }

            return LoadedComponents;
        }
    }
}
