﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using TradeInSpace.Explore.DataForge;

namespace TradeInSpace.Explore.SC
{

    public class SCComponent : DFBase
    {
        internal SCComponent(XmlElement fromElement, SCEntityClassDef parent) : base(fromElement)
        {
            Type = fromElement.GetAttribute("__polymorphicType");
            ParentEntity = parent;
        }

        public SCEntityClassDef ParentEntity { get; set; }
    }
}
