﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using TradeInSpace.Explore.DataForge;

namespace TradeInSpace.Explore.SC.Params
{
    public class MissionLocationParams
    {
        private string actionAreaSuperGUID;
        private string objectContainerSuperGUID;

        public string ActionAreaSuperGUID
        {
            get => actionAreaSuperGUID;
            set
            {
                actionAreaSuperGUID = value;
                if(value.Contains(','))
                    FinalAAGUID = Guid.Parse(value.Split(',').Last());
            }
        }
        public string ObjectContainerSuperGUID
        {
            get => objectContainerSuperGUID;
            set
            {
                objectContainerSuperGUID = value;
                if (value.Contains(','))
                    FinalContainerGUID = Guid.Parse(value.Split(',').Last());
            }
        }


        public Guid FinalAAGUID { get; private set; } = Guid.Empty;
        public Guid FinalContainerGUID { get; private set; } = Guid.Empty;


        public List<Guid> Tags { get; set; }
        public List<Guid> ProducesTags { get; set; }
        public List<Guid> ConsumesTags { get; set; }
        public Dictionary<string, string> StringVariants { get; set; }

        public static MissionLocationParams FromXML(XmlElement fromElement, DFDatabase rawGameDB)
        {
            return new MissionLocationParams()
            {
                ActionAreaSuperGUID = fromElement.GetAttribute("actionAreaSuperGuid"),
                ObjectContainerSuperGUID = fromElement.GetAttribute("objectContainerSuperGuid"),
                Tags = fromElement.SelectNodes("./generalTags/tags/Reference/@value")
                    .OfType<XmlAttribute>().Select(t => new Guid(t.Value)).ToList(),
                ProducesTags = fromElement.SelectNodes("./producesTags/tags/Reference/@value")
                    .OfType<XmlAttribute>().Select(t => new Guid(t.Value)).ToList(),
                ConsumesTags = fromElement.SelectNodes("./consumesTags/tags/Reference/@value")
                    .OfType<XmlAttribute>().Select(t => new Guid(t.Value)).ToList(),
                StringVariants = fromElement
                    .SelectNodes("./stringVariants/variants/MissionStringVariant")
                    .Cast<XmlElement>()
                    .GroupBy(xe => xe.GetAttribute("tag")) //Have to do this because of a single location
                    .ToDictionary(grp => grp.Key,
                                    grp => rawGameDB.Localize(grp.Last().GetAttribute("string")))
            };
        }
    }
}
