﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace TradeInSpace.Explore.SC.Components
{
    [SCEntityComponent("SCItemVehicleArmorParams")]
    [JsonObject(MemberSerialization.OptIn)]
    public class ArmorComponent : SCComponent
    {
        [JsonProperty]
        public float SignalInfrared { get; private set; }
        [JsonProperty]
        public float SignalElectromagnetic { get; private set; }
        [JsonProperty]
        public float SignalCrossSection { get; private set; }


        [JsonProperty]
        public float DamagePhysical { get; private set; }
        [JsonProperty]
        public float DamageEnergy { get; private set; }
        [JsonProperty]
        public float DamageDistortion { get; private set; }
        [JsonProperty]
        public float DamageThermal { get; private set; }
        [JsonProperty]
        public float DamageBiochemical { get; private set; }
        [JsonProperty]
        public float DamageStun { get; private set; }

        public ArmorComponent(XmlElement fromElement, SCEntityClassDef parent) : base(fromElement, parent)
        {
            SignalInfrared = float.Parse(fromElement.GetAttribute("signalInfrared"));
            SignalElectromagnetic = float.Parse(fromElement.GetAttribute("signalElectromagnetic"));
            SignalCrossSection = float.Parse(fromElement.GetAttribute("signalCrossSection"));

            DamagePhysical = float.Parse(fromElement.SelectSingleNode("./damageMultiplier/DamageInfo/@DamagePhysical").Value);
            DamageEnergy = float.Parse(fromElement.SelectSingleNode("./damageMultiplier/DamageInfo/@DamageEnergy").Value);
            DamageDistortion = float.Parse(fromElement.SelectSingleNode("./damageMultiplier/DamageInfo/@DamageDistortion").Value);
            DamageThermal = float.Parse(fromElement.SelectSingleNode("./damageMultiplier/DamageInfo/@DamageThermal").Value);
            DamageBiochemical = float.Parse(fromElement.SelectSingleNode("./damageMultiplier/DamageInfo/@DamageBiochemical").Value);
            DamageStun = float.Parse(fromElement.SelectSingleNode("./damageMultiplier/DamageInfo/@DamageStun").Value);
        }
    }
}
