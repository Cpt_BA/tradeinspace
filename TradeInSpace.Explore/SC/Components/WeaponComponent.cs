﻿using Newtonsoft.Json;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using TradeInSpace.Explore.DataForge;
using TradeInSpace.Models;

namespace TradeInSpace.Explore.SC.Components
{
    [SCEntityComponent("SCItemWeaponComponentParams")]
    [JsonObject(MemberSerialization.OptIn)]
    public class WeaponComponent : SCComponent
    {
        [JsonProperty]
        public List<FireAction> Actions { get; private set; } = null;
        [JsonProperty]
        public RegenConsumerParams RegenParams { get; private set; } = null;


        [JsonProperty]
        public float OverpowerRate { get; private set; } = -1.0f;
        [JsonProperty]
        public float UnderpowerRate { get; private set; } = -1.0f;


        public WeaponComponent(XmlElement fromElement, SCEntityClassDef parent) : base(fromElement, parent)
        {
            Actions = fromElement
                .SelectNodes("./fireActions/*")
                .OfType<XmlElement>()
                .Select(e => FireAction.FromXML(e, parent.GameDatabase, this))
                .Where(e => e != null)
                .ToList();

            var opStats = fromElement.SelectSingleNode("./connectionParams/overpowerStats");
            var upStats = fromElement.SelectSingleNode("./connectionParams/underpowerStats");
            var regenNode = fromElement.SelectSingleNode("./weaponRegenConsumerParams/SWeaponRegenConsumerParams");

            if(opStats != null)
                OverpowerRate = float.Parse(opStats.Attributes["fireRateMultiplier"].Value);

            if(upStats != null)
                UnderpowerRate = float.Parse(upStats.Attributes["fireRateMultiplier"].Value);

            if (regenNode != null)
            {
                RegenParams = regenNode.Deserialize<RegenConsumerParams>(RootElementName: "SWeaponRegenConsumerParams");
            }
        }
    }

    public class RegenConsumerParams
    {
        [XmlAttribute("requestedRegenPerSec")]
        public string RequestedRegenPerSec { get; set; }
        [XmlAttribute("regenerationCooldown")]
        public string RegenerationCooldown { get; set; }
        [XmlAttribute("regenerationCostPerBullet")]
        public string RegenerationCostPerBullet { get; set; }
        [XmlAttribute("requestedAmmoLoad")]
        public string RequestedAmmoLoad { get; set; }
    }

    public class FireAction
    {
        public string ModeName { get; protected set; }
        public string Name { get; protected set; }
        [JsonIgnore]
        public XmlElement FromElement { get; }

        internal FireAction(XmlElement fromElement, DFDatabase rawGameData)
        {
            FromElement = fromElement;

            ModeName = fromElement.GetAttribute("name");
            Name = rawGameData.Localize(fromElement.GetAttribute("localisedName"));
        }

        public static FireAction FromXML(XmlElement fromElement, DFDatabase gameData, WeaponComponent parentComponent)
        {
            var actionType = fromElement.LocalName;
            FireAction createdAction = null;
            try
            {
                switch (actionType)
                {
                    case "SWeaponActionSequenceParams":
                        createdAction = new FireActionSequence(fromElement, gameData, parentComponent);
                        break;
                    case "SWeaponActionFireSingleParams":
                    case "SWeaponActionFireRapidParams":
                        createdAction = new FireActionBasic(fromElement, gameData);
                        break;
                    case "SWeaponActionFireBeamParams":
                        createdAction = new FireActionBeam(fromElement, gameData);
                        break;
                    case "SWeaponActionFireChargedParams":
                        createdAction = new FireActionCharged(fromElement, gameData);
                        break;
                    case "SWeaponActionFireBurstParams":
                        createdAction = new FireActionBurst(fromElement, gameData);
                        break;
                    case "SWeaponActionConditionParams":
                    case "SWeaponActionFireEnergyParams":
                    case "SWeaponActionGatheringBeamParams":
                    case "SWeaponActionMeleeParams":
                    case "SWeaponActionFireHealingBeamParams":
                    case "SWeaponActionFireTractorBeamParams":
                    case "SWeaponActionFireSalvageRepairParams":
                        break;
                    default:
                        throw new Exception($"Unknown weapon fire type: {actionType}");
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, $"Error parsing weapon action (type: {actionType}) {ex.Message}");
                Log.Error($"Parent Component: {parentComponent.Type}");
                Log.Error($"Parent Entity ID: {parentComponent.ParentEntity?.ID}");
                Log.Error($"Parent Entity Name: {parentComponent.ParentEntity?.InstanceName}");
                Log.Error($"Offending XML: ");
                Log.Error(fromElement.OuterXml);
            }
            return createdAction;
        }
    }

    public class FireActionSequence : FireAction
    {
        internal FireActionSequence(XmlElement fromElement, DFDatabase gameData, WeaponComponent parentComponent)
            :base(fromElement, gameData)
        {
            ModeName = fromElement.GetAttribute("mode");
            FireActions = fromElement
                .SelectNodes("./sequenceEntries/SWeaponSequenceEntryParams/weaponAction/*")
                .OfType<XmlElement>()
                .Select(e => FireAction.FromXML(e, gameData, parentComponent))
                .Where(fa => fa != null)
                .ToList();
        }

        public List<FireAction> FireActions { get; protected set; }
    }

    public class FireActionBasic : FireAction
    {
        internal FireActionBasic(XmlElement fromElement, DFDatabase gameData)
            : base(fromElement, gameData)
        {
            FireRate = float.Parse(fromElement.GetAttribute("fireRate"));
            HeatPerShot = float.Parse(fromElement.GetAttribute("heatPerShot"));

            var launcher = fromElement.SelectSingleNode("./launchParams/*") as XmlElement;
            switch (launcher.LocalName)
            {
                case "SProjectileLauncher":
                    PelletCount = int.Parse(launcher.GetAttribute("pelletCount"));
                    break;
                case "SMissileLauncher":
                    FireMissile = true;
                    break;
                default:
                    break;
            }
            
        }

        public float FireRate { get; protected set; }
        public float HeatPerShot { get; protected set; }
        public int? PelletCount { get; protected set; } = null;
        public bool FireMissile { get; protected set; } = false;
    }

    public class FireActionBurst : FireActionBasic
    {
        internal FireActionBurst(XmlElement fromElement, DFDatabase gameData)
            : base(fromElement, gameData)
        {
            ShotCount = int.Parse(fromElement.GetAttribute("shotCount"));
        }

        public int ShotCount { get; protected set; }
    }

    public class FireActionBeam : FireAction
    {
        internal FireActionBeam(XmlElement fromElement, DFDatabase gameData)
            : base(fromElement, gameData)
        {
            if (float.TryParse(fromElement.GetAttribute("energyRate"), out var energyRateVar))
                EnergyRate = energyRateVar;
            if (float.TryParse(fromElement.GetAttribute("heatPerSecond"), out var heatRateVar))
                HeatPerSecond = heatRateVar;
            HitType = fromElement.GetAttribute("hitType");

            var dpsInfo = FromElement
                .SelectNodes("./damagePerSecond/DamageInfo/@*[contains(name(),'Damage')]")
                .OfType<XmlAttribute>();

            DPS = dpsInfo
                .Select(a => {
                    if (float.TryParse(a.Value, out var floatVal)) return floatVal;
                    else return 0;
                })
                .Sum();
        }

        public float EnergyRate { get; protected set; }
        public float HeatPerSecond { get; protected set; }
        public float ZeroDamageRange { get; protected set; }
        public float FullDamageRange { get; protected set; }
        public string HitType { get; protected set; }
        public float DPS { get; protected set; }
    }

    public class FireActionCharged : FireAction
    {
        internal FireActionCharged(XmlElement fromElement, DFDatabase gameData)
            : base(fromElement, gameData)
        {
            ChargeTime = float.Parse(fromElement.GetAttribute("chargeTime"));
            CooldownTime = float.Parse(fromElement.GetAttribute("cooldownTime"));
            ChargedMultiplier = float.Parse(fromElement.SelectSingleNode("./maxChargeModifier/@damageMultiplier")?.Value ?? "1");
        }

        public float ChargeTime { get; protected set; }
        public float CooldownTime { get; protected set; }
        public float ChargedMultiplier { get; protected set; }
    }

}
