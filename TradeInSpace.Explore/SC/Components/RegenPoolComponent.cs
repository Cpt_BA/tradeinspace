﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace TradeInSpace.Explore.SC.Components
{
    [SCEntityComponent("SCItemWeaponRegenPoolComponentParams")]
    [JsonObject(MemberSerialization.OptIn)]
    public class RegenPoolComponent : SCComponent
    {
        [JsonProperty]
        public float RegenFillRate { get; private set; }
        [JsonProperty]
        public float AmmoLoad { get; private set; }

        public RegenPoolComponent(XmlElement fromElement, SCEntityClassDef parent) : base(fromElement, parent)
        {
            RegenFillRate = float.Parse(fromElement.GetAttribute("regenFillRate"));
            AmmoLoad = float.Parse(fromElement.GetAttribute("ammoLoad"));
        }
    }
}
