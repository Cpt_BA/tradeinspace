﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using TradeInSpace.Explore.DataForge;
using TradeInSpace.Models;
using ModifierType = TradeInSpace.Models.Enums.ModifierType;
using ModifierMode = TradeInSpace.Models.Enums.ModifierMode;
using ModifierActivation = TradeInSpace.Models.Enums.ModifierActivation;

namespace TradeInSpace.Explore.SC.Components
{
    [SCEntityComponent("EntityComponentAttachableModifierParams", "SWeaponModifierComponentParams")]
    [JsonObject(MemberSerialization.OptIn)]
    public class AttachableModifierComponent : SCComponent
    {
        [JsonProperty]
        public List<AttachableModifierParams> Modifiers { get; set; }

        [JsonProperty]
        public ModifierActivation Activation { get; set; }

        [JsonProperty]
        public bool Interruptable { get; set; } = false;

        [JsonProperty]
        public int Charges { get; set; } = -1;

        public AttachableModifierComponent(XmlElement fromElement, SCEntityClassDef parent) : base(fromElement, parent)
        {
            Charges = fromElement.ReadAttributeTyped<int>("charges", -1);
            Interruptable = fromElement.ReadAttributeTyped<int>("canInterrupt", 0) == 1;
            switch(fromElement.ReadAttributeTyped<string>("activationMethod", "ActivateOnDemand"))
            {
                case "ActivateOnDemand":
                    Activation = ModifierActivation.Active;
                    break;
                case "ActivateOnAttach":
                    Activation = ModifierActivation.Passive;
                    break;
            }

            Modifiers = 
                fromElement.SelectNodes("./modifiers/*").OfType<XmlElement>()
                .Union(fromElement.SelectNodes("./modifier/*").OfType<XmlElement>())
                .SelectMany(e => AttachableModifierParams.FromRootElement(e, parent.GameDatabase))
                .Where(m => m.Value != 1.0f) //Ignore any modifiers that don't do anything
                .ToList();
        }
    }

    [GenerateTS]
    public class AttachableModifierParams
    {

        public AttachableModifierParams(float lifetime, float delay, int actionIndex, ModifierType type, ModifierMode mode, float amount, 
            bool isPercentagePoints = false)
        {
            ActionIndex = actionIndex;
            Lifetime = lifetime;
            Delay = delay;
            Type = type;
            Mode = mode;
            Value = amount;

            if (isPercentagePoints)
            {
                Value = 1.0f + (Value / 100.0f);
            }
        }

        public ModifierType Type { get; set; }
        public ModifierMode Mode { get; set; }
        public float Value { get; set; }
        public float Lifetime { get; set; } = -1;
        public float Delay { get; set; } = 0;
        public int ActionIndex { get; set; }

        private static AttachableModifierParams FromAttribute(float lifetime, float delay, int actionIndex, XmlElement element, string attributeName, 
            ModifierType type, ModifierMode mode = ModifierMode.MultiplicitivePost, bool isPercentagePoints = false)
        {
            return new AttachableModifierParams(lifetime, delay, actionIndex, type, mode, float.Parse(element.GetAttribute(attributeName)), 
                isPercentagePoints: isPercentagePoints);
        }

        private static AttachableModifierParams FromAttribute(float lifetime, float delay, int actionIndex, XmlAttribute attribute, 
            ModifierType type, ModifierMode mode = ModifierMode.MultiplicitivePost, bool isPercentagePoints = false)
        {
            return new AttachableModifierParams(lifetime, delay, actionIndex, type, mode, float.Parse(attribute.Value), 
                isPercentagePoints: isPercentagePoints);
        }

        private static IEnumerable<AttachableModifierParams> FromXPath(float lifetime, float delay, int actionIndex, XmlElement element, string xpath, 
            ModifierType type, ModifierMode mode = ModifierMode.MultiplicitivePost, bool isPercentagePoints = false)
        {
            var result = element.SelectSingleNode(xpath);
            switch (result)
            {
                case XmlAttribute attr:
                    yield return FromAttribute(lifetime, delay, actionIndex, attr, type, mode, 
                        isPercentagePoints: isPercentagePoints);
                    break;
                case XmlElement elem:
                    throw new NotSupportedException("Elements not supported yet");
                default:
                    break;
            }

            yield break;
        }

        private static IEnumerable<AttachableModifierParams> FromXPaths(float lifetime, float delay, int actionIndex, XmlElement element, 
            params (ModifierType Type, ModifierMode Mode, string XPath)[] selectors)
        {
            foreach (var selector in selectors)
            {
                foreach(var result in FromXPath(lifetime, delay, actionIndex, element, selector.XPath, selector.Type, selector.Mode))
                {
                    yield return result;
                }
            }
        }

        private static IEnumerable<AttachableModifierParams> FromXPathss(float lifetime, float delay, int actionIndex, XmlElement element,
            params XPathModifierSelector[] selectors)
        {
            foreach (var selector in selectors)
            {
                foreach (var result in FromXPath(lifetime, delay, actionIndex, element, selector.XPathSelector, selector.Type, selector.Mode, selector.IsPercentagePoints))
                {
                    yield return result;
                }
            }
        }

        public static IEnumerable<AttachableModifierParams> FromRootElement(XmlElement fromElement, DFDatabase gameData, float delay = 0)
        {
            var actionType = fromElement.LocalName;
            var actionIndex = fromElement.ReadAttributeTyped<int>("fireActionIndex");

            var lifetimeAttr = fromElement.SelectSingleNode("./modifierLifetime/ItemModifierTimedLife/@lifetime") as XmlAttribute;
            float lifetime = -1;

            if(lifetimeAttr != null)
            {
                lifetime = float.Parse(lifetimeAttr.Value);
            }

            //bool attachable = (actionType == "ItemMineableRockModifierParams");

            foreach (var item in FromXPathss(lifetime, delay, actionIndex, fromElement, 
                //ItemMiningModifierParams
                new XPathModifierSelector(ModifierType.MiningResistance, ModifierMode.MultiplicitivePost, "./MiningLaserModifier/@resistanceModifier"),
                new XPathModifierSelector(ModifierType.MiningInstability, ModifierMode.MultiplicitivePost, "./MiningLaserModifier/laserInstability/FloatModifierMultiplicative/@value", true),
                new XPathModifierSelector(ModifierType.MiningOptimalWindowSize, ModifierMode.MultiplicitivePost, "./MiningLaserModifier/optimalChargeWindowSizeModifier/FloatModifierMultiplicative/@value", true),
                new XPathModifierSelector(ModifierType.MiningShatterDamage, ModifierMode.MultiplicitivePost, "./MiningLaserModifier/shatterdamageModifier/FloatModifierMultiplicative/@value", true),
                new XPathModifierSelector(ModifierType.MiningOptimalWindowRate, ModifierMode.MultiplicitivePost, "./MiningLaserModifier/optimalChargeWindowRateModifier/FloatModifierMultiplicative/@value", true),
                new XPathModifierSelector(ModifierType.MiningCatastrophicWindowRate, ModifierMode.MultiplicitivePost, "./MiningLaserModifier/catastrophicChargeWindowRateModifier/FloatModifierMultiplicative/@value", true),
                //ItemHeatModifierParams
                new XPathModifierSelector(ModifierType.Heat, ModifierMode.MultiplicitivePost, "./@heatGenerationMultiplier"),
                //ItemSignatureModifierParams
                new XPathModifierSelector(ModifierType.Signature, ModifierMode.MultiplicitivePost, "./@signatureSizeMultiplier"),
                //ItemWeaponBeamVFXParams
                new XPathModifierSelector(ModifierType.WeaponBeamStrengthVFX, ModifierMode.MultiplicitivePost, "./strengthOverride/ItemWeaponBeamVFXStrengthOverrideParams/@strength"),
                //ItemMiningBoosterParams (This appears to be a modifier to the base mining window level, not really power)
                new XPathModifierSelector(ModifierType.MiningWindowLevel, ModifierMode.MultiplicitivePost, "./@powerLevelChange", true),
                //MiningFilterParams
                new XPathModifierSelector(ModifierType.MiningFilter, ModifierMode.MultiplicitivePost, "./@filterPercentage", true),
                //ItemWeaponModifiersParams
                new XPathModifierSelector(ModifierType.WeaponFireRate, ModifierMode.MultiplicitivePost, "./weaponModifier/weaponStats/@fireRateMultiplier"),
                new XPathModifierSelector(ModifierType.WeaponDamage, ModifierMode.MultiplicitivePost, "./weaponModifier/weaponStats/@damageMultiplier"),
                new XPathModifierSelector(ModifierType.WeaponProjectileSpeed, ModifierMode.MultiplicitivePost, "./weaponModifier/weaponStats/@projectileSpeedMultiplier"),
                new XPathModifierSelector(ModifierType.WeaponAmmoCost, ModifierMode.MultiplicitivePost, "./weaponModifier/weaponStats/@ammoCostMultiplier"),
                new XPathModifierSelector(ModifierType.WeaponHeatGeneration, ModifierMode.MultiplicitivePost, "./weaponModifier/weaponStats/@heatGenerationMultiplier"),
                new XPathModifierSelector(ModifierType.WeaponSoundRadius, ModifierMode.MultiplicitivePost, "./weaponModifier/weaponStats/@soundRadiusMultiplier"),
                new XPathModifierSelector(ModifierType.WeaponRecoilDecay, ModifierMode.MultiplicitivePost, "./weaponModifier/weaponStats/recoilModifier/@decayMultiplier"),
                new XPathModifierSelector(ModifierType.WeaponSpreadMin, ModifierMode.MultiplicitivePost, "./weaponModifier/weaponStats/spreadModifier/@minMultiplier"),
                new XPathModifierSelector(ModifierType.WeaponSpreadMax, ModifierMode.MultiplicitivePost, "./weaponModifier/weaponStats/spreadModifier/@maxMultiplier"),
                new XPathModifierSelector(ModifierType.WeaponSpreadFirst, ModifierMode.MultiplicitivePost, "./weaponModifier/weaponStats/spreadModifier/@firstAttackMultiplier"),
                new XPathModifierSelector(ModifierType.WeaponSpreadAttack, ModifierMode.MultiplicitivePost, "./weaponModifier/weaponStats/spreadModifier/@attackMultiplier"),
                new XPathModifierSelector(ModifierType.WeaponSpreadDecy, ModifierMode.MultiplicitivePost, "./weaponModifier/weaponStats/spreadModifier/@decayMultiplier"),
                new XPathModifierSelector(ModifierType.WeaponZoom, ModifierMode.MultiplicitivePost, "./weaponModifier/weaponStats/aimModifier/@zoomScale"),
                new XPathModifierSelector(ModifierType.SalvageSpeed, ModifierMode.MultiplicitivePost, "./weaponModifier/weaponStats/salvageModifier/@salvageSpeedMultiplier"),
                new XPathModifierSelector(ModifierType.SalvageRadius, ModifierMode.MultiplicitivePost, "./weaponModifier/weaponStats/salvageModifier/@radiusMultiplier"),
                new XPathModifierSelector(ModifierType.SalvageEfficiency, ModifierMode.MultiplicitivePost, "./weaponModifier/weaponStats/salvageModifier/@extractionEfficiency"),
                //Duplicates of above for when processing directly on a weaponModifier element
                new XPathModifierSelector(ModifierType.WeaponFireRate, ModifierMode.MultiplicitivePost, "./@fireRateMultiplier"),
                new XPathModifierSelector(ModifierType.WeaponDamage, ModifierMode.MultiplicitivePost, "./@damageMultiplier"),
                new XPathModifierSelector(ModifierType.WeaponProjectileSpeed, ModifierMode.MultiplicitivePost, "./@projectileSpeedMultiplier"),
                new XPathModifierSelector(ModifierType.WeaponAmmoCost, ModifierMode.MultiplicitivePost, "./@ammoCostMultiplier"),
                new XPathModifierSelector(ModifierType.WeaponHeatGeneration, ModifierMode.MultiplicitivePost, "./@heatGenerationMultiplier"),
                new XPathModifierSelector(ModifierType.WeaponSoundRadius, ModifierMode.MultiplicitivePost, "./@soundRadiusMultiplier"),
                new XPathModifierSelector(ModifierType.WeaponRecoilDecay, ModifierMode.MultiplicitivePost, "./recoilModifier/@decayMultiplier"),
                new XPathModifierSelector(ModifierType.WeaponSpreadMin, ModifierMode.MultiplicitivePost, "./spreadModifier/@minMultiplier"),
                new XPathModifierSelector(ModifierType.WeaponSpreadMax, ModifierMode.MultiplicitivePost, "./spreadModifier/@maxMultiplier"),
                new XPathModifierSelector(ModifierType.WeaponSpreadFirst, ModifierMode.MultiplicitivePost, "./spreadModifier/@firstAttackMultiplier"),
                new XPathModifierSelector(ModifierType.WeaponSpreadAttack, ModifierMode.MultiplicitivePost, "./spreadModifier/@attackMultiplier"),
                new XPathModifierSelector(ModifierType.WeaponSpreadDecy, ModifierMode.MultiplicitivePost, "./spreadModifier/@decayMultiplier"),
                new XPathModifierSelector(ModifierType.WeaponZoom, ModifierMode.MultiplicitivePost, "./aimModifier/@zoomScale"),
                //ItemMineableRockModifierParams TODO,
                new XPathModifierSelector(ModifierType.SalvageSpeed, ModifierMode.MultiplicitivePost, "./salvageModifier/@salvageSpeedMultiplier"),
                new XPathModifierSelector(ModifierType.SalvageRadius, ModifierMode.MultiplicitivePost, "./salvageModifier/@radiusMultiplier"),
                new XPathModifierSelector(ModifierType.SalvageEfficiency, ModifierMode.MultiplicitivePost, "./salvageModifier/@extractionEfficiency")
                ))
                yield return item;

            //DelayedModifierTriggerParams
            if(actionType == "DelayedModifierTriggerParams")
            {
                var actionDelay = float.Parse(fromElement.SelectSingleNode("./@delay").Value);

                //Call this recursively for delayed actions, adding the delays as we go
                foreach (var delayedItem in fromElement.SelectNodes("./modifier/*").OfType<XmlElement>())
                {
                    foreach(var delayedModifier in FromRootElement(delayedItem, gameData, actionDelay + delay))
                    {
                        yield return delayedModifier;
                    }
                }
            }
        }

        private class XPathModifierSelector
        {
            public XPathModifierSelector(ModifierType type, ModifierMode mode, string xPathSelector, bool isPercentagePoints = false)
            {
                Type = type;
                Mode = mode;
                XPathSelector = xPathSelector;
                IsPercentagePoints = isPercentagePoints;
            }

            public ModifierType Type { get; set; }
            public ModifierMode Mode { get; set; }
            public string XPathSelector { get; set; }
            public bool IsPercentagePoints { get; set; } = false;

            public static implicit operator XPathModifierSelector(Tuple<ModifierType, ModifierMode, string> tuple)
            {
                return new XPathModifierSelector(tuple.Item1, tuple.Item2, tuple.Item3);
            }

            public static implicit operator XPathModifierSelector(Tuple<ModifierType, ModifierMode, string, bool> tuple)
            {
                return new XPathModifierSelector(tuple.Item1, tuple.Item2, tuple.Item3, tuple.Item4);
            }
        }
    }
}
