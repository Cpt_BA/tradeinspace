﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace TradeInSpace.Explore.SC.Components
{
    [SCEntityComponent("SHealthComponentParams")]
    public class HealthComponent:SCComponent
    {
        public float? Health { get; private set; } = null;

        public HealthComponent(XmlElement fromElement, SCEntityClassDef parent) : base(fromElement, parent)
        {
            if (int.TryParse(fromElement.GetAttribute("Health"), out var out_health))
            {
                Health = out_health;
            }
        }
    }
}
