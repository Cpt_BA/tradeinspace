﻿using Serilog;
using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace TradeInSpace.Explore.SC.Components
{
    [SCEntityComponent("SEntityPhysicsControllerParams")]
    public class PhysicsComponent : SCComponent
    {
        public float? Mass { get; private set; } = null;

        public PhysicsComponent(XmlElement fromElement, SCEntityClassDef parent) : base(fromElement, parent)
        {
            var physTypes = fromElement["PhysType"];
            var rigidParams = physTypes["SEntityRigidPhysicsControllerParams"];
            if (rigidParams != null)
            {
                if(float.TryParse(rigidParams.GetAttribute("Mass"), out var out_mass))
                {
                    Mass = out_mass;
                }
                else
                {
                    Log.Warning($"Invalid mass valid {rigidParams.GetAttribute("Mass")}");
                }
            }
        }
    }
}
