﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using TradeInSpace.Explore.SC.Data;

namespace TradeInSpace.Explore.SC.Components
{
    [SCEntityComponent("SCItemSuitArmorParams")]
    [JsonObject(MemberSerialization.OptIn)]
    public class SuitArmorComponent : SCComponent
    {
        [JsonProperty]
        public string[] ProtectedBodyParts { get; private set; }
        [JsonProperty]
        public DamageInfo Resistance { get; private set; }

        public SuitArmorComponent(XmlElement fromElement, SCEntityClassDef parent) : base(fromElement, parent)
        {
            var resistanceGUID = Guid.Parse(fromElement.GetAttribute("damageResistance"));
            Resistance = ParentEntity.GameDatabase.GetEntry<DamageResistance>(resistanceGUID)?.Resistance;

            ProtectedBodyParts = fromElement
                .SelectNodes("protectedBodyParts/Reference/@value")
                .Cast<XmlAttribute>()
                .Select(a => Guid.Parse(a.Value))
                .Select(g => ParentEntity.GameDatabase.GetEntry<BodyPart>(g).Name)
                .ToArray();
        }
    }
}
