﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace TradeInSpace.Explore.SC.Components
{
    [SCEntityComponent("IFCSParams")]
    [JsonObject(MemberSerialization.OptIn)]
    public class IFCSComponent : SCComponent
    {
        [JsonProperty]
        public float MaxSpeed { get; private set; }
        [JsonProperty]
        public float MaxBoostSpeed { get; private set; }

        public IFCSComponent(XmlElement fromElement, SCEntityClassDef parent) : base(fromElement, parent)
        {
            if (float.TryParse(fromElement.GetAttribute("scmSpeed"), out var scmSpeed))
                MaxSpeed = scmSpeed;
            if (float.TryParse(fromElement.GetAttribute("maxSpeed"), out var maxSpeed))
                MaxBoostSpeed = maxSpeed;
        }
    }
}
