﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace TradeInSpace.Explore.SC.Components
{
    [SCEntityComponent("SEntityComponentMiningLaserParams")]
    [JsonObject(MemberSerialization.OptIn)]
    public class MiningLaserComponent : SCComponent
    {
        [JsonProperty]
        public float ResistanceModifier { get; private set; }
        [JsonProperty]
        public float MUL_InstabilityModifier { get; private set; }
        [JsonProperty]
        public float MUL_WindowSizeModifier { get; private set; }
        [JsonProperty]
        public float MUL_ShatterDamageModifier { get; private set; }
        [JsonProperty]
        public float MUL_OptimalChargeRate { get; private set; }
        [JsonProperty]
        public float MUL_OverchargeRate { get; private set; }

        public MiningLaserComponent(XmlElement fromElement, SCEntityClassDef parent) : base(fromElement, parent)
        {
            var modifierSection = fromElement["miningLaserModifiers"];

            ResistanceModifier = float.Parse(modifierSection.GetAttribute("resistanceModifier"));

            MUL_InstabilityModifier = float.Parse(modifierSection["laserInstability"]?["FloatModifierMultiplicative"]?.GetAttribute("value") ?? "0");
            MUL_WindowSizeModifier = float.Parse(modifierSection["optimalChargeWindowSizeModifier"]?["FloatModifierMultiplicative"]?.GetAttribute("value") ?? "0");
            MUL_ShatterDamageModifier = float.Parse(modifierSection["shatterdamageModifier"]?["FloatModifierMultiplicative"]?.GetAttribute("value") ?? "0");
            MUL_OptimalChargeRate = float.Parse(modifierSection["optimalChargeWindowRateModifier"]?["FloatModifierMultiplicative"]?.GetAttribute("value") ?? "0");
            MUL_OverchargeRate = float.Parse(modifierSection["catastrophicChargeWindowRateModifier"]?["FloatModifierMultiplicative"]?.GetAttribute("value") ?? "0");
        }
    }
}
