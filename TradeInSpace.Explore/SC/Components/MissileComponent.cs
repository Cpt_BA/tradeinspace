﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using TradeInSpace.Explore.SC.Data;

namespace TradeInSpace.Explore.SC.Components
{
    [SCEntityComponent("SCItemMissileParams")]
    [JsonObject(MemberSerialization.OptIn)]
    public class MissileComponent : SCComponent
    {
        [JsonProperty]
        public TrackingData TrackingData;
        [JsonProperty]
        public float MissileSpeed;
        [JsonProperty]
        public float IgniteTime;
        [JsonProperty]
        public float ExplosionRadiusMin;
        [JsonProperty]
        public float ExplosionRadiusMax;
        [JsonProperty]
        public DamageInfo ExplosionDamage;

        public MissileComponent(XmlElement fromElement, SCEntityClassDef parent) : base(fromElement, parent)
        {
            if(fromElement["targetingParams"] != null)
            {
                TrackingData = TrackingData.FromXML(fromElement["targetingParams"]);
            }

            IgniteTime = float.Parse(fromElement.GetAttribute("igniteTime"));
            MissileSpeed = float.Parse(fromElement.SelectSingleNode("./GCSParams/@linearSpeed").Value);

            var explosionElement = fromElement["explosionParams"];
            ExplosionRadiusMin = float.Parse(explosionElement.SelectSingleNode("./@minRadius").Value);
            ExplosionRadiusMax = float.Parse(explosionElement.SelectSingleNode("./@maxRadius").Value);
            ExplosionDamage = explosionElement.SelectSingleNode("./damage/DamageInfo")?.Deserialize<DamageInfo>();
        }
    }

    [JsonObject(MemberSerialization.OptIn)]
    public class TrackingData
    {
        [JsonProperty]
        public string SignalType { get; private set; }
        [JsonProperty]
        public float MinimumValue { get; private set; }
        [JsonProperty]
        public float MaxDistance { get; private set; }
        [JsonProperty]
        public float TrackingAngle { get; private set; }
        [JsonProperty]
        public float LockingAngle { get; private set; }
        [JsonProperty]
        public float LockTime { get; private set; }

        private TrackingData() { }
        public static TrackingData FromXML(XmlElement element)
        {
            return new TrackingData()
            {
                SignalType = element.GetAttribute("trackingSignalType"),
                MinimumValue = float.Parse(element.HasAttribute("trackingSignalMin") ? element.GetAttribute("trackingSignalMin") : "-1"),
                MaxDistance = float.Parse(element.HasAttribute("trackingDistanceMax") ? element.GetAttribute("trackingDistanceMax") : "-1"),
                TrackingAngle = float.Parse(element.HasAttribute("trackingAngle") ? element.GetAttribute("trackingAngle") : "-1"),
                LockingAngle = float.Parse(element.HasAttribute("lockingAngle") ? element.GetAttribute("lockingAngle") : "-1"),
                LockTime = float.Parse(element.HasAttribute("lockTime") ? element.GetAttribute("lockTime") : "-1")
            };
        }
    }
}
