﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace TradeInSpace.Explore.SC.Components
{
    [SCEntityComponent("SCItemPowerPlantParams")]
    public class PowerPlantComponent : SCComponent
    {

        public PowerPlantComponent(XmlElement fromElement, SCEntityClassDef parent) : base(fromElement, parent)
        {
        }
    }
}
