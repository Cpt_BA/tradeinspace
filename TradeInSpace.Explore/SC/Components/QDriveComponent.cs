﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace TradeInSpace.Explore.SC.Components
{
    [SCEntityComponent("SCItemQuantumDriveParams")]
    [JsonObject(MemberSerialization.OptIn)]
    public class QDriveComponent : SCComponent
    {
        [JsonProperty]
        public double JumpRange { get; private set; }
        [JsonProperty]
        public float DriveSpeed { get; private set; }
        [JsonProperty]
        public float CooldownTime { get; private set; }
        [JsonProperty]
        public float S1AccelRate { get; private set; }
        [JsonProperty]
        public float S2AccelRate { get; private set; }
        [JsonProperty]
        public float EngageSpeed { get; private set; }
        [JsonProperty]
        public float SpoolTime { get; private set; }
        [JsonProperty]
        public float FuelConsumptionRate { get; private set; }


        public float SplineDriveSpeed { get; private set; }
        public float SplineCooldownTime { get; private set; }
        public float SplineS1AccelRate { get; private set; }
        public float SplineS2AccelRate { get; private set; }
        public float SplineEngageSpeed { get; private set; }
        public float SplineSpoolTime { get; private set; }

        public QDriveComponent(XmlElement fromElement, SCEntityClassDef parent) : base(fromElement, parent)
        {
            JumpRange = double.Parse(fromElement.GetAttribute("jumpRange"));
            FuelConsumptionRate = float.Parse(fromElement.GetAttribute("quantumFuelRequirement"));

            var jumpParams = fromElement["params"];
            DriveSpeed = float.Parse(jumpParams.GetAttribute("driveSpeed"));
            CooldownTime = float.Parse(jumpParams.GetAttribute("cooldownTime"));
            S1AccelRate = float.Parse(jumpParams.GetAttribute("stageOneAccelRate"));
            S2AccelRate = float.Parse(jumpParams.GetAttribute("stageTwoAccelRate"));
            EngageSpeed = float.Parse(jumpParams.GetAttribute("engageSpeed"));
            SpoolTime = float.Parse(jumpParams.GetAttribute("spoolUpTime"));

            var splineJumpParams = fromElement["splineJumpParams"];
            SplineDriveSpeed = float.Parse(splineJumpParams.GetAttribute("driveSpeed"));
            SplineCooldownTime = float.Parse(splineJumpParams.GetAttribute("cooldownTime"));
            SplineS1AccelRate = float.Parse(splineJumpParams.GetAttribute("stageOneAccelRate"));
            SplineS2AccelRate = float.Parse(splineJumpParams.GetAttribute("stageTwoAccelRate"));
            SplineEngageSpeed = float.Parse(splineJumpParams.GetAttribute("engageSpeed"));
            SplineSpoolTime = float.Parse(splineJumpParams.GetAttribute("spoolUpTime"));
        }
    }
}
