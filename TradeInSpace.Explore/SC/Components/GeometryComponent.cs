﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace TradeInSpace.Explore.SC.Components
{
    [SCEntityComponent("SGeometryResourceParams")]
    public class GeometryComponent : SCComponent
    {
        public string ModelPath { get; private set; } = null;
        public float ScaleMultiplier { get; private set; }
        public int AttachFlags { get; private set; }

        public GeometryComponent(XmlElement fromElement, SCEntityClassDef parent) : base(fromElement, parent)
        {
            ModelPath = (fromElement.SelectSingleNode("./Geometry/Geometry/Geometry/@path") as XmlAttribute)?.Value;
            ScaleMultiplier = float.Parse((fromElement.SelectSingleNode("./Geometry/@ScaleMultiplier") as XmlAttribute)?.Value ?? "1.0");
            AttachFlags = int.Parse((fromElement.SelectSingleNode("./Geometry/Geometry/@AttachFlags") as XmlAttribute)?.Value ?? "0");
        }
    }
}
