﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Xml;
using TradeInSpace.Explore.SC.Data;
using TradeInSpace.Explore.DataForge;
using Newtonsoft.Json;

namespace TradeInSpace.Explore.SC.Components
{
    [SCEntityComponent("SAttachableComponentParams")]
    [JsonObject(MemberSerialization.OptIn)]
    public class AttachableComponent : SCComponent
    {
        public string Name { get; private set; }
        public string Description { get; private set; }

        [JsonProperty]
        public bool ShouldAttach { get; private set; }

        public string AttachmentType { get; private set; }
        public string Subtype { get; private set; }
        public int Size { get; private set; }
        public int Grade { get; private set; }
        public Guid ManufacturerID { get; private set; }
        public List<string> Tags { get; private set; }
        public List<string> RequiredTags { get; private set; }

        //Only load these when specifically requested
        public Manufacturer Manufacturer 
            { get { return ParentEntity.GameDatabase.GetEntry<Manufacturer>(ManufacturerID); } }

        public AttachableComponent(XmlElement fromElement, SCEntityClassDef parent) : base(fromElement, parent)
        {
            var attachMode = fromElement.HasAttribute("attachToTileItemPort") ? fromElement.GetAttribute("attachToTileItemPort") : null;

            switch (attachMode)
            {
                case null:
                case "ExteriorConnection":
                case "DefaultConnection":
                    ShouldAttach = true;
                    break;
                case "NoConnection":
                    ShouldAttach = false;
                    break;
                default:
                    break;
            }

            var attachDef = fromElement["AttachDef"];

            Name = parent.GameDatabase.Localize(attachDef["Localization"].GetAttribute("Name"));
            Description = parent.GameDatabase.Localize(attachDef["Localization"].GetAttribute("Description"));
            AttachmentType = attachDef.GetAttribute("Type");
            Subtype = attachDef.GetAttribute("SubType");
            Size = int.Parse(attachDef.GetAttribute("Size"));
            Grade = int.Parse(attachDef.GetAttribute("Grade"));
            ManufacturerID = Guid.Parse(attachDef.GetAttribute("Manufacturer"));
            Tags = attachDef.GetAttribute("Tags").Split(' ').ToList();
            RequiredTags = attachDef.GetAttribute("RequiredTags").Split(' ').ToList();

            //TODO: Kind of a hack...replace?
            if (Subtype == "UNDEFINED") Subtype = null;
        }

        internal override void OnDFEntriesCompleted()
        {
            base.OnDFEntriesCompleted();
        }
    }
}
