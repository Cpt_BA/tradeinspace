﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace TradeInSpace.Explore.SC.Components
{
    [SCEntityComponent("SCItemShieldGeneratorParams")]
    [JsonObject(MemberSerialization.OptIn)]
    public class ShieldComponent : SCComponent
    {
        [JsonProperty]
        public float MaxShield { get; private set; }
        [JsonProperty]
        public float MaxRegenRate { get; private set; }
        [JsonProperty]
        public float DownedRegenDelay { get; private set; }
        [JsonProperty]
        public float DamagedRegenDelay { get; private set; }

        [JsonProperty]
        public float HardenFactor { get; private set; }
        [JsonProperty]
        public float HardenDuration { get; private set; }
        [JsonProperty]
        public float HardenCooldown { get; private set; }

        public ShieldComponent(XmlElement fromElement, SCEntityClassDef parent) : base(fromElement, parent)
        {
            MaxShield = float.Parse(fromElement.GetAttribute("MaxShieldHealth"));
            MaxRegenRate = float.Parse(fromElement.GetAttribute("MaxShieldRegen"));
            DownedRegenDelay = float.Parse(fromElement.GetAttribute("DownedRegenDelay"));
            DamagedRegenDelay = float.Parse(fromElement.GetAttribute("DamagedRegenDelay"));

            var hardening = fromElement["ShieldHardening"];
            if (hardening != null)
            {
                HardenFactor = float.Parse(hardening.GetAttribute("Factor"));
                HardenDuration = float.Parse(hardening.GetAttribute("Duration"));
                HardenCooldown = float.Parse(hardening.GetAttribute("Cooldown"));
            }
        }
    }
}
