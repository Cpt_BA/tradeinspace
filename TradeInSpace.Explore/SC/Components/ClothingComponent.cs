﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace TradeInSpace.Explore.SC.Components
{
    [SCEntityComponent("SCItemClothingParams")]
    [JsonObject(MemberSerialization.OptIn)]
    public class ClothingComponent : SCComponent
    {
        [JsonProperty]
        public int? MinResistance { get; private set; }
        [JsonProperty]
        public int? MaxResistance { get; private set; }

        public ClothingComponent(XmlElement fromElement, SCEntityClassDef parent) : base(fromElement, parent)
        {
            if(int.TryParse(fromElement.SelectSingleNode("./TemperatureResistance/@MinResistance").Value, out var minValue))
            {
                MinResistance = minValue;
            }
            if (int.TryParse(fromElement.SelectSingleNode("./TemperatureResistance/@MaxResistance").Value, out var maxValue))
            {
                MaxResistance = maxValue;
            }
        }
    }
}
