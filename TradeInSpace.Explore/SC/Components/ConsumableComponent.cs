﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using TradeInSpace.Explore.SC.Data;

namespace TradeInSpace.Explore.SC.Components
{
    [SCEntityComponent("SCItemConsumableParams")]
    [JsonObject(MemberSerialization.OptIn)]
    public class ConsumableComponent : SCComponent
    {
        [JsonProperty]
        public float Hunger { get; private set; }
        [JsonProperty]
        public float Thirst { get; private set; }
        [JsonProperty]
        public int Uses { get; private set; }
        [JsonProperty]
        public List<ConsumableEffect> Effects { get; private set; }


        private Guid ConsumableGUID = Guid.Empty;

        public ConsumableComponent(XmlElement fromElement, SCEntityClassDef parent) : base(fromElement, parent)
        {
            Guid.TryParse(fromElement.SelectSingleNode("./defaultContents/ConsumableContent/@consumableSubtype")?.Value, out ConsumableGUID);
            
            if (int.TryParse(fromElement.SelectSingleNode("./consumableVolume/SMicroCargoUnit/@microSCU")?.Value, out var usesAmount))
            {
                Uses = usesAmount;
            }
        }

        internal override void OnDFEntriesCompleted()
        {
            if (ConsumableGUID != Guid.Empty)
            {
                var consumable = ParentEntity.GameDatabase.GetEntry<Consumable>(ConsumableGUID);

                Hunger = consumable.Hunger;
                Thirst = consumable.Thirst;
                Effects = consumable.Effects;
            }
        }
    }
}
