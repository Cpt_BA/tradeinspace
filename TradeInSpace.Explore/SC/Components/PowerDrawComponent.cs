﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace TradeInSpace.Explore.SC.Components
{
    [SCEntityComponent("EntityComponentPowerConnection")]
    [JsonObject(MemberSerialization.OptIn)]
    public class PowerDrawComponent : SCComponent
    {
        [JsonProperty]
        public float PowerBase { get; private set; }
        [JsonProperty]
        public float PowerActive { get; private set; }
        [JsonProperty]
        public float PowerToEM { get; set; }
        [JsonProperty]
        public float DecayRateEM { get; set; }

        public PowerDrawComponent(XmlElement fromElement, SCEntityClassDef parent) : base(fromElement, parent)
        {
            PowerBase = float.Parse(fromElement.GetAttribute("PowerBase"));
            PowerActive = float.Parse(fromElement.GetAttribute("PowerDraw"));
            PowerToEM = float.Parse(fromElement.GetAttribute("PowerToEM"));
            DecayRateEM = float.Parse(fromElement.GetAttribute("DecayRateOfEM"));
        }
    }
}
