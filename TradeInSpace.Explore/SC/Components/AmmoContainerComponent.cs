﻿using Newtonsoft.Json;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using TradeInSpace.Explore.SC.Data;

namespace TradeInSpace.Explore.SC.Components
{
    [SCEntityComponent("SAmmoContainerComponentParams")]
    [JsonObject(MemberSerialization.OptIn)]
    public class AmmoContainerComponent : SCComponent
    {
        [JsonProperty]
        public int MaxAmmo { get; private set; }
        [JsonProperty]
        public int StartAmmo { get; private set; }
        [JsonProperty]
        public AmmoParams AmmoData { get; private set; }

        private Guid AmmoTypeGUID;

        public AmmoContainerComponent(XmlElement fromElement, SCEntityClassDef parent) : base(fromElement, parent)
        {
            MaxAmmo = int.Parse(fromElement.GetAttribute("maxAmmoCount"));
            StartAmmo = int.Parse(fromElement.GetAttribute("initialAmmoCount"));
            AmmoTypeGUID = Guid.Parse(fromElement.GetAttribute("ammoParamsRecord"));
        }

        internal override void OnDFEntriesCompleted()
        {
            base.OnDFEntriesCompleted();

            AmmoData = ParentEntity.GameDatabase.GetEntries<AmmoParams>().FirstOrDefault(ap => ap.ID == AmmoTypeGUID);

            if(AmmoData == null)
            {
                Log.Warning($"Unable to find ammo params entry for GUID {AmmoTypeGUID} [{ParentEntity.InstanceName}]");
            }
        }
    }
}
