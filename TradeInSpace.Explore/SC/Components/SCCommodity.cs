﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using TradeInSpace.Explore.DataForge;
using TradeInSpace.Explore.SC.Data;

namespace TradeInSpace.Explore.SC.Components
{
    [SCEntityComponent("CommodityComponentParams")]
    public class SCCommodity : SCComponent
    {
        public string Name { get; private set; }
        public string Description { get; private set; }
        public Guid TypeID { get; private set; }
        public Guid SubtypeID { get; private set; }

        public int uSCUSize { get; private set; }

        public ResourceTypeGroup Category { get; private set; }
        public ResourceType Commodity { get; private set; }

        public SCCommodity(XmlElement fromElement, SCEntityClassDef parent) : base(fromElement, parent)
        {
            Name = parent.GameDatabase.Localize(fromElement.GetAttribute("name"));
            Description = parent.GameDatabase.Localize(fromElement.GetAttribute("description"));

            TypeID = Guid.Parse(fromElement.GetAttribute("type"));
            SubtypeID = Guid.Parse(fromElement.GetAttribute("subtype"));

            uSCUSize = (int)SCUtils.ReadCargoUnitAsMicroSCU(fromElement["occupancy"].FirstChild as XmlElement);
        }

        internal override void OnDFEntriesCompleted()
        {
            Category = ParentEntity.GameDatabase.GetEntry<ResourceTypeGroup>(TypeID);
            Commodity = ParentEntity.GameDatabase.GetEntry<ResourceType>(SubtypeID);

            base.OnDFEntriesCompleted();
        }
    }
}
