﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace TradeInSpace.Explore.SC.Components
{
    [SCEntityComponent("CriminalRecordUIProviderParams")]
    [JsonObject(MemberSerialization.OptIn)]
    public class CriminalRecordUIComponent : SCComponent
    {
        [JsonProperty]
        public bool ShowFelonies;
        [JsonProperty]
        public bool ShowMisdemeanors;
        [JsonProperty]
        public bool AutoStartTemoveTime;
        [JsonProperty]
        public float WarningTime;
        [JsonProperty]
        public float RemoveTimeUpdateSeconds;

        public CriminalRecordUIComponent(XmlElement fromElement, SCEntityClassDef parent) : base(fromElement, parent)
        {
            ShowFelonies = fromElement.GetAttribute("displayFelonies") == "1";
            ShowMisdemeanors = fromElement.GetAttribute("displayMisdemeanors") == "1";
            AutoStartTemoveTime = fromElement.GetAttribute("autoStartRemoveTime") == "1";
            WarningTime = float.Parse(fromElement.GetAttribute("warningTime"));
            RemoveTimeUpdateSeconds = float.Parse(fromElement.GetAttribute("removeTimeUpdateSeconds"));
        }
    }
}
