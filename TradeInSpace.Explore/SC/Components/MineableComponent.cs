﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using TradeInSpace.Explore.SC.Data;
using TradeInSpace.Models;

namespace TradeInSpace.Explore.SC.Components
{
    [SCEntityComponent("MineableParams")]
    public class MineableComponent : SCComponent
    {
        public Guid CompositionID { get; private set; }

        public MineableComposition Composition { get; set; }

        public MineableComponent(XmlElement fromElement, SCEntityClassDef parent) : base(fromElement, parent)
        {
            CompositionID = Guid.Parse(fromElement.GetAttribute("composition"));
        }

        internal override void OnDFEntriesCompleted()
        {
            Composition = ParentEntity.GameDatabase.GetEntry<MineableComposition>(CompositionID);
        }
    }
}
