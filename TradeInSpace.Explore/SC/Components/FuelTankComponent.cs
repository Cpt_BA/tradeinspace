﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace TradeInSpace.Explore.SC.Components
{
    [SCEntityComponent("SCItemFuelTankParams")]
    [JsonObject(MemberSerialization.OptIn)]
    public class FuelTankComponent:SCComponent
    {
        [JsonProperty]
        public float FillRate { get; private set; }
        [JsonProperty]
        public float DrainRate { get; private set; }
        [JsonProperty]
        public float Capacity { get; private set; }

        public FuelTankComponent(XmlElement fromElement, SCEntityClassDef parent) : base(fromElement, parent)
        {
            FillRate = float.Parse(fromElement.GetAttribute("fillRate"));
            DrainRate = float.Parse(fromElement.GetAttribute("drainRate"));
            Capacity = float.Parse(fromElement.GetAttribute("capacity"));
        }
    }
}
