﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace TradeInSpace.Explore.SC.Components
{
    [SCEntityComponent("SCItemCoolerParams")]
    [JsonObject(MemberSerialization.OptIn)]
    public class CoolerComponent : SCComponent
    {
        [JsonProperty]
        public float CoolingRate { get; private set; }

        public CoolerComponent(XmlElement fromElement, SCEntityClassDef parent) : base(fromElement, parent)
        {
            CoolingRate = float.Parse(fromElement.GetAttribute("CoolingRate"));
        }
    }
}
