﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using TradeInSpace.Explore.SC;

namespace TradeInSpace.Explore.Socpak.Components
{
    [SCEntityComponent("SCItemPurchasableParams")]
    public class Purchasable : SCComponent
    {
        public string DispalyName { get; private set; }
        public string DispalyType { get; private set; }

        public Purchasable(XmlElement fromElement, SCEntityClassDef parent) : base(fromElement, parent)
        {
            DispalyName = fromElement.GetAttribute("displayName");
            DispalyType = fromElement.GetAttribute("displayType");
        }
    }
}
