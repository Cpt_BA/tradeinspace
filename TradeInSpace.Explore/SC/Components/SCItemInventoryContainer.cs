﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using TradeInSpace.Explore.SC.Data;

namespace TradeInSpace.Explore.SC.Components
{
    [SCEntityComponent("SCItemInventoryContainerComponentParams")]
    public class SCItemInventoryContainer : SCComponent
    {
        public Guid ContainerID { get; private set; }

        public InventoryContainer Container { get; private set; }

        public SCItemInventoryContainer(XmlElement fromElement, SCEntityClassDef parent) : base(fromElement, parent)
        {
            ContainerID = Guid.Parse(fromElement.GetAttribute("containerParams"));
        }

        internal override void OnDFEntriesCompleted()
        {
            if(ContainerID != Guid.Empty)
                Container = ParentEntity.GameDatabase.GetEntry<InventoryContainer>(ContainerID);

            base.OnDFEntriesCompleted();
        }
    }
}
