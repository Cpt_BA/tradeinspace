﻿using Newtonsoft.Json;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using TradeInSpace.Explore.SC.Loadout;

namespace TradeInSpace.Explore.SC.Components
{
    [SCEntityComponent("SEntityComponentDefaultLoadoutParams")]
    public class DefaultLoadoutParams : SCComponent
    {
        public ItemPortLoadoutParams Loadout { get; private set; }

        public DefaultLoadoutParams(XmlElement fromElement, SCEntityClassDef parent) : base(fromElement, parent)
        {
            var loadoutElement = fromElement["loadout"];
            Loadout = ItemPortLoadoutParams.FromXML(loadoutElement, this);
        }

        public string GetLoadoutJSON(IEnumerable<string> IgnoreTypes)
        {
            return JsonConvert.SerializeObject(Loadout?.GetDTOs(IgnoreTypes));
        }
    }

    public class ItemPortLoadoutParams
    {
        internal DefaultLoadoutParams ParentLoadout { get; private set; }

        public List<ItemPortLoadoutEntry> Entries { get; private set; }

        private ItemPortLoadoutParams() { }

        internal IEnumerable<LoadoutEntryDTO> GetDTOs(IEnumerable<string> IgnoreTypes)
        {
            return Entries.Select(e =>
            {
                return new LoadoutEntryDTO()
                {
                    PortName = e.PortName,
                    ComponentKey = Utilities.CleanKey(e.EntityClass),
                    Children = e?.Loadout?.GetDTOs(IgnoreTypes)?.ToList()
                };
            }).ToList();
        }

        public static ItemPortLoadoutParams FromXML(XmlElement element, DefaultLoadoutParams forLoadout)
        {
            var LoadoutElement = element?.FirstChild as XmlElement;
            if (LoadoutElement == null) return null;


            var GameData = forLoadout.ParentEntity.GameDatabase;

            List<ItemPortLoadoutEntry> LoadoutEntries = new List<ItemPortLoadoutEntry>();

            if (LoadoutElement.Name == "SItemPortLoadoutXMLParams" ||
                LoadoutElement.Name == "Item")
            {
                var loadoutPath = LoadoutElement.GetAttribute("loadoutPath");
                if (!string.IsNullOrEmpty(loadoutPath))
                {
                    var xmlLoadout = GameData.ReadPackedFileCryXML(loadoutPath)?["Loadout"];

                    if (xmlLoadout != null)
                    {
                        LoadoutEntries = xmlLoadout
                                .SelectNodes("./Items/Item")
                                .OfType<XmlElement>()
                                .Select(e => ItemPortLoadoutEntry.FromFileXML(e, forLoadout))
                                .ToList();

                    }
                }
            }
            else if (LoadoutElement.Name == "SItemPortLoadoutManualParams")
            {
                LoadoutEntries = LoadoutElement
                    .SelectNodes("./entries/SItemPortLoadoutEntryParams")
                    .OfType<XmlElement>()
                    .Select(e => ItemPortLoadoutEntry.FromManualXML(e, forLoadout))
                    .ToList();
            }
            else if (LoadoutElement.Name == "SItemPortLoadoutOutfitParams") { return null; }
            else if (LoadoutElement.Name == "SItemPortActorRecordParams") { return null; }
            else
            {
                throw new NotSupportedException();
            }

            if (LoadoutEntries != null)
            {
                return new ItemPortLoadoutParams()
                {
                    ParentLoadout = forLoadout,
                    Entries = LoadoutEntries
                };
            }
            else
            {
                return null;
            }
        }

        public void PopulateLoadout(LoadoutTree intoLoadout)
        {
            var GameDatabase = intoLoadout.From.GameDatabase;
            var Entity = intoLoadout.From;

            Log.Verbose($"[{Entity.InstanceName}] Processing Loadout Entry {intoLoadout.Root.PortName}");

            foreach (var loadoutEntry in Entries)
            {
                if (string.IsNullOrEmpty(loadoutEntry.EntityClass) ||
                    string.IsNullOrEmpty(loadoutEntry.PortName))
                {
                    Log.Verbose($"Skipping blank ship port entry in loadout ({loadoutEntry.PortName})/({loadoutEntry.EntityClass}).");
                    continue;
                }

                Log.Verbose($"Attaching {loadoutEntry.EntityClass} to port {loadoutEntry.PortName}");
                var flatChildren = Utilities.FlattenRecursive(intoLoadout.Root, c => c.Slots);
                var flatNames = flatChildren.Select(c => c.PortName).ToList();

                var matchingChild = flatChildren
                    .FirstOrDefault(c => c.PortName.ToLower() == loadoutEntry.PortName.ToLower());
                if (matchingChild != null)
                {

                    var loadoutComponent = GameDatabase.GetEntityByInstanceName(loadoutEntry.EntityClass);

                    if (loadoutComponent == null)
                    {
                        Log.Warning($"Vehicle {Entity.InstanceName} - Port {loadoutEntry.PortName} - Entity Class specified in loadout ({loadoutEntry.EntityClass}) not found");
                        continue;
                    }

                    var newEntry = matchingChild.Port.AttachComponent(loadoutComponent);
                    //If the loadout entry we are looking at has children, apply those loadouts as well
                    if (loadoutEntry.Loadout != null && newEntry != null)
                    {
                        loadoutEntry.Loadout.PopulateLoadout(newEntry);
                    }
                }
                else
                {
                    if (loadoutEntry.PortName != "hardpoint_fuel_port" && loadoutEntry.PortName != "hardpoint_air_traffic_controller")
                    {
                        Log.Warning($"Missing port {loadoutEntry.PortName} on entry {intoLoadout.Root.PortName} on entity {Entity.InstanceName}");
                    }
                }
            }
        }
    }

    public class ItemPortLoadoutEntry
    {
        public DefaultLoadoutParams ParentLoadout { get; private set; }
        public ItemPortLoadoutParams Loadout { get; private set; }
        public LoadoutEntry AttachedEntry { get; internal set; }

        public string PortName { get; private set; }
        public string EntityClass { get; private set; }

        private ItemPortLoadoutEntry() { }

        public static ItemPortLoadoutEntry FromManualXML(XmlElement element, DefaultLoadoutParams forLoadout)
        {
            if (element == null) return null;

            return new ItemPortLoadoutEntry()
            {
                PortName = element.GetAttribute("itemPortName"),
                EntityClass = element.GetAttribute("entityClassName"),

                ParentLoadout = forLoadout,
                Loadout = ItemPortLoadoutParams.FromXML(element["loadout"], forLoadout)
            };
        }
        public static ItemPortLoadoutEntry FromFileXML(XmlElement element, DefaultLoadoutParams forLoadout)
        {
            if (element == null) return null;

            return new ItemPortLoadoutEntry()
            {
                PortName = element.GetAttribute("portName"),
                EntityClass = element.GetAttribute("itemName"),

                ParentLoadout = forLoadout,
                Loadout = ItemPortLoadoutParams.FromXML(element["Items"], forLoadout)
            };
        }
    }
}
