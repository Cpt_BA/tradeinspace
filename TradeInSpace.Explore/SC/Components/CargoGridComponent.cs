﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace TradeInSpace.Explore.SC.Components
{
    [SCEntityComponent("SCItemCargoGridParams")]
    [JsonObject(MemberSerialization.OptIn)]
    public class CargoGridComponent : SCComponent
    {
        [JsonProperty]
        public float CrateGenRate { get; private set; }
        [JsonProperty]
        public int CrateMax { get; private set; }
        [JsonProperty]
        public bool MiningOnly { get; private set; }

        [JsonProperty]
        public float DimX { get; private set; }
        [JsonProperty]
        public float DimY { get; private set; }
        [JsonProperty]
        public float DimZ { get; private set; }

        [JsonProperty("SCU")]
        public int SCU_Actual { get; private set; }
        public float SCU_Raw { get; private set; }

        public CargoGridComponent(XmlElement fromElement, SCEntityClassDef parent) : base(fromElement, parent)
        {
            CrateGenRate = float.Parse(fromElement.GetAttribute("crateGenPercentageOnDestroy"));
            CrateMax = int.Parse(fromElement.GetAttribute("crateMaxOnDestroy"));
            MiningOnly = fromElement.GetAttribute("miningOnly") == "1";
            var cargoDims = SCUtils.ReadVector3(fromElement["dimensions"]);

            SCU_Actual = SCUtils.CalculateCargoGridSCU(cargoDims);
            SCU_Raw = SCUtils.CalculateCargoGridSCU(cargoDims, NearestWhole: false);
        }
    }
}
