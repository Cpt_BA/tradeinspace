﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace TradeInSpace.Explore.SC.Components
{
    [SCEntityComponent("EntityComponentHeatConnection")]
    [JsonObject(MemberSerialization.OptIn)]
    public class HeatDrawComponent : SCComponent
    {
        [JsonProperty]
        public float HeatBase { get; private set; }
        [JsonProperty]
        public float HeatActive { get; private set; }
        [JsonProperty]
        public float TemperatureToIR { get; private set; }
        [JsonProperty]
        public float SpecificHeatCapacity { get; private set; }
        [JsonProperty]
        public float CoolingRate { get; private set; }

        public HeatDrawComponent(XmlElement fromElement, SCEntityClassDef parent) : base(fromElement, parent)
        {
            HeatBase = float.Parse(fromElement.GetAttribute("ThermalEnergyBase"));
            HeatActive = float.Parse(fromElement.GetAttribute("ThermalEnergyDraw"));
            TemperatureToIR = float.Parse(fromElement.GetAttribute("TemperatureToIR"));
            SpecificHeatCapacity = float.Parse(fromElement.GetAttribute("SpecificHeatCapacity"));
            CoolingRate = float.Parse(fromElement.GetAttribute("MaxCoolingRate"));
        }
    }
}
