﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace TradeInSpace.Explore.SC.Components
{
    [SCEntityComponent("RemovableChipParams")]
    [JsonObject(MemberSerialization.OptIn)]
    public class RemovableChipComponent : SCComponent
    {
        [JsonProperty]
        public float Duration { get; private set; }
        [JsonProperty]
        public float ErrorChance { get; private set; }

        public RemovableChipComponent(XmlElement fromElement, SCEntityClassDef parent) : base(fromElement, parent)
        {
            Duration = float.Parse(fromElement.SelectSingleNode("./values/RemovableChipValue[@name='Duration']/@value").Value);
            ErrorChance = float.Parse(fromElement.SelectSingleNode("./values/RemovableChipValue[@name='ErrorChance']/@value").Value);
        }
    }
}
