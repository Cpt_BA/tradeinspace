﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace TradeInSpace.Explore.SC.Components
{
    [SCEntityComponent("SCItem")]
    public class SCItem : SCComponent
    {
        public List<SCItemPort> ItemPorts { get; set; }

        public SCItem(XmlElement fromElement, SCEntityClassDef parent) : base(fromElement, parent)
        {
            this.ItemPorts = fromElement
                .SelectNodes("./ItemPorts/SItemPortCoreParams/Ports")
                .OfType<XmlElement>()
                .Select(e => SCItemPort.FromXML(e))
                .ToList();
        }
    }

    public class SCItemPort
    {
        public static SCItemPort FromXML(XmlElement fromElement)
        {
            return new SCItemPort()
            {
                Name = fromElement.GetAttribute("Name"),
                DisplayName = fromElement.GetAttribute("DisplayName")
            };
        }

        public string Name { get; private set; }
        public string DisplayName { get; private set; }
    }
}
