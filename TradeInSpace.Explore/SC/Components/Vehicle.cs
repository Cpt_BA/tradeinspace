﻿using System;
using System.Collections.Generic;
using System.DoubleNumerics;
using System.Linq;
using System.Text;
using System.Xml;
using TradeInSpace.Explore.DataForge;
using TradeInSpace.Explore.SC.Data;
using TradeInSpace.Explore.Socpak;

namespace TradeInSpace.Explore.SC.Components
{
    [SCEntityComponent("VehicleComponentParams")]
    public class Vehicle : SCComponent
    {
        public string Name { get; private set; }
        public string Description { get; private set; }

        public string Career { get; private set; }
        public string Role { get; private set; }

        public Guid ManufacturerGuid { get; private set; }
        public Manufacturer Manufacturer { get; private set; }
        public string DefinitionFile { get; private set; }
        public string DefinitionModification { get; private set; }
        public int CrewSize { get; private set; }
        public bool PlayerShip { get; private set; } = false;

        public float Size_X { get; private set; }
        public float Size_Y { get; private set; }
        public float Size_Z { get; private set; }

        public LandingPadSizeEntry SizeEntry { get; private set; }
        public ShipInsuranceRecord Insurance { get; private set; }

        public List<ShipContainerParams> ContainerEntries { get; private set; }

        public Vehicle(XmlElement fromElement, SCEntityClassDef parent) : base(fromElement, parent)
        {
            Name = parent.GameDatabase.Localize(fromElement.GetAttribute("vehicleName"));
            Description = parent.GameDatabase.Localize(fromElement.GetAttribute("vehicleDescription"));

            Career = parent.GameDatabase.Localize(fromElement.GetAttribute("vehicleCareer"));
            Role = parent.GameDatabase.Localize(fromElement.GetAttribute("vehicleRole"));

            ManufacturerGuid = Guid.Parse(fromElement.GetAttribute("manufacturer"));
            DefinitionFile = fromElement.GetAttribute("vehicleDefinition");
            DefinitionModification = fromElement.GetAttribute("modification");
            CrewSize = int.Parse(fromElement.GetAttribute("crewSize"));

            Size_X = float.Parse(fromElement["maxBoundingBoxSize"].GetAttribute("x"));
            Size_Y = float.Parse(fromElement["maxBoundingBoxSize"].GetAttribute("y"));
            Size_Z = float.Parse(fromElement["maxBoundingBoxSize"].GetAttribute("z"));
        }

        internal override void OnDFEntriesCompleted()
        {
            base.OnDFEntriesCompleted();

            //NOTE: Looking at static data on the *Parent Entity*, better way to do this...?
            var entityData = ParentEntity.Element.SelectSingleNode("./StaticEntityClassData");
            if (entityData != null)
            {
                var insuraceElem = entityData.SelectSingleNode("./SEntityInsuranceProperties/shipInsuranceParams") as XmlElement;
                if (insuraceElem != null)
                {
                    Insurance = ShipInsuranceRecord.FromXML(insuraceElem);

                    //All variants point to the base ship for insurance
                    PlayerShip = Insurance.ShipClass == ParentEntity.InstanceName;
                }
            }

            Manufacturer = ParentEntity.GameDatabase.GetEntry<Manufacturer>(ManufacturerGuid);
            ContainerEntries = new List<ShipContainerParams>();

            var containerRecords = Element
                .SelectNodes(".//objectContainers/SVehicleObjectContainerParams")
                .OfType<XmlElement>();

            foreach (var container in containerRecords)
            {
                var posElem = container.SelectSingleNode(".//Position") as XmlElement;
                var rotElem = container.SelectSingleNode(".//Rotation") as XmlElement;

                var posVect = new Vector3(
                    posElem.ReadPropertyDouble("x"),
                    posElem.ReadPropertyDouble("y"),
                    posElem.ReadPropertyDouble("z"));
                var rotQuat = Quaternion.CreateFromYawPitchRoll(
                    rotElem.ReadPropertyDouble("x") / 180 * Math.PI,
                    rotElem.ReadPropertyDouble("y") / 180 * Math.PI,
                    rotElem.ReadPropertyDouble("z") / 180 * Math.PI);

                ContainerEntries.Add(new ShipContainerParams()
                {
                    ArchivePath = container.GetAttribute("fileName"),
                    Offset = posVect,
                    Rotation = rotQuat
                });
            }

            /*
            foreach(var container in containerRecords)
            {
                var containerSOC = SOC_Archive.LoadFromGameDB(ParentEntity.GameDatabase, container.GetAttribute("fileName"));
                if (containerSOC == null) continue;

                var entrySOC = containerSOC.RootEntry;
                if (entrySOC == null) continue;

                loadedDocs.Add(containerSOC);
                containerSOC.ExploreRecursive();

                allDocs += containerSOC.RootDoc.DocumentElement.OuterXml + Environment.NewLine;

                foreach (var childEntry in entrySOC.ExploreRecursive(true))
                {
                    if (childEntry?.Item2?.Element?.OuterXml != null)
                    {
                        childEntry.Item2.LoadFullDetails();
                        allDocs += childEntry.Item2.Archive?.RootDoc?.OuterXml + Environment.NewLine;
                    }
                }



                var additional = entrySOC.LoadAdditionalData();
                entrySOC.LoadAllEntities(false);

                foreach(var ent in containerSOC.RootEntry.Entities.OfType<FullEntity>())
                {
                    //allDocs += ent.Element.OuterXml + Environment.NewLine;
                }

                //if (additional != null) allDocs += additional.OuterXml + Environment.NewLine;

            }
            */
        }
    }

    public class ShipContainerParams
    {
        public Vector3 Offset;
        public Vector4 Rotation;
        public string ArchivePath;
    }
}
