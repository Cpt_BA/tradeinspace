﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace TradeInSpace.Explore.SC
{
    public static class SCUtils
    {
        public static float ReadCargoUnitAsMicroSCU(XmlElement CargoElement)
        {
            switch (CargoElement.GetAttribute("__polymorphicType"))
            {
                case "SMicroCargoUnit":
                    return int.Parse(CargoElement.GetAttribute("microSCU"));
                case "SCentiCargoUnit":
                    return float.Parse(CargoElement.GetAttribute("centiSCU")) * (int)CommoditySizes_uSCU.cSCU;
                case "SStandardCargoUnit":
                    return float.Parse(CargoElement.GetAttribute("standardCargoUnits")) * (int)CommoditySizes_uSCU.SCU;
                default:
                    throw new NotSupportedException($"Unsupported cargo size type {CargoElement.GetAttribute("__polymorphicType")}");
            }
        }

        public static Vector3 ReadVector3(XmlElement VectorElement, string ExpectedType = "Vec3")
        {
            if (VectorElement.GetAttribute("__type") != ExpectedType)
                throw new ArgumentException("VectorElement has wrong type", nameof(VectorElement));

            return new Vector3(
                float.Parse(VectorElement.GetAttribute("x")),
                float.Parse(VectorElement.GetAttribute("y")),
                float.Parse(VectorElement.GetAttribute("z")));
        }

        public static int CalculateCargoGridSCU(Vector3 CargoGridSize, float SlotSize = 1.25f, bool NearestWhole = true)
        {
            if (NearestWhole)
            {
                return (int)(
                    Math.Floor(CargoGridSize.X / SlotSize) * 
                    Math.Floor(CargoGridSize.Y / SlotSize) * 
                    Math.Floor(CargoGridSize.Z / SlotSize));
            }
            else
            {
                return (int)(
                    (CargoGridSize.X / SlotSize) * 
                    (CargoGridSize.Y / SlotSize) * 
                    (CargoGridSize.Z / SlotSize));
            }
        }
    }

    public enum CommoditySizes_uSCU
    {
        uSCU = 1,
        cSCU = 100,
        SCU = 100 * 100
    }
}
