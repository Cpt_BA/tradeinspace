﻿using CgfConverter;
using ICSharpCode.SharpZipLib.Zip;
using System;
using System.Collections.Generic;
using System.Text;

namespace TradeInSpace.Explore.DataForge
{
    public interface IPackedFileReader
    {
        IEnumerable<ZipEntry> ZipChildren();
        bool HasEntry(string Path);
        
        byte[] ReadFileBinary(string Path);
        CryEngine ReadCryEngineFile(string Path);
    }
}
