﻿using CgfConverter;
using ICSharpCode.SharpZipLib.Zip;
using Serilog;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using TradeInSpace.Explore.SC;
using TradeInSpace.Explore.SC.Data;
using TradeInSpace.Explore.SC.Loadout;
using TradeInSpace.Explore.Socpak;
using TradeInSpace.Explore.Subsumption;

namespace TradeInSpace.Explore.DataForge
{
    public partial class DFDatabase : IPackedFileReader
    {
        public static string GameDCBPath = "Data/Game.dcb";

        public static event DFDBProgressEventHandler ParserProgress;

        //The only "real" source of entries now.
        //Uses dynamic typing w/ attribute registration :)
        protected Dictionary<Guid, DFEntry> _Entries = new Dictionary<Guid, DFEntry>();

        //Store entity definitions directly because we need to look these up frequently by their ClassDefID
        public Dictionary<Guid, SCEntityClassDef> _EntityDefs = new Dictionary<Guid, SCEntityClassDef>();


        protected Dictionary<string, string> _Localizations = new Dictionary<string, string>(StringComparer.CurrentCultureIgnoreCase);

        public TagDatabase Tags { get; internal set; }
        public LoadoutManager Loadouts { get; private set; }
        public SubsumptionManager Subsumption { get; private set; }
        public IReadOnlyCollection<DFEntry> Entries { get { return _Entries.Values; } }
        public IEnumerable<SCEntityClassDef> Entities { get { return _EntityDefs.Values; } }
        public IReadOnlyDictionary<string, string> Localizations { get { return _Localizations; } }

        public CacheLevel Caching { get; set; }

        public string Channel { get; set; }
        public string Version { get; set; }
        public string FilePath { get; private set; }

        public bool DFLoaded { get => Document != null; }

        internal XmlDocument Document;
        public ZipFile GamePack;

        private Dictionary<string, byte[]> _FileCache = new Dictionary<string, byte[]>();
        private Dictionary<string, XmlDocument> _CryXMLCache = new Dictionary<string, XmlDocument>();

        public bool HasEntityErrors { get => EntityLoadErrors.Count() != 0; }
        public List<LoadError> EntityLoadErrors = new List<LoadError>();

        private DFDatabase() { }

        public static DFDatabase ParseFromSettings(LauncherSettings LauncherSettings = null, string Channel = "LIVE", CacheLevel Cache = CacheLevel.Memory)
        {
            if (LauncherSettings == null)
            {
                LauncherSettings = SCLauncherReader.LoadGameSettings();
            }

            if (LauncherSettings == null) throw new Exception("Unable to load default settings");

            var matchingVersion = LauncherSettings.GetGame("SC", Channel);

            if (matchingVersion == null) throw new Exception($"Unable to find channel {Channel} in game settings.");

            return ParseFromSettings(matchingVersion, Cache);
        }

        public static DFDatabase ParseFromSettings(LauncherInstalledVersion version, CacheLevel Cache = CacheLevel.Memory)
        {
            var db = ParseFromFile(version.GetPath("Data.p4k"), Cache);

            db.Channel = version.ChannelID;
            db.Version = version.ChannelDetails.VersionLabel.ToUpper();

            return db;
        }

        public static DFDatabase ParseFromStream(Stream P4KStream, CacheLevel Cache = CacheLevel.Memory, string Path = null)
        {
            Log.Information($"Loading P4K File - {Path}");

            DFDatabase db = new DFDatabase()
            {
                 Caching = Cache,
                 FilePath = Path
            };

            StreamWatcherUpdater p4kWatcher = new StreamWatcherUpdater(P4KStream, ProgressPhase.ReadP4K);
            p4kWatcher.Start();

            db.GamePack = new ZipFile(P4KStream)
            {
                KeysRequired = (s, e) => { e.Key = new byte[] { 0x5E, 0x7A, 0x20, 0x02, 0x30, 0x2E, 0xEB, 0x1A, 0x3B, 0xB6, 0x17, 0xC3, 0x0F, 0xDE, 0x1E, 0x47 }; }
            };

            p4kWatcher.Finish();

            Log.Information($"{db.GamePack.Count} files in archive.");

            db._Localizations = db.ReadPackedFile("Data/Localization/english/global.ini")
                .Split(new[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries)
                .Select(l => l.Split(new[] { '=' }, 2))
                .ToDictionary(l => "@" + l[0], l => l[1], StringComparer.CurrentCultureIgnoreCase);

            return db;
        }

        public static DFDatabase ParseFromFile(string SCGameP4KFile, CacheLevel Cache = CacheLevel.Memory)
        {
            return ParseFromStream(new FileStream(SCGameP4KFile, FileMode.Open, FileAccess.Read), Cache, 
                Path: SCGameP4KFile);
        }

        public void ParseDFContent(SubsumptionManager subManager = null)
        {
            if (Loadouts != null)
                return; //Has already been loaded

            Log.Information("Parsing DataForge Content File");

            LoadDataforgeContent();

            XmlElement rootNode = Document.DocumentElement;

            var availableTypes = DFEntryAttribute.FindAllLoaded();
            var availableTypeLookup = availableTypes.ToDictionary(e => e.Entry.TypeName, e => e);

            Log.Information($"Starting parse of {rootNode.ChildNodes.Count} nodes.");
            int progressCounter = 0;

            foreach (XmlElement entry in rootNode.ChildNodes)
            {
                var entityTypeClass = entry.GetAttribute("__type");

                try
                {
                    DFEntry createdEntry;
                    
                    if (!availableTypeLookup.ContainsKey(entityTypeClass))
                    {
                        createdEntry = new DFEntry(entry, this);
                    }
                    else
                    {
                        var matchingEntry = availableTypeLookup[entityTypeClass];
                        createdEntry = Activator.CreateInstance(matchingEntry.Class, new object[] { entry, this }) as DFEntry;
                    }

                    _Entries[createdEntry.ID] = createdEntry;

                }
                catch (Exception ex)
                {
                    Log.Error(ex, $"Error loading entity from DF Database.");
                    Log.Error($"Entity Class: {entityTypeClass}");
                    Log.Error($"Entity XML:");
                    Log.Error(entry.OuterXml);

                    EntityLoadErrors.Add(new LoadError(entityTypeClass, entry.LocalName, ex));
                }

                progressCounter++;
                if(progressCounter % 100 == 0)
                    UpdateProgress(this, ProgressPhase.DFEntries, progressCounter, rootNode.ChildNodes.Count);
            }

            //Cache the entity class definitions specifically first
            //so we can look them up during OnDFEntriesCompleted
            _EntityDefs = GetEntries<SCEntityClassDef>().ToDictionary(e => e.ID);

            progressCounter = 0;

            var sortedEntries = _Entries.Values.OrderBy(e =>
            {
                var entryAttribute = availableTypes.FirstOrDefault(t => t.Entry.TypeName == e.Type);
                return entryAttribute.Entry?.ParsePriority ?? 100;
            });

            foreach (var entry in sortedEntries)
            {
                entry.OnDFEntriesCompleted();

                progressCounter++;
                if (progressCounter % 100 == 0)
                    UpdateProgress(this, ProgressPhase.Entities, progressCounter, _Entries.Values.Count);
            }

            Loadouts = new LoadoutManager(this);
            //Could do this here, but this is pretty aggressive and takes a couple minutes
            //Loadouts.PreloadLoadouts();
            InitSubsumption(subManager);
        }

        public void InitSubsumption(SubsumptionManager existingManager = null)
        {
            Subsumption = existingManager ?? new SubsumptionManager();
            Subsumption.Attach(this);
        }

        internal static void UpdateProgress(object caller, ProgressPhase phase, long currentStepProgress, long currentStepMaximum)
        {
            ParserProgress?.Invoke(caller, new DFDBProgressEventArgs(phase, currentStepProgress, currentStepMaximum));
        }

        /*
        public SCEntityClassDef GetEntityByName(string entityName)
        {
            return this._Entities.ContainsKey(entityName) ? this._Entities[entityName] : null;
        }
        */

        public string Localize(string InputText, bool ReturnNull = false, bool UninitBlank = false)
        {
            if (InputText == null) return null;
            if (string.IsNullOrWhiteSpace(InputText)) return "";
            if (UninitBlank && InputText == "@LOC_UNINITIALIZED") return "";
            if (!_Localizations.ContainsKey(InputText))
            {
                Log.Verbose($"WARN: No Key defined for {InputText}");
                return ReturnNull ? null : InputText;
            }
            return _Localizations[InputText];
        }

        public string ConvertTags(string TagString)
        {
            return Tags?.ConvertTags(TagString) ?? TagString;
        }

        private string CleanPath(string InputPath)
        {
            var path = InputPath.Replace('\\', '/').Replace("data", "Data");
            if (!path.StartsWith("Data") && !path.StartsWith("Engine") && !path.StartsWith("mods")) path = "Data/" + path;
            return path;
        }

        internal XmlDocument LoadDataforgeContent()
        {
            var fileDoc = new XmlDocument();

            var existingData = CheckCache(GameDCBPath);
            if (existingData != null)
            {
                var fileContent = Encoding.UTF8.GetString(existingData);
                fileDoc.LoadXml(fileContent);
            }
            else
            {
                byte[] dcbData = _InnerReadBinary(GameDCBPath);
                Log.Information($"Loaded DCB File - {dcbData.Length} bytes");
                WriteCache(GameDCBPath, dcbData);

                using (var memStream = new MemoryStream(dcbData))
                {
                    var watcher = new StreamWatcherUpdater(memStream, ProgressPhase.ReadDCB);
                    watcher.Start();
                    
                    using (var binStream = new BinaryReader(memStream))
                    {
                        var dfStream = new unforge.DataForge(binStream);
                        fileDoc = dfStream.FullDocument;
                    }
                    
                    watcher.Finish();
                }
            }
            Document = fileDoc;
            return fileDoc;
        }

        private string ReplaceInvalidChars(string filename)
        {
            return string.Join("_", filename.Split(Path.GetInvalidFileNameChars()));
        }

        private byte[] CheckCache(string filePath)
        {
            var safe_path = ReplaceInvalidChars(filePath);

            switch (Caching)
            {
                case CacheLevel.Memory:
                    if (_FileCache.ContainsKey(safe_path))
                        return _FileCache[safe_path];
                    else
                        return null;
                case CacheLevel.File:
                    var disk_path = $"Cache/{safe_path}";
                    if (File.Exists(disk_path))
                        return File.ReadAllBytes(disk_path);
                    else return null;
                case CacheLevel.Diabled:
                default:
                    return null;
            }
        }

        private void WriteCache(string filePath, byte[] data)
        {
            var safe_path = ReplaceInvalidChars(filePath);

            switch (Caching)
            {
                case CacheLevel.Memory:
                    _FileCache[safe_path] = data;
                    break;
                case CacheLevel.File:
                    var disk_path = $"Cache/{safe_path}";

                    if (!Directory.Exists("./Cache")) Directory.CreateDirectory("./Cache");
                    File.WriteAllBytes(disk_path, data);
                    break;
            }
        }

        private byte[] _InnerReadBinary(string filePath)
        {
            filePath = CleanPath(filePath);


            var existing = CheckCache(filePath);
            if (existing != null) return existing;

            var entry = GamePack.GetEntry(filePath);
            if (entry == null) return null;
            var zipStream = GamePack.GetInputStream(entry.ZipFileIndex);
            if (zipStream.CanSeek) zipStream.Seek(0, SeekOrigin.Begin);
            

            using (var reader = new BinaryReader(zipStream))
            {
                var result = reader.ReadBytes((int)entry.Size);
                if(entry.Size != result.Length)
                {
                    throw new Exception($"Did not read enough bytes from stream! ({result.Length} actual vs. {entry.Size} expected)");
                }

                WriteCache(filePath, result);
                return result;
            }
        }

        public string ReadPackedFile(string filePath)
        {
            filePath = CleanPath(filePath);

            string finalText;
            var data = _InnerReadBinary(filePath);
            if (data == null) return null;
            using (var memStream = new MemoryStream(data))
            {
                using (var textReader = new StreamReader(memStream))
                {
                    finalText = textReader.ReadToEnd();
                }
            }
            return finalText;
        }

        public byte[] ReadPackedFileBinary(string filePath)
        {
            filePath = CleanPath(filePath);

            return _InnerReadBinary(filePath);
        }

        public XmlDocument ReadPackedFileCryXML(string filePath, bool withSeek = false)
        {
            filePath = CleanPath(filePath);

            if (_CryXMLCache.ContainsKey(filePath)) return _CryXMLCache[filePath];

            var binaryXMLData = ReadPackedFileBinary(filePath);

            if (binaryXMLData == null)
                return null;

            if (!withSeek)
            {

                using (var memStream = new MemoryStream(binaryXMLData))
                {
                    var dfStream = unforge.CryXmlSerializer.ReadStream(memStream);

                    _CryXMLCache[filePath] = dfStream;
                    return dfStream;
                }
            }
            else
            {

                for (int i = 0; i < binaryXMLData.Length - 5; i++)
                {
                    if (binaryXMLData[i] == 'C' && binaryXMLData[i + 1] == 'r' && binaryXMLData[i + 2] == 'y' &&
                        binaryXMLData[i + 3] == 'X' && binaryXMLData[i + 4] == 'm' && binaryXMLData[i + 5] == 'l')
                    {
                        try
                        {
                            using (MemoryStream ms = new MemoryStream(binaryXMLData, i, binaryXMLData.Length - i))
                            {
                                var xmlDoc = unforge.CryXmlSerializer.ReadStream(ms);
                                return xmlDoc;
                            }
                        }
                        catch (Exception ex)
                        {
                            //Console.WriteLine($"Error reading .soc data for {this.Name} in {this.ParentSoc.Name}");
                            return null;
                        }
                    }
                }
                return null;
            }
        }

        public CryEngine ReadPackedCGAFile(string filePath)
        {
            return new CryEngine(filePath, "",
                (test_path) => this.GamePack.GetEntry(CleanPath(test_path)) != null,
                (resolve_path) => this.ReadPackedFileBinary(CleanPath(resolve_path)));
        }

        public IEnumerable<XmlDocument> ReadAllCryXML(string folderPath, string extension = null)
        {
            folderPath = CleanPath(folderPath);

            return GamePack
                .Cast<ZipEntry>()
                .Where(ze => ze.Name.StartsWith(folderPath) && ze.IsFile && (extension == null ? true : Path.GetExtension(ze.Name) == extension))
                .Select(ze => ReadPackedFileCryXML(ze.Name)) //A bit wasteful here since it will re-find the record, oh well
                .ToList();
        }

        IEnumerable<ZipEntry> IPackedFileReader.ZipChildren() => GamePack.OfType<ZipEntry>();
        bool IPackedFileReader.HasEntry(string Path) => GamePack.GetEntry(Path) != null;
        byte[] IPackedFileReader.ReadFileBinary(string Path) => ReadPackedFileBinary(Path);
        CryEngine IPackedFileReader.ReadCryEngineFile(string Path) => ReadPackedCGAFile(Path);
    }


    public class DFDBProgressEventArgs : EventArgs
    {
        public DFDBProgressEventArgs(ProgressPhase phase, long currentStepProgress, long currentStepMaximum)
        {
            Phase = phase;
            CurrentStepProgress = currentStepProgress;
            CurrentStepMaximum = currentStepMaximum;
        }

        public ProgressPhase Phase { get; set; }
        public long CurrentStepProgress { get; set; }
        public long CurrentStepMaximum { get; set; }

        public float Progress
        {
            get
            {
                return (float)(((double)CurrentStepProgress) / CurrentStepMaximum);
            }
        }
    }

    public enum ProgressPhase
    {
        ReadP4K = 1,
        ReadDCB = 2,
        DFEntries = 3,
        Entities = 4,
        Loadouts = 5,

        FinalStep = Loadouts,

        //Used for post-load events
        Socpak = 6,
    }

    public enum CacheLevel
    {
        Diabled = 0,
        Memory = 1,
        File = 2
    }

    public delegate void DFDBProgressEventHandler(Object sender, DFDBProgressEventArgs e);

    public class LoadError
    {
        public LoadError(string classType, string instanceName, Exception error)
        {
            ClassType = classType;
            InstanceName = instanceName;
            Error = error;
        }

        public string ClassType { get; set; }
        public string InstanceName { get; set; }
        public Exception Error { get; set; }
    }
}
