﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace TradeInSpace.Explore.DataForge
{
    class StreamWatcherUpdater
    {
        private CancellationTokenSource CancelSource;
        private bool Completed = false;
        private Task StartedTask;

        public Stream Stream { get; }
        public ProgressPhase Phase { get; }
        public int UpdateInterval { get; }

        public StreamWatcherUpdater(Stream stream, ProgressPhase phase, int updateInterval = 250)
        {
            CancelSource = new CancellationTokenSource();
            Stream = stream;
            Phase = phase;
            UpdateInterval = updateInterval;
        }

        public void Start()
        {
            Completed = false;
            StartedTask = Task.Factory.StartNew(Run, TaskCreationOptions.LongRunning);
        }

        private async Task Run()
        {
            while (!Completed && !CancelSource.IsCancellationRequested)
            {
                try
                {
                    DFDatabase.UpdateProgress(this, Phase, Stream.Position, Stream.Length);
                }
                catch (ObjectDisposedException) { Completed = true; }
                await Task.Delay(TimeSpan.FromMilliseconds(UpdateInterval), CancelSource.Token);
            }
        }

        public void Finish()
        {
            Completed = true;
            StartedTask.Wait();
        }
    }
}
