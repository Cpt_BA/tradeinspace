﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TradeInSpace.Explore.SC;
using TradeInSpace.Explore.SC.Data;
using TradeInSpace.Explore.Socpak;

namespace TradeInSpace.Explore.DataForge
{
    public partial class DFDatabase
    {
        public IEnumerable<T> GetEntries<T>()
            where T : DFEntry
        {
            return _Entries.Values.OfType<T>();
        }

        public T GetEntry<T>(Guid ID, bool AllowMissing = true)
            where T : DFEntry
        {
            if (ID == Guid.Empty) return null;
            if (AllowMissing && !_Entries.ContainsKey(ID)) return null;

            return _Entries[ID] as T;
        }

        public T GetEntry<T>(Guid? ID)
            where T : DFEntry
        {
            if (!ID.HasValue) return null;
            return GetEntry<T>(ID.Value);
        }

        public IEnumerable<SCEntityClassDef> GetEntitiesWithComponents<T>()
            where T : SCComponent
        {
            return _EntityDefs.Values
                .Where(e => e.Components.OfType<T>().Count() > 0);
        }

        public IEnumerable<SCEntityClassDef> GetEntitiesWithComponents<T1, T2>()
            where T1 : SCComponent
            where T2 : SCComponent
        {
            //Not the best way, sue me.
            return _EntityDefs.Values
                .Where(e => e.Components.OfType<T1>().Count() > 0)
                .Where(e => e.Components.OfType<T2>().Count() > 0);
        }

        public IEnumerable<SCEntityClassDef> GetEntitiesWithComponents<T1, T2, T3>()
            where T1 : SCComponent
            where T2 : SCComponent
            where T3 : SCComponent
        {
            //Not the best way, sue me.
            return _EntityDefs.Values
                .Where(e => e.Components.OfType<T1>().Count() > 0)
                .Where(e => e.Components.OfType<T2>().Count() > 0)
                .Where(e => e.Components.OfType<T3>().Count() > 0);
        }

        public IEnumerable<SCEntityClassDef> GetEntitiesWithComponents<T1, T2, T3, T4>()
            where T1 : SCComponent
            where T2 : SCComponent
            where T3 : SCComponent
            where T4 : SCComponent
        {
            //Not the best way, sue me.
            return _EntityDefs.Values
                .Where(e => e.Components.OfType<T1>().Count() > 0)
                .Where(e => e.Components.OfType<T2>().Count() > 0)
                .Where(e => e.Components.OfType<T3>().Count() > 0)
                .Where(e => e.Components.OfType<T4>().Count() > 0);
        }

        public IEnumerable<T> GetComponentsOnEntities<T>()
            where T : SCComponent
        {
            return _EntityDefs.Values.SelectMany(e => e.Components).OfType<T>();
        }

        public SCEntityClassDef GetEntity(Guid ID)
        {
            return _EntityDefs.ContainsKey(ID) ? _EntityDefs[ID] : null;
        }

        public SCEntityClassDef GetEntityByInstanceName(string InstanceName)
        {
            return _EntityDefs.Values.SingleOrDefault(ent => ent.InstanceName == InstanceName);
        }
    }
}
