﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Xml;

namespace TradeInSpace.Explore.DataForge
{

    public abstract class DFBase
    {
        internal DFBase(XmlElement fromElement)
        {
            Element = fromElement;
            Type = fromElement.GetAttribute("__type");
        }

        public string this[string AttributeName]
        {
            get { return Element.GetAttribute(AttributeName); }
        }

        internal virtual void OnDFEntriesCompleted() { }

        public XmlElement Element { get; set; }
        public string Type { get; set; }
    }
}
