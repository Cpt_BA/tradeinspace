﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Xml;
using System.Xml.Serialization;
using TradeInSpace.Explore.Subsumption;

namespace TradeInSpace.Explore
{
    using DN = System.DoubleNumerics;

    public static class Utilities
    {
        [DebuggerHidden]
        public static IEnumerable<T> FlattenRecursive<T>(T element, Func<T, IEnumerable<T>> childSelector)
        {
            yield return element;
            var children = childSelector(element);
            if (children != null)
            {
                foreach (var child in children)
                {
                    //Child itself will be emitted from FlattenRecursive
                    foreach (var grandChild in FlattenRecursive(child, childSelector))
                    {
                        yield return grandChild;
                    }
                }
            }
        }

        [DebuggerHidden]
        public static IEnumerable<(T child, T parent)> FlattenRecursiveTuple<T>(T element, Func<T, IEnumerable<T>> childSelector, T elementParent = default)
        {
            yield return (element, elementParent);
            var children = childSelector(element);
            if (children != null)
            {
                foreach (var child in children)
                {
                    //nested loop here will iterate the flattened collection of a single child
                    foreach (var nested_child in FlattenRecursiveTuple(child, childSelector, element))
                    {
                        yield return nested_child;
                    }
                }
            }
        }

        [DebuggerHidden]
        public static T ReadAttributeTyped<T>(this XmlElement xmlNode, string propertyName, T fallbackValue = default)
        {
            if (xmlNode.HasAttribute(propertyName))
            {
                try
                {
                    return (T)Convert.ChangeType(xmlNode.GetAttribute(propertyName), typeof(T));
                }
                catch
                {
                    return fallbackValue;
                }
            }
            else return fallbackValue;
        }

        public static string CleanKey(string Text)
        {
            if (Text == null) return null;
            return string.Join("_", Text.ToLower()
                .Replace('\u00A0', '_')
                .Replace(" ", "_")
                .Replace("-", "_")
                .Replace("&", "_")
                .Replace(",", "_")
                .Replace("'", "")
                .Replace("\"", "")
                .Replace("#", "")
                .Replace("(", "")
                .Replace(")", "")
                .Split(new char[] { '_' }, StringSplitOptions.RemoveEmptyEntries));
        }

        public static string GetFullErrorDetails(Exception exception, bool IncludeTypes=  true, bool IncludeStackTrace = true)
        {
            StringBuilder result = new StringBuilder();

            void ProcessException(Exception ex)
            {
                if (IncludeTypes)
                    result.AppendLine(ex.GetType().FullName);
                
                result.AppendLine(ex.Message);

                switch (ex)
                {
                    case FileLoadException fex:
                        result.AppendLine($"Unable to load the following file: {fex.FileName}");
                        break;
                }

                if (IncludeStackTrace)
                    result.AppendLine(ex.StackTrace);

                if (ex.InnerException != null)
                {
                    result.AppendLine("== Inner Exception ==");

                    ProcessException(ex.InnerException);
                }
                else if (ex is AggregateException agrex)
                {
                    result.AppendLine("== Inner Exceptions ==");

                    foreach (var inex in agrex.InnerExceptions)
                    {
                        ProcessException(inex);
                    }
                }
            }

            ProcessException(exception);

            return result.ToString();
        }

        public static string CleanPath(string UglyPath)
        {
            return UglyPath?.ToLower()?.Replace('\\', '/');
        }

        public static double ReadPropertyDouble(this XmlElement elem, string PropertyName, double FallbackValue = 0)
        {
            if (elem.HasAttribute(PropertyName) && double.TryParse(elem.GetAttribute(PropertyName), out var propValue))
                return propValue;
            else
                return FallbackValue;
        }

        private static Dictionary<(Type, string), XmlSerializer> _SerializerCache = new Dictionary<(Type, string), XmlSerializer>();
        private static CultureInfo _DefaultParserCulture = CultureInfo.GetCultureInfo("en-US");

        [DebuggerHidden]
        public static T Deserialize<T>(this XmlNode node, string RootElementName = null) where T : class
        {
            if (node == null)
                return null;

            var t = typeof(T);
            var cacheKey = (t, RootElementName);

            if(!_SerializerCache.ContainsKey(cacheKey))
                _SerializerCache[cacheKey] = new XmlSerializer(t, new XmlRootAttribute() { ElementName = RootElementName });

            var serializer = _SerializerCache[cacheKey];

            using (var xmlNodeReader = new XmlNodeReader(node))
            {
                return (T)serializer.Deserialize(xmlNodeReader);
            }
        }

        [DebuggerHidden]
        public static IEnumerable<T> DeserializeBulk<T>(this XmlNodeList nodes, string RootElementName = null) where T : class
        {
            return DeserializeBulk<T>(nodes.OfType<XmlNode>(), RootElementName);
        }

        [DebuggerHidden]
        public static IEnumerable<T> DeserializeBulk<T>(this IEnumerable<XmlNode> nodes, string RootElementName = null) where T : class
        {
            if (nodes == null || nodes.Count() == 0)
                yield break;
            var t = typeof(T);
            var cacheKey = (t, RootElementName);

            if (!_SerializerCache.ContainsKey(cacheKey))
                _SerializerCache[cacheKey] = new XmlSerializer(t, new XmlRootAttribute() { ElementName = RootElementName });

            var serializer = _SerializerCache[cacheKey];

            foreach (XmlNode child in nodes.OfType<XmlElement>())
            {
                using (var xmlNodeReader = new XmlNodeReader(child))
                {
                    yield return (T)serializer.Deserialize(xmlNodeReader);
                }
            }
        }

        public static DN.Matrix4x4 AppendTranslateRotate(this DN.Matrix4x4 transformation, DN.Vector3 translation, DN.Quaternion rotation, DN.Vector3 scale)
        {
            return (DN.Matrix4x4.CreateTranslation(translation) * DN.Matrix4x4.CreateFromQuaternion(rotation)) * transformation;
        }

        public static DN.Matrix4x4 MatrixFromTuple(
            (float, float, float, float, float, float, float, float, float, float, float, float) Transformation)
        {
            return new DN.Matrix4x4(
                        Transformation.Item1, Transformation.Item2, Transformation.Item3, Transformation.Item4, 
                        Transformation.Item5, Transformation.Item6, Transformation.Item7, Transformation.Item8, 
                        Transformation.Item9, Transformation.Item10, Transformation.Item11, Transformation.Item12,
                        0, 0, 0, 1);
        }

        public static DN.Matrix4x4 MatrixFromTuple(
            (float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float) Transformation)
        {
            return new DN.Matrix4x4(
                        Transformation.Item1, Transformation.Item2, Transformation.Item3, Transformation.Item4,
                        Transformation.Item5, Transformation.Item6, Transformation.Item7, Transformation.Item8,
                        Transformation.Item9, Transformation.Item10, Transformation.Item11, Transformation.Item12,
                        Transformation.Item13, Transformation.Item14, Transformation.Item15, Transformation.Item16);
        }

        public static DN.Matrix4x4 MatrixFromTupleTransposed(
            (float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float) Transformation)
        {
            return new DN.Matrix4x4(
                        Transformation.Item1, Transformation.Item5, Transformation.Item9, Transformation.Item13,
                        Transformation.Item2, Transformation.Item6, Transformation.Item10, Transformation.Item14,
                        Transformation.Item3, Transformation.Item7, Transformation.Item11, Transformation.Item15,
                        Transformation.Item4, Transformation.Item8, Transformation.Item12, Transformation.Item16);
        }

        public static DN.Vector3 TransformVec3(this DN.Matrix4x4 transformation, Vector3 coordinate)
        {
            return new DN.Vector3(
                transformation.M11 * coordinate.X + transformation.M12 * coordinate.X + transformation.M13 * coordinate.X + transformation.M14 * coordinate.X,
                transformation.M11 * coordinate.Y + transformation.M12 * coordinate.Y + transformation.M13 * coordinate.Y + transformation.M14 * coordinate.Y,
                transformation.M11 * coordinate.Z + transformation.M12 * coordinate.Z + transformation.M13 * coordinate.Z + transformation.M14 * coordinate.Z
                );
        }

        public static DN.Vector3 CalculateScale(this DN.Matrix4x4 transformation)
        {
            return new DN.Vector3(
                new DN.Vector3(transformation.M11, transformation.M12, transformation.M13).Length(),
                new DN.Vector3(transformation.M21, transformation.M22, transformation.M23).Length(),
                new DN.Vector3(transformation.M31, transformation.M32, transformation.M33).Length()
                );
        }

        public static bool Decompose(this DN.Matrix4x4 transformation, out Vector3 translation, out Vector4 rotation, out Vector3 scale)
        {
            /*
            var result = DN.DNUtils.DecomposeDouble(transformation, out var dnScale, out var dnRotation, out var dnTranslation);

            //TODO: Not sure why decompose fails in certain situations (double vs float precision?)
            //position calculation will always be 100% so theres that :shrug:
            //DecomposeDouble writes the results out progressively, with rotation being the only thing that can fail

            translation = new Vector3(dnTranslation.X, dnTranslation.Y, dnTranslation.Z);
            rotation = new Vector4(dnRotation.X, dnRotation.Y, dnRotation.Z, dnRotation.W);
            scale = new Vector3(dnScale.X, dnScale.Y, dnScale.Z);

            return result;
            */

            
            translation = new Vector3(transformation.M14, transformation.M24, transformation.M34);

            var Sx = new DN.Vector3(transformation.M11, transformation.M21, transformation.M31).Length();
            var Sy = new DN.Vector3(transformation.M11, transformation.M21, transformation.M31).Length();
            var Sz = new DN.Vector3(transformation.M11, transformation.M21, transformation.M31).Length();
            scale = new Vector3(Sx, Sy, Sz);
            
            rotation = new Vector4((
                transformation.M11 / scale.X, transformation.M12 / scale.Y, transformation.M13 / scale.Z,
                transformation.M21 / scale.X, transformation.M22 / scale.Y, transformation.M23 / scale.Z,
                transformation.M31 / scale.X, transformation.M32 / scale.Y, transformation.M33 / scale.Z));
            
            return true;
        }

        public static DN.Vector3 ToEulerAngles(this DN.Quaternion q)
        {
            var rotMat = CgfConverter.Structs.Matrix3x3.CreateFromQuaternion(
                new System.Numerics.Quaternion((float)q.X, (float)q.Y, (float)q.Z, (float)q.W));

            double sy = Math.Sqrt(rotMat.M11 * rotMat.M11 + rotMat.M21 * rotMat.M21);
            bool singular = sy < 1e-6;

            var rtd = 180 / Math.PI;

            if (!singular)
            {
                return new DN.Vector3(
                    rtd * Math.Atan2(rotMat.M32, rotMat.M33),
                    rtd * Math.Atan2(-rotMat.M31, sy),
                    rtd * Math.Atan2(rotMat.M21, rotMat.M11)
                    );
            }
            else
            {
                return new DN.Vector3(
                    rtd * Math.Atan2(-rotMat.M23, rotMat.M22),
                    rtd * Math.Atan2(-rotMat.M31, sy),
                    rtd * 0
                    );
            }

            /*
            // Store the Euler angles in radians
            var pitchYawRoll = new DN.Vector3();

            var sqw = q.W * q.W;
            var sqx = q.X * q.X;
            var sqy = q.Y * q.Y;
            var sqz = q.Z * q.Z;

            // If quaternion is normalised the unit is one, otherwise it is the correction factor
            var unit = sqx + sqy + sqz + sqw;
            var test = q.X * q.Y + q.Z * q.W;

            if (test > 0.4999f * unit)                              // 0.4999f OR 0.5f - EPSILON
            {
                // Singularity at north pole
                pitchYawRoll.Y = 2f * (float)Math.Atan2(q.X, q.W);  // Yaw
                pitchYawRoll.X = Math.PI * 0.5f;                         // Pitch
                pitchYawRoll.Z = 0f;                                // Roll
            }
            else if (test < -0.4999f * unit)                        // -0.4999f OR -0.5f + EPSILON
            {
                // Singularity at south pole
                pitchYawRoll.Y = -2f * (float)Math.Atan2(q.X, q.W); // Yaw
                pitchYawRoll.X = -Math.PI * 0.5f;                        // Pitch
                pitchYawRoll.Z = 0f;                                // Roll
            }
            else
            {
                pitchYawRoll.Y = (float)Math.Atan2(2f * q.Y * q.W - 2f * q.X * q.Z, sqx - sqy - sqz + sqw);       // Yaw
                pitchYawRoll.X = (float)Math.Asin(2f * test / unit);                                             // Pitch
                pitchYawRoll.Z = (float)Math.Atan2(2f * q.X * q.W - 2f * q.Y * q.Z, -sqx + sqy - sqz + sqw);      // Roll
            }

            //Convert back to degrees
            pitchYawRoll.Z = (pitchYawRoll.X / Math.PI) * 180.0;
            pitchYawRoll.Y = (pitchYawRoll.Y / Math.PI) * 180.0;
            pitchYawRoll.X = (pitchYawRoll.Z / Math.PI) * 180.0;

            return pitchYawRoll;
            */

            /*
            DN.Vector3 euler = new DN.Vector3();

            // if the input quaternion is normalized, this is exactly one. Otherwise, this acts as a correction factor for the quaternion's not-normalizedness
            double unit = (q.X * q.X) + (q.Y * q.Y) + (q.Z * q.Z) + (q.W * q.W);

            // this will have a magnitude of 0.5 or greater if and only if this is a singularity case
            double test = q.X * q.W - q.Y * q.Z;

            if (test > 0.499995 * unit) // singularity at north pole
            {
                euler.X = Math.PI / 2;
                euler.Y = 2.0 * Math.Atan2(q.Y, q.X);
                euler.Z = 0;
            }
            else if (test < -0.499995 * unit) // singularity at south pole
            {
                euler.X = -Math.PI / 2;
                euler.Y = -2.0 * Math.Atan2(q.Y, q.X);
                euler.Z = 0;
            }
            else // no singularity - this is the majority of cases
            {
                euler.X = Math.Asin(2.0 * (q.W * q.X - q.Y * q.Z));
                euler.Y = Math.Atan2(2.0 * q.W * q.Y + 2.0 * q.Z * q.X, 1 - 2.0 * (q.X * q.X + q.Y * q.Y));
                euler.Z = Math.Atan2(2.0 * q.W * q.Z + 2.0 * q.X * q.Y, 1 - 2.0 * (q.Z * q.Z + q.X * q.X));
            }

            // all the math so far has been done in radians. Before returning, we convert to degrees...
            euler *= 180 / Math.PI;

            //...and then ensure the degree values are between 0 and 360
            euler.X %= 360;
            euler.Y %= 360;
            euler.Z %= 360;

            return euler;
            */
        }

        public static DN.Quaternion EulerToQuaternion(this DN.Vector3 euler)
        {
            var xOver2 = euler.X * (Math.PI / 180.0) * 0.5;
            var yOver2 = euler.Y * (Math.PI / 180.0) * 0.5;
            var zOver2 = euler.Z * (Math.PI / 180.0) * 0.5;

            var sinXOver2 = Math.Sin(xOver2);
            var cosXOver2 = Math.Cos(xOver2);
            var sinYOver2 = Math.Sin(yOver2);
            var cosYOver2 = Math.Cos(yOver2);
            var sinZOver2 = Math.Sin(zOver2);
            var cosZOver2 = Math.Cos(zOver2);

            DN.Quaternion result = new DN.Quaternion(
                cosYOver2 * sinXOver2 * cosZOver2 + sinYOver2 * cosXOver2 * sinZOver2,
                sinYOver2 * cosXOver2 * cosZOver2 - cosYOver2 * sinXOver2 * sinZOver2,
                cosYOver2 * cosXOver2 * sinZOver2 - sinYOver2 * sinXOver2 * cosZOver2,
                cosYOver2 * cosXOver2 * cosZOver2 + sinYOver2 * sinXOver2 * sinZOver2);

            return result;
        }
        public static string FormatXML(string UglyXML)
        {
            if (string.IsNullOrWhiteSpace(UglyXML)) return "";

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(UglyXML);
            
            return FormatXML(doc);
        }
        public static string FormatBytes(long numBytes)
        {
            if (numBytes < 1024)
                return $"{numBytes} B";

            if (numBytes < 1024L * 1024L)
                return $"{numBytes / 1024d:0.##} KB";

            if (numBytes < 1024L * 1024L * 1024L)
                return $"{numBytes / (1024d * 1024d):0.##} MB";

            if (numBytes < 1024L * 1024L * 1024L * 1024L)
                return $"{numBytes / (1024d * 1024d * 1024d):0.##} GB";

            if (numBytes < 1024L * 1024L * 1024L * 1024L * 1024L)
                return $"{numBytes / (1024d * 1024d * 1024d * 1024d):0.##} TB";

            if (numBytes < 1024L * 1024L * 1024L * 1024L * 1024L * 1024L)
                return $"{numBytes / (1024d * 1024d * 1024d * 1024d * 1024d):0.##} PB";

            return $"{numBytes / (1024d * 1024d * 1024d * 1024d * 1024d * 1024d):0.##} EB";
        }

        public static string FormatXML(XmlDocument UglyDocument)
        {
            if (UglyDocument == null) return "";

            string strRetValue = null;
            Encoding enc = Encoding.UTF8;
            // enc = new System.Text.UTF8Encoding(false);

            XmlWriterSettings xmlWriterSettings = new XmlWriterSettings();
            xmlWriterSettings.Encoding = enc;
            xmlWriterSettings.Indent = true;
            xmlWriterSettings.IndentChars = "    ";
            xmlWriterSettings.NewLineChars = "\r\n";
            xmlWriterSettings.NewLineHandling = NewLineHandling.Replace;
            xmlWriterSettings.NewLineOnAttributes = true;
            //xmlWriterSettings.OmitXmlDeclaration = true;
            xmlWriterSettings.ConformanceLevel = ConformanceLevel.Document;


            using (MemoryStream ms = new MemoryStream())
            {
                using (XmlWriter writer = XmlWriter.Create(ms, xmlWriterSettings))
                {
                    UglyDocument.Save(writer);
                    writer.Flush();
                    ms.Flush();

                    writer.Close();
                }

                ms.Position = 0;
                using (StreamReader sr = new StreamReader(ms, enc))
                {
                    strRetValue = sr.ReadToEnd();

                    sr.Close();
                }

                ms.Close();
            }

            xmlWriterSettings = null;
            return strRetValue;
        }

        public static IEnumerable<T> PathToTree<T>(
            IEnumerable<T> FlatStructure,
            Func<T, string> PathSelector,
            Func<T, System.Collections.IList> ChildCollectionSelector,
            Func<string, T> DummyFactory,
            bool SortDirectories = true)
        {
            Dictionary<string, List<T>> folderChildren = new Dictionary<string, List<T>>();
            HashSet<string> registeredPaths = new HashSet<string>(); //Lookup optimization
            folderChildren[""] = new List<T>(); // Root collection

            void EnsurePath(string Path)
            {
                if (string.IsNullOrEmpty(Path))
                    return;

                var parentFolder = System.IO.Path.GetDirectoryName(Path);

                // Make sure a potential parent folder exists first
                if (!string.IsNullOrEmpty(parentFolder))
                    EnsurePath(parentFolder);

                if (!folderChildren.ContainsKey(parentFolder))
                    folderChildren[parentFolder] = new List<T>();



                // If we haven't created one yet, add it
                if (!registeredPaths.Contains(Path))
                {
                    folderChildren[parentFolder].Add(DummyFactory(Path));
                    registeredPaths.Add(Path);
                }

                if (!folderChildren.ContainsKey(Path))
                {
                    folderChildren[Path] = new List<T>();
                    registeredPaths.Add(Path);
                }
            }

            var children = FlatStructure.ToDictionary<T, string>(PathSelector);
            var paths = SortDirectories ? 
                children.Keys.OrderBy(x => x, StringComparer.InvariantCulture).AsEnumerable() : 
                children.Keys;

            foreach (var path in paths)
            {
                var filePath = Path.GetDirectoryName(path);

                EnsurePath(filePath);
                folderChildren[filePath].Add(children[path]);
            }

            List<T> rootItems = folderChildren[""];

            foreach (var childSet in folderChildren)
            {
                if (string.IsNullOrEmpty(childSet.Key))
                    continue;

                var parentFolder = Path.GetDirectoryName(childSet.Key);

                var parentItem = folderChildren[parentFolder].FirstOrDefault(t => PathSelector(t).Equals(childSet.Key));

                if (parentItem == null)
                {

                }

                var childCollection = ChildCollectionSelector(parentItem);

                foreach (var childItem in childSet.Value)
                {
                    childCollection.Add(childItem);
                }
            }

            return rootItems;
        }

        public static System.Drawing.Color AdjustHSV(this System.Drawing.Color self, float HueAdjust, float SaturationAdjust, float ValueAdjust)
        {
            return Utilities.FromAhsb(
                self.A,
                Math.Min(1, Math.Max(0, self.GetHue() + HueAdjust)),
                Math.Min(1, Math.Max(0, self.GetSaturation() + SaturationAdjust)),
                Math.Min(1, Math.Max(0, self.GetBrightness() + ValueAdjust)));
        }

        public static System.Drawing.Color hsv2rgb(float h, float s, float v)
        {
            Func<float, int> f = delegate (float n)
            {
                float k = (n + h * 6) % 6;
                return (int)((v - (v * s * (Math.Max(0, Math.Min(Math.Min(k, 4 - k), 1))))) * 255);
            };
            return System.Drawing.Color.FromArgb(f(5), f(3), f(1));
        }

        public static System.Drawing.Color FromAhsb(int alpha, float hue, float saturation, float brightness)
        {

            if (0 > alpha || 255 < alpha)
            {
                throw new ArgumentOutOfRangeException("alpha", alpha,
                  "Value must be within a range of 0 - 255.");
            }
            if (0f > hue || 360f < hue)
            {
                throw new ArgumentOutOfRangeException("hue", hue,
                  "Value must be within a range of 0 - 360.");
            }
            if (0f > saturation || 1f < saturation)
            {
                throw new ArgumentOutOfRangeException("saturation", saturation,
                  "Value must be within a range of 0 - 1.");
            }
            if (0f > brightness || 1f < brightness)
            {
                throw new ArgumentOutOfRangeException("brightness", brightness,
                  "Value must be within a range of 0 - 1.");
            }

            if (0 == saturation)
            {
                return System.Drawing.Color.FromArgb(alpha, Convert.ToInt32(brightness * 255),
                  Convert.ToInt32(brightness * 255), Convert.ToInt32(brightness * 255));
            }

            float fMax, fMid, fMin;
            int iSextant, iMax, iMid, iMin;

            if (0.5 < brightness)
            {
                fMax = brightness - (brightness * saturation) + saturation;
                fMin = brightness + (brightness * saturation) - saturation;
            }
            else
            {
                fMax = brightness + (brightness * saturation);
                fMin = brightness - (brightness * saturation);
            }

            iSextant = (int)Math.Floor(hue / 60f);
            if (300f <= hue)
            {
                hue -= 360f;
            }
            hue /= 60f;
            hue -= 2f * (float)Math.Floor(((iSextant + 1f) % 6f) / 2f);
            if (0 == iSextant % 2)
            {
                fMid = hue * (fMax - fMin) + fMin;
            }
            else
            {
                fMid = fMin - hue * (fMax - fMin);
            }

            iMax = Convert.ToInt32(fMax * 255);
            iMid = Convert.ToInt32(fMid * 255);
            iMin = Convert.ToInt32(fMin * 255);

            switch (iSextant)
            {
                case 1:
                    return System.Drawing.Color.FromArgb(alpha, iMid, iMax, iMin);
                case 2:
                    return System.Drawing.Color.FromArgb(alpha, iMin, iMax, iMid);
                case 3:
                    return System.Drawing.Color.FromArgb(alpha, iMin, iMid, iMax);
                case 4:
                    return System.Drawing.Color.FromArgb(alpha, iMid, iMin, iMax);
                case 5:
                    return System.Drawing.Color.FromArgb(alpha, iMax, iMin, iMid);
                default:
                    return System.Drawing.Color.FromArgb(alpha, iMax, iMid, iMin);
            }
        }
    }
}
