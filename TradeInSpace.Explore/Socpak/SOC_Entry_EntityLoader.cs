﻿using CgfConverter.CryEngineCore;
using CgfConverter.Models;
using ICSharpCode.SharpZipLib.Zip;
using Serilog;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.DoubleNumerics;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using TradeInSpace.Explore.Subsumption;
using unforge;

namespace TradeInSpace.Explore.Socpak
{
    using DN = System.DoubleNumerics;

    public partial class SOC_Entry
    {


        static string[] KeepClass = new string[] { "SCShop" };
        static string[] KeepLayer = new string[] { "landing_pads", "shop_admin", "shop_armor", "shop_casaba", "shop_platinumbay", "Platinumbay_sml_001", "casaba_sml_001",
            "New_Deal", "LivefireWeapons_sml_001", "hangar_smlfrnt_001","hangar_smltop_001","hangar_xltop_001" };
        static string[] IgnoreComponents = new string[] { "EntityComponentLight" };/*, "EntityComponentParticleEffect", "AudioTriggerSpot", "FogVolume", "AudioAreaAmbience",
            "EntityComponentSimpleRotation", "GravityArea", "EntityComponentCameraSource", "EntityComponentHolographicVolume",
            "LedgeObject", "EntityComponentEnvironmentLight", "EntityComponentLightGroup", "EntityComponentMusicArea", "EntityComponentColorGradient",
            "EntityComponentRenderToTextureView", "EntityComponentHazard", "EntityComponentRoomConnector", "GravityBox" };*/
        static string[] KeepComponents = new string[] { "HarvestableProviderComponent", "SCTransitDestination", "SCTransitGateway", "EntityComponentShop",
            "LandingArea", "ActionAreaComponent", "LandingAreaGroup", "EntityComponentComment", "EntityComponentShoppingProvider", "EntityComponentActorUsable",
            "EntityComponentShipInsuranceProvider"};

#if DEBUG
        //Static, so stays between calls
        public static HashSet<string> AllSeenClasses = new HashSet<string>();
        public static HashSet<string> AllSeenLayer = new HashSet<string>();
#endif

        internal void PreloadEntities()
        {
            var exposedEntities = Archive.RootDoc.SelectNodes("ObjectContainer/ExposedEntities/Entity");

            if (exposedEntities != null)
            {
                Entities.AddRange(exposedEntities.Cast<XmlElement>().Select(xe => ExposedEntity.FromXML(xe, this)));

                foreach (var entity in Entities)
                {
                    //entity.Tags = SourceFile.LoadedFrom.ConvertTags(entity.Tags);
                }
            }
        }

        private FullEntity LoadSingleEntity(XmlElement EntityElement, bool FilterByComponents = true, bool DynamicComponents = true)
        {
#if DEBUG
            AllSeenClasses.Add(EntityElement.GetAttribute("EntityClass"));
            AllSeenLayer.Add(EntityElement.GetAttribute("Layer"));
#endif

            if (FilterByComponents)
            {
                //Class trumps all
                if (!KeepClass.Contains(EntityElement.GetAttribute("EntityClass")))
                {
                    //Then layer
                    if (!KeepLayer.Contains(EntityElement.GetAttribute("Layer")) || !EntityElement.GetAttribute("Layer").StartsWith("Gameplay"))
                    {
                        var components = EntityElement.SelectNodes(".//PropertiesDataCore/*").Cast<XmlElement>();
                        //Skip if there are no components
                        if (components.Count() == 0)
                            return null;
                        //If we have *ANY* that we want to keep, don't check ignore list
                        if (!components.Any(e => KeepComponents.Contains(e.LocalName)))
                        {
                            //Skip this one if any it matches our ignore list
                            if (components.Any(e => IgnoreComponents.Contains(e.LocalName)))
                                return null;
                        }
                    }
                }
            }

            //Link SOC Entity to Entity prototype from database
            var fullEntity = FullEntity.FromXML(EntityElement, this, DynamicComponents);
            fullEntity.DatabaseEntity = SourceFile.LoadedFrom.GetEntity(fullEntity.ClassGUID);

            return fullEntity;
        }

        public void LoadAllEntities(bool FilterByComponents = true, bool DynamicComponents = true, bool ModelReferences = true)
        {
            if (EntitiesLoaded) return;

            //Mark this early, so we only ever check a SOC_Entry once.
            EntitiesLoaded = true;
            //Save these for later
            var exposedGUIDs = Entities.Select(e => e.GUID);
            Entities.Clear();


            //First handle our own entity
            if (AdditionalData != null)
            {
                this.OwnEntity = LoadSingleEntity(AdditionalData, FilterByComponents, DynamicComponents);

                Entities.Add(OwnEntity);
            }

            //Then planet data seperately
            if (HasPlanetDetails())
            {
                var planetDetails = LoadPlanetDetails();
                PlanetJSON = LoadPlanetJSON();
                var planetElement = planetDetails?.SelectNodes("Entities/Entity")?.OfType<XmlElement>()?.SingleOrDefault();
                if (planetDetails != null && PlanetJSON != null && planetElement != null)
                {
                    this.PlanetEntity = LoadSingleEntity(planetElement, FilterByComponents, DynamicComponents);

                    Entities.Add(PlanetEntity);
                }
                else
                {
                    Log.Warning($"NULL Planet JSON for planet: {Name}");
                }
            }
            

            if (!HasAddtionalEntities) return;
            //Then handle child entities

            var entityFile = Archive.ReadPackedCGAFile(EntitesPath);
            try
            {
                entityFile.ProcessCryengineFiles();

                var xmlData = entityFile.Chunks.OfType<ChunkBinaryXmlData_3>().SingleOrDefault();

                if (xmlData != null)
                {
                    using (var ms = new MemoryStream(xmlData.Data))
                    {
                        var SOCentities = CryXmlSerializer.ReadStream(ms);
                        EntitiesDocument = SOCentities?.DocumentElement;

                        if (SOCentities == null) return;

                        var entitiesA = SOCentities.SelectNodes("SCOC_Entities/Entity").OfType<XmlElement>();
                        var entitiesB = SOCentities.SelectNodes("Entities/Entity").OfType<XmlElement>();

                        foreach (XmlElement childEnt in entitiesA.Union(entitiesB))
                        {
                            var matchingEntity = LoadSingleEntity(childEnt, FilterByComponents, DynamicComponents);
                            if (matchingEntity == null) { continue; }

                            this.Entities.Add(matchingEntity);
                        }

                        //Then handle any post-processing that might need to happen
                        foreach (var ent in Entities.OfType<FullEntity>())
                        {
                            ent.EntryFullyLoaded();
                        }
                    }
                }

                if (ModelReferences)
                {
                    var geomData = entityFile.Chunks.OfType<ChunkBulkModelReference>()
                        .Where(c => !c.ParseError)
                        .ToList();
                    foreach (var geomChunk in geomData)
                    {
                        foreach (var geometry in geomChunk.References.Where(r => r.ReferenceType == BulkReferenceType.Model))
                        {
                            if (geometry.ModelIndex < 0 || geometry.ModelIndex >= geomChunk.ReferencedModels.Count)
                                continue;

                            var modelPath = geomChunk.ReferencedModels[geometry.ModelIndex];

                            var transform = Utilities.MatrixFromTuple(geometry.Transform);
                            transform.Decompose(out var translate, out var rotate, out var scale);


                            GeometryReferences.Add(new SOC_ReferencedGeometry()
                            {
                                ID = geometry.ID,
#if FALSE //DEBUG
                                Name = $"{Name}_{modelPath}_{Convert.ToBase64String(geometry.ReferenceBytes)}",
#else
                                Name = Path.GetFileNameWithoutExtension(modelPath),
#endif
                                ModelPath = Utilities.CleanPath(modelPath),
                                LocalPosition = translate,
                                LocalRotation = rotate,
                                LocalScale = scale,
                            });
                        }
                    }
                }
            }
            catch (FileLoadException ex)
            {
                Log.Warning($"Failed to load .soc entity details for {Name}/{ArchivePath}");
            }
        }
    }
}
