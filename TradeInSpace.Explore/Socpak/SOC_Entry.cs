﻿using CgfConverter;
using Newtonsoft.Json.Linq;
using Serilog;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using TradeInSpace.Explore.DataForge;
using TradeInSpace.Explore.Subsumption;

namespace TradeInSpace.Explore.Socpak
{
    using DN = System.DoubleNumerics;

    [DebuggerDisplay("SC {Name} ({Label}) [{Class}]")]
    public partial class SOC_Entry
    {
        /// <summary>
        /// Direct parent of this entry.
        /// </summary>
        public SOC_Entry Parent { get; set; }

        /// <summary>
        /// The archive the original entity was created from.
        /// For planets, this is the star system
        /// but it's generally next highest logical owner
        /// </summary>
        internal SOC_Archive SourceFile { get; set; }

        /// <summary>
        /// The entry's own archive, for loading it's entity children as well as additional structure.
        /// </summary>
        public SOC_Archive Archive { get; set; }

        /// <summary>
        /// Represents the entity of the .socpak itself, usually a planet for a planets' socpak
        /// </summary>
        public FullEntity OwnEntity { get; set; }

        /// <summary>
        /// Represents the planet itself if our socpak is for a planet
        /// this is technically a child of OwnEntity, but entities don't have parent/child directly
        /// </summary>
        public FullEntity PlanetEntity { get; set; }

        //Something new used for planets. Planet meta-data should just be broken out itself...
        public string PlanetJSON { get; set; }

        public string ArchivePath { get; set; }
        public string Name { get; set; }

        public string Label { get; set; }
        public bool Visible { get; set; }

        public Guid GUID { get; set; }
        public Guid? StarmapRecord { get; set; } = null;
        public string SuperGUID { get; set; }

        public string Pos { get; set; }
        public string Rot { get; set; }
        public string Scale { get; set; }
        public string Class { get; set; }

        public override string ToString()
        {
            return $"SOC Entry: {Name} - {GUID} ({Label}) [{Class}]";
        }

        public List<string> Tags { get; set; }

        public Vector3 Position
        {
            get
            {
                return Vector3.FromString(Pos);
            }
        }

        public Vector4 Rotation
        {
            get
            {
                return Vector4.FromString(Rot);
            }
        }
        [XmlIgnore]
        public DN.Matrix4x4 Transform { get; set; }
        [XmlIgnore]
        public Vector3 GlobalPosition { get; set; }
        [XmlIgnore]
        public Vector4 GlobalRotation { get; set; }
        [XmlIgnore]
        public Vector3 LocalPosition { get { return Vector3.FromString(Pos); } }
        public Vector4 LocalRotation { get { return Vector4.FromString(Rot); } }
        public Vector3 LocalScale { get { return Vector3.FromString(Scale, new Vector3(1, 1, 1)); } }

        public bool ArchiveLoaded { get => Archive != null; }
        public bool EntitiesLoaded { get; set; }


        private bool HasAdditionalData { get; set; }
        private bool HasAddtionalEntities { get => Archive?.HasRecord(EntitesPath) ?? false; }


        private string EntitesPath
        {
            get => Path.ChangeExtension(Archive.Name, ".soc");
        }


        public List<SOC_Entry> Children { get; set; } = new List<SOC_Entry>();
        public List<ExposedEntity> Entities { get; set; } = new List<ExposedEntity>();
        public List<SOC_ReferencedGeometry> GeometryReferences { get; set; } = new List<SOC_ReferencedGeometry>();

        public XmlElement Element { get; private set; }
        public XmlElement EntitiesDocument { get; private set; }
        public XmlElement AdditionalData { get; set; }

        private SOC_Entry(SOC_Archive forFile)
        {
            SourceFile = forFile;
        }

        internal static SOC_Entry CreateRoot(string Path)
        {
            var shortName = System.IO.Path.GetFileNameWithoutExtension(Path.Replace('\\', '/'));

            return new SOC_Entry(null)
            {
                Element = null,
                Parent = null,

                ArchivePath = Path,
                Name = shortName
            };
        }

        internal static SOC_Entry FromXMLElement(XmlElement Element, SOC_Archive FromArchive, SOC_Entry ParentSOC)
        {
            var entry = new SOC_Entry(FromArchive)
            {
                Element = Element,
                Parent = ParentSOC,

                ArchivePath = Element.GetAttribute("name"),
                Name = Element.GetAttribute("entityName"),
                Class = Element.GetAttribute("class"),
                Label = Element.GetAttribute("label"),
                GUID = Guid.Parse(Element.GetAttribute("guid")),
                StarmapRecord = Element.HasAttribute("starMapRecord") ?
                                            (Guid?)Guid.Parse(Element.GetAttribute("starMapRecord")) : null,
                Visible = Element.GetAttribute("visible") == "1",
                Pos = Element.GetAttribute("pos"),
                Rot = Element.GetAttribute("rot"),
                HasAdditionalData = Element.GetAttribute("hasAdditionalData") == "1",
                Tags = Element.HasAttribute("tags") ? Element.GetAttribute("tags").Split(',').ToList() : new List<string>()
            };
            entry.SuperGUID = entry.BuildSuperGUID();
            return entry;
        }

        public SOC_Entry SearchUpSoc(Func<SOC_Entry, bool> Test, bool IncludeRoot = false)
        {
            var checkingParent = IncludeRoot ? this : this.Parent;
            while (checkingParent != null)
            {
                if (Test(checkingParent))
                {
                    return checkingParent;
                }
                checkingParent = checkingParent.Parent;
            }
            return null;
        }

        public FullEntity GetEntity(Guid ID)
        {
            return Entities.FirstOrDefault(ent => ent.GUID == ID) as FullEntity;
        }

        public bool LoadFullDetails()
        {
            if (this.ArchiveLoaded) return true;
            //TODO: Is this the best way to handle this???
            //DFDatabase.UpdateProgress(this, ProgressPhase.Socpak, 0, 0);
            //var detailsPath = Path.ChangeExtension(Path.GetFileNameWithoutExtension(SourceFile.Path), ".xml");

            this.Archive = SOC_Archive.LoadFromSoc(SourceFile.LoadedFrom, this, ArchivePath);
            return ArchiveLoaded;
        }

        private bool HasPlanetDetails()
        {
            return Archive?.HasRecord($"{Archive.Name}.pla") ?? false;
        }

        private XmlDocument LoadPlanetDetails()
        {
            return Archive?.ReadCryXML($"{Archive.Name}.pla");
        }

        private string LoadPlanetJSON()
        {
            return Archive?.ReadPlanetJSON($"{Archive.Name}.pla");
        }

        private CryEngine LoadPlanetFull()
        {
            return Archive.ReadPackedCGAFile($"{Archive.Name}.pla");
        }

        private XmlDocument LoadRemapDetails()
        {
            return Archive?.ReadCryXML($"{Archive.Name}.rmp");
        }

        public IEnumerable<(int Depth, SOC_Entry Container)> ExploreRecursive(bool ReturnRoot = true, bool LoadEntities = false)
        {
            return ExploreRecursiveInner(0, ReturnRoot, (_) => true, LoadEntities: LoadEntities);
        }

        public IEnumerable<(int Depth, SOC_Entry Container)> ExploreRecursive(
            bool ReturnRoot = true, Func<(int Depth, SOC_Entry Container), bool> ShouldExplore = null,
            bool LoadEntities = false)
        {
            return ExploreRecursiveInner(0, ReturnRoot, ShouldExplore, LoadEntities: LoadEntities);
        }

        internal IEnumerable<(int Depth, SOC_Entry Container)> ExploreRecursiveInner(
            int Depth, bool ReturnRoot = true, Func<(int Depth, SOC_Entry Container), bool> ShouldExplore = null,
            bool LoadEntities = false)
        {
            if (Depth == 0 && ReturnRoot)
            {
                yield return (Depth, this);
            }

            foreach (var sock in Children)
            {
                var currentCheck = (Depth + 1, sock);

                if (ShouldExplore != null && !ShouldExplore(currentCheck)) continue;

                sock.LoadFullDetails();
                if (LoadEntities)
                    sock.LoadAllEntities(FilterByComponents: false);

                yield return currentCheck;

                foreach (var recursiveChild in sock.ExploreRecursiveInner(Depth + 1, ReturnRoot, ShouldExplore, LoadEntities: LoadEntities))
                {
                    yield return recursiveChild;
                }
            }

            yield break;
        }

        public FullEntity SearchActionAreaGUID(string ActionAreaSuperGUID)
        {
            var GUIDChunks = ActionAreaSuperGUID
                .Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
                .Select(g => Guid.Parse(g))
                .ToList();
            if (GUIDChunks.Count == 0) return null;

            var ActionAreaGUID = GUIDChunks.Last();

            SOC_Entry currentSearch = this;

            for (int searchIndex = 0; searchIndex < GUIDChunks.Count - 1; searchIndex++)
            {
                currentSearch.LoadFullDetails();
                currentSearch.LoadAllEntities(false);

                //This handles most cases when just looking for the next container
                var nextChild = currentSearch.Children.SingleOrDefault(c => c.GUID == GUIDChunks[searchIndex]);

                if (nextChild == null)
                {
                    Log.Warning($"Unable to find GUID [{GUIDChunks[searchIndex]}] on [{currentSearch.GUID}]. Resorting to depth search for final AA Guid.");

                    //Do a recursive search for the remainng few that fail by explicity searching
                    //for the last GUID in the path, which represents the ActionArea's GUID
                    bool foundRecord = false;
                    foreach (var searchChild in currentSearch.ExploreRecursive(true, LoadEntities: true))
                    {
                        searchChild.Item2.LoadFullDetails();
                        searchChild.Item2.LoadAllEntities(false);

                        //If we come across the next container we expect, we may as well use that
                        if (searchChild.Item2.GUID == GUIDChunks[searchIndex])
                        {
                            foundRecord = true;
                            currentSearch = searchChild.Item2;
                            break;
                        }
                    }

                    if (!foundRecord)
                    {
                        //If we don't find anything in the recursive search, throw an error
                        Log.Error($"Unable to find GUID [{GUIDChunks[searchIndex]}] on [{currentSearch.GUID}]");
                        return null;
                    }
                }
                else
                    currentSearch = nextChild;
            }

            //Go from the deepest search we found, and look for the AA guid from there
            foreach (var searchChild in currentSearch.ExploreRecursive(true, LoadEntities: true))
            {
                foreach (var entCheck in searchChild.Item2.Entities)
                {
                    if (entCheck.GUID == ActionAreaGUID)
                    {
                        return entCheck as FullEntity;
                    }
                }
            }

            Log.Error($"Unable to find ActionArea GUID [{ActionAreaGUID}] on [{currentSearch.GUID}] for SuperGUID [{ActionAreaSuperGUID}]");
            return null;
        }

        public SOC_Entry SearchSuperGUID(string SuperGUID, bool ExpectEntity = true, bool AllowExplore = true)
        {
            var GUIDChunks = SuperGUID
                .Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
                .Select(g => Guid.Parse(g))
                .ToList();
            if (GUIDChunks.Count == 0) return null;

            SOC_Entry currentSearch = this;

            for (int searchIndex = 0; searchIndex < GUIDChunks.Count; searchIndex++)
            {
                if (AllowExplore)
                {
                    currentSearch.LoadFullDetails();
                    currentSearch.LoadAllEntities(false);
                }

                //This handles 95+% of cases when just looking for the next container
                var nextChild = currentSearch.Children.SingleOrDefault(c => c.GUID == GUIDChunks[searchIndex]);

                if (nextChild == null)
                {
                    if (ExpectEntity)
                    {
                        if (searchIndex == GUIDChunks.Count - 2)
                        {
                            Log.Warning($"Unable to find GUID [{GUIDChunks[searchIndex]}] on [{currentSearch.GUID}]. Resorting to depth search for final AA Guid.");
                        }

                        //Do a recursive search for the remainng few that fail by explicity searching
                        //for the last GUID in the path, which represents the ActionArea's GUID
                        bool foundRecord = false;
                        foreach (var searchChild in currentSearch.ExploreRecursive(true, LoadEntities: true))
                        {
                            //If we come across the next container we expect, we may as well use that
                            if (searchChild.Item2.GUID == GUIDChunks[searchIndex])
                            {
                                foundRecord = true;
                                currentSearch = searchChild.Item2;
                                break;
                            }

                            foreach (var entCheck in searchChild.Item2.Entities)
                            {
                                if (entCheck.GUID == GUIDChunks[GUIDChunks.Count - 1])
                                {
                                    return entCheck.Owner;
                                }
                                else if (entCheck.GUID == GUIDChunks[searchIndex])
                                {
                                    throw new Exception($"Expected to find GUID [{GUIDChunks[searchIndex]}] as a container, but found as an entity.");
                                }
                            }
                        }

                        if (!foundRecord)
                        {
                            //If we don't find anything in the recursive search, throw an error
                            Log.Error($"Unable to find GUID [{GUIDChunks[searchIndex]}] on [{currentSearch.GUID}]");
                            return null;
                        }
                    }
                    else
                    {
                        //If we aren't searching for entites, we don't have a final GUID to search for
                        //at best could implement a global match for anything in the remaining super path
                        Log.Error($"Unable to find GUID [{GUIDChunks[searchIndex]}] on [{currentSearch.GUID}]");
                        return null;
                    }
                }
                else
                    currentSearch = nextChild;
            }

            return currentSearch;
        }

        public string BuildSuperGUID()
        {
            string superGUID = $"{this.GUID}";
            var testEntity = this.Parent;
            while (testEntity != null && testEntity.GUID != Guid.Empty)
            {
                superGUID = $"{testEntity.GUID},{superGUID}";
                testEntity = testEntity.Parent;
            }
            return superGUID;
        }

        public void CalculateGlobalPositions()
        {
            void HandleNode(SOC_Entry node, DN.Matrix4x4 parentTransform)
            {
                if (node.Transform == null || node.GlobalPosition == null)
                {
                    node.Transform = parentTransform.AppendTranslateRotate((DN.Vector3)node.LocalPosition, (DN.Quaternion)node.LocalRotation, (DN.Vector3)node.LocalScale);

                    var nodeGlobalTranform = DN.Vector3.Transform(DN.Vector3.Zero, node.Transform);
                    var nodeGlobalRotation = DN.Quaternion.CreateFromRotationMatrix(node.Transform);

                    node.GlobalPosition = nodeGlobalTranform;
                    node.GlobalRotation = nodeGlobalRotation;
                }

                foreach (var child in node.Children)
                {
                    HandleNode(child, node.Transform);
                }

                if (node?.Entities != null)
                {
                    foreach (var entity in node.Entities.OfType<FullEntity>())
                    {
                        if (entity.Transform == null || entity.GlobalPosition == null)
                        {
                            entity.Transform = node.Transform.AppendTranslateRotate((DN.Vector3)entity.LocalPosition, (DN.Quaternion)entity.LocalRotation, (DN.Vector3)entity.LocalScale);

                            var entityGlobalTransform = DN.Vector3.Transform(DN.Vector3.Zero, entity.Transform);
                            var entityGlobalRotation = DN.Quaternion.CreateFromRotationMatrix(entity.Transform);

                            entity.GlobalPosition = entityGlobalTransform;
                            entity.GlobalRotation = entityGlobalRotation;
                        }
                    }
                }
            }

            HandleNode(this, DN.Matrix4x4.Identity);
        }
    }
}

