﻿using System;
using System.Collections.Generic;
using System.Text;
using TradeInSpace.Explore.Subsumption;

namespace TradeInSpace.Explore.Socpak
{
    using DN = System.DoubleNumerics;

    public class SOC_ReferencedGeometry
    {
        public int ID;
        public string Name;
        public Vector3 LocalPosition = new Vector3();
        public Vector4 LocalRotation = new Vector4(0, 0, 0, 1);
        public Vector3 LocalScale = new Vector3(-1, 1, 1);
        public Vector3 MinBounds = new Vector3();
        public Vector3 MaxBounds = new Vector3();
        public int ParentID = -1;
        public string ModelPath = null;

        public DN.Matrix4x4 Transform { get; set; }
    }
}
