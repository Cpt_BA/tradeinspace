﻿using CgfConverter;
using CgfConverter.CryEngineCore;
using Newtonsoft.Json;
using Serilog;
using System;
using System.Collections.Generic;
using System.DoubleNumerics;
using System.IO;
using System.Linq;
using System.Text;
using TradeInSpace.Explore.DataForge;

namespace TradeInSpace.Explore.Socpak
{
    public class ModelCache
    {
        Dictionary<string, ModelCachedProperties> _cachedModelProperties = new Dictionary<string, ModelCachedProperties>();

        static bool UseComplexShapes = true;

        public ModelCache(DFDatabase RawGameDB, string CacheFilePath = null)
        {
            if(!string.IsNullOrEmpty(CacheFilePath) && File.Exists(CacheFilePath))
            {
                _cachedModelProperties = JsonConvert.DeserializeObject<Dictionary<string, ModelCachedProperties>>(File.ReadAllText(CacheFilePath));
            }

            this.RawGameDB = RawGameDB;
            this.CacheFilePath = CacheFilePath;
        }

        private ModelCachedProperties DetermineProperties(string ModelPath)
        {
            var modelCGA = RawGameDB.ReadPackedCGAFile(ModelPath);

            if (modelCGA == null)
                throw new FileNotFoundException(ModelPath);

            modelCGA.ProcessCryengineFiles();

            if (modelCGA.Models.All(m => !m.HasGeometry))
                return new ModelCachedProperties(ModelPath, Invisible: true);

            AARect3 bounds = new AARect3();
            var allVectors = GetAllModelVerticies(modelCGA).SelectMany(v => v).ToList();
            List<Vector2> convexHull = null;

            if (allVectors?.Count > 0 && UseComplexShapes)
            {
                //Outer bounds calculation
                foreach (var vector in allVectors)
                {
                    bounds.AddPoint(vector);
                }

                Log.Verbose($"Model {modelCGA.InputFile} contains {allVectors.Count} vectors.");

                convexHull = CollapseVectors(allVectors);

                if(convexHull.Count < 3)
                {
                    convexHull = null;
                }
                else
                {
                    Log.Verbose($"Model reduced to {convexHull.Count} points for a convex hull.");
                }
            }
            else
            {
                foreach (var chunk in modelCGA.Models
                    //.Where(m => m.HasGeometry)
                    .SelectMany(m => m.ChunkMap.Values.OfType<ChunkMesh>()))
                {
                    //Only export a bounds if we have verts and indicies
                    if (chunk.NumVertices > 0 && chunk.NumIndices > 0)
                    {
                        bounds.AddPoint(chunk.BoundsMin);
                        bounds.AddPoint(chunk.BoundsMax);
                    }
                }
            }

            if (bounds.Min == null || bounds.Max == null)
            {
                return new ModelCachedProperties(ModelPath, Invisible: true);
            }
            else
            {
                var trueMin = Vector3.Min(bounds.Min, bounds.Max);
                var trueMax = Vector3.Max(bounds.Min, bounds.Max);

                return new ModelCachedProperties(ModelPath, trueMin, trueMax)
                {
                    ConvexHull = convexHull
                };
            }
        }
        
        public List<List<Vector3>> GetAllModelVerticies(CryEngine cryModel)
        {
            List<List<Vector3>> modelVectors = new List<List<Vector3>>();

            SCHieracrhyNode shipRoot = SOC_Explorer.ExploreModel(RawGameDB, cryModel);

            var modelChunks = Utilities.FlattenRecursive(shipRoot, n => n.Children);
            var vertChunks = modelChunks.Where(c => c?.GeometryData?.Vertexes != null);

            foreach(var chunk in vertChunks)
            {
                modelVectors.Add(chunk.GeometryData.Vertexes
                    .Select((v) => new Vector3(v.Item1, v.Item2, v.Item3))
                    .Select(chunk.TranfomPoint)
                    .ToList());
            }
            /*
            foreach (var model in cryModel.Models)
            {
                foreach(var node in model.ChunkMap.Values.OfType<ChunkNode>())
                {
                    var meshChunk = model.ChunkMap[node.ObjectNodeID] as ChunkMesh;
                    if (meshChunk == null) continue;
                    
                    List<Vector3> chunkVectors = new List<Vector3>();

                    Matrix4x4 chunkTransform = Utilities.MatrixFromTuple(node.LocalTransformTuple);

                    if (meshChunk.VerticesData != 0)
                    {
                        var tmpVertices = (ChunkDataStream)model.ChunkMap[meshChunk.VerticesData];

                        if (tmpVertices.VertCount() > 0)
                        {
                            var chunkPoints = tmpVertices.GetVerts()
                                .Select(v => Vector3.FromTuple(v))
                                .Select(v => chunkTransform.TransformVec3(v))
                                .Select(v => (Vector3)v);

                            chunkVectors.AddRange(chunkPoints);
                        }
                    }
                    if(meshChunk.VertsUVsData != 0)
                    {
                        //Remap coordinate to bounding box
                        var tmpVertsUVs = (ChunkDataStream)model.ChunkMap[meshChunk.VertsUVsData];
                        var multiplerVector = (new Vector3(meshChunk.BoundsMin) - new Vector3(meshChunk.BoundsMax)).Abs() / 2f;
                        if (multiplerVector.X < 1) { multiplerVector.X = 1; }
                        if (multiplerVector.Y < 1) { multiplerVector.Y = 1; }
                        if (multiplerVector.Z < 1) { multiplerVector.Z = 1; }
                        var boundaryBoxCenter = (new Vector3(meshChunk.BoundsMin) + new Vector3(meshChunk.BoundsMax)) / 2f;

                        if (tmpVertsUVs.VertCount() > 0)
                        {
                            var chunkPoints = tmpVertsUVs.GetVerts()
                                .Select(pos => ((pos * multiplerVector) + boundaryBoxCenter))
                                .Select(v => chunkTransform.TransformVec3(v))
                                .Select(v => (Vector3)v);

                            chunkVectors.AddRange(chunkPoints);
                        }
                    }

                    if(chunkVectors.Count > 0)
                        modelVectors.Add(chunkVectors);

                }
            }
            */

            return modelVectors;
        }

        List<Vector2> CollapseVectors(IEnumerable<Vector3> modelVectors)
        {
            var convexHull = GetConvexHull(modelVectors.Select(v => new Vector2(v.X, v.Y)).ToList());

            return convexHull;
        }

        public static double cross(Vector2 O, Vector2 A, Vector2 B)
        {
            return (A.X - O.X) * (B.Y - O.Y) - (A.Y - O.Y) * (B.X - O.X);
        }

        public static List<Vector2> GetConvexHull(List<Vector2> points)
        {
            if (points == null)
                return null;

            if (points.Count() <= 1)
                return points;

            int n = points.Count(), k = 0;
            List<Vector2> H = new List<Vector2>(new Vector2[2 * n]);

            points.Sort((a, b) =>
                 a.X == b.X ? a.Y.CompareTo(b.Y) : a.X.CompareTo(b.X));

            // Build lower hull
            for (int i = 0; i < n; ++i)
            {
                while (k >= 2 && cross(H[k - 2], H[k - 1], points[i]) <= 0)
                    k--;
                H[k++] = points[i];
            }

            // Build upper hull
            for (int i = n - 2, t = k + 1; i >= 0; i--)
            {
                while (k >= t && cross(H[k - 2], H[k - 1], points[i]) <= 0)
                    k--;
                H[k++] = points[i];
            }

            return H.Take(k - 1).ToList();
        }

        public void SaveCache()
        {
            File.WriteAllText(CacheFilePath, JsonConvert.SerializeObject(_cachedModelProperties, Formatting.Indented));
        }

        public ModelCachedProperties this[string ModelPath]
        {
            get
            {
                var lower_path = ModelPath.ToLower();

                if (!_cachedModelProperties.ContainsKey(lower_path))
                {
                    try
                    {
                        _cachedModelProperties[lower_path] = DetermineProperties(lower_path);
                    }
                    catch(FileLoadException fex)
                    {
                        if (fex.FileName.Contains("brush/designer"))
                        {
                            //NOOP: Bunch of these just exist, no real problem
                        }
                        else
                        {
                            Log.Warning(fex, $"Unable to load model: {lower_path}");
                        }
                        _cachedModelProperties[lower_path] = new ModelCachedProperties(lower_path, new Vector3(), new Vector3());
                    }
                    catch (Exception ex)
                    {
                        Log.Warning(ex, $"Error determining model properties for {lower_path}: {ex.Message}");
                        _cachedModelProperties[lower_path] = new ModelCachedProperties(lower_path, new Vector3(), new Vector3());
                    }
                }

                return _cachedModelProperties[lower_path];
            }
        }

        public DFDatabase RawGameDB { get; }
        public string CacheFilePath { get; }
    }

    public class ModelCachedProperties
    {
        public string Path { get; set; }

        public ModelCachedProperties(string Path, Vector3 LowerBounds = null, Vector3 UpperBounds = null, bool Invisible = false)
        {
            this.Invisible = Invisible;
            this.Path = Path;
            this.LowerBounds = LowerBounds ?? new Vector3();
            this.UpperBounds = UpperBounds ?? new Vector3();
        }

        public bool Invisible { get; set; }
        public Vector3 LowerBounds { get; set; }
        public Vector3 UpperBounds { get; set; }
        public Vector3 Size { get => (UpperBounds - LowerBounds).Abs(); } //Due to weird negative x issues...

        public List<Vector2> ConvexHull { get; set; }

        //Store proxy coordinates directly??
        //Store outline poly instead of min/max??

        public IEnumerable<Vector3> GetPoints()
        {
            
            if(ConvexHull != null && ConvexHull.Count >= 3)
            {
                foreach(var hullPoint in ConvexHull)
                {
                    yield return new Vector3(hullPoint.X, hullPoint.Y, LowerBounds.Z);
                }
            }
            else
            
            {
                yield return new Vector3(Math.Min(LowerBounds.X, UpperBounds.X), Math.Min(LowerBounds.Y, UpperBounds.Y), LowerBounds.Z);
                yield return new Vector3(Math.Max(LowerBounds.X, UpperBounds.X), Math.Min(LowerBounds.Y, UpperBounds.Y), LowerBounds.Z);
                yield return new Vector3(Math.Max(LowerBounds.X, UpperBounds.X), Math.Max(LowerBounds.Y, UpperBounds.Y), LowerBounds.Z);
                yield return new Vector3(Math.Min(LowerBounds.X, UpperBounds.X), Math.Max(LowerBounds.Y, UpperBounds.Y), LowerBounds.Z);
            }
        }
    }

    public class Vector2
    {
        public Vector2(double x, double y)
        {
            X = x;
            Y = y;
        }

        public double X { get; set; }
        public double Y { get; set; }
    }
}
