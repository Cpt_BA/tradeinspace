﻿using CgfConverter.CryEngineCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Xml;
using TradeInSpace.Explore.DataForge;
using TradeInSpace.Explore.SC;
using TradeInSpace.Explore.SC.Data;
using TradeInSpace.Explore.Socpak.Components;
using TradeInSpace.Explore.Subsumption;

namespace TradeInSpace.Explore.Socpak
{
    using DN = System.DoubleNumerics;

    [DebuggerDisplay("FE [{GUID}] {Label} [{Tags} - {Class}]")]
    public class FullEntity : ExposedEntity
    {
        private FullEntity(XmlElement element, SOC_Entry entity, bool DynamicComponents = true)
        {
            Element = element;
            Owner = entity;

            if (Guid.TryParse(element.GetAttribute("EntityCryGUID"), out var g))
            {
                GUID = g;
            }
            else
            {
                GUID = new Guid();//Things like particle effects don't come with a GUID by default
            }
            ClassGUID = element.HasAttribute("EntityClassGUID") ? Guid.Parse(element.GetAttribute("EntityClassGUID")) : Guid.Empty;
            
            Label = element.GetAttribute("Name");
            Class = element.GetAttribute("EntityClass");
            Layer = element.GetAttribute("Layer");
            Pos = element.GetAttribute("Pos");
            Rotate = element.GetAttribute("Rotate");
            Tags = element.SelectNodes(".//Tags/Tag/@TagId").Cast<XmlAttribute>().Select(a => a.InnerText).ToArray();
            
            Components = element.SelectNodes(".//PropertiesDataCore/*").Cast<XmlElement>()
                .Select(c => SOC_Component.FromXML(this, c, entity, DynamicComponents)).ToList();
        }

        internal void EntryFullyLoaded()
        {
            foreach (var c in Components)
                c.OnOwnerFinishedParsing();
        }

        public string Pos { get; set; }
        public string Rotate { get; set; }
        public string Scale { get; set; }
        public string Class { get; set; }
        public string Layer { get; set; }
        public Guid ClassGUID { get; set; }
        public XmlElement Element { get; set; }
        public SCEntityClassDef DatabaseEntity { get; set; }
        public List<SOC_Component> Components { get; set; }

        [JsonIgnore]
        public DN.Matrix4x4 Transform { get; set; }
        [JsonIgnore]
        public Vector3 LocalPosition { get { return Vector3.FromString(Pos); } }
        [JsonIgnore]
        public Vector4 LocalRotation { get { return Vector4.FromString(Rotate); } }
        [JsonIgnore]
        public Vector3 LocalScale { get { return Vector3.FromString(Scale, Vector3.One); } }
        [JsonIgnore]
        public Vector3 GlobalPosition { get; set; }
        [JsonIgnore]
        public Vector4 GlobalRotation { get; set; }

        public string this[string propertyName]
        {
            get
            {
                return Element.GetAttribute(propertyName);
            }
        }

        private DFDatabase GameDB
        {
            get => Owner?.SourceFile?.LoadedFrom;
        }

        public T GetComponent<T>() where T : SOC_Component
        {
            return Components.OfType<T>().FirstOrDefault();
        }

        public static new FullEntity FromXML(XmlElement element, SOC_Entry entity, bool DynamicComponents = true)
        {
            return new FullEntity(element, entity, DynamicComponents);
        }

        //Bit of a hack to improve performance on lookups
        private static Dictionary<(Guid, Guid), IEnumerable<string>> _MissionLocationCache = null;
        private static string[] NameSearchXPath = new[]
        {
            ".//Elem[@id='0130de18-97e2-4d4c-9bb1-3e7ef5fd0490']/@string",
            ".//Elem[@tag='0130de18-97e2-4d4c-9bb1-3e7ef5fd0490']/@string",
            ".//EntityComponentGoToPoint/@pointName",
            ".//EntityComponentObjectMetadata/@name",
            ".//SCTransitDestination/@Name"
        };
        public string TryGetDisplayName()
        {
            string displayName = null;
            //Attempt to resolve display name from entity

            if (string.IsNullOrEmpty(displayName))
            {
                var starmapID = Element.SelectSingleNode(".//EntityComponentObjectMetadata/metadata/Elem/@starmapRecord") as XmlAttribute;
                if (starmapID != null)
                {
                    if (Guid.TryParse(starmapID.Value, out var starmapGuid))
                    {
                        var starmapRecord = GameDB.GetEntry<StarmapObject>(starmapGuid);
                        displayName = GameDB.Localize(starmapRecord?.Name);
                    }
                }
                if (!string.IsNullOrEmpty(displayName))
                {

                }
            }

            foreach(var searchXPath in NameSearchXPath)
            {
                if (string.IsNullOrWhiteSpace(displayName))
                {
                    var searchNode = Element.SelectSingleNode(searchXPath) as XmlAttribute;
                    displayName = GameDB.Localize(searchNode?.Value);
                }
            }

            if (string.IsNullOrEmpty(displayName) && Class == "SCShop")
            {
                var replacements = new[]
                {
                    "SCShop",
                    "Small",
                    "OrisonSP",
                    "NewBabbage",
                };

                var cleanName = this.Label.Replace("_", " ");
                foreach (var replace in replacements)
                    cleanName = cleanName.Replace(replace, "");

                displayName = cleanName.Trim();
            }

            if (string.IsNullOrEmpty(displayName))
            {
                //Handles if this entity happens to be the self-entity of the parent.
                var owner = Owner?.OwnEntity == this ? Owner.Parent : Owner;

                if(_MissionLocationCache == null)
                {
                    _MissionLocationCache = GameDB.GetEntries<MissionLocation>().GroupBy(
                        ml => (ml.LocationParams.FinalAAGUID, ml.LocationParams.FinalContainerGUID))
                        .ToDictionary(
                            kvp => kvp.Key,
                            kvp => kvp.Select(ml => (ml.Element.SelectSingleNode(".//MissionStringVariant[@tag='0130de18-97e2-4d4c-9bb1-3e7ef5fd0490']/@string") as XmlAttribute)?.Value));
                }

                if(_MissionLocationCache.ContainsKey((GUID, owner.GUID)))
                {
                    displayName = GameDB.Localize(_MissionLocationCache[(GUID, owner.GUID)].FirstOrDefault());
                }
            }

            return displayName;
        }

        private string _modelPath;
        public string ModelPath
        {
            get
            {
                if (_modelPath == null)
                {
                    var renderComponent = Components.SingleOrDefault(c => c.ComponentType == "SGeometryResourceParams");
                    if (renderComponent != null)
                    {
                        _modelPath = renderComponent.FromElement.SelectSingleNode("./Geometry/Geometry/Geometry/@path")?.Value;
                    }

                    if(_modelPath == null && this.DatabaseEntity != null)
                    {
                        //If we still don't have it, check the the entity class for the property
                        _modelPath = DatabaseEntity.Element.SelectSingleNode("./Components/SGeometryResourceParams/Geometry/Geometry/Geometry/@path")?.Value;
                    }
                }
                    
                return _modelPath;
            }
        }

        public Model LoadModel(DFDatabase rawGameData)
        {
            var geometryPath = this.ModelPath;

            if (geometryPath == null) return null;

            var data = rawGameData.ReadPackedFileBinary(geometryPath);

            return Model.FromBuffer(data, geometryPath);
        }
    }
}
