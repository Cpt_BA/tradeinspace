﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace TradeInSpace.Explore.Socpak.Components
{
    [SOC_Component("EntityComponentGreenZone")]
    public class SOC_GreenZone : SOC_Component
    {
        public SOC_GreenZone(FullEntity entity, XmlElement fromElement, SOC_Entry parent) : base(entity, fromElement, parent)
        {
        }
    }
}
