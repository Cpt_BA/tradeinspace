﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace TradeInSpace.Explore.Socpak.Components
{
    [SOC_Component("ProceduralEntity")]
    public class SOC_ProceduralEntity : SOC_Component
    {
        public float? AtmosphereHeight;
        public float? PlanetGravity;

        public SOC_ProceduralEntity(FullEntity entity, XmlElement fromElement, SOC_Entry parent)
            : base(entity, fromElement, parent)
        {
            if(float.TryParse((fromElement.SelectSingleNode("./Atmosphere/General/@AtmoHeight") as XmlAttribute)?.Value, out var atmoHeight))
            {
                AtmosphereHeight = atmoHeight;
            }
            
            if(float.TryParse((fromElement.SelectSingleNode("./Planet/PlanetGeometry/PhysProperties/@Gravity") as XmlAttribute)?.Value, out var planetGrav))
            {
                PlanetGravity = planetGrav;
            }
        }
    }
}
