﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using TradeInSpace.Explore.DataForge;
using TradeInSpace.Explore.SC.Data;
using TradeInSpace.Explore.Socpak;
using TradeInSpace.Explore.Socpak.Components;

namespace TradeInSpace.Explore.Socpak.Components
{
    [SOC_Component("HarvestableProviderComponent")]
    public class SOC_HarvestableProvider : SOC_Component
    {
        public HarvestablePresetProvider HarvestingPreset;

        public SOC_HarvestableProvider(FullEntity entity, XmlElement fromElement, SOC_Entry parent)
            : base(entity, fromElement, parent)
        {
            var presetGUID = Guid.Parse(fromElement.GetAttribute("preset"));
            HarvestingPreset = GameDatabase.GetEntry<HarvestablePresetProvider>(presetGUID);
        }
    }
}
