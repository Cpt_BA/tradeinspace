﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Xml;
using TradeInSpace.Explore.DataForge;
using TradeInSpace.Explore.SC;

namespace TradeInSpace.Explore.Socpak.Components
{

    public class SOC_Component
    {
        public SOC_Component(FullEntity entity, XmlElement fromElement, SOC_Entry parent)
        {
            Parent = parent;
            Entity = entity;
            FromElement = fromElement;
            ComponentType = FromElement.GetAttribute("__type");
        }

        public SOC_Entry Parent { get; set; }
        public string ComponentType { get; set; }
        public FullEntity Entity { get; }
        public XmlElement FromElement { get; set; }
        public string this[string PropertyName]
        {
            get
            {
                return FromElement.GetAttribute(PropertyName);
            }
        }

        internal virtual void OnOwnerFinishedParsing() { }

        internal DFDatabase GameDatabase { get { return Parent.Archive.LoadedFrom; } }

        public static SOC_Component FromXML(FullEntity onEntity, XmlElement element, SOC_Entry socFile, bool DynamicComponents = true)
        {
            if(!DynamicComponents)
                return new SOC_Component(onEntity, element, socFile);

            var availableTypes = SOC_ComponentAttribute.FindAllLoaded();
            var matchingComponent = availableTypes.Where(c => c.Component.EntityType == element.LocalName);

            if (matchingComponent.Count() > 1)
            {
                throw new Exception("Multiple components defined under same name");
            }
            else if (matchingComponent.Count() == 1)
            {
                var compInstance = Activator.CreateInstance(matchingComponent.First().Class, new object[] { onEntity, element, socFile }) as SOC_Component;
                return compInstance;
            }
            else
            {
                return new SOC_Component(onEntity, element, socFile);
            }
        }
    }

    [AttributeUsage(AttributeTargets.Class)]
    public class SOC_ComponentAttribute : Attribute
    {
        public string EntityType { get; set; }

        private static List<(Type Class, SOC_ComponentAttribute Component)> LoadedComponents;

        public SOC_ComponentAttribute(string polymorphicType)
        {
            EntityType = polymorphicType;
        }

        public static IEnumerable<(Type Class, SOC_ComponentAttribute Component)> FindAllLoaded()
        {
            if (LoadedComponents == null)
            {
                LoadedComponents = new List<(Type Class, SOC_ComponentAttribute Component)>();
                var domainTypes = AppDomain.CurrentDomain.GetAssemblies().SelectMany(a => a.GetTypes());

                foreach (Type type in domainTypes)
                {
                    var matchingAttr = type.GetCustomAttribute(typeof(SOC_ComponentAttribute), true) as SOC_ComponentAttribute;
                    if (matchingAttr != null)
                    {
                        LoadedComponents.Add((type, matchingAttr));
                    }
                }
            }

            return LoadedComponents;
        }
    }
}
