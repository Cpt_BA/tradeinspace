﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using TradeInSpace.Explore.DataForge;
using TradeInSpace.Explore.SC.Data;

namespace TradeInSpace.Explore.Socpak.Components
{
    [SOC_Component("EntityComponentShoppingProvider")]
    public class SOC_ShoppingProvider : SOC_Component
    {
        public Guid BrandGUID { get; private set; }
        public Brand Brand { get; private set; }
        public string ShopType { get; private set; }

        public SOC_ShoppingProvider(FullEntity onEntity, XmlElement fromElement, SOC_Entry parent) : base(onEntity, fromElement, parent)
        {
            BrandGUID = Guid.Parse(fromElement.GetAttribute("brand"));
            Brand = GameDatabase.GetEntry<Brand>(BrandGUID);
            ShopType = fromElement.GetAttribute("shopType");
        }
    }
}
