﻿using Newtonsoft.Json;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using TradeInSpace.Explore.SC;
using TradeInSpace.Explore.SC.Data;
using static TradeInSpace.Models.Enums;

namespace TradeInSpace.Explore.Socpak.Components
{
    [SOC_Component("EntityComponentShop")]
    public class SOC_ComponentShop : SOC_Component
    {
        public static List<string> UnrecognizedShopTypes = new List<string>();

        string InventoryPath { get; set; }

        public OfflineInventory Inventory { get; private set; }

        public List<Guid> KioskGUIDs { get; private set; }
        public List<Guid> RackGUIDs { get; private set; }
        public List<Guid> LandingAreaGUIDs { get; private set; }

        public List<FullEntity> Kiosks;
        public List<FullEntity> Racks;
        public List<FullEntity> LandingAreas;

        //public ShopType ShopType { get; private set; }

        public SOC_ComponentShop(FullEntity onEntity, XmlElement fromElement, SOC_Entry parent) : base(onEntity, fromElement, parent)
        {
            //Weirdly stored in root, and not as properties of this element.
            var shopData = onEntity.Element["EntityComponentShop"];

            if (shopData != null)
            {
                KioskGUIDs = shopData.SelectNodes("ShopKioskGUIDs/ShopKioskGUID/@entityCryGUID")
                    .OfType<XmlAttribute>().Select(a => Guid.Parse(a.Value)).ToList();

                RackGUIDs = shopData.SelectNodes("ShopRackGUIDs/ShopRackGUID/@entityCryGUID")
                    .OfType<XmlAttribute>().Select(a => Guid.Parse(a.Value)).ToList();

                LandingAreaGUIDs = shopData.SelectNodes("LandingAreaNodeGUIDs/LandingAreaNodeGUID/@entityCryGUID")
                    .OfType<XmlAttribute>().Select(a => Guid.Parse(a.Value)).ToList();
            }
            else
            {
                KioskGUIDs = new List<Guid>();
                RackGUIDs = new List<Guid>();
                LandingAreaGUIDs = new List<Guid>();
            }

            InventoryPath = onEntity.Element.SelectSingleNode("./PropertiesDataCore/EntityComponentShop/@offlineInventoryJSON").Value;
        }

        internal override void OnOwnerFinishedParsing()
        {
            base.OnOwnerFinishedParsing();

            if (!string.IsNullOrEmpty(this.InventoryPath))
            {
                var inventoryJson = GameDatabase.ReadPackedFile(this.InventoryPath);

                if (string.IsNullOrEmpty(inventoryJson))
                {
                    Log.Warning($"Shop {Entity.Label}/{Entity.GUID} has empty offline inventory: {InventoryPath}");
                }
                else
                {
                    this.Inventory = JsonConvert.DeserializeObject<OfflineInventory>(inventoryJson);

                    if (this.Inventory?.Collection?.Inventory != null)
                    {
                        foreach (var entry in this.Inventory?.Collection?.Inventory)
                        {
                            entry.Item = GameDatabase.GetEntity(entry.GUID);
                        }
                    }
                }
            }

            Kiosks = KioskGUIDs.Select(kg => Entity.Owner.GetEntity(kg)).ToList();
            Racks = RackGUIDs.Select(rg => Entity.Owner.GetEntity(rg)).ToList();
            LandingAreas = LandingAreaGUIDs.Select(lg => Entity.Owner.GetEntity(lg)).ToList();

            var kioskComponents = Kiosks.Select(k => k.GetComponent<SOC_ShoppingProvider>());
            /*
            //Stil don't want to be using this =/
            switch (Entity.Label)
            {
                case "SCShop_lt_a_platinumbay_small_a":
                    ShopType = ShopType.PlatinumBay;
                    break;
                case "SCShop_Teachs_Ship_Shop":
                    ShopType = ShopType.Teachs;
                    break;
                case "SCShop-003":
                    ShopType = ShopType.VantageRentals;
                    break;
                case "SCShop_TravelerRentals_Lorville":
                    ShopType = ShopType.TravelerRentals;
                    break;
                case var s when s.StartsWith("SCShop_Admin_lt_base_d"):
                case var t when t.StartsWith("Shop_ht_delta_indfarm_l_multi"):
                case var u when u.StartsWith("Shop_ht_delta_indmine_l_multi"):
                case "SCShop_AdminOffice_PortOlisar":
                case "Shop_ht_delta_shubin_m_store":
                case "SCShop_Lorville_admin_commodities":
                case "Shop_ht_delta_rayari_m_store":
                case var a when a.StartsWith("SCShop_Admin_"):
                case var ao when ao.StartsWith("SCShop_AdminOffice_"):
                    ShopType = ShopType.AdminTerminal;
                    break;
                case "SCShop_fence_kiosk_small":
                    ShopType = ShopType.FenceTerminal;
                    break;

                case "SCShop_Lorville_admin_refinery":
                case var ke when ke.EndsWith("_MiningKiosk"):
                case var m when m.StartsWith("SCShop_Mining"):
                case var mk when mk.StartsWith("SCShop_MiningKiosk"):
                    ShopType = ShopType.AdminRefinery;
                    break;
                case "SCShop-001": //??What is this
                    ShopType = ShopType.Other;
                    break;
                case "SCShop_ShipWeapons_UtilStation":
                    ShopType = ShopType.CongreveWeapons;
                    break;
                case var s when s.StartsWith("ShopEntity_TammanyAndSons"):
                    ShopType = ShopType.TammanyAndSons;
                    break;
                case var s when s.StartsWith("SCShop_lt_a_casaba_small_base_a"):
                case var t when t.StartsWith("SCShop_Casaba"):
                case "SCShop_Entity_CasabaOutlets_Area18":
                    if (Entity.Layer.StartsWith("util_armor_gen_sml"))
                    {
                        ShopType = ShopType.BulwarkArmor;
                    }
                    else
                    {
                        ShopType = ShopType.Casaba;
                    }
                    break;
                case var s when s.StartsWith("SCShop_Entity_Garrity"):
                    ShopType = ShopType.GarrityDefense;
                    break;
                case var s when s.StartsWith("SCShop_Entity_Dumpers_Depot"):
                case var d when d.StartsWith("SCShop_DumpersDepot_"):
                case "SCShop_ShipComponents_GrimHex":
                    ShopType = ShopType.DumpersDepot;
                    break;
                case var s when s.StartsWith("SCShop_Entity_ConscientiousObjects"):
                    ShopType = ShopType.ConscientiousObjects;
                    break;
                case var s when s.StartsWith("SCShop_ShipWeapon_HDShowcase"):
                    ShopType = ShopType.HDShowcase;
                    break;
                case var s when s.StartsWith("SCShop_NewDeal"):
                    ShopType = ShopType.NewDeal;
                    break;
                case var s when s.StartsWith("SCShop_LiveFire"):
                    ShopType = ShopType.LiveFireWeapons;
                    break;
                case var s when s.StartsWith("SCShop_Skutters"):
                    ShopType = ShopType.Skutters;
                    break;
                case var s when s.StartsWith("SCShop_Centermass"):
                    ShopType = ShopType.Centermass;
                    break;
                case var s when s.StartsWith("SCShop_AstroArmada"):
                    ShopType = ShopType.AstroArmada;
                    break;
                case var s when s.StartsWith("SCShop_Entity_CubbyBlast"):
                    ShopType = ShopType.CubbyBlast;
                    break;
                case "TDD_SCShop-001":
                case var s when s.StartsWith("SCShop_CommEx_TDD"):
                    ShopType = ShopType.TDD;
                    break;
                case var s when s.StartsWith("SCShop_CommEx_Transfers"):
                    ShopType = ShopType.CommExTransfers;
                    break;
                case var s when s.StartsWith("SCShop_Aparelli"):
                    ShopType = ShopType.Aparelli;
                    break;
                case var s when s.StartsWith("SCShop_ShubinInterstellar"):
                    ShopType = ShopType.ShubinInterstellar;
                    break;
                case var s when s.StartsWith("SCShop_OmegaPro"):
                    ShopType = ShopType.OmegaPro;
                    break;
                case var s when s.StartsWith("SCShop_RegalLuxuryRentals"):
                    ShopType = ShopType.RegalLuxury;
                    break;
                case var c when c.StartsWith("SCShop_Cordrys"):
                    ShopType = ShopType.Cordrys;
                    break;
                case var c when c.StartsWith("SCShop_KCTrending"):
                    ShopType = ShopType.KCTrending;
                    break;
                case var c when c.StartsWith("SCShop_Technotic"):
                    ShopType = ShopType.Technotic;
                    break;
                case "SCShop_Bazaar_GadgetStand_Levski":
                case "SCShop_Entity_BazaarStand_Clothing_Levski":
                    ShopType = ShopType.GrandBarter;
                    break;
                case var s when s.StartsWith("SCShop_Whammers"):
                    ShopType = ShopType.Whammers;
                    break;
                case var s when s.StartsWith("SCShop_Ellroys"):
                    ShopType = ShopType.Ellroys;
                    break;
                case var s when s.StartsWith("SCShop_NoodleBar"):
                    ShopType = ShopType.NoodleBar;
                    break;
                case var s when s.StartsWith("SCShop_JuiceBar"):
                    ShopType = ShopType.JuiceBar;
                    break;
                case var b when b.StartsWith("SCShop_Bar"):
                case var m when m.StartsWith("SCShop_MVBar"):
                case var k when k.StartsWith("SCShop_Market_Bar"):
                    ShopType = ShopType.Bar;
                    break;
                case var s when s.StartsWith("SCShop_CoffeeToGo"):
                    ShopType = ShopType.CoffeToGo;
                    break;
                case var h when h.StartsWith("SCShop_hotdog_cart"):
                    ShopType = ShopType.HotdogCart;
                    break;
                case var b when b.StartsWith("SCShop_HotDogBar"):
                    ShopType = ShopType.HotdogBar;
                    break;
                case var p when p.StartsWith("SCShop_PizzaBar"):
                    ShopType = ShopType.PizzaBar;
                    break;
                case var c when c.StartsWith("SCShop_burrito_cart"):
                    ShopType = ShopType.BurritoCart;
                    break;
                case var b when b.StartsWith("SCShop_BurritoBar"):
                    ShopType = ShopType.BurritoBar;
                    break;
                case var b when b.StartsWith("SCShop_GarciaGreens"):
                    ShopType = ShopType.GarciaGreens;
                    break;
                case var b when b.StartsWith("SCShop_Twyns"):
                    ShopType = ShopType.Twyns;
                    break;
                case var b when b.StartsWith("SCShop_GLoc"):
                    ShopType = ShopType.GLoc;
                    break;

                case var b when b.StartsWith("SCShop_Wallys"):
                    ShopType = ShopType.Wallys;
                    break;
                case var b when b.StartsWith("SCShop_FactoryLine"):
                    ShopType = ShopType.FactoryLine;
                    break;
                case var b when b.StartsWith("SCShop_Novelty_Kiosk"):
                    ShopType = ShopType.NoveltyKiosk;
                    break;
                case var b when b.StartsWith("SCShop_RacingBar"):
                    ShopType = ShopType.RacingBar;
                    break;
                case var b when b.StartsWith("SCShop_Old38"):
                    ShopType = ShopType.Old38;
                    break;
                case var b when b.StartsWith("SCShop_CafeMusain"):
                    ShopType = ShopType.CafeMusain;
                    break;


                case "SCShop_Cargo_Office":
                case "SCShop_Cargo_Office_Food":
                    ShopType = ShopType.CargoOffice;
                    break;
                case "SCShop_Cargo_Office_Rentals":
                    ShopType = ShopType.CargoOfficeRentals;
                    break;

                case "SCShop_CubbyBlast_Food_Area18":
                case "SCShop_TammanySons_Food_Lorville":
                case "SCShop-ExpoHall_Lobby":
                case "SCShop_Garage_Services":
                case "SCShop-CBD_Main_Hall":
                case "SCShop_Levski_Landing_Services":
                case "SCShopCommissary":
                case var s when s.StartsWith("SCShop_LandingServices"):
                case var t when t.StartsWith("LandingServices"):
                case var f when f.StartsWith("SCShop_FleetWeek"):
                    ShopType = ShopType.Ignore;
                    break;
                default:
                    //throw new Exception($"Unkown shop type {Entity.Label}");
                    UnrecognizedShopTypes.Add(Entity.Label);
                    ShopType = ShopType.Ignore;
                    break;
            }
            */
        }
    }

    public class OfflineInventory
    {
        public string ShopID { get; set; }
        public OfflineInventoryCollection Collection { get; set; }
    }

    public class OfflineInventoryCollection
    {
        public InventoryOffering[] Inventory;
    }

    public class InventoryOffering
    {
        public Guid GUID { get { return Guid.Parse(ID.ID.Single()); } }

        public IDEntry ID { get; set; }
        public float BuyPrice { get; set; }
        public float SellPrice { get; set; }
        public float CurrentInventory { get; set; }
        public float MaxInventory { get; set; }
        public SCEntityClassDef Item { get; set; }
    }

    public class IDEntry
    {
        public string[] ID { get; set; }
    }
}
