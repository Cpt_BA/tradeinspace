﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using static TradeInSpace.Models.Enums;
using System.Xml;
using TradeInSpace.Explore.SC.Data;
using TradeInSpace.Explore.DataForge;

namespace TradeInSpace.Explore.Socpak.Components
{
    [SOC_Component("LandingArea")]
    public class LandingAreaComponent : SOC_Component
    {

        public LandingAreaComponent(FullEntity onEntity, XmlElement fromElement, SOC_Entry parent) : base(onEntity, fromElement, parent)
        {
            var availableSizes = GameDatabase.GetEntries<LandingPadSize>();

            string name = GameDatabase.Localize(fromElement.GetAttribute("HUDDisplayName"));
            string serviceClass = fromElement.GetAttribute("servicesClass");
            string usedBy = fromElement.GetAttribute("canBeUsedBy");

            bool enabled = fromElement.GetAttribute("enabled") == "1";
            bool shipCustomization = fromElement.HasAttribute("allowPortModification") ? (fromElement.GetAttribute("allowPortModification") == "1") : false;
            bool repair = fromElement.HasAttribute("repairAvailable") ? (fromElement.GetAttribute("repairAvailable") == "1") : false;
            bool allowGround = fromElement.GetAttribute("allowGroundVehicles") == "1";
            bool allowShip = fromElement.GetAttribute("allowSpaceships") == "1";
            bool autoRegister = fromElement.HasAttribute("autoRegisterWithATC") ? fromElement.GetAttribute("autoRegisterWithATC") == "1" : false;

            var padsize = new Vector3(
                float.Parse(fromElement["dimensions"].GetAttribute("x")),
                float.Parse(fromElement["dimensions"].GetAttribute("y")),
                float.Parse(fromElement["dimensions"].GetAttribute("z"))
            );

            //Find largest size-preset with all dimensions smaller than the testing pad
            var matchingShipSize = availableSizes
                .Select(p => p.ShipSize)
                .Where(p => p != null && p.X <= padsize.X && p.Y <= padsize.Y && p.Z <= padsize.Z && p.Type == LandingPadType.Ship)
                .OrderByDescending(p => p.X * p.Y * p.Z)
                .FirstOrDefault();
            var matchingGVSize = availableSizes
                .Select(p => p.ShipSize)
                .Where(p => p != null && p.X <= padsize.X && p.Y <= padsize.Y && p.Z <= padsize.Z && p.Type == LandingPadType.GroundVehicle)
                .OrderByDescending(p => p.X * p.Y * p.Z)
                .FirstOrDefault();

            var test = availableSizes.ToList();

            Enabled = enabled;
            Name = name;
            ServicesClass = serviceClass;
            ShipFitting = shipCustomization;
            ShipRepair = repair;
            AutoRegister = autoRegister;
            UsedBy = usedBy;
            AllowGround = allowGround;
            AllowShip = allowShip;
            GroundVehicleSize = matchingGVSize?.Size;
            ShipSize = matchingShipSize?.Size;
            PadSize = padsize;
        }

        public string Name { get; set; }
        public string ServicesClass { get; set; }
        public string UsedBy { get; set; }
        public bool Enabled { get; set; }
        public bool ShipFitting { get; set; }
        public bool ShipRepair { get; set; }
        public bool AutoRegister { get; set; }

        public bool AllowGround { get; set; }
        public bool AllowShip { get; set; }

        public Vector3 PadSize { get; set; }

        public PadSize? ShipSize { get; set; }
        public PadSize? GroundVehicleSize { get; set; }
    }
}
