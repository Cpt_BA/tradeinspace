﻿using CgfConverter;
using CgfConverter.CryEngineCore;
using Serilog;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using TradeInSpace.Explore.DataForge;
using TradeInSpace.Explore.SC;
using TradeInSpace.Explore.SC.Components;
using TradeInSpace.Explore.SC.Data;
using TradeInSpace.Explore.SC.Loadout;
using TradeInSpace.Explore.Socpak;
using TradeInSpace.Explore.Subsumption;
using TradeInSpace.Parse;

namespace TradeInSpace.Explore
{
    using DN = System.DoubleNumerics;

    public class SOC_Explorer
    {
        DFDatabase GameDB;
        static string[] IgnoreEntityClasses = new string[]
        {
            "Light", "AudioTriggerSpot", "LightBox", "FogVolume", "LedgeObject", "VibrationAudioPoint", "LightGroup"
        };

        protected SOC_Explorer(DFDatabase gameDB)
        {
            GameDB = gameDB;
        }

        public static SCHieracrhyNode ExploreShip(DFDatabase GameDB, Vehicle shipComponent, SearchConfig Config = null)
        {
            Config = Config ?? new SearchConfig();
            var rootEntry = new SCHieracrhyNode()
            {
                NodeType = SCHieracrhyNodeType.Root,
                Name = shipComponent.ParentEntity.InstanceName,
                ID = shipComponent.ParentEntity.ID,
                LocalPosition = Vector3.Zero,
                LocalRotation = Vector4.Identity,
                LocalScale = Vector3.One
            };

            var modelPath = shipComponent.ParentEntity.GetComponent<GeometryComponent>()?.ModelPath;

            if (modelPath == null) return rootEntry;

            SCHieracrhyNode model_node;

            if(Config.LoadModelStructure)
            {
                model_node = ExploreModel(GameDB, modelPath, Config, rootEntry);
            }
            else
            {
                model_node = Build_Dummy(Path.GetFileNameWithoutExtension(modelPath), SCHieracrhyNodeType.Model, rootEntry);
            }

            if (Config.IncludeModelReferences)
            {
                model_node.ModelPath = modelPath;
            }

            Config.AddModel(model_node.ModelPath);

            var subContainerExplorer = new SOC_Explorer(GameDB);
            foreach (var innerContainer in shipComponent.ContainerEntries)
            {
                var subArchiveRoot = SOC_Archive.LoadFromGameDBSocPak(GameDB, innerContainer.ArchivePath);
                if (subArchiveRoot != null)
                {
                    var subArchiveNode = subContainerExplorer.Inner_ExploreNode(subArchiveRoot.RootEntry, Config, rootEntry);
                    subArchiveNode.LocalPosition = innerContainer.Offset;
                    subArchiveNode.LocalRotation = innerContainer.Rotation;
                    subArchiveNode.LocalScale = Vector3.One;
                }
                else
                {
                    Log.Warning($"{rootEntry.Name} Attempted to load missing container: {innerContainer.ArchivePath}");
                }
            }

            var shipLoadout = LoadoutTree.CreateFromVehicle(shipComponent, true);
            //model_node has a pseudo-root, so get first child to skip that
            if (shipLoadout != null)
            {
                ApplyLoadout(GameDB, shipLoadout.Root, model_node.Children.First(), Config);
            }

            return rootEntry;
        }

        static Dictionary<string, CryEngine> _ModelCache = new Dictionary<string, CryEngine>();

        private static CryEngine LoadModel(DFDatabase GameDB, string ModelPath)
        {
            CryEngine model = null;

            if (_ModelCache.ContainsKey(ModelPath) && _ModelCache[ModelPath] != null)
                model = _ModelCache[ModelPath];
            else
            {
                model = GameDB.ReadPackedCGAFile(ModelPath);
                try
                {
                    model.ProcessCryengineFiles();
                }
                catch { model = null; }
            }
            _ModelCache[ModelPath] = model;

            return model;
        }

        public static SCHieracrhyNode ExploreEquipment(DFDatabase GameDB, SCEntityClassDef EquipmentClass, SearchConfig Config = null, SCHieracrhyNode Parent = null)
        {
            Config = Config ?? new SearchConfig();

            var attachGeom = EquipmentClass.GetComponent<GeometryComponent>();
            var attachModelPath = attachGeom?.ModelPath;
            var attachModelScale = attachGeom?.ScaleMultiplier ?? 1;

            var equipment_root = Build_Dummy(EquipmentClass.InstanceName, SCHieracrhyNodeType.Equipment, Parent: Parent);
            equipment_root.LocalScale = new Vector3(attachModelScale, attachModelScale, attachModelScale);

            Config.AddModel(attachModelPath);

            if (!string.IsNullOrEmpty(attachModelPath))
            {
                if (Config.LoadModelStructure)
                {
                    ExploreModel(GameDB, attachModelPath, Config, equipment_root);
                }
                else
                {
                    if (Config.IncludeModelReferences)
                        equipment_root.ModelPath = attachModelPath;
                }
            }
            else
            {
                Build_Dummy("Empty", Parent: equipment_root);
            }

            var equipmentLoadout = LoadoutTree.CreateFromEntity(EquipmentClass, true);
            if (equipmentLoadout != null)
            {
                ApplyLoadout(GameDB, equipmentLoadout.Root, equipment_root, Config);
            }

            return equipment_root;
        }

        public static SCHieracrhyNode ExploreModel(DFDatabase GameDB, string ModelPath, SearchConfig Config = null, SCHieracrhyNode Parent = null)
        {
            CryEngine model = LoadModel(GameDB, ModelPath);

            if(model == null)
            {
                return Build_Dummy("Missing_" + Path.GetFileName(ModelPath.Replace('\\', '/')), SCHieracrhyNodeType.Dummy, Parent);
            }

            return ExploreModel(GameDB, model, Config, Parent);
        }

        public static SCHieracrhyNode ExploreModel(DFDatabase GameDB, CryEngine Model, SearchConfig Config = null, SCHieracrhyNode Parent = null)
        {
            Config = Config ?? new SearchConfig();

            var rootEntry = Build_Dummy(Path.GetFileNameWithoutExtension(Model.InputFile), SCHieracrhyNodeType.Model, Parent);

            Config.AddModel(Model.InputFile);
            
            //TODO: Duplicated in 3 places, should consolidate
            var ignoreTextureIDs = new List<int>();
            for(int i = 0; i < Model.Materials.Count; i++)
                if(Model.Materials[i].Shader?.Equals("nodraw", StringComparison.CurrentCultureIgnoreCase) ?? true)
                    ignoreTextureIDs.Add(i);

            //An object can have multiple roots, so we can't rely on just RootNode for enumeration
            var roots = Model.Models[0].NodeMap.Values.Where(a => a.ParentNodeID == ~0);
            foreach (var rootChunk in roots)
            {
                ExploreModelChunk(rootChunk, Model, Config, rootEntry, IgnoreTextureID: ignoreTextureIDs);
            }

            return rootEntry;
        }

        private static SCHieracrhyNode ExploreModelForSingleChunk(DFDatabase GameDB, string ModelPath, string ChunkName,
            StringComparison stringComparer = StringComparison.CurrentCulture, SearchConfig Config = null, SCHieracrhyNode Parent = null)
        {
            CryEngine model = LoadModel(GameDB, ModelPath);

            if (model == null)
            {
                return Build_Dummy("Missing_" + Path.GetFileName(ModelPath.Replace('\\', '/')), SCHieracrhyNodeType.Dummy, Parent);
            }

            return ExploreModelForSingleChunk(GameDB, model, ChunkName, stringComparer, Config, Parent);
        }

        private static SCHieracrhyNode ExploreModelForSingleChunk(DFDatabase GameDB, CryEngine Model, string ChunkName, 
            StringComparison stringComparer = StringComparison.CurrentCulture, SearchConfig Config = null, SCHieracrhyNode Parent = null)
        {
            IEnumerable<ChunkNode> _NodeSearch(ChunkNode _Chunk)
            {
                if(string.Equals(_Chunk.Name, ChunkName, stringComparer))
                {
                    yield return _Chunk;
                }
                
                if(_Chunk.AllChildNodes != null)
                    foreach(var child in _Chunk.AllChildNodes)
                        foreach (var child_match in _NodeSearch(child))
                            yield return child_match;
            }

            //TODO: Duplicated in 3 places, should consolidate
            var ignoreTextureIDs = new List<int>();
            for (int i = 0; i < Model.Materials.Count; i++)
                if (Model.Materials[i].Shader?.Equals("nodraw", StringComparison.CurrentCultureIgnoreCase) ?? true)
                    ignoreTextureIDs.Add(i);

            var roots = Model.Models[0].NodeMap.Values.Where(a => a.ParentNodeID == ~0);
            foreach (var rootChunk in roots)
            {
                foreach(var match in _NodeSearch(rootChunk))
                {
                    //NOTE: This ends all enumerations immediately. only the first match will trigger
                    //Also don't do a recursive export when in this mode, since this is used to extract individual chunks (IE: Mule)
                    var chunk_node = ExploreModelChunk(match, Model, Config, Parent, Recursive: false, ignoreTextureIDs);
                    chunk_node.LocalTransform = DN.Matrix4x4.Identity;
                    return chunk_node;
                }
            }

            return null;
        }

        private static SCHieracrhyNode ExploreModelChunk(ChunkNode ModelChunk, CryEngine CryModel, SearchConfig Config, SCHieracrhyNode Parent, 
            bool Recursive = true, List<int> IgnoreTextureID = null)
        {
            SCHieracrhyNode _ProcessGeometryNode(ChunkNode _Chunk, SCHieracrhyNode _Parent)
            {
                SCHieracrhyNode node = Build_From_Node(_Chunk, SCHieracrhyNodeType.Chunk, _Parent);

                node.AdditionalProperties.Add("TIS_RawProps", _Chunk.Properties);

                if (Config.LoadModelGeometry)
                {
                    var model_data = CryModelGeometryData.ReadFromChunk(_Chunk, IgnoreTextureID);

                    if (model_data != null)
                    {
                        //node.NodeType = SCHieracrhyNodeType.Chunk;
                        node.GeometryData = model_data;
                    }
                }

                return node;
            }

            SCHieracrhyNode _ProcessHelperNode(ChunkNode _Chunk, SCHieracrhyNode _Parent)
            {
                var node = Build_From_Node(_Chunk, SCHieracrhyNodeType.Chunk, _Parent);

                node.LocalScale = Vector3.One; //Helper nodes should not alter scale.

                return node;
            }

            SCHieracrhyNode chunk_node = null;

            if (CryModel.Models.Count > 1)
            {
                var geoNode = CryModel.Models[1].NodeMap.Values.FirstOrDefault(n => n.Name == ModelChunk.Name);

                if (geoNode != null)
                {
                    switch (geoNode.ObjectChunk.ChunkType)
                    {
                        case ChunkType.Mesh:
                            chunk_node = _ProcessGeometryNode(geoNode, Parent);
                            break;
                        default:
                            break;
                    }
                }
            }


            if(chunk_node == null)
                chunk_node = _ProcessHelperNode(ModelChunk, Parent);

            //TODO: Better place for this
            var model_chunk_name = $"{Path.GetFileNameWithoutExtension(CryModel.InputFile)}_{ModelChunk.Name}";
            chunk_node.ModelID = model_chunk_name.GetHashCode();

            if (Recursive)
            {
                if (ModelChunk.AllChildNodes != null)
                {
                    foreach (var childChunk in ModelChunk.AllChildNodes)
                    {
                        ExploreModelChunk(childChunk, CryModel, Config, chunk_node, Recursive, IgnoreTextureID);
                    }
                }
            }

            return chunk_node;
        }

        static void ApplyLoadout(DFDatabase GameDB, LoadoutEntry slot, SCHieracrhyNode hardpointNode, SearchConfig Config, bool Recursive = true)
        {
            var attachedComponent = slot.Port?.AttachedComponent;

            if (attachedComponent != null && Config.ApplyShipLoadout)
            {
                ExploreEquipment(GameDB, attachedComponent, Config, hardpointNode);
            }

            foreach (var slot_geometry in slot.SubPartModelPaths)
            {
                var slot_model_path = slot_geometry.Item1;
                var slot_model_geometry = slot_geometry.Item2;

                if (Config.LoadModelStructure)
                {
                    var matched = ExploreModelForSingleChunk(GameDB, slot_model_path, slot_model_geometry, StringComparison.OrdinalIgnoreCase,
                        Config: Config, Parent: hardpointNode);
                    if (matched == null)
                    {

                    }
                    else
                    {

                    }
                }
                //These don't really work with model references.....
                //Geometry would need to be baked up, or 

                else if (Config.IncludeModelReferences)
                {
                    var extra_geometry = Build_Dummy(slot_model_geometry, SCHieracrhyNodeType.Dummy, Parent: hardpointNode);
                    extra_geometry.ModelPath = slot_model_path;
                    extra_geometry.AdditionalProperties.Add("TIS_ModelChunk", slot_model_geometry);
                }
            }

            if (Recursive && slot.Children.Count > 0)
            {
                //hardpointNode should be populated with model details by this point
                //Now we check the loadout configuration to see if we need to recursive apply loadouts to loaded equipment
                var flatChildren = Utilities.FlattenRecursive(hardpointNode, h => h.Children).OrderBy(h => h.Name).ToList();

                var searchSlot = slot;
                if (searchSlot.Class == "ItemPort")
                {
                    searchSlot = slot.Children[0];
                }

                foreach (var child in searchSlot.Children)
                {
                    //TODO: Better solution...
                    var child_node = flatChildren.FirstOrDefault(c => c.Name == child.PortName && c.NodeType == SCHieracrhyNodeType.Chunk);

                    if (child_node != null)
                    {
                        ApplyLoadout(GameDB, child, child_node, Config, Recursive);
                    }
                    else
                    {
                        Log.Warning($"Unable to find hardpoint for loadout entry: {child.PortName}");
                    }
                }
            }
        }

        public static SCHieracrhyNode ExploreArchive(DFDatabase GameDB, string ArchivePath, SearchConfig Config = null)
        {
            Config = Config ?? new SearchConfig();
            var rootArchive = SOC_Archive.LoadFromGameDBSocPak(GameDB, ArchivePath);
            return ExploreArchive(GameDB, rootArchive, Config);
        }

        public static SCHieracrhyNode ExploreArchive(DFDatabase GameDB, SOC_Archive Archive, SearchConfig Config = null)
        {
            Config = Config ?? new SearchConfig();

            var node = ExploreNode(GameDB, Archive.RootEntry, Config);
            return node;
        }

        public static SCHieracrhyNode ExploreNode(DFDatabase GameDB, SOC_Entry RootEntry, SearchConfig Config = null)
        {
            Config = Config ?? new SearchConfig();
            var explorer = new SOC_Explorer(GameDB);

            var result = explorer.Inner_ExploreNode(RootEntry, Config);
            result.TotalCount = Utilities.FlattenRecursive(result, r => r.Children).Count();
            return result;
        }

        private SCHieracrhyNode Inner_ExploreNode(SOC_Entry Entry, SearchConfig Config, SCHieracrhyNode Parent = null)
        {
            var node = Build_Entry(Entry, Parent);
            SCHieracrhyNode myEntityNode = null;

            Entry.LoadFullDetails();

            if (Config.LoadEntities || Config.LoadReferences)
            {
                Entry.LoadAllEntities(DynamicComponents: Config.DynamicComponents, FilterByComponents: false);
            }

            if (Config.LoadEntities)
            {
                var fullEnts = Entry.Entities.OfType<FullEntity>().ToList();
                if (fullEnts.Count > 0)
                {
                    var entityContainer = Config.DummyEntries ? Build_Dummy("Entities", Parent: node) : node;

                    //TODO: Better place for this filtering.
                    //But we should hide some classes from exporting to save space
                    foreach (var Entity in fullEnts)
                    {
                        bool has_models = !string.IsNullOrEmpty(Entity.ModelPath) && Config.LoadModelStructure;

                        //Skip entities in the group only if they don't have a model to them.
                        if (!has_models && IgnoreEntityClasses.Contains(Entity.Class, StringComparer.OrdinalIgnoreCase))
                        {
                            continue;
                        }

                        SCHieracrhyNode entityNode;
                        if (has_models)
                        {
                            entityNode = ExploreModel(GameDB, Entity.ModelPath, Config, entityContainer);
                            entityNode.LocalPosition = Entity.LocalPosition;
                            entityNode.LocalRotation = Entity.LocalRotation;
                            entityNode.LocalScale = Entity.LocalScale;
                        }
                        else
                            entityNode = Build_Entity(Entity, entityContainer);

                        WriteEntityProperties(GameDB, entityNode, Entity);

                        if (Entity == Entry.OwnEntity)
                            myEntityNode = entityNode;
                    }
                }
            }

            if (Config.LoadReferences)
            {
                if (Entry.GeometryReferences.Count > 0)
                {
                    var geometryContainer = Config.DummyEntries ? Build_Dummy("Geometry", Parent: node) : node;

                    foreach (var reference in Entry.GeometryReferences)
                    {
                        var geometry = Build_Geometry(reference, geometryContainer);

                        if (!string.IsNullOrEmpty(reference.ModelPath) && Config.LoadModelStructure)
                        {
                            geometry.ModelPath = null;

                            ExploreModel(GameDB, reference.ModelPath, Config, geometry);
                        }
                    }
                }
            }

            if (Config.Recursive)
            {
                if (Entry.Children.Count > 0)
                {
                    var childContainer = Config.DummyEntries ? Build_Dummy("Children", Parent: node) : node;

                    foreach (var Child in Entry.Children)
                    {
                        Inner_ExploreNode(Child, Config, childContainer);
                    }
                }
            }

            if(Entry.OwnEntity != null)
            {
                node.DisplayName = myEntityNode?.DisplayName;
            }

            return node;
        }

        private static void WriteEntityProperties(DFDatabase GameDatabase, SCHieracrhyNode Node, FullEntity Entity)
        {
            var elem = Entity.Element;

            var node_tags = new List<string>();

            if (Entity.Tags.Length > 0)
                node_tags.Add(GameDatabase.ConvertTags(string.Join(",", Entity.Tags)));

            //These are properties that will make it through to the FBX export
            Node.AdditionalProperties.Add("TIS_EntityID", Entity.GUID.ToString());
            Node.AdditionalProperties.Add("TIS_EntityClass", Entity.Class);

            List<Guid> EntityConnections = new List<Guid>();

            switch (Entity.Class)
            {
                case "TransitGateway":

                    break;
                case "TransitDestination":
                    var name_key = elem.SelectSingleNode("./PropertiesDataCore/SCTransitDestination/@Name")?.Value;
                    var name = string.IsNullOrEmpty(name_key) ? GameDatabase.Localize(name_key) : "Unknown Stop";
                    Node.Name = name; //Overwrite the name property here.
                    break;
                case "TransitManager":
                    var destination_elems = elem.SelectSingleNode("./SCTransitManager/TransitDestinations/Destination/Gateway").OfType<XmlElement>();
                    var gateways = new List<string>();
                    foreach(var dest_elem in destination_elems)
                    {
                        var destination_id = dest_elem.GetAttribute("Entity");
                        var doorway_id = dest_elem.SelectSingleNode("./Peripheral[@eType=2]/@Entity")?.Value ?? Guid.Empty.ToString();

                        gateways.Add($"{destination_id}");
                        gateways.Add($"{doorway_id}");
                    }
                    if(gateways.Count > 0)
                    {
                        Node.AdditionalProperties.Add("TIS_TransitStops", string.Join(",", gateways));
                    }
                    break;
            }

            var area = elem.SelectSingleNode("./AreaComponent") as XmlElement;
            if(area != null)
            {
                List<Vector2> coordinates = null;
                float bottom = 0;
                float height = 0;

                switch (area.GetAttribute("Type"))
                {
                    case "Box":
                        var min = area.GetAttribute("BoxMin").Split(',').Select(float.Parse).ToArray();
                        var max = area.GetAttribute("BoxMax").Split(',').Select(float.Parse).ToArray();
                        
                        if(min.Length == 3 || max.Length == 3)
                        {
                            bottom = min[2];
                            height = max[2] - min[2];

                            coordinates = new List<Vector2>()
                            {
                                new Vector2(min[0], min[1]),
                                new Vector2(min[0], max[1]),
                                new Vector2(max[0], max[1]),
                                new Vector2(max[0], min[1])
                            };
                        }
                        break;
                    case "Shape":

                        var coords = area
                            .SelectNodes("./Points/Point/@Pos")
                            .OfType<XmlAttribute>()
                            .Select(a => a.Value.Split(',').Select(float.Parse).ToArray())
                            .ToList();

                        if(coords.Count > 0)
                        {
                            bottom = coords.First()[0];
                            height = float.Parse(area.GetAttribute("Height"));
                            coordinates = coords.Select(c => new Vector2(c[0], c[1])).ToList();
                        }

                        break;
                    default:
                        break;
                }

                if(coordinates != null)
                {
                    Node.AdditionalProperties.Add("TIS_Area_Bottom", bottom);
                    Node.AdditionalProperties.Add("TIS_Area_Height", height);
                    Node.AdditionalProperties.Add("TIS_Area_Points", coordinates);
                    
                    foreach(var connection in area.SelectNodes("./Entities/Entity/@EntityCryGUID").OfType<XmlAttribute>())
                    {
                        EntityConnections.Add(new Guid(connection.Value));
                    }
                }
                else
                {
                    
                }
            }

            var interactable_component = elem.SelectSingleNode("./PropertiesDataCore/InteractableComponent");
            if (interactable_component != null)
                node_tags.Add("interactable");

            var door = elem.SelectSingleNode("./SCItemDoor");
            if (door != null)
                node_tags.Add("door");

            Node.AdditionalProperties.Add("TIS_Tags", String.Join(",", node_tags));

            if (EntityConnections.Count > 0)
                Node.AdditionalProperties.Add("TIS_Connections", String.Join(",", EntityConnections.Select(e => e.ToString())));
        }

        private static SCHieracrhyNode Build_Dummy(string NodeTitle, SCHieracrhyNodeType NodeType = SCHieracrhyNodeType.Dummy, SCHieracrhyNode Parent = null)
        {
            var node = new SCHieracrhyNode()
            {
                ID = Guid.Empty,
                Name = NodeTitle,
                NodeType = NodeType,
                Parent = Parent,
                LocalPosition = Vector3.Zero,
                LocalRotation = Vector4.Identity,
                LocalScale = Vector3.One
            };
            Parent?.Children?.Add(node);
            return node;
        }

        private static SCHieracrhyNode Build_From_Node(ChunkNode Node, SCHieracrhyNodeType NodeType, SCHieracrhyNode Parent = null)
        {
            var transform = Utilities.MatrixFromTupleTransposed(Node.LocalTransformTuple);
            transform.Decompose(out var _pos, out var _rot, out var _scale);

            var node = new SCHieracrhyNode()
            {
                ID = Guid.Empty,
                Name = Node.Name,
                NodeType = NodeType,
                Parent = Parent,
                LocalPosition = new Vector3(_pos.X, _pos.Y, _pos.Z),
                LocalRotation = new Vector4(_rot.X, _rot.Y, _rot.Z, _rot.W),
                LocalScale = new Vector3(_scale.X, _scale.Y, _scale.Z)
            };
            Parent?.Children?.Add(node);
            return node;
        }

        private static SCHieracrhyNode Build_Entry(SOC_Entry Entry, SCHieracrhyNode Parent = null)
        {

            var node = new SCHieracrhyNode()
            {
                ID = Entry.GUID,
                Name = Entry.Name,
                NodeType = SCHieracrhyNodeType.ObjectContainer,
                Parent = Parent,
                SOCNode = Entry,
                SOCEntity = Entry?.OwnEntity,
                LocalPosition = Entry.LocalPosition,
                LocalRotation = Entry.LocalRotation,
                LocalScale = Entry.LocalScale,
                Tags = Entry.Tags
            };
            Parent?.Children?.Add(node);
            return node;
        }

        private static SCHieracrhyNode Build_Entity(FullEntity Entity, SCHieracrhyNode Parent = null)
        {
            var node = new SCHieracrhyNode()
            {
                ID = Entity.GUID,
                Name = Entity.Label,
                DisplayName = Entity.TryGetDisplayName(),
                NodeType = SCHieracrhyNodeType.Entity,
                Parent = Parent,
                SOCEntity = Entity,
                LocalPosition = Entity.LocalPosition,
                LocalRotation = Entity.LocalRotation,
                LocalScale = Entity.LocalScale,
                ModelPath = Utilities.CleanPath(Entity.ModelPath),
                Tags = Entity.Tags.ToList()
            };
            Parent?.Children?.Add(node);
            return node;
        }

        private static SCHieracrhyNode Build_Geometry(SOC_ReferencedGeometry Geometry, SCHieracrhyNode Parent =  null)
        {
            var node = new SCHieracrhyNode()
            {
                ID = Guid.NewGuid(), //Referenced geometry does not have a GUID, so make one
                Name = Geometry.Name,
                NodeType = SCHieracrhyNodeType.Geometry,
                Parent = Parent,
                SOCGeometry = Geometry,
                LocalPosition = Geometry.LocalPosition,
                LocalRotation = Geometry.LocalRotation,
                LocalScale = Geometry.LocalScale,
                ModelPath = Utilities.CleanPath(Geometry.ModelPath),
            };
            Parent?.Children?.Add(node);
            return node;
        }

        public class SearchConfig
        {
            public bool Recursive { get; set; } = true;
            public bool DynamicComponents { get; set; } = false;

            public bool LoadEntities { get; set; } = true;
            public bool LoadReferences { get; set; } = true;
            public bool DummyEntries { get; set; } = false;
            public bool LoadModelStructure { get; set; } = false;
            public bool LoadModelGeometry { get; set; } = false;

            public bool ApplyShipLoadout { get; set; } = false;

            public bool IncludeModelReferences { get; set; } = false;

            public List<string> UsedModels { get; set; } = new List<string>();

            public void AddModel(string ModelPath)
            {
                if (string.IsNullOrWhiteSpace(ModelPath))
                    return;
                var model_path = ModelPath.Replace("\\", "/");
                if (!UsedModels.Contains(model_path))
                    UsedModels.Add(model_path);
            }
        }
    }

    [DebuggerDisplay("{Name} - {NodeType}")]
    public class SCHieracrhyNode
    {
        public Guid ID { get; set; }
        public long ModelID { get; set; }

        public string Name { get; set; }
        public string DisplayName { get; internal set; }

        public SCHieracrhyNodeType NodeType { get; set; }
        public bool __TransformWarning { get; set; }

        public Vector3 LocalPosition { get => localPosition; set { localPosition = value; _local = null; } }
        public Vector4 LocalRotation { get => localRotation; set { localRotation = value; _local = null; } }
        public Vector3 LocalScale { get => localScale; set { localScale = value; _local = null; } }

        public Dictionary<string, object> AdditionalProperties = new Dictionary<string, object> { };

        public CryModelGeometryData GeometryData { get; set; }

        private DN.Matrix4x4? _local = default;
        public DN.Matrix4x4 LocalTransform
        {
            get
            {
                if (_local == null)
                {
                    var R = DN.Matrix4x4.CreateFromQuaternion(new DN.Quaternion(LocalRotation.X, LocalRotation.Y, LocalRotation.Z, LocalRotation.W));
                    var S = DN.Matrix4x4.CreateScale(LocalScale.X, LocalScale.Y, LocalScale.Z);
                    var T = DN.Matrix4x4.CreateTranslation(LocalPosition.X, LocalPosition.Y, LocalPosition.Z);
                    _local = (S * R) * T;
                }
                return _local.Value;
            }
            set
            {
                _local = value;

                var error = value.Decompose(out var _LocalPosition, out var _LocalRotation, out var _LocalScale);

                LocalPosition = _LocalPosition;
                LocalRotation = _LocalRotation;
                LocalScale = _LocalScale;

                __TransformWarning = error;
            }
        }

        private DN.Matrix4x4 _global = default;
        private Vector3 _globalPositon;
        private Vector4 _globalRotation;
        private Vector3 _globalScale;
        private Vector3 localPosition = Vector3.Zero;
        private Vector4 localRotation = Vector4.Identity;
        private Vector3 localScale = Vector3.One;

        public DN.Matrix4x4 GlobalTransform
        {
            get
            {
                if (_global != null) CalculateGlobalTransform();
                return _global;
            }
        }

        public Vector3 GlobalPosition { get { if (_global != null) CalculateGlobalTransform(); return _globalPositon; } }
        public Vector4 GlobalRotation { get { if (_global != null) CalculateGlobalTransform(); return _globalRotation; } }
        public Vector3 GlobalScale { get { if (_global != null) CalculateGlobalTransform(); return _globalScale; } }


        public SOC_Entry SOCNode { get; set; }
        public FullEntity SOCEntity { get; set; }
        public SOC_ReferencedGeometry SOCGeometry { get; set; }

        public List<string> Tags { get; set; }

        public string ModelPath { get; set; }
        public string ModelPathFixed
        {
            get { return Path.ChangeExtension(ModelPath, "").TrimEnd('.'); }
        }

        public int TotalCount { get; set; }

        private void CalculateGlobalTransform()
        {
            if (_global == default)
            {
                var globalToLocal = LocalTransform * (Parent?.GlobalTransform ?? DN.Matrix4x4.Identity);
                _global = globalToLocal;
                _global.Decompose(out _globalPositon, out _globalRotation, out _globalScale);
            }
        }

        public SCHieracrhyNode Parent { get; set; } = null;
        public List<SCHieracrhyNode> Children { get; set; } = new List<SCHieracrhyNode>();

        public Vector3 TranfomPoint(Vector3 Position)
        {
            return DN.Vector3.Transform((DN.Vector3)Position, GlobalTransform);
        }
    }

    public enum SCHieracrhyNodeType
    {
        Root,
        Dummy,
        ObjectContainer,
        Entity,
        Geometry,
        Model,
        Chunk,
        Hardpoint,
        Equipment
    }
}
