﻿using CgfConverter;
using ICSharpCode.SharpZipLib.Zip;
using Newtonsoft.Json.Linq;
using Serilog;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using TradeInSpace.Explore.DataForge;
using TradeInSpace.Explore.Subsumption;
using unforge;

namespace TradeInSpace.Explore.Socpak
{
    [DebuggerDisplay("SC {Name}")]
    public class SOC_Archive : IPackedFileReader
    {
        internal DFDatabase LoadedFrom { get; set; }

        public string Name { get; private set; }
        public string Path { get; private set; }
        public ZipFile FileData { get; private set; }
        public SOC_Entry RootEntry { get; set; }
        public XmlDocument RootDoc { get; set; }
        public Dictionary<Guid, XmlDocument> EntityData { get; set; } = new Dictionary<Guid, XmlDocument>();

        private Stream ZipStream { get; set; }

        private SOC_Archive() { }

        public static SOC_Archive LoadFromGameDBSocPak(DFDatabase rawGameData, string Name, string Path, bool LoadChildren = false, SOC_Entry Parent = null)
        {
            var rootEntry = SOC_Entry.CreateRoot(Path);
            var rootArchive = LoadFromSoc(rawGameData, rootEntry, Path, LoadChildren);

            if(rootArchive == null)
            {
                Log.Warning($"SOC Archive does not exist: {Path} - {Name}");
                return null;
            }

            rootArchive.EnumerateEntityDetails();
            rootEntry.SourceFile = rootArchive;
            
            if(Parent != null)
            {
                rootEntry.Parent = Parent;
                Parent.Children.Add(rootEntry);
            }
            
            return rootArchive;
        }

        public static SOC_Archive LoadFromGameDBSocPak(DFDatabase rawGameData, string ArchivePath, bool LoadChildren = true, SOC_Entry Parent = null)
        {
            var ArchiveName = System.IO.Path.GetFileNameWithoutExtension(ArchivePath.Replace('\\', '/'));
            return LoadFromGameDBSocPak(rawGameData, ArchiveName, ArchivePath, LoadChildren, Parent);
        }

        public static SOC_Archive LoadFromSubsubmption(DFDatabase rawGameData, SubsumptionNode node, SOC_Entry parentEntry)
        {
            SOC_Archive childArchive;

            if(string.IsNullOrEmpty(node.Filename))
            {
                childArchive = parentEntry.Archive;
            }
            else
            {
                var containerPath = node.SOC_Entry?.ArchivePath ?? $"ObjectContainers/{node.Filename}";
                childArchive = LoadFromGameDBSocPak(rawGameData, containerPath, false, parentEntry);

                if (childArchive == null)
                {
                    Log.Warning($"Unable to load {containerPath}");
                    return null;
                }
            }

            //childArchive.RootEntry.LoadFullDetails();
            //childArchive.RootEntry.LoadAllEntities();
            childArchive.RootEntry.SuperGUID = node.SuperGUID;
            childArchive.RootEntry.GUID = new Guid(node.SuperGUID.Split(',').Last());
            
            /*
            foreach (var subsumpChild in node.Children)
            {
                var matchingChild = parentArchive.RootEntry.Children.SingleOrDefault(c => c.SuperGUID == subsumpChild.SuperGUID);

                if(matchingChild == null)
                {
                    //Uh-oh
                }
                else
                {
                    subsumpChild.SOC_Entry = matchingChild;
                }
            }
            */

            return childArchive;
        }

        internal static SOC_Archive LoadFromSoc(DFDatabase rawGameData, SOC_Entry entry, string PathOverride = null, bool LoadChildren = true)
        {
            var archivePath = PathOverride ?? entry.ArchivePath;

            var rawFileData = rawGameData.ReadPackedFileBinary(archivePath);
            if (rawFileData == null)
            {
                Log.Verbose($"Unable to locate .socpak file for entry {entry.Name} - {archivePath}");
                return null;
            }

            var zipStream = new MemoryStream(rawFileData);
            if (zipStream == null) return null;

            var socArchive = new SOC_Archive()
            {
                LoadedFrom = rawGameData,
                // STUPID LINUX PARSING SLASHES DIFFERENT HERE
                Name = System.IO.Path.GetFileNameWithoutExtension(archivePath.Replace('\\', '/')),
                Path = archivePath,
                ZipStream = zipStream,
                FileData = new ZipFile(zipStream),
                RootDoc = new XmlDocument(),
                RootEntry = entry
            };

            try
            {
                //Only attempt to parse a matching <archivename>.xml file when load socpaks
                //This class is also used to load any archive (.pak) from the SC p4k
                if (System.IO.Path.GetExtension(archivePath).ToLower() == ".socpak")
                {
                    var mainEntry = $"{socArchive.Name}.xml";
                    var archive = socArchive.FileData.GetEntry(mainEntry);
                    if (archive != null)
                    {
                        var mainEntryStream = socArchive.FileData.GetInputStream(archive);
                        socArchive.RootDoc.Load(mainEntryStream);
                    }
                    else
                    {
                        Log.Error($"Unable to find expected file ('{socArchive.Name}.xml') in archive. Files are:");
                        for (int i = 0; i < socArchive.FileData.Count; i++)
                        {
                            var file = socArchive.FileData[i];
                            Log.Error($"\t[{i}] = {file.Name} ({file.Size} bytes)");
                        }

                        throw new FileNotFoundException();
                    }
                }
            }
            catch (OutOfMemoryException) { throw; }
            catch (FileNotFoundException) { throw; }
            catch (Exception ex)
            {
                Log.Error(ex, "Error parsing root document");
            }

            entry.Archive = socArchive;

            if (LoadChildren)
            {
                socArchive.EnumerateEntityDetails();

                if(socArchive.RootDoc.DocumentElement != null)
                {
                    socArchive.LoadContainerChildren(socArchive.RootDoc.DocumentElement, socArchive.RootEntry, true);
                }
                else
                {
                    Log.Error($"Root Document Element is null on archive {socArchive.Name} - [{socArchive.Path}]");
                }
            }

            return socArchive;
        }

        internal void EnumerateEntityDetails()
        {
            if (EntityData.Count != 0) return;

            for(int i = 0; i< FileData.Count; i++)
            {
                var file = FileData[i];
                if (file.Name.Contains("/entdata/"))
                {
                    var doc = ReadCryXML(file.Name);
                    //var test = ReadCryJSON(file.Name);
                    //DoTest(file.Name);

                    if (doc != null)
                    {
                        Guid entityGUID = Guid.Parse(doc.SelectSingleNode("./Entity/@EntityCryGUID").Value);
                        if (!EntityData.ContainsKey(entityGUID))
                        {
                            EntityData.Add(entityGUID, doc);
                        }
                        else
                        {

                        }
                    }
                }
            }
        }

        public bool HasRecord(string Path)
        {
            return FileData.GetEntry(Path) != null;
        }

        public byte[] ReadFile(string Path)
        {
            if (!HasRecord(Path)) return null;

            return FileData.GetInputStream(FileData.GetEntry(Path)).ReadAllBytes(false);
        }

        public Stream GetFileStream(string Path)
        {
            if (!HasRecord(Path)) return null;

            return FileData.GetInputStream(FileData.GetEntry(Path));
        }

        public CryEngine ReadPackedCGAFile(string filePath)
        {
            return new CryEngine(filePath, "", HasRecord, ReadFile);
        }

        private void Test(string Name, Action func)
        {
            Stopwatch s = new Stopwatch();
            s.Start();
            for (int i = 0; i < 50 * 1000; i++)
            {
                func();
            }
            s.Stop();
            Log.Information($"Test: [{Name}] - {s.ElapsedMilliseconds} ms");
        }

        public void DoTest(string Path)
        {
            if (!HasRecord(Path)) return;

            byte[] socData = ReadFile(Path);

            for (int i = 0; i < socData.Length - 5; i++)
            {
                if (socData[i] == 'C' && socData[i + 1] == 'r' && socData[i + 2] == 'y' &&
                    socData[i + 3] == 'X' && socData[i + 4] == 'm' && socData[i + 5] == 'l')
                {
                    using (MemoryStream ms = new MemoryStream(socData, i, socData.Length - i))
                    {
                        Test("JSON", () =>
                        {
                            ms.Seek(0, SeekOrigin.Begin);
                            Parse.CryEngineToJSONConverter.ParseToJObject(ms);
                        });

                        Test("XML", () =>
                        {
                            ms.Seek(0, SeekOrigin.Begin);
                            CryXmlSerializer.ReadStream(ms);
                        });
                    }
                }
            }
        }

        public XmlDocument ReadCryXML(string Path)
        {
            if (!HasRecord(Path)) return null;

            /*
            byte[] socData = ReadFile(Path);

            using ( MemoryStream strm = new MemoryStream(socData))
            {
                strm.Seek(0, SeekOrigin.Begin);

                if (socData[0] == 'C' && socData[1] == 'r' && socData[2] == 'y' &&
                    socData[3] == 'X' && socData[4] == 'm' && socData[5] == 'l')
                {
                    return CryXmlSerializer.ReadStream(strm);
                }

                foreach (var chunk in ReadChunkedFileStream(strm))
                {
                    if (chunk.ChunkID == ChunkType.BinaryXmlDataSC)
                    {
                        strm.Seek(chunk.ChunkStart, SeekOrigin.Begin);

                        return CryXmlSerializer.ReadStream(strm);
                    }
                }
            }
            */
            
            byte[] socData = ReadFile(Path);

            for (int i = 0; i < socData.Length - 5; i++)
            {
                if (socData[i] == 'C' && socData[i + 1] == 'r' && socData[i + 2] == 'y' &&
                    socData[i + 3] == 'X' && socData[i + 4] == 'm' && socData[i + 5] == 'l')
                {
                    try
                    {
                        using (MemoryStream ms = new MemoryStream(socData, i, socData.Length - i))
                        {
                            ms.Seek(0, SeekOrigin.Begin);
                            var xmlDoc = CryXmlSerializer.ReadStream(ms);
                            return xmlDoc;
                        }
                    }
                    catch (Exception ex)
                    {
                        //Console.WriteLine($"Error reading .soc data for {this.Name} in {this.ParentSoc.Name}");
                        return null;
                    }
                }
            }

            return null;
        }

        public JObject ReadCryJSON(string Path)
        {
            if (!HasRecord(Path)) return null;

            byte[] socData = ReadFile(Path);

            for (int i = 0; i < socData.Length - 5; i++)
            {
                if (socData[i] == 'C' && socData[i + 1] == 'r' && socData[i + 2] == 'y' &&
                    socData[i + 3] == 'X' && socData[i + 4] == 'm' && socData[i + 5] == 'l')
                {
                    try
                    {
                        using (MemoryStream ms = new MemoryStream(socData, i, socData.Length - i))
                        {
                            return Parse.CryEngineToJSONConverter.ParseToJObject(ms);
                        }
                    }
                    catch (Exception ex)
                    {
                        //Console.WriteLine($"Error reading .soc data for {this.Name} in {this.ParentSoc.Name}");
                        return null;
                    }
                }
            }
            return null;
        }

        public string ReadPlanetJSON(string Path)
        {
            foreach(var chunk in ReadChunkedFile(Path))
            {
                if(chunk.ChunkID == 0x00150011)
                {
                    var dataText = Encoding.UTF8.GetString(chunk.SOCData, chunk.ChunkStart, chunk.ChunkLength);
                    return dataText;
                }
                else
                {

                }
            }
            return null;
        }

        public IEnumerable<(int ChunkID, byte[] SOCData, int ChunkStart, int ChunkLength)> ReadChunkedFile(string Path)
        {
            if (!HasRecord(Path)) yield break;

            byte[] socData = FileData.GetInputStream(FileData.GetEntry(Path)).ReadAllBytes(false);

            if (socData[0] != 'C' || socData[1] != 'r' || socData[2] != 'C' || socData[3] != 'h' || socData[4] != 'F') yield break;

            var chunkCount = BitConverter.ToInt32(socData, 0x08);
            var chunkStart = BitConverter.ToInt32(socData, 0x0C);

            for (int chunkNumber = 0; chunkNumber < chunkCount; chunkNumber++)
            {
                var chunkHeaderStart = chunkStart + chunkNumber * 0x10;

                var chunkID = BitConverter.ToInt32(socData, chunkHeaderStart);
                //Number
                var chunkDataSize = BitConverter.ToInt32(socData, chunkHeaderStart + 0x08);
                var chunkDataStart = BitConverter.ToInt32(socData, chunkHeaderStart + 0x0C);

                yield return (chunkID, socData, chunkDataStart, chunkDataSize);
            }
        }

        public IEnumerable<(ChunkType ChunkID, int ChunkStart, int ChunkLength)> ReadChunkedFileStream(Stream fileStream)
        {
            using (BinaryReader streamReader = new BinaryReader(fileStream))
            {
                var header1 = streamReader.ReadChar();
                var header2 = streamReader.ReadChar();
                var header3 = streamReader.ReadChar();
                var header4 = streamReader.ReadChar();
                var header5 = streamReader.ReadChar();

                if (header1 != 'C' || header2 != 'r' || header3 != 'C' || header4 != 'h' || header5 != 'F')
                    yield break;

                streamReader.BaseStream.Seek(0x08, SeekOrigin.Begin);

                var chunkCount = streamReader.ReadInt32();
                var chunkStart = streamReader.ReadInt32();

                for (int chunkNumber = 0; chunkNumber < chunkCount; chunkNumber++)
                {
                    var chunkHeaderStart = chunkStart + chunkNumber * 0x10;

                    streamReader.BaseStream.Seek(chunkHeaderStart, SeekOrigin.Begin);
                    var chunkID = (ChunkType)streamReader.ReadUInt32();
                    streamReader.BaseStream.Seek(4, SeekOrigin.Current);
                    //Number
                    var chunkDataSize = streamReader.ReadInt32();
                    var chunkDataStart = streamReader.ReadInt32();

                    yield return (chunkID, chunkDataStart, chunkDataSize);
                }
            }
        }

        /*
public XmlDocument ReadCryXMLData(string path, bool UseSource = false)
{
   var zipEntry = FileData.GetEntry(path);
   if (zipEntry == null) return null;
   w
   var zipStream = FileData.GetInputStream(zipEntry);
   byte[] zipContent = new byte[zipEntry.Size];
   zipStream.Read(zipContent, 0, (int)zipEntry.Size);

   using (var reader = new MemoryStream(zipContent))
   {
       return unforge.CryXmlSerializer.ReadStream(reader);
   }
}
*/
        private void LoadContainerChildren(XmlElement element, SOC_Entry parent, bool Recursive = false)
        {
            var directChildren = element.SelectNodes("./ChildObjectContainers/Child").Cast<XmlElement>().ToList();
            
            foreach (XmlElement childContainerDetails in directChildren)
            {
                var socChild = SOC_Entry.FromXMLElement(childContainerDetails, this, parent);

                if (EntityData.ContainsKey(socChild.GUID))
                    socChild.AdditionalData = EntityData[socChild.GUID].DocumentElement;

                if (parent == null)
                    RootEntry.Children.Add(socChild);
                else
                    parent.Children.Add(socChild);

                if(Recursive)
                    LoadContainerChildren(childContainerDetails, socChild, Recursive);
            }
        }

        public IEnumerable<(int Depth, SOC_Entry Container)> ExploreRecursive(int Depth = 0, bool LoadEntities = false)
        {
            return RootEntry.ExploreRecursiveInner(Depth, false, LoadEntities: LoadEntities);
        }

        IEnumerable<ZipEntry> IPackedFileReader.ZipChildren() => FileData.OfType<ZipEntry>();
        bool IPackedFileReader.HasEntry(string Path) => FileData.GetEntry(Path) != null;
        byte[] IPackedFileReader.ReadFileBinary(string Path) => ReadFile(Path);
        CryEngine IPackedFileReader.ReadCryEngineFile(string Path) => ReadPackedCGAFile(Path);
    }
}
