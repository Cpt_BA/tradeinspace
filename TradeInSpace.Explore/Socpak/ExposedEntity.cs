﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Xml;
using TradeInSpace.Explore.SC;
using TradeInSpace.Explore.Socpak.Components;

namespace TradeInSpace.Explore.Socpak
{
    [DebuggerDisplay("EE [{GUID}] {Label} [{Tags}]")]
    public class ExposedEntity
    {
        public SOC_Entry Owner { get; set; }
        public Guid GUID { get; set; }
        public string Label { get; set; }
        public string[] Tags { get; set; }

        public string SuperGUID { get { return Owner.SuperGUID + "," + GUID.ToString(); } }

        public static ExposedEntity FromXML(XmlElement element, SOC_Entry entry)
        {
            return new ExposedEntity()
            {
                Owner = entry,
                GUID = Guid.Parse(element.GetAttribute("guid")),
                Label = element.GetAttribute("label"),
                Tags = element.HasAttribute("tags") ? element.GetAttribute("tags").Split(',') : new string[] { }
            };
        }
    }
}
