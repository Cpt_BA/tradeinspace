﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using Newtonsoft.Json;
using Serilog.Parsing;

namespace TradeInSpace.Explore
{
    public class SCLauncherReader
    {
        public static LauncherSettings LoadGameSettings(string FallbackRootFolder = null)
        {
            var tempFolder = Path.Combine(Path.GetTempPath(), "SC_Temp");

            try
            {
                string dbPath;

                if(RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
                {
                    dbPath = Environment.ExpandEnvironmentVariables(@"%appdata%\rsilauncher\Local Storage\leveldb");
                }
                else
                {
                    // Docker/Linux support, hardcoded for now. TODO: config/args?
                    dbPath = "/sc_data/leveldb";
                }

                char[] SplitChars = new[] { '\x00', '\x01' };
                
                var options = new LevelDB.Options
                {
                    CreateIfMissing = false
                };
                //Copy the files out in case the launcher is running and has them opened
                Directory.CreateDirectory(tempFolder);
                foreach (string newPath in Directory.GetFiles(dbPath, "*.*",
                    SearchOption.AllDirectories))
                    File.Copy(newPath, newPath.Replace(dbPath, tempFolder), true);

                Dictionary<string, string> launcherSettings = new Dictionary<string, string>();

                
                using (var db = new LevelDB.DB(options, tempFolder))
                {
                    foreach (var item in db as IEnumerable<KeyValuePair<string, string>>)
                    {
                        var keyParts = item.Key.Split(SplitChars, StringSplitOptions.RemoveEmptyEntries);
                        var value = item.Value.Split(SplitChars, StringSplitOptions.RemoveEmptyEntries).Last();

                        if (keyParts.Length != 2) continue;

                        if (keyParts[0] == "_file://")
                        {
                            launcherSettings.Add(keyParts[1], value);
                        }
                    }
                }

                if (launcherSettings.ContainsKey("library"))
                {
                    var settings = JsonConvert.DeserializeObject<LauncherSettings>(launcherSettings["library"]);
                    settings.Loaded();


                    if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
                    {
                        settings.RootInstallPath = launcherSettings.ContainsKey("library-folder") ? launcherSettings["library-folder"] : FallbackRootFolder;
                    }
                    else
                    {
                        // Docker/Linux support, hardcoded for now. TODO: config/args?
                        settings.RootInstallPath = "/sc_data/game";
                    }

                    return settings;
                }
            }
            catch (Exception ex)
            {
                 Console.WriteLine($"Error loading settings: {ex.Message}");
            }
            finally
            {
                if (Directory.Exists(tempFolder))
                {
                    Directory.Delete(tempFolder, true);
                }
            }

            return null;
        }

        public static LauncherSettings BuildFallbackSettings(string P4KPath)
        {
            var settings = new LauncherSettings()
            {
                RootInstallPath = Path.GetDirectoryName(P4KPath),
                Games = new LauncherGame[]
                {
                    new LauncherGame()
                    {
                        Name = "Star Citizen",
                        ID = "SC",
                        AvailableChannels = new LauncherGameChannel[]
                        {
                            new LauncherGameChannel()
                            {
                                Name = "LIVE",
                                ID = "",
                                VersionLabel = "DirectLoad"
                            }
                        }
                    }
                },
                InstalledVersions = new LauncherInstalledVersion[]
                {
                    new LauncherInstalledVersion()
                    {
                        GameID = "SC",
                        ChannelID = "",
                        VersionSettings = new LauncherInstalledVersion.InstalledSettings()
                        {
                            GameID = "SC",
                            ChannelID = "",
                            InstallDir = "",
                        }
                    }
                }
            };
            settings.InstalledVersions[0].AllSettings = settings;
            settings.InstalledVersions[0].ChannelDetails = settings.Games[0].AvailableChannels[0];
            return settings;
        }
    }

    public class LauncherSettings
    {
        [JsonProperty("installed")]
        public LauncherGame[] Games { get; set; }
        [JsonProperty("defaults")]
        public LauncherInstalledVersion[] InstalledVersions { get; set; }
        [JsonIgnore]
        public string RootInstallPath { get; set; }

        public LauncherInstalledVersion GetGame(string GameID, string ChannelID)
        {
            return InstalledVersions.First(iv => iv.GameID == GameID && iv.ChannelID == ChannelID);
        }

        internal void Loaded()
        {
            foreach(var version in InstalledVersions)
            {
                var matchingGame = Games.FirstOrDefault(g => g.ID == version.GameID);
                version.ChannelDetails = matchingGame?.AvailableChannels.FirstOrDefault(c => c.ID == version.ChannelID);
                version.AllSettings = this;
            }
        }
    }

    public class LauncherGame
    {
        [JsonProperty("id")]
        public string ID { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("channels")]
        public LauncherGameChannel[] AvailableChannels { get; set; }

        public LauncherGameChannel GetChannel(string ChannelID)
        {
            return AvailableChannels.FirstOrDefault(c => c.ID == ChannelID);
        }
    }

    public class LauncherGameChannel
    {
        [JsonProperty("id")]
        public string ID { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("version")]
        public string Version { get; set; }
        [JsonProperty("versionLabel")]
        public string VersionLabel { get; set; }
        [JsonProperty("platformId")]
        public string PlatformID { get; set; }
        [JsonProperty("status")]
        public string Status { get; set; }
    }

    public class LauncherInstalledVersion
    {
        [JsonIgnore]
        public LauncherGameChannel ChannelDetails { get; internal set; }
        [JsonIgnore]
        public LauncherSettings AllSettings { get; internal set; }



        [JsonProperty("gameId")]
        public string GameID { get; set; }
        [JsonProperty("channelId")]
        public string ChannelID { get; set; }
        [JsonProperty("settings")]
        public InstalledSettings VersionSettings { get; set; }


        public string GetPath(string FileName)
        {
            return Path.Combine(AllSettings.RootInstallPath, VersionSettings.InstallDir, ChannelID, FileName);
        }

        public class InstalledSettings
        {
            [JsonProperty("gameId")]
            public string GameID { get; set; }
            [JsonProperty("channelId")]
            public string ChannelID { get; set; }
            [JsonProperty("gameName")]
            public string GameName { get; set; }
            [JsonProperty("channelName")]
            public string ChannelName { get; set; }
            [JsonProperty("hostname")]
            public string Hostname { get; set; }
            [JsonProperty("port")]
            public string Port { get; set; }
            [JsonProperty("eacSandbox")]
            public string EACSandbox { get; set; }
            [JsonProperty("installDir")]
            public string InstallDir { get; set; }
            [JsonProperty("executable")]
            public string Executable { get; set; }
            [JsonProperty("launchOptions")]
            public string LaunchOptions { get; set; }
        }
    }
}
