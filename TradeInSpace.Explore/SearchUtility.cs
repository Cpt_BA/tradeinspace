﻿using ICSharpCode.SharpZipLib.Zip;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml;
using TradeInSpace.Explore.DataForge;
using TradeInSpace.Explore.Socpak;

namespace TradeInSpace.Explore
{
    public class SearchUtility
    {
        // Ignore socpak and dcb files from showing up on content scans.
        public static string[] ContentIgnoreExtensions = new string[] { ".socpak", ".dcb" };

        public static async Task<IEnumerable<SearchResult>> PerformTextSearchAsync(DFDatabase gameDB, SearchParameters searchParameters, Action<string, int, int> fnReportProgress = null)
        {
            return await Task.Run(() => PerformTextSearch(gameDB, searchParameters, fnReportProgress));
        }

        public static IEnumerable<SearchResult> PerformTextSearch(DFDatabase gameDB, SearchParameters searchParameters, Action<string, int, int> fnReportProgress = null)
        {
            IEnumerable<int> CheckSearch(string Input, string SearchText)
            {
                int searchPosition = -1;

                switch (searchParameters.Mode)
                {
                    case SearchParameters.SearchMode.Contains:
                        do
                        {
                            searchPosition = Input.IndexOf(SearchText, searchPosition + 1, StringComparison.OrdinalIgnoreCase);
                            if (searchPosition != -1)
                                yield return searchPosition;
                        } while (searchPosition != -1);
                        break;
                    case SearchParameters.SearchMode.ContainsCaseSensitive:
                        do
                        {
                            searchPosition = Input.IndexOf(SearchText, searchPosition + 1);
                            if (searchPosition != -1)
                                yield return searchPosition;
                        } while (searchPosition != -1);
                        break;
                    case SearchParameters.SearchMode.Regex:
                        var matches = Regex.Matches(Input, SearchText);
                        foreach(Match match in matches)
                        {
                            yield return match.Index;
                        }
                        break;
                }
            }

            IEnumerable<SearchResult> ProcessSearch(SearchParameters.DataSource SourceType, Func<string> fnGetName, Func<string> fnSourceGetText, string[] Path = null)
            {
                if (searchParameters.Source.HasFlag(SourceType))
                {
                    var name = fnGetName();

                    SearchSource Context = new SearchSource()
                    {
                        Name = name,
                        SourceType = SourceType,
                        SourcePath = Path,
                        SourceDocument = null
                    };

                    if (Context.SourceText == null)
                        Context.SourceText = fnSourceGetText();

                    if (Context.SourceText == null)
                        yield break;

                    var lineBreaks = CheckSearch(Context.SourceText, "\n");

                    foreach(var textSearchResult in CheckSearch(Context.SourceText, searchParameters.Text))
                    {
                        var lineStart = Context.SourceText.LastIndexOf("\n", textSearchResult);
                        var lineEnd = Context.SourceText.IndexOf("\n", textSearchResult);
                        if (lineStart == -1) lineStart = 0;
                        if (lineEnd == -1) lineEnd = Context.SourceText.Length - 1;
                        var line = Context.SourceText.Substring(lineStart, lineEnd - lineStart);
                        var lineNumber = lineBreaks.Count(lidx => lidx < textSearchResult);

                        yield return new SearchResult()
                        {
                            Index = textSearchResult,
                            //TODO: Won't be accurate for regex
                            Length = searchParameters.Text.Length,
                            Context = line,
                            LineNumber = lineNumber,
                            LineOffset = textSearchResult - lineStart,
                            Source = Context
                        };
                    }
                }

                yield break;
            }

            int index = 0;
            foreach(ZipEntry fileEntry in gameDB.GamePack)
            {
                byte[] rawContent = null;
                string textContent = null;
                SOC_Archive archiveContent = null;

                IEnumerable<SearchResult> nameResults = new List<SearchResult>();
                IEnumerable<SearchResult> contentResults = new List<SearchResult>();
                IEnumerable<SearchResult> socpakFastResults = new List<SearchResult>();
                IEnumerable<SearchResult> socpakFullResults = new List<SearchResult>();
                IEnumerable<SearchResult> dataforgeResults = new List<SearchResult>();

                if (fnReportProgress != null)
                    fnReportProgress(fileEntry.Name, index, (int)gameDB.GamePack.Count);

                try
                {

                    if (searchParameters.Source.HasFlag(SearchParameters.DataSource.P4KContent))
                    {
                        rawContent = gameDB.ReadPackedFileBinary(fileEntry.Name);
                        textContent = Encoding.UTF8.GetString(rawContent);
                    }

                    nameResults = ProcessSearch(SearchParameters.DataSource.P4KName, () => fileEntry.Name, () => fileEntry.Name);
                    contentResults = ProcessSearch(SearchParameters.DataSource.P4KContent, () => fileEntry.Name, () => textContent);

                    if (Path.GetExtension(fileEntry.Name) == ".socpak")
                    {
                        if (searchParameters.Source.HasFlag(SearchParameters.DataSource.SocpakFast) ||
                            searchParameters.Source.HasFlag(SearchParameters.DataSource.SocpakFull))
                        {
                            archiveContent = SOC_Archive.LoadFromGameDBSocPak(gameDB, fileEntry.Name, LoadChildren: false);
                        }

                        if (archiveContent != null)
                        {
                            socpakFastResults = ProcessSearch(SearchParameters.DataSource.SocpakFast,
                                () => $"{archiveContent.Name}.xml",
                                () =>
                            {
                                return Utilities.FormatXML(archiveContent.RootDoc);
                            }, Path: new string[] { fileEntry.Name });

                            if (searchParameters.Source.HasFlag(SearchParameters.DataSource.SocpakFull))
                            {
                                socpakFullResults = archiveContent.FileData
                                    .OfType<ZipEntry>()
                                    .SelectMany((innerFileEntry) =>
                                {
                                    var innerContent = archiveContent.ReadCryXML(innerFileEntry.Name);

                                    return ProcessSearch(SearchParameters.DataSource.SocpakFull,
                                        () => innerFileEntry.Name,
                                        () =>
                                        {
                                            return Utilities.FormatXML(innerContent);
                                        }, Path: new string[] { fileEntry.Name });
                                });
                            }
                        }
                    }

                    if (fileEntry.Name.Equals(DFDatabase.GameDCBPath, StringComparison.OrdinalIgnoreCase))
                    {
                        if (!gameDB.DFLoaded)
                        {
                            //This means we loaded in search-only mode
                            //Need to load dcb file and save to memory.
                            gameDB.LoadDataforgeContent();
                        }

                        var dataforgeXML = gameDB.Document;
                        var rootNodes = dataforgeXML.DocumentElement.ChildNodes;

                        dataforgeResults = rootNodes
                            .OfType<XmlNode>()
                            .SelectMany((node) =>
                        {
                            var nodeXML = node.OuterXml;
                            var nodeName = $"<{node.Name}>";
                            return ProcessSearch(SearchParameters.DataSource.DataForge,
                                () => nodeName,
                                () =>
                            {
                                return Utilities.FormatXML(nodeXML);
                            }, Path: new string[] { DFDatabase.GameDCBPath });
                        });
                    }

                }
                catch (Exception ex)
                {
                    // :^)
                    //TODO: Add proper logging/handling here
                }

                var finalResults = nameResults
                    .Union(contentResults)
                    .Union(socpakFastResults)
                    .Union(socpakFullResults)
                    .Union(dataforgeResults);

                foreach (var result in finalResults)
                {
                    yield return result;
                }
                index++;
            }

            if (searchParameters.Source.HasFlag(SearchParameters.DataSource.DataForge))
            {
                if(fnReportProgress != null)
                    fnReportProgress(DFDatabase.GameDCBPath, (int)gameDB.GamePack.Count, (int)gameDB.GamePack.Count);

            }
        }

    }

    public class SearchResult
    {
        public int Index { get; set; }
        public int Length { get; set; }
        public int LineNumber { get; set; }
        public int LineOffset { get; set; }
        public string Context { get; set; }

        public SearchSource Source { get; set; }

        internal SearchResult() { }
        public string LineDescription
        {
            get
            {
                return $"{LineNumber}:{LineOffset}";
            }
        }
    }

    public class SearchSource
    {
        public SearchParameters.DataSource SourceType { get; set; }
        public string Name { get; set; }
        public string FullName
        {
            get
            {
                var fullPath = new List<string>(SourcePath ?? new string[] { });
                fullPath.Add(Name);

                return string.Join(" | ", fullPath);
            }
        }
        public string[] SourcePath { get; set; }

        public string SourceText { get; set; }
        public XmlDocument SourceDocument { get; set; }
    }

    public class SearchParameters
    {
        public string Text { get; set; }
        public SearchMode Mode { get; set; }
        public DataSource Source { get; set; }

        public enum SearchMode
        {
            Contains,
            ContainsCaseSensitive,
            Regex
        }

        [Flags]
        public enum DataSource
        {
            None = 0,

            P4KName = 1,
            P4KContent = 2,
            DataForge = 4,
            SocpakFast = 8,
            SocpakFull = 16
        }
    }
}
