﻿using SC_Explorer.Properties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.IO;
using TradeInSpace.Explore.DataForge;
using System.ComponentModel;
using TradeInSpace.Explore;
using TradeInSpace.Explore.Subsumption;
using Microsoft.Win32;

namespace SC_Explorer
{
    /// <summary>
    /// Interaction logic for LoadingWindow.xaml
    /// </summary>
    public partial class LoadingWindow : Window, INotifyPropertyChanged
    {
        private bool loading;
        private LauncherInstalledVersion selectedVersion;
        private LauncherSettings settings;

        public bool Loading { get => loading; set { loading = value; OnPropertyChanged(nameof(Loading)); } }
        public event PropertyChangedEventHandler PropertyChanged;

        public LoadingWindow()
        {
            try
            {
                var launcherInstallDir = Microsoft.Win32.Registry.GetValue(@"HKEY_LOCAL_MACHINE\SOFTWARE\81bfc699-f883-50c7-b674-2483b6baae23", "InstallLocation", null) as string;
                var RSIDir = Path.GetDirectoryName(launcherInstallDir);
                Settings = SCLauncherReader.LoadGameSettings(RSIDir);
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Error loading launcher details. {ex.Message}");
                Application.Current.Shutdown();
            }

            InitializeComponent();

            DFDatabase.ParserProgress += DFDatabase_ParserProgress;
        }

        private void DFDatabase_ParserProgress(object sender, DFDBProgressEventArgs e)
        {
            this.Dispatcher.Invoke(() =>
            {
                primaryProgress.Value = ((int)e.Phase * 100.0f) / (int)ProgressPhase.FinalStep;
                string progressPhase = "";
                switch (e.Phase)
                {
                    case ProgressPhase.ReadP4K:
                        progressPhase = "Reading P4K Archive";
                        break;
                    case ProgressPhase.ReadDCB:
                        progressPhase = "Reading Dataforge DB";
                        break;
                    case ProgressPhase.DFEntries:
                        progressPhase = "Parsing Dataforge Entries";
                        break;
                    case ProgressPhase.Entities:
                        progressPhase = "Creating SC Entities";
                        break;
                    case ProgressPhase.Loadouts:
                        progressPhase = "Creating Ship Loadouts";
                        break;
                    case ProgressPhase.Socpak:
                        progressPhase = "Loading SOC Archive";
                        break;
                }
                primaryLabel.Content = progressPhase;

                secondaryProgress.Value = e.Progress * 100;
                if (e.CurrentStepMaximum == 0) e.CurrentStepMaximum = 1;
                secondaryLabel.Content = $"[{e.Progress * 100:##.#}%] {e.CurrentStepProgress}/{e.CurrentStepMaximum}";
            });
        }

        private void OnPropertyChanged(string PropertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(PropertyName));
        }

        public LauncherSettings Settings
        { 
            get => settings;
            set 
            { 
                settings = value;
                OnPropertyChanged(nameof(Settings));
            }
        }

        public LauncherInstalledVersion SelectedVersion
        {
            get => selectedVersion;
            set
            {
                selectedVersion = value;
                OnPropertyChanged(nameof(SelectedVersion));
            }
        }

        private async void Load_Click(object sender, RoutedEventArgs e)
        {
            bool loadLoadouts = this.chkLoadouts.IsChecked ?? false;
            bool loadSubsumption = this.chkSubsumption.IsChecked ?? false;
            bool loadSubDetails = this.chkSubDetails.IsChecked ?? false;
            bool loadSubEntities = this.chkSubEntities.IsChecked ?? false;

            if (SelectedVersion != null)
            {
                Loading = true;
                try
                {
                    var loadedDatabase = await Task.Run(() =>
                    {
                        var tmpLoaded = DFDatabase.ParseFromSettings(SelectedVersion, CacheLevel.Diabled);

                        tmpLoaded.ParseDFContent();

                        if (loadLoadouts)
                            tmpLoaded.Loadouts.PreloadLoadouts();
                        if (loadSubsumption)
                        {
                            tmpLoaded.Subsumption.LoadAllSystems();
                            tmpLoaded.Subsumption.LoadAllChildren(PreloadDetails: loadSubDetails, PreloadEntities: loadSubEntities);
                        }

                        return tmpLoaded;
                    });
                    Loading = false;

                    DFDatabase.ParserProgress -= this.DFDatabase_ParserProgress;

                    MainWindow window = new MainWindow(loadedDatabase);
                    window.Show();
                    this.Close();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error Loading game .P4K file" + Environment.NewLine + TradeInSpace.Explore.Utilities.GetFullErrorDetails(ex));
                    Application.Current.Shutdown();
                }
            }
            else
            {
                MessageBox.Show("No game file selected");
            }
        }

        private async void SearchLoad_Click(object sender, RoutedEventArgs e)
        {
            if (SelectedVersion != null)
            {
                Loading = true;

                var loadedDatabase = await Task.Run(() => DFDatabase.ParseFromSettings(SelectedVersion, CacheLevel.Diabled));

                Loading = false;

                DFDatabase.ParserProgress -= this.DFDatabase_ParserProgress;

                MainWindow window = new MainWindow(loadedDatabase);
                window.Show();
                this.Close();
            }
            else
            {
                MessageBox.Show("No game file selected");
            }
        }

        private void DirectOpen_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog()
            {
                DefaultExt = "*.p4k",
                Filter = "SC .P4K Archive|*.p4k",
                InitialDirectory = Properties.Settings.Default.LastLoadDirectory
            };
            var result = ofd.ShowDialog();

            if (result == true)
            {
                Properties.Settings.Default.LastLoadDirectory = System.IO.Path.GetDirectoryName(ofd.FileName);
                Properties.Settings.Default.Save();

                Settings = SCLauncherReader.BuildFallbackSettings(ofd.FileName);
                _Versions.SelectedIndex = 0;
            }
        }
    }
}
