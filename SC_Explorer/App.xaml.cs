﻿using Serilog;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace SC_Explorer
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private void Application_Startup(object sender, StartupEventArgs e)
        {
            Log.Logger = new LoggerConfiguration()
                .WriteTo.Debug(Serilog.Events.LogEventLevel.Debug)
                .WriteTo.File("SC_Explorer.log", Serilog.Events.LogEventLevel.Warning, 
                    retainedFileCountLimit: 5, 
                    rollingInterval: RollingInterval.Day)
                .CreateLogger();

            Current.DispatcherUnhandledException += Current_DispatcherUnhandledException;
        }

        private void Current_DispatcherUnhandledException(object sender, System.Windows.Threading.DispatcherUnhandledExceptionEventArgs e)
        {
            Log.Error(e.Exception, "Unhandled fatal error");

            MessageBox.Show($"Fatal Exception Occurred: {e.Exception.Message}\r\n\r\n{e.Exception.StackTrace}", 
                "Fatal Exception", MessageBoxButton.OK, MessageBoxImage.Error);
        }
    }
}
