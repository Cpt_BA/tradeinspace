﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace SC_Explorer.Util
{
    public class NullVisibilityConverter : IValueConverter
    {
        public Visibility Value { get; set; } = Visibility.Visible;
        public Visibility NoValue { get; set; } = Visibility.Collapsed;

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return value == null ? NoValue : Value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            Visibility? v = value as Visibility?;
            return ((v.HasValue) || (v.Value == NoValue)) ? null : "";
        }
    }
}
