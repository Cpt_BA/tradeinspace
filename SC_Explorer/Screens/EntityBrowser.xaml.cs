﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TradeInSpace.Explore;
using TradeInSpace.Explore.DataForge;
using TradeInSpace.Explore.SC;

namespace SC_Explorer.Screens
{
    /// <summary>
    /// Interaction logic for DataforgeBrowser.xaml
    /// </summary>
    public partial class EntityBrowser : UserControl
    {
        public static readonly DependencyProperty GameDatabaseProperty = DependencyProperty.Register(
            "GameDatabase",
            typeof(DFDatabase),
            typeof(EntityBrowser));


        public EntityBrowser()
        {
            InitializeComponent();
        }

        public DFDatabase GameDatabase
        {
            get { return (DFDatabase)GetValue(GameDatabaseProperty); }
            set { SetValue(GameDatabaseProperty, value); }
        }

        private void gd_Entities_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (gd_Entities.SelectedItem == null)
            {
                selectedEntityDetails.Text = "";
            }
            else
            {
                var entitySelection = gd_Entities.SelectedItem as SCEntityClassDef;
                selectedEntityDetails.Text = Utilities.FormatXML(entitySelection.Element.OuterXml);
            }
        }

        private void gd_Components_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (gd_Components.SelectedItem == null)
            {
                selectedComponentDetails.Text = "";
            }
            else
            {
                var component = (SCComponent)gd_Components.SelectedItem;
                selectedComponentDetails.Text = Utilities.FormatXML(component.Element.OuterXml);
            }
        }

        private void Entities_Filter(object sender, FilterEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(Entity_Search.Text)) 
                e.Accepted = true;
            else
            {
                if(e.Item is SCEntityClassDef entity)
                {
                    bool idMatch = entity.ID.ToString().IndexOf(Entity_Search.Text, StringComparison.OrdinalIgnoreCase) >= 0;
                    bool nameMatch = entity.InstanceName.IndexOf(Entity_Search.Text, StringComparison.OrdinalIgnoreCase) >= 0;
                    e.Accepted = idMatch || nameMatch;
                }
            }
        }

        private void Components_Filter(object sender, FilterEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(Component_Search.Text)) 
                e.Accepted = true;
            else
            {
                if(e.Item is SCComponent component)
                {
                    e.Accepted = component.Type.IndexOf(Component_Search.Text, StringComparison.OrdinalIgnoreCase) >= 0;
                }
            }
        }

        private void Entity_Search_TextChanged(object sender, TextChangedEventArgs e)
        {
            var cvs = this.FindResource("sortedEntities") as CollectionViewSource;
            cvs.View.Refresh();
        }
        private void Component_Search_TextChanged(object sender, TextChangedEventArgs e)
        {
            var cvs = this.FindResource("sortedComponents") as CollectionViewSource;
            cvs.View.Refresh();
        }
    }
}
