﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TradeInSpace.Explore;
using TradeInSpace.Explore.DataForge;
using TradeInSpace.Explore.SC;

namespace SC_Explorer.Screens
{
    /// <summary>
    /// Interaction logic for ComponentExplorer.xaml
    /// </summary>
    public partial class ComponentBrowser : UserControl
    {
        public static readonly DependencyProperty GameDatabaseProperty = DependencyProperty.Register(
            "GameDatabase",
            typeof(DFDatabase),
            typeof(ComponentBrowser), new PropertyMetadata((d, dc) =>
            {
                var ents = dc.NewValue as DFDatabase;
                if (ents != null)
                {
                    d.SetValue(ComponentsProperty,
                        ents.Entities
                        .SelectMany(e => e.Components)
                        .GroupBy(c => c.Type)
                        .ToDictionary(c => c.Key, c => c.ToList()));
                }
            }));
        public static readonly DependencyProperty ComponentsProperty = DependencyProperty.Register(
            "Components",
            typeof(Dictionary<string, List<SCComponent>>),
            typeof(ComponentBrowser));


        public ComponentBrowser()
        {
            InitializeComponent();
        }

        public DFDatabase GameDatabase
        {
            get { return (DFDatabase)GetValue(GameDatabaseProperty); }
            set { SetValue(GameDatabaseProperty, value); }
        }

        public Dictionary<string, List<SCComponent>> Components
        {
            get { return (Dictionary<string, List<SCComponent>>)GetValue(ComponentsProperty); }
            set { SetValue(ComponentsProperty, value); }
        }

        private void gd_Entities_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (gd_Entities.SelectedItem == null)
            {
                selectedEntityDetails.Text = "";
            }
            else
            {
                var entityComponent = ((SCComponent)gd_Entities.SelectedItem);


                selectedComponentDetails.Text = Utilities.FormatXML(entityComponent.Element.OuterXml);
                selectedEntityDetails.Text = Utilities.FormatXML(entityComponent.ParentEntity.Element.OuterXml);
            }
        }

        private void Components_Filter(object sender, FilterEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(Component_Search.Text))
                e.Accepted = true;
            else
            {
                if (e.Item is KeyValuePair<string, List<SCComponent>> component)
                {
                    e.Accepted = component.Key.IndexOf(Component_Search.Text, StringComparison.OrdinalIgnoreCase) >= 0;
                }
            }
        }

        private void Entities_Filter(object sender, FilterEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(Entity_Search.Text))
                e.Accepted = true;
            else
            {
                if (e.Item is SCEntityClassDef entity)
                {
                    e.Accepted = entity.InstanceName.IndexOf(Entity_Search.Text, StringComparison.OrdinalIgnoreCase) >= 0;
                }
            }
        }

        private void Entity_Search_TextChanged(object sender, TextChangedEventArgs e)
        {
            var cvs = this.FindResource("sortedEntities") as CollectionViewSource;
            cvs.View.Refresh();
        }
        private void Component_Search_TextChanged(object sender, TextChangedEventArgs e)
        {
            var cvs = this.FindResource("sortedComponents") as CollectionViewSource;
            cvs.View.Refresh();
        }
    }
}
