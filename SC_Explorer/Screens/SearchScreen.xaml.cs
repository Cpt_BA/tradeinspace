﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TradeInSpace.Explore;
using TradeInSpace.Explore.DataForge;

namespace SC_Explorer.Screens
{
    /// <summary>
    /// Interaction logic for SearchScreen.xaml
    /// </summary>
    public partial class SearchScreen : UserControl, INotifyPropertyChanged
    {
        public static readonly DependencyProperty GameDatabaseProperty = DependencyProperty.Register(
            "GameDatabase",
            typeof(DFDatabase),
            typeof(SearchScreen));
        private bool busy;
        private string searchText;

        public event PropertyChangedEventHandler PropertyChanged;
        private void RaisePropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public SearchScreen()
        {
            InitializeComponent();
        }

        public DFDatabase GameDatabase
        {
            get { return (DFDatabase)GetValue(GameDatabaseProperty); }
            set { SetValue(GameDatabaseProperty, value); }
        }

        public ObservableCollection<SearchResult> Results { get; set; } = new ObservableCollection<SearchResult>();
        public bool Busy
        {
            get => busy;
            set
            {
                busy = value;
                RaisePropertyChanged(nameof(Busy));
                RaisePropertyChanged(nameof(CanSearch));
            }
        }

        public bool CanSearch
        {
            get
            {
                return !Busy && (SearchText?.Length ?? 0) > 2;
            }
            set { }
        }

        public string SearchText
        {
            get => searchText;
            set
            {
                searchText = value;
                RaisePropertyChanged(nameof(SearchText));
                RaisePropertyChanged(nameof(CanSearch));
            }
        }


        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (this.GameDatabase == null)
                return;

            if (Busy)
                return;

            Busy = true;

            var gameDB = this.GameDatabase;

            SearchParameters.DataSource source = SearchParameters.DataSource.None;

            foreach (var sourceName in lstSources.SelectedItems.OfType<ListBoxItem>())
            {
                switch (sourceName.Content)
                {
                    case "DataForge":
                        source |= SearchParameters.DataSource.DataForge;
                        break;
                    case "P4K Name":
                        source |= SearchParameters.DataSource.P4KName;
                        break;
                    case "Content":
                        source |= SearchParameters.DataSource.P4KContent;
                        break;
                    case ".socpak Fast":
                        source |= SearchParameters.DataSource.SocpakFast;
                        break;
                    case ".socpak Full (Entities)":
                        source |= SearchParameters.DataSource.SocpakFull;
                        break;
                }
            }

            SearchParameters.SearchMode mode = SearchParameters.SearchMode.Contains;

            if (radCaseIns.IsChecked ?? false)
                mode = SearchParameters.SearchMode.Contains;
            else if (radCaseSen.IsChecked ?? false)
                mode = SearchParameters.SearchMode.ContainsCaseSensitive;
            else if (radRegex.IsChecked ?? false)
                mode = SearchParameters.SearchMode.Regex;

            SearchParameters parameters = new SearchParameters()
            {
                Mode = mode,
                Source = source,
                Text = txtSearch.Text
            };

            Results.Clear();

            new Thread(() =>
            {
                onSearchProgress("Start", 0, (int)gameDB.GamePack.Count);

                foreach (var result in SearchUtility.PerformTextSearch(gameDB, parameters, onSearchProgress))
                {
                    Application.Current.Dispatcher.Invoke(() =>
                    {
                        Results.Add(result);
                    });
                }

                onSearchProgress("Done", (int)gameDB.GamePack.Count, (int)gameDB.GamePack.Count);

                Application.Current.Dispatcher.Invoke(() =>
                {
                    Busy = false;
                });
            }).Start();
        }

        private void onSearchProgress(string FileName, int Index, int Count)
        {
            if (Index % 1000 == 0 || Index == Count)
            {
                Application.Current.Dispatcher.Invoke(() =>
                {
                    var progress = ((float)Index) / Count;

                    prgProgress.Value = progress * 100;
                    lblProgress.Text = $"[{Index}/{Count} files]\n{FileName}";
                });
            }
        }

        private void lstResults_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var result = grdResults.SelectedItem as SearchResult;
            if (result == null)
                return;

            selectedResult.Text = result.Source.SourceText;
            selectedResult.ScrollTo(result.LineNumber, result.LineOffset);
            selectedResult.Select(result.Index, result.Length);
        }

        private void SaveFile_MenuItem_Click(object sender, RoutedEventArgs e)
        {
            var result = grdResults.SelectedItem as SearchResult;
            if (result == null)
                return;

            System.Windows.Forms.SaveFileDialog sfd = new System.Windows.Forms.SaveFileDialog();
            sfd.FileName =  result.Source.SourceType == SearchParameters.DataSource.DataForge ?
                            result.Source.Name.Replace("<", "").Replace(">", "") :
                            System.IO.Path.GetFileName(result.Source.Name);
            var diag = sfd.ShowDialog();
            if (diag == System.Windows.Forms.DialogResult.OK || 
                diag == System.Windows.Forms.DialogResult.Yes)
            {
                //NOTE: Should probably be doing binary exporting here...

                var outData = result.Source.SourceText;
                if(result.Source.SourceType == SearchParameters.DataSource.P4KName)
                {
                    outData = GameDatabase.ReadPackedFile(result.Source.Name);
                }

                System.IO.File.WriteAllText(sfd.FileName, outData);
            }
        }
    }
}
