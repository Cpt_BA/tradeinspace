﻿using Microsoft.Win32;
using Serilog;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TradeInSpace.Explore;
using TradeInSpace.Explore.DataForge;
using TradeInSpace.Explore.SC;
using TradeInSpace.Explore.SC.Components;
using TradeInSpace.Explore.SC.Loadout;
using TradeInSpace.Export;

namespace SC_Explorer.Screens
{
    /// <summary>
    /// Interaction logic for ComponentExplorer.xaml
    /// </summary>
    public partial class VehicleBrowser : UserControl
    {
        public static readonly DependencyProperty GameDatabaseProperty = DependencyProperty.Register(
            "GameDatabase",
            typeof(DFDatabase),
            typeof(VehicleBrowser), new PropertyMetadata((d, dc) =>
            {
                var gameDB = dc.NewValue as DFDatabase;
                if (gameDB?.Loadouts != null)
                {
                    d.SetValue(VehiclesProperty, gameDB.Loadouts.AllLoadouts);
                }
            }));
        public static readonly DependencyProperty VehiclesProperty = DependencyProperty.Register(
            "Vehicles",
            typeof(List<LoadoutTree>),
            typeof(VehicleBrowser));


        public VehicleBrowser()
        {
            InitializeComponent();
        }

        public DFDatabase GameDatabase
        {
            get { return (DFDatabase)GetValue(GameDatabaseProperty); }
            set { SetValue(GameDatabaseProperty, value); }
        }

        public List<LoadoutTree> Vehicles
        {
            get { return (List<LoadoutTree>)GetValue(VehiclesProperty); }
            set { SetValue(VehiclesProperty, value); }
        }

        private void gd_Vehicles_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (gd_Vehicles.SelectedItem == null)
            {
                selectedEntityDetails.Text = "";
            }
            else
            {
                var loadout = gd_Vehicles.SelectedItem as LoadoutTree;


                selectedVehicleDetails.Text = Utilities.FormatXML(loadout?.From?.Element?.OuterXml);
                selectedVehicleLoadoutDetails.Text = Utilities.FormatXML(loadout?.LoadoutDocument?.OuterXml);

                var treeItem = t_VehicleLoadout.SelectedItem;
                if (treeItem is LoadoutEntry selectedEntry)
                {
                    selectedEntityDetails.Text = Utilities.FormatXML(selectedEntry?.EntryElement?.OuterXml);
                    //Show parent b/c child LoadoutEntry doesn't contain full item details
                    selectedComponentDetails.Text = Utilities.FormatXML(
                            selectedEntry.Port?.AttachedComponent?.Element?.OuterXml ??
                            selectedEntry.Parent?.Port?.AttachedComponent?.Element?.OuterXml);
                }
                else if (treeItem is LoadoutPort selectedPort)
                {
                    selectedEntityDetails.Text = Utilities.FormatXML(selectedPort.Element?.OuterXml);
                    selectedComponentDetails.Text = Utilities.FormatXML(selectedPort.AttachedComponent?.Element?.OuterXml);
                }
                else if (treeItem is LoadoutTree selectedTree)
                {
                    selectedEntityDetails.Text = Utilities.FormatXML(selectedTree.LoadoutDocument?.OuterXml);
                    selectedComponentDetails.Text = Utilities.FormatXML(selectedTree.From?.Element?.OuterXml);
                }
            }
        }

        private void Vehicles_Filter(object sender, FilterEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(Vehicle_Search.Text))
                e.Accepted = true;
            else
            {
                if (e.Item is KeyValuePair<string, List<SCComponent>> component)
                {
                    e.Accepted = component.Key.IndexOf(Vehicle_Search.Text, StringComparison.OrdinalIgnoreCase) >= 0;
                }
            }
        }

        private void Vehicle_Search_TextChanged(object sender, TextChangedEventArgs e)
        {
            var cvs = this.FindResource("sortedVehicles") as CollectionViewSource;
            cvs.View.Refresh();
        }

        private void t_VehicleLoadout_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            gd_Vehicles_SelectionChanged(null, null);
        }

        private void root_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            //Load and refresh data.
            SetValue(VehicleBrowser.VehiclesProperty, GameDatabase.Loadouts.AllLoadouts);
            Vehicle_Search_TextChanged(null, null);
        }

        private async void ExportModel_Click(object sender, RoutedEventArgs e)
        {
            var loadout = gd_Vehicles.SelectedItem as LoadoutTree;

            if (loadout == null)
            {
                Cursor = Cursors.Arrow;
                return;
            }

            SaveFileDialog sfd = new SaveFileDialog()
            {
                FileName = System.IO.Path.ChangeExtension(loadout.From.InstanceName, ".fbx"),
                Filter = $"Blender .fbx Files (*.fbx)|.fbx",
                OverwritePrompt = true,
                InitialDirectory = Properties.Settings.Default.LastExportDirectory
            };

            if (sfd.ShowDialog() ?? false)
            {
                Cursor = Cursors.Wait;

                //Get this into local context, otherwise threading issue
                var gamedb = this.GameDatabase;

                await Task.Run(() =>
                {
                    var structure = SOC_Explorer.ExploreShip(gamedb, loadout.Vehicle, MegaModelConfiguration);
                    var fbx_scene = CryToFBXExporter.ConvertHierarchyToScene(structure, gamedb);
                    CryToFBXExporter.WriteScene(fbx_scene, sfd.FileName);
                });

                MessageBox.Show("Export Succesful!");

                ShowFile(sfd.FileName);

                Cursor = Cursors.Arrow;
            }
        }

        private void ShowFile(string showPath)
        {
            if (!System.IO.Directory.Exists(showPath) && !System.IO.File.Exists(showPath))
            {
                MessageBox.Show(string.Format("{0} Path does not exist!", showPath));
                return;
            }

            ProcessStartInfo startInfo = new ProcessStartInfo()
            {
                Arguments = showPath,
                FileName = "explorer.exe"
            };

            Process.Start(startInfo);
        }

        SOC_Explorer.SearchConfig MegaModelConfiguration = new SOC_Explorer.SearchConfig()
        {
            LoadEntities = true,
            LoadReferences = true,
            Recursive = true,
            DummyEntries = true,
            DynamicComponents = true,
            LoadModelStructure = true,
            LoadModelGeometry = true,
            IncludeModelReferences = false,
            ApplyShipLoadout = true
        };
    }
}
