﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TradeInSpace.Explore;
using TradeInSpace.Explore.DataForge;
using TradeInSpace.Explore.SC;
using TradeInSpace.Explore.SC.Data;

namespace SC_Explorer.Screens
{
    /// <summary>
    /// Interaction logic for BuildingBlocksBrowser.xaml
    /// </summary>
    public partial class BuildingBlocksBrowser : UserControl
    {
        public static readonly DependencyProperty GameDatabaseProperty = DependencyProperty.Register(
            "GameDatabase",
            typeof(DFDatabase),
            typeof(BuildingBlocksBrowser), new PropertyMetadata((d, dc) =>
            {
                var ents = dc.NewValue as DFDatabase;
                if (ents != null)
                {
                    var canvases = ents.GetEntries<BuildingBlocksCanvas>().ToList();
                    d.SetValue(CanvasesProperty, canvases);
                }
            }));
        public static readonly DependencyProperty CanvasesProperty = DependencyProperty.Register(
            "Canvases",
            typeof(List<BuildingBlocksCanvas>),
            typeof(BuildingBlocksBrowser));

        public BuildingBlocksBrowser()
        {
            InitializeComponent();
        }

        public DFDatabase GameDatabase
        {
            get { return (DFDatabase)GetValue(GameDatabaseProperty); }
            set { SetValue(GameDatabaseProperty, value); }
        }

        public List<BuildingBlocksCanvas> Canvases
        {
            get { return (List<BuildingBlocksCanvas>)GetValue(CanvasesProperty); }
            set { SetValue(CanvasesProperty, value); }
        }

        private void Canvas_Search_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void gd_Canvases_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (gd_Canvases.SelectedItem == null)
            {
            }
            else
            {
                var canvas = (BuildingBlocksCanvas)gd_Canvases.SelectedItem;

                RebuildCanvas(canvas);
            }
        }

        private void gd_Widgets_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (gd_Widgets.SelectedItem == null)
            {
                selectedWidgetDetails.Text = "";
            }
            else
            {
                var widget = (BBCanvasWidgetBase)gd_Widgets.SelectedItem;
                selectedWidgetDetails.Text = Utilities.FormatXML(widget.Element.OuterXml);
            }
        }

        private void RebuildCanvas(BuildingBlocksCanvas bbCanvas)
        {
            canvasOut.Children.Clear();

            foreach(var block in bbCanvas.Blocks)
            {
                canvasOut.Children.Add(BuildElement(block));
            }
        }

        private FrameworkElement BuildElement(BBCanvasWidgetBase _base)
        {
            Border result = null;

            if(_base is BBWidget widget)
            {
                result = new Border();
            }
            else
            {
                result = new Border();
            }

            var finalPosition = _base.Position.ToVector3() + _base.PositionOffset.ToVector3();

            result.Margin = new Thickness(finalPosition.X, finalPosition.Y, 0, 0);
            result.Width = Math.Max(_base.Sizing.Width.Value, 0);
            result.Height = Math.Max(_base.Sizing.Height.Value, 0);

            result.BorderBrush = Brushes.Black;
            result.BorderThickness = new Thickness(1);

            return result;
        }
    }
}
