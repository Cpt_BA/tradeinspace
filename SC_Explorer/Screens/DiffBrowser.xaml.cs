﻿using CgfConverter;
using CgfConverter.CryEngineCore;
using DiffPlex.DiffBuilder;
using DiffPlex.DiffBuilder.Model;
using HoloXPLOR.DataForge;
using ICSharpCode.SharpZipLib.Zip;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml;
using TradeInSpace.Explore.DataForge;
using TradeInSpace.Explore.Socpak;
using TradeInSpace.Explore.Util;
using TradeInSpace.Models;

namespace SC_Explorer.Screens
{
    /// <summary>
    /// Interaction logic for DiffBrowser.xaml
    /// </summary>
    public partial class DiffBrowser : UserControl, INotifyPropertyChanged
    {
        public static readonly DependencyProperty GameDatabaseProperty = DependencyProperty.Register(
            nameof(GameDatabase),
            typeof(DFDatabase),
            typeof(DiffBrowser), new PropertyMetadata((d, dc) =>
            {
                var gameDB = dc.NewValue as DFDatabase;
                if (gameDB != null)
                {
                    //d.SetValue(ArchiveItemsProperty, ConvertZipToTree(gameDB.GamePack));
                }
            }));

        private List<DiffResult> removedDiffItems;
        private List<DiffResult> modifiedDiffItems;
        private List<DiffResult> addedDiffItems;

        private List<DiffResult> allRemovedDiffItems;
        private List<DiffResult> allModifiedDiffItems;
        private List<DiffResult> allAddedDiffItems;
        private bool busy;
        private int changeIndex;
        private int totalChanges;

        private Dictionary<Guid, XmlElement> oldDCBElements;
        private Dictionary<Guid, XmlElement> newDCBElements;

        private List<(DiffPiece, bool)> allDiffPieces;


        private DFDatabase CompareDatabase { get; set; }

        public DFDatabase GameDatabase
        {
            get { return (DFDatabase)GetValue(GameDatabaseProperty); }
            set { SetValue(GameDatabaseProperty, value); }
        }

        public List<DiffResult> AddedDiffItems
        {
            get => addedDiffItems; set
            {
                addedDiffItems = value;
                RaisePropertyChanged(nameof(AddedDiffItems));
            }
        }

        public List<DiffResult> ModifiedDiffItems
        {
            get => modifiedDiffItems; set
            {
                modifiedDiffItems = value;
                RaisePropertyChanged(nameof(ModifiedDiffItems));
            }
        }

        public List<DiffResult> RemovedDiffItems
        {
            get => removedDiffItems; set
            {
                removedDiffItems = value;
                RaisePropertyChanged(nameof(RemovedDiffItems));
            }
        }

        public List<DiffResult> AllRemovedDiffItems
        {
            get => allRemovedDiffItems; set
            {
                allRemovedDiffItems = value;
                RaisePropertyChanged(nameof(AllRemovedDiffItems));
            }
        }

        public List<DiffResult> AllModifiedDiffItems
        {
            get => allModifiedDiffItems; set
            {
                allModifiedDiffItems = value;
                RaisePropertyChanged(nameof(AllModifiedDiffItems));
            }
        }

        public List<DiffResult> AllAddedDiffItems
        {
            get => allAddedDiffItems; set
            {
                allAddedDiffItems = value;
                RaisePropertyChanged(nameof(AllAddedDiffItems));
            }
        }

        public bool Busy
        {
            get => busy;
            set
            {
                Cursor = value ? Cursors.Wait : Cursors.Arrow;
                busy = value;
                RaisePropertyChanged(nameof(Busy));
            }
        }

        public int ChangeIndex
        {
            get => changeIndex;
            set
            {
                changeIndex = value;
                RaisePropertyChanged(nameof(ChangeIndex));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void RaisePropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public DiffBrowser()
        {
            AddedDiffItems = new List<DiffResult>();
            ModifiedDiffItems = new List<DiffResult>();
            RemovedDiffItems = new List<DiffResult>();

            AllAddedDiffItems = new List<DiffResult>();
            AllModifiedDiffItems = new List<DiffResult>();
            AllRemovedDiffItems = new List<DiffResult>();

            InitializeComponent();
        }

        private async void btnCompare_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog()
            {
                DefaultExt = "*.p4k",
                Filter = "SC .P4K Archive|*.p4k",
                InitialDirectory = Properties.Settings.Default.LastCompareDirectory
            };
            var result = ofd.ShowDialog();

            if (result == true)
            {
                Properties.Settings.Default.LastCompareDirectory = System.IO.Path.GetDirectoryName(ofd.FileName);
                Properties.Settings.Default.Save();

                if (GameDatabase.FilePath == ofd.FileName)
                {
                    MessageBox.Show("Can not compare identical P4K files.");
                    return;
                }

                Busy = true;

                CompareDatabase = await Task.Run(
                    () => DFDatabase.ParseFromFile(ofd.FileName, CacheLevel.Diabled)
                );

                await BuildDiff();

                Busy = false;
            }
        }

        private void CollectionViewSource_FilterAdded(object sender, FilterEventArgs e)
        {
            e.Accepted = (e.Item as DiffResult).Status == DiffStatus.Added;
        }

        private void CollectionViewSource_FilterRemoved(object sender, FilterEventArgs e)
        {
            e.Accepted = (e.Item as DiffResult).Status == DiffStatus.Removed;
        }

        private void CollectionViewSource_FilterUpdated(object sender, FilterEventArgs e)
        {
            e.Accepted = (e.Item as DiffResult).Status == DiffStatus.Modified;
        }

        private async Task BuildDiff()
        {
            AddedDiffItems?.Clear();
            ModifiedDiffItems?.Clear();
            RemovedDiffItems?.Clear();
            AllAddedDiffItems?.Clear();
            AllModifiedDiffItems?.Clear();
            AllRemovedDiffItems?.Clear();

            DFDatabase compareDB = CompareDatabase;
            DFDatabase gameDB = GameDatabase;

            var diffResults = await Task.Run(
                () => DiffCalculator.BuildDiff(compareDB, gameDB)
            );

            AddedDiffItems = diffResults.RootAdded;
            ModifiedDiffItems = diffResults.RootModified;
            RemovedDiffItems = diffResults.RootRemoved;
            
            AllAddedDiffItems = diffResults.Added;
            AllModifiedDiffItems = diffResults.Modified;
            AllRemovedDiffItems = diffResults.Removed;
        }

        private async void TreeView_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            var diffResult = treeDiffs.SelectedItem as DiffResult;

            if (diffResult == null) return;
            if (diffResult.Status == DiffStatus.Dummy) return;

            if (Busy) return;
            Busy = true;

            //Handle looking for socpak changes inline, don't do a normal diff
            if (ExtractBrowser.ArchiveExtensions.Contains(System.IO.Path.GetExtension(diffResult.Name).ToLower()))
            {
                if (diffResult.Children.Count == 0)
                {
                    DFDatabase compareDB = CompareDatabase;
                    DFDatabase gameDB = GameDatabase;

                    var oldArchive = SOC_Archive.LoadFromGameDBSocPak(compareDB, diffResult.FilePath, false, null);
                    var newArchive = SOC_Archive.LoadFromGameDBSocPak(gameDB, diffResult.FilePath, false, null);

                    var archiveDiff = await Task.Run(
                        () => DiffCalculator.BuildDiff(oldArchive, newArchive)
                    );

                    var addedNode = DiffResult.Dummy($"[{archiveDiff.Added.Count:N0}] Added");
                    var modifiedNode = DiffResult.Dummy($"[{archiveDiff.Modified.Count:N0}] Modified");
                    var removedNode = DiffResult.Dummy($"[{archiveDiff.Removed.Count:N0}] Removed");

                    archiveDiff.RootAdded.ForEach(addedNode.Children.Add);
                    archiveDiff.RootModified.ForEach(modifiedNode.Children.Add);
                    archiveDiff.RootRemoved.ForEach(removedNode.Children.Add);

                    diffResult.Children.Add(addedNode);
                    diffResult.Children.Add(modifiedNode);
                    diffResult.Children.Add(removedNode);
                }

                Busy = false;
                return;
            }

            if (System.IO.Path.GetExtension(diffResult.Name).ToLower() == ".dcb")
            {
                if (diffResult.Children.Count == 0)
                {
                    DFDatabase compareDB = CompareDatabase;
                    DFDatabase gameDB = GameDatabase;

                    var dcbDiff = await Task.Run(
                        () => DiffCalculator.BuildDCBDiff(compareDB, gameDB, out oldDCBElements, out newDCBElements)
                    );

                    var addedNode = DiffResult.Dummy($"[{dcbDiff.Added.Count:N0}] Added");
                    var modifiedNode = DiffResult.Dummy($"[{dcbDiff.Modified.Count:N0}] Modified");
                    var removedNode = DiffResult.Dummy($"[{dcbDiff.Removed.Count:N0}] Removed");

                    dcbDiff.RootAdded.ForEach(addedNode.Children.Add);
                    dcbDiff.RootModified.ForEach(modifiedNode.Children.Add);
                    dcbDiff.RootRemoved.ForEach(removedNode.Children.Add);

                    diffResult.Children.Add(addedNode);
                    diffResult.Children.Add(modifiedNode);
                    diffResult.Children.Add(removedNode);
                }

                Busy = false;
                return;
            }

            //Otherwise try and force the file to render as text. and do the diff between them.
            string oldText = "", newText = "", oldTitle = "N/A", newTitle = "N/A";

            if (diffResult.Diff.OldSource is SOC_Archive socArchive) ;
            else if (diffResult.Diff.OldSource is DFDatabase oldDB) ;
            else
                throw new FormatException();

            switch (diffResult.Diff.Source)
            {
                case DiffSource.Archive:
                    switch (diffResult.Status)
                    {
                        case DiffStatus.Added:
                            newTitle = diffResult.Name;
                            newText = AttemptReadFileContents(diffResult.FilePath, diffResult.Diff.NewSource);
                            break;

                        case DiffStatus.Modified:
                            oldTitle = diffResult.Name;
                            oldText = AttemptReadFileContents(diffResult.FilePath, diffResult.Diff.OldSource);

                            newTitle = diffResult.Name;
                            newText = AttemptReadFileContents(diffResult.FilePath, diffResult.Diff.NewSource);
                            break;

                        case DiffStatus.Removed:
                            oldTitle = diffResult.Name;
                            oldText = AttemptReadFileContents(diffResult.FilePath, diffResult.Diff.OldSource);
                            break;
                    }
                    break;
                case DiffSource.DCB:
                    switch (diffResult.Status)
                    {
                        case DiffStatus.Added:
                            newTitle = diffResult.Name;
                            newText = Utilities.FormatXML(newDCBElements[diffResult.RecordGUID.Value].OuterXml);
                            break;

                        case DiffStatus.Modified:
                            oldTitle = diffResult.Name;
                            oldText = Utilities.FormatXML(oldDCBElements[diffResult.RecordGUID.Value].OuterXml);

                            newTitle = diffResult.Name;
                            newText = Utilities.FormatXML(newDCBElements[diffResult.RecordGUID.Value].OuterXml);
                            break;

                        case DiffStatus.Removed:
                            oldTitle = diffResult.Name;
                            oldText = Utilities.FormatXML(oldDCBElements[diffResult.RecordGUID.Value].OuterXml);
                            break;
                    }
                    break;
            }

            var diffModel = await Task.Run(
                () => SideBySideDiffBuilder.Instance.BuildDiffModel(oldText, newText, true, false)
            );

            diffViewer.DiffModel = diffModel;
            ChangeIndex = 0;

            var oldChanges = diffModel.OldText.Lines.Where(l => l.Type != ChangeType.Unchanged && l.Type != ChangeType.Imaginary);
            var newChanges = diffModel.NewText.Lines.Where(l => l.Type != ChangeType.Unchanged && l.Type != ChangeType.Imaginary);
            allDiffPieces = new List<(DiffPiece, bool)>();

            foreach(var line in oldChanges)
            {
                var matchingNew = newChanges.FirstOrDefault(c => c.Position == line.Position);

                if(matchingNew != null)
                {
                    allDiffPieces.Add((line, true));
                }
            }

            foreach(var newLine in newChanges)
            {
                var matchingNew = newChanges.First(c => c.Position == newLine.Position);

                //Actual new lines
                if (matchingNew == null)
                {
                    var insertIndex = allDiffPieces.IndexOf(allDiffPieces.First(c => c.Item1.Position == newLine.Position));
                    allDiffPieces.Insert(insertIndex, (newLine, false));
                }
            }

            totalChanges = allDiffPieces.Count;
            lblDiffNumber.Content = $"{ChangeIndex + 1:N0}/{totalChanges:N0}";

            Busy = false;
        }

        public static string AttemptReadFileContents(string FilePath, IPackedFileReader FileReader, bool SupportHex = true)
        {
            string resultText = "";
            byte[] byteContents = FileReader.ReadFileBinary(FilePath);

            bool TryFormat(ref string Result, Func<string> fnAttemptFormat)
            {
                try
                {
                    Result = fnAttemptFormat();
                    return true;
                }
                catch { }
                return false;
            }

            if (CryEngine.validExtensions.Contains(System.IO.Path.GetExtension(FilePath)))
            {
                try
                {
                    //This is non-optimal because of a potential double-read of the binary data
                    //As well as a file not existing but us saying it should.
                    var cryEngine = new CryEngine(FilePath, "", FileReader.HasEntry, FileReader.ReadFileBinary);
                    cryEngine.ProcessCryengineFiles();
                    foreach (var chunk in cryEngine.Chunks)
                    {
                        if (chunk is ChunkBinaryXmlData_3 xmlChunk)
                        {
                            byteContents = xmlChunk.Data;
                        }
                    }
                }
                catch (Exception ex)
                {
                }
            }

            if (TryFormat(ref resultText, () =>
            {
                using (var memStream = new MemoryStream(byteContents))
                {
                    return Utilities.FormatXML(CryXmlSerializer.ReadStream(memStream));
                }
            }))
                return resultText;

            var fullTextContents = Encoding.UTF8.GetString(byteContents);

            //if (System.IO.Path.GetExtension(FilePath) == ".json" || ForceCheckJSON)
                if (TryFormat(ref resultText, () => Utilities.FormatJSON(fullTextContents)))
                    return resultText;

            //if (System.IO.Path.GetExtension(FilePath) == ".xml" || ForceCheckXML)
                if (TryFormat(ref resultText, () => Utilities.FormatXML(fullTextContents)))
                    return resultText;

            if (ExtractBrowser.TextExtensions.Contains(System.IO.Path.GetExtension(FilePath)))
                if (TryFormat(ref resultText, () => fullTextContents))
                    return resultText;

            if (SupportHex && TryFormat(ref resultText, () => Utilities.FormatHexdump(byteContents)))
                return resultText;

            return null;
        }

        private void SaveLeft_Click(object sender, RoutedEventArgs e)
        {
            var Item = ((e.Source as MenuItem)?.Parent as ContextMenu)?.PlacementTarget as TextBlock;
            WriteFiles(Item?.DataContext as DiffResult, true, false);
        }

        private void SaveRight_Click(object sender, RoutedEventArgs e)
        {
            var Item = ((e.Source as MenuItem)?.Parent as ContextMenu)?.PlacementTarget as TextBlock;
            WriteFiles(Item?.DataContext as DiffResult, false, true);
        }

        private void SaveBoth_Click(object sender, RoutedEventArgs e)
        {
            var Item = ((e.Source as MenuItem)?.Parent as ContextMenu)?.PlacementTarget as TextBlock;
            WriteFiles(Item?.DataContext as DiffResult, true, true);
        }

        private void WriteFiles(DiffResult result, bool Old, bool New)
        {
            var diffResult = result;

            if (diffResult == null) return;

            if (diffResult.Diff.OldSource is SOC_Archive socArchive) ;
            else if (diffResult.Diff.OldSource is DFDatabase oldDB) ;
            else
                return;

            var baseName = System.IO.Path.GetFileNameWithoutExtension(diffResult.Name);
            var extension = System.IO.Path.GetExtension(diffResult.Name);

            switch(diffResult.Diff.Source)
            {
                case DiffSource.Archive:
                    if (Old)
                    {
                        var textData = AttemptReadFileContents(diffResult.FilePath, diffResult.Diff.OldSource, false);
                        var oldData = (textData != null) ?
                            Encoding.UTF8.GetBytes(textData) :
                            diffResult.Diff.OldSource.ReadFileBinary(diffResult.FilePath);
                        SaveFile(oldData, System.IO.Path.ChangeExtension($"Old_{baseName}", extension));
                    }
                    if (New)
                    {
                        var textData = AttemptReadFileContents(diffResult.FilePath, diffResult.Diff.NewSource, false);
                        var newData = (textData != null) ?
                            Encoding.UTF8.GetBytes(textData) :
                            diffResult.Diff.NewSource.ReadFileBinary(diffResult.FilePath);
                        SaveFile(newData, System.IO.Path.ChangeExtension($"New_{baseName}", extension));
                    }
                    break;
                case DiffSource.DCB:
                    if (Old)
                    {
                        var textData = Utilities.FormatXML(oldDCBElements[diffResult.RecordGUID.Value].OuterXml);
                        SaveFile(Encoding.UTF8.GetBytes(textData), 
                            System.IO.Path.ChangeExtension($"Old_{baseName}", extension));
                    }
                    if (New)
                    {
                        var textData = Utilities.FormatXML(newDCBElements[diffResult.RecordGUID.Value].OuterXml);
                        SaveFile(Encoding.UTF8.GetBytes(textData), 
                            System.IO.Path.ChangeExtension($"New_{baseName}", extension));
                    }
                    break;
            }
        }

        private void SaveFile(byte[] Contents, string InitialName)
        {
            var extension = System.IO.Path.GetExtension(InitialName);

            SaveFileDialog sfd = new SaveFileDialog()
            {
                FileName = InitialName,
                Filter = $"{extension} Files (*{extension})|{extension}",
                OverwritePrompt = true,
                InitialDirectory = Properties.Settings.Default.LastExportDirectory
            };

            if (sfd.ShowDialog() ?? false)
            {
                Properties.Settings.Default.LastExportDirectory = System.IO.Path.GetDirectoryName(sfd.FileName);
                Properties.Settings.Default.Save();

                File.WriteAllBytes(sfd.FileName, Contents);
            }
        }

        private void DiffNavigate_Click(object sender, RoutedEventArgs e)
        {
            var tag = (sender as Button)?.Tag;

            switch (tag)
            {
                case "First":
                    ChangeIndex = 0;
                    break;
                case "-1":
                    ChangeIndex -= 1;
                    break;
                case "1":
                    ChangeIndex += 1;
                    break;
                case "Last":
                    ChangeIndex = totalChanges - 1;
                    break;
                default:
                    return;
            }

            ChangeIndex = Math.Min(totalChanges - 1, Math.Max(0, ChangeIndex));

            lblDiffNumber.Content = $"{ChangeIndex + 1:N0}/{totalChanges:N0}";

            if (totalChanges > 0)
            {
                var change = allDiffPieces[ChangeIndex];
                diffViewer.GoTo(change.Item1, change.Item2);
            }
        }
    }
}
