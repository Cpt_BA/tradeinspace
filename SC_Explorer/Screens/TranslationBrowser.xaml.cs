﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TradeInSpace.Explore.DataForge;

namespace SC_Explorer.Screens
{
    /// <summary>
    /// Interaction logic for TranslationBrowser.xaml
    /// </summary>
    public partial class TranslationBrowser : UserControl, INotifyPropertyChanged
    {
        public static readonly DependencyProperty GameDatabaseProperty = DependencyProperty.Register(
            "GameDatabase",
            typeof(DFDatabase),
            typeof(TranslationBrowser), new PropertyMetadata((d, dc) =>
            {
                var gameDB = dc.NewValue as DFDatabase;
                var trBrowser = d as TranslationBrowser;
                if (gameDB != null)
                {
                    trBrowser.Localizations = gameDB.Localizations.ToDictionary(kvp => kvp.Key, kvp => kvp.Value);
                    trBrowser.RefreshLocalizationFilter();
                }
            }));
        public event PropertyChangedEventHandler PropertyChanged;

        public TranslationBrowser()
        {
            FilteredLocalizations = new List<KeyValuePair<string, string>>();

            InitializeComponent();
        }

        public DFDatabase GameDatabase
        {
            get { return (DFDatabase)GetValue(GameDatabaseProperty); }
            set { SetValue(GameDatabaseProperty, value); }
        }

        public Dictionary<string, string> Localizations{ get; set; }
        public List<KeyValuePair<string, string>> FilteredLocalizations { get; set; }

        private void txtSearch_TextChanged(object sender, TextChangedEventArgs e)
        {
            RefreshLocalizationFilter();
        }

        internal void RefreshLocalizationFilter()
        {
            if (string.IsNullOrEmpty(txtSearch.Text))
            {
                FilteredLocalizations = Localizations.ToList();
            }
            else
            {
                FilteredLocalizations.Clear();

                foreach (var item in Localizations)
                {
                    if (item.Key.Contains(txtSearch.Text, StringComparison.OrdinalIgnoreCase) ||
                       item.Value.Contains(txtSearch.Text, StringComparison.OrdinalIgnoreCase))
                    {
                        FilteredLocalizations.Add(item);
                    }
                }
            }

            OnPropertyChanged(nameof(FilteredLocalizations));
            tblEntries.Items.Refresh(); //Must do otherwise old list sticks until interacted with
        }

        protected void OnPropertyChanged([CallerMemberName] string name = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }
    }
}
