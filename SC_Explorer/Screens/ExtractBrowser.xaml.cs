﻿using CgfConverter;
using CgfConverter.CryEngineCore;
using ICSharpCode.AvalonEdit.Highlighting;
using ICSharpCode.SharpZipLib.Zip;
using Microsoft.Win32;
using Serilog;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml;
using TradeInSpace.Explore.DataForge;
using TradeInSpace.Explore.Socpak;
using TradeInSpace.Parse;
using unforge;

namespace SC_Explorer.Screens
{
    /// <summary>
    /// Interaction logic for ExtractBrowser.xaml
    /// </summary>
    public partial class ExtractBrowser : UserControl, INotifyPropertyChanged
    {
        public static readonly DependencyProperty GameDatabaseProperty = DependencyProperty.Register(
            nameof(GameDatabase),
            typeof(DFDatabase),
            typeof(ExtractBrowser), new PropertyMetadata((d, dc) =>
            {
                var gameDB = dc.NewValue as DFDatabase;
                if (gameDB != null)
                {
                }
            }));
        public static readonly DependencyProperty ArchiveItemsProperty = DependencyProperty.Register(
            nameof(ArchiveItems),
            typeof(List<ExtractItemViewModel>),
            typeof(ExtractBrowser));
        public static readonly DependencyProperty ArchiveChildItemsProperty = DependencyProperty.Register(
            nameof(ArchiveChildItems),
            typeof(List<ExtractItemViewModel>),
            typeof(ExtractBrowser));
        public static readonly DependencyProperty PackedChildItemsProperty = DependencyProperty.Register(
            nameof(PackedChildItems),
            typeof(List<ExtractItemViewModel>),
            typeof(ExtractBrowser));

        private bool archiveSelectionHasChildren = false;
        private bool archiveSelectionHasPacked = false;
        private SOC_Archive selectedArchive = null;
        private CryEngine selectedPacked = null;

        public static string[] TextExtensions = new string[]
        {
            ".txt",
            ".cfg",
            ".ini",
            ".ext"
        };
        public static string[] ArchiveExtensions = new string[]
        {
            ".socpak",
            ".pak"
        };
        public static string[] ImageExtensions = new string[]
        {
            ".ttf",
            ".dds"
        };
        private bool busy;
        private bool data_loaded;
        private float progress;
        private GCHandle loaded_image_data;

        public event PropertyChangedEventHandler PropertyChanged;
        private void RaisePropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public ExtractBrowser()
        {
            InitializeComponent();
        }

        public bool ArchiveSelectionHasChildren
        {
            get => archiveSelectionHasChildren;
            set
            {
                archiveSelectionHasChildren = value;
                RaisePropertyChanged(nameof(ArchiveSelectionHasChildren));
            }
        }

        public bool ArchiveSelectionHasPacked
        {
            get => archiveSelectionHasPacked;
            set
            {
                archiveSelectionHasPacked = value;
                RaisePropertyChanged(nameof(ArchiveSelectionHasPacked));
            }
        }

        public DFDatabase GameDatabase
        {
            get { return (DFDatabase)GetValue(GameDatabaseProperty); }
            set { SetValue(GameDatabaseProperty, value); }
        }

        public List<ExtractItemViewModel> ArchiveItems
        {
            get { return (List<ExtractItemViewModel>)GetValue(ArchiveItemsProperty); }
            set
            {
                SetValue(ArchiveItemsProperty, value);
                RaisePropertyChanged(nameof(ArchiveItems));
            }
        }

        public List<ExtractItemViewModel> ArchiveChildItems
        {
            get { return (List<ExtractItemViewModel>)GetValue(ArchiveChildItemsProperty); }
            set
            {
                SetValue(ArchiveChildItemsProperty, value);
                RaisePropertyChanged(nameof(ArchiveChildItems));
            }
        }

        public List<ExtractItemViewModel> PackedChildItems
        {
            get { return (List<ExtractItemViewModel>)GetValue(PackedChildItemsProperty); }
            set
            {
                SetValue(PackedChildItemsProperty, value);
                RaisePropertyChanged(nameof(PackedChildItems));
            }
        }

        public bool Busy
        {
            get => busy;
            set
            {
                Cursor = value ? Cursors.Wait : Cursors.Arrow;
                busy = value;
                RaisePropertyChanged(nameof(Busy));
            }
        }

        public bool DataLoaded
        {
            get => data_loaded;
            set
            {
                data_loaded = value;
                RaisePropertyChanged(nameof(DataLoaded));
            }
        }

        public float Progress
        {
            get => progress;
            set
            {
                progress = value;
                RaisePropertyChanged(nameof(Progress));
            }
        }

        private bool TryContentFormat(string Format, Func<string> AttemptCreate)
        {
            try
            {
                var result = AttemptCreate();

                if (string.IsNullOrEmpty(result))
                    return false;

                selectedFileContents.SyntaxHighlighting = HighlightingManager.Instance.GetDefinition(Format);
                selectedFileContents.Text = result;

                return true;
            }
            catch (Exception ex) { }
            return false;
        }

        private void ClearControls(bool KeepArchvie = false)
        {
            if (!KeepArchvie)
            {
                selectedArchive = null;
                ArchiveSelectionHasChildren = false;
            }
            selectedPacked = null;
            ArchiveSelectionHasPacked = false;
            selectedFileContents.Text = "";
        }

        private void treeArchiveItems_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            ClearControls();

            var selectedItem = treeArchiveItems.SelectedItem as ExtractItemViewModel;

            if (selectedItem == null) return;

            if (selectedItem.SourceEntry == null) return;

            if (ArchiveExtensions.Contains(System.IO.Path.GetExtension(selectedItem.Path).ToLower()))
            {
                try
                {
                    selectedArchive = SOC_Archive.LoadFromGameDBSocPak(GameDatabase, selectedItem.Path, false);
                }
                catch { }

                if (selectedArchive != null)
                {
                    //Add a dummy root so the user can export the entire archive in one go
                    var archiveRoot = ExtractItemViewModel.CreateDummy(selectedItem.Header.Replace('.', '_'));
                    var archiveChild = ConvertZipToTree(selectedArchive);
                    foreach (var child in archiveChild)
                        archiveRoot.Children.Add(child);

                    //Auto-expand only the first layer for inner archives
                    archiveRoot.IsExpanded = true;

                    /*
                    //Append the root name to the path of all child items so things export correctly
                    var flatChildren = TradeInSpace.Explore.Utilities.FlattenRecursive(archiveRoot, c => c.Children).ToList();
                    foreach (var child in flatChildren)
                        child.Path = System.IO.Path.Combine(archiveRoot.Header, child.Path);
                    */

                    ArchiveChildItems = new List<ExtractItemViewModel>(new[] { archiveRoot });
                    ArchiveSelectionHasChildren = true;

                    return;
                }
            }

            ShowFileContents(selectedItem.Path, selectedItem.SourceArchive);
        }

        private void treeInnerContents_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            ClearControls(true);

            var selectedItem = treeInnerContents.SelectedItem as ExtractItemViewModel;

            if (selectedItem == null) return;
            if (selectedItem.ItemType != ExtractItemType.Archive) return;

            ShowFileContents(selectedItem.Path, selectedItem.SourceArchive);
        }

        private void treePackedContents_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            var selectedItem = treePackedContents.SelectedItem as ExtractItemViewModel;
            var sourceChunk = selectedItem?.SourceEntry as Chunk;

            if (selectedItem == null) return;
            if (sourceChunk == null || selectedItem.ItemType != ExtractItemType.Chunk) return;

            byte[] chunkContent;

            if (sourceChunk is ChunkBinaryXmlData_3 cryChunkXML)
            {
                chunkContent = cryChunkXML.Data;
            }
            else
            {
                byte[] fullContent;

                if (selectedArchive != null)
                {
                    fullContent = selectedArchive.ReadFile(selectedPacked.InputFile);
                }
                else
                {
                    fullContent = GameDatabase.ReadPackedFileBinary(selectedPacked.InputFile);
                }

                chunkContent = fullContent
                    .Skip((int)sourceChunk.Offset)
                    .Take((int)sourceChunk.DataSize)
                    .ToArray();
            }

            //var textContents = Encoding.UTF8.GetString(fullContent);

            ShowFileContents("",
                () => null,
                () => chunkContent);
        }

        private void ShowFileContents(string Path, IPackedFileReader FileReader)
        {
            //Handle dds images. They come in a base file containing header information (*.dds)
            //as well as multiple LOD's (i think) in multiple files (*.dds.1, *.dds.2) to be concatendated


            //Need to do a Contains() here because differe image LOD's come with ascending extensions.
            //ie: texture.dds, texture.dds.1, texture.dds.2, etc...
            if (ImageExtensions.Any(ie => Path.Contains(ie, StringComparison.OrdinalIgnoreCase)))
            {
                try
                {
                    var baseName = Path;
                    int fileIndex = 0;

                    if (System.IO.Path.GetExtension(baseName) != ".dds")
                    {
                        //Turn "texture.dds.1" into just "texture.dds"
                        baseName = baseName.Substring(0, baseName.LastIndexOf('.'));
                        fileIndex = int.Parse(Path.Substring(Path.LastIndexOf('.') + 1));
                    }

                    BitmapSource finalImage = null;

                    Log.Information($"Loading DDS File: {Path}");

                    Log.Information($"Reading DDS Header: {baseName}");
                    var headerData = FileReader.ReadFileBinary(baseName);

                    var header = DDSHeader.Read(headerData);
                    PixelFormat pixelFormat = PixelFormats.Default;
                    int width = header.Width, height = header.Height;

                    byte[] data = (fileIndex == 0) ? header.AdditionalData : FileReader.ReadFileBinary(Path);
                    byte[] decodedData = DDSParser.DecodeDDSData(header, fileIndex, data, out int bitsPerPixel, out width, out height);

                    switch (bitsPerPixel)
                    {
                        case 8:
                            pixelFormat = PixelFormats.Gray8;
                            break;
                        case 16:
                            pixelFormat = PixelFormats.Gray16;
                            break;
                        case 24:
                            pixelFormat = PixelFormats.Bgr24;
                            break;
                        case 32:
                            pixelFormat = PixelFormats.Bgra32;
                            break;
                        default:
                            break;
                    }


                    int stride = width * (bitsPerPixel / 8);
                    finalImage = Utilities.WpfSource(decodedData, width, height, stride, pixelFormat);

                    selectedFileImage.Source = finalImage;

                    selectedFileContents.Visibility = Visibility.Hidden;
                    selectedFileImage.Visibility = Visibility.Visible;

                    return;
                }
                catch (Exception ex)
                {

                }
            }

            selectedFileContents.Visibility = Visibility.Visible;
            selectedFileImage.Visibility = Visibility.Hidden;

            //Otherwise handle generically

            try
            {
                ShowFileContents(Path, () => FileReader.ReadCryEngineFile(Path), () => FileReader.ReadFileBinary(Path));
            }
            catch (Exception ex)
            {
                Log.Error(ex, $"Error loading file fallback: {Path} - {ex.Message}");
            }
        }

        private void ShowFileContents(string Name,
            Func<CryEngine> fnAttemptReadPacked,
            Func<byte[]> fnReadBinary)
        {
            if (CryEngine.validExtensions.Contains(System.IO.Path.GetExtension(Name).ToLower()))
            {
                try
                {
                    var packed = fnAttemptReadPacked();

                    if (packed != null)
                    {
                        packed.ProcessCryengineFiles();

                        selectedPacked = packed;
                        PackedChildItems = packed.Chunks.Select(ExtractItemViewModel.CreateFromChunk).ToList();
                        ArchiveSelectionHasPacked = true;

                        GC.Collect();
                        return;
                    }
                }
                catch
                {
                }
            }

            var byteContents = fnReadBinary();

            if (TryContentFormat("XML", () =>
                {
                    using (var memStream = new MemoryStream(byteContents))
                    {
                        return Utilities.FormatXML(CryXmlSerializer.ReadStream(memStream));
                    }
                }))
                return;

            var textContents = Encoding.UTF8.GetString(byteContents);

            //if (System.IO.Path.GetExtension(Name) == ".json")
                if (TryContentFormat("Json", () => Utilities.FormatJSON(textContents)))
                    return;

            //if(System.IO.Path.GetExtension(Name) == ".xml")
                if (TryContentFormat("XML", () => Utilities.FormatXML(textContents)))
                    return;

            if (TextExtensions.Contains(System.IO.Path.GetExtension(Name).ToLower()))
                if (TryContentFormat("TXT", () => textContents))
                    return;

            if (TryContentFormat("Markdown", () => Utilities.FormatHexdump(byteContents)))
                return;
        }

        private static List<ExtractItemViewModel> ConvertZipToTree(IPackedFileReader fileReader)
        {
            var flatTreeItems = new List<ExtractItemViewModel>();
            foreach (ZipEntry entry in fileReader.ZipChildren())
                flatTreeItems.Add(ExtractItemViewModel.CreateFromArchive(fileReader, entry));

            var treeRoot = TradeInSpace.Explore.Utilities.PathToTree(
                flatTreeItems,
                t => t.Path,
                t => t.Children,
                p => ExtractItemViewModel.CreateDummy(p)
                ).ToList();

            return treeRoot;
        }

        private async void LoadData_Click(object sender, RoutedEventArgs e)
        {
            if (Busy) return;
            
            Busy = true;
            lblExportProgress.Content = "Loading...";

            IPackedFileReader gameDB = GameDatabase;

            var zipTreeRoot = await Task.Run(() => ConvertZipToTree(gameDB));

            SetValue(ArchiveItemsProperty, zipTreeRoot);

            lblExportProgress.Content = $"{gameDB.ZipChildren().Count():N0} Files loaded.";
            DataLoaded = true;
            Busy = false;
        }

        private void SaveFile_Click(object sender, RoutedEventArgs e)
        {
            if(Busy) return;

            var item = ((e.Source as MenuItem)?.Parent as ContextMenu)?.PlacementTarget as TextBlock;
            var itemModel = item?.DataContext as ExtractItemViewModel;

            if (itemModel == null) return;

            var InitialName = System.IO.Path.GetFileName(string.IsNullOrEmpty(itemModel.Path) ? "outfile" : itemModel.Path );
            var extension = System.IO.Path.GetExtension(InitialName).ToLower();

            SaveFileDialog sfd = new SaveFileDialog()
            {
                FileName = InitialName,
                Filter = $"{extension} Files (*{extension})|{extension}",
                OverwritePrompt = true,
                InitialDirectory = Properties.Settings.Default.LastExportDirectory
            };

            if (sfd.ShowDialog() ?? false)
            {
                Properties.Settings.Default.LastExportDirectory = System.IO.Path.GetDirectoryName(sfd.FileName);
                Properties.Settings.Default.Save();

                SaveFile(itemModel, sfd.FileName);
            }
        }

        private async void SaveFolderFlat_Click(object sender, RoutedEventArgs e)
        {
            var item = ((e.Source as MenuItem)?.Parent as ContextMenu)?.PlacementTarget as TextBlock;
            var itemModel = item?.DataContext as ExtractItemViewModel;

            if (itemModel == null) return;

            await ExportFolder(itemModel, false);
        }

        private async void SaveFolderFull_Click(object sender, RoutedEventArgs e)
        {
            var item = ((e.Source as MenuItem)?.Parent as ContextMenu)?.PlacementTarget as TextBlock;
            var itemModel = item?.DataContext as ExtractItemViewModel;

            if (itemModel == null) return;

            await ExportFolder(itemModel, true);
        }

        private void SaveFile(ExtractItemViewModel Item, string SaveName)
        {
            byte[] contentBytes = null;

            switch (Item.ItemType)
            {
                case ExtractItemType.Dummy:
                    return;
                case ExtractItemType.Archive:
                    var contentsString = DiffBrowser.AttemptReadFileContents(Item.Path, Item.SourceArchive, false);
                    if (contentsString != null) contentBytes = Encoding.UTF8.GetBytes(contentsString);
                    break;
                case ExtractItemType.Chunk:
                    var sourceChunk = Item.SourceEntry as Chunk;

                    switch(sourceChunk)
                    {
                        case ChunkJsonData jsonData:
                            contentBytes = Encoding.UTF8.GetBytes(jsonData.JSONData);
                            break;
                        case ChunkBinaryXmlData xmlData:
                            contentBytes = xmlData.Data;
                            break;
                        default:
                            byte[] fullContent = Item.SourceArchive.ReadFileBinary(Item.Path);

                            contentBytes = fullContent
                                .Skip((int)sourceChunk.Offset)
                                .Take((int)sourceChunk.DataSize)
                                .ToArray();
                            break;
                    }
                    break;
            }

            if (contentBytes == null)
                contentBytes = Item.SourceArchive.ReadFileBinary(Item.Path);

            File.WriteAllBytes(SaveName, contentBytes);
        }

        private async Task ExportFolder(ExtractItemViewModel FolderItem, bool WithStructure)
        {

            var flatItems = TradeInSpace.Explore.Utilities.FlattenRecursive(FolderItem, i => i.Children).ToList();
            var flatCount = flatItems.Count;
            var totalBytes = flatItems
                .Where(i => i.ItemType == ExtractItemType.Archive)
                .Sum(i => (i.SourceEntry as ZipEntry).Size);

            if(flatCount > 100 || totalBytes > 1024L * 1024L * 1024L)
            {
                var humanBytes = TradeInSpace.Explore.Utilities.FormatBytes(totalBytes);
                var confirmResult = MessageBox.Show(
                    $"You are about to export {flatCount:N0} files, totaling {humanBytes}.\r\nContinue?", 
                    $"Export {flatCount:N0} files?", 
                    MessageBoxButton.YesNo);
                if (confirmResult == MessageBoxResult.No)
                    return;
            }

            System.Windows.Forms.FolderBrowserDialog fbd = new System.Windows.Forms.FolderBrowserDialog()
            {
                Description = "Select folder to export all files to.",
                ShowNewFolderButton = true,
                SelectedPath = Properties.Settings.Default.LastExportDirectory
            };

            var result = fbd.ShowDialog();

            if (result == System.Windows.Forms.DialogResult.Cancel)
                return;

            var rootExport = fbd.SelectedPath;
            
            //Save settings
            Properties.Settings.Default.LastExportDirectory = rootExport;
            Properties.Settings.Default.Save();

            //Append the invisible root folder to the output path when exporting a socpak contianer root
            if (selectedArchive != null && ArchiveExtensions.Contains(System.IO.Path.GetExtension(FolderItem.Path.Replace('_', '.').ToLower())))
            {
                rootExport = System.IO.Path.Combine(rootExport, FolderItem.Header);
            }
            var count = 0;

            async Task ProcessChild(ExtractItemViewModel Item)
            {
                if (count % 10 == 0 || count == flatCount - 1)
                {
                    Progress = ((float)count) / flatCount * 100.0f;
                    lblExportProgress.Content = $"[{count}/{flatCount}] {Item.Header}";
                }

                if (Item.ItemType != ExtractItemType.Dummy)
                {
                    var exportPath = WithStructure ?
                        Item.Path :
                        System.IO.Path.GetFileName(Item.Path);
                    var targetPath = System.IO.Path.Combine(rootExport, exportPath);
                    var targetDirectory = System.IO.Path.GetDirectoryName(targetPath);

                    if (!Directory.Exists(targetDirectory))
                        Directory.CreateDirectory(targetDirectory);

                    await Task.Run(() =>
                        {
                            bool written = false;
                            for (int i = 0; i < 5 && !written; i++)
                            {
                                try
                                {
                                    SaveFile(Item, targetPath);
                                    written = true;
                                }
                                catch (Exception ex) { }
                            }
                        });
                }

                count++;

                foreach (var child in Item.Children)
                    await ProcessChild(child);
            }

            Busy = true;

            await ProcessChild(FolderItem);

            lblExportProgress.Content = "Done!";
            Busy = false;
            Progress = 0;
        }
    }

    public class ExtractItemViewModel : INotifyPropertyChanged
    {
        private bool isExpanded;

        public string Header { get; set; }
        public string Path { get; set; }
        public bool IsFile { get; set; }

        public IPackedFileReader SourceArchive { get; set; }
        public object SourceEntry { get; set; }

        public ExtractItemType ItemType { get; set; }

        public bool IsExpanded
        {
            get => isExpanded;
            set
            {
                isExpanded = value;
                RaisePropertyChanged(nameof(IsExpanded));
            }
        }

        public ObservableCollection<ExtractItemViewModel> Children { get; set; } =
            new ObservableCollection<ExtractItemViewModel>();


        public event PropertyChangedEventHandler PropertyChanged;
        private void RaisePropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public static ExtractItemViewModel CreateFromArchive(IPackedFileReader FileReader, ZipEntry Entry)
        {
            var Path = Entry.Name;

            return new ExtractItemViewModel()
            {
                ItemType = ExtractItemType.Archive,
                Header = System.IO.Path.GetFileName(Path),
                Path = Path,
                IsFile = true,
                SourceArchive = FileReader,
                SourceEntry = Entry
            };
        }

        public static ExtractItemViewModel CreateDummy(string Path)
        {
            return new ExtractItemViewModel()
            {
                ItemType = ExtractItemType.Dummy,
                Header = System.IO.Path.GetFileName(Path),
                Path = Path,
                IsFile = false,
                SourceArchive = null,
                SourceEntry = null
            };
        }

        public static ExtractItemViewModel CreateFromChunk(Chunk cryChunk)
        {
            var header = $"{cryChunk.ChunkType} [{cryChunk.Offset:X8}:{cryChunk.Offset + cryChunk.DataSize:X8}]";

            return new ExtractItemViewModel()
            {
                ItemType = ExtractItemType.Chunk,
                Header = header,
                Path = "",
                IsFile = true,
                SourceArchive = null,
                SourceEntry = cryChunk
            };
        }

        internal void InvalidateChildren()
        {
            RaisePropertyChanged(nameof(Children));
        }
    }

    public enum ExtractItemType
    {
        Dummy,
        Archive,
        Chunk
    }
}
