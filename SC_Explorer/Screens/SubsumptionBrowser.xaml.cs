﻿using SC_Explorer.ViewModels;
using Svg;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Drawing;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TradeInSpace.Explore.DataForge;
using TradeInSpace.Explore.Subsumption;
using System.IO;
using TradeInSpace.Explore.Socpak;
using TradeInSpace.Export;

namespace SC_Explorer.Screens
{
    /// <summary>
    /// Interaction logic for SubsumptionBrowser.xaml
    /// </summary>
    public partial class SubsumptionBrowser : UserControl, INotifyPropertyChanged
    {
        public static readonly DependencyProperty GameDatabaseProperty = DependencyProperty.Register(
            "GameDatabase",
            typeof(DFDatabase),
            typeof(SubsumptionBrowser), new PropertyMetadata((d, dc) =>
            {
                var parent = d as SubsumptionBrowser;
                var gameDB = dc.NewValue as DFDatabase;
                if (gameDB != null)
                {
                    //gameDB.Subsumption.Load();
                    //gameDB.Subsumption.LoadAllChildren();

                    if (gameDB.Subsumption.Systems?.Count() > 0)
                    {
                        parent.SubsumptionNodes.Add(new SubsumptionChildViewModel(gameDB.Subsumption.Systems.First(), gameDB));
                    }
                }
            }));

        public ObservableCollection<HierarchicalItem> SubsumptionNodes { get; set; } = new ObservableCollection<HierarchicalItem>();

        public SubsumptionBrowser()
        {
            InitializeComponent();
        }

        public DFDatabase GameDatabase
        {
            get { return (DFDatabase)GetValue(GameDatabaseProperty); }
            set { SetValue(GameDatabaseProperty, value); }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }


        //Test render to SVG
        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            _DoRender(false);
        }

        private void MenuItem_Click_1(object sender, RoutedEventArgs e)
        {
            _DoRender(true);
        }

        private void _DoRender(bool IncludeEntities = false)
        {
            var selectedNode = (_SubsumptionTree.SelectedItem as SubsumptionChildViewModel)?.Item?.SOC_Entry;

            if (selectedNode == null)
                return;

            try
            {
                //GameDatabase.Subsumption.LoadFrom(selectedNode, PreloadDetails: true, PreloadEntities: true);
                selectedNode.CalculateGlobalPositions();

                var svgXY = SVGOrbitExporter.GenerateSVG(selectedNode, v => v.X, v => v.Y, IncludeEntities);
                var svgXZ = SVGOrbitExporter.GenerateSVG(selectedNode, v => v.X, v => v.Z, IncludeEntities);
                var svgYZ = SVGOrbitExporter.GenerateSVG(selectedNode, v => v.Y, v => v.Z, IncludeEntities);

                File.WriteAllText("test_file_xy.svg", svgXY);
                File.WriteAllText("test_file_xz.svg", svgXZ);
                File.WriteAllText("test_file_yz.svg", svgYZ);

                var xyWindow = new MapDisplayWindow(svgXY);
                xyWindow.Show();

                var xzWindow = new MapDisplayWindow(svgXZ);
                xzWindow.Show();

                var yzWindow = new MapDisplayWindow(svgYZ);
                yzWindow.Show();
            }
            catch (Exception ex)
            {

            }
        }
    }
}
