﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TradeInSpace.Explore;
using TradeInSpace.Explore.DataForge;
using TradeInSpace.Explore.SC.Data;

namespace SC_Explorer.Screens
{
    /// <summary>
    /// Interaction logic for DataforgeBrowser.xaml
    /// </summary>
    public partial class DataforgeBrowser : UserControl
    {
        public static readonly DependencyProperty GameDatabaseProperty = DependencyProperty.Register(
            "GameDatabase",
            typeof(DFDatabase),
            typeof(DataforgeBrowser), new PropertyMetadata((d, dc) =>
            {
                var ents = dc.NewValue as DFDatabase;
                if (ents != null)
                {
                    d.SetValue(EntryTypesProperty,
                        ents.Entries
                        .GroupBy(c => c.Type)
                        .ToDictionary(c => c.Key, c => c.ToList()));
                }
            }));
        public static readonly DependencyProperty EntryTypesProperty = DependencyProperty.Register(
            "EntryTypes",
            typeof(Dictionary<string, List<DFEntry>>),
            typeof(DataforgeBrowser));


        public DataforgeBrowser()
        {
            InitializeComponent();
        }

        public DFDatabase GameDatabase
        {
            get { return (DFDatabase)GetValue(GameDatabaseProperty); }
            set { SetValue(GameDatabaseProperty, value); }
        }

        public Dictionary<string, List<DFEntry>> EntryTypes
        {
            get { return (Dictionary<string, List<DFEntry>>)GetValue(EntryTypesProperty); }
            set { SetValue(EntryTypesProperty, value); }
        }

        private void gd_EntryGroup_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var selectedItem = gd_EntryGroup.SelectedItem as DFEntry;

            if(selectedItem != null)
            {
                selectedDFRecordDetails.Text = Utilities.FormatXML(selectedItem.Element.OuterXml);

                if(Guid.TryParse(selectedItem["entityClass"], out Guid entityClass))
                {
                    var classEnt = GameDatabase.GetEntity(entityClass);
                    selectedDFRecordClassDetails.Text = Utilities.FormatXML(classEnt.Element.OuterXml);
                }
                else
                {
                    selectedDFRecordClassDetails.Text = "";
                }
            }
            else
            {
                selectedDFRecordDetails.Text = "";
                selectedDFRecordClassDetails.Text = "";
            }
        }

        private void Entries_Filter(object sender, FilterEventArgs e)
        {
            if (string.IsNullOrEmpty(Entry_Search.Text))
                e.Accepted = true;
            else
            {
                if(e.Item is KeyValuePair<string, List<DFEntry>> entry)
                {
                    e.Accepted = entry.Key.IndexOf(Entry_Search.Text, StringComparison.OrdinalIgnoreCase) >= 0;
                }
            }
        }

        private void SelectedEntries_Filter(object sender, FilterEventArgs e)
        {
            if (string.IsNullOrEmpty(SelectedEntry_Search.Text))
                e.Accepted = true;
            else
            {
                if(e.Item is DFEntry entry)
                {
                    bool idMatch = entry.ID.ToString().IndexOf(SelectedEntry_Search.Text, StringComparison.OrdinalIgnoreCase) >= 0;
                    bool nameMatch = entry.InstanceName.IndexOf(SelectedEntry_Search.Text, StringComparison.OrdinalIgnoreCase) >= 0;
                    e.Accepted = idMatch || nameMatch;
                }
            }
        }

        private void Entry_Search_TextChanged(object sender, TextChangedEventArgs e)
        {
            var cvs = this.FindResource("sortedEntries") as CollectionViewSource;
            cvs.View.Refresh();
        }

        private void SelectedEntry_Search_TextChanged(object sender, TextChangedEventArgs e)
        {
            var cvs = this.FindResource("sortedSelectedEntries") as CollectionViewSource;
            cvs.View.Refresh();
        }
    }
}
