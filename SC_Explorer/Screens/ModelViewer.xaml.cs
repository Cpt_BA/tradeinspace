﻿using CgfConverter;
using ICSharpCode.SharpZipLib.Zip;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TradeInSpace.Explore;
using TradeInSpace.Explore.DataForge;

namespace SC_Explorer.Screens
{
    /// <summary>
    /// Interaction logic for ModelViewer.xaml
    /// </summary>
    public partial class ModelViewer : UserControl, INotifyPropertyChanged
    {
        public static readonly DependencyProperty GameDatabaseProperty = DependencyProperty.Register(
            "GameDatabase",
            typeof(DFDatabase),
            typeof(ModelViewer));
        public static readonly DependencyProperty ArchiveItemsProperty = DependencyProperty.Register(
            nameof(ArchiveItems),
            typeof(List<ExtractItemViewModel>),
            typeof(ModelViewer));

        public ModelViewer()
        {
            InitializeComponent();
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void RaisePropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public DFDatabase GameDatabase
        {
            get { return (DFDatabase)GetValue(GameDatabaseProperty); }
            set { SetValue(GameDatabaseProperty, value); }
        }

        public List<ExtractItemViewModel> ArchiveItems
        {
            get { return (List<ExtractItemViewModel>)GetValue(ArchiveItemsProperty); }
            set
            {
                SetValue(ArchiveItemsProperty, value);
                RaisePropertyChanged(nameof(ArchiveItems));
            }
        }

        private static List<ExtractItemViewModel> ConvertZipToTree(IPackedFileReader fileReader)
        {
            var flatTreeItems = new List<ExtractItemViewModel>();
            foreach (ZipEntry entry in fileReader.ZipChildren())
            {
                var ext = System.IO.Path.GetExtension(entry.Name);

                if (ext == ".cga" || ext == ".cgf")
                {
                    flatTreeItems.Add(ExtractItemViewModel.CreateFromArchive(fileReader, entry));
                }
            }

            var treeRoot = TradeInSpace.Explore.Utilities.PathToTree(
                flatTreeItems,
                t => t.Path,
                t => t.Children,
                p => ExtractItemViewModel.CreateDummy(p)
                ).ToList();

            return treeRoot;
        }

        private void UserControl_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if(((bool)e.NewValue) == true && ArchiveItems == null)
            {
                ArchiveItems = ConvertZipToTree(GameDatabase);
            }
        }

        private void TreeView_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            var viewModel = e.NewValue as ExtractItemViewModel;

            if (viewModel != null && viewModel.IsFile)
            {
                CryEngine modelData = null;
                try
                {
                    modelData = GameDatabase.ReadPackedCGAFile(viewModel.Path);
                    modelData.ProcessCryengineFiles();

                    var deconstructed = SOC_Explorer.ExploreModel(GameDatabase, modelData, new SOC_Explorer.SearchConfig()
                    {
                        ApplyShipLoadout = true,
                        DynamicComponents = true,
                        IncludeModelReferences  = false,
                        LoadModelGeometry = true,
                        LoadModelStructure = true,
                        LoadEntities = true,
                        LoadReferences = true,
                        DummyEntries = true,
                        Recursive = true,
                    });

                    sceneRoot.Children.Clear();
                    sceneRoot.Children.Add(BuildViewportObject(deconstructed));
                }
                catch (Exception ex)
                {

                }
            }
        }

        private Model3DGroup BuildViewportObject(SCHieracrhyNode Node)
        {
            Model3DGroup groupNode = new Model3DGroup();

            if(Node.GeometryData != null)
            {
                var model = new GeometryModel3D();

                var vectors = new Point3DCollection();
                var indexes = new Int32Collection();

                foreach (var vert in Node.GeometryData.Vertexes)
                    vectors.Add(new Point3D(vert.Item1, vert.Item2, vert.Item3));

                foreach (var index in Node.GeometryData.VertexIndexes)
                {
                    indexes.Add((int)index.Item1);
                    indexes.Add((int)index.Item2);
                    indexes.Add((int)index.Item3);
                }

                model.Geometry = new MeshGeometry3D()
                {
                    Positions = vectors,
                    TriangleIndices = indexes
                };
                model.Material = this.FindResource("diffuse") as DiffuseMaterial;

                groupNode.Children.Add(model);
            }

            var nodeLocal = Node.LocalTransform;
            groupNode.Transform = new MatrixTransform3D(new Matrix3D(
                nodeLocal.M11, nodeLocal.M12, nodeLocal.M13, nodeLocal.M14,
                nodeLocal.M21, nodeLocal.M22, nodeLocal.M23, nodeLocal.M24,
                nodeLocal.M31, nodeLocal.M32, nodeLocal.M33, nodeLocal.M34,
                nodeLocal.M41, nodeLocal.M42, nodeLocal.M43, nodeLocal.M44));

            foreach(var child in Node.Children)
            {
                groupNode.Children.Add(BuildViewportObject(child));
            }

            return groupNode;
        }
    }
}
