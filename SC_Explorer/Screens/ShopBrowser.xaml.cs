﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TradeInSpace.Explore;
using TradeInSpace.Explore.DataForge;
using TradeInSpace.Explore.SC;
using TradeInSpace.Explore.SC.Components;
using TradeInSpace.Explore.SC.Loadout;
using TradeInSpace.Explore.Subsumption;

namespace SC_Explorer.Screens
{
    /// <summary>
    /// Interaction logic for ComponentExplorer.xaml
    /// </summary>
    public partial class ShopBrowser : UserControl
    {
        public static readonly DependencyProperty GameDatabaseProperty = DependencyProperty.Register(
            "GameDatabase",
            typeof(DFDatabase),
            typeof(ShopBrowser), new PropertyMetadata((d, dc) =>
            {
                var gameDB = dc.NewValue as DFDatabase;
                if (gameDB?.Subsumption?.ShopInventories?.RootShop?.ShopLayoutNodes != null)
                {
                    d.SetValue(ShopsProperty, gameDB.Subsumption.ShopInventories.RootShop.ShopLayoutNodes.ToList());
                }
            }));

        public static readonly DependencyProperty ShopsProperty = DependencyProperty.Register(
            "Shops",
            typeof(List<ShopLayoutNode>),
            typeof(ShopBrowser));

        public List<ShopInventoryNode> Inventory { get; set; }

        public ShopBrowser()
        {
            InitializeComponent();
        }

        public DFDatabase GameDatabase
        {
            get { return (DFDatabase)GetValue(GameDatabaseProperty); }
            set { SetValue(GameDatabaseProperty, value); }
        }

        private void txtSearchShop_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (GameDatabase?.Subsumption?.ShopInventories?.RootShop?.ShopLayoutNodes != null)
            {
                if (string.IsNullOrEmpty(txtSearchShop.Text))
                {
                    SetValue(ShopsProperty, GameDatabase.Subsumption.ShopInventories.RootShop.ShopLayoutNodes.ToList());
                }
                else
                {
                    var flatShops = TradeInSpace.Explore.Utilities.FlattenRecursive(GameDatabase.Subsumption.ShopInventories.RootShop, n => n.ShopLayoutNodes);

                    // Create pseudo-elements for UI when filtering to give it a flat structure
                    var keepShops = flatShops
                        .Where(node => node?.Name != null && node.Name.IndexOf(txtSearchShop.Text, StringComparison.OrdinalIgnoreCase) != -1)
                        .Select(sin => new ShopLayoutNode()
                        {
                            ID = sin.ID,
                            Index = sin.Index,
                            Name = sin.Name,
                            Parent = sin.Parent,
                            ShopInventoryNodes = sin.ShopInventoryNodes
                        })
                        .ToList();

                    SetValue(ShopsProperty, keepShops);
                }
            }
        }
    }
}
