﻿using Microsoft.Win32;
using Newtonsoft.Json.Linq;
using OSGeo.GDAL;
using OSGeo.OGR;
using Serilog;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TradeInSpace.Explore.DataForge;
using TradeInSpace.Explore.Socpak;
using TradeInSpace.Explore.Subsumption;
using TradeInSpace.Export.Planet;
using TradeInSpace.Models;

namespace SC_Explorer.Screens
{
    /// <summary>
    /// Interaction logic for PlanetRender.xaml
    /// </summary>
    public partial class PlanetRender : UserControl, INotifyPropertyChanged
    {
        public static readonly DependencyProperty GameDatabaseProperty = DependencyProperty.Register(
            nameof(GameDatabase),
            typeof(DFDatabase),
            typeof(PlanetRender), new PropertyMetadata((d, dc) =>
            {
                var gameDB = dc.NewValue as DFDatabase;
                var planetView = d as PlanetRender;
                if (gameDB != null && planetView != null)
                {
                    planetView.SetupRenderer();
                }
            }));

        public event PropertyChangedEventHandler PropertyChanged;
        private void RaisePropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        protected PlanetRenderer RenderManager;
        private bool GdalInit = false;

        public List<PlanetOption> Planets { get; set; } = null;
        public ImageSource PlanetRenderImage { get; set; } = null;
        public PlanetData SelectedPlanet { get; set; } = null;
        public RenderResult PlanetRenderOutput { get; set; } = null;

        public int LoadProgress { get; set; }
        public string LoadMessage { get; set; }
        public bool IsValidExport { get; set; } = false;
        public bool HasPlanetSelection { get; set; } = false;
        public Point PlanetSelectedPoint { get; set; } = new Point(0, 0);

        public bool HasSelection { get { return SelectedPlanet != null; } }
        public bool HasOutput { get { return PlanetRenderOutput != null; } }

        private int SelectedRenderSize { get => int.Parse((cmbRenderScale.SelectedItem as ComboBoxItem).Tag.ToString()); }
        private CoordinateMode SelectedCoordainateMode { get => (CoordinateMode)((cmbCoordinateSystem.SelectedItem as ComboBoxItem)?.Tag); }

        public PlanetRender()
        {
            InitializeComponent();
        }

        public DFDatabase GameDatabase
        {
            get { return (DFDatabase)GetValue(GameDatabaseProperty); }
            set { SetValue(GameDatabaseProperty, value); }
        }

        private void UpdateProgress(float Percent = 0, string Message = "")
        {
            LoadProgress = (int)(Percent * 100);
            LoadMessage = Message;
            RaisePropertyChanged(nameof(LoadProgress));
            RaisePropertyChanged(nameof(LoadMessage));
        }

        private void SetupRenderer()
        {
            RenderManager = new PlanetRenderer(GameDatabase);
            RenderManager.PreLoadProgress += RenderManager_PreLoadProgress;
            RenderManager.RenderProgress += RenderManager_RenderProgress;
        }

        private void RenderManager_PreLoadProgress(object sender, int progress, int maximum)
        {
            Dispatcher.Invoke(() =>
            {
                UpdateProgress(progress / (float)maximum, $"{progress} / {maximum} Ecosystem Preloaded");
            });
        }

        private void RenderManager_RenderProgress(object sender, int progress, int maximum)
        {
            Dispatcher.Invoke(() =>
            {
                UpdateProgress(progress / (float)maximum, $"{progress} / {maximum} Rows Rendered");
            });
        }

        //When control is shown/hidden from tab view
        private async void UserControl_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if(Planets == null && GameDatabase != null)
            {
                Cursor = Cursors.Wait;

                if (!GdalInit)
                {
                    GdalConfiguration.ConfigureGdal();
                    GdalConfiguration.ConfigureOgr();

                    Gdal.AllRegister();
                    Ogr.RegisterAll();

                    GdalInit = true;
                }

                UpdateProgress(Message: "Loading Subsumption Data");

                var gaedn = GameDatabase; //Need to take a reference up front for async

                await Task.Run(() =>
                {
                    if (gaedn.Subsumption == null)
                    {
                        gaedn.InitSubsumption();
                    }
                    gaedn.Subsumption.LoadAllSystems();
                    gaedn.Subsumption.LoadAllChildren(PreloadDetails: false, PreloadEntities: false, LoadChildren: true, PreloadArchives: false);
                });

                UpdateProgress(Message: "Searching for planets");

                Planets = new List<PlanetOption>();

                await Task.Run(() =>
                {
                    foreach (var systemEntry in gaedn.Subsumption.AllNodes)
                    {
                        if (systemEntry?.SOC_Entry?.Class == "OrbitingObjectContainer" &&
                            systemEntry.SOC_Entry.ArchivePath != null)
                        {
                            SOC_Archive ooc_archive = SOC_Archive.LoadFromSubsubmption(gaedn, systemEntry, gaedn.Subsumption.MasterRoot);
                            if (ooc_archive == null) continue;

                            ooc_archive.RootEntry.LoadFullDetails();
                            ooc_archive.RootEntry.LoadAllEntities(false, false, false);
                            if (ooc_archive.RootEntry.PlanetEntity == null) continue;

                            Planets.Add(new PlanetOption()
                            {
                                ID = ooc_archive.Name,
                                Name = ooc_archive.Name,
                                PlanetArchive = ooc_archive,
                            });
                        }
                    }
                });

                UpdateProgress(Message: $"{Planets.Count} planets found!");
                RaisePropertyChanged(nameof(Planets));

                Cursor = Cursors.Arrow;
            }
        }

        //When planet dropdown changes
        private async void cmbPlanets_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                var selectedPlanet = cmbPlanets.SelectedItem as PlanetOption;
                PlanetData planetData = null;

                if (selectedPlanet != null)
                {
                    var renderer = RenderManager;
                    var gamedb = GameDatabase;

                    await Task.Run(() =>
                    {
                        var planetJson = JObject.Parse(selectedPlanet.PlanetArchive.RootEntry.PlanetJSON);
                        planetData = PlanetData.CreateFromPLAJson(gamedb, selectedPlanet.ID, planetJson, renderer);
                    });
                }

                SelectedPlanet = planetData;
                PlanetRenderOutput = null;

                RaisePropertyChanged(nameof(SelectedPlanet));
                RaisePropertyChanged(nameof(HasSelection));
                RaisePropertyChanged(nameof(PlanetRenderOutput));

                ResetCamera_Click(null, null);

                //Coordainte grid. TODO: Better?
                canvasCoordinateGrid.Children.Clear();
                var step = (SelectedPlanet.TileCount / 180.0f) * 10.0f; //10 degree step

                Binding thicknessBinding = new Binding("ZoomInverse");
                for (float i = step; i < SelectedPlanet.TileCount; i += step)
                {
                    //Horizontal Line
                    var horizLine = new Line()
                    {
                        X1 = 0,
                        Y1 = i,
                        X2 = SelectedPlanet.TileCount * 2,
                        Y2 = i
                    };
                    var vertLine = new Line()
                    {
                        X1 = i * 2,
                        Y1 = 0,
                        X2 = i * 2,
                        Y2 = SelectedPlanet.TileCount
                    };

                    canvasCoordinateGrid.Children.Add(horizLine);
                    canvasCoordinateGrid.Children.Add(vertLine);
                    BindingOperations.SetBinding(horizLine, Line.StrokeThicknessProperty, thicknessBinding);
                    BindingOperations.SetBinding(vertLine, Line.StrokeThicknessProperty, thicknessBinding);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, $"Error with planet selection - {ex.Message}.");

                MessageBox.Show($"Error with selected planet - {ex.Message}\r\n\r\n{ex.StackTrace}");
            }
        }

        private void cmbRenderScale_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //Recalculate image validitiy
            UpdateCoordBounds();
        }

        private async void Button_Render_Click(object sender, RoutedEventArgs e)
        {
            Cursor = Cursors.Wait;

            try
            {
                var settings = new RenderSettings();

                settings.BlendEcosystems = chkEcoBlend.IsChecked.Value;
                settings.WIP_HillShading = chkHillShade.IsChecked.Value;

                settings.TileSize = SelectedRenderSize;
                settings.CoordinateMode = SelectedCoordainateMode;
                settings.SplatMapRenderArea = exportSplatRegion;
                settings.BinaryOceanMask = chkBinaryOceanMask.IsChecked.Value;
                settings.HeightmapOceanSurface = chkHeightmapOcean.IsChecked.Value;

                if (float.TryParse(numOceanDepth.Text, out var oceanDepth))
                {
                    settings.OceanDepth = oceanDepth;
                }
                else
                {
                    settings.OceanDepth = -2000;
                }

                Stopwatch timer = new Stopwatch();
                timer.Start();

                var renderer = RenderManager; //grab these here for async stuff
                var planet = SelectedPlanet;
                RenderResult result = await Task.Run(() => renderer.RenderScaledPlanet(planet, settings));

                timer.Stop();

                UpdateProgress(1.0f, $"Done in {timer.Elapsed.TotalSeconds:f2}s!");

                PlanetRenderOutput = result;
                RaisePropertyChanged(nameof(PlanetRenderOutput));
                RaisePropertyChanged(nameof(HasOutput));

                await UpdatePreview();

                renderImageNormalize.ScaleX = 1.0f / settings.TileSize;
                renderImageNormalize.ScaleY = 1.0f / settings.TileSize;

                renderImageTranslate.X = exportSplatRegion.X * 2;
                renderImageTranslate.Y = exportSplatRegion.Y;
            }
            catch (Exception ex)
            {
                Log.Error(ex, $"Error with planet render - {ex.Message}.");

                MessageBox.Show($"Error rendering planet - {ex.Message}\r\n\r\n{ex.StackTrace}");
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private async void cmbPreviewLayer_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            await UpdatePreview();
        }

        private async Task UpdatePreview()
        {
            var result = PlanetRenderOutput;

            if (result == null) return;

            int previewMode = cmbPreviewLayer.SelectedIndex;

            //TODO: Reduce copying
            System.Drawing.Bitmap resultBitmap = await Task.Run(() =>
            {
                var writeBitmap = new System.Drawing.Bitmap(result.TotalWidth, result.TotalHeight);

                for (int y = 0; y < result.TotalHeight; y++)
                {
                    for (int x = 0; x < result.TotalWidth; x++)
                    {
                        int resultA = 255, resultR = 0, resultG = 0, resultB = 0;

                        switch (previewMode)
                        {
                            case 0: //Surface color
                                var outPixel = result.GetResultPixel(x, y);
                                resultA = outPixel.A;
                                resultR = outPixel.R;
                                resultG = outPixel.G;
                                resultB = outPixel.B;
                                break;
                            case 1: //Elevation
                                {
                                    var outHeight = result.GetResultHeight(x, y);
                                    var outHeightByte = (((int)outHeight) + short.MaxValue) >> 8;
                                    resultR = resultG = resultB = outHeightByte;
                                }
                                break;
                            case 2: //Temperature
                                {
                                    var outClimate = result.GetResultClimate(x, y);
                                    resultR = resultG = resultB = outClimate.T;
                                    break;
                                }
                            case 3: //Humidity
                                {
                                    var outClimate = result.GetResultClimate(x, y);
                                    resultR = resultG = resultB = outClimate.H;
                                    break;
                                }
                            case 4: //Ocean Mask
                                {
                                    var outOcean = result.GetResultOceanMask(x, y);
                                    resultR = resultG = resultB = outOcean;
                                    break;
                                }
                        }

                        resultA = (int)MathHelper.ClampValue(resultA, 0, 255);
                        resultR = (int)MathHelper.ClampValue(resultR, 0, 255);
                        resultG = (int)MathHelper.ClampValue(resultG, 0, 255);
                        resultB = (int)MathHelper.ClampValue(resultB, 0, 255);

                        writeBitmap.SetPixel(x, y, System.Drawing.Color.FromArgb(resultA, resultR, resultG, resultB));
                    }
                }

                return writeBitmap;
            });

            PlanetRenderImage = BitmapToImageSource(resultBitmap);
            RaisePropertyChanged(nameof(PlanetRenderImage));
        }

        private BitmapImage BitmapToImageSource(System.Drawing.Bitmap bitmap)
        {
            using (MemoryStream memory = new MemoryStream())
            {
                bitmap.Save(memory, System.Drawing.Imaging.ImageFormat.Bmp);
                memory.Position = 0;
                BitmapImage bitmapimage = new BitmapImage();
                bitmapimage.BeginInit();
                bitmapimage.StreamSource = memory;
                bitmapimage.CacheOption = BitmapCacheOption.OnLoad;
                bitmapimage.EndInit();
                return bitmapimage;
            }
        }

        private string BuildFileName(string Extension)
        {
            var name = PlanetRenderOutput.PlanetData.BaseName;
            var coords = PlanetRenderOutput.CoordinateBounds;
            var layer = "";
            if(Extension != ".gpkg")
            {
                switch (cmbPreviewLayer.SelectedIndex)
                {
                    case 0: layer = "_surface"; break;
                    case 1: layer = "_heightmap"; break;
                    case 2: layer = "_temperature"; break;
                    case 3: layer = "_humidity"; break;
                    case 4: layer = "_oceanmask"; break;
                }
            }
            return System.IO.Path.ChangeExtension($"{name}_{coords.Left:n0}_{coords.Top:n0}_{coords.Right:n0}_{coords.Bottom:n0}{layer}", Extension);
        }

        private void Button_ExportPNG_Click(object sender, RoutedEventArgs e)
        {
            if (PlanetRenderOutput == null) return;
            try
            {
                var outDirectory = Properties.Settings.Default.LastRenderDirectory;
                SaveFileDialog sfd = new SaveFileDialog();
                sfd.InitialDirectory = outDirectory;
                sfd.FileName = System.IO.Path.Combine(outDirectory, BuildFileName(".png"));
                if (sfd.ShowDialog().Value)
                {
                    switch (cmbPreviewLayer.SelectedIndex)
                    {
                        case 0: //Surface
                            PlanetRenderOutput.ExportImageGDAL(PlanetRenderOutput.PlanetData.BaseName, "MEM", IncludeExport: true, "PNG", sfd.FileName);
                            break;
                        case 1: //Elevation
                            PlanetRenderOutput.ExportHeightmapGDAL(PlanetRenderOutput.PlanetData.BaseName, "MEM", IncludeExport: true, "PNG", sfd.FileName);
                            break;
                        case 2: //Temperature
                            PlanetRenderOutput.ExportTemperatureGDAL(PlanetRenderOutput.PlanetData.BaseName, "MEM", IncludeExport: true, "PNG", sfd.FileName);
                            break;
                        case 3: //Humidity
                            PlanetRenderOutput.ExportHumidityGDAL(PlanetRenderOutput.PlanetData.BaseName, "MEM", IncludeExport: true, "PNG", sfd.FileName);
                            break;
                        case 4: //OceanMask
                            PlanetRenderOutput.ExportOceanMaskGDAL(PlanetRenderOutput.PlanetData.BaseName, "MEM", IncludeExport: true, "PNG", sfd.FileName);
                            break;
                    }

                    Utilities.ExploreFile(sfd.FileName);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Error occured while saving: {ex.Message}\r\n{ex.StackTrace}");
            }
        }

        private void Button_ExportGeoTIFF_Click(object sender, RoutedEventArgs e)
        {
            if (PlanetRenderOutput == null) return;

            try
            {
                var outDirectory = Properties.Settings.Default.LastRenderDirectory;
                SaveFileDialog sfd = new SaveFileDialog();
                sfd.InitialDirectory = outDirectory;
                sfd.FileName = System.IO.Path.Combine(outDirectory, BuildFileName(".tiff"));
                if (sfd.ShowDialog().Value)
                {
                    switch (cmbPreviewLayer.SelectedIndex)
                    {
                        case 0: //Surface
                            PlanetRenderOutput.ExportImageGDAL(sfd.FileName, "GTiff", IncludeExport: false);
                            break;
                        case 1: //Elevation
                            PlanetRenderOutput.ExportHeightmapGDAL(sfd.FileName, "GTiff", IncludeExport: false);
                            break;
                        case 2: //Temperature
                            PlanetRenderOutput.ExportTemperatureGDAL(sfd.FileName, "GTiff", IncludeExport: false);
                            break;
                        case 3: //Humidity
                            PlanetRenderOutput.ExportHumidityGDAL(sfd.FileName, "GTiff", IncludeExport: false);
                            break;
                        case 4: //Ocean Mask
                            PlanetRenderOutput.ExportOceanMaskGDAL(sfd.FileName, "GTiff", IncludeExport: false);
                            break;
                    }

                    Utilities.ExploreFile(sfd.FileName);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Error occured while saving: {ex.Message}\r\n{ex.StackTrace}");
            }
        }

        private void Button_ExportGeoPKG_Click(object sender, RoutedEventArgs e)
        {
            if (PlanetRenderOutput == null) return;

            try
            {
                var outDirectory = Properties.Settings.Default.LastRenderDirectory;
                SaveFileDialog sfd = new SaveFileDialog();
                sfd.InitialDirectory = outDirectory;
                sfd.FileName = System.IO.Path.Combine(outDirectory, BuildFileName(".gpkg"));
                if (sfd.ShowDialog().Value)
                {
                    PlanetRenderOutput.RenderGeoPackage(sfd.FileName, SelectedPlanet.BaseName,
                        IncludeSurface: chkGeoLayerSurface.IsChecked.Value,
                        IncludeHeightmap: chkGeoLayerHeight.IsChecked.Value,
                        IncludeTemp: chkGeoLayerTemp.IsChecked.Value,
                        IncludeHumid: chkGeoLayerHumid.IsChecked.Value,
                        IncludeOceanMask: chkGeoLayerOcean.IsChecked.Value);

                    Utilities.ExploreFile(sfd.FileName);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Error occured while saving: {ex.Message}\r\n{ex.StackTrace}");
            }
        }

        #region Mouse Camer Code

        public float ZoomSpeed = 0.001f;
        public float ZoomFactor { get; set; } = 1.0f;
        public float ZoomInverse { get => 1.0f / ZoomFactor; }

        private bool IsDragging = false;
        private bool WasMoved = false;
        private Point DragStart = default;

        private void UpdateTransforms()
        {
            var oldZoom = ZoomFactor;
            ZoomFactor = MathHelper.ClampValue(ZoomFactor, 0.0001f, 1000f);

            renderScale.ScaleX = ZoomFactor;
            renderScale.ScaleY = ZoomFactor;
        }

        private void canvasMain_MouseMove(object sender, MouseEventArgs e)
        {
            var mousePos = e.GetPosition(canvasMain);
            var imagePos = canvasMain.TranslatePoint(mousePos, canvasInnerTranslate);

            if (IsDragging)
            {
                var mouseDelta = mousePos - DragStart;

                renderTranslate.X += (mouseDelta.X / ZoomFactor);
                renderTranslate.Y += (mouseDelta.Y / ZoomFactor);
                DragStart = mousePos;
                WasMoved = true;
            }

            if (HasPlanetSelection)
            {
                //Saved position is in transformed image space, but calcs are in screen-space
                imagePos = PlanetSelectedPoint;
                mousePos = canvasInnerTranslate.TranslatePoint(imagePos, canvasMain);
            }

            crosshairHoriz.X1 = 0;
            crosshairHoriz.X2 = planetOutline.ActualWidth;
            crosshairHoriz.Y1 = crosshairHoriz.Y2 = imagePos.Y;

            crosshairVert.Y1 = 0;
            crosshairVert.Y2 = planetOutline.ActualHeight;
            crosshairVert.X1 = crosshairVert.X2 = imagePos.X;

            if (HasOutput)
            {
                var offsetImagePos = canvasMain.TranslatePoint(mousePos, imgRenderMain);

                var nasacoords = SelectedCoordainateMode == CoordinateMode.NASA;
                var mouse_pos_img = new System.Drawing.RectangleF((float)imagePos.X / 2, (float)imagePos.Y, 0, 0);
                var coords = SelectedPlanet.SplatToCoordinates(mouse_pos_img, NASACoords: nasacoords);

                Canvas.SetLeft(canvasPopupDetails, mousePos.X + 20);
                Canvas.SetTop(canvasPopupDetails, mousePos.Y + 20);

                var tooltipText = new StringBuilder();
                tooltipText.AppendLine($"Lon(x): {coords.X:n3} Lat(y): {coords.Y:n3}");

                if (offsetImagePos.X >= 0 && offsetImagePos.X < PlanetRenderOutput.TotalWidth &&
                    offsetImagePos.Y >= 0 && offsetImagePos.Y < PlanetRenderOutput.TotalHeight)
                {
                    var surface = PlanetRenderOutput.GetResultPixel((int)offsetImagePos.X, (int)offsetImagePos.Y);
                    var climate = PlanetRenderOutput.GetResultClimate((int)offsetImagePos.X, (int)offsetImagePos.Y);
                    var height = PlanetRenderOutput.GetResultHeight((int)offsetImagePos.X, (int)offsetImagePos.Y);
                    var ocean = PlanetRenderOutput.GetResultOceanMask((int)offsetImagePos.X, (int)offsetImagePos.Y);

                    var height_m = (height / (float)short.MaxValue) * 
                        (RenderSettings.DefaultSettings.EcosystemTerrainInfluence + RenderSettings.DefaultSettings.GlobalTerrainInfluence);

                    tooltipText.AppendLine($"Height: {height_m:f2}M");
                    tooltipText.AppendLine($"Climate T: {climate.T} H: {climate.H}");
                    tooltipText.AppendLine($"Ocean: {(ocean / 255f):p2}");
                }

                txtHoverDetails.Text = tooltipText.ToString().Trim();
                txtHoverDetails.Visibility = Visibility.Visible;
            }
            else
            {
                txtHoverDetails.Text = "";
                txtHoverDetails.Visibility = Visibility.Hidden;
            }
        }

        private void canvasMain_MouseDown(object sender, MouseButtonEventArgs e)
        {
            IsDragging = true;
            WasMoved = false;
            DragStart = e.GetPosition(canvasMain);
        }

        private void canvasMain_MouseUp(object sender, MouseButtonEventArgs e)
        {
            IsDragging = false;
            if (!WasMoved)
            {
                if (HasPlanetSelection)
                {
                    HasPlanetSelection = false;
                }
                else
                {
                    HasPlanetSelection = true;
                    PlanetSelectedPoint = e.GetPosition(canvasInnerTranslate);
                }

                RaisePropertyChanged(nameof(HasPlanetSelection));
                RaisePropertyChanged(nameof(PlanetSelectedPoint));
            }
        }

        private void canvasMain_MouseEnter(object sender, MouseEventArgs e)
        {

        }

        private void canvasMain_MouseLeave(object sender, MouseEventArgs e)
        {
            IsDragging = false;
            WasMoved = true;
        }

        private void canvasMain_MouseWheel(object sender, MouseWheelEventArgs e)
        {
            float zoomMod = 1.0f + (e.Delta * ZoomSpeed);
            ZoomFactor *= zoomMod;

            var mousePos = e.GetPosition(canvasMain);

            renderTranslate.X += (Math.Sign(e.Delta) * -mousePos.X / (8 * ZoomFactor));
            renderTranslate.Y += (Math.Sign(e.Delta) * -mousePos.Y / (8 * ZoomFactor));
            
            UpdateTransforms();

            RaisePropertyChanged(nameof(ZoomFactor));
            RaisePropertyChanged(nameof(ZoomInverse));

            canvasMain_MouseMove(null, e);
        }

        private void renderMain_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            UpdateTransforms();
        }

        #endregion

        #region Region Control

        private System.Drawing.Rectangle exportSplatRegion = new System.Drawing.Rectangle();
        private System.Drawing.RectangleF exportImageRegion = new System.Drawing.RectangleF();

        private void ResetCamera_Click(object sender, RoutedEventArgs e)
        {
            exportImageRegion = new System.Drawing.RectangleF(0, 0, SelectedPlanet.TileCount, SelectedPlanet.TileCount);
            UpdateCoordBounds();
        }

        private void CopyCamera_Click(object sender, RoutedEventArgs e)
        {
            var tl = canvasMain.TranslatePoint(new Point(0, 0), canvasInnerTranslate);
            var br = canvasMain.TranslatePoint(new Point(canvasMain.ActualWidth, canvasMain.ActualHeight), canvasInnerTranslate);

            exportImageRegion = new System.Drawing.RectangleF(
                (float)(tl.X / 2 ), 
                (float)(tl.Y ),
                (float)((br.X - tl.X) / 2 ), 
                (float)((br.Y - tl.Y) ));
            UpdateCoordBounds();
        }

        private void latlonBounds_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (!float.TryParse(txtMinX.Text, out var latMin) ||
                !float.TryParse(txtMaxX.Text, out var latMax) ||
                !float.TryParse(txtMinY.Text, out var lonMin) ||
                !float.TryParse(txtMaxY.Text, out var lonMax))
            {
                return;
            }

            var nasacoords = SelectedCoordainateMode == CoordinateMode.NASA;
            System.Drawing.RectangleF clampedRegion;
            if(nasacoords)
                clampedRegion = MathHelper.ClampRectangle(latMin, lonMin, latMax, lonMax, 0f, -90f, 360f, 180f);
            else
                clampedRegion = MathHelper.ClampRectangle(latMin, lonMin, latMax, lonMax, -180f, -90f, 360f, 180f);

            exportImageRegion = SelectedPlanet.CoordinatesToSplat(clampedRegion, nasacoords);
            UpdateCoordBounds(UpdateTextboxes: false);
        }

        private void UpdateCoordBounds(bool UpdateTextboxes = true)
        {
            if (SelectedPlanet == null) return;

            var nasacoords = SelectedCoordainateMode == CoordinateMode.NASA;

            var clampedRegion = MathHelper.ClampRectangle(
                exportImageRegion.Left, exportImageRegion.Top, exportImageRegion.Right, exportImageRegion.Bottom,
                0, 0, SelectedPlanet.TileCount, SelectedPlanet.TileCount);

            var coordBounds = SelectedPlanet.SplatToCoordinates(clampedRegion, NASACoords: nasacoords);

            int tileLeft = (int)Math.Floor(clampedRegion.Left);
            int tileTop = (int)Math.Floor(clampedRegion.Top);

            exportSplatRegion = new System.Drawing.Rectangle(
                tileLeft, tileTop,
                (int)(Math.Ceiling(clampedRegion.Right) - tileLeft),
                (int)(Math.Ceiling(clampedRegion.Bottom) - tileTop));

            planetOutline.Width = SelectedPlanet.TileCount * 2;
            planetOutline.Height = SelectedPlanet.TileCount;

            Canvas.SetLeft(renderRegionOutline, tileLeft * 2);
            Canvas.SetTop(renderRegionOutline, tileTop);
            renderRegionOutline.Width = exportSplatRegion.Width * 2;
            renderRegionOutline.Height = exportSplatRegion.Height;

            Canvas.SetLeft(exportRegionOutline, clampedRegion.X * 2);
            Canvas.SetTop(exportRegionOutline, clampedRegion.Y);
            exportRegionOutline.Width = clampedRegion.Width * 2;
            exportRegionOutline.Height = clampedRegion.Height;

            if (UpdateTextboxes)
            {
                txtMinX.Text = $"{coordBounds.Left}";
                txtMaxX.Text = $"{coordBounds.Right}";
                txtMinY.Text = $"{coordBounds.Top}";
                txtMaxY.Text = $"{coordBounds.Bottom}";
            }

            txtMinXpx.Text = $"{exportSplatRegion.Left}";
            txtMaxXpx.Text = $"{exportSplatRegion.Right}";
            txtMinYpx.Text = $"{exportSplatRegion.Top}";
            txtMaxYpx.Text = $"{exportSplatRegion.Bottom}";

            long sizeCheck = exportSplatRegion.Width * 2 * SelectedRenderSize;
            sizeCheck *= exportSplatRegion.Height * SelectedRenderSize;
            sizeCheck *= 4; //4 channels in output image. TODO: Swtich to int32 and use bitshifting, to allow images one step largers

            IsValidExport = sizeCheck <= int.MaxValue;

            var statsText = new StringBuilder();
            
            statsText.AppendLine($"{exportSplatRegion.Width:n0} x {exportSplatRegion.Height:n0} from planet splat data.");

            int channelDataSize = 13; //9 bytes in render result, plus addisional 4 (copies) for WPF render

            if(IsValidExport)
            {
                sizeCheck /= 4; //Need sizeCheck to be element count

                statsText.AppendLine($"{exportSplatRegion.Width * 2 * SelectedRenderSize:n0} x {exportSplatRegion.Height * SelectedRenderSize:n0}px output size.");
                statsText.AppendLine($"{exportSplatRegion.Width * exportSplatRegion.Height:n0} tiles to render.");
                statsText.AppendLine($"~{sizeCheck / 1024 / 1024 / 1024.0f * channelDataSize:n2}GB expected memory usage.");
            }
            else
            {
                statsText.AppendLine($"ERROR: Image exceeds maximum supported dimensions (2 GP total resolution)");
            }

            lblRenderStats.Text = statsText.ToString();

            RaisePropertyChanged(nameof(IsValidExport));
        }

        #endregion

        private void cmbCoordinateSystem_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (SelectedPlanet == null) return;

            exportImageRegion = new System.Drawing.RectangleF(0, 0, SelectedPlanet.TileCount, SelectedPlanet.TileCount);
            UpdateCoordBounds();
        }
    }

    public class PlanetOption
    {
        public string ID { get; set; }
        public string Name { get; set; }

        public SOC_Archive PlanetArchive { get; set; }
    }
}
