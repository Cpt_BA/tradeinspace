﻿using System;
using System.Threading;
using System.Collections.Generic;
using System.ComponentModel;
using System.Collections.Specialized;
using System.Linq;
using System.Collections.Concurrent;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Runtime.CompilerServices;
using SC_Explorer.ViewModels;

namespace SC_Explorer.Controls
{
    using Timer = System.Timers.Timer;

    //Thank you paws ^_^


    /// <summary>
    /// This is my third implementation of a HierarchicalList view. The original implementation was based on
    /// http://blogs.msdn.com/b/atc_avalon_team/archive/2006/03/01/541206.aspx but converting a treeview to a listview means you
    /// lose virtualization for child items, which for large messages causes unacceptable performance (by default sort director sends out
    /// tag messages roughly 208 fields in size. There was also an issue with filtering, because filtering only affects the first level of a tree viewItems.
    /// (this is claimed by 'design' because theres senarios where you wouldent want this because expanding treviews can sometimes be expensive, and youw ould need to
    /// do that to filter a tree view.)
    /// 
    /// This implementation does the oposit of original implementation, it takes a list view and trys to make it act like a treeview. 
    /// We basicly layout all the items liniarly and then make it LOOK like there hirarchialy placed then manualy controll collapseing and expanding.
    /// Not to many downsides to this implmentation except no hirarchical templates, which we dont need anyways. The code was implemented in haste so its a little messy
    /// 
    /// This main class thie HierarchicalListView's job is to syncronzie The ItemsSource field and convert it to a HierarchicalSource which the actual templates
    /// Listview will actualy bind to that HierarchicalSource.
    /// 
    /// TODO need filtering callbacks
    /// 
    /// TODO there are some perfomance issues with this with larger messages, or frequently updating the list view. Need to look into how to fix (maby we need to reaproach the problem, again)
    /// </summary>
    public class HierarchicalListView : Control, INotifyPropertyChanged
    {


        static HierarchicalListView()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(HierarchicalListView), new FrameworkPropertyMetadata(typeof(HierarchicalListView)));

        }

        private HierarchicalItemCollection collection = new HierarchicalItemCollection();
        private INotifyCollectionChanged changeable;
        private ListView view;
        private GridView viewPart;
        public HierarchicalListView() : base()
        {
            Columns = new GridViewColumnCollection();
            HierarchicalSource = collection;
            //HierarchicalSource = new List<int>();
        }

        public override void OnApplyTemplate()
        {
            view = Template.FindName("PART_VIEW", this) as ListView;
            viewPart = view != null ? view.View as GridView : null;

            view.SelectionChanged += listPart_SelectionChanged;

            List<GridViewColumn> temps = new List<GridViewColumn>();
            foreach (var column in Columns)
                temps.Add(column);
            Columns.Clear();

            foreach (var column in temps)
                viewPart.Columns.Add(column);

            base.OnApplyTemplate();
        }

        private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        private void listPart_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            NotifyPropertyChanged(nameof(SelectedItem));
            NotifyPropertyChanged(nameof(SelectedItems));
        }

        private static void ItemsSourcePropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs args)
        {
            var instance = (HierarchicalListView)obj;
            if (instance.changeable != null)
            {
                instance.changeable.CollectionChanged -= instance.changeable_CollectionChanged;
            }

            instance.changeable = args.NewValue as INotifyCollectionChanged;

            if (instance.changeable != null)
            {
                instance.changeable.CollectionChanged += instance.changeable_CollectionChanged;

                //foreach (MDField field in instance.ItemsSource)
                //{
                //    instance.collection.Add(field);
                //}
                var source = instance.ItemsSource;
                var task = new Task(() =>
                {
                    instance.collection.Clear(true);
                    foreach (IHasChildren field in source)
                    {
                        instance.collection.Add(field);
                    }

                }, TaskCreationOptions.LongRunning);
                task.Start();

            }

        }

        private static void ColumnsPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs args)
        {
            //viewPart = Template.FindName("PART_VIEW", this) as GridView;
            //var instance = (HierarchicalListView)obj;
            //if (instance.viewPart != null)
            //{
            //    foreach (var column in instance.Columns)
            //    {
            //        instance.viewPart.Columns.Add(column);
            //    }
            //}   


        }

        private void changeable_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            //mirror changes to our other collection
            if (e.Action == NotifyCollectionChangedAction.Add)
            {
                foreach (IHasChildren item in e.NewItems)
                    collection.Add(item);
            }
            else if (e.Action == NotifyCollectionChangedAction.Remove)
            {
                foreach (IHasChildren item in e.OldItems)
                    collection.Remove(item);
            }
            else if (e.Action == NotifyCollectionChangedAction.Reset)
            {
                collection.Clear();
                foreach (IHasChildren item in ItemsSource)
                    collection.Add(item);
            }
            else
                throw new NotImplementedException();
        }



        /// <summary> Define the columns used in the list view, note the first column should have a special style</summary>
        public GridViewColumnCollection Columns
        {
            get { return (GridViewColumnCollection)GetValue(ColumnsProperty); }
            set { SetValue(ColumnsProperty, value); }
        }


        public System.Collections.IEnumerable ItemsSource
        {
            get { return (System.Collections.IEnumerable)GetValue(ItemsSourceProperty); }
            set { SetValue(ItemsSourceProperty, value); }
        }

        public System.Collections.IList SelectedItems
        {
            get { return this.view?.SelectedItems; }
        }

        public object SelectedItem
        {
            get { return this.view?.SelectedItem; }
        }



        public System.Collections.IEnumerable HierarchicalSource
        {
            get { return (System.Collections.IEnumerable)GetValue(HierarchicalSourceProperty); }
            set { SetValue(HierarchicalSourceProperty, value); }
        }

        // Using a DependencyProperty as the backing store for HierarchicalSource.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty HierarchicalSourceProperty =
            DependencyProperty.Register("HierarchicalSource", typeof(System.Collections.IEnumerable), typeof(HierarchicalListView));
        


        // Using a DependencyProperty as the backing store for ItemsSource.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ItemsSourceProperty =
            DependencyProperty.Register("ItemsSource", typeof(System.Collections.IEnumerable), typeof(HierarchicalListView), new PropertyMetadata(ItemsSourcePropertyChanged));


        // Using a DependencyProperty as the backing store for Columns.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ColumnsProperty =
            DependencyProperty.Register("Columns", typeof(GridViewColumnCollection), typeof(HierarchicalListView), new PropertyMetadata(ColumnsPropertyChanged));

        public event PropertyChangedEventHandler PropertyChanged;


        /// <summary>
        /// This class holds two lists, a list of HierarchicalItem and a list (dictionary) of MDFields (genericly objects for now
        /// the fact there MDfields dosint mater). HierarchicalItem is suposed to be transparent and you olnly add and remove MDFields.
        /// As a performance improvement the class now batches collection changes and only posts updates on a specified (compile time) 
        /// interval. This helps remove lag from GUI when message breakdown (Which utilizes this class) updates quickly with the users
        /// mouse dragging everywhere, it can still be a little slow but thats frome size of messages not notifications flooding
        /// </summary>
        public class HierarchicalItemCollection : IEnumerable<HierarchicalItem>, INotifyCollectionChanged, IDisposable
        {
            private readonly int MAX_UPDATE_RATE = 10; // in ms
            private readonly object listLocker = new object();
            private List<HierarchicalItem> itemsList;
            private ConcurrentDictionary<object, HierarchicalItem> dataToItemMap;

            private SynchronizationContext syncContext; //for making callbacks on main thread
            private Timer batchTimer;
            private List<NotifyCollectionChangedEventArgs> batchedItems;
            public HierarchicalItemCollection(SynchronizationContext context = null)
            {
                syncContext = context ?? SynchronizationContext.Current;
                batchTimer = new Timer(MAX_UPDATE_RATE);
                batchTimer.Elapsed += batchTimer_Elapsed;
                batchTimer.AutoReset = false;

                itemsList = new List<HierarchicalItem>();
                dataToItemMap = new ConcurrentDictionary<object, HierarchicalItem>();
                batchedItems = new List<NotifyCollectionChangedEventArgs>();
            }

            public void Dispose()
            {
                if (batchTimer != null)
                {
                    batchTimer.Dispose();
                    batchTimer = null;
                }
            }


            public HierarchicalItem Add(IHasChildren field, int level = 0)
            {
                if (field.GetType().IsSubclassOf(typeof(HierarchicalItem)))
                {
                    var existingVM = field as HierarchicalItem;
                    existingVM.Level = level;
                    existingVM.ParentCollection = this;
                    lock (listLocker)
                        itemsList.Add(existingVM);
                    return existingVM;
                }
                else
                {
                    return Add(field, level, true);
                }
            }

            private HierarchicalItem Add(IHasChildren field, int level, bool notifyReset)
            {
                HierarchicalItem item = null;
                if (dataToItemMap.TryGetValue(field, out item))
                    return item;

                //var parentItem = field.AssociatedParent;
                //int count = 0;
                //while (parentItem != null)
                //{
                //    count++;
                //    parentItem = parentItem.AssociatedParent;
                //}

                item = new GenericHierarchicalItem(this, field, level);
                lock (listLocker)
                    itemsList.Add(item);
                //dataToItemMap.Add(field, item);


                foreach (IHasChildren childField in field.Children)
                    item.Children.Add(Add(childField, level + 1, false));

                if (notifyReset)
                    OnCollectionChange(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));


                return item;

            }

            public void Remove(object o)
            {
                HierarchicalItem item;
                dataToItemMap.TryRemove(o, out item);
                if (item != null)
                {
                    lock (listLocker)
                        itemsList.Remove(item);
                    OnCollectionChange(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Remove, item));
                }

            }

            public void Clear(bool silent = false)
            {
                lock (listLocker) //this seems to be a small perfomance problem, optimaly we would do a itemsList.clear, but then some code reading the list can get burned by concurent modification.
                    itemsList = new List<HierarchicalItem>();
                dataToItemMap.Clear();
                if (!silent)
                    OnCollectionChange(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
            }

            internal void NotifyVisibilityChanged()
            {
                var action = NotifyCollectionChangedAction.Reset; //just goign with reset event, makes things easyer
                var ev = new NotifyCollectionChangedEventArgs(action);
                OnCollectionChange(ev);
            }


            protected virtual void OnCollectionChange(NotifyCollectionChangedEventArgs e)
            {
                //we sorta support old way without timer, so if this class is disposed it will still work
                // but i dont know if its legal to call functions on timer after its disposed so theres a potentual race condition.
                var timer = batchTimer;
                if (timer != null)
                {
                    if (e.Action == NotifyCollectionChangedAction.Reset)
                        batchedItems.Clear(); //no longer matters whats in here
                    batchedItems.Add(e);
                    batchTimer.Start();
                }
                else
                {
                    syncContext.Post((none) => CollectionChanged(this, e), null);
                }
            }

            void batchTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
            {
                var localList = new List<NotifyCollectionChangedEventArgs>();
                localList = Interlocked.Exchange(ref batchedItems, localList);
                if (localList.Count > 0)
                {
                    syncContext.Post((none) =>
                    {
                        foreach (var item in localList)
                            CollectionChanged?.Invoke(this, item);
                    }, null);
                }

            }

            public event NotifyCollectionChangedEventHandler CollectionChanged;

            private IEnumerable<HierarchicalItem> GetVisibleItems(IEnumerable<HierarchicalItem> fromObject = null)
            {
                foreach(var child in fromObject.Where(o => o.IsVisible))
                {
                    yield return child;

                    foreach(var entryChild in GetVisibleItems(child.Children))
                    {
                        yield return entryChild;
                    }
                }
            }

            public IEnumerator<HierarchicalItem> GetEnumerator()
            {
                return GetVisibleItems(itemsList).GetEnumerator();
            }


            System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
            {
                return GetEnumerator();
            }

        }

        public interface IHasChildren
        {
            IEnumerable<Object> Children { get; }
        }
    }
}


