﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TradeInSpace.Explore.Socpak;

namespace SC_Explorer.Controls
{
    /// <summary>
    /// Interaction logic for SocpakChildDetails.xaml
    /// </summary>
    public partial class ExposedEntityDetails : UserControl
    {
        public static readonly DependencyProperty EntityProperty = DependencyProperty.Register(
            "Entity",
            typeof(ExposedEntity),
            typeof(ExposedEntityDetails),
            new PropertyMetadata(EntityPropertyChanged));


        public ExposedEntityDetails()
        {
            InitializeComponent();
        }

        private static void EntityPropertyChanged(
            DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            var control = (ExposedEntityDetails)obj;
            var entity = control.Entity as FullEntity;

            if (entity == null)
            {
                control._xmlDoc.Text = "";
                return;
            }

            control._xmlDoc.Text = Utilities.FormatXML(entity.Element.OuterXml);
            control._Doc.Text = Utilities.FormatXML(entity.DatabaseEntity?.Element?.OuterXml);
        }

        public ExposedEntity Entity
        {
            get { return (ExposedEntity)GetValue(EntityProperty); }
            set { SetValue(EntityProperty, value); }
        }
    }
}
