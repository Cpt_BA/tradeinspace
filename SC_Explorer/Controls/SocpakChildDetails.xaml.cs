﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TradeInSpace.Explore.Socpak;

namespace SC_Explorer.Controls
{
    /// <summary>
    /// Interaction logic for SocpakChildDetails.xaml
    /// </summary>
    public partial class SocpakChildDetails : UserControl
    {
        public static readonly DependencyProperty EntryProperty = DependencyProperty.Register(
            "Entry",
            typeof(SOC_Entry),
            typeof(SocpakChildDetails),
            new PropertyMetadata(EntryPropertyChanged));


        public SocpakChildDetails()
        {
            InitializeComponent();
        }

        private static void EntryPropertyChanged(
            DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            var control = (SocpakChildDetails)obj;
            var entry = control.Entry;

            if (entry == null) return;

            control._childRecordDoc.Text = Utilities.FormatXML(entry.Element?.OuterXml);
            control._childrenDoc.Text = Utilities.FormatXML(entry.Archive?.RootDoc?.OuterXml);
            control._additionalDataDoc.Text = Utilities.FormatXML(entry.OwnEntity?.Element?.OuterXml);
            control._entityDoc.Text = Utilities.FormatXML(entry.EntitiesDocument?.OuterXml);
        }

        public SOC_Entry Entry
        {
            get { return (SOC_Entry)GetValue(EntryProperty); }
            set { SetValue(EntryProperty, value); }
        }
    }
}
