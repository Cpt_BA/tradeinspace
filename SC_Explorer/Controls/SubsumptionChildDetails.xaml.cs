﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TradeInSpace.Explore.Socpak;
using TradeInSpace.Explore.Subsumption;

namespace SC_Explorer.Controls
{
    /// <summary>
    /// Interaction logic for SubsumptionChildDetails.xaml
    /// </summary>
    public partial class SubsumptionChildDetails : UserControl
    {
        public static readonly DependencyProperty EntryProperty = DependencyProperty.Register(
            "Entry",
            typeof(SubsumptionNode),
            typeof(SubsumptionChildDetails),
            new PropertyMetadata(EntryPropertyChanged));


        public SubsumptionChildDetails()
        {
            InitializeComponent();
        }

        private static void EntryPropertyChanged(
            DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            var control = (SubsumptionChildDetails)obj;
            var entry = control.Entry;

            if (entry == null) return;

            control._childRecordDoc.Text = Utilities.FormatXML(entry.OriginalElement?.OuterXml);
            control._childrenDoc.Text = Utilities.FormatXML(entry.SOC_Entry?.Element?.OuterXml);
        }

        public SubsumptionNode Entry
        {
            get { return (SubsumptionNode)GetValue(EntryProperty); }
            set { SetValue(EntryProperty, value); }
        }
    }
}
