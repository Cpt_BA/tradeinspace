﻿using SC_Explorer.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TradeInSpace.Explore.DataForge;
using TradeInSpace.Explore.Socpak;
using TradeInSpace.Explore.Subsumption;

namespace SC_Explorer.ViewModels
{
    public class SubsumptionChildViewModel : HierarchicalItem, HierarchicalListView.IHasChildren
    {
        protected bool isInitialized;
        protected string filter;
        protected bool canExpand;

        public SubsumptionChildViewModel(SubsumptionNode Child, DFDatabase DFData) : base()
        {
            this.Item = Child;
            this.DFData = DFData;

            CanExpand = this.Item.Children.Count > 0;
        }
        public SubsumptionChildViewModel(SubsumptionRoot RootNode, DFDatabase DFData) : this(new SubsumptionNode()
        {
            Children = RootNode.Children,
            Name = RootNode.Name
        }, DFData) { }


        protected IEnumerable<object> GetChildren()
        {
            foreach (var item in Item.Children)
            {
                yield return item;
            }
        }

        protected void InitializeData()
        {
            if (!isInitialized)
            {
                foreach (var child in Item.Children.OrderBy(c => c.Name))
                {
                    Children.Add(new SubsumptionChildViewModel(child, DFData));
                }

                //temporarally disabled
                //Children.Add(new NamedCollectionViewModel($"[{socChildVMs.Count()}] Children", socChildVMs));
                //Children.Add(new NamedCollectionViewModel($"[{socEntVMs.Count()}] Entities", socEntVMs));

                foreach (var child in Children)
                {
                    child.ParentCollection = this.ParentCollection;
                    child.Level = this.Level + 1;
                }
                this.ParentCollection?.NotifyVisibilityChanged();
                isInitialized = true;
            }
        }


        internal override void OnExpandedChanged()
        {
            if (IsExpanded)
            {
                InitializeData();

                if (Children.Count == 0)
                {
                    CanExpand = false;
                    IsExpanded = false;
                }
            }

            base.OnExpandedChanged();
        }

        protected void OnFilterChanged()
        {
        }

        public string Filter { get => filter; set => Set(ref filter, value, OnFilterChanged); }




        public string Name { get => Item.Name; }
        public SubsumptionNode Item { get; }
        public DFDatabase DFData { get; }
    }
}
