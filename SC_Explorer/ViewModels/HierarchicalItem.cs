﻿using System;
using System.Runtime.CompilerServices;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Windows.Data;
using SC_Explorer.Controls;
using static SC_Explorer.Controls.HierarchicalListView;

namespace SC_Explorer.ViewModels
{
    public abstract class HierarchicalItem : INotifyPropertyChanged, IHasChildren
    {
        private bool canExpand;
        private bool isExpanded;
        private int level;
        private bool isVisible;
        private HierarchicalItemCollection parentCollection;

        public HierarchicalItem(HierarchicalItemCollection ParentCollection = null, int Level = 0, bool Visible = true)
        {
            parentCollection = ParentCollection;
            CanExpand = false;
            level = Level;
            isVisible = Visible;
            Children = new ObservableCollection<HierarchicalItem>();
        }

        public bool CanExpand { get => canExpand; set => Set(ref canExpand, value); }
        public bool IsExpanded { get => isExpanded; set => Set(ref isExpanded, value, OnExpandedChanged); }
        public int Level { get => level; set => Set(ref level, value); }
        public bool IsVisible
        {
            get => isVisible;
            set
            {
                foreach(var child in Children)
                {
                    child.IsVisible = value;
                }
                Set(ref isVisible, value);
                parentCollection?.NotifyVisibilityChanged();
            }
        }
        internal HierarchicalItemCollection ParentCollection
        {
            get => parentCollection;
            set => parentCollection = value;
        }


        protected void Set<T>(ref T field, T value, [CallerMemberName] string propertyName = "") => Set(ref field, value, () => { }, propertyName);

        protected void Set<T>(ref T field, T value, Action handlerShortcut, [CallerMemberName] string propertyName = "")
        {
            if (!EqualityComparer<T>.Default.Equals(field, value))
            {
                field = value;
                handlerShortcut?.Invoke();
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
            }
        }


        public event PropertyChangedEventHandler PropertyChanged;
        public ObservableCollection<HierarchicalItem> Children { get; protected set; } = new ObservableCollection<HierarchicalItem>();
        IEnumerable<object> HierarchicalListView.IHasChildren.Children => Children;

        internal virtual void OnExpandedChanged()
        {
            foreach(var child in Children)
            {
                child.IsVisible = this.IsExpanded;
            }
        }
    }
}
