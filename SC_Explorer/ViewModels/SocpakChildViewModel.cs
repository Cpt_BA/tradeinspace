﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TradeInSpace.Explore.Socpak;
using SC_Explorer.Controls;
using TradeInSpace.Explore;
using TradeInSpace.Explore.DataForge;

namespace SC_Explorer.ViewModels
{
    public class SocpakChildViewModel : HierarchicalItem, HierarchicalListView.IHasChildren
    {
        protected bool isInitialized;
        protected string filter;

        public SocpakChildViewModel(SOC_Entry Child, DFDatabase DFData) : base()
        {
            this.Item = Child;
            this.DFData = DFData;

            CanExpand = true;
        }

        /*
        protected IEnumerable<object> GetChildren()
        {
            foreach(var item in Item.Children)
            {
                yield return item;
            }

            foreach (var item in Item.Entities)
            {
                yield return item;
            }
        }
        */

        protected void InitializeData()
        {
            if(!isInitialized)
            {
                this.Item.LoadFullDetails();
                this.Item.LoadAllEntities(false);

                //var allChildren = this.GetChildren();

                var socChildVMs = new List<SocpakChildViewModel>();
                var socEntVMs = new List<ExposedEntityViewModel>();

                foreach(var child in Item.Children.OrderBy(c => c.Name))
                {
                    socChildVMs.Add(new SocpakChildViewModel(child, DFData));
                }

                foreach(var entity in Item.Entities.OrderBy(e => e.Label))
                {
                    socEntVMs.Add(new ExposedEntityViewModel(entity));
                }

                //temporarally disabled
                Children.Add(new NamedCollectionViewModel($"[{socChildVMs.Count()}] Child Containers", socChildVMs, this.ParentCollection, this.Level + 1));
                Children.Add(new NamedCollectionViewModel($"[{socEntVMs.Count()}] Entities", socEntVMs, this.ParentCollection, this.Level + 1));

                foreach(var child in Children)
                {
                    //child.ParentCollection = this.ParentCollection;
                    //child.Level = this.Level + 1;
                }

                this.ParentCollection.NotifyVisibilityChanged();
                isInitialized = true;
            }
        }


        internal override void OnExpandedChanged()
        {
            if(IsExpanded)
            {
                InitializeData();

                if (Children.Count == 0)
                {
                    CanExpand = false;
                    IsExpanded = false;
                }
            }

            base.OnExpandedChanged();
        }

        protected void OnFilterChanged()
        {
        }

        public string Filter { get => filter; set => Set(ref filter, value, OnFilterChanged); }




        public string Name { get => Item.Name; }
        public SOC_Entry Item { get; }
        public DFDatabase DFData { get; }
    }
}
