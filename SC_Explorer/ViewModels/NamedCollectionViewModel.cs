﻿using SC_Explorer.Controls;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SC_Explorer.ViewModels
{

    public class NamedCollectionViewModel : HierarchicalItem, HierarchicalListView.IHasChildren
    {
        public NamedCollectionViewModel(string name, IEnumerable<HierarchicalItem> collection, HierarchicalListView.HierarchicalItemCollection parentCollection = null, int level = 0)
        {
            Name = name;
            //fix for the note below. update the actual collection used for evaultion.
            Children = new ObservableCollection<HierarchicalItem>(collection);
            CanExpand = Children.Count() != 0;
            IsExpanded = false;

            ParentCollection = parentCollection;
            Level = level;
            
            foreach(var child in Children)
            {
                child.ParentCollection = ParentCollection;
                child.Level = Level + 1;
            }

            ParentCollection.NotifyVisibilityChanged();
            this.OnExpandedChanged();
        }

        //NOTE: Both "Children" here are not the children on HierarchicalItem used for evaluation.
        //      must add to that Children collection explicity
        IEnumerable<object> HierarchicalListView.IHasChildren.Children => base.Children;

        public string Name { get; }
        public string Item { get => Name; }
    }
}
