﻿using SC_Explorer.Controls;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TradeInSpace.Explore.Socpak;

namespace SC_Explorer.ViewModels
{
    class ExposedEntityViewModel : HierarchicalItem
    {
        public ExposedEntityViewModel(ExposedEntity entity)
        {
            Item = entity;
            CanExpand = false;
        }


        public string Name { get => Item.Label; }
        public ExposedEntity Item { get; }
    }
}
