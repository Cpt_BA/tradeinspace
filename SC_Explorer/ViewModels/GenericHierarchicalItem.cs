﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static SC_Explorer.Controls.HierarchicalListView;

namespace SC_Explorer.ViewModels
{
    class GenericHierarchicalItem : HierarchicalItem
    {
        private bool childrenVisible;
        private object data;
        public GenericHierarchicalItem(HierarchicalItemCollection itemCollection, object newData, int newLevel)
            : base(itemCollection, newLevel, true)
        {
            childrenVisible = true;
            data = newData;
        }

        public bool ChildrenVisible
        {
            get { return childrenVisible; }
            set
            {
                foreach (var item in Children)
                    item.IsVisible = value;
                Set(ref childrenVisible, value);
            }
        }

        public object Data { get { return data; } set { Set(ref data, value); } }
    }
}
