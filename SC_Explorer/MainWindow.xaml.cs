﻿using System;
using System.Text.RegularExpressions;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TradeInSpace.Explore;
using TradeInSpace.Explore.Socpak;
using SC_Explorer.ViewModels;
using Color = System.Drawing.Color;
using Path = System.IO.Path;
using TradeInSpace.Explore.DataForge;
using TradeInSpace.Export;
using TradeInSpace.Explore.SC.Data;

namespace SC_Explorer
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        public DFDatabase GameData { get; set; }
        public ObservableCollection<HierarchicalItem> RootNodes { get; set; }
        public List<Tag> Tags { get; set; }
        public bool SearchOnlyMode { get; set; } = false;

        public MainWindow()
        {
            try
            {
                var launcherInstallDir = Microsoft.Win32.Registry.GetValue(@"HKEY_LOCAL_MACHINE\SOFTWARE\81bfc699-f883-50c7-b674-2483b6baae23", "InstallLocation", null) as string;
                var RSIDir = Path.GetDirectoryName(launcherInstallDir);
                var SCSettings = SCLauncherReader.LoadGameSettings(RSIDir);

                GameData = DFDatabase.ParseFromSettings(SCSettings, "LIVE", CacheLevel.Diabled);
                GameData.ParseDFContent();

                Tags = GameData.Tags.RootTags;

                PostLoadDatabase();
                InitializeComponent();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error Loading game .P4K file" + Environment.NewLine + TradeInSpace.Explore.Utilities.GetFullErrorDetails(ex));
                Application.Current.Shutdown();
            }
        }

        public MainWindow(DFDatabase LoadedDatabase)
        {
            try
            {
                this.GameData = LoadedDatabase;

                SearchOnlyMode = !GameData.DFLoaded;

                Tags = SearchOnlyMode ?
                    new List<Tag>() :
                    GameData.Tags.RootTags;

                PostLoadDatabase();
                InitializeComponent();

                if (SearchOnlyMode)
                    tabControls.SelectedIndex = tabControls.Items.IndexOf(tabControls.Items.Cast<TabItem>().FirstOrDefault(ti => ti.Header?.ToString() == "Search"));

                if (GameData.HasEntityErrors)
                {
                    StringBuilder message = new StringBuilder();

                    message.AppendLine($"NOTE! {GameData.EntityLoadErrors.Count} Entity errors occurred while loading.");
                    message.AppendLine("You can continue, but things may be (additionally) unstable. You have been warned!");
                    message.AppendLine();
                    message.AppendLine("Entities with errors:");
                    foreach(var error in GameData.EntityLoadErrors.Take(50))
                    {
                        message.AppendLine($"\t{error.InstanceName}");
                    }
                    if (GameData.EntityLoadErrors.Count > 50)
                    {
                        message.AppendLine($"\t...{GameData.EntityLoadErrors.Count - 50} more...");
                    }

                    MessageBox.Show(message.ToString(), "Load Errors", MessageBoxButton.OK, MessageBoxImage.Warning, MessageBoxResult.OK);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error Loading game .P4K file" + Environment.NewLine + TradeInSpace.Explore.Utilities.GetFullErrorDetails(ex));
                Application.Current.Shutdown();
            }
        }

        private void PostLoadDatabase()
        {
            var stantonDB = SOC_Archive.LoadFromGameDBSocPak(GameData, "Data/ObjectContainers/PU/system/stanton/stantonsystem.socpak");

            RootNodes = new ObservableCollection<HierarchicalItem>()
            {
                 new SocpakChildViewModel(stantonDB.RootEntry, GameData)
            };

            Title = GameData.Version.ToUpper();
        }



        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private void ContentControl_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {

        }

        private void _SystemTree_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {

        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            _DoRender(false);
        }

        private void MenuItem_Click_1(object sender, RoutedEventArgs e)
        {
            _DoRender(true);
        }

        private void _DoRender(bool IncludeEntities = false)
        {
            var selectedNode = (_SystemTree.SelectedItem as SocpakChildViewModel)?.Item;

            if (selectedNode == null)
                return;

            try
            {
                //GameDatabase.Subsumption.LoadFrom(selectedNode, PreloadDetails: true, PreloadEntities: true);

                var svgXY = SVGOrbitExporter.GenerateSVG(selectedNode, v => v.X, v => v.Y, IncludeEntities);
                var svgXZ = SVGOrbitExporter.GenerateSVG(selectedNode, v => v.X, v => v.Z, IncludeEntities);
                var svgYZ = SVGOrbitExporter.GenerateSVG(selectedNode, v => v.Y, v => v.Z, IncludeEntities);

                File.WriteAllText("test_file_xy.svg", svgXY);
                File.WriteAllText("test_file_xz.svg", svgXZ);
                File.WriteAllText("test_file_yz.svg", svgYZ);

                var xyWindow = new MapDisplayWindow(svgXY);
                xyWindow.Show();
                
                var xzWindow = new MapDisplayWindow(svgXZ);
                xzWindow.Show();
                
                var yzWindow = new MapDisplayWindow(svgYZ);
                yzWindow.Show();
            }
            catch (Exception ex)
            {

            }
        }

        private async void ExportSelected_Direct(object sender, RoutedEventArgs e)
        {
            await ExportSelected(false);
        }

        private async void ExportSelected_Recursive(object sender, RoutedEventArgs e)
        {
            await ExportSelected(true);
        }

        private async Task ExportSelected(bool Explore)
        {
            var selectedNode = (_SystemTree.SelectedItem as SocpakChildViewModel)?.Item;

            if (selectedNode == null)
                return;

            System.Windows.Forms.FolderBrowserDialog fbd = new System.Windows.Forms.FolderBrowserDialog()
            {
                Description = "Select folder to export all models to.",
                ShowNewFolderButton = true,
                SelectedPath = Properties.Settings.Default.LastModelExportDirectory
            };

            var result = fbd.ShowDialog();

            if (result == System.Windows.Forms.DialogResult.Cancel)
                return;

            var rootExport = fbd.SelectedPath;

            //Save settings
            Properties.Settings.Default.LastModelExportDirectory = rootExport;
            Properties.Settings.Default.Save();

            try
            {
                //Run this in the background.
                Cursor = Cursors.Wait;

                var structure = await Task.Run(() =>
                {
                    return SOC_Explorer.ExploreArchive(GameData, selectedNode.ArchivePath,
                        new SOC_Explorer.SearchConfig()
                        {
                            LoadEntities = true,
                            LoadReferences = true,
                            DynamicComponents = false,
                            Recursive = Explore
                        });
                });

                var pointCloudText = await Task.Run(() =>
                {
                    return CSVExporter.ExportCSV(GameData, structure, Path.Combine(rootExport, "Models"));
                });

                File.WriteAllText(Path.Combine(rootExport, Path.ChangeExtension(selectedNode.Name, ".csv")), pointCloudText);

                var flatItems = TradeInSpace.Explore.Utilities.FlattenRecursive(structure, s => s.Children);
                MessageBox.Show($"Wrote {flatItems.Count():N0} lines to file.");

                Cursor = Cursors.Arrow;
            }
            catch (Exception ex)
            {

            }
        }

        private void MenuItem_Click_3(object sender, RoutedEventArgs e)
        {
            var selectedEntity = (_SystemTree.SelectedItem as ExposedEntityViewModel)?.Item as FullEntity;

            if (selectedEntity == null)
                return;

            try
            {
                var model = selectedEntity.LoadModel(GameData);
            }
            catch (Exception ex)
            { 
            }
        }

        private void TreeView_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            var selectedTag = _tagTree.SelectedItem as Tag;

            if (selectedTag == null)
                return;

            Clipboard.SetText(selectedTag.ID.ToString());
        }
    }
    
}
