﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using System.Xml;

using WPFControls = System.Windows.Controls;
using WPFImage = System.Windows.Media;

namespace SC_Explorer
{
    public static class Utilities
    {
        public static string FormatXML(string UglyXML)
        {
            if (string.IsNullOrWhiteSpace(UglyXML)) return "";

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(UglyXML);

            return FormatXML(doc);
        }

        public static string FormatXML(XmlDocument UglyDocument)
        {
            string strRetValue = null;
            Encoding enc = Encoding.UTF8;
            // enc = new System.Text.UTF8Encoding(false);

            XmlWriterSettings xmlWriterSettings = new XmlWriterSettings();
            xmlWriterSettings.Encoding = enc;
            xmlWriterSettings.Indent = true;
            xmlWriterSettings.IndentChars = "    ";
            xmlWriterSettings.NewLineChars = "\r\n";
            xmlWriterSettings.NewLineHandling = NewLineHandling.Replace;
            xmlWriterSettings.NewLineOnAttributes = true;
            //xmlWriterSettings.OmitXmlDeclaration = true;
            xmlWriterSettings.ConformanceLevel = ConformanceLevel.Document;


            using (MemoryStream ms = new MemoryStream())
            {
                using (XmlWriter writer = XmlWriter.Create(ms, xmlWriterSettings))
                {
                    UglyDocument.Save(writer);
                    writer.Flush();
                    ms.Flush();

                    writer.Close();
                }

                ms.Position = 0;
                using (StreamReader sr = new StreamReader(ms, enc))
                {
                    strRetValue = sr.ReadToEnd();

                    sr.Close();
                }

                ms.Close();
            }

            xmlWriterSettings = null;
            return strRetValue;
        }

        public static string FormatJSON(string UglyJSON)
        {
            return JsonConvert.SerializeObject(JsonConvert.DeserializeObject(UglyJSON), Newtonsoft.Json.Formatting.Indented);
        }

        public static string FormatHexdump(byte[] FileData, int ColumnSize = 16, int IncludeBytes = 10 * 1024)
        {
            var printBytes = Math.Min(IncludeBytes, FileData.Length);
            var finalOutput = new StringBuilder();

            for (int i = 0; i < printBytes; i += ColumnSize)
            {
                finalOutput.Append($"{i:X4} ");

                for (int col = 0; col < ColumnSize; col++)
                {
                    if (i + col < printBytes)
                        finalOutput.Append($"{FileData[i + col]:X2}");
                    else
                        finalOutput.Append("  ");
                }

                var lineBytes = Math.Min(FileData.Length - i, ColumnSize);

                var displayText = Encoding.ASCII.GetString(FileData, i, lineBytes)
                    .Replace("\r", @"\r")
                    .Replace("\n", @"\n")
                    .Replace("\t", @"\t");

                finalOutput.Append($" {displayText}");
                finalOutput.AppendLine();
            }

            return finalOutput.ToString();
        }

        public static bool Contains(this string source, string check, StringComparison comp)
        {
            return source?.IndexOf(check, comp) >= 0;
        }

        public static BitmapSource WpfSource(byte[] imageData, int width, int height, int stride, WPFImage.PixelFormat pixelFormat)
        {
            return BitmapSource.Create(width, height, 96.0, 96.0, pixelFormat, null, imageData, stride);
        }

        //https://stackoverflow.com/a/13680458/6539248
        public static bool ExploreFile(string filePath)
        {
            if (!System.IO.File.Exists(filePath))
            {
                return false;
            }
            //Clean up file path so it can be navigated OK
            filePath = System.IO.Path.GetFullPath(filePath);
            System.Diagnostics.Process.Start("explorer.exe", string.Format("/select,\"{0}\"", filePath));
            return true;
        }

        public static List<BitmapSource> WpfSources(Pfim.IImage image, out GCHandle dataHandle)
        {
            var pinnedArray = GCHandle.Alloc(image.Data, GCHandleType.Pinned);
            var addr = pinnedArray.AddrOfPinnedObject();
            var bsource = BitmapSource.Create(image.Width, image.Height, 96.0, 96.0,
                PixelFormat(image), null, addr, image.DataLen, image.Stride);

            List<BitmapSource> sources = new List<BitmapSource>();

            sources.Add(bsource);

            foreach (var mip in image.MipMaps)
            {
                var mipAddr = addr + mip.DataOffset;
                var mipSource = BitmapSource.Create(mip.Width, mip.Height, 96.0, 96.0,
                    PixelFormat(image), null, mipAddr, mip.DataLen, mip.Stride);
                sources.Add(mipSource);
            }

            dataHandle = pinnedArray;
            return sources;
        }

        private static WPFImage.PixelFormat PixelFormat(Pfim.IImage image)
        {
            switch (image.Format)
            {
                case Pfim.ImageFormat.Rgb24:
                    return WPFImage.PixelFormats.Bgr24;
                case Pfim.ImageFormat.Rgba32:
                    return WPFImage.PixelFormats.Bgra32;
                case Pfim.ImageFormat.Rgb8:
                    return WPFImage.PixelFormats.Gray8;
                case Pfim.ImageFormat.R5g5b5a1:
                case Pfim.ImageFormat.R5g5b5:
                    return WPFImage.PixelFormats.Bgr555;
                case Pfim.ImageFormat.R5g6b5:
                    return WPFImage.PixelFormats.Bgr565;
                default:
                    throw new Exception($"Unable to convert {image.Format} to WPF PixelFormat");
            }
        }
    }
}
