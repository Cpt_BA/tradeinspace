﻿using Svg;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SC_Explorer
{
    using SVG = SharpVectors.Dom.Svg;

    /// <summary>
    /// Interaction logic for MapDisplayWindow.xaml
    /// </summary>
    public partial class MapDisplayWindow : Window
    {
        public string DisplaySVG { get; set; } = "";

        public MapDisplayWindow()
        {
            InitializeComponent();
        }

        public MapDisplayWindow(string SVGText)
        {
            DisplaySVG = SVGText;

            InitializeComponent();
        }
    }
}
