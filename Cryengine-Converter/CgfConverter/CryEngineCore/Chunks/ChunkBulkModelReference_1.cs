﻿using CgfConverter.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CgfConverter.CryEngineCore
{
    public class ChunkBulkModelReference : Chunk
    {
        public bool ParseError = false;

        public List<string> ReferencedModels = new List<string>();
        public List<BulkModelReference> References = new List<BulkModelReference>();
    }

    public class ChunkBulkModelReference_1 : ChunkBulkModelReference
    {
        public override void Read(BinaryReader b)
        {
            base.Read(b);

            //Local functions
            void readReferencedPath()
            {
                var pathBytes = b.ReadBytes(256);
                var pathString = Encoding.ASCII.GetString(pathBytes);
                var endIndex = pathString.IndexOf('\0');
                if (endIndex >= 0)
                    pathString = pathString.Substring(0, endIndex);

                ReferencedModels.Add(pathString);
            }

            //Start parsing
            int z1 = b.ReadInt32();

            int primaryRecordCount = b.ReadInt32();
            
            for(int i = 0; i < primaryRecordCount; i++)
            {
                readReferencedPath();
            }

            int secondaryRecordCount = b.ReadInt16();
            int tertiaryRecordCount = b.ReadInt16();


            for (int i = 0; i < secondaryRecordCount + tertiaryRecordCount; i++)
            {
                readReferencedPath();
            }


            /*
            if (recordType != 0x0D)
            {
                var pos = b.BaseStream.Position;
                b.BaseStream.Seek(Offset, SeekOrigin.Begin);
                File.WriteAllBytes($"chunk_{recordType:X}.bin", b.ReadBytes((int)DataSize));
                throw new Exception("Byte sequence out-of-order!");
            }
            */

            int recordFooterType = b.ReadInt32();
            SkipBytes(b, 0x18);

            int instancesLength = b.ReadInt32();
            var startPos = b.BaseStream.Position;

            //instancesLength -= (int)(startPos - Offset);

            BulkModelReference previousRef = null;
            int id = 0;
            try
            {
                if (instancesLength > 0)
                {
                    while (b.BaseStream.Position < startPos + instancesLength)
                    {
                        BulkModelReference modelRef = new BulkModelReference();
                        modelRef.ReadBulkModelReference(b, recordFooterType, id, previousRef);
                        References.Add(modelRef);
                        previousRef = modelRef;
                        id++;
                    }

                    if (b.BaseStream.Position - startPos != instancesLength)
                    {
                        throw new Exception("Did not read all instance data!");
                    }
                }
            }
#if DEBUG
            catch(InvalidDataException ex)
            {
                b.BaseStream.Seek(Offset, SeekOrigin.Begin);
                var dump_bytes = new byte[Size];
                b.Read(dump_bytes, 0, (int)Size);
                var debug_dir = @"E:\SC\StarCitizen\Debug Dump";
                if (!Directory.Exists(debug_dir))
                    Directory.CreateDirectory(debug_dir);
                File.WriteAllBytes(Path.Combine(debug_dir, Path.ChangeExtension(ParentModel.FileName, ".bin")), dump_bytes);
                b.BaseStream.Seek(Offset + Size, SeekOrigin.Begin);
            }
#endif
            catch (Exception ex)
            {
                //something...
                ParseError = true;
                b.BaseStream.Seek(Offset + Size, SeekOrigin.Begin);
            }
        }
    }
}
