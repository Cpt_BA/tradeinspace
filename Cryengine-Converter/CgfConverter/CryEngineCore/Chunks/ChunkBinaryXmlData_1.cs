﻿using System;
using System.IO;
using System.Xml;

namespace CgfConverter.CryEngineCore
{
    public class ChunkBinaryXmlData_1 : ChunkBinaryXmlData     //  0xCCCBF004:  Binary XML Data
    {
        public override void Read(BinaryReader b)
        {
            base.Read(b);

            var bytesToRead = (Int32)(this.Size - Math.Max(b.BaseStream.Position - this.Offset, 0));

            Data = b.ReadBytes(bytesToRead);
        }
    }
}
