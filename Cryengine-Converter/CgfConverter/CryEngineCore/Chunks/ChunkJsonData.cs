﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CgfConverter.CryEngineCore
{
    //		ChunkType	0xcccbf011	CgfConverter.ChunkType
    public abstract class ChunkJsonData : Chunk
    {
        public string JSONData { get; set; }

        public override string ToString()
        {
            return $"JSON Chunk: {JSONData.Length} chars";
        }
    }

    public class ChunkJsonData_15 : ChunkJsonData
    {
        public override void Read(BinaryReader b)
        {
            base.Read(b);
            JSONData = Encoding.UTF8.GetString(b.ReadBytes((int)this.DataSize));
        }
    }
}
