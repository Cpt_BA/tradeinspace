﻿using CgfConverter.Structs;
using Extensions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace CgfConverter.Models
{
    public class BulkModelReference
    {
        public int ID;
        public Vector3 _minBounds;
        public Vector3 _maxBounds;
        public Matrix3x4 RelativeTransform;
        public short ModelIndex;
        public BulkReferenceType ReferenceType;
        public long Offset;

#if DEBUG
        public long Length;
        public byte[] ReferenceBytes;
#endif

        /*
        public Matrix4x4 LocalTransform
        {
            get
            {
                return Matrix4x4Extensions.CreateFromMatrix3x4(RelativeTransform);
            }
        }
        */

        public (float, float, float) Position
        {
            get
            {
                return (RelativeTransform.Translation.X, RelativeTransform.Translation.Y, RelativeTransform.Translation.Z);
            }
        }

        public (float, float, float, float, float, float, float, float, float) Rotation
        {
            get
            {
                return (RelativeTransform.Rotation.M11, RelativeTransform.Rotation.M12, RelativeTransform.Rotation.M13,
                        RelativeTransform.Rotation.M21, RelativeTransform.Rotation.M22, RelativeTransform.Rotation.M23,
                        RelativeTransform.Rotation.M31, RelativeTransform.Rotation.M32, RelativeTransform.Rotation.M33);
            }
        }

        public (float, float, float) Scale
        {
            get
            {
                return (RelativeTransform.Scale.X, RelativeTransform.Scale.Y, RelativeTransform.Scale.Z);
            }
        }

        public (float, float, float) MinBounds
        {
            get
            {
                return (_minBounds.X, _minBounds.Y, _minBounds.Z);
            }
        }

        public (float, float, float) MaxBounds
        {
            get
            {
                return (_maxBounds.X, _maxBounds.Y, _maxBounds.Z);
            }
        }

        public (float, float, float, float, float, float, float, float, float, float, float, float) Transform
        {
            get
            {
                return (
                    RelativeTransform.M11, RelativeTransform.M12, RelativeTransform.M13, RelativeTransform.M14,
                    RelativeTransform.M21, RelativeTransform.M22, RelativeTransform.M23, RelativeTransform.M24,
                    RelativeTransform.M31, RelativeTransform.M32, RelativeTransform.M33, RelativeTransform.M34);
            }
        }

        public void ReadBulkModelReference(BinaryReader b, int footerType, int id, BulkModelReference previousRef)
        {
            ID = id;
            Offset = b.BaseStream.Position;

            ReferenceType = (BulkReferenceType)b.ReadInt32();

            //TODO: Loss of precision here...
            _minBounds = b.ReadVector3(BinaryReaderExtensions.InputType.Double);
            _maxBounds = b.ReadVector3(BinaryReaderExtensions.InputType.Double);

            long something2 = b.ReadInt64();
            ModelIndex = b.ReadInt16();
            short flags = b.ReadInt16();

            //Also loss of precision here.
            switch (ReferenceType)
            {
                case BulkReferenceType.Model:
                    RelativeTransform = b.ReadMatrix3x4(BinaryReaderExtensions.InputType.Double);
                    break;
                case BulkReferenceType.Unknown_09:
                    //RelativeTransform = b.ReadMatrix3x4(BinaryReaderExtensions.InputType.Double);
                    RelativeTransform = new Matrix3x4();
                    break;
                case BulkReferenceType.Decal:
                    //Pretty sure these are decals
                    RelativeTransform = b.ReadMatrix3x3_AS_3x4(BinaryReaderExtensions.InputType.Double);
                    //RelativeTransform.M14 = previousRef.RelativeTransform.M14;
                    //RelativeTransform.M24 = previousRef.RelativeTransform.M24;
                    //RelativeTransform.M34 = previousRef.RelativeTransform.M34;
                    break;
                 case BulkReferenceType.Unknown_00:
                    throw new Exception($"Unknown ref type: 0x{ReferenceType:X}");
                default:
                    throw new Exception($"Unknown ref type: 0x{ReferenceType:X}");
            }

            if (ReferenceType == BulkReferenceType.Model)
            {
                switch (footerType)
                {
                    case 0x08:
                    case 0x09:
                    case 0x0A:
                        b.ReadBytes(16);
                        break;
                    case 0x0C:
                        b.ReadBytes(24);
                        break;
                    case 0x0D:
                        b.ReadBytes(32);
                        break;
                }
            }
            else if (ReferenceType == BulkReferenceType.Unknown_09)
            {
                if (ID == 493)
                {
                    b.ReadBytes(512);
                }
                else if(ID == 622)
                {
                    b.ReadBytes(608);
                }
                else
                {

                }
            }

#if DEBUG
            Length = b.BaseStream.Position - Offset;
            b.BaseStream.Seek(Offset, SeekOrigin.Begin);
            ReferenceBytes = b.ReadBytes((int)Length); //Stream ends up where we left off
#endif
        }
    }

    public enum BulkReferenceType
    {
        Unknown_00  = 0x00,
        Model       = 0x01,
        Unknown_09  = 0x09,
        Decal       = 0x10
    }
}
