﻿using DataImport.Commands;
using Microsoft.SqlServer.Types;
using System;
using System.Collections.Generic;
using System.Text;
using TradeInSpace.Explore;
using DN = System.DoubleNumerics;

namespace DataImport.Utils
{
    public class Coordinate
    {
        internal Vector3 Source { get; set; }
        public Coordinate(double Lat, double Lon, double Alt = 0)
        {
            this.Latitude = Lat;
            this.Longitude = Lon;
            this.Altitude = Alt;
        }

        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public double Altitude { get; set; }

        public static Coordinate ConvertFromXYZ(Vector3 Vect)
        {
            return ConvertFromXYZ(Vect.X, Vect.Y, Vect.Z);
        }
        public static Coordinate ConvertFromXYZFlipped(Vector3 Vect)
        {
            return ConvertFromXYZ(Vect.X, Vect.Z, Vect.Y);
        }

        public static Coordinate ConvertFromXYZ(double X, double Y, double Z, bool NASACoords = true)
        {
            double xyLen = Math.Sqrt(X * X + Y * Y);
            double lat = Math.Atan2(Z, xyLen) * (180 / Math.PI);
            double lon = Math.Atan2(-X, Y) * (180 / Math.PI); //Adjusted here to match SC's coordinate system
            double alt = Math.Sqrt(X * X + Y * Y + Z * Z);

            if (NASACoords)
            {
                //90 for adjustment, and 360 to get an output range of 0-360deg
                return new Coordinate(lat, (lon + 90f + 360f) % 360f, alt) { Source = new Vector3(X, Y, Z) };
            }
            else
            {
                return new Coordinate(lat, lon, alt) { Source = new Vector3(X, Y, Z) };
            }
        }

        public SqlGeometry GetSQLGeometry()
        {
            return SqlGeometry.Point(Longitude, Latitude, GenerateShapefileCommand.World_SRID);
        }
    }

    public class CoordinateHelper
    {
        public static dynamic BuildLocationAdditionalProperties(Vector3 GlobalPosition, Vector3 ParentGlobalPosition, Vector4 GlobalRotation)
        {
            var childPosition = GlobalPosition - ParentGlobalPosition;
            //Don't need to transpose for just rotation
            DN.Matrix4x4.CreateFromQuaternion((DN.Quaternion)GlobalRotation).Decompose(out var a, out var childRotation, out var childScale);

            dynamic locationAdditional = new System.Dynamic.ExpandoObject();

            locationAdditional.LocalPosition = new
            {
                X = childPosition.X,
                Y = childPosition.Y,
                Z = childPosition.Z
            };
            locationAdditional.GlobalPosition = new
            {
                X = GlobalPosition.X,
                Y = GlobalPosition.Y,
                Z = GlobalPosition.Z
            };
            locationAdditional.Rotation = new
            {
                X = childRotation.X,
                Y = childRotation.Y,
                Z = childRotation.Z
            };

            return locationAdditional;
        }

        public static dynamic BuildLocationAdditionalProperties(DN.Matrix4x4 Transform, Vector3 ParentGlobalPosition)
        {
            DN.Matrix4x4.Transpose(Transform).Decompose(out var globalPosition, out var globalRotation, out var _3);

            return BuildLocationAdditionalProperties(globalPosition, ParentGlobalPosition, globalRotation);
        }
    }
}
