﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using Pfim;

namespace DataImport
{
    class ImageTools
    {
        public static byte[] ConvertDDSToPNG(byte[] ddsBytes)
        {
            using (var reader = new MemoryStream(ddsBytes))
            using (var image = Pfimage.FromStream(reader))
            {
                PixelFormat format;

                switch (image.Format)
                {
                    case Pfim.ImageFormat.Rgba32:
                        format = PixelFormat.Format32bppArgb;
                        break;
                    default:
                        throw new NotImplementedException();
                }

                var handle = GCHandle.Alloc(image.Data, GCHandleType.Pinned);
                try
                {
                    var data = Marshal.UnsafeAddrOfPinnedArrayElement(image.Data, 0);
                    var bitmap = new Bitmap(image.Width, image.Height, image.Stride, format, data);
                    using(var captureStream = new MemoryStream())
                    {
                        bitmap.Save(captureStream, System.Drawing.Imaging.ImageFormat.Png);
                        return captureStream.ToArray();
                    }
                }
                finally
                {
                    handle.Free();
                }
            }
        }
    }
}
