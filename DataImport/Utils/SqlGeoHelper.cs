﻿using DataImport.Commands;
using Microsoft.SqlServer.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataImport.Utils
{
    public class SQLGeoHelper
    {
        public static SqlGeography Line(Coordinate A, Coordinate B)
        {
            return GeographyLine(new Coordinate[] { A, B });
        }

        public static SqlGeography GeographyLine(IEnumerable<Coordinate> Coords, bool SelfClosing = false)
        {
            return BuildGeographyFromCoords(OpenGisGeographyType.LineString, Coords, SelfClosing);
        }

        public static SqlGeography GeographyPolygon(IEnumerable<Coordinate> Coords, bool SelfClosing = false)
        {
            return BuildGeographyFromCoords(OpenGisGeographyType.Polygon, Coords, SelfClosing);
        }

        public static SqlGeometry GeometryLine(IEnumerable<Coordinate> Coords, bool SelfClosing = false)
        {
            return BuildGeometryFromCoords(OpenGisGeometryType.LineString, Coords, SelfClosing);
        }

        public static SqlGeometry GeometryPolygon(IEnumerable<Coordinate> Coords, bool SelfClosing = false)
        {
            return BuildGeometryFromCoords(OpenGisGeometryType.Polygon, Coords, SelfClosing);
        }

        public static SqlGeometry GeometryPoint(Coordinate Coord)
        {
            return BuildGeometryFromCoords(OpenGisGeometryType.Point, new[] { Coord }, SelfClosing: false);
        }

        public static SqlGeometry FromMapArea(MapArea area)
        {
            switch (area.AreaType)
            {
                case TradeInSpace.Models.AreaType.Point:
                    return GeometryPoint(area.Center);
                case TradeInSpace.Models.AreaType.Line:
                case TradeInSpace.Models.AreaType.Polygon:
                    return GeometryLine(area.Coordinates);
            }
            return null;
        }

        public static SqlGeography BuildGeographyFromCoords(OpenGisGeographyType PolyType, IEnumerable<Coordinate> Coords, bool SelfClosing = false)
        {
            if (PolyType != OpenGisGeographyType.Point)
            {
                //Determine if we need to flip this before submitting.
                if (!(SignedPolygonArea(Coords) < 0)) //Indicates polygon is clockwise
                {
                    Coords = Coords.Reverse();
                }
            }

            SqlGeographyBuilder gb = new SqlGeographyBuilder();
            gb.SetSrid(GenerateShapefileCommand.World_SRID);
            gb.BeginGeography(PolyType);
            if (Coords.Count() > 0)
            {
                var start = Coords.First();
                gb.BeginFigure(start.Latitude, start.Longitude);
                foreach (var c in Coords.Skip(1))
                    gb.AddLine(c.Latitude, c.Longitude);
                if (SelfClosing)
                    gb.AddLine(start.Latitude, start.Longitude);
                gb.EndFigure();
            }
            gb.EndGeography();
            return gb.ConstructedGeography;
        }

        public static SqlGeometry BuildGeometryFromCoords(OpenGisGeometryType PolyType, IEnumerable<Coordinate> Coords, bool SelfClosing = false)
        {
            if (PolyType != OpenGisGeometryType.Point)
            {
                //Determine if we need to flip this before submitting.
                if (!(SignedPolygonArea(Coords) < 0)) //Indicates polygon is clockwise
                {
                    Coords = Coords.Reverse();
                }
            }

            SqlGeometryBuilder gb = new SqlGeometryBuilder();
            gb.SetSrid(0);
            gb.BeginGeometry(PolyType);
            if (Coords.Count() > 0)
            {
                var start = Coords.First();
                gb.BeginFigure((start.Longitude), start.Latitude);
                foreach (var c in Coords.Skip(1))
                    gb.AddLine((c.Longitude), c.Latitude);
                if (SelfClosing)
                    gb.AddLine((start.Longitude), start.Latitude);
                gb.EndFigure();
            }
            gb.EndGeometry();
            return gb.ConstructedGeometry;
        }


        private static double SignedPolygonArea(IEnumerable<Coordinate> coordinates)
        {
            var pts = coordinates.ToList();
            pts.Add(pts[0]);
            // Get the areas.
            double area = 0;
            for (int i = 0; i < pts.Count - 1; i++)
            {
                area +=
                    (pts[i + 1].Longitude - pts[i].Longitude) *
                    (pts[i + 1].Latitude + pts[i].Latitude) / 2;
            }

            // Return the result.
            return area;
        }
    }
}
