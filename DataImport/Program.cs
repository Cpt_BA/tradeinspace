﻿using DataImport.Importers;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Services;
using Google.Apis.Sheets.v4;
using Google.Apis.Util.Store;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Xml;
using Serilog.Sinks.File;
using System.Xml.Linq;
using TradeInSpace.Data;
using TradeInSpace.Models;
using static Google.Apis.Sheets.v4.SheetsService;
using static TradeInSpace.Models.Enums;
using Microsoft.Extensions.Logging;
using Serilog;
using Serilog.Events;
using DataImport.Actions;
using Azure.Storage.Blobs;
using CommandLine;
using DataImport.Commands;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.DependencyInjection;
using TradeInSpace.Explore;
using DataImport.Importers.SLPostProcess;
using DataImport.Utils;
using TradeInSpace.Explore.Subsumption;

namespace DataImport
{
    class Program
    {
        static string GetMode
        {
            get
            {
#if DEBUG
                return "Development";
#elif STAGING
                return "Staging";
#else
                return "Release";
#endif
            }
        }

        static void Main(string[] args)
        {
            var tisHost = new HostBuilder()
                .ConfigureAppConfiguration((hostContext, configApp) =>
                {
                    configApp.SetBasePath(Directory.GetCurrentDirectory());
                    configApp.AddJsonFile($"appsettings.json", optional: false, reloadOnChange: true);
                    configApp.AddJsonFile($"appsettings.{GetMode}.json", optional: true);
                })
                .UseSerilog((hostcontext, logConfig) =>
                {
                    logConfig.MinimumLevel.Debug();
                    logConfig.MinimumLevel.Override("Microsoft", LogEventLevel.Information);
                    logConfig.Enrich.FromLogContext();
                    logConfig.WriteTo.Console(restrictedToMinimumLevel: LogEventLevel.Information);
                    logConfig.WriteTo.File("output_log.txt", LogEventLevel.Debug, rollingInterval: RollingInterval.Day, retainedFileCountLimit: 31);
                    logConfig.WriteTo.File("output_wrn.txt", LogEventLevel.Warning, rollingInterval: RollingInterval.Day, retainedFileCountLimit: 31);
                })
                .ConfigureServices((hostContext, services) =>
                {
                    var config = hostContext.Configuration;

                    //Register our main database provider
                    services.AddSingleton<TISContext>((sp =>
                    {
                        var opts = new DbContextOptionsBuilder<TISContext>()
                            .UseSqlServer(config["ConnectionStrings:TradeInSpace"])
                            .Options;
                        var context = new TISContext(opts);
                        context.Database.SetCommandTimeout(TimeSpan.FromMinutes(10));

                        DBInitializer.Initialize(context);
                        Log.Information("Connected to and initialized DB.");

                        return context;
                    }));

                    //Also register local SC game settings
                    services.AddSingleton<LauncherSettings>(sp => SCLauncherReader.LoadGameSettings());

                    services.AddSingleton<ImportDatabaseAction>();
                    services.AddSingleton<ImportTradeTableAction>();
                    services.AddSingleton<ImportTransitDurationsAction>();
                    services.AddSingleton<GenerateShapefileAction>();

                    //Register all of the Importers
                    {
                        services.AddSingleton<GameComponentImporter>();
                        services.AddSingleton<GameEquipmentImporter>();
                        services.AddSingleton<GameShipImporter>();
                        services.AddSingleton<MiningCompositionImporter>();
                        services.AddSingleton<MiningElementImporter>();
                        services.AddSingleton<MissionLocationImporter>();
                        services.AddSingleton<SystemBodyImporter>();
                        services.AddSingleton<SystemLocationImporter>();
                        services.AddSingleton<HarvestableImporter>();
                        services.AddSingleton<TradeImporter>();
                        services.AddSingleton<TradeItemImporter>();
                        services.AddSingleton<LocationTradeServicesImporter>();
                        services.AddSingleton<MissionImporter>();
                        services.AddSingleton<ManufacturerImporter>();
                    }

                    //Then register the post-processors
                    {
                        services.AddSingleton<SystemLocationPostProcessImporter>();
                        services.AddSingleton<SubsumptionPostProcessImporter>();
                        services.AddSingleton<CriminalTerminalPostProcessor>();
                        services.AddSingleton<GreenZonePostProcessor>();
                        services.AddSingleton<HarvestableProviderPostProcessor>();
                        services.AddSingleton<RespawnPointPostProcessor>();
                        services.AddSingleton<SystemLandingZonePostProcessor>();
                        services.AddSingleton<TradeShopPostProcessor>();
                    }

                    //And finally the main application commands and services.
                    services.AddSingleton<SCImportCommand>();
                    services.AddSingleton<StationTimeImportCommand>();
                    services.AddSingleton<GenerateShapefileCommand>();
                    services.AddSingleton<RenderModelsCommand>();
                    services.AddSingleton<RenderPlanetsCommand>();

                    //Misc
                    services.AddSingleton<SubsumptionManager>();

                    services.AddHostedService<DataImportService>();
                })
                .UseConsoleLifetime()
                .Build();

            tisHost.Run();
        }

        private static IEnumerable<XmlElement> SearchInner(XmlElement element, string propName)
        {
            foreach (XmlElement node in element.ChildNodes)
            {
                if(node.Attributes.Cast<XmlAttribute>().Any(a => a.Name.Contains(propName, StringComparison.InvariantCultureIgnoreCase)))
                {
                    yield return node;
                }

                foreach(var innerMatch in SearchInner(node, propName))
                {
                    yield return innerMatch;
                }
            }
        }
    }
}
