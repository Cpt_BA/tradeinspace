﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataImport.Data
{
    public class Constants
    {
        public static StationTransitDuration[] TransitDurations = new StationTransitDuration[]
        {
            new StationTransitDuration("Levski - AdminTerminal", 30, "Elevators", 0, "", 30),
            new StationTransitDuration("Grim HEX - AdminTerminal", 0, "", 0, "", 90),
            new StationTransitDuration("Port Olisar - AdminTerminal", 0    ,"", 0,"", 30),
            new StationTransitDuration("Area18 - AdminTerminal", 300  ,"Tram", 0,"", 300),
            new StationTransitDuration("R&R CRU-L1 - AdminTerminal", 120  ,"Elevators", 0,"",  45   ),
            new StationTransitDuration("R&R CRU-L4 - AdminTerminal", 120  ,"Elevators", 0,"",  45   ),
            new StationTransitDuration("R&R CRU-L5 - AdminTerminal", 120  ,"Elevators", 0,"",   45   ),
            new StationTransitDuration("R&R HUR-L1 - AdminTerminal", 120  ,"Elevators", 0,"",   45   ),
            new StationTransitDuration("R&R HUR-L2 - AdminTerminal", 120  ,"Elevators", 0,"",   45   ),
            new StationTransitDuration("R&R HUR-L3 - AdminTerminal", 120  ,"Elevators", 0,"",   45   ),
            new StationTransitDuration("R&R HUR-L4 - AdminTerminal", 120  ,"Elevators", 0,"",   45   ),
            new StationTransitDuration("R&R HUR-L5 - AdminTerminal", 120  ,"Elevators", 0,"",   45   ),
            new StationTransitDuration("R&R ARC-L1 - AdminTerminal", 120  ,"Elevators", 0,"",   45   ),
            new StationTransitDuration("Lorville - AdminTerminal", 300  ,"Tram", 0,"",   300  ),
            new StationTransitDuration("Shubin Mining Facility SCD-1 - AdminTerminal", 0    ,"", 0,"",   30   ),
            new StationTransitDuration("Tram & Myers Mining - AdminTerminal", 0    ,"", 0,"",   30   ),
            new StationTransitDuration("ArcCorp Mining Area 141 - AdminTerminal", 0    ,"", 0,"",   30   ),
            new StationTransitDuration("Kudre Ore - AdminTerminal", 0    ,"", 0,"",   30   ),
            new StationTransitDuration("Benson Mining Outpost - AdminTerminal", 0    ,"", 0,"",   30   ),
            new StationTransitDuration("ArcCorp Mining Area 157 - AdminTerminal", 0    ,"", 0,"",   30   ),
            new StationTransitDuration("HDMS-Bezdek - AdminTerminal", 0    ,"", 0,"",   30   ),
            new StationTransitDuration("HDMS-Lathan - AdminTerminal", 0    ,"", 0,"",   30   ),
            new StationTransitDuration("HDMS-Norgaard - AdminTerminal", 0    ,"", 0,"",   30   ),
            new StationTransitDuration("HDMS-Anderson - AdminTerminal", 0    ,"", 0,"",   30   ),
            new StationTransitDuration("HDMS-Hahn - AdminTerminal", 0    ,"", 0,"",   30   ),
            new StationTransitDuration("HDMS-Perlman - AdminTerminal", 0    ,"", 0,"",   30   ),
            new StationTransitDuration("HDMS-Woodruff - AdminTerminal", 0    ,"", 0,"",   30   ),
            new StationTransitDuration("HDMS-Ryder - AdminTerminal", 0    ,"", 0,"",   30   ),
            new StationTransitDuration("HDMS-Edmond - AdminTerminal", 0    ,"", 0,"",   30   ),
            new StationTransitDuration("HDMS-Oparei - AdminTerminal", 0    ,"", 0,"",   30   ),
            new StationTransitDuration("HDMS-Pinewood - AdminTerminal", 0    ,"", 0,"",   30   ),
            new StationTransitDuration("HDMS-Thedus - AdminTerminal", 0    ,"", 0,"",   30   ),
            new StationTransitDuration("HDMS-Hadley - AdminTerminal", 0    ,"", 0,"",   30   ),
            new StationTransitDuration("HDMS-Stanhope - AdminTerminal", 0    ,"", 0,"",   30   ),
            new StationTransitDuration("Humboldt Mines - AdminTerminal", 0    ,"", 0,"",   30   ),
            new StationTransitDuration("Loveridge Mineral Reserve - AdminTerminal", 0    ,"", 0,"",   30   ),
            new StationTransitDuration("Shubin Mining Facility SAL-2 - AdminTerminal", 0    ,"", 0,"",   30   ),
            new StationTransitDuration("Shubin Mining Facility SAL-5 - AdminTerminal", 0    ,"", 0,"",   30   ),
            new StationTransitDuration("ArcCorp Mining Area 045 - AdminTerminal", 0    ,"", 0,"",   30   ),
            new StationTransitDuration("ArcCorp Mining Area 048 - AdminTerminal", 0    ,"", 0,"",   30   ),
            new StationTransitDuration("ArcCorp Mining Area 056 - AdminTerminal", 0    ,"", 0,"",   30   ),
            new StationTransitDuration("ArcCorp Mining Area 061 - AdminTerminal", 0    ,"", 0,"",   30   ),
            new StationTransitDuration("Gallete Family Farms - AdminTerminal", 0    ,"", 0,"",   30   ),
            new StationTransitDuration("Terra Mills HydroFarm - AdminTerminal", 0    ,"", 0,"",   30   ),
            new StationTransitDuration("Bountiful Harvest Hydroponics - AdminTerminal", 0    ,"", 0,"",   30   ),
            new StationTransitDuration("Hickes Research Outpost - AdminTerminal", 0    ,"", 0,"",   30   ),
            new StationTransitDuration("Deakins Research Outpost - AdminTerminal", 0    ,"", 0,"",   30   ),
            new StationTransitDuration("Levski - AdminRefinery", 30   ,"Elevators  ", 0,"",   30   ),
            new StationTransitDuration("Grim HEX - AdminRefinery", 0    ,"", 0,"",   0    ),
            new StationTransitDuration("Port Olisar - AdminRefinery", 0    ,"", 0,"",   0    ),
            new StationTransitDuration("Lorville - AdminRefinery", 300  ,"Tram", 0,"",   90   ),
            new StationTransitDuration("Area18 - AdminRefinery", 300  ,"Tram", 0,"",   300  ),
            new StationTransitDuration("Lorville - CommExTransfers", 300  ,"Tram", 0,"",   60   ),
            new StationTransitDuration("Area18 - TDD", 300  ,"Tram", 0,"",   60   ),
            new StationTransitDuration("Jumptown - AdminTerminal", 0    ,"", 0,"",   0    ),
            new StationTransitDuration("NT-999-XX - AdminTerminal", 0    ,"", 0,"",   0    ),
            new StationTransitDuration("PRIVATE PROPERTY - AdminTerminal", 0    ,"", 0,"",   0    ),
            new StationTransitDuration("Nuen Waste Management - AdminTerminal", 0    ,"", 0,"",   0    ),
            new StationTransitDuration("Paradise Cove - AdminTerminal", 0    ,"", 0,"",   0    ),
            new StationTransitDuration("The Orphanage - AdminTerminal", 0    ,"", 0,"",   0    ),
            new StationTransitDuration("Reclamation & Disposal Orinth - AdminTerminal", 0    ,"", 0,"",   0    ),
            new StationTransitDuration("Brio's Breaker Yard - AdminTerminal", 0    ,"", 0,"",   0    ),
            new StationTransitDuration("Samson & Son's Salvage Center - AdminTerminal", 0    ,"", 0,"",   0    ),
            new StationTransitDuration("R&R CRU-L4 - FenceTerminal", 0    ,"", 0,"",   0    ),
            new StationTransitDuration("R&R CRU-L5 - FenceTerminal", 0    ,"", 0,"",   0    ),
        };

        public struct StationTransitDuration
        {
            public string Name;

            public int TransitDuration;
            public string TransitNotes;

            public int TravelDuration;
            public string TravelNotes;

            public int RunningDuration;

            public StationTransitDuration(string name, int transitDuration, string transitNotes, int travelDuration, string travelNotes, int runningDuration)
            {
                Name = name;
                TransitDuration = transitDuration;
                TransitNotes = transitNotes;
                TravelDuration = travelDuration;
                TravelNotes = travelNotes;
                RunningDuration = runningDuration;
            }
        }
    }
}
