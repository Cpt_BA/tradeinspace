﻿using CommandLine;
using DataImport.Commands;
using DataImport.Utils;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

namespace DataImport
{
    [Verb("import_all")]
    public class ImportAllCommandArgs
    {
        [Option(HelpText = "SC version to import - must be downloaded and installed. (LIVE or PTU)", Default = "LIVE")]
        public string Channel { get; set; }
        [Option(HelpText = "ID of already-imported game database to export to google docs for manual trade table adjustments. Uses latest if not supplied.", Required = false)]
        public int GameDB_ID { get; set; }
        [Option(HelpText = "[ignore or reset] What to do when a matching version is found.")]
        public string Collision { get; set; }
    }

    class DataImportService : IHostedService, IDisposable
    {
        public IApplicationLifetime AppLifetime { get; }
        public SCImportCommand SCImportCommand { get; }
        public StationTimeImportCommand StationTimeCommand { get; }
        public GenerateShapefileCommand ShapeCommand { get; }
        public RenderModelsCommand RenderModelsCommand { get; }
        public RenderPlanetsCommand RenderPlanetsCommand { get; }

        public DataImportService(IApplicationLifetime AppLifetime, 
            SCImportCommand scImportcommand,
            StationTimeImportCommand stationTimeCommand,
            GenerateShapefileCommand shapeCommand,
            RenderModelsCommand renderModelsCommand,
            RenderPlanetsCommand renderPlanetsCommand)
        {
            this.AppLifetime = AppLifetime;
            SCImportCommand = scImportcommand;
            StationTimeCommand = stationTimeCommand;
            ShapeCommand = shapeCommand;
            RenderModelsCommand = renderModelsCommand;
            RenderPlanetsCommand = renderPlanetsCommand;
        }

        public async Task StartAsync(CancellationToken cancellationToken)
        {
            var verbTypes = Assembly.GetExecutingAssembly()
                .GetTypes()
                .Where(t => t.GetCustomAttribute<VerbAttribute>() != null)
                .ToArray();

            Parser.Default.ParseArguments(Environment.GetCommandLineArgs().Skip(1).ToArray(), verbTypes)
                .WithParsed((result) =>
                {
                    switch (result)
                    {
                        case SCImportCommandArgs cmdArgs:
                            SCImportCommand.ExecuteCommand(cmdArgs);
                            break;
                        case StationTimeImportCommandArgs cmdArgs:
                            StationTimeCommand.ExecuteCommand(cmdArgs);
                            break;
                        case ImportAllCommandArgs cmdArgs:
                            SCImportCommand.ExecuteCommand(new SCImportCommandArgs()
                            {
                                Channel = cmdArgs.Channel,
                                Collision = cmdArgs.Collision
                            });
                            StationTimeCommand.ExecuteCommand(new StationTimeImportCommandArgs()
                            {
                                Channel = cmdArgs.Channel,
                                GameDB_ID = cmdArgs.GameDB_ID
                            });
                            ShapeCommand.ExecuteCommand(new GenerateShapefileCommandArgs()
                            {
                                Channel = cmdArgs.Channel,
                                GameDB_ID = cmdArgs.GameDB_ID,
                                Target = "stantonsystem"
                            });
                            break;
                        case GenerateShapefileCommandArgs cmdArgs:
                            ShapeCommand.ExecuteCommand(cmdArgs);
                            break;
                        case RenderModelsCommandArgs cmdArgs:
                            RenderModelsCommand.ExecuteCommand(cmdArgs);
                            break;
                        case RenderPlanetsCommandArgs cmdArgs:
                            RenderPlanetsCommand.ExecuteCommand(cmdArgs);
                            break;
                    }
                })
                .WithNotParsed((IEnumerable<Error> err) =>
                {
                    Log.Error($"Invalid Args: {string.Join(' ', Environment.GetCommandLineArgs())}");

                    throw new Exception(string.Join(Environment.NewLine, err.Select(e => JsonConvert.SerializeObject(e))));
                });


            Log.Information("Application completed. Shutting down...");
            //ASP.Net Hosting core is designed to continuall host a (web) application itself
            //So we just signal that to close the app upon execution completion
            AppLifetime.StopApplication();
        }

        public async Task StopAsync(CancellationToken cancellationToken)
        {
        }

        public void Dispose()
        {
        }
    }
}
