﻿using CommandLine;
using DataImport.Utils;
using Google.Apis.Sheets.v4.Data;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using TradeInSpace.Data;
using TradeInSpace.Models;

namespace DataImport.Commands
{
    [Verb("time_import")]
    public class StationTimeImportCommandArgs
    {
        [Option(HelpText = "SC version to import - must be downloaded and installed. (LIVE or PTU)", Default = "LIVE")]
        public string Channel { get; set; }
        [Option(HelpText = "ID of already-imported game database to export to google docs for manual trade table adjustments. Uses latest if not supplied.", Required = false)]
        public int GameDB_ID { get; set; }
    }

    public class StationTimeImportCommand : DataCommand<StationTimeImportCommandArgs>
    {
        public StationTimeImportCommand(TISContext siteDB)
            : base(siteDB)
        {
        }


        public override bool ExecuteCommand(StationTimeImportCommandArgs cmdArgs)
        {
            var gameDB = GetGameDatabase(cmdArgs.GameDB_ID, cmdArgs.Channel);

            var locationTimes = Data.Constants.TransitDurations.ToDictionary(r => r.Name);

            foreach(var shop in gameDB.TradeShops.ToList())
            {
                if (locationTimes.ContainsKey(shop.Key))
                {
                    var shopTimes = locationTimes[shop.Key];

                    shop.TransitDurationS = shopTimes.TransitDuration;
                    shop.TransitNotes = shopTimes.TransitNotes;
                    shop.TravelDurationS = shopTimes.TravelDuration;
                    shop.TravelNotes = shopTimes.TravelNotes;
                    shop.RunningDurationS = shopTimes.RunningDuration;
                }
            }

            SiteDB.SaveChanges();

            return true;
        }
    }
}
