﻿using CommandLine;
using Newtonsoft.Json.Linq;
using OSGeo.GDAL;
using OSGeo.OGR;
using Serilog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using TradeInSpace.Data;
using TradeInSpace.Explore;
using TradeInSpace.Explore.DataForge;
using TradeInSpace.Explore.Socpak;
using TradeInSpace.Export;
using TradeInSpace.Export.Planet;

namespace DataImport.Commands
{
    [Verb("render_planets")]
    internal class RenderPlanetsCommandArgs
    {
        [Option(HelpText = "SC channel to import - must be downloaded and installed. (LIVE or PTU)", Default = "LIVE")]
        public string Channel { get; set; }

        [Option('h', "hillshade", Required = false, Default = false)]
        public bool ComputeHillshading { get; set; }
        [Option('s', "scale", Required = false, Default = 4)]
        public int RenderScale { get; set; }


        [Option('f', "force", Required = false, Default = false, HelpText = "When supplied: Remove and re-render all outputs files.")]
        public bool Force { get; set; }


        [Option('o', "output", Required = true)]
        public string OutputDirectory { get; set; }
    }

    internal class RenderPlanetsCommand : DataCommand<RenderPlanetsCommandArgs>
    {
        public RenderPlanetsCommand(TISContext SiteDB)
            : base(SiteDB)
        {
        }

        public override bool ExecuteCommand(RenderPlanetsCommandArgs cmdArg)
        {
            GdalConfiguration.ConfigureGdal();
            GdalConfiguration.ConfigureOgr();

            Gdal.AllRegister();
            Ogr.RegisterAll();

            var launcherSettings = SCLauncherReader.LoadGameSettings();
            var rawGameDB = DFDatabase.ParseFromSettings(launcherSettings, cmdArg.Channel);
            var renderer = new PlanetRenderer(rawGameDB);

            if (rawGameDB.Subsumption == null)
            {
                rawGameDB.InitSubsumption();
            }
            rawGameDB.Subsumption.LoadAllSystems();
            rawGameDB.Subsumption.LoadAllChildren(PreloadDetails: false, PreloadEntities: false, LoadChildren: true);

            foreach (var systemEntry in rawGameDB.Subsumption.AllNodes)
            {
                try
                {
                    if (systemEntry?.SOC_Entry?.Class == "OrbitingObjectContainer" &&
                        systemEntry.SOC_Entry.ArchivePath != null)
                    {
                        SOC_Archive ooc_archive = SOC_Archive.LoadFromSubsubmption(rawGameDB, systemEntry, rawGameDB.Subsumption.MasterRoot);
                        if (ooc_archive == null) continue;

                        ooc_archive.RootEntry.LoadFullDetails();
                        ooc_archive.RootEntry.LoadAllEntities(false, false, false);
                        if (ooc_archive.RootEntry.PlanetEntity == null) continue;

                        Log.Information($"Rendering planet: {ooc_archive.Name}");

                        RenderSingle(cmdArg, rawGameDB, renderer, ooc_archive);
                    }
                }
                catch (Exception ex)
                {
                    Log.Error(ex, $"Error rendering planet: {ex.Message}");
                }
            }

            return true;
        }

        private void RenderSingle(RenderPlanetsCommandArgs args, DFDatabase gamedb, PlanetRenderer renderer, SOC_Archive planetArchive)
        {
            var planetJson = JObject.Parse(planetArchive.RootEntry.PlanetJSON);
            var planetData = PlanetData.CreateFromPLAJson(gamedb, planetArchive.Name, planetJson, renderer);

            var final_path = Path.Combine(args.OutputDirectory, Path.ChangeExtension(planetArchive.Name, ".gpkg"));

            if (File.Exists(final_path) && !args.Force)
            {
                Log.Information($"Planet {planetArchive.Name} already rendered. Skipping");
                return;
            }

            var planetSettings = new RenderSettings()
            {
                TileSize = args.RenderScale,
                SplatMapRenderArea = new System.Drawing.Rectangle(0, 0, planetData.TileCount, planetData.TileCount),
                WIP_HillShading = args.ComputeHillshading
            };

            renderer.RenderWholeSurface(final_path, "GPKG", planetData, planetSettings);
        }
    }
}
