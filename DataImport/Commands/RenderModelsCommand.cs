﻿using CommandLine;
using Serilog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using TradeInSpace.Data;
using TradeInSpace.Explore;
using TradeInSpace.Explore.DataForge;
using TradeInSpace.Explore.SC;
using TradeInSpace.Explore.SC.Components;
using TradeInSpace.Explore.SC.Data;
using TradeInSpace.Explore.Subsumption;
using TradeInSpace.Export;
using UkooLabs.FbxSharpie;

namespace DataImport.Commands
{
    [Verb("render_models")]
    internal class RenderModelsCommandArgs
    {
        [Option(HelpText = "SC channel to import - must be downloaded and installed. (LIVE or PTU)", Default = "LIVE")]
        public string Channel { get; set; }

        [Option('s', "vehicle_skeleton", Required = false, Default = false)]
        public bool ExportVehicleSkeleton { get; set; }
        [Option('m', "vehicle_model", Required = false, Default = false)]
        public bool ExportVehicleModel { get; set; }
        [Option('e', "equipment", Required = false, Default = false)]
        public bool ExportEquipment { get; set; }

        [Option('c', "no_collect", HelpText = "When specified, do not collect models when running in vehicle_skeleton mode.", Required = false, Default = false)]
        public bool SkipModelCollection { get; set; }

        [Option("type_filter", Required = false, Default = null)]
        public string EquipmentTypeFilter { get; set; }
        [Option('f', "force", Required = false, Default = false, HelpText = "When supplied: Remove and re-render all outputs files.")]
        public bool Force { get; set; }


        [Option('o', "output", Required = true)]
        public string OutputDirectory { get; set; }
    }

    internal class RenderModelsCommand : DataCommand<RenderModelsCommandArgs>
    {
        SOC_Explorer.SearchConfig EquipmentMegaModelConfiguration = new SOC_Explorer.SearchConfig()
        {
            LoadEntities = true,
            LoadReferences = true,
            Recursive = true,
            DummyEntries = true,
            DynamicComponents = true,
            LoadModelStructure = true,
            LoadModelGeometry = true,
            IncludeModelReferences = false,
            ApplyShipLoadout = false
        };

        SOC_Explorer.SearchConfig MegaModelConfiguration = new SOC_Explorer.SearchConfig()
        {
            LoadEntities = true,
            LoadReferences = true,
            Recursive = true,
            DummyEntries = true,
            DynamicComponents = true,
            LoadModelStructure = true,
            LoadModelGeometry = true,
            IncludeModelReferences = false,
            ApplyShipLoadout = true
        };

        SOC_Explorer.SearchConfig ShipSkeletonConfiguration = new SOC_Explorer.SearchConfig()
        {
            LoadEntities = true,
            LoadReferences = true,
            Recursive = true,
            DummyEntries = true,
            DynamicComponents = true,
            LoadModelStructure = false,
            LoadModelGeometry = false,
            IncludeModelReferences = true,
            ApplyShipLoadout = false
        };

        SOC_Explorer.SearchConfig CollectModelsConfiguration = new SOC_Explorer.SearchConfig()
        {
            LoadEntities = true,
            LoadReferences = true,
            Recursive = true,
            DummyEntries = false,
            DynamicComponents = true,
            LoadModelStructure = true,
            LoadModelGeometry = false,
            IncludeModelReferences = true,
            ApplyShipLoadout = true
        };

        public RenderModelsCommand(TISContext siteDB)
            : base(siteDB)
        {
            
        }

        public override bool ExecuteCommand(RenderModelsCommandArgs cmdArg)
        {
            //Load Game PKG file
            var launcherSettings = SCLauncherReader.LoadGameSettings();
            var rawGameDB = DFDatabase.ParseFromSettings(launcherSettings, cmdArg.Channel);
            rawGameDB.ParseDFContent();

            if (cmdArg.ExportEquipment)
            {
                RenderEquipment(rawGameDB, cmdArg);
            }

            if (cmdArg.ExportVehicleSkeleton)
            {
                var collect_models = !cmdArg.SkipModelCollection;
                RenderAllVehilces("Vehicle_Skeleton", collect_models, rawGameDB, cmdArg, ShipSkeletonConfiguration);
            }

            if (cmdArg.ExportVehicleModel)
            {
                RenderAllVehilces("Vehicle_Complete", false, rawGameDB, cmdArg, MegaModelConfiguration);
            }

            if (!cmdArg.SkipModelCollection)
            {
                RenderUsedModels(rawGameDB, cmdArg, CollectModelsConfiguration);
                RenderUsedModels(rawGameDB, cmdArg, ShipSkeletonConfiguration);
                //Mega-Model does not reference ships.
            }

            return true;
        }

        public void RenderEquipment(DFDatabase gameDatabase, RenderModelsCommandArgs Args)
        {
            void _RenderSingle(SCEntityClassDef equipmentEntity)
            {
                var attachable = equipmentEntity?.GetComponent<AttachableComponent>();

                if (attachable != null)
                {
                    var equipment_dir = Path.Combine(Args.OutputDirectory, "Equipment");
                    var fbx_path = Path.Combine(equipment_dir, Path.ChangeExtension(equipmentEntity.ID.ToString(), ".fbx"));

                    if (File.Exists(fbx_path))
                    {
                        if (Args.Force)
                            File.Delete(fbx_path);
                        else
                            return;
                    }

                    var structure = SOC_Explorer.ExploreEquipment(gameDatabase, equipmentEntity, EquipmentMegaModelConfiguration);
                    CryToFBXExporter.WriteHierarchy(structure, fbx_path, gameDatabase);
                }
            }

            var ents = gameDatabase
                .GetComponentsOnEntities<AttachableComponent>()
                .Where(c => c.Type != "NOITEM_Vehicle")
                .Select(c => c.ParentEntity)
                .ToList();
            for (int i = 0; i < ents.Count; i++)
            {
                var entity = ents[i];
                try
                {
                    _RenderSingle(entity);
                }
                catch (Exception ex)
                {
                    Log.Error(ex, "Entity render failed.");
                }
                if (i % 500 == 0)
                {
                    Log.Information($"{i} / {ents.Count} Entities.");
                }
            }
        }

        public void RenderAllVehilces(string PassName, bool CollectModels, DFDatabase GameDatabase, RenderModelsCommandArgs Args, 
            SOC_Explorer.SearchConfig SearchConfig)
        {
            void _RenderSingle(Vehicle _VehicleComponent)
            {
                var vehicle_ent = _VehicleComponent.ParentEntity;
                var fbx_path = Path.Combine(Args.OutputDirectory, PassName, Path.ChangeExtension(vehicle_ent.ID.ToString(), ".fbx"));

                if (File.Exists(fbx_path))
                {
                    if (Args.Force)
                        File.Delete(fbx_path);
                    else
                        return;
                }

                if (CollectModels)
                {
                    Log.Information($"[{PassName}] Collecting Models - {vehicle_ent.InstanceName}");
                    SOC_Explorer.ExploreShip(GameDatabase, _VehicleComponent, CollectModelsConfiguration);
                }

                Log.Information($"[{PassName}] Exploring - {vehicle_ent.InstanceName}");
                var structure = SOC_Explorer.ExploreShip(GameDatabase, _VehicleComponent, SearchConfig);

                CryToFBXExporter.WriteHierarchy(structure, fbx_path, GameDatabase);
            }

            var vehicleInsurance = GameDatabase
                .GetEntries<ShipInsurance>()
                .Single()
                .Ships
                .Select(ir => ir.ShipClass);

            var vehicles = GameDatabase
                .GetComponentsOnEntities<Vehicle>()
                .Where(v => vehicleInsurance.Contains(v.ParentEntity.InstanceName, StringComparer.OrdinalIgnoreCase))
                .ToList();
            for (int i = 0; i < vehicles.Count; i++)
            {
                try
                {
                    _RenderSingle(vehicles[i]);
                }
                catch (Exception ex)
                {
                    Log.Error(ex, "Vehicle render failed.");
                }
                if (i % 500 == 0)
                {
                    Log.Information($"Vehicles - {PassName} - {i} / {vehicles.Count}.");
                }
            }
        }

        public void RenderUsedModels(DFDatabase GameDatabase, RenderModelsCommandArgs Args, SOC_Explorer.SearchConfig SearchConfig)
        {
            void _RenderSingle(string _ModelPath)
            {
                var fbx_path = Path.Combine(Args.OutputDirectory, "Models", Path.ChangeExtension(_ModelPath, ".fbx"));

                if (File.Exists(fbx_path))
                {
                    if (Args.Force)
                        File.Delete(fbx_path);
                    else
                        return;
                }

                var modelCGA = GameDatabase.ReadPackedCGAFile(_ModelPath);

                try
                {
                    modelCGA.ProcessCryengineFiles();
                }
                catch (Exception ex)
                {
                    Log.Error(ex, $"Error dumping model: {_ModelPath}");
                    return;
                }

                var fbx_scene = CryToFBXExporter.ConvertModelToScene(modelCGA);
                CryToFBXExporter.WriteScene(fbx_scene, fbx_path);
            }

            for (int i = 0; i < SearchConfig.UsedModels.Count; i++)
            {
                try
                {
                    _RenderSingle(SearchConfig.UsedModels[i]);
                }
                catch (Exception ex)
                {
                    Log.Error(ex, "Used model render failed.");
                }
                if (i % 500 == 0)
                {
                    Log.Information($"Used Models - {i} / {SearchConfig.UsedModels.Count}.");
                }
            }
        }
    }
}
