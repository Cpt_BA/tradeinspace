﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TradeInSpace.Data;
using TradeInSpace.Models;

namespace DataImport.Commands
{
    public abstract class DataCommand<T>
    {
        public DataCommand(TISContext SiteDB)
        {
            this.SiteDB = SiteDB;
        }

        public TISContext SiteDB { get; }

        public abstract bool ExecuteCommand(T cmdArgs);

        internal GameDatabase GetGameDatabase(int GameDB_ID, string Channel = "LIVE")
        {
            var gameDB = SiteDB
                .GameDatabases
                .Include(gd => gd.TradeTables).ThenInclude(tt => tt.Entries)
                .Include(gd => gd.TradeTables).ThenInclude(tt => tt.EquipmentEntries)
                .Include(gd => gd.TradeShops).ThenInclude(ts => ts.Location).ThenInclude(l => l.ChildLocations)
                .Include(gd => gd.TradeShops).ThenInclude(ts => ts.Location).ThenInclude(l => l.ParentBody)
                .Include(gd => gd.TradeShops).ThenInclude(ts => ts.Location).ThenInclude(l => l.ParentLocation)
                .Include(gd => gd.Equipment)
                .Include(gd => gd.TradeItems)
                .Include(gd => gd.SystemBodies)
                .SingleOrDefault(g => g.FinishedImporting && g.ID == GameDB_ID && g.DB_Channel == Channel);

            if (gameDB == null)
            {
                gameDB = SiteDB
                    .GameDatabases
                    .Where(g => g.FinishedImporting && g.DB_Channel == Channel && g.Enabled)
                    .Include(gd => gd.TradeTables)
                    .Include(gd => gd.TradeTables).ThenInclude(tt => tt.Entries)
                    .Include(gd => gd.TradeTables).ThenInclude(tt => tt.EquipmentEntries)
                    .Include(gd => gd.TradeShops).ThenInclude(ts => ts.Location).ThenInclude(l => l.ChildLocations)
                    .Include(gd => gd.TradeShops).ThenInclude(ts => ts.Location).ThenInclude(l => l.ParentBody)
                    .Include(gd => gd.TradeShops).ThenInclude(ts => ts.Location).ThenInclude(l => l.ParentLocation)
                    .Include(gd => gd.Equipment)
                    .Include(gd => gd.TradeItems)
                    .Include(gd => gd.SystemBodies)
                    .OrderByDescending(gd => gd.FinishedImporting)
                    .First();
            }



            if (gameDB == null)
            {
                throw new Exception("No valid game db imported.");
            }

            return gameDB;
        }
    }
}
