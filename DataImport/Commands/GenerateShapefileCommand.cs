﻿using CommandLine;
using DataImport.Utils;
using NetTopologySuite.Features;
using NetTopologySuite.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;
using Serilog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;
using System.Xml;
using TradeInSpace.Data;
using TradeInSpace.Explore;
using TradeInSpace.Explore.DataForge;
using TradeInSpace.Explore.Socpak;
using TradeInSpace.Explore.Socpak.Components;
using TradeInSpace.Explore.Subsumption;
using TradeInSpace.Export;
using TradeInSpace.Models;

namespace DataImport.Commands
{
    using NTSG = NetTopologySuite.Geometries;

    [Verb("generate_shape")]
    public class GenerateShapefileCommandArgs
    {
        [Option(HelpText = "SC channel to import - must be downloaded and installed. (LIVE or PTU)", Default = "LIVE")]
        public string Channel { get; set; }
        [Option(HelpText = "ID of already-imported game database to export to google docs for manual trade table adjustments. Uses latest if not supplied.", Required = false)]
        public int GameDB_ID { get; set; }
        [Option(HelpText = "Key of the target to map. ie: stantonsystem, delamar, new_babbage, etc.", Required = true)]
        public string Target { get; set; }

        [Option('p', HelpText = "Only generate qgis project files for planets. Do not render shapefile information.", Default = false)]
        public bool ProjectOnly { get; set; }
    }

    public class GenerateShapefileCommand : DataCommand<GenerateShapefileCommandArgs>
    {
        GameDatabase siteDB;

        public static double Deg2Rad = Math.PI / 180;
        public static int World_SRID = 4326;
        Dictionary<string, Dictionary<string, string[]>> EntityLookupCache;
        List<MapArea> areaBuffer = new List<MapArea>();
        DFDatabase rawGameDB;

        MapImportSettings mapSettings;
        ModelCache sizeCache;
        MapTagStack tagStack = new MapTagStack();

        public bool EntityPropertyCaching = false;

        string entityCachePath = "entity_tag_cache.json";
        string mapRenderPath = @"D:\maps\";
        string mapConfigPath = @"D:\maps\mapproxy";
        string mapSeedPath   = @"D:\maps\mapproxy_seeds";

        bool fullExport = true;
        bool saveToDB = false;

        List<Regex> IgnoreModelPaths = new List<Regex>()
        {
            new Regex(@"[_\W](debirs|flora|solar_panel|decal|trash|light|beam|pile|lights|signage|soil|sign|signs|lighting|advert|border_dressing|decals|logo|proxy)[_\W]"),
        };
        List<Regex> IgnoreModelPathsAggressive = new List<Regex>()
        {
            new Regex(@"[_\W](rock|debirs|cable|flora|solar_panel|decal|trash|light|beam|pile|lights|mound|dish|geology|signage|soil|sign|signs|flower|lighting|advert|border_dressing|decals|logo|proxy)[_\W]"),
        };


        SOC_Explorer.SearchConfig SearchConfiguration = new SOC_Explorer.SearchConfig()
        {
            LoadEntities = true,
            LoadReferences = true,
            Recursive = true
        };

        public GenerateShapefileCommand(TISContext siteDB, SubsumptionManager subsumptionManager)
            : base(siteDB)
        {
            subManager = subsumptionManager;
        }

        public override bool ExecuteCommand(GenerateShapefileCommandArgs cmdArg)
        {
            if (!RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
            {
                mapRenderPath = "/sc_data/map_data";
                mapConfigPath = "/sc_data/map_configs";
                mapSeedPath = "/sc_data/map_seeds";
            }

            //Cache entity property lookups, this saves a lot of time while debugging, but leads to stale data when in production
#if DEBUG
            EntityPropertyCaching = true;
#endif

            //TODO: Args loading?
            mapSettings = JsonConvert.DeserializeObject<MapImportSettings>(File.ReadAllText("map_export_config.json"));

            /*
            var csFactory = NTSG.Implementation.DotSpatialAffineCoordinateSequenceFactory.Instance;
            
            var sequence = csFactory.Create(3, NTSG.Ordinates.XYZM);
            for (var i = 0; i < 3; i++)
            {
                sequence.SetOrdinate(i, NTSG.Ordinate.X, points[i].X);
                sequence.SetOrdinate(i, NTSG.Ordinate.Y, points[i].Y);
                sequence.SetOrdinate(i, NTSG.Ordinate.Z, 1 + i);
                //if (testM)
                sequence.SetOrdinate(i, NTSG.Ordinate.M, 11 + i);
            }
            */

            EntityLookupCache = new Dictionary<string, Dictionary<string, string[]>>();
            if (EntityPropertyCaching && File.Exists(entityCachePath))
            {
                EntityLookupCache = JsonConvert.DeserializeObject<Dictionary<string, Dictionary<string, string[]>>>(File.ReadAllText(entityCachePath));
            }

            if (saveToDB)
            {
                throw new NotSupportedException("DB Points disabled for now.");

                SiteDB.ChangeTracker.AutoDetectChangesEnabled = false;

                Log.Information("Removing Old Points...");
                //SiteDB.Waypoints.RemoveRange(SiteDB.Waypoints.ToList());
                //SiteDB.Areas.RemoveRange(SiteDB.Areas.ToList());
                SiteDB.SaveChanges();
                Log.Information("Removed.");
            }

            siteDB = GetGameDatabase(cmdArg.GameDB_ID, cmdArg.Channel);

            //Load Game PKG file
            var launcherSettings = SCLauncherReader.LoadGameSettings();
            rawGameDB = DFDatabase.ParseFromSettings(launcherSettings, cmdArg.Channel, Cache: CacheLevel.Memory);


            sizeCache = new ModelCache(rawGameDB, "model_sizes.json"); //TODO: Better place for this =/

            if(!cmdArg.ProjectOnly)
                rawGameDB.ParseDFContent();
            subManager.Attach(rawGameDB);

            RenderSystem(rawGameDB, "Data/ObjectContainers/PU/system/stanton/stantonsystem.socpak", cmdArg.ProjectOnly);

            //RenderAllShapefiles(rawGameDB, "Data/objectcontainers/pu/system/nyx/delamar/delamar.socpak");

            //RenderAllShapefiles(rawGameDB, "Data/objectcontainers/pu/system/stanton/stanton1.socpak", new Guid("4c557c37-9273-c18e-8f84-cae53ad44d82"));      //Hurston - Lorville
            //RenderAllShapefiles(rawGameDB, "Data/objectcontainers/pu/system/stanton/stanton2.socpak");      //Crusader - Orison
            //RenderAllShapefiles(rawGameDB, "Data/objectcontainers/pu/system/stanton/stanton3.socpak");      //ArcCorp - Area18
            //RenderAllShapefiles(rawGameDB, "Data/objectcontainers/pu/system/stanton/stanton4.socpak", new Guid("4CE1F681-B479-5269-0536-3D7F02FDCCAF"));      //MicroTech - New Babbage
            //RenderAllShapefiles(rawGameDB, "Data/objectcontainers/pu/system/stanton/stanton1b.socpak", new Guid("45301614-0d69-2557-eaf0-bb453df743b6"));       //Aberdeen - Klescher

            //RenderAllShapefiles(rawGameDB, "Data/objectcontainers/pu/system/stanton/stanton4a.socpak");-
            //RenderAllShapefiles(rawGameDB, "Data/objectcontainers/pu/system/stanton/stanton4b.socpak");
            //RenderAllShapefiles(rawGameDB, "Data/objectcontainers/pu/system/stanton/stanton4c.socpak");

            sizeCache.SaveCache();
            if (EntityPropertyCaching)
            {
                File.WriteAllText(entityCachePath, JsonConvert.SerializeObject(EntityLookupCache, Newtonsoft.Json.Formatting.Indented));
            }
            SiteDB.SaveChanges();

            return true;
        }

        private void RenderSystem(DFDatabase RawGameDB, string RootArchive, bool ProjectOnly = false)
        {
            var systemArchive = SOC_Archive.LoadFromGameDBSocPak(RawGameDB, RootArchive);
            Log.Information("Loading all Systems Recursively....");
            //var structure = SOC_Explorer.ExploreNode(RawGameDB, systemArchive.RootEntry, Config: SearchConfiguration);

            void HandleNode(SOC_Entry SOC_Node)
            {
                if (SOC_Node.Class == "OrbitingObjectContainer")
                {
                    //if (SOC_Node.GUID.ToString().ToUpper() == "4CE1F681-B479-5269-0536-3D7F02FDCCAF")
                    //if (SOC_Node.Name.Contains("Hurston"))
                    {
                        RenderAllShapefiles(RawGameDB, SOC_Node.ArchivePath, SOC_Node.GUID, ProjectOnly);
                        sizeCache.SaveCache();
                    }
                }

                foreach (var child in SOC_Node.Children)
                {
                    HandleNode(child);
                }
            }

            HandleNode(systemArchive.RootEntry);
        }

        private void RenderAllShapefiles(DFDatabase RawGameDB, string RootArchive, Guid BodyGUID, bool ProjectOnly = false)
        {
            var loadedArchive = SOC_Archive.LoadFromGameDBSocPak(RawGameDB, RootArchive);

            if(loadedArchive == null)
            {
                Log.Warning($"No archive actually exists for {RootArchive}");
                return;
            }

            Log.Information($"Starting exploration of archive: {loadedArchive.Name}");

            if (ProjectOnly)
            {
                SearchConfiguration.Recursive = false;
                //SearchConfiguration.LoadEntities = false;
            }

            var structure = SOC_Explorer.ExploreNode(RawGameDB, loadedArchive.RootEntry, Config: SearchConfiguration);
            var dbRecord = siteDB.SystemBodies.FirstOrDefault(b => b.SCRecordID == BodyGUID);

            if(dbRecord == null)
            {
                Log.Warning($"No DB record found for planet: {RootArchive}");
                return;
            }

            Log.Information($"Got {structure.TotalCount} raw nodes. Starting to render {structure.Name} - {dbRecord.Name}.");
            HandlePlanetSCNode(RawGameDB, structure, dbRecord, ProjectOnly);

            if (areaBuffer.Count == 0)
            {
                Log.Information($"No Geometry for {RootArchive}. Skipping...");
                return;
            }
            else
            {
                Log.Information("Done.");
            }

            if(saveToDB)
            {
                for (int i = 0; i < areaBuffer.Count; i += 10000)
                {
                    /*
                    SiteDB.Areas.AddRange(areaBuffer.Skip(i).Take(10000).Select(a =>
                    {
                        return new OSM_Area()
                        {
                            Area = SQLGeoHelper.FromMapArea(a),
                            AreaType = (int)a.AreaType,
                            Tags = a.Tags,
                            Name = a.Name,
                            Elevation = (decimal)a.Elevation,
                            Height = (decimal)a.Height
                        };
                    }));
                    */
                    Log.Information($"Saving [{i + 10000}/{areaBuffer.Count}]");
                    SiteDB.SaveChanges();
                }
            }
            else
            {
                Log.Information($"Total Polygons: {areaBuffer.Count(a => a.AreaType == AreaType.Polygon)}");
                Log.Information($"Total Lines: {areaBuffer.Count(a => a.AreaType == AreaType.Line)}");
                Log.Information($"Total Points: {areaBuffer.Count(a => a.AreaType == AreaType.Point)}");

                Dictionary<string, FeatureCollection> LayerFeatures = new Dictionary<string, FeatureCollection>();
                var factory = NTSG.GeometryFactory.Default;

                //Can i just use regular direct 3d coords?
                NTSG.Coordinate ToNTSG (Coordinate Coord)
                {
                    return new NTSG.Coordinate()
                    {
                        X = Coord.Longitude,
                        Y = Coord.Latitude,
                        //Z = Coord.Altitude
                    };
                }

                int areaIndex = 0;

                foreach (var area in areaBuffer)
                {
                    foreach(var layer in mapSettings.MapLayers)
                    {
                        if (!LayerFeatures.ContainsKey(layer.Name))
                            LayerFeatures[layer.Name] = new FeatureCollection();

                        bool Keep = false;

                        //For making a catch-all layer
                        if (layer.Rules.Count == 0) Keep = true;

                        foreach(var searchRule in layer.Rules)
                        {

                            bool result = searchRule.LayerMode == LayerRule.Mode.Include ? false : true;

                            switch (searchRule.SearchSource)
                            {
                                case LayerRule.Source.Tags:
                                    result = searchRule.MatchTags.Any(tag => area.Tags.Contains(tag));
                                    break;
                                case LayerRule.Source.Name:
                                    result = searchRule.MatchTags.Any(tag => area.Name.Contains(tag));
                                    break;
                            }

                            if (searchRule.LayerMode == LayerRule.Mode.Include && result)
                                Keep = true;
                            else if (searchRule.LayerMode == LayerRule.Mode.Exclude && result)
                                Keep = false;
                        }

                        if (!Keep) continue;

                        NTSG.Geometry featureGeo = null;

                        switch(layer.DataType)
                        {
                            case AreaType.Polygon:
                                if (area.AreaType != AreaType.Polygon)
                                    continue; //Polygon layers only accept polygons

                                if (area.Coordinates.Length < 4)
                                    continue;

                                featureGeo = factory.CreatePolygon(factory.CreateLinearRing(area.Coordinates.Select(ToNTSG).ToArray()));
                                break;
                            case AreaType.Line:
                                if (area.Coordinates.Length < 2)
                                    continue;

                                featureGeo = factory.CreateLineString(area.Coordinates.Select(ToNTSG).ToArray());
                                break;
                            case AreaType.Point:
                                if (area.Coordinates.Length == 0)
                                    continue;
                                
                                featureGeo = factory.CreatePoint(ToNTSG(area.Center));
                                break;
                        }

                        if (featureGeo != null)
                        {
                            var attributes = new AttributesTable();
                            attributes.Add("ID", areaIndex);
                            attributes.Add("Name", area.Name);
                            attributes.Add("Elevation", area.Elevation);
                            attributes.Add("Height", area.Height);
                            attributes.Add("Tags", area.Tags);
                            attributes.Add("GUID", area.ID.ToString());
                            attributes.Add("Position", area.Center.Source.ToString());

                            var feature = new Feature(featureGeo, attributes);

                            LayerFeatures[layer.Name].Add(feature);
                        }

                        if (layer.Exclusive)
                        {
                            break;
                        }
                    }

                    areaIndex++;
                }

                //All layers complete here

                foreach (var layer in mapSettings.MapLayers)
                {
                    var layerFeatures = LayerFeatures[layer.Name];

                    if (layerFeatures.Count == 0)
                    {
                        Log.Warning($"Layer {layer.Name} contains no elements for {loadedArchive.Name}. Writing empty layer file.");
                        //continue;
                    }
                    else
                    {
                        Log.Information($"Layer {layer.Name} completed with {layerFeatures.Count} features.");
                    }

                    var outdir = Path.Combine(mapRenderPath, dbRecord.Key);

                    if (!Directory.Exists(outdir))
                        Directory.CreateDirectory(outdir);

                    bool saved = false;
                    var dummyItem = new Feature(new NTSG.Point(0, 0), new AttributesTable());

                    for (int attempt = 0; attempt < 5 && !saved; attempt++)
                    {
                        try
                        {
                            var shpWriter = new ShapefileDataWriter(Path.Combine(outdir, layer.Name), factory);
                            if (layerFeatures.Count > 0)
                                shpWriter.Header = ShapefileDataWriter.GetHeader(layerFeatures.FirstOrDefault(), layerFeatures.Count);
                            else
                                shpWriter.Header = ShapefileDataWriter.GetHeader(new DbaseFieldDescriptor[] { }, 0);
                            shpWriter.Write(layerFeatures); //One collection containing all features.
                            saved = true;
                        }
                        catch (Exception ex)
                        {

                        }
                    }
                }

                areaBuffer.Clear();
            }
        }

        private void GenerateQGISProjextFile(SOC_Entry RootEntry, string DisplayName, string OutPath)
        {
            string  keyname = RootEntry.Name,
                    surfacePath = $"GPKG:/renders/Planets/{keyname}.gpkg:{keyname}_surface",
                    heightmapPath = $"GPKG:/renders/Planets/{keyname}.gpkg:{keyname}_heightmap";

            var replaceMap = new Dictionary<string, string>()
            {
                { "/qgis/properties/WMSRootName", keyname },
                { "/qgis/projectMetadata/creation", DateTime.Now.ToString() },
                { "/qgis/layer-tree-group/layer-tree-group/@name", keyname },
                { "/qgis//maplayer[starts-with(./id, 'surface_')]/@minScale", "1e+12" }, //Fix for QGIS maxing out layer visibility rules at 1e+08
                { "/qgis//maplayer[starts-with(./id, 'surface_')]/datasource", surfacePath },
                { "/qgis//maplayer[starts-with(./id, 'heightmap_')]/datasource", heightmapPath },
                { "/qgis//layer-tree-layer[starts-with(@id, 'surface_')]/@source", surfacePath },
                { "/qgis//layer-tree-layer[starts-with(@id, 'heightmap_')]/@source", heightmapPath },
            };

            var projectInstance = File.ReadAllText("Data/template_map_project.qgs");

            XmlDocument projectDoc = new XmlDocument();
            projectDoc.LoadXml(projectInstance);

            foreach(var pair in replaceMap)
            {
                var elements = projectDoc.SelectNodes(pair.Key);

                if(elements.Count == 0)
                {
                    throw new Exception($"Found no matching elements for QGIS project replacement rule: {pair.Key}");
                }

                foreach(var element in elements)
                {
                    if (element is XmlAttribute attr)
                        attr.Value = pair.Value;
                    else if (element is XmlElement elem)
                        elem.InnerText = pair.Value;
                }
            }

            File.WriteAllText(OutPath, projectDoc.DocumentElement.OuterXml);
        }

        private void GenerateMapProxyConfig(SOC_Entry RootEntry, string DisplayName, string OutPath) //TODO: Cleaner way of handling these...
        {
            var name = Path.GetFileNameWithoutExtension(OutPath.Replace('\\', '/'));
            var baseURL = $"http://mapping.internal.tradein.space:8080/ows/?SERVICE=WMS&MAP=/web/{name}/all_layers.qgs&";

            var configuration = new MapProxy.Configuration()
            {
                globals = new
                {
                    cache = new
                    {
                        meta_size = new[] { 2, 2 },
                        concurrent_tile_creators = 2,
                        type = "mbtiles",
                        bulk_meta_tiles = true,
                        base_dir = "/mapproxy/cache",
                        lock_dir = "/mapproxy/cache/tile_locks",
                        tile_lock_dir = "/mapproxy/cache/tile_locks"
                    }
                },
                caches = new Dictionary<string, MapProxy.CacheConfig>()
                {
                    {
                        $"{name}_building_cache",
                        new MapProxy.CacheConfig()
                        {
                            grids = new[] { "webmercator" },
                            sources = new[] { "wms:surface", "wms:heightmap", "wms:Terrain", "wms:Buildings", "wms:Entities" }
                        }
                    },
                    {
                        $"{name}_transit_cache",
                        new MapProxy.CacheConfig()
                        {
                            grids = new[] { "webmercator" },
                            sources = new[] { "wms:Transit" }
                        }
                    },
                    {
                        $"{name}_annotation_cache",
                        new MapProxy.CacheConfig()
                        {
                            grids = new[] { "webmercator" },
                            sources = new[] { "wms:Annotations" }
                        }
                    }
                },
                layers = new MapProxy.LayerConfig[]
                {
                    new MapProxy.LayerConfig()
                    {
                        name = "Buildings",
                        sources = new[] { $"{name}_building_cache" },
                        title = "Buildings",
                        attribution = new
                        {
                            title = "TradeIn.Space",
                            url = "https://tradein.space/"
                        }
                    },
                    new MapProxy.LayerConfig()
                    {
                        name = "Transit",
                        sources = new[] { $"{name}_transit_cache" },
                        title = "Transit"
                    },
                    new MapProxy.LayerConfig()
                    {
                        name = "Annotations",
                        sources = new[] { $"{name}_annotation_cache" },
                        title = "Annotations"
                    }
                },
                services = new
                {
                    wms = new
                    {
                        md = new
                        {
                            title = string.IsNullOrEmpty(DisplayName) ? RootEntry.Name : DisplayName
                        }
                    }
                },
                grids = new
                {
                    webmercator = new
                    {
                        @base = "GLOBAL_GEODETIC",
                        bbox = new[] { 0, -90, 360, 90 }
                    }
                },
                sources = new
                {
                    wms = new
                    {
                        http = new
                        {
                            client_timeout = 300
                        },
                        coverage = new
                        {
                            bbox = new[] { 0, -90, 360, 90 },
                            srs = "EPSG:4326"
                        },
                        req = new
                        {
                            layers = new[] { "Buildings", "Entities", "Terrain", "Transit", "Annotations", "surface", "heightmap" },
                            transparent = true,
                            url = baseURL,
                            format = "image/png"
                        },
                        supported_srs = new[] { "EPSG:4326" },
                        type = "wms",
                        wms_opts = new
                        {
                            featureinfo = true,
                            version = "1.3.0",
                            legendurl = $"{baseURL}REQUEST=GetLegendGraphic&FORMAT=image/png&STYLE=default&LAYERS=Annotations"
                        }
                    }
                }
            };

            var configYML = new YamlDotNet.Serialization.Serializer().Serialize(configuration);
            File.WriteAllText(OutPath, configYML);
        }

        private void GenerateMapProxySeed(SOC_Entry RootEntry, string DisplayName, string OutPath) //TODO: Cleaner way of handling these...
        {
            var name = Path.GetFileNameWithoutExtension(OutPath.Replace('\\', '/'));
            var configuration = new
            {
                seeds = new Dictionary<string, MapProxy.SeedConfig>()
                {
                    {
                        $"{name}_seed_building",
                        new MapProxy.SeedConfig()
                        {
                            caches = new string[] { $"{name}_building_cache" },
                            levels = new Dictionary<string, int>()
                            {
                                { "to", 8 }
                            }
                        }
                    },
                    {
                        $"{name}_seed_transit",
                        new MapProxy.SeedConfig()
                        {
                            caches = new string[] { $"{name}_transit_cache" },
                            levels = new Dictionary<string, int>()
                            {
                                { "to", 8 }
                            }
                        }
                    }
                }
            };

            var configYML = new YamlDotNet.Serialization.Serializer().Serialize(configuration);
            File.WriteAllText(OutPath, configYML);
        }

        private void HandlePlanetSCNode(DFDatabase RawGameDB, SCHieracrhyNode PlanetNode, SystemBody Body, bool ProjectOnly = false)
        {
            void CheckEntityTagCache<T>(T element, Func<T, string> fnID, Func<T, XmlElement> fnElement)
            {
                if (element == null)
                    return;

                var id = fnID(element);
                var xElem = fnElement(element);

                if (id == default || xElem == default)
                    return;

                foreach (var query in mapSettings.QueryTags)
                {
                    if (!EntityLookupCache.ContainsKey(id))
                        EntityLookupCache[id] = new Dictionary<string, string[]>();

                    //Have we already run this query against this ID before?
                    if (EntityLookupCache[id].ContainsKey(query.Key))
                    {
                        //Need to write the latest versions from our test in case we have updated the search file
                        //we just need to check if there was a non-null result obtained originally to make the detrmination
                        if (EntityLookupCache[id][query.Key] != null)
                        {
                            EntityLookupCache[id][query.Key] = query.Value.ToArray();
                        }
                    }
                    else if (xElem?.SelectSingleNode(query.Key) != null)
                    {
                        EntityLookupCache[id][query.Key] = query.Value.ToArray();
                    }
                    else
                    {
                        EntityLookupCache[id][query.Key] = null;
                    }

                    tagStack.AddTags(EntityLookupCache[id][query.Key]);
                }
            }

            bool ForcedKeep(SCHieracrhyNode Node)
            {
                return mapSettings.KeepTags.Any(t => tagStack.Contains(t));
            }

            void ProcessChild(SCHieracrhyNode ChildNode)
            {
                tagStack.Push();

                try
                {
                    if (mapSettings.SkipNames.Any(a => ChildNode.Name.Contains(a, StringComparison.OrdinalIgnoreCase)))
                    {
                        return;
                    }

                    if(ChildNode.NodeType == SCHieracrhyNodeType.ObjectContainer && 
                        mapSettings.SkipContainers.Contains(ChildNode.ID.ToString().ToLowerInvariant()))
                    {
                        return;
                    }

                    if(ChildNode.SOCNode?.OwnEntity?.Class == "OrbitingObjectContainer" ||
                        ChildNode.SOCNode?.Class == "OrbitingObjectContainer")
                    {
                        //return;
                    }

                    if (ChildNode.SOCEntity != null)
                        if (mapSettings.SkipClasses.Any(c => ChildNode.SOCEntity?.Class == c))
                            return;

                    //Process the tags for the current item. This ensurs children get them properly
                    if (ChildNode.Tags != null)
                        foreach (var nodeTag in ChildNode.Tags)
                            if (mapSettings.DBTags.ContainsKey(nodeTag))
                                tagStack.AddTags(mapSettings.DBTags[nodeTag]);

                    CheckEntityTagCache(ChildNode?.SOCEntity, n => $"Entity-{n.GUID}", n => n.Element);
                    CheckEntityTagCache(ChildNode?.SOCEntity?.DatabaseEntity, n => $"DBEntity-{n.ID}", n => n.Element);
                    CheckEntityTagCache(ChildNode?.SOCNode, n => $"Node-{n.GUID}", n => n.Element);

                    //Iterate over children. This make it a depth-first search.
                    foreach (var nestedChild in ChildNode.Children)
                        ProcessChild(nestedChild);

                    //Any tags applied after this point will only be applied self and not to children.
                    switch (ChildNode.NodeType)
                    {
                        case SCHieracrhyNodeType.Dummy:
                            return; //Don't create any geometry for dummy nodes
                            break;
                        case SCHieracrhyNodeType.Root:
                            break;
                        case SCHieracrhyNodeType.ObjectContainer:
                            tagStack.AddTags("container");

                            if (!tagStack.Contains("destination"))
                                return; //Don't make points for the containers themselves

                            //CreatePoint(ChildNode.Name, Coordinate.ConvertFromXYZ(ChildNode.GlobalPosition));
                            break;
                        case SCHieracrhyNodeType.Entity:
                            tagStack.AddTags("entity");
                            //foreach(var tagSelector in select)

                            if(ChildNode.SOCEntity != null && ChildNode.SOCEntity?.Owner?.OwnEntity == ChildNode.SOCEntity)
                            {
                                //Ignore self-entity for object containers that are not direct destinations
                                if(!tagStack.Contains("destination"))
                                    return;
                            }

                            CreateNavSpline(ChildNode);
                            break;

                        case SCHieracrhyNodeType.Geometry:
                            tagStack.AddTags("geometry");
                            break;
                    }

                    if (!ProjectOnly)
                    {
                        if (ChildNode.ModelPath != null)
                        {
                            var modelProps = sizeCache[ChildNode.ModelPath];

                            //Assign tags based on strings in the model path.
                            foreach (var pathCheck in mapSettings.PathMatchTags)
                                if (ChildNode.ModelPath.Contains(pathCheck.Key, StringComparison.OrdinalIgnoreCase))
                                    tagStack.AddTags(pathCheck.Value);

                            if (!ForcedKeep(ChildNode))
                            {
                                if (IgnoreModelPathsAggressive.Any(ignore => ignore.IsMatch(ChildNode.ModelPath) || ignore.IsMatch(ChildNode.Name)))
                                    return;
                                if (mapSettings.SkipModels.Any(a => ChildNode.ModelPath.Contains(a, StringComparison.OrdinalIgnoreCase)))
                                    return;
                            }

                            //Object must be at least 0.25m in both dimensions
                            //if (modelProps.Size.X > 0.5 && modelProps.Size.Y > 0.5)
                            {
                                var transformedPoly = modelProps.GetPoints().Select(ChildNode.TranfomPoint);
                                CreatePolygon(ChildNode, ChildNode.GlobalPosition, transformedPoly,
                                    SelfClosing: true, Height: (float)(modelProps.Size?.Z ?? 0));
                            }
                        }
                        else if (tagStack.Contains("landingpad"))
                        {

                            var landingElem = ChildNode.SOCEntity.Element.SelectSingleNode(".//LandingArea") as XmlElement;

                            if (landingElem == null) return;

                            var landingArea = new LandingAreaComponent(ChildNode.SOCEntity, landingElem, ChildNode.SOCEntity.Owner);

                            //Hack to hide specifically AI dropoff landingpads used for high level UGF missions
                            if (ChildNode.SOCEntity?.Owner?.Name?.StartsWith("ugf_") ?? false && !landingArea.AutoRegister)
                            {
                                return;
                            }

                            var halfDim = landingArea.PadSize / 2;

                            var points = new List<Vector3>()
                            {
                                new Vector3(-halfDim.X, -halfDim.Y, -halfDim.Z),
                                new Vector3(halfDim.X, -halfDim.Y, -halfDim.Z),
                                new Vector3(halfDim.X, halfDim.Y, -halfDim.Z),
                                new Vector3(-halfDim.X, halfDim.Y, -halfDim.Z),
                            }.Select(ChildNode.TranfomPoint);

                            if (landingArea.AllowGround)
                                tagStack.AddTags($"ground_pad_{landingArea.GroundVehicleSize.ToString().ToLower()}");

                            if (landingArea.AllowShip)
                                tagStack.AddTags($"ship_pad_{landingArea.GroundVehicleSize.ToString().ToLower()}");

                            if (landingArea.ShipRepair)
                                tagStack.AddTags("ship_repair");

                            if (landingArea.ShipFitting)
                                tagStack.AddTags("ship_fitting");

                            CreatePolygon(ChildNode, ChildNode.GlobalPosition, points,
                                SelfClosing: true, Height: halfDim.Z * 2);
                        }
                        else if (tagStack.Contains("transit_manager"))
                        {
                            CreateTransitManager(ChildNode);
                        }
                        else if (tagStack.Contains("area_box"))
                        {
                            var areaElem = ChildNode.SOCEntity.Element.SelectSingleNode(".//AreaBoxComponent") as XmlElement;

                            if (areaElem == null)
                            {
                                areaElem = ChildNode.SOCEntity.Element.SelectSingleNode(".//AreaComponent[@Type='Box']") as XmlElement;

                                if (areaElem == null)
                                {
                                    Log.Error($"No area element defined for area_box: {ChildNode.Name} - {ChildNode.SOCEntity.Label}");
                                    return;
                                }
                            }

                            var minBounds = Vector3.FromString(areaElem.GetAttribute("BoxMin"));
                            var maxBounds = Vector3.FromString(areaElem.GetAttribute("BoxMax"));
                        }
                        else if (tagStack.Contains("area_sphere"))
                        {
                            var areaElem = ChildNode.SOCEntity.Element.SelectSingleNode(".//AreaComponent") as XmlElement;
                            var areaRadius = areaElem.ReadPropertyDouble("SphereRadius");

                            IEnumerable<Vector3> GetCirclePoints()
                            {
                                for (double i = 0; i < 360; i += 5)
                                {
                                    yield return new Vector3(Math.Cos(i * Deg2Rad) * areaRadius, Math.Sin(i * Deg2Rad) * areaRadius, 0);
                                }
                            }

                            CreatePolygon(ChildNode, ChildNode.GlobalPosition, GetCirclePoints().Select(ChildNode.TranfomPoint), SelfClosing: true);
                        }
                        else if (tagStack.Contains("area_shape"))
                        {
                            var areaElem = ChildNode.SOCEntity.Element.SelectSingleNode(".//AreaComponent") as XmlElement;
                            var areaPoints = ChildNode.SOCEntity.Element.SelectNodes(".//Points/Point/@Pos")
                                .OfType<XmlAttribute>().Select(a => Vector3.FromString(a.Value));

                            CreatePolygon(ChildNode, ChildNode.GlobalPosition, areaPoints.Select(ChildNode.TranfomPoint), SelfClosing: true);
                        }
                        else if (tagStack.Count > 1)
                        {
                            if (string.IsNullOrWhiteSpace(ChildNode.DisplayName))
                                return;

                            if (tagStack.Contains("destination"))
                                CreatePoint(ChildNode.Parent, Vector3.Zero);
                            else
                                CreatePoint(ChildNode, Vector3.Zero); //Point is relative to the global position of ChildNode
                        }
                    }
                }
                finally
                {
                    tagStack.Pop();
                }
            }

            //Actual function body

            if (PlanetNode.NodeType != SCHieracrhyNodeType.ObjectContainer) return;

            PlanetNode.SOCNode.LoadAllEntities();

            if (PlanetNode.SOCNode.PlanetJSON == null) return;

            JObject planetMetadata = JObject.Parse(PlanetNode.SOCNode.PlanetJSON);

            var generalProperties = planetMetadata["data"]?["General"];
            var sphereDetails = generalProperties?["tSphere"];

            if (sphereDetails != null)
            {
                var clean_name = Body.Key;

                var heightmapPath = sphereDetails.Value<string>("sHMWorld");
                var planetDir = Path.Combine(mapRenderPath, clean_name);

                //var test = PlanetNode.SOCNode.Archive.ReadPackedCGAFile(sphereDetails.Value<string>("sMaterialTerrainPlanet"));
                //test.ProcessCryengineFiles();

                Body.BodyRadius = sphereDetails.Value<float>("fPlanetTerrainRadius");

                if (!Directory.Exists(planetDir)) Directory.CreateDirectory(planetDir);
                if (!Directory.Exists(mapConfigPath)) Directory.CreateDirectory(mapConfigPath);
                if (!Directory.Exists(mapSeedPath)) Directory.CreateDirectory(mapSeedPath);

                /*
                //Not needed anymore, since the gpkg render contains heightmap data
                var hmPath = Path.Combine(planetDir, "heightmap.asc");
                SaveHeightmapToFileASC(heightmapPath, hmPath);
                */

                var qgisPath = Path.Combine(planetDir, "all_layers.qgs");
                GenerateQGISProjextFile(PlanetNode.SOCNode, PlanetNode.Name, qgisPath);

                var mpPath = Path.Combine(mapConfigPath, $"{clean_name}.yaml");
                GenerateMapProxyConfig(PlanetNode.SOCNode, PlanetNode.Name, mpPath);

                var mpSeedPath = Path.Combine(mapSeedPath, $"{clean_name}.yaml");
                GenerateMapProxySeed(PlanetNode.SOCNode, PlanetNode.Name, mpSeedPath);

                if(!ProjectOnly)
                    ProcessChild(PlanetNode);
            }
        }

        public void CreateNavSpline(SCHieracrhyNode entityNode)
        {
            if (entityNode.SOCEntity == null)
                return;

            List<Vector3> splineCoords = new List<Vector3>();
            var controlPoints = entityNode.SOCEntity.Element.SelectNodes("./NavSpline/ControlPoint").OfType<XmlElement>();

            //Ignore vertical shaft elevators
            if (entityNode.Name == "TransitNavSpline-003" && controlPoints.Count() == 2)
            {
                return;
            }

            //TODO: Create actual curve geometry (or just interpolate additional points inbetween)
            foreach (var controlElement in controlPoints)
            {
                var pointPosition = Vector3.FromString(controlElement.GetAttribute("position"));
                var pointRotation = Vector4.FromString(controlElement.GetAttribute("rotation"));

                splineCoords.Add(entityNode.TranfomPoint(pointPosition));
            }

            if (splineCoords.Count == 0)
                return;

            tagStack.AddTags("spline");

            CreateLine(entityNode, entityNode.GlobalPosition, splineCoords);
        }

        public void CreateTransitManager(SCHieracrhyNode entityNode)
        {
            if (entityNode.SOCEntity == null)
                return;

            foreach (var transitDestination in entityNode.SOCEntity.Element.SelectNodes(".//TransitDestinations").OfType<XmlElement>())
            {
                var destinationName = rawGameDB.Localize(transitDestination.GetAttribute("nameIdentifier"));

                foreach(var destinationGateway in transitDestination.SelectNodes("./Gateway").OfType<XmlElement>())
                {
                    tagStack.Push();

                    try
                    {
                        tagStack.AddTags("transit_gateway");

                        var gatewayPosition = Vector3.FromString(destinationGateway.GetAttribute("gatewayPos"));
                        var gatewayRotation = Vector4.FromString(destinationGateway.GetAttribute("gatewayQuat"));

                        CreatePoint(entityNode, gatewayPosition);
                    }
                    finally
                    {
                        tagStack.Pop();
                    }
                }
            }
        }

        public void SaveHeightmapToFileASC(string InPath, string OutFileName, int SuperSampleCount = 1)
        {
            var heightmapBytes = rawGameDB.ReadPackedFileBinary(InPath);
            var heightmapASCText = ASCExporter.GenerateASCFromHeightmap(heightmapBytes);
            File.WriteAllText(OutFileName, heightmapASCText);
        }


        public SubsumptionManager subManager { get; }

        public void CreateLine(SCHieracrhyNode Node, Coordinate CenterCoord, IEnumerable<Coordinate> Coordinates, bool SelfClosing = false)
        {
            var final = SelfClosing ? Coordinates.Append(Coordinates.First()) : Coordinates;
            AddGeom(Node, CenterCoord, final, AreaType.Line, Coordinates.Average(c => c.Altitude));
        }

        public void CreateLine(SCHieracrhyNode Node, Vector3 Center, IEnumerable<Vector3> Points, bool SelfClosing = false)
        {
            CreateLine(Node, Coordinate.ConvertFromXYZ(Center), Points.Select(p => Coordinate.ConvertFromXYZ(p)), SelfClosing);
        }

        public void CreatePolygon(SCHieracrhyNode Node, Coordinate CenterCoord, IEnumerable<Coordinate> Coordinates, bool SelfClosing = false, double Height = 0)
        {
            var final = SelfClosing ? Coordinates.Append(Coordinates.First()) : Coordinates;
            AddGeom(Node, CenterCoord, final, AreaType.Polygon, Coordinates.Average(c => c.Altitude), Height);
        }

        public void CreatePolygon(SCHieracrhyNode Node, Vector3 Center, IEnumerable<Vector3> Points, bool SelfClosing = false, double Height = 0)
        {
            var center = new Vector3(Points.Average(p => p.X), Points.Average(p => p.Y), Points.Average(p => p.Z));
            CreatePolygon(Node, Coordinate.ConvertFromXYZ(center), Points.Select(p => Coordinate.ConvertFromXYZ(p)), SelfClosing, Height);
        }

        public void CreateRectangle(SCHieracrhyNode Node, Vector3 Center, Vector3 minBounds, Vector3 maxBounds, bool SelfClosing = false, double Height = 0)
        {
            CreatePolygon(Node, Center, new []
            {
                minBounds,
                new Vector3(maxBounds.X, minBounds.Y, minBounds.Z),
                new Vector3(maxBounds.X, maxBounds.Y, minBounds.Z),
                new Vector3(minBounds.X, maxBounds.Y, minBounds.Z),
            }, SelfClosing, Height);
        }

        public void CreatePoint(SCHieracrhyNode Node, Coordinate Pos)
        {
            AddGeom(Node, Pos, new[] { Pos }, AreaType.Point, Elevation: Pos.Altitude, Height: 0);
        }

        public void CreatePoint(SCHieracrhyNode Node, Vector3 RelativePosition)
        {
            CreatePoint(Node, Coordinate.ConvertFromXYZ(Node.TranfomPoint(RelativePosition)));
        }

        public void AddGeom(SCHieracrhyNode Node, Coordinate CenterCoord, IEnumerable<Coordinate> Coordinates, AreaType areaType, double Elevation = 0.0, double Height = 0.0)
        {
            //If no tags, we don't care
            if (tagStack.Count == 0)
                return;

            var tagstring = string.Join(",", tagStack.GetTags());

            areaBuffer.Add(new MapArea()
            {
                ID = Node.ID,
                Name = Node.DisplayName ?? Node.Name,
                Center = CenterCoord,
                Coordinates = Coordinates.ToArray(),
                Tags = tagstring,
                AreaType = areaType,
                Elevation = Elevation,
                Height = Height
            });

            if(areaBuffer.Count % 10000 == 0)
            {
                Log.Information($"Processed {areaBuffer.Count} so far.");
            }
        }

        public SystemBody getRootBody(GameDatabase gameData, SystemBody planetBody)
        {
            return planetBody.ParentBody != null ? this.getRootBody(gameData, planetBody.ParentBody) : planetBody;
        }
    }

    public class MapTagStack
    {
        private List<HashSet<string>> _currentTags = new List<HashSet<string>>();

        private HashSet<string> _activeTags
        {
            get
            {
                return _currentTags[_currentTags.Count - 1];
            }
        }

        public MapTagStack()
        {
            //Start with one.
            _currentTags.Add(new HashSet<string>());
        }

        public List<string> GetTags()
        {
            return _activeTags.ToList();
        }

        public int Count { get => _activeTags.Count; }
        public bool Contains(string Tag) => _activeTags.Contains(Tag);

        public void AddTags(params string[] Tags)
        {
            if (Tags == null)
                return;
            foreach (var tag in Tags)
                _activeTags.Add(tag);
        }

        public void AddTags(IEnumerable<string> Tags)
        {
            foreach (var tag in Tags)
                _activeTags.Add(tag);
        }

        public void Push()
        {
            _currentTags.Add(new HashSet<string>(_activeTags));
        }

        public void Pop()
        {
            if (_currentTags.Count == 1)
                throw new Exception("Cannot remove last item from stack.");
            _currentTags.RemoveAt(_currentTags.Count - 1);
        }
    }

    public class MapImportSettings
    {
        public Dictionary<string, string[]> QueryTags { get; set; }
        public Dictionary<string, string[]> DBTags { get; set; }
        public Dictionary<string, string[]> PathMatchTags { get; set; }
        public List<string> KeepTags { get; set; }
        public List<string> SkipNames { get; set; }
        public List<string> SkipContainers { get; set; }
        public List<string> SkipModels { get; set; }
        public List<string> SkipClasses { get; set; }
        public List<LayerConfig> MapLayers { get; set; }
    }

    public class MapArea
    {
        //Store original xyz vec3s?

        public Guid ID { get; set; }
        public Coordinate Center { get; set; }
        public Coordinate[] Coordinates { get; set; }
        public string Name { get; set; }
        public string Tags { get; set; }
        public double Elevation { get; set; }
        public double Height { get; set; }
        public AreaType AreaType { get; set; }
    }

    public class LayerConfig
    {
        [JsonRequired]
        public string Name { get; set; }

        public bool Enabled { get; set; } = true;
        public bool Exclusive { get; set; } = true;


        public AreaType DataType { get; set; } = AreaType.Polygon;

        [JsonRequired]
        public List<LayerRule> Rules { get; set; }
    }

    public class LayerRule
    {
        [Serializable]
        [JsonConverter(typeof(StringEnumConverter))]
        public enum Mode
        {
            Include, Exclude
        }
        [Serializable]
        [JsonConverter(typeof(StringEnumConverter))]
        public enum Source
        {
            Tags,
            Name
        }

        public List<string> MatchTags { get; set; }
        
        public Mode LayerMode { get; set; }
        public Source SearchSource { get; set; } = Source.Tags;
    }

    public class MapProxy
    {
        public class LayerConfig
        {
            public string name { get; set; }
            public string title { get; set; }
            public string[] sources { get; set; }
            public object attribution { get; set; }
        }

        public class SeedConfig
        {
            public string[] caches { get; set; }
            public Dictionary<string, int> levels { get; set; }
            //TODO: grids+coverages
        }

        public class CacheConfig
        {
            public string[] grids { get; set; }
            public string[] sources { get; set; }
        }

        public class Configuration
        {
            public object globals;
            public Dictionary<string, CacheConfig> caches;
            public LayerConfig[] layers;
            public object services;
            public object grids;
            public object sources;
        }
    }
}
