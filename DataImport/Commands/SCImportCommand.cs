﻿using CommandLine;
using DataImport.Actions;
using DataImport.Importers;
using DataImport.Importers.SLPostProcess;
using Serilog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using TradeInSpace.Data;
using TradeInSpace.Explore;
using TradeInSpace.Models;

namespace DataImport.Commands
{
    [Verb("sc_import_main", isDefault: true, HelpText = "Import Star Citizen game data (uses installed launcher data to determine locations and installed versions.)")]
    public class SCImportCommandArgs
    {
        [Option(HelpText = "[LIVE or PTU] SC version to import - must be downloaded and installed.", Default = "LIVE")]
        public string Channel { get; set; }
        [Option(HelpText = "[ignore or reset] What to do when a matching version is found.")]
        public string Collision { get; set; }
    }

    public class SCImportCommand : DataCommand<SCImportCommandArgs>
    {

        public SCImportCommand(LauncherSettings launcherSettings, TISContext siteDB, ImportDatabaseAction importAction)
            :base(siteDB)
        {
            LauncherSettings = launcherSettings;
            ImportAction = importAction;
        }

        LauncherSettings LauncherSettings { get; }
        ImportDatabaseAction ImportAction { get; }

        public override bool ExecuteCommand(SCImportCommandArgs importCommand)
        {
            var channelInfo = LauncherSettings.GetGame("SC", importCommand.Channel);
            if (channelInfo?.ChannelDetails?.Status != "installed") throw new Exception($"Channel {importCommand.Channel} not installed!");

            var channelVersion = channelInfo.ChannelDetails.VersionLabel;

            var existingDB = SiteDB.GameDatabases.SingleOrDefault(gd => gd.DB_Version == channelVersion);

            if(existingDB != null)
            {
                if (string.IsNullOrEmpty(importCommand.Collision)) {
                    //throw new Exception($"Database already imported for version {channelVersion}. Specify collision mode [ignore or reset] to proceed.");
                }

                switch (importCommand.Collision)
                {
                    case "ignore":
                        //Don't need to do anything...
                        break;
                    case "reset":
                        WipeGameDatabase(existingDB);
                        break;
                }
            }

            var importedGameDB = ImportAction.CreateNewGameDatabase(channelInfo);

            Log.Information($"Loading Star-Citizen GameDB: {channelVersion}");

            //TODO: Need to handle versions more appropriately
            importedGameDB.Version = SiteDB.Versions.First();

            ImportAction.PerformAction(importedGameDB);

            return false;
        }

        private void WipeGameDatabase(GameDatabase gameDB)
        {
            gameDB?.Version?.GameDatabases?.Remove(gameDB);

            SiteDB.GameDatabases.Remove(gameDB);
            SiteDB.SaveChanges();
        }
    }
}
