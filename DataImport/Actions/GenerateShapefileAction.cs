﻿using DataImport.Utils;
using System;
using System.Collections.Generic;
using System.Text;
using TradeInSpace.Data;
using TradeInSpace.Models;

namespace DataImport.Actions
{
    class GenerateShapefileAction
    {
        private TISContext SiteDB { get; }
        private GameDatabase GameDB { get; set; }

        public GenerateShapefileAction(TISContext gameDB)
        {
            SiteDB = gameDB;
        }

        public void PerformAction(GameDatabase gameDB)
        {
            GameDB = gameDB;


        }
    }
}
