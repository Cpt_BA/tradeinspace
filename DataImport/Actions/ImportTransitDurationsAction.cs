﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using static TradeInSpace.Models.Enums;
using System.Text.RegularExpressions;
using TradeInSpace.Data;
using TradeInSpace.Models;
using Serilog;
using Microsoft.Extensions.Logging;

namespace DataImport.Actions
{
    class ImportTransitDurationsAction
    {
        public static void PerformAction(TISContext context, GameDatabase gameDB, ILoggerFactory log, List<Dictionary<string, string>> stationData)
        {
            Log.Information($"Importing transit durations into Game Database {gameDB.DB_Version}");

            foreach(var stationRow in stationData)
            {
                var station = gameDB.TradeShops.SingleOrDefault(s => s.Name == stationRow["Name"]);

                if(station == null)
                {
                    throw new Exception($"Unable to find station for name {stationRow["Name"]}");
                }

                station.ClosestApproachKM = float.Parse(stationRow["ClosestApproach"]);
                station.TransitDurationS = int.Parse(stationRow["TransitDuration"]);
                station.TravelDurationS = int.Parse(stationRow["TravelDuration"]);
                station.RunningDurationS = int.Parse(stationRow["RunningDuration"]);
                station.TransitNotes = stationRow["TransitNotes"];
                station.TravelNotes = stationRow["TravelNotes"];
            }
            
            context.SaveChanges();
        }
    }
}
