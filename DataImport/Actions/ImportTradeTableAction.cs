﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using TradeInSpace.Data;
using TradeInSpace.Models;
using static TradeInSpace.Models.Enums;

namespace DataImport.Actions
{
    class ImportTradeTableAction
    {
        public static void PerformAction(TISContext context, GameDatabase gameDB, ILoggerFactory log, List<Dictionary<string, string>> tradeSheet)
        {
            Log.Information($"Importing trade table onto Game Database {gameDB.DB_Version}");

            var tableVersion = new TradeTable()
            {
                GameDatabase = gameDB,
                Name = "Imported",
                Active = true,
                Entries = new List<TradeTableEntry>(),
                TableCreated = DateTime.UtcNow,
                TableUpdated = DateTime.UtcNow
            };

            List<TradeTableEntry> _TradeEntries = new List<TradeTableEntry>();
            foreach (var tradeRow in tradeSheet)
            {
                foreach(var tradeSheetEntry in tradeRow)
                {
                    if (tradeSheetEntry.Key == "Name") continue;

                    var item = gameDB.TradeItems.SingleOrDefault(s => s.Name == tradeSheetEntry.Key);
                    if (item == null) throw new Exception($"Unable to find item from trade table: {tradeSheetEntry.Key}");

                    var station = gameDB.TradeShops.SingleOrDefault(s => s.Name == tradeRow["Name"]);
                    if (station == null) throw new Exception($"Unable to find station from trade table: {tradeRow["Name"]}");


                    if (tradeSheetEntry.Value == "-") continue;

                    var entry = Regex.Match(tradeSheetEntry.Value, @"(Buy|Sell) @ ([\d\.]+) - ([\d\.]+) UEC\s+([\d\.]+) SCU/min of (\d+)");

                    var tradeEntry = new TradeTableEntry()
                    {
                        Table = tableVersion,
                        Station = station,
                        Item = item,
                        BadTrade = false,
                        Type = Enum.Parse<TradeType>(entry.Groups[1].Value),
                        MinUnitPrice = float.Parse(entry.Groups[2].Value),
                        MaxUnitPrice = float.Parse(entry.Groups[3].Value),
                        UnitCapacityRate = (int)(float.Parse(entry.Groups[4].Value) * 100),
                        UnitCapacityMax = int.Parse(entry.Groups[5].Value),
                    };

                    _TradeEntries.Add(tradeEntry);
                }

            }

            //Pass to determine if any trades are "bad" (negative/no profit)
            foreach(var tradeItemEntries in _TradeEntries.GroupBy(te => te.Item))
            {
                var buys = tradeItemEntries.Where(i => i.Type == TradeType.Buy);
                var sells = tradeItemEntries.Where(i => i.Type == TradeType.Sell);

                if (buys.Count() == 0 || sells.Count() == 0) continue;

                var min_buy = buys.Min(i => i.MinUnitPrice);
                var max_sell = sells.Max(i => i.MaxUnitPrice);

                foreach (var entry in sells)
                {
                    if(entry.MaxUnitPrice <= min_buy)
                    {
                        entry.BadTrade = true;
                    }
                }

                foreach (var entry in buys)
                {
                    if (entry.MinUnitPrice >= max_sell)
                    {
                        entry.BadTrade = true;
                    }
                }
            }

            context.TradeEntries.AddRange(_TradeEntries);

            context.TradeTables.Add(tableVersion);
            context.SaveChanges();
        }
    }
}
