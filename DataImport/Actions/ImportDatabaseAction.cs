﻿using Azure.Storage.Blobs;
using DataImport.Importers;
using DataImport.Importers.SLPostProcess;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;
using Serilog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using TradeInSpace.Data;
using TradeInSpace.Explore;
using TradeInSpace.Explore.DataForge;
using TradeInSpace.Explore.Subsumption;
using TradeInSpace.Models;

namespace DataImport.Actions
{
    public class ImportDatabaseAction
    {
        public ImportDatabaseAction(TISContext context,

            SystemBodyImporter import_SystemBody,
            SystemLocationImporter import_SystemLocation,
            TradeItemImporter import_TradeItem,
            GameEquipmentImporter import_GameEquipment,
            GameShipImporter import_GameShip,
            MiningElementImporter import_MiningElement,
            MiningCompositionImporter import_MiningComposition,
            HarvestableImporter import_Harvestable,
            SystemLocationPostProcessImporter post_processor,
            SubsumptionPostProcessImporter post_processor_sub,
            TradeImporter import_Trades,
            LocationTradeServicesImporter import_LocationServices,
            MissionImporter import_Missions,
            ManufacturerImporter import_Manufacturer,

            SubsumptionManager subsumptionManager,

            CriminalTerminalPostProcessor pp_Criminal,
            SystemLandingZonePostProcessor pp_Landing,
            TradeShopPostProcessor pp_TradeShop,
            RespawnPointPostProcessor pp_Respawn,
            GreenZonePostProcessor pp_GreenZone,
            HarvestableProviderPostProcessor pp_Harvestable)
        {
            Context = context;
            Import_SystemBody = import_SystemBody;
            Import_SystemLocation = import_SystemLocation;
            Import_TradeItem = import_TradeItem;
            Import_GameEquipment = import_GameEquipment;
            Import_GameShip = import_GameShip;
            Import_MiningElement = import_MiningElement;
            Import_MiningComposition = import_MiningComposition;
            Import_Harvestable = import_Harvestable;
            Import_PostProcess = post_processor;
            Import_PostProcessSub = post_processor_sub;
            Import_Trades = import_Trades;
            Import_LocationServices = import_LocationServices;
            Import_Missions = import_Missions;
            Import_Manufacturer = import_Manufacturer;
            SubsumptionManager = subsumptionManager;
            PP_Criminal = pp_Criminal;
            PP_Landing = pp_Landing;
            PP_TradeShop = pp_TradeShop;
            PP_Respawn = pp_Respawn;
            PP_GreenZone = pp_GreenZone;
            PP_Harvestable = pp_Harvestable;
        }

        TISContext Context { get; }
        
        SystemBodyImporter Import_SystemBody { get; }
        SystemLocationImporter Import_SystemLocation { get; }
        TradeItemImporter Import_TradeItem { get; }
        GameEquipmentImporter Import_GameEquipment { get; }
        GameShipImporter Import_GameShip { get; }
        MiningElementImporter Import_MiningElement { get; }
        MiningCompositionImporter Import_MiningComposition { get; }
        HarvestableImporter Import_Harvestable { get; }
        SystemLocationPostProcessImporter Import_PostProcess { get; }
        SubsumptionPostProcessImporter Import_PostProcessSub { get; set; }
        TradeImporter Import_Trades { get; }
        LocationTradeServicesImporter Import_LocationServices { get; }
        MissionImporter Import_Missions { get; }
        ManufacturerImporter Import_Manufacturer { get; }

        SubsumptionManager SubsumptionManager { get; }
        CriminalTerminalPostProcessor PP_Criminal { get; }
        SystemLandingZonePostProcessor PP_Landing { get; }
        TradeShopPostProcessor PP_TradeShop { get; }
        RespawnPointPostProcessor PP_Respawn { get; }
        GreenZonePostProcessor PP_GreenZone { get; }
        HarvestableProviderPostProcessor PP_Harvestable { get; }

        public void PerformAction(GameDatabase gameDB)
        {
            Context.GameDatabases.Add(gameDB);
            
            Log.Information("Starting parse");
            
            ImportFromGame(gameDB, stepCompleted: () => { Context.SaveChanges(); });

            Log.Information("Parsing complete");

            foreach(var existingChannel in Context.GameDatabases.Where(db => db.DB_Channel == gameDB.DB_Channel))
            {
                existingChannel.Enabled = false;
            }

            gameDB.Enabled = true;
            gameDB.FinishedImporting = true;
            gameDB.ImportTime = DateTime.Now;

            if (gameDB.DB_Channel == "LIVE")
            {
                gameDB.Version.Enabled = true;
                gameDB.Version.Default = true;
                gameDB.Version.GameDatabaseID = gameDB.ID;
            }

            Context.SaveChanges();
        }

        static readonly string MetadataFileName = "f_win_game_client_release.id";

        private void ImportFromGame(GameDatabase websiteDB, Action stepCompleted = null)
        {
            var launcherSettings = SCLauncherReader.LoadGameSettings();
            var dataforgeDatabase = DFDatabase.ParseFromSettings(launcherSettings, websiteDB.DB_Channel);

            dataforgeDatabase.ParseDFContent(SubsumptionManager);
            dataforgeDatabase.Subsumption.LoadAllSystems();
            dataforgeDatabase.Subsumption.LoadAllChildren();

            var importerSteps = new DataImporter[]
            {
                /* Simple data imports */
                Import_Manufacturer,
                Import_TradeItem,
                Import_MiningElement,
                Import_MiningComposition,
                Import_Harvestable,
                Import_GameEquipment,
                Import_GameShip,

                /* System/Subsumption explorer */
                Import_SystemBody,
                Import_SystemLocation,
                //Import_MissionLocations,
                Import_Missions,
                
                Import_PostProcess,
                Import_PostProcessSub,
                
                /* Final Imports requiring multiple previous */
                Import_Trades,
                Import_LocationServices
            };

            Import_PostProcess.AddProcessor(PP_Criminal);
            Import_PostProcess.AddProcessor(PP_Landing);
            Import_PostProcess.AddProcessor(PP_TradeShop);
            Import_PostProcess.AddProcessor(PP_Respawn);
            Import_PostProcess.AddProcessor(PP_GreenZone);
            Import_PostProcess.AddProcessor(PP_Harvestable);

            foreach (var importer in importerSteps)
            {
                var importName = importer.GetType().Name;
                var importIndex = Array.IndexOf(importerSteps, importer);

                Console.Title = $"[{importIndex + 1}/{importerSteps.Length}] Importer: {importName}";

                importer.CreateRecords(Context, dataforgeDatabase, websiteDB);
                importer.ImporterCompleted(Context, dataforgeDatabase, websiteDB);
                Log.Information($"==== Finished {importName} ====");
                stepCompleted?.Invoke();

                //TODO: HACK! Put somewhere better/cleaner
                if (TradeInSpace.Explore.Socpak.Components.SOC_ComponentShop.UnrecognizedShopTypes.Count != 0)
                {
                    throw new Exception($"Unrecognized shop types [{string.Join(", ", TradeInSpace.Explore.Socpak.Components.SOC_ComponentShop.UnrecognizedShopTypes)}]");
                }
            }
        }

        public GameDatabase CreateNewGameDatabase(LauncherInstalledVersion installedVersion)
        {
            var launcherSettings = installedVersion.AllSettings;
            var channelDetails = installedVersion.ChannelDetails;

            if (channelDetails.Status != "installed")
            {
                throw new Exception($"Channel {channelDetails.ID} not currently installed!");
            }

            string SCFolder = Path.Combine(launcherSettings.RootInstallPath, installedVersion.VersionSettings.InstallDir, installedVersion.ChannelID);

            int BuildNumber = 0;
            DateTime? BuildTime = null;

            if (File.Exists(Path.Join(SCFolder, MetadataFileName)))
            {
                try
                {
                    var metadata = JObject.Parse(File.ReadAllText(Path.Join(SCFolder, MetadataFileName)));
                    BuildNumber = int.Parse(metadata["Data"].Value<string>("RequestedP4ChangeNum"));

                    var timestamp = metadata["Data"].Value<string>("BuildDateStamp") + " " + metadata["Data"].Value<string>("BuildTimeStamp");

                    if (int.Parse(channelDetails.VersionLabel.Split('.').Last()) != BuildNumber)
                    {
                        //File doesn't match what we laoded from launcher, ignore the build date and use now.
                        Log.Error("Game Client Release file version does not match expected");
                        BuildTime = DateTime.UtcNow;
                    }
                    else
                    {
                        foreach (var zone in _timeZones)
                        {
                            timestamp = timestamp.Replace(zone.Key, zone.Value);
                        }

                        BuildTime = DateTime.Parse(timestamp);
                    }
                }
                catch { }
            }

            string displayVersion = channelDetails.VersionLabel.ToUpper();

            return new GameDatabase()
            {
                Name = displayVersion,
                Description = $"Imported {DateTime.UtcNow.ToString("g")}",
                DB_Build = BuildNumber,
                DB_BuildTime = BuildTime,
                DB_Version = displayVersion,
                DB_Channel = channelDetails.ID,
                Enabled = false,
                FinishedImporting = false,

                Ships = new List<GameShip>(),
                Equipment = new List<GameEquipment>(),

                SystemBodies = new List<SystemBody>(),
                SystemLocations = new List<SystemLocation>(),
                LandingZones = new List<SystemLandingZone>(),

                TradeTables = new List<TradeTable>(),
                TradeItems = new List<TradeItem>(),
                TradeShops = new List<TradeShop>(),
                Manufacturers = new List<Manufacturer>(),

                MiningElements = new List<MiningElement>(),
                HarvestableLocations = new List<HarvestableLocation>(),
                Harvestables = new List<Harvestable>(),
                SubHarvestables = new List<SubHarvestable>(),
                MiningCompositions = new List<MiningComposition>(),
                MiningCompositionEntries = new List<MiningCompositionEntry>(),

                MissionGivers = new List<MissionGiver>(),
                MissionGiverScopes = new List<MissionGiverScope>(),
                Missions = new List<MissionGiverEntry>()
            };
        }

        //Is this overkill? of course, but they do have offices all over the world :)
        static Dictionary<string, string> _timeZones = new Dictionary<string, string>() {
            {"ACDT", "+1030"},  {"ACST", "+0930"},  {"ADT", "-0300"},   {"AEDT", "+1100"},
            {"AEST", "+1000"},  {"AHDT", "-0900"},  {"AHST", "-1000"},  {"AST", "-0400"},
            {"AT", "-0200"},    {"AWDT", "+0900"},  {"AWST", "+0800"},  {"BAT", "+0300"},
            {"BDST", "+0200"},  {"BET", "-1100"},   {"BST", "-0300"},   {"BT", "+0300"},
            {"BZT2", "-0300"},  {"CADT", "+1030"},  {"CAST", "+0930"},  {"CAT", "-1000"},
            {"CCT", "+0800"},   {"CDT", "-0500"},   {"CED", "+0200"},   {"CET", "+0100"},
            {"CEST", "+0200"},  {"CST", "-0600"},   {"EAST", "+1000"},  {"EDT", "-0400"},
            {"EED", "+0300"},   {"EET", "+0200"},   {"EEST", "+0300"},  {"EST", "-0500"},
            {"FST", "+0200"},   {"FWT", "+0100"},   {"GMT", "GMT"},     {"GST", "+1000"},
            {"HDT", "-0900"},   {"HST", "-1000"},   {"IDLE", "+1200"},  {"IDLW", "-1200"},
            {"IST", "+0530"},   {"IT", "+0330"},    {"JST", "+0900"},   {"JT", "+0700"},
            {"MDT", "-0600"},   {"MED", "+0200"},   {"MET", "+0100"},   {"MEST", "+0200"},
            {"MEWT", "+0100"},  {"MST", "-0700"},   {"MT", "+0800"},    {"NDT", "-0230"},
            {"NFT", "-0330"},   {"NT", "-1100"},    {"NST", "+0630"},   {"NZ", "+1100"},
            {"NZST", "+1200"},  {"NZDT", "+1300"},  {"NZT", "+1200"},   {"PDT", "-0700"},
            {"PST", "-0800"},   {"ROK", "+0900"},   {"SAD", "+1000"},   {"SAST", "+0900"},
            {"SAT", "+0900"},   {"SDT", "+1000"},   {"SST", "+0200"},   {"SWT", "+0100"},
            {"USZ3", "+0400"},  {"USZ4", "+0500"},  {"USZ5", "+0600"},  {"USZ6", "+0700"},
            {"UT", "-0000"},    {"UTC", "-0000"},   {"UZ10", "+1100"},  {"WAT", "-0100"},
            {"WET", "-0000"},   {"WST", "+0800"},   {"YDT", "-0800"},   {"YST", "-0900"},
            {"ZP4", "+0400"},   {"ZP5", "+0500"},   {"ZP6", "+0600"}
        };
    }
}
