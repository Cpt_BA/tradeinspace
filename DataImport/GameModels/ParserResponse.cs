﻿using TradeInSpace.Explore.Socpak;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TradeInSpace.Models;

namespace DataImport.GameModels
{
    public class ParserResponse<K, DB> where DB : GameRecord
    {
        public ParserResponse(K entry, DB response)
        {
            Entry = entry;
            Record = response;
        }

        public K Entry { get; set; }
        public DB Record { get; set; }
    }

    public class ParserResponseCollection<K, DB> : List<ParserResponse<K,DB>> where DB : GameRecord
    {
        public void Add(K SocEntity, DB Response)
        {
            this.Add(new ParserResponse<K,DB>(SocEntity, Response));
        }

        public bool HasGUID(Guid searchGUID)
        {
            return this.Any(r => r.Record.SCRecordID == searchGUID);
        }

        public bool HasKey(K key)
        {
            return this.Any(r => r.Entry?.Equals(key) ?? false);
        }

        public bool HasValue(DB value)
        {
            return this.Any(r => r.Record == value);
        }

        public ParserResponse<K,DB> FindByGUID(Guid searchGUID)
        {
            return this.Single(r => r.Record.SCRecordID == searchGUID);
        }

        public DB GetValue(K Key)
        {
            return this.SingleOrDefault(r => r.Entry?.Equals(Key) ?? false)?.Record;
        }
    }
}
