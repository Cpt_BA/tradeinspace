﻿using TradeInSpace.Explore.Socpak;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using TradeInSpace.Data;
using TradeInSpace.Models;
using TradeInSpace.Explore;
using TradeInSpace.Explore.DataForge;

namespace DataImport.Importers
{
    public abstract class DataImporter
    {
        public ILogger Logger { get; }

        public DataImporter(ILogger logger = null)
        {
            Logger = logger;
        }

        public abstract void CreateRecords(TISContext context, DFDatabase rawGameData, GameDatabase workingDataset);
        public virtual void ImporterCompleted(TISContext context, DFDatabase rawGameData, GameDatabase workingDataset) { }
    }
}
