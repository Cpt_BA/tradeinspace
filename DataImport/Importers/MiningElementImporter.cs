﻿using DataImport.GameModels;
using Microsoft.Extensions.Logging;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TradeInSpace.Data;
using TradeInSpace.Explore;
using TradeInSpace.Explore.DataForge;
using TradeInSpace.Explore.SC.Data;
using TradeInSpace.Models;

namespace DataImport.Importers
{
    public class MiningElementImporter : DataImporter
    {
        public MiningElementImporter(ILoggerFactory factory = null)
            : base(factory?.CreateLogger("Mining Element Importer"))
        {
        }

        public ParserResponseCollection<MineableElement, MiningElement> ImportedElements = new ParserResponseCollection<MineableElement, MiningElement>();

        public override void CreateRecords(TISContext context, DFDatabase rawGameData, GameDatabase workingDataset)
        {
            foreach (var elementRecord in rawGameData.GetEntries<MineableElement>())
            {
                var commodityTradeItem = workingDataset.TradeItems.SingleOrDefault(ti => ti.SCRecordID == elementRecord.ResourceTypeID);

                if(commodityTradeItem == null)
                {
                    Log.Error($"No matching commodity found for {elementRecord.ResourceTypeID}/{elementRecord.InstanceName}. Skipping.");
                    continue;
                }

                var refinedTradeItem = elementRecord.Resource.RefinesTo != null ? 
                    workingDataset.TradeItems.Single(ti => ti.SCRecordID == elementRecord.Resource.RefinesTo.ID) : null;

                var element = new MiningElement()
                {
                    SCRecordID = elementRecord.ID,
                    Key = Utilities.CleanKey(elementRecord.InstanceName),
                    ClusterFactor = elementRecord.ClusterFactor,
                    ExplosionMultiplier = elementRecord.ExplosionMultiplier,
                    Instability = elementRecord.Instability,
                    Resistance = elementRecord.Resistance,
                    WindowMidpoint = elementRecord.WindowMidpoint,
                    WindowMidpointRandomness = elementRecord.WindowMidpointRandomness,
                    WindowMidpointThinness = elementRecord.WindowMidpointThinness,
                    TradeItem = commodityTradeItem,
                    RefinedTradeItem = refinedTradeItem
                };

                workingDataset.MiningElements.Add(element);
                ImportedElements.Add(elementRecord, element);

                Logger?.LogInformation($"Imported mining element {elementRecord.InstanceName}");
            }
        }
    }
}
