﻿using DataImport.GameModels;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using TradeInSpace.Data;
using TradeInSpace.Explore;
using TradeInSpace.Explore.DataForge;
using TradeInSpace.Explore.SC.Data;
using TradeInSpace.Explore.Socpak;
using TradeInSpace.Explore.Socpak.Components;
using TradeInSpace.Models;

namespace DataImport.Importers
{
    public class MissionLocationImporter : DataImporter
    {
        public ParserResponseCollection<SOC_Entry, SystemLocation> ImportedLocations = new ParserResponseCollection<SOC_Entry, SystemLocation>();
        public ParserResponseCollection<SOC_Entry, SystemBody> ImportedBodies = new ParserResponseCollection<SOC_Entry, SystemBody>();

        public MissionLocationImporter(ILoggerFactory factory = null)
            : base(factory?.CreateLogger("Mission Location Importer"))
        {
        }

        public override void CreateRecords(TISContext context, DFDatabase rawGameData, GameDatabase workingDataset)
        {
            string[] IgnoreTypes = new string[] { };

            Logger?.LogInformation("Loading Stanton System");
            var stantonRoot = SOC_Archive.LoadFromGameDBSocPak(rawGameData, "Data/ObjectContainers/PU/system/stanton/stantonsystem.socpak");

            var missionLocations = rawGameData.GetEntries<MissionLocation>().ToList();
            Logger?.LogInformation($"Loaded {missionLocations.Count} Locations");

            var TAG_Name = rawGameData.Tags.GetTagID("Name", "Text").ToString();
            var TAG_Description = rawGameData.Tags.GetTagID("Description", "Text").ToString();

            
            for(int currentIndex = 0; currentIndex < missionLocations.Count;currentIndex++)
            {
                var location = missionLocations[currentIndex];

                //Attempt to search with the ActionArea GUID first
                var aaEntity = stantonRoot.RootEntry.SearchActionAreaGUID(location.LocationParams.ActionAreaSuperGUID);

                SOC_Entry missionLocationSOC = null;

                if (aaEntity == null)
                {
                    if (string.IsNullOrEmpty(location.LocationParams.ObjectContainerSuperGUID))
                    {
                        Logger.LogError($"Unable to find SOC Child for SuperGUID {location.LocationParams.ActionAreaSuperGUID}");
                        continue;
                    }
                    else
                    {
                        //missionLocationSOC = stantonRoot.RootEntry.SearchSuperGUID(location.LocationParams.ObjectContainerSuperGUID);
                    }
                }
                else
                {
                    //TODO: Incorporate regions as defined by the ActionArea
                    //to limit the sub-entites we import. But since we handle
                    //everything from an object container pov, this is difficule
                    //because each action area is an endpoint.

                    missionLocationSOC = aaEntity.Owner;
                }

                if (missionLocationSOC == null)
                {
                    Logger.LogError($"Unable to find SOC Container for Mission Location {location.InstanceName}");
                    continue;
                }

                var starmapDetails = rawGameData.GetEntry<StarmapObject>(missionLocationSOC.StarmapRecord);

                Logger?.LogInformation($"[1/2] [{currentIndex + 1}/{missionLocations.Count}] Found Location {location.InstanceName} - {starmapDetails?.Name}");

                string locationName = "", locationDescription = "";

                Enums.LocationType locationType = Enums.LocationType.Other;
                if (starmapDetails != null)
                {
                    switch (starmapDetails.ObjectType.Name)
                    {
                        case "Planet":
                        case "Moon":
                        case "Star":
                        case "CardinalPoint":
                        case "YouAreHere":
                        case "QuantumTracePoint":
                        case "Asteroid":
                        case "Manmade":
                        case "Outpost":
                        case "LandingZone":
                            locationType = Enum.Parse<Enums.LocationType>(starmapDetails.ObjectType.Name, true);
                            break;
                        case "Lagrange Point":
                            locationType = Enums.LocationType.LagrangePoint;
                            break;
                        default:
                            break;
                    }

                    locationName = starmapDetails.Name;
                    locationDescription = starmapDetails.Description;
                }
                else
                {
                    //Need something to determine what the location is
                    //Could be a cave, or an unmarked outpost, etc.
                    //Leave as other for now
                }

                //Handle substitutions defined in the mission location data itself (could be coming from a .soc entity)
                if (location.LocationParams.StringVariants.ContainsKey(TAG_Name))
                    locationName = location.LocationParams.StringVariants[TAG_Name];
                if (location.LocationParams.StringVariants.ContainsKey(TAG_Description))
                    locationDescription = location.LocationParams.StringVariants[TAG_Description];

                if (string.IsNullOrEmpty(locationName))
                {
                    Logger.LogWarning($"Falling back to mission location instace name {location.InstanceName}");
                    locationName = location.InstanceName;
                }

                switch (locationType)
                {
                    case Enums.LocationType.Planet:
                    case Enums.LocationType.Moon:
                    case Enums.LocationType.Star:
                    case Enums.LocationType.AsteroidField:
                    case Enums.LocationType.Manmade:
                        CreateBodyRecord(missionLocationSOC, locationType, locationName, locationDescription);
                        break;
                }


                var sysLocation = new SystemLocation()
                {
                    SCRecordID = location.ID,
                    //These are set after the fact, b/c records come in any order
                    //ParentLocation = parentLocation,
                    //ParentBody = body,
                    StarmapID = missionLocationSOC.StarmapRecord,
                    Key = Utilities.CleanKey(locationName),
                    Name = locationName,
                    Description = locationDescription,
                    LocationType = locationType,
                    //default, changed later if not hidden
                    IsHidden = true,
                    QTDistance = null
                };

                if(starmapDetails != null)
                {
                    sysLocation.IsHidden = !starmapDetails.HasQuantum;
                    sysLocation.QTDistance = starmapDetails.HasQuantum ? (int?)starmapDetails.ArrivalRadius : null;
                }

                ImportedLocations.Add(missionLocationSOC, sysLocation);
            }
        }

        private void CreateBodyRecord(SOC_Entry LocationSOC, Enums.LocationType locationType, string Name, string Description)
        {
            LocationSOC.LoadFullDetails();
            LocationSOC.LoadAllEntities(FilterByComponents: false, DynamicComponents: true, ModelReferences: false);

            var orbitParams = LocationSOC.OwnEntity?.Components?.SingleOrDefault(c => c.ComponentType == "EntityComponentOrbit");
            if (orbitParams == null) return;

            float? bodyRadius = null;

            if(LocationSOC.PlanetJSON != null)
            {
                var extraDetails = JsonConvert.DeserializeObject<JObject>(LocationSOC.PlanetJSON);
                bodyRadius = extraDetails.SelectToken("data.General.tSphere.fPlanetTerrainRadius").ToObject<float>();
            }

            var orbitRadius = float.Parse(orbitParams.FromElement.SelectSingleNode("@OrbitalRadius").InnerText);
            var orbitStart = float.Parse(orbitParams.FromElement.SelectSingleNode("@OrbitalAngle").InnerText);

            var planetParams = LocationSOC.PlanetEntity.GetComponent<SOC_ProceduralEntity>();

            float? Atmosphere = planetParams?.AtmosphereHeight;
            float? Gravity = planetParams?.PlanetGravity;

            var body = new SystemBody()
            {
                SCRecordID = LocationSOC.GUID,
                Key = Utilities.CleanKey(LocationSOC.Name),
                Name = Name,
                Description = Description,
                BodyType = Enum.Parse<Enums.BodyType>(locationType.ToString()),
                OrbitRadius = orbitRadius,
                OrbitStart = orbitStart,
                AtmosphereHeightKM = Atmosphere,
                Gravity = Gravity,
                BodyRadius = bodyRadius
            };

            ImportedBodies.Add(LocationSOC, body);
        }

        public override void ImporterCompleted(TISContext context, DFDatabase rawGameData, GameDatabase workingDataset)
        {
            Dictionary<Guid, SystemLocation> importedIDs = ImportedLocations
                .GroupBy(il => il.Entry.GUID)
                .ToDictionary(g => g.Key, g => g.Select(kvp => kvp.Record).First());

            //One pass for location parents
            foreach(var kvpLocation in ImportedLocations)
            {
                //Find the first parent that is a record we've imported
                var nearestImportedParent = kvpLocation.Entry.SearchUpSoc(soc => importedIDs.ContainsKey(soc.GUID));
                if (nearestImportedParent == null) continue;

                kvpLocation.Record.ParentLocation = importedIDs[nearestImportedParent.GUID];
            }

            //Second pass for bodies
            foreach (var kvpLocation in ImportedLocations)
            {
                //Find the first parent that is a record we've imported
                var nearestImportedParent = kvpLocation.Entry.SearchUpSoc(soc => importedIDs.ContainsKey(soc.GUID));
                if (nearestImportedParent == null) continue;

                kvpLocation.Record.ParentLocation = importedIDs[nearestImportedParent.GUID];
            }
        }
    }
}
