﻿using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using TradeInSpace.Data;
using TradeInSpace.Explore;
using TradeInSpace.Explore.DataForge;
using TradeInSpace.Explore.SC;
using TradeInSpace.Explore.SC.Components;
using TradeInSpace.Explore.SC.Data;
using TradeInSpace.Explore.SC.Loadout;
using TradeInSpace.Models;

namespace DataImport.Importers
{
    public class GameEquipmentImporter : DataImporter
    {
        /*
        static string[] IgnoreTypes = new string[]
        {
            "NOITEM_Player", "DoorController", "WheeledController", "Sensor", "Seat", "SeatAccess", "LightController",
            "Display","Decal","ControlPanel","CommsController","Usable","Transponder",
            "StatusScreen","SeatDashboard", "Flair_Floor"
        };
        */
        static string[] IgnoreTypes = new string[0];

        static Dictionary<string, string> NameFixes = new Dictionary<string, string>()
        {
            { "rsi_explorer_undersuit_01_01_01", "Venture Undersuit Alpha" },
            { "rsi_odyssey_undersuit_01_01_01", "Odyssey II Undersuit Alpha" },
            //More helpful renames than anything...
            {"aegs_avenger_nose_s3", "Avenger Nose VariPuck S3 Gimbal Mount" },
            {"aegs_javelin_manned_turret_gimbal_s5", "Javelin Mannet Turret VariPuck S5 Gimbal Mount" },
            {"mount_gimbal_mining", "Mining - VariPuck S2 Gimbal Mount" },

            //Ignore equipments
            {"carryable_1h_cu_glowstick_pink_active_prison", null },
            {"carryable_1h_cu_glowstick_pink_active", null },
            {"grin_multitool_01_default_mining", null },
            {"grin_multitool_01_default_grapple", null },
            {"grin_multitool_01_default_cutter", null },
            {"grin_multitool_01_ai", null },
            {"grin_multitool_01_energy_placeholder", null },
            {"grin_multitool_01_default_cutter_ai", null },
            {"carryable_1h_sq_missionitem_hackingchip_debug", null },
            {"carryable_1h_sq_missionitem_hackingchip_spd1rel1_bootleg", null },
            {"clothing_undersuit_no_oxygen", null },
            {"clothing_undersuit", null },
        };

        public GameEquipmentImporter(ManufacturerImporter mfgImporter, ILoggerFactory loggerFactor)
            : base(loggerFactor.CreateLogger("Game Ship Equipment Importer"))
        {
            MfgImporter = mfgImporter;
        }

        public ManufacturerImporter MfgImporter { get; }

        public override void CreateRecords(TISContext context, DFDatabase rawGameData, GameDatabase workingDataset)
        {
            var attachableEntities = rawGameData
                .GetComponentsOnEntities<AttachableComponent>();

            var allVehicles = rawGameData.GetComponentsOnEntities<Vehicle>();
            //var vehicleInsurance = rawGameData.GetEntries<ShipInsurance>().Single().Ships
            //    .Select(si => si.ShipClass);

            //var insuranceVehicles = allVehicles.Where(v => vehicleInsurance.Contains(v.ParentEntity.InstanceName));

            var allEquipment = rawGameData.GetEntitiesWithComponents<AttachableComponent>().Distinct().ToList();
            

            foreach (var equipmentGroup in allEquipment.GroupBy(eq => eq.GetComponent<AttachableComponent>()?.AttachmentType ?? "N/A"))
            {
                var processedItems = 0;
                Logger?.LogInformation($"Equipment Category: {equipmentGroup.Key} [{equipmentGroup.Count()} Items]");

                foreach (var equipment in equipmentGroup)
                {
                    var attach = equipment.GetComponent<AttachableComponent>();
                    //var item = component.GetComponent<SCItem>();
                    processedItems++;

                    switch (attach.AttachmentType)
                    {
                        default:
                            var structure = LoadoutTree.CreateFromEntity(equipment, false);
                            var loadout = equipment.GetComponent<DefaultLoadoutParams>();

                            var fixedName = attach.Name;
                            var cleanKey = Utilities.CleanKey(equipment.InstanceName);



                            if (cleanKey.Contains("_pu_ai") ||      //AI
                                cleanKey.Contains("_ai_") ||        //AI
                                cleanKey.Contains("_s42") ||        //SQ42
                                cleanKey.Contains("_sq42") ||       //SQ42
                                cleanKey.Contains("_hijacked") ||   //Hijacked Variant
                                cleanKey.Contains("_wreck") ||      //Planet wrecks
                                cleanKey.Contains("_shubin") ||     //Named wreck?
                                cleanKey.EndsWith("_nointerior") || //Exterior only variants
                                cleanKey.EndsWith("_tow") ||        //Theaters of War
                                cleanKey.EndsWith("_template") ||   //Template/unsued variants
                                //cleanKey.EndsWith("hd_sec") ||      //Hurston Security??
                                //cleanKey.EndsWith("microtech") ||   //Microtech Security?
                                cleanKey.EndsWith("centurion") ||   //No idea....
                                cleanKey.EndsWith("_test_inf_ammo") ||   //I want.....
                                cleanKey.EndsWith("_test_no_sound") ||   //Shhhh
                                cleanKey.EndsWith("scientist"))     //Smart guys
                            {
                                Logger.LogDebug($"Skipping equipment classified as AI/SQ42/TOW/etc. {cleanKey}");
                                continue;
                            }

                            if (cleanKey.EndsWith("gld") && !fixedName.Contains("Gold") && fixedName != "G-2 Helmet Executive")
                            {
                                fixedName += " (Gold)";
                            }

                            if (rawGameData.Localize($"@item_Name{equipment.InstanceName}", true) != null)
                            {
                                fixedName = rawGameData.Localize($"@item_Name{equipment.InstanceName}");
                            }
                            else if (rawGameData.Localize($"@item_Name_{equipment.InstanceName}", true) != null)
                            {
                                fixedName = rawGameData.Localize($"@item_Name_{equipment.InstanceName}");
                            }

                            if (NameFixes.ContainsKey(cleanKey))
                            {
                                fixedName = NameFixes[cleanKey];
                                if (fixedName == null)
                                {
                                    Log.Information($"Skipping ignored equipment key {cleanKey}");
                                    continue;
                                }
                            }

                            if (structure?.Root?.PortName?.EndsWith("Alpha") ?? false)
                            {
                                fixedName = structure.Root.PortName;
                            }

                            //Replace non-breaking spaces with regular, and trim excess whitespace
                            //Also "directional" quotes with regular ones
                            //Sad that we have to do this but here we are...
                            fixedName = fixedName
                                .Replace('\u00A0', ' ')
                                .Replace('\u0060', '\'')
                                .Replace('\u00B4', '\'')
                                .Replace('\u2018', '\'')
                                .Replace('\u2019', '\'')
                                .Replace('\u201C', '"')
                                .Replace('\u201D', '"')
                                .Trim();

                            var dbAttach = new GameEquipment()
                            {
                                SCRecordID = equipment.ID,

                                Name = fixedName,
                                Key = cleanKey,
                                Description = attach.Description.Replace(@"\n", "\n"),

                                Health = equipment.GetComponent<HealthComponent>()?.Health,
                                Mass = equipment.GetComponent<PhysicsComponent>()?.Mass,

                                AttachmentType = attach.AttachmentType,
                                AttachmentSubtype = attach.Subtype,
                                Size = attach.Size,
                                Grade = attach.Grade,
                                Tags = string.Join(",", attach.Tags.ToArray()),
                                RequiredTags = string.Join(",", attach.RequiredTags.ToArray()),

                                Manufacturer = attach.Manufacturer?.Name,
                                _Manufacturer = MfgImporter.ImportedManufacturers.FirstOrDefault(im => im.Entry == attach.Manufacturer)?.Record,
                                PortLayout = structure?.GetStructureJSON(IgnoreTypes),
                                DefaultLoadout = loadout?.GetLoadoutJSON(IgnoreTypes),
                                Properties = GetAdditionPropertiesJSON(attach)
                            };

                            //Seperate \ and \n charaters in regex because the description is un-escaped
                            var classMatch = Regex.Match(dbAttach.Description, @"\nClass: ([^\\\n]*?)[\\\n]");

                            if (classMatch.Success)
                            {
                                //Fix non breaking spaces
                                dbAttach.Class = classMatch.Groups[1].Value.Replace('\u00A0', ' ');

                                if (dbAttach.Class == "Stealt") dbAttach.Class = "Stealth";
                            }
                            else
                            {
                                dbAttach.Class = "Other";
                            }

                            workingDataset.Equipment.Add(dbAttach);

                            Logger?.LogInformation($"[{equipmentGroup.Key}: {processedItems}/{equipmentGroup.Count()}] Imported Equipment {dbAttach.Name} - Type {dbAttach.AttachmentType}/{dbAttach.AttachmentSubtype} - S{dbAttach.Size}:G{dbAttach.Grade}");

                            break;

                        case var ignored when IgnoreTypes.Contains(ignored):
                            break;
                    }
                }

                Logger?.LogInformation($"Category Completed: {equipmentGroup.Key}");
                Logger?.LogInformation($"");
            }
        }

        public string GetAdditionPropertiesJSON
            (AttachableComponent componentAttachable)
        {
            JObject additionalProperties = new JObject();

            var componentEntity = componentAttachable.ParentEntity;

            //Relies on components explicitly opting-out of all properties
            //Then using [JsonProperty] to explicitly mark the properties we care about here

            AddProperties(additionalProperties, componentAttachable, "Attachable");
            AddProperties(additionalProperties, componentEntity.GetComponent<IFCSComponent>(), "IFCS");
            AddProperties(additionalProperties, componentEntity.GetComponent<ArmorComponent>(), "Armor");
            AddProperties(additionalProperties, componentEntity.GetComponent<CoolerComponent>(), "Cooler");
            AddProperties(additionalProperties, componentEntity.GetComponent<WeaponComponent>(), "Weapon");
            AddProperties(additionalProperties, componentEntity.GetComponent<QDriveComponent>(), "QDrive");
            AddProperties(additionalProperties, componentEntity.GetComponent<ShieldComponent>(), "Shield");
            AddProperties(additionalProperties, componentEntity.GetComponent<MissileComponent>(), "Missile");
            AddProperties(additionalProperties, componentEntity.GetComponent<HeatDrawComponent>(), "Heat");
            AddProperties(additionalProperties, componentEntity.GetComponent<ClothingComponent>(), "Cloth");
            AddProperties(additionalProperties, componentEntity.GetComponent<CargoGridComponent>(), "Cargo");
            AddProperties(additionalProperties, componentEntity.GetComponent<SuitArmorComponent>(), "FPSArmor");
            AddProperties(additionalProperties, componentEntity.GetComponent<RegenPoolComponent>(), "RegenPool");
            AddProperties(additionalProperties, componentEntity.GetComponent<ConsumableComponent>(), "Food");
            AddProperties(additionalProperties, componentEntity.GetComponent<MiningLaserComponent>(), "MiningLaser");
            AddProperties(additionalProperties, componentEntity.GetComponent<AmmoContainerComponent>(), "Ammo");
            AddProperties(additionalProperties, componentEntity.GetComponent<RemovableChipComponent>(), "Chip");
            AddProperties(additionalProperties, componentEntity.GetComponent<AttachableModifierComponent>(), "Modifier");


            var power = componentEntity.GetComponent<PowerDrawComponent>();
            var powerPlant = componentEntity.GetComponent<PowerPlantComponent>();

            if (power != null)
            {
                //energy generation is based on the power component
                //and just "generates" when it has the power plant component
                if (powerPlant != null)
                {
                    AddProperties(additionalProperties, power, "PowerPlant");
                }
                else
                {
                    AddProperties(additionalProperties, power, "Energy");
                }
            }

            var tank = componentEntity.GetComponent<FuelTankComponent>();

            if (tank != null)
            {
                switch (componentAttachable.AttachmentType) {
                    case "QuantumFuelTank":
                    case "ExternalFuelTank":
                    case "FuelTank":
                        AddProperties(additionalProperties, tank, componentAttachable.AttachmentType);
                        break;
                    case "FuelIntake":
                        break;
                    default:
                        break;
                }
            }

            return additionalProperties.ToString();
        }

        private void AddProperties(JObject ObjectToUpdate, Object MergeWith, string PropertiesKey)
        {
            if (MergeWith == null) return;

            ObjectToUpdate[PropertiesKey] = JObject.FromObject(MergeWith);
        }
    }



    /*
     * Full list of types as of late 3.8
     * 
     * AmmoBox
     * UNDEFINED
     * Room
     * Audio
     * Usable
     * Battery
     * Display
     * WeaponPersonal
     * Char_Armor_Helmet
     * Light
     * Misc
     * LifeSupport
     * Elevator
     * Player
     * AIModule
     * Door
     * StatusScreen
     * ControlPanel
     * AirTrafficController
     * Cargo
     * Paints
     * Char_Hair_Color
     * Char_Skin_Color
     * Container
     * Char_Flair
     * Char_Armor_Arms
     * Char_Armor_Torso
     * Char_Armor_Legs
     * Char_Armor_Undersuit
     * Char_Body
     * Char_Clothing_Feet
     * Char_Clothing_Hands
     * Char_Clothing_Hat
     * Char_Clothing_Legs
     * Char_Clothing_Torso_0
     * Char_Clothing_Torso_1
     * Char_Head_Eyebrow
     * Char_Accessory_Eyes
     * Char_Head_Eyes
     * Char_Head
     * Char_Accessory_Head
     * Char_Head_Eyelash
     * Char_Head_Hair
     * Char_Lens
     * Gadget
     * Decal
     * Visor
     * Button
     * Lightgroup
     * Sensor
     * PowerPlant
     * ShopDisplay
     * Flair_Surface
     * Bottle
     * Flair_Floor
     * Flair_Wall
     * Cloth
     * HangarStock
     * HangarExpansion
     * Suit
     * Debris
     * Armor
     * CommsController
     * MiningController
     * CoolerController
     * DoorController
     * EnergyController
     * LightController
     * TargetSelector
     * WeaponController
     * WheeledController
     * Cooler
     * WeaponDefensive
     * SeatDashboard
     * WeaponGun
     * Ping
     * Scanner
     * FuelTank
     * QuantumFuelTank
     * FuelIntake
     * LandingSystem
     * MissileLauncher
     * Turret
     * QuantumInterdictionGenerator
     * Radar
     * Seat
     * SeatAccess
     * SelfDestruct
     * Shield
     * ManneuverThruster
     * MainThruster
     * TurretBase
     * MiningModifier
     * MiningArm
     * EMP
     * Missile
     * WeaponMining
     * MobiGlas
     * WeaponAttachment
     * Transponder
     * FlightController
     * Char_Clothing_Torso_2
     * ShieldController
     * QuantumDrive
     * RemovableChip
     * 
     */
}
