﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Xml;
using Azure.Storage.Blobs;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using TradeInSpace.Data;
using TradeInSpace.Explore;
using TradeInSpace.Explore.DataForge;
using TradeInSpace.Explore.SC;
using TradeInSpace.Explore.SC.Components;
using TradeInSpace.Explore.SC.Data;
using TradeInSpace.Explore.SC.Loadout;
using TradeInSpace.Models;
using static TradeInSpace.Models.Enums;

namespace DataImport.Importers
{
    public class GameShipImporter : DataImporter
    {
        public ManufacturerImporter MfgImporter { get; }

        public GameShipImporter(ManufacturerImporter mfgImporter, ILoggerFactory factory = null)
            : base(factory?.CreateLogger("Game Ship Importer"))
        {
            MfgImporter = mfgImporter;
        }

        public override void CreateRecords(TISContext context, DFDatabase rawGameData, GameDatabase workingDataset)
        {
            var allVehicles = rawGameData.GetComponentsOnEntities<Vehicle>();

            foreach (var vehicle in allVehicles)
            {
                var vehicleEnt = vehicle.ParentEntity;
                string vehicleKey = Utilities.CleanKey(vehicleEnt.InstanceName);

                if (vehicle.Insurance == null)
                {
                    Logger.LogDebug($"Skipping vehicle with no insurance data. {vehicleEnt.InstanceName}");
                    continue;
                }

                if (!vehicle.PlayerShip)
                {
                    Logger.LogDebug($"Skipping template/AI vehicle. {vehicleEnt.InstanceName}");
                    continue;
                }

                var vehicleAttachment = vehicleEnt.GetComponent<AttachableComponent>();

                var tree = LoadoutTree.CreateFromVehicle(vehicle, true);
                if (tree == null) continue;

                var loadoutFlat = Utilities.FlattenRecursive(tree.Root, t => t.Children);

                var allAttached = loadoutFlat
                    .Select(c => c.Port?.AttachedComponent)
                    .Where(c => c != null);

                var cargoEntities = allAttached
                    .Select(c => c.GetComponent<CargoGridComponent>())
                    .Where(c => c != null)
                    .ToList();
                var openInventories = allAttached
                    .Select(c => c.GetComponent<SCItemInventoryContainer>())
                    .Where(c => c?.Container != null && c.Container.IsOpen)
                    .ToList();

                PadSize? shipSize = (PadSize)vehicleAttachment.Size;
                int? cargoSize = null;
                if (cargoEntities.Count > 0 || openInventories.Count > 0)
                {
                    cargoSize = 
                        cargoEntities.Sum(ce => ce.SCU_Actual) + 
                        openInventories.Sum(oi => (int)oi.Container.uSCUCapacity / (int)CommoditySizes_uSCU.SCU);
                }

                //Search by ports, not by installed component
                var qDriveComponents = loadoutFlat
                    .Where(c => c?.Port?.PortTypes?.Any(p => p.Matches("QuantumDrive")) ?? false)
                    .ToList();

                int? qDriveSize = null;

                if (qDriveComponents.Count > 0)
                {
                    qDriveSize = qDriveComponents.First().Port.MaxSize;
                }

                Logger?.LogInformation($"Parsed {shipSize?.ToString() ?? "Unknown Sized"} ship {vehicle.Name} with cargo capacity of {cargoSize} SCU");

                var defaults = vehicleEnt.GetComponent<DefaultLoadoutParams>();
                string uploadedBlobPath = null;

                // TODO: Proper linux support
                // Also disabled for now since i'm not using the images. maybe renders now....
                /*
                if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
                {
                    try
                    {
                        if (ShipImageFixes.ContainsKey(vehicleKey))
                        {
                            var imagePath = Path.Combine("UI", "Frontend", "assets", "TIF", "Ships", ShipImageFixes[vehicleKey]);

                            var ddsBytes = rawGameData.ReadPackedFileBinary(imagePath);
                            if (ddsBytes != null)
                            {
                                var pngBytes = ImageTools.ConvertDDSToPNG(ddsBytes);

                                string uploadedName = Path.ChangeExtension(vehicleKey, "png");

                                var existingBlob = BlobContainer.GetBlobClient(uploadedName);

                                if (existingBlob.Exists())
                                {
                                    Logger.LogWarning($"Overwriting image at {existingBlob.Uri}");
                                }

                                using (var writeStream = new MemoryStream(pngBytes))
                                {
                                    var uploadResponse = existingBlob.Upload(writeStream, true);
                                }

                                uploadedBlobPath = existingBlob.Uri.ToString();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Logger.LogError(ex, "Error loading ship image.");
                    }
                }
                */

                var additionalProperties = new
                {
                    BaseWaitTimeMinutes = vehicle.Insurance.BaseWaitTimeMinutes,
                    ExpeditedWaitTimeMinutes = vehicle.Insurance.ExpeditedWaitTimeMinutes,
                    ExpediteFee = vehicle.Insurance.ExpediteFee,
                    Career = vehicle.Career,
                    Role = vehicle.Role,
                    Dimensions = new
                    {
                        X = vehicle.Size_X,
                        Y = vehicle.Size_Y,
                        Z = vehicle.Size_Z
                    }
                };

                var dbManufacturer = MfgImporter.ImportedManufacturers.FirstOrDefault(im => im.Entry == vehicle.Manufacturer)?.Record;

                workingDataset.Ships.Add(new GameShip()
                {
                    SCRecordID = vehicleEnt.ID,
                    Key = vehicleKey,
                    Name = vehicle.Name,
                    Manufacturer = vehicle.Manufacturer?.Name,
                    ManufacturerID = dbManufacturer?.ID,
                    _Manufacturer = dbManufacturer,
                    Description = vehicle.Description,
                    QuantumDriveSize = qDriveSize,
                    CargoCapacity = cargoSize,
                    Size = shipSize,
                    ImageURL = uploadedBlobPath,
                    AdditionalProperties = JsonConvert.SerializeObject(additionalProperties)
                });
            }
        }
    }
}
