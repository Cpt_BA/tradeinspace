﻿using Castle.DynamicProxy.Generators;
using DataImport.Importers.SLPostProcess;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.VisualBasic.CompilerServices;
using Serilog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Xml;
using System.Xml.Serialization;
using TradeInSpace.Data;
using TradeInSpace.Explore;
using TradeInSpace.Explore.DataForge;
using TradeInSpace.Explore.SC.Components;
using TradeInSpace.Explore.Subsumption;
using TradeInSpace.Models;

namespace DataImport.Importers
{
    public class TradeImporter : DataImporter
    {
        public TradeImporter(TradeShopPostProcessor shopImporter, SubsumptionManager subsumptionManager, ILoggerFactory factory = null)
            : base(factory?.CreateLogger("Trade Importer"))
        {
            ShopImporter = shopImporter;
            SubsumptionManager = subsumptionManager;
        }

        public TradeShopPostProcessor ShopImporter { get; }
        public SubsumptionManager SubsumptionManager { get; }

        public override void CreateRecords(TISContext context, DFDatabase rawGameData, GameDatabase workingDataset)
        {
            // Initialize trade table for this import if one doesn't exist.
            TradeTable ImportTradeTable;
            if (workingDataset.TradeTables.Any())
            {
                ImportTradeTable = workingDataset.TradeTables.First();
            }
            else
            {
                ImportTradeTable = new TradeTable()
                {
                    Entries = new List<TradeTableEntry>(),
                    EquipmentEntries = new List<TradeEquipmentEntry>(),
                    Active = false,
                    Name = "Test",
                };
                workingDataset.TradeTables.Add(ImportTradeTable);
            }

            Log.Information("Loading Subsumption Trade Files");

            var productPrices = rawGameData.ReadPackedFileCryXML(@"Data\Libs\Subsumption\Shops\RetailProductPrices.xml");

            var templateFiles = rawGameData.ReadAllCryXML("Data/Libs/Subsumption/Shops/Templates", ".xml").ToList();

            var products = productPrices.Deserialize<Node>("Subsumption");
            var rentalTemplates = templateFiles
                .SelectMany(tf => tf.SelectNodes("/Subsumption/ShopRentalTemplate").Cast<XmlElement>())
                .DeserializeBulk<RentalTemplateEntry>("ShopRentalTemplate")
                .ToDictionary(rt => rt.ID);


            var flatProducts = Utilities.FlattenRecursive(products, p => p.RetailProducts).ToList();

            Log.Information($"Loaded. {flatProducts.Count} Product entries");

            Func<Guid, Node> findProduct = (id) => flatProducts.Single(p => p.ID == id);
            Func<string, TradeShop> findShopByElement = (superGuid) =>
            {
                var parts = superGuid.Split(",").Select(g => Guid.Parse(g)).ToList();
                SystemLocation searchLoc = workingDataset.SystemLocations.Single(l => l.SCRecordID == parts[0]);
                for (int i = 1; i < parts.Count; i++)
                {
                    var child = searchLoc.ChildLocations.SingleOrDefault(cl => cl.SCRecordID == parts[i]);
                    if (child != null)
                    {
                        searchLoc = child;
                    }
                    else if(i == parts.Count - 1)
                    {
                        return null;
                    }
                }
                return workingDataset.TradeShops.Single(ts => ts.Location == searchLoc);
            };

            var newCommodityTrades = new List<TradeTableEntry>();
            var newEquipmentTrades = new List<TradeEquipmentEntry>();

            var seenMapping = new HashSet<string>();

            foreach(var importedShop in ShopImporter.ImportedShops)
            {
                var superGUID = importedShop.entity.SuperGUID;
                var dbShop = importedShop.shop;

                var shopLayoutMapping = SubsumptionManager.LookupShopProfileExact(superGUID);

                if(shopLayoutMapping == null)
                {
                    Log.Warning($"No shop inventory profile defined for shop {importedShop.entity.Label}/{superGUID}");
                    continue;
                }

                seenMapping.Add(shopLayoutMapping.ShopSuperGUID);
                var shop = SubsumptionManager.GetShopLayout(shopLayoutMapping.LayoutGUID);

                if(shop == null)
                {
                    Log.Warning($"No shop inventory profile data defined for shop {importedShop.entity.Label}/{superGUID}");
                    continue;
                }

                var shopCommodityTrades = new List<TradeTableEntry>();
                var shopEquipmentTrades = new List<TradeEquipmentEntry>();

                foreach (var trade in shop.Shop.ShopInventoryNodes)
                {
                    var subProduct = findProduct(trade.InventoryID);

                    var dbProduct = workingDataset.Equipment.SingleOrDefault(e => e.SCRecordID == trade.InventoryID);
                    var dbCommodity = workingDataset.TradeItems.SingleOrDefault(t => t.SCRecordID == trade.InventoryID);

                    var basePrice = subProduct.BasePrice * (1.0f + (trade.BasePriceOffsetPercentage / 100.0f));
                    var minPrice = basePrice * (1.0f - (trade.MaxDiscountPercentage / 100.0f));
                    var maxPrice = basePrice * (1.0f + (trade.MaxPremiumPercentage / 100.0f));
                    var avgPrice = (minPrice + maxPrice) / 2.0f;
                    var tradeType = trade.TransactionType == "Buy" ? Enums.TradeType.Buy : Enums.TradeType.Sell;

                    if(dbProduct == null && dbCommodity == null)
                    {
                        Log.Error($"Unable to find product {trade.Name}/{trade.InventoryID}");
                        //throw new Exception($"Unable to find product {trade.InventoryID}");
                    }

                    //Duplicate equipment import for location?
                    var existingTrade = shopEquipmentTrades.FirstOrDefault(set => set.Equipment == dbProduct && set.Station == dbShop);
                    if (dbProduct != null && existingTrade != null)
                    {
                        if(tradeType == Enums.TradeType.Buy && existingTrade.BuyPrice == 0)
                            existingTrade.BuyPrice = maxPrice;
                        else if(tradeType == Enums.TradeType.Sell && existingTrade.SellPrice == 0)
                            existingTrade.SellPrice = minPrice;
                        
                        continue; // Always continue
                    }

                    //Duplicate commodity import?
                    if (dbCommodity != null && shopCommodityTrades.Any(sct => sct.Item == dbCommodity && sct.Station == dbShop))
                        continue;

                    switch (trade.TransactionType)
                    {
                        case "Buy":
                        case "Sell":
                        case null:
                            if (dbProduct != null)
                            {
                                shopEquipmentTrades.Add(new TradeEquipmentEntry()
                                {
                                    Equipment = dbProduct,
                                    Station = dbShop,
                                    BuyPrice = tradeType == Enums.TradeType.Buy ? maxPrice : 0,
                                    SellPrice = tradeType == Enums.TradeType.Sell ? minPrice : 0,
                                    Quantity = trade.Inventory
                                });
                            }
                            else if (dbCommodity != null)
                            {
                                shopCommodityTrades.Add(new TradeTableEntry()
                                {
                                    Item = dbCommodity,
                                    Station = dbShop,
                                    MinUnitPrice = minPrice,
                                    MaxUnitPrice = maxPrice,
                                    Type = tradeType,
                                    UnitCapacityRate = (trade.MaxInventory) * trade.RefreshRatePercentagePerMinute / 100.0f,
                                    UnitCapacityMax = trade.MaxInventory
                                });
                            }
                            break;
                        case "Rent":
                            foreach(var rentalTemplateID in trade.RentalTemplates)
                            {
                                var rentalGUID = Guid.Parse(rentalTemplateID.Data);

                                if (rentalGUID == Guid.Empty || !rentalTemplates.ContainsKey(rentalGUID))
                                    continue;

                                var template = rentalTemplates[rentalGUID];

                                shopEquipmentTrades.Add(new TradeEquipmentEntry()
                                {
                                    Equipment = dbProduct,
                                    Station = dbShop,
                                    BuyPrice = avgPrice * (template.PercentageOfSalePrice / 100.0f),
                                    SellPrice = avgPrice * (template.PercentageOfSalePrice / 100.0f),
                                    RentalDuration = template.RentalDuration
                                });
                            }
                            //var rentals = 
                            break;
                        default:
                            break;
                    }

                }

                int buyTrades = shopCommodityTrades.Count(c => c.Type == Enums.TradeType.Buy),
                    sellTrades = shopCommodityTrades.Count(c => c.Type == Enums.TradeType.Sell);

                Log.Information($"Imported {shopEquipmentTrades.Count} equipment trades, and {shopCommodityTrades.Count} commodity trades (B:{buyTrades}\\S:{sellTrades}) at location {shop.Shop.Name}");

                newEquipmentTrades.AddRange(shopEquipmentTrades);
                newCommodityTrades.AddRange(shopCommodityTrades);
            }

            Log.Information($"Total: {newEquipmentTrades.Count} equipment trades, and {newCommodityTrades.Count} commodity trades.");
            Log.Information($"Imported Shops: {ShopImporter.ImportedShops.Count()}");
            Log.Information($"Shop Layouts: {seenMapping.Count()}.");
            
            if (seenMapping.Count() != ShopImporter.ImportedShops.Count())
            {
                Log.Error($"Did not assign a layout to all shops after parsing. This may means inventories were not imported properly.");
            }

            foreach (var ee in newEquipmentTrades) ImportTradeTable.EquipmentEntries.Add(ee);
            foreach (var te in newCommodityTrades) ImportTradeTable.Entries.Add(te);
        }
    }

    
    public class RentalTemplateEntry
    {
        [XmlAttribute]
        public Guid ID { get; set; }
        [XmlAttribute]
        public string Name { get; set; }
        [XmlAttribute]
        public float PercentageOfSalePrice { get; set; }
        [XmlAttribute]
        public float RentalDuration { get; set; }
    }

    public class Node
    {
        [XmlAttribute]
        public Guid ID { get; set; }
        [XmlAttribute]
        public int Index { get; set; }
        [XmlAttribute]
        public string Name { get; set; }
        [XmlAttribute]
        public string AutoGenerated { get; set; }
        [XmlAttribute]
        public float BasePrice { get; set; }
        [XmlAttribute]
        public float MaxDiscount { get; set; }
        [XmlAttribute]
        public float MaxPremium { get; set; }

        public Node[] RetailProducts { get; set; }
    }
}
