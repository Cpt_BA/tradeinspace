﻿using ICSharpCode.SharpZipLib.Zip;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Linq;
using System.Xml;
using TradeInSpace.Models;
using static TradeInSpace.Models.Enums;
using TradeInSpace.Explore.Socpak;
using DataImport.GameModels;
using Microsoft.Extensions.Logging;
using TradeInSpace.Data;
using TradeInSpace.Explore;
using TradeInSpace.Explore.DataForge;
using TradeInSpace.Explore.SC.Data;
using TradeInSpace.Explore.Socpak.Components;
using TradeInSpace.Explore.Subsumption;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Serilog;
using DN = System.DoubleNumerics;
using DataImport.Utils;

namespace DataImport.Importers
{
    public class SystemBodyImporter : DataImporter
    {
        public ParserResponseCollection<SOC_Entry, SystemBody> ImportedBodies { get; set; }
        public ParserResponseCollection<SOC_Entry, SystemLocation> ImportedLocations { get; set; }
        public SubsumptionManager SubsumptionManager { get; private set; }

        public SystemBodyImporter(SubsumptionManager subsumptionManager, ILoggerFactory factory = null)
            : base(factory?.CreateLogger("System Body Importer"))
        {
            SubsumptionManager = subsumptionManager;
        }

        public override void CreateRecords(TISContext context, DFDatabase rawGameData, GameDatabase workingDataset)
        {
            ImportedBodies = new ParserResponseCollection<SOC_Entry, SystemBody>();
            ImportedLocations = new ParserResponseCollection<SOC_Entry, SystemLocation>();

            //SubsumptionManager.ExploreAllSystems();

            var planetNodes = SubsumptionManager.AllNodes
                .Where(sn => sn.SOC_Entry?.Class?.Contains("OrbitingObjectContainer") ?? false)
                .ToList();

            Log.Information($"Importing {planetNodes.Count} planet nodes ({SubsumptionManager.AllNodes.Count} total nodes)");

            foreach(var planetNode in planetNodes)
            {
                var bodySOC = planetNode.SOC_Entry;

                bodySOC.LoadFullDetails();
                bodySOC.LoadAllEntities(false);

                if (bodySOC.StarmapRecord.HasValue && 
                    !string.IsNullOrEmpty(bodySOC.Name) && 
                    (bodySOC?.Class?.Contains("OrbitingObjectContainer") ?? false))
                {
                    var bodyEntity = bodySOC.OwnEntity;
                    if (bodyEntity == null) continue;
                    
                    var orbitComponent = bodyEntity.Components.SingleOrDefault(c => c.ComponentType == "SOrbitComponentParams");
                    if (orbitComponent == null) continue;

                    var starmapDF = rawGameData.GetEntry<StarmapObject>(bodySOC.StarmapRecord.Value, AllowMissing: true);
                    if (starmapDF == null)
                    {
                        Logger.LogWarning($"Unable to find starmap entry {bodySOC.StarmapRecord.Value} for planet {planetNode.Name}");
                        continue;
                    }
                    
                    BodyType? bodyType = null;

                    switch (starmapDF.ObjectType.Name)
                    {
                        case "Planet": bodyType = BodyType.Planet; break;
                        case "Moon": bodyType = BodyType.Moon; break;
                        case "Manmade": bodyType = BodyType.Other; break;
                        case "Lagrange Point": bodyType = BodyType.AsteroidField; break;
                        case "Star": bodyType = BodyType.Star; break;
                        case "PointOfInterest": bodyType = BodyType.Other; break;
                        case "CardinalPoint":
                        case "Asteroid":
                        case "Outpost":
                        case "LandingZone":
                        case "QuantumTracePoint":
                        case "YouAreHere":
                            break;
                        default:
                            throw new Exception($"Unknown starmap point type {starmapDF.ObjectType.Name}");
                    }

                    //var bodyTags = entityDoc.SelectNodes("//Tag/@TagId").Cast<XmlAttribute>().Select(a => rawGameData.Tags[Guid.Parse(a.Value)]).ToArray();

                    double orbitRadius = double.Parse(orbitComponent.FromElement.SelectSingleNode("@OrbitalRadius").InnerText);
                    double orbitStart = double.Parse(orbitComponent.FromElement.SelectSingleNode("@OrbitalAngle").InnerText);

                    if (orbitRadius == 0 && orbitStart == 0 && orbitComponent.FromElement.HasAttribute("parentGUID"))
                    {
                        if (planetNode.LocalPosition != null)
                        {
                            //TODO: Support azimuth
                            double xPos = planetNode.LocalPosition.X;
                            double yPos = planetNode.LocalPosition.Y;
                            double zPos = planetNode.LocalPosition.Z;

                            double calcRadius = Math.Sqrt(xPos * xPos + yPos * yPos);
                            double calcStart = Math.Atan2(yPos, xPos) / Math.PI * 180.0;

                            orbitRadius = calcRadius;
                            orbitStart = calcStart;
                        }
                        else
                        {
                            //Probably expected to have a position
                        }
                    }

                    

                    var parent = SearchParent(workingDataset, bodySOC);
                    var parentLocation = workingDataset.SystemLocations.FirstOrDefault(l => l.StarmapID == parent?.StarmapRecord);
                    var parentBody = workingDataset.SystemBodies.FirstOrDefault(l => l.SCRecordID == parent?.GUID);

                    float? Atmosphere = null;
                    float? Gravity = null;
                    float? Radius = null;
                    float? RotationSpeed = null;

                    if(bodySOC.PlanetEntity != null)
                    {
                        var planetParams = bodySOC.PlanetEntity.GetComponent<SOC_ProceduralEntity>();

                        Atmosphere = planetParams?.AtmosphereHeight;
                        Gravity = planetParams?.PlanetGravity;

                        if (bodySOC.PlanetJSON != null)
                        {
                            var extraDetails = JsonConvert.DeserializeObject<JObject>(bodySOC.PlanetJSON);
                            Radius = extraDetails.SelectToken("data.General.tSphere.fPlanetTerrainRadius").ToObject<float>();
                            RotationSpeed = extraDetails.SelectToken("data.General.fRotationSpeed").ToObject<float>();
                        }
                    }

                    bodySOC.CalculateGlobalPositions();
                    var bodyAdditional = CoordinateHelper.BuildLocationAdditionalProperties(bodySOC.Transform, Vector3.Zero);

                    var nameKey = Utilities.CleanKey(starmapDF.Name);
                    var extraData = ManualBodyData.ContainsKey(nameKey) ? ManualBodyData[nameKey] : null;

                    bodyAdditional.Rotation = new
                    {
                        X = extraData?.RotationOffset ?? 0,
                        Y = 0,
                        Z = 0
                    };
                    bodyAdditional.AtmosphereHeight = Atmosphere;
                    bodyAdditional.Gravity = Gravity;
                    bodyAdditional.Radius = Radius;
                    bodyAdditional.RotationSpeed = RotationSpeed;
                    bodyAdditional.OMRadius = extraData?.OrbitalMarkerRadius ?? 0;

                    var bodyLocation = new SystemLocation()
                    {
                        SCRecordID = bodySOC.GUID,
                        ParentLocation = parentLocation,
                        ParentBody = parentLocation,
                        StarmapID = bodySOC.StarmapRecord.Value,
                        Key = nameKey,
                        Name = starmapDF.Name,
                        Description = starmapDF.Description,
                        LocationType = Enum.Parse<LocationType>(bodyType.ToString()),
                        IsHidden = starmapDF.HiddenInStarmap,
                        QTDistance = starmapDF.HasQuantum ? (int?)starmapDF.ArrivalRadius : null,
                        ChildLocations = new List<SystemLocation>(),
                        AdditionalProperties = JsonConvert.SerializeObject(bodyAdditional)
                    };

                    var body = new SystemBody()
                    {
                        SCRecordID = bodySOC.GUID,
                        ParentBody = parentBody,
                        SystemLocation = bodyLocation,
                        Key = nameKey,
                        Name = starmapDF.Name,
                        Description = starmapDF.Description,
                        BodyType = bodyType.Value,
                        OrbitRadius = (float)orbitRadius,
                        OrbitStart = (float)orbitStart,
                        AtmosphereHeightKM = Atmosphere,
                        Gravity = Gravity,
                        BodyRadius = Radius,
                        AdditionalProperties = JsonConvert.SerializeObject(bodyAdditional)
                    };

                    bodyLocation.ParentBody = parentLocation;

                    if (ImportedBodies.HasGUID(bodySOC.GUID))
                    {
                        Logger?.LogError($"System body GUID already imported - {body.Name} - {bodySOC.GUID}");
                        throw new Exception("System body GUID already imported");
                    }

                    Logger?.LogInformation($"Imported System Body {body.Name} orbiting around {parentBody?.Name ?? "Nothing"}");

                    ImportedLocations.Add(bodySOC, bodyLocation);
                    ImportedBodies.Add(bodySOC, body);

                    workingDataset.SystemBodies.Add(body);
                    workingDataset.SystemLocations.Add(bodyLocation);
                }
            }
        }

        private SOC_Entry SearchParent(GameDatabase workingDataset, SOC_Entry fromChild)
        {
            var searchParent = fromChild.Parent;
            while(searchParent != null)
            {
                if (searchParent.StarmapRecord.HasValue) return searchParent;
                searchParent = searchParent.Parent;
            }
            return null;
        }

        // HACK: hardcoded data is bad mmkay
        public Dictionary<string, SystemBodyManualData> ManualBodyData = new Dictionary<string, SystemBodyManualData>()
        {
            { "aberdeen", new SystemBodyManualData() {
                OrbitalMarkerRadius = 402707,
                RotationOffset = 116.9448295
            } },
            { "arial", new SystemBodyManualData() {
                OrbitalMarkerRadius = 501541,
                RotationOffset = 38.5142349
            } },
            { "arccorp", new SystemBodyManualData() {
                OrbitalMarkerRadius = 1151000,
                RotationOffset = 230.8844272
            } },
            { "calliope", new SystemBodyManualData() {
                OrbitalMarkerRadius = 352245,
                RotationOffset = 212.4145472
            } },
            { "cellin", new SystemBodyManualData() {
                OrbitalMarkerRadius = 380528,
                RotationOffset = 253.6008872
            } },
            { "clio", new SystemBodyManualData() {
                OrbitalMarkerRadius = 489849,
                RotationOffset = 311.6916655
            } },
            { "crusader", new SystemBodyManualData() {
                OrbitalMarkerRadius = 10758000,
                RotationOffset = 300.5056215
            } },
            { "daymar", new SystemBodyManualData() {
                OrbitalMarkerRadius = 430029,
                RotationOffset = 30.2954426
            } },
            { "euterpe", new SystemBodyManualData() {
                OrbitalMarkerRadius = 314060,
                RotationOffset = 269.161587
            } },
            { "hurston", new SystemBodyManualData() {
                OrbitalMarkerRadius = 1438000,
                RotationOffset = 19.39
            } },
            { "ita", new SystemBodyManualData() {
                OrbitalMarkerRadius = 472452,
                RotationOffset = 243.1995839
            } },
            { "lyria", new SystemBodyManualData() {
                OrbitalMarkerRadius = 383203,
                RotationOffset = 359.4217912
            } },
            { "magda", new SystemBodyManualData() {
                OrbitalMarkerRadius = 494840,
                RotationOffset = 242.7977464
            } },
            { "microtech", new SystemBodyManualData() {
                OrbitalMarkerRadius = 1439000,
                RotationOffset = 217.2829681
            } },
            { "wala", new SystemBodyManualData() {
                OrbitalMarkerRadius = 413056,
                RotationOffset = 135.56548
            } },
            { "yela", new SystemBodyManualData() {
                OrbitalMarkerRadius = 455482,
                RotationOffset = 218.8119445
            } }
        };

        public class SystemBodyManualData
        {
            public double RotationOffset { get; set; }
            public double OrbitalMarkerRadius { get; set; }
        }
    }
}
