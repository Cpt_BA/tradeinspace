﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Xml;
using TradeInSpace.Models;
using Microsoft.Extensions.Logging;
using TradeInSpace.Data;
using TradeInSpace.Explore;
using TradeInSpace.Explore.DataForge;
using TradeInSpace.Explore.SC.Data;
using TradeInSpace.Explore.SC.Components;

namespace DataImport.Importers
{
    public class TradeItemImporter : DataImporter
    {
        public TradeItemImporter(ILoggerFactory factory = null)
            : base(factory?.CreateLogger("Trade Item Importer"))
        {
        }

        public override void CreateRecords(TISContext context, DFDatabase rawGameData, GameDatabase workingDataset)
        {
            var commDB = rawGameData.GetEntries<ResourceDatabase>().Single();
            var commodityEntities = rawGameData.GetComponentsOnEntities<SCCommodity>();

            var miningElements = rawGameData.GetEntries<MineableElement>();

            foreach (var gameCommodity in commDB.AllResources)
            {
                //Logic specifically because of processed food item.
                //Has two in-game entries for some reason.
                if (workingDataset.TradeItems.Any(ti => ti.Name == gameCommodity.Name))
                {
                    Logger.LogWarning($"Skipping duplicate item {gameCommodity.Name}");
                    continue;
                }

                /*
                if (gameCommodity.CommodityEntity == null)
                {
                    Logger.LogWarning($"Resource {gameCommodity.Name} has no matching commodity entity loaded");
                    continue;
                }
                */

                if (gameCommodity.Category == null)
                {
                    Logger.LogWarning($"Resource {gameCommodity.Name} has no Category/Type defined");
                    continue;
                }

                var dbItem = new TradeItem()
                {
                    SCRecordID = gameCommodity.ID,
                    Key = Utilities.CleanKey(gameCommodity.Name),
                    Name = gameCommodity.Name,
                    Description = gameCommodity.Description,
                    Category = gameCommodity.Category.Name,
                    Mineable = miningElements.Any(me => me.Resource == gameCommodity)
                };
                workingDataset.TradeItems.Add(dbItem);

                Logger?.LogInformation($"Imported Trade Item {dbItem.Name} - {dbItem.Category}");
            }
        }
    }
}
