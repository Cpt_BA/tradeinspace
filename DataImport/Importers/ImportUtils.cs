﻿using TradeInSpace.Explore.Socpak;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TradeInSpace.Explore;

namespace DataImport.Importers
{
    public static class ImportUtils
    {
        public abstract class SelfReferencing<T>
            where T : SelfReferencing<T>
        {
            public T Parent { get; set; } = null;
            public List<T> Children { get; set; } = new List<T>();
        }


        public static IEnumerable<T> FlatToTree<T>(IEnumerable<T> Entries, Func<T, IEnumerable<T>> ChildEnumerator) where T : SelfReferencing<T>
        {
            foreach(var entry in Entries)
            {
                var entryChildren = ChildEnumerator(entry);
                foreach(var child in entryChildren)
                {
                    child.Parent = entry;
                    entry.Children.Add(child);
                }
            }
            return Entries.Where(e => e.Parent == null);
        }
    }
}
