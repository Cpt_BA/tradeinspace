﻿using System;
using System.Collections.Generic;
using System.Text;
using TradeInSpace.Data;
using TradeInSpace.Explore.DataForge;
using SC = TradeInSpace.Explore.SC.Data;
using DB = TradeInSpace.Models;
using DataImport.GameModels;
using System.Linq;
using TradeInSpace.Explore.SC.Components;
using TradeInSpace.Explore;
using Microsoft.Extensions.Logging;
using static TradeInSpace.Models.Enums;

namespace DataImport.Importers
{
    public class LocationTradeServicesImporter : DataImporter
    {
        Dictionary<string, ServiceFlags> equipmentTypeMapping = new Dictionary<string, ServiceFlags>()
        {
            {"Char_Armor_Arms", ServiceFlags.FPSArmor },
            {"Char_Armor_Helmet", ServiceFlags.FPSArmor },
            {"Char_Armor_Legs", ServiceFlags.FPSArmor },
            {"Char_Armor_Torso", ServiceFlags.FPSArmor },
            {"Char_Armor_Undersuit", ServiceFlags.FPSArmor },
            {"Char_Clothing_Feet", ServiceFlags.FPSClothing },
            {"Char_Clothing_Hands", ServiceFlags.FPSClothing },
            {"Char_Clothing_Hat", ServiceFlags.FPSClothing },
            {"Char_Clothing_Legs", ServiceFlags.FPSClothing },
            {"Char_Clothing_Torso_0", ServiceFlags.FPSClothing },
            {"Char_Clothing_Torso_1", ServiceFlags.FPSClothing },
            {"Cooler", ServiceFlags.ShipEquipment },
            {"Drink", ServiceFlags.FPSFood },
            {"Food", ServiceFlags.FPSFood },
            {"Gadget", ServiceFlags.FPSEquipment },
            {"MiningModifier", ServiceFlags.ShipEquipment },
            {"Misc", ServiceFlags.FPSEquipment },
            {"Missile", ServiceFlags.ShipEquipment },
            {"MissileLauncher", ServiceFlags.ShipEquipment },
            {"MobiGlas", ServiceFlags.FPSEquipment },
            //{"NOITEM_Vehicle", ServiceFlags.ShipPurchase },
            {"Paints", ServiceFlags.ShipEquipment },
            {"PowerPlant", ServiceFlags.ShipEquipment },
            {"QuantumDrive", ServiceFlags.ShipEquipment },
            {"RemovableChip", ServiceFlags.FPSCrypto },
            {"Shield", ServiceFlags.ShipEquipment },
            {"Turret", ServiceFlags.ShipEquipment },
            {"WeaponAttachment", ServiceFlags.ShipEquipment },
            {"WeaponGun", ServiceFlags.ShipEquipment },
            {"WeaponMining", ServiceFlags.ShipEquipment },
            {"WeaponPersonal", ServiceFlags.FPSWeapon }
        };

        public LocationTradeServicesImporter(ILoggerFactory factory = null)
            : base(factory?.CreateLogger("Location Trade Services Importer"))
        {
        }

        public override void CreateRecords(TISContext context, DFDatabase rawGameData, DB.GameDatabase workingDataset)
        {
            foreach(var shop in workingDataset.TradeShops)
            {
                ServiceFlags servicesToAdd = ServiceFlags.None;

                switch (shop.ShopType)
                {
                    case ShopType.AdminRefinery:
                        servicesToAdd |= ServiceFlags.Refinery;
                        break;
                    case ShopType.Casaba:
                        servicesToAdd |= ServiceFlags.FPSClothing;
                        break;
                }

                if (shop.TradeEntries?.Any() ?? false)
                    servicesToAdd |= ServiceFlags.Trade;

                if (shop.EquipmentEntries?.Any() ?? false)
                {
                    foreach (var equipmentTrade in shop.EquipmentEntries)
                    {
                        if (equipmentTypeMapping.ContainsKey(equipmentTrade.Equipment.AttachmentType))
                            servicesToAdd |= equipmentTypeMapping[equipmentTrade.Equipment.AttachmentType];
                        else if(equipmentTrade.Equipment.AttachmentType == "NOITEM_Vehicle")
                        {
                            if (equipmentTrade.BuyPrice > 0) servicesToAdd |= ServiceFlags.ShipPurchase;
                            if (equipmentTrade.SellPrice > 0) servicesToAdd |= ServiceFlags.ShipRental;
                        }
                    }
                }

                shop.Location.ServiceFlags |= servicesToAdd;
            }
        }
    }
}
