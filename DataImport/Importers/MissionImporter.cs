﻿using DataImport.GameModels;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using TradeInSpace.Data;
using TradeInSpace.Explore;
using TradeInSpace.Explore.DataForge;
using TradeInSpace.Explore.SC.Data;

namespace DataImport.Importers
{
    using DB = TradeInSpace.Models;

    public class MissionImporter : DataImporter
    {
        public MissionImporter(ILoggerFactory factory = null)
            : base(factory?.CreateLogger("Mission Importer"))
        {
        }

        public ParserResponseCollection<SReputationScopeParams, DB.MissionGiverScope> ImportedScopes = new ParserResponseCollection<SReputationScopeParams, DB.MissionGiverScope>();
        public ParserResponseCollection<MissionGiver, DB.MissionGiver> ImportedGivers = new ParserResponseCollection<MissionGiver, DB.MissionGiver>();
        public ParserResponseCollection<MissionGiverEntry, DB.MissionGiverEntry> ImportedMissions = new ParserResponseCollection<MissionGiverEntry, DB.MissionGiverEntry>();

        public Dictionary<string, string> GiverNameRemap = new Dictionary<string, string>()
        {
            { "BlacJacBountyDepartment", "BlacJac Bounty Department" },
            { "BountyHunterGuild", "BountyHunter Guild" },
            { "ClovusDarneely", "Clovus Darneely" },
            { "ConstantineHurston", "Constantine Hurston" },
            { "CrusaderBountyDepartment", "Crusader Bounty Department" },
            { "HurstonBountyDepartment", "Hurston Bounty Department" },
            { "MilesEckhart", "Miles Eckhart" },
            { "MircoTechBountyDepartment", "MircoTech Bounty Department" },
            { "NorthRock_Bounty", "NorthRock Bounty" },
            { "ReccoBattaglia", "Recco Battaglia" },
            { "Ruto", "Ruto" },
            { "TeciaPacheco", "Tecia Pacheco" },
            { "Vaughn", "Vaughn" },
            { "WallaceKlim", "Wallace Klim" },
            { "CDF", "CDF" }
        };


        public override void CreateRecords(TISContext context, DFDatabase rawGameData, DB.GameDatabase workingDataset)
        {

            /* Import MissionGiverScope entries */

            foreach (var scopeRecord in rawGameData.GetEntries<SReputationScopeParams>())
            {
                var sorted_levels = scopeRecord.StandingLevels.OrderBy(sl => sl.MinReputation).ToList();
                var t0_index = sorted_levels.IndexOf(sorted_levels.Find(l => l.MinReputation >= 0)); //First level with 0 or higher is considered T0
                if (t0_index == -1) t0_index = 0;

                var additionalProperties = new
                {
                    Ranks = sorted_levels.Select((sl, idx) => new DB.MissionScopeRankDTO()
                    {
                        Title = sl.DisplayName,
                        Tier = idx - t0_index,
                        Min = sl.MinReputation,
                        Drift = sl.DriftReputation,
                        DriftHours = sl.DriftHours
                    })
                };

                string iconSVG = null;

                if (!string.IsNullOrWhiteSpace(scopeRecord.IconPath))
                {
                    iconSVG = rawGameData.ReadPackedFile(scopeRecord.IconPath);
                }

                var dbScope = new DB.MissionGiverScope()
                {
                    Key = Utilities.CleanKey(scopeRecord.Name),
                    SCRecordID = scopeRecord.ID,

                    Name = rawGameData.Localize(scopeRecord.DisplayName),
                    Description = scopeRecord.Description,
                    
                    StandingInitial = scopeRecord.StartingValue,
                    StandingMax = scopeRecord.MaxValue,

                    IconSVG = iconSVG,
                    AdditionalProperties = JsonConvert.SerializeObject(additionalProperties)
                };

                workingDataset.MissionGiverScopes.Add(dbScope);
                ImportedScopes.Add(scopeRecord, dbScope);

                Logger?.LogInformation($"Imported mission scope {dbScope.Key}/{dbScope.Name} (Additional Properties: {additionalProperties})");
            }

            //Pre-cache all possible scopes rewarded per mission giver.
            var giverRewardScopes = rawGameData.GetEntries<MissionGiverEntry>()
                .GroupBy(mge => mge.MissionGiverGUID)
                .Where(g => g.Key != Guid.Empty)
                .ToDictionary(
                    g => g.Key, 
                    group => group.SelectMany(
                        mission => mission.RewardSets.SelectMany(
                            reward_set => reward_set.Rewards
                                .Where(reward => reward.Type == DB.RewardType.Standing && reward.RepScope != "affinity")
                                .Select(reward => reward.RepScope))).ToList()
                );

            /* Import MissionGiver entries */

            foreach (var giverRecord in rawGameData.GetEntries<MissionGiver>())
            {
                string mappedName;
                if (GiverNameRemap.ContainsKey(giverRecord.InstanceName))
                    mappedName = GiverNameRemap[giverRecord.InstanceName];
                else
                    mappedName = giverRecord.InstanceName;

                List<string> allScopes = null;
                if (giverRewardScopes.ContainsKey(giverRecord.ID) && giverRewardScopes[giverRecord.ID] != null)
                    allScopes = giverRewardScopes[giverRecord.ID].Distinct().ToList();

                var additionalProperties = new
                {
                    UIScopes = giverRecord.ListedScopes ?? new List<string>(),
                    Scopes = allScopes ?? new List<string>()
                };

                var dbGiver = new DB.MissionGiver()
                {
                    Key = Utilities.CleanKey(giverRecord.InstanceName),
                    Name = giverRecord.Name,
                    Description = giverRecord.Description,
                    SCRecordID = giverRecord.ID,

                    InviteTimeout = giverRecord.InviteTimeout,
                    VisitTimeout = giverRecord.VisitTimeout,
                    ShortCooldown = giverRecord.ShortCooldown,
                    MediumCooldown = giverRecord.MediumCooldown,
                    LongCooldown = giverRecord.LongCooldown,

                    Icon = giverRecord.IconData == null ? null : Convert.ToBase64String(giverRecord.IconData),
                    AdditionalProperties = JsonConvert.SerializeObject(additionalProperties)
                };

                workingDataset.MissionGivers.Add(dbGiver);
                ImportedGivers.Add(giverRecord, dbGiver);

                Logger?.LogInformation($"Imported mission giver {dbGiver.Name}");
            }

            /* Import MissionGiverEntry and requirements */

            foreach (var missionEntryRecord in rawGameData.GetEntries<MissionGiverEntry>())
            {
                var dbMissionEntry = new DB.MissionGiverEntry()
                {
                    Key = Utilities.CleanKey(missionEntryRecord.InstanceName),
                    SCRecordID = missionEntryRecord.ID,
                    SecretMission = missionEntryRecord.SecretMission,
                    Title = missionEntryRecord.FullTitle,
                    Description = missionEntryRecord.FullDescription,
                    MissionType = missionEntryRecord.MissionType,
                    Lawful = missionEntryRecord.LawfulMission,
                    Giver = missionEntryRecord.MissionGiver != null ? ImportedGivers.GetValue(missionEntryRecord.MissionGiver) : null,
                    GiverName = missionEntryRecord.MissionGiverName,
                    AvailableLocation = missionEntryRecord.LocationMissionAvailableGUID != Guid.Empty ?
                        workingDataset.SystemLocations.SingleOrDefault(l => l.StarmapID == missionEntryRecord.LocationMissionAvailableGUID) : null,
                    MinUEC = missionEntryRecord.MinUECReward,
                    MaxUEC = missionEntryRecord.MaxUECReward,
                    PaysBonus = missionEntryRecord.BonusReward
                };

                List<DB.MissionNoteDTO> Notes = new List<DB.MissionNoteDTO>();

                if (missionEntryRecord.NotifyOnAvailable)
                    Notes.Add(new DB.MissionNoteDTO("Notifies when available.", "Note"));

                if (missionEntryRecord.ShowAsOffer)
                    Notes.Add(new DB.MissionNoteDTO("Offered as a mission via your HUD.", "Note"));

                if (missionEntryRecord.MissionBuyInAmount > 0)
                {
                    dbMissionEntry.BuyInUEC = missionEntryRecord.MissionBuyInAmount;

                    Notes.Add(new DB.MissionNoteDTO($"Mission has a buy-in of {dbMissionEntry.BuyInUEC} UEC.", "BuyIn"));

                    if (missionEntryRecord.RefundBuyInOnWithdraw)
                    {
                        Notes.Add(new DB.MissionNoteDTO($"Buy-in is non-refundable!", "Warning"));
                    }
                }

                if (missionEntryRecord.MaxInstances > 1)
                    Notes.Add(new DB.MissionNoteDTO($"Up to {missionEntryRecord.MaxInstances} instances of this mission can exist simultaneously.", "Note"));


                if(missionEntryRecord.MaxPlayersPerInstance > 1)
                    Notes.Add(new DB.MissionNoteDTO($"This mission can be split between {missionEntryRecord.MaxPlayersPerInstance} players.", "Note"));
                
                if (missionEntryRecord.MaxInstancesPerPlayer > 1)
                    Notes.Add(new DB.MissionNoteDTO($"You can personally accept up to {missionEntryRecord.MaxInstances} instances of this mission simultaneously.", "Note"));

                if (!missionEntryRecord.CanBeShared)
                    Notes.Add(new DB.MissionNoteDTO("This mission can not be shared with allies!", "Warning"));

                if (missionEntryRecord.OnceOnly)
                    Notes.Add(new DB.MissionNoteDTO("You can only accept this mission once!", "Warning"));

                if (missionEntryRecord.GrantsArrestLicense)
                    Notes.Add(new DB.MissionNoteDTO("This mission grants you an arrest license.", "ArrestLicense"));

                if (missionEntryRecord.AvailableInPrison)
                    Notes.Add(new DB.MissionNoteDTO("This mission is available while in prison.", "Note"));
                else
                    Notes.Add(new DB.MissionNoteDTO("This mission is not available while in prison.", "Warning"));

                if (missionEntryRecord.FailIfSentToPrison)
                    Notes.Add(new DB.MissionNoteDTO("You fail this mission if sent to prison.", "Warning"));

                if(missionEntryRecord.RequestOnly)
                    Notes.Add(new DB.MissionNoteDTO("Mission is by request only.", "Warning"));

                if (missionEntryRecord.RespawnTime > 0)
                {
                    if (missionEntryRecord.RespawnTimeVariation == 0)
                        Notes.Add(new DB.MissionNoteDTO($"Mission respawns every {missionEntryRecord.RespawnTime} seconds.", "Cooldown"));
                    else
                        Notes.Add(new DB.MissionNoteDTO($"Mission respawns every {missionEntryRecord.RespawnTime} +/- {missionEntryRecord.RespawnTimeVariation} seconds.", "Cooldown"));
                }

                if (missionEntryRecord.InstanceHasLifeTime)
                {
                    if(missionEntryRecord.InstanceLifeTimeVariation == 0)
                        Notes.Add(new DB.MissionNoteDTO($"Mission instance has a timer of {missionEntryRecord.InstanceLifeTime} seconds.", "Cooldown"));
                    else
                        Notes.Add(new DB.MissionNoteDTO($"Mission instance has a timer of {missionEntryRecord.InstanceLifeTime} +/- {missionEntryRecord.InstanceLifeTimeVariation} seconds.", "Cooldown"));

                    if (!missionEntryRecord.ShowLifeTimeInMobiGlas)
                    {
                        Notes.Add(new DB.MissionNoteDTO($"There is no indicator for the remaining time in mobiglass for this mission!", "Warning"));
                    }
                }

                if (missionEntryRecord.CanReacceptAfterAbandoning)
                {
                    if(missionEntryRecord.AbandonedCooldownTimeVariation == 0)
                        Notes.Add(new DB.MissionNoteDTO($"Mission will come back after {missionEntryRecord.AbandonedCooldownTime} seconds if abandoned.", "Cooldown"));
                    else
                        Notes.Add(new DB.MissionNoteDTO($"Mission will come back after {missionEntryRecord.AbandonedCooldownTime} +/- {missionEntryRecord.AbandonedCooldownTimeVariation} seconds if abandoned.", "Cooldown"));
                }
                else
                    Notes.Add(new DB.MissionNoteDTO($"Mission can not be reaccepted after being abandoned!", "Warning"));


                if (missionEntryRecord.HasPersonalCooldown)
                {
                    if(missionEntryRecord.PersonalCooldownTimeVariation == 0)
                        Notes.Add(new DB.MissionNoteDTO($"Mission has a personal cooldown of {missionEntryRecord.PersonalCooldownTime} seconds.", "Cooldown"));
                    else
                        Notes.Add(new DB.MissionNoteDTO($"Mission has a personal cooldown of {missionEntryRecord.PersonalCooldownTime} +/- {missionEntryRecord.PersonalCooldownTimeVariation} seconds.", "Cooldown"));
                }

                if(!missionEntryRecord.LawfulMission)
                    Notes.Add(new DB.MissionNoteDTO($"Mission is unlawful!", "Illegal"));


                dbMissionEntry.Requirements = JsonConvert.SerializeObject(
                    missionEntryRecord.Requirements.Select(mer =>
                    {
                        return new DB.MissionRequirementDTO()
                        {
                            Type = mer.Type,
                            Title = mer.Title,
                            Min = mer.Min,
                            Max = mer.Max,
                            MissionKeys = mer?.Missions?.Select(m => Utilities.CleanKey(m.InstanceName))?.ToArray(),
                            TargetGiverKey = ImportedGivers.GetValue(mer?.TargetGiver)?.Key,
                            RepScope = Utilities.CleanKey(mer.RepScope)
                        };
                    }));

                dbMissionEntry.RewardSets = JsonConvert.SerializeObject(
                    missionEntryRecord.RewardSets.Select(rew =>
                    {
                        return new DB.MissionRewardSetDTO()
                        {
                            Title = rew.Title,
                            Rewards = rew.Rewards.Select(r =>
                            {
                                return new DB.MissionRewardDTO()
                                {
                                    Type = r.Type,
                                    Amount = r.Amount,
                                    //Web client will do this lookup for convenience
                                    TargetGiverKey = ImportedGivers.GetValue(r.TargetEntity)?.Key,
                                    RepScope = Utilities.CleanKey(r.RepScope)
                                };
                            }).ToArray()
                        };
                    }));

                dbMissionEntry.Notes = JsonConvert.SerializeObject(Notes);
                

                workingDataset.Missions.Add(dbMissionEntry);
                ImportedMissions.Add(missionEntryRecord, dbMissionEntry);

                Logger?.LogInformation($"Imported mission giver entry {dbMissionEntry.Title}");
            }
        }
    }
}
