﻿using TradeInSpace.Explore;
using TradeInSpace.Explore.Socpak;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TradeInSpace.Data;
using TradeInSpace.Models;
using TradeInSpace.Explore.DataForge;

namespace DataImport.Importers
{
    class EntityWalker : DataImporter
    {
        public EntityWalker(ILoggerFactory factory = null)
            : base(factory?.CreateLogger("Entity Walker"))
        {
        }

        public EntityWalker(bool allEntities)
        {
            AllEntities = allEntities;
        }

        bool AllEntities { get; set; }

        public override void CreateRecords(TISContext context, DFDatabase rawGameData, GameDatabase workingDataset)
        {
            var stantonSock = SOC_Archive.LoadFromGameDBSocPak(rawGameData, "Data/ObjectContainers/PU/system/stanton/stantonsystem.socpak");

            List<SOC_Archive> NextCheck = new List<SOC_Archive>();
            NextCheck.Add(stantonSock);
            
            StringBuilder build = new StringBuilder();
            foreach (var nestedContainer in stantonSock.ExploreRecursive())
            {
                var containerDepth = nestedContainer.Item1;
                var entry = nestedContainer.Item2;
                var indent = "".PadLeft(containerDepth, '\t');

                Logger?.LogDebug($"{indent}{entry.Name} - {entry.Label}[{entry.Class}] ({entry.Pos})");

                if (AllEntities)
                {
                    entry.LoadAllEntities();
                }

                foreach (var expEnt in entry.Entities.Cast<ExposedEntity>())
                {
                    switch(expEnt)
                    {
                        case FullEntity fe:
                            Logger?.LogDebug($"{indent}\tFE:  {fe.Label}[{string.Join(",", fe.Components.Select(c => c.ComponentType))}]");

                            var comment = fe.Components.SingleOrDefault(c => c.ComponentType == "EntityComponentComment");
                            if(comment != null) Logger?.LogDebug($"{indent}\tCOMMENT: {comment.FromElement.GetAttribute("text")}");
                            break;
                        case ExposedEntity ee:
                            Logger?.LogDebug($"{indent}\tEE:  {ee.Label}[{ee.Tags}]");
                            break;
                    }
                }
                
            }
            var outp = build.ToString();
            
        }
    }
}
