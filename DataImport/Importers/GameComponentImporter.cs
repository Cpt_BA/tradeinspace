﻿using DataImport.GameModels;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TradeInSpace.Data;
using TradeInSpace.Explore.DataForge;
using TradeInSpace.Explore.SC.Components;
using TradeInSpace.Explore.Socpak;
using TradeInSpace.Models;

namespace DataImport.Importers
{
    public class GameComponentImporter : DataImporter
    {
        ParserResponseCollection<SOC_Entry, GameComponent> Components
            = new ParserResponseCollection<SOC_Entry, GameComponent>();

        public GameComponentImporter(ILoggerFactory logger = null)
            : base(logger?.CreateLogger("Game Components"))
        {

        }

        public override void CreateRecords(TISContext context, DFDatabase rawGameData, GameDatabase workingDataset)
        {
            var attachables = rawGameData.GetEntitiesWithComponents<AttachableComponent>();

            foreach(var attachableItem in attachables)
            {
                
            }
        }
    }
}
