﻿using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TradeInSpace.Explore;
using TradeInSpace.Explore.DataForge;
using TradeInSpace.Explore.Socpak;
using TradeInSpace.Models;

namespace DataImport.Importers.SLPostProcess
{
    public class RespawnPointPostProcessor : SystemLocationPostProcessor
    {
        public RespawnPointPostProcessor()
        {
        }


        private bool FoundRespawnPoint = false;
        internal override void StartLocation(SystemLocation record, SOC_Entry entry)
        {
            FoundRespawnPoint = false;
        }

        internal override void CompleteLocation(SystemLocation record, SOC_Entry entry)
        {
            if (FoundRespawnPoint)
            {
                record.ServiceFlags |= Enums.ServiceFlags.FPSRespawnPoint;
                Log.Information($"Location {record.Name} has Respawn point.");
            }

            FoundRespawnPoint = false;
        }

        internal override void ProcessChildSOC(SystemLocation record, SOC_Entry entry, SOC_Entry exploringSOC)
        {
        }

        internal override void ProcessEntity(SystemLocation record, SOC_Entry entry, FullEntity entityChild)
        {
            if (entityChild.DatabaseEntity != null)
            {
                if (entityChild.DatabaseEntity.Components.Any(c => c.Type == "SpawnPointComponentParams"))
                {
                    FoundRespawnPoint = true;
                }
            }
        }
    }
}
