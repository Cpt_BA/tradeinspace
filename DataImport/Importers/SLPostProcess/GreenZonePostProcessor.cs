﻿using Microsoft.Extensions.Logging;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TradeInSpace.Data;
using TradeInSpace.Explore;
using TradeInSpace.Explore.DataForge;
using TradeInSpace.Explore.Socpak;
using TradeInSpace.Explore.Socpak.Components;
using TradeInSpace.Models;

namespace DataImport.Importers.SLPostProcess
{
    public class GreenZonePostProcessor : SystemLocationPostProcessor
    {
        public GreenZonePostProcessor()
        {
        }


        private bool FoundGreenZone = false;
        internal override void StartLocation(SystemLocation record, SOC_Entry entry)
        {
            FoundGreenZone = false;
        }

        internal override void CompleteLocation(SystemLocation record, SOC_Entry entry)
        {
            if (FoundGreenZone)
            {
                record.ServiceFlags |= Enums.ServiceFlags.GreenZone;
                Log.Information($"Location {record.Name} has Green Zone.");
            }

            FoundGreenZone = false;
        }

        internal override void ProcessChildSOC(SystemLocation record, SOC_Entry entry, SOC_Entry exploringSOC)
        {
        }

        internal override void ProcessEntity(SystemLocation record, SOC_Entry entry, FullEntity entityChild)
        {
            var greenComponent = entityChild.GetComponent<SOC_GreenZone>();

            if (greenComponent != null)
            {
                FoundGreenZone = true;
            }
        }
    }
}
