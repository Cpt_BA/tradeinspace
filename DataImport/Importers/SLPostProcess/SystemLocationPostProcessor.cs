﻿using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TradeInSpace.Data;
using TradeInSpace.Explore;
using TradeInSpace.Explore.DataForge;
using TradeInSpace.Explore.Socpak;
using TradeInSpace.Models;

namespace DataImport.Importers
{
    public abstract class SystemLocationPostProcessor
    {
        internal DFDatabase RawGameData { get; private set; }
        internal GameDatabase WorkingDataset { get; private set; }

        public SystemLocationPostProcessor()
        {
        }

        public virtual void Initialize(DFDatabase RawGameData, GameDatabase WorkingDataset)
        {
            this.RawGameData = RawGameData;
            this.WorkingDataset = WorkingDataset;
        }

        internal abstract void StartLocation(SystemLocation record, SOC_Entry entry);
        internal abstract void CompleteLocation(SystemLocation record, SOC_Entry entry);
        internal abstract void ProcessChildSOC(SystemLocation record, SOC_Entry entry, SOC_Entry exploringSOC);
        internal abstract void ProcessEntity(SystemLocation record, SOC_Entry entry, FullEntity entityChild);
    }
}
