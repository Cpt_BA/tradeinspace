﻿using DataImport.Utils;
using Newtonsoft.Json;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TradeInSpace.Explore;
using TradeInSpace.Explore.DataForge;
using TradeInSpace.Explore.SC.Components;
using TradeInSpace.Explore.SC.Data;
using TradeInSpace.Explore.Socpak;
using TradeInSpace.Explore.Socpak.Components;
using TradeInSpace.Models;

namespace DataImport.Importers.SLPostProcess
{
    public class HarvestableProviderPostProcessor : SystemLocationPostProcessor
    {
        public HarvestableProviderPostProcessor(HarvestableImporter harvestableImporter)
        {
            HarvestableImporter = harvestableImporter;
        }

        internal override void ProcessChildSOC(SystemLocation record, SOC_Entry entry, SOC_Entry exploringSOC)
        {
        }

        List<(SOC_Entry TrueParent, HarvestablePresetEntry Harvestable)> Harvestables = new List<(SOC_Entry, HarvestablePresetEntry)>();

        public HarvestableImporter HarvestableImporter { get; }

        internal override void StartLocation(SystemLocation record, SOC_Entry entry)
        {
            Harvestables.Clear();
        }
        internal override void ProcessEntity(SystemLocation record, SOC_Entry entry, FullEntity entityChild)
        {
            var harvestableComponent = entityChild.GetComponent<SOC_HarvestableProvider>();
            
            if (harvestableComponent?.HarvestingPreset?.Harvestables is List<HarvestablePresetEntry> presets)
            {
                var trueParent = entry;
                if(entityChild.Owner != entry)
                {
                    var testParent = entityChild.Owner;
                    while (testParent != null && !testParent.StarmapRecord.HasValue)
                    {
                        if (testParent.Parent == entry) break;

                        testParent = testParent.Parent;
                    }
                    trueParent = testParent;
                }

                if (trueParent == null) throw new Exception("Somehow [entityChild] not a child of [entry]");

                Harvestables.AddRange(presets.Select(hp => (trueParent, hp)));
            }
        }
        internal override void CompleteLocation(SystemLocation record, SOC_Entry entry)
        {
            if(Harvestables.Count > 0)
            {
                foreach (var harvestableDetails in Harvestables.GroupBy(h => h.TrueParent, h => h.Harvestable))
                {
                    entry.Parent.CalculateGlobalPositions();
                    entry.CalculateGlobalPositions();

                    var groupParent = harvestableDetails.Key;
                    //Use group's parent if it is valid, otherwise use the noraml starmap ID
                    var starmapLocation = groupParent.StarmapRecord ?? record.StarmapID;
                    var starmapEntry = RawGameData.GetEntry<StarmapObject>(starmapLocation);

                    var harvestableUnder = record;

                    if(groupParent.GUID != record.SCRecordID)
                    {
                        var additional = CoordinateHelper.BuildLocationAdditionalProperties(entry.Transform, entry.Parent.GlobalPosition);

                        //IF the group of harvestables belongs to a different SystemLocation, we should make one
                        //This happens with mining sites, and they should probably be created sooner...
                        harvestableUnder = new SystemLocation()
                        {
                            SCRecordID = groupParent.GUID,
                            ParentLocation = record,
                            ParentBody = record.ParentBody,
                            StarmapID = groupParent.StarmapRecord,
                            Key = Utilities.CleanKey(groupParent.Name),
                            Name = starmapEntry.Name,
                            Description = starmapEntry.Description,
                            LocationType = Enums.LocationType.PointOfInterest,// Enum.Parse<Enums.LocationType>(starmapEntry.ObjectType.Name.Replace(" ", ""), true),
                            IsHidden = starmapEntry.HiddenInStarmap,
                            QTDistance = starmapEntry.HasQuantum ? (int?)starmapEntry.ArrivalRadius : null,
                            ChildLocations = new List<SystemLocation>(),
                            AdditionalProperties = JsonConvert.SerializeObject(additional)
                        };
                        this.WorkingDataset.SystemLocations.Add(harvestableUnder);
                    }

                    //Group by preset to prevent duplicates of the same location/resource from showing up.
                    foreach(var harvestable in harvestableDetails.GroupBy(h => h.Preset))
                    {
                        var newLocation = new HarvestableLocation()
                        {
                            Harvestable = HarvestableImporter.ImportedHarvestables.GetValue(harvestable.Key),
                            ParentLocation = harvestableUnder,
                            Key = Utilities.CleanKey(starmapEntry.Name),
                            SCRecordID = record.SCRecordID,
                            Name = starmapEntry.Name
                        };

                        WorkingDataset.HarvestableLocations.Add(newLocation);
                    }
                }

                record.ServiceFlags |= Enums.ServiceFlags.Harvestable;
            }
        }
    }
}
