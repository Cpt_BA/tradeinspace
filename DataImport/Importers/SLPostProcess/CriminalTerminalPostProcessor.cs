﻿using Microsoft.Extensions.Logging;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TradeInSpace.Data;
using TradeInSpace.Explore;
using TradeInSpace.Explore.DataForge;
using TradeInSpace.Explore.SC.Components;
using TradeInSpace.Explore.Socpak;
using TradeInSpace.Models;

namespace DataImport.Importers.SLPostProcess
{
    public class CriminalTerminalPostProcessor : SystemLocationPostProcessor
    {
        public CriminalTerminalPostProcessor()
        {
        }


        private bool FoundFineTerminal = false;
        private bool FoundHackingTerminal = false;

        internal override void StartLocation(SystemLocation record, SOC_Entry entry)
        {
            FoundFineTerminal = false;
            FoundHackingTerminal = false;
        }

        internal override void CompleteLocation(SystemLocation record, SOC_Entry entry)
        {
            if (FoundFineTerminal)
            {
                record.ServiceFlags |= Enums.ServiceFlags.Fine;
                Log.Information($"Location {record.Name} has Fine payment terminal.");
            }

            if (FoundHackingTerminal)
            {
                record.ServiceFlags |= Enums.ServiceFlags.Hacking;
                Log.Information($"Location {record.Name} has hacking terminal.");
            }

            FoundFineTerminal = false;
            FoundHackingTerminal = false;
        }

        internal override void ProcessChildSOC(SystemLocation record, SOC_Entry entry, SOC_Entry exploringSOC)
        {
        }

        internal override void ProcessEntity(SystemLocation record, SOC_Entry entry, FullEntity entityChild)
        {
            if (entityChild.DatabaseEntity != null)
            {
                var criminalUI = entityChild.DatabaseEntity.GetComponent<CriminalRecordUIComponent>();
                if(criminalUI != null)
                {
                    if(!criminalUI.ShowFelonies && criminalUI.ShowMisdemeanors)
                    {
                        FoundFineTerminal = true;
                    }
                    else if(criminalUI.ShowFelonies)
                    {
                        FoundHackingTerminal = true;
                    }
                }
            }
        }
    }
}
