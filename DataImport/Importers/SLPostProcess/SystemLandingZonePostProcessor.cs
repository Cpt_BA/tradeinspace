﻿using Microsoft.Extensions.Logging;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using TradeInSpace.Data;
using TradeInSpace.Explore;
using TradeInSpace.Explore.DataForge;
using TradeInSpace.Explore.Socpak;
using TradeInSpace.Explore.Socpak.Components;
using TradeInSpace.Models;
using static TradeInSpace.Models.Enums;

namespace DataImport.Importers.SLPostProcess
{
    public class SystemLandingZonePostProcessor : SystemLocationPostProcessor
    {
        public SystemLandingZonePostProcessor()
        {
        }



        List<LandingAreaComponent> LocationPads = new List<LandingAreaComponent>();
        List<Guid> PadGUIDs = new List<Guid>();
        bool HasShipTerminals = false;

        internal override void StartLocation(SystemLocation record, SOC_Entry entry)
        {
            LocationPads.Clear();
            PadGUIDs.Clear();
            HasShipTerminals = false;
        }
        internal override void CompleteLocation(SystemLocation record, SOC_Entry entry)
        {
            var validPads = LocationPads.Where(p => p != null && 
                                    p.Enabled && p.ShipSize.HasValue && p.AllowShip &&
                                    (p.UsedBy == "Player" || p.UsedBy == "All")).ToList();

            if (validPads.Count > 0)
            {
                var padData = new SystemLandingZone()
                {
                    Name = entry.Name,
                    Description = entry.Label,
                    Key = record.Key,
                    Location = record,

                    TPads = validPads.Count(p => p.ShipSize == PadSize.Tiny),
                    XSPads = validPads.Count(p => p.ShipSize == PadSize.XSmall),
                    SPads = validPads.Count(p => p.ShipSize == PadSize.Small),
                    MPads = validPads.Count(p => p.ShipSize == PadSize.Medium),
                    LPads = validPads.Count(p => p.ShipSize == PadSize.Large),
                    XLPads = validPads.Count(p => p.ShipSize == PadSize.XLarge)
                };

                padData.Location.ServiceFlags |= ServiceFlags.ShipLanding;
                WorkingDataset.LandingZones.Add(padData);
            }

            if (HasShipTerminals)
            {
                record.ServiceFlags |= ServiceFlags.ShipClaim;
            }
        }

        internal override void ProcessChildSOC(SystemLocation record, SOC_Entry entry, SOC_Entry exploringSOC)
        {
        }

        internal override void ProcessEntity(SystemLocation record, SOC_Entry entry, FullEntity entityChild)
        {
            var landingAreaComponent = entityChild.GetComponent<LandingAreaComponent>();

            if (landingAreaComponent != null)
            {
                LocationPads.Add(landingAreaComponent);
            }

            var shipInsuranceComponent = entityChild.Element.SelectSingleNode("./PropertiesDataCore/EntityComponentShipInsuranceProvider") as XmlElement;

            if(shipInsuranceComponent != null)
            {
                HasShipTerminals = true;
            }

            if (entityChild.Class == "SCShop")
            {
                var newPadGUIDs = entityChild.Element.SelectNodes("./EntityComponentShop/LandingAreaNodeGUIDs/LandingAreaNodeGUID/@entityCryGUID").OfType<XmlAttribute>().Select(a => Guid.Parse(a.Value)).ToList();

                if (newPadGUIDs.Count > 0)
                {
                    if (PadGUIDs.Count != 0) Log.Warning($"There are already landing pad GUIDs defined for parent entity {record.Name} when adding more pads for entity {entityChild.Label}");
                    PadGUIDs.AddRange(newPadGUIDs);
                }
            }
        }
    }
}
