﻿using DataImport.GameModels;
using DataImport.Utils;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Reflection.Metadata.Ecma335;
using System.Text;
using TradeInSpace.Explore;
using TradeInSpace.Explore.DataForge;
using TradeInSpace.Explore.SC.Components;
using TradeInSpace.Explore.SC.Data;
using TradeInSpace.Explore.Socpak;
using TradeInSpace.Explore.Socpak.Components;
using TradeInSpace.Explore.Subsumption;
using TradeInSpace.Models;
using static TradeInSpace.Models.Enums;

namespace DataImport.Importers.SLPostProcess
{
    public class TradeShopPostProcessor : SystemLocationPostProcessor
    {
        public Dictionary<FullEntity, SystemLocation> ImportedLocations = new Dictionary<FullEntity, SystemLocation>();
        public List<(FullEntity entity, TradeShop shop)> ImportedShops = new List<(FullEntity entity, TradeShop shop)>();

        public SubsumptionManager SubsumptionManager { get; }

        public TradeShopPostProcessor(SubsumptionManager subsumptionManager)
        {
            SubsumptionManager = subsumptionManager;
        }

        public override void Initialize(DFDatabase RawGameData, GameDatabase WorkingDataset)
        {
            base.Initialize(RawGameData, WorkingDataset);

        }

        internal override void StartLocation(SystemLocation record, SOC_Entry entry)
        {
        }
        internal override void CompleteLocation(SystemLocation record, SOC_Entry entry)
        {
        }

        internal override void ProcessChildSOC(SystemLocation record, SOC_Entry entry, SOC_Entry exploringSOC)
        {
        }

        internal override void ProcessEntity(SystemLocation location, SOC_Entry entry, FullEntity entityChild)
        {

            var shopComponent = entityChild.GetComponent<SOC_ComponentShop>();
            
            if (shopComponent != null)
            {
                Log.Debug($"Shop Location: {entry.Name} - {entry.Label} / {entry.GUID}");
                Log.Debug($"Shop Entity: {entityChild.Label} - {entityChild.Layer} / {entityChild.GUID}");

                foreach (var kiosk in shopComponent.Kiosks)
                {
                    Log.Debug($"\tKiosk: [{kiosk.GUID}] {kiosk.Label} - {kiosk["Layer"]}");
                }

                if (!entry.StarmapRecord.HasValue)
                {
                    Log.Warning($"Shop {entry.Name} has no starmap record ID defined for it");
                    return;
                }


                var shopType = Enums.ShopType.Ignore; //shopComponent.ShopType;

                //First try for an exact match by our (sometimes imperfect) SuperGUID, then try a more systematic approach second
                var shopLayoutMapping = 
                    SubsumptionManager.LookupShopProfileExact(entityChild.SuperGUID) ?? 
                    SubsumptionManager.LookupShopProfile(entry, entityChild);
                
                if (shopLayoutMapping != null)
                {
                    var shopInfo = SubsumptionManager.GetShopLayout(shopLayoutMapping.LayoutGUID);

                    if (shopInfo.ShopType == ShopType.Ignore)
                    {
                        Log.Warning($"Skipping ignored shop {shopInfo.Shop.Name}");

                        return;
                    }

                    shopType = shopInfo.ShopType;
                }
                else
                {
                    Log.Error($"Unable to locate Shop Profile for shop {entityChild.Label}/{entityChild.SuperGUID}. Attempting fallback search");
                }
                
                if (shopType == Enums.ShopType.Ignore)
                {
                    Log.Warning($"Skipping ignored location - {entityChild.Label}");

                    return;
                }

                var fullName = $"{location.Name} - {shopType}";
                var existingShop = WorkingDataset.TradeShops.FirstOrDefault(l => l.Key == Utilities.CleanKey(fullName));

                if (existingShop != null)
                {
                    Log.Information($"Merging entity [{existingShop.Name}] for same shop: {fullName}");
                    ImportedShops.Add((entityChild, existingShop));
                }
                else
                {
                    var starmapEntry = location.StarmapID.HasValue ? RawGameData.GetEntry<StarmapObject>(location.StarmapID) : null;

                    entry.CalculateGlobalPositions();
                    var locationAdditional = CoordinateHelper.BuildLocationAdditionalProperties(entityChild.Transform, entry.GlobalPosition);

                    var store_location = new SystemLocation()
                    {
                        SCRecordID = entityChild.GUID,
                        ParentLocation = location,
                        ParentBody = location.ParentBody,
                        Key = Utilities.CleanKey(fullName),
                        Name = fullName,
                        ServiceFlags = Enums.ServiceFlags.None,
                        LocationType = Enums.LocationType.Store,
                        IsHidden = location.IsHidden,
                        AdditionalProperties = JsonConvert.SerializeObject(locationAdditional)
                    };
                    WorkingDataset.SystemLocations.Add(store_location);

                    ImportedLocations.Add(entityChild, store_location);

                    var tradeStation = new TradeShop()
                    {
                        SCRecordID = entityChild.GUID,
                        Key = Utilities.CleanKey(fullName),
                        Name = fullName,
                        Description = entityChild.Label,
                        ShopType = shopType,
                        Location = store_location,
                        //Say user flies 60km when no starmap destination.
                        //Should probably be handled in the web UI itself...
                        ClosestApproachKM = (starmapEntry?.ArrivalRadius ?? 60000) / 1000
                    };
                    WorkingDataset.TradeShops.Add(tradeStation);

                    ImportedShops.Add((entityChild, tradeStation));

                    Log.Information($"Created Trade Shop {fullName}");
                }
            }
        }
    }
}
