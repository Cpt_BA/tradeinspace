﻿using DataImport.GameModels;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TradeInSpace.Data;
using TradeInSpace.Explore.DataForge;
using TradeInSpace.Explore.Socpak;
using TradeInSpace.Models;

namespace DataImport.Importers.SLPostProcess
{
    public class SubsumptionPostProcessImporter : DataImporter
    {
        public SubsumptionPostProcessImporter(ILoggerFactory logFactory)
            : base(logFactory.CreateLogger("Subsumption Location Post-Processing"))
        {
        }

        private List<SystemLocationPostProcessor> Processors = new List<SystemLocationPostProcessor>();
        public SystemBodyImporter SystemBodyImporter { get; }
        public SystemLocationImporter SystemLocationImporter { get; }

        public void AddProcessor(SystemLocationPostProcessor postProcessor)
        {
            Processors.Add(postProcessor);
        }

        public void ClearProcessors()
        {
            Processors.Clear();
        }

        public override void CreateRecords(TISContext context, DFDatabase rawGameData, GameDatabase workingDataset)
        {
            var exploredGUIDs = workingDataset.SystemLocations.Select(l => l.SCRecordID);

            foreach (var processor in Processors)
            {
                processor.Initialize(rawGameData, workingDataset);
            }

            Logger?.LogInformation($"Starting {Processors.Count} SystemLocation Post-Processors");

            var allExplore = new ParserResponseCollection<SOC_Entry, SystemLocation>();

            int Count = 0;
            int Total = allExplore.Count;
            foreach (var importedLocation in allExplore)
            {
                var locationEntry = importedLocation.Entry;

                //Need to be thourough
                locationEntry.LoadAllEntities(false);

                Logger?.LogInformation($"");
                Logger?.LogInformation($"[{Count}/{Total}] Exploring location - {locationEntry.Name}");
                Logger?.LogDebug($"Full: [{locationEntry.GUID}] {locationEntry.Label} ({locationEntry.Pos})");

                foreach (var processor in Processors)
                {
                    processor.StartLocation(importedLocation.Record, importedLocation.Entry);
                }

                foreach (var nestedChild in importedLocation.Entry.ExploreRecursive(ShouldExplore: (e) => !exploredGUIDs.Contains(e.Container.GUID)))
                {
                    var exploringSOC = nestedChild.Item2;

                    exploringSOC.LoadAllEntities(false);

                    foreach (var processor in Processors)
                    {
                        processor.ProcessChildSOC(importedLocation.Record, importedLocation.Entry, exploringSOC);

                        foreach (var entityChild in exploringSOC.Entities.OfType<FullEntity>())
                        {
                            processor.ProcessEntity(importedLocation.Record, importedLocation.Entry, entityChild);
                        }
                    }
                }

                foreach (var processor in Processors)
                {
                    processor.CompleteLocation(importedLocation.Record, importedLocation.Entry);
                }

                Count++;
            }
        }
    }
}
