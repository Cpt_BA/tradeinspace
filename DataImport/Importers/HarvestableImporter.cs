﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TradeInSpace.Data;
using TradeInSpace.Explore;
using TradeInSpace.Explore.DataForge;
using SC = TradeInSpace.Explore.SC.Data;
using DB = TradeInSpace.Models;
using DataImport.GameModels;
using TradeInSpace.Models;
using TradeInSpace.Explore.SC.Data;
using TradeInSpace.Explore.SC.Components;

namespace DataImport.Importers
{
    public class HarvestableImporter : DataImporter
    {
        public HarvestableImporter(MiningElementImporter elementImporter, MiningCompositionImporter compositionImporter, ILoggerFactory factory = null)
            : base(factory?.CreateLogger("Harvestable Importer"))
        {
            ElementImporter = elementImporter;
            CompositionImporter = compositionImporter;
        }

        public MiningElementImporter ElementImporter { get; }
        public MiningCompositionImporter CompositionImporter { get; }

        public ParserResponseCollection<SC.HarvestablePreset, DB.Harvestable> ImportedHarvestables = new ParserResponseCollection<SC.HarvestablePreset, DB.Harvestable>();

        public override void CreateRecords(TISContext context, DFDatabase rawGameData, DB.GameDatabase workingDataset)
        {
            Dictionary<SC.HarvestablePreset, DB.Harvestable> presetMap = new Dictionary<SC.HarvestablePreset, DB.Harvestable>();

            foreach (var harvestablePreset in rawGameData.GetEntries<SC.HarvestablePreset>())
            {
                var commodityComponent = harvestablePreset.HarvestableEntity.GetComponent<SCCommodity>();
                var harvestableComposition = harvestablePreset.HarvestableEntity.GetComponent<MineableComponent>();

                var dbElement = commodityComponent != null ? ElementImporter.ImportedElements.SingleOrDefault(ie => ie.Entry?.Resource == commodityComponent?.Commodity)?.Record : null;
                var dbComposition = CompositionImporter.ImportedCompositions.GetValue(harvestableComposition?.Composition);

                var harvestable = new DB.Harvestable()
                {
                    GameDB = workingDataset,
                    Key = Utilities.CleanKey(harvestablePreset.InstanceName),
                    Element = dbElement,
                    ElementUnits = commodityComponent?.uSCUSize ?? 0,
                    Composition = dbComposition,
                    SCRecordID = harvestablePreset.ID,
                    MinElevation = harvestablePreset.MinElevation,
                    MaxElevation = harvestablePreset.MaxElevation,
                    RespawnTime = harvestablePreset.RespawnTime,
                    SubHarvestSlots = harvestablePreset.SubHarvestableSlots,
                    SubHarvestableSlotChance = harvestablePreset.SubHarvestableChance,
                    SubHarvestables = new List<DB.SubHarvestable>()
                };

                workingDataset.Harvestables.Add(harvestable);
                ImportedHarvestables.Add(harvestablePreset, harvestable);

                Logger?.LogInformation($"Imported Harvestable Preset {harvestablePreset.InstanceName}");
            }

            foreach (var gameHarvestable in rawGameData.GetEntries<SC.HarvestablePreset>())
            {
                var dbParentHarvest = ImportedHarvestables.GetValue(gameHarvestable);

                foreach(var gameSubHarvestable in gameHarvestable.SubHarvestables)
                {
                    var dbChildHarvest = ImportedHarvestables.GetValue(gameSubHarvestable.Preset);

                    var subHarvestable = new DB.SubHarvestable()
                    {
                        GameDB = workingDataset,
                        Key = Utilities.CleanKey($"{gameHarvestable.InstanceName}_{gameSubHarvestable.Preset.InstanceName}"),
                        SCRecordID = gameSubHarvestable.Preset.ID,
                        ParentHarvestable = dbParentHarvest,
                        ChildHarvestable = dbChildHarvest,
                        RelativeProbability = gameSubHarvestable.Probability,
                        RespawnMultiplier = gameSubHarvestable.RespawnMultiplier
                    };

                    //dbParentHarvest.SubHarvestables.Add(subHarvestable);
                    workingDataset.SubHarvestables.Add(subHarvestable);

                    Logger?.LogInformation($"Imported SubHarvestable Preset {gameHarvestable.InstanceName} -> {gameSubHarvestable.Preset.InstanceName}");
                }
            }
        }
    }
}
