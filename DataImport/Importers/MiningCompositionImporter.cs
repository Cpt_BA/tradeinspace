﻿using DataImport.GameModels;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TradeInSpace.Data;
using TradeInSpace.Explore;
using TradeInSpace.Explore.DataForge;
using TradeInSpace.Explore.SC.Data;
using TradeInSpace.Models;

namespace DataImport.Importers
{
    public class MiningCompositionImporter : DataImporter
    {
        public MiningCompositionImporter(MiningElementImporter elementImporter, ILoggerFactory factory = null)
            : base(factory?.CreateLogger("Mining Composition Importer"))
        {
            ElementImporter = elementImporter;
        }

        public ParserResponseCollection<MineableComposition, MiningComposition> ImportedCompositions = new ParserResponseCollection<MineableComposition, MiningComposition>();

        public MiningElementImporter ElementImporter { get; }

        public override void CreateRecords(TISContext context, DFDatabase rawGameData, GameDatabase workingDataset)
        {
            foreach (var compositionRecord in rawGameData.GetEntries<MineableComposition>())
            {
                var entries = compositionRecord.Entries
                    .Where(compEntry => ElementImporter.ImportedElements.HasKey(compEntry.Element))
                    .Select(compEntry =>
                {
                    return new MiningCompositionEntry()
                    {
                        SCRecordID = compEntry.ElementID,
                        Key = Utilities.CleanKey($"{compositionRecord.InstanceName}_{compEntry.Element.InstanceName}"),
                        Element = ElementImporter.ImportedElements.GetValue(compEntry.Element),
                        Probability = compEntry.Probability,
                        MinPercentage = compEntry.MinPercentage,
                        MaxPercentage = compEntry.MaxPercentage,
                        CurveExponent = compEntry.CurveExponent
                    };
                }).ToList();

                var mineComp = new MiningComposition()
                {
                    SCRecordID = compositionRecord.ID,
                    Key = Utilities.CleanKey(compositionRecord.InstanceName),
                    Entries = entries.ToList(),
                    Name = compositionRecord.InstanceName,
                    MinimumDistinct = compositionRecord.MinimumDistinctElements
                };
                 
                workingDataset.MiningCompositions.Add(mineComp);
                entries.ForEach(entry => workingDataset.MiningCompositionEntries.Add(entry));

                ImportedCompositions.Add(compositionRecord, mineComp);

                Logger?.LogInformation($"Imported mining composition {compositionRecord.InstanceName}");
            }
        }
    }
}
