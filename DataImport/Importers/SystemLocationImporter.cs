﻿using DataImport.GameModels;
using TradeInSpace.Explore.Socpak;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using TradeInSpace.Data;
using TradeInSpace.Models;
using static DataImport.Importers.ImportUtils;
using TradeInSpace.Explore;
using TradeInSpace.Explore.DataForge;
using TradeInSpace.Explore.SC.Data;
using TradeInSpace.Explore.Subsumption;
using DN = System.DoubleNumerics;
using Newtonsoft.Json;
using DataImport.Utils;

namespace DataImport.Importers
{
    public class SystemLocationImporter : DataImporter
    {
        public ParserResponseCollection<SOC_Entry, SystemLocation> ImportedLocations;

        private readonly SubsumptionManager subManager;
        private SystemBodyImporter systemImporter;

        private static Dictionary<string, Guid> StarmapFix = new Dictionary<string, Guid>()
        {
            {"stash_001_dust_{1620C488-A2F4-477B-8823-4C14023019E9}_socpak",new Guid("c6987a08-91bf-4fda-93be-62d03b344a8d") }, //Duplicate mapping for 
            {"drug_farm_001_frost_{B2C51FC5-62D2-47A2-A5D5-6BB2716D751A}_socpak",new Guid("c1405795-f6f7-4c5a-bec5-8cba4595712c") } //
        };

        public SystemLocationImporter(SubsumptionManager subManager, SystemBodyImporter systemImporter, 
            ILoggerFactory factory = null)
            : base(factory?.CreateLogger("System Location Importer"))
        {
            this.subManager = subManager;
            this.systemImporter = systemImporter;

            this.ImportedLocations = new ParserResponseCollection<SOC_Entry, SystemLocation>();
        }

        public override void CreateRecords(TISContext context, DFDatabase rawGameData, GameDatabase workingDataset)
        {
            Logger?.LogInformation("Loading subsumption hierarchy");

            //subManager.ExploreAllSystems();

            /*
             * Mission location parsing...Does not work yet, but should be a better solution ultimately
             * Probably not??? there is too much variance with supplied values for these to be useful i guess....
             * No wonder game has so many bugs XD
             * 
            var missionLocations = rawGameData.GetEntries<MissionLocation>().ToList();
            Logger?.LogInformation($"Found {missionLocations.Count} Mission Locations");

            var MissionLocationMap = new Dictionary<MissionLocation, SubsumptionNode>();

            foreach (var missionLoc in missionLocations)
            {
                var locationSuperGUID = string.IsNullOrWhiteSpace(missionLoc.LocationParams.ActionAreaSuperGUID) ?
                    missionLoc.LocationParams.ObjectContainerSuperGUID : missionLoc.LocationParams.ActionAreaSuperGUID;

                var containerSuperGUID = missionLoc.LocationParams.ObjectContainerSuperGUID;
                var containerParentSuperGUID = string.Join(',', containerSuperGUID.Split(',').SkipLast(1));

                //PROBLEM: GUID Resolution is flawed
                //There is sometimes a "gap" in the mission SuperGUID that leads to inability to directly string-compare superGuids
                //Example: A mission location points to 
                //  4a269932-183e-362d-aacc-188e7debbea9,495625f2-8a2c-e796-b2f9-31fb9d5d809b,45e19546-c411-8f0c-272a-ed7ae34242b1,472359fe-1867-4d24-f216-3f1d6abf1c8e,4c4433f0-05e0-0a55-261b-30112c243091
                //With the valid target having a real superGUID of
                //  4a269932-183e-362d-aacc-188e7debbea9,495625f2-8a2c-e796-b2f9-31fb9d5d809b,45e19546-c411-8f0c-272a-ed7ae34242b1,472359fe-1867-4d24-f216-3f1d6abf1c8e,42398137-e2c3-cfd6-9f74-5a15c4d814b4,4c4433f0-05e0-0a55-261b-30112c243091
                //Note the extra GUID in the second-to-last position
                //This appears to be a problem with mission locations specifically, but may come up later
                //Resoltuion: Slice search and do a recursive search by depth ~_~

                var searchSystem = locationSuperGUID.Split(",").First();
                var searchParts = locationSuperGUID.Split(",").Skip(1);
                //Do a preliminary full search through subsumption
                SubsumptionNode currentSearch = subManager.AllNodes.SingleOrDefault(n => n.SuperGUID == locationSuperGUID);

                var quickSearch = subManager.AllNodes.Where(n => n.GUID == locationSuperGUID.Split(",").Last());
                if (currentSearch == null && quickSearch.Count() > 0)
                {
                    currentSearch = quickSearch.First();
                }

                if (currentSearch == null)
                {
                    currentSearch = subManager.AllNodes.Single(n => n.SuperGUID == searchSystem);
                    foreach (var searchGUID in searchParts)
                    {
                        //Fast direct-child search
                        var foundMatches = currentSearch.Children.Where(c => c.GUID == searchGUID).ToList();
                        //Slow path
                        if (foundMatches.Count == 0)
                        {
                            foundMatches = Utilities.FlattenRecursive(currentSearch, c => c.Children)
                                .Where(c => c.GUID == searchGUID).ToList();
                        }

                        if (foundMatches.Count == 0)
                        {
                            Logger.LogError($"Unable to find GUID {searchGUID} under parent {currentSearch.Name}/{currentSearch.SuperGUID} for superGUID path {locationSuperGUID}");
                            currentSearch = null;
                            break;
                        }
                        else if (foundMatches.Count > 1)
                        {

                        }

                        currentSearch = foundMatches.First();
                    }
                }

                if(currentSearch != null)
                {
                    MissionLocationMap[missionLoc] = currentSearch;
                }
                else
                {
                    Logger.LogError($"Unable to find Subsumption node with superGUID: {locationSuperGUID}");
                }
            }
            */

            //Still using system locations for now, but we can at least enumerate via subsumption tree structure
            
            string[] IgnoreTypes = new string[] { };

            Logger?.LogInformation($"Parsing System Locations - {systemImporter.ImportedLocations.Count} locations");

            Func<SOC_Entry, bool> FindStarmapParent = (parentTest) =>
            {
                return systemImporter.ImportedBodies.HasGUID(parentTest.GUID);
                //return systemImporter.DBBodyLocations.Any(l => parentTest.StarmapRecord.HasValue && parentTest.StarmapRecord.Value == l.Value.StarmapID);
                //return workingDataset.SystemLocations.Any(l => l.SCRecordID == parentTest.GUID);
            };

            foreach (var bodyEntry in systemImporter.ImportedLocations)
            {
                var bodySOC = bodyEntry.Entry;
                var body = bodyEntry.Record;

                bodySOC.CalculateGlobalPositions();

                if (bodySOC.StarmapRecord.HasValue)
                {
                    var bodyStarmap = rawGameData.GetEntry<StarmapObject>(bodySOC.StarmapRecord.Value);

                    Logger?.LogDebug($"");
                    Logger?.LogDebug($"");
                    Logger?.LogInformation($"Exploring [{bodySOC.GUID}] {bodySOC.Label} - {bodySOC?.Name ?? bodySOC.Name}");

                    foreach (var containerEntry in bodySOC.ExploreRecursive(ReturnRoot: true, ShouldExplore: c => !systemImporter.ImportedBodies.HasGUID(c.Container.GUID)))
                    {
                        var containerChild = containerEntry.Item2;

                        /*
                        if(StarmapFix.ContainsKey(containerChild.Label))
                        {
                            Logger?.LogWarning($"Using Manual Override for location {containerChild.Name}");
                            containerChild.StarmapRecord = StarmapFix[containerChild.Label];
                        }
                        */

                        if (containerChild.StarmapRecord.HasValue && containerChild.StarmapRecord.Value != Guid.Empty)
                        {
                            Logger?.LogDebug($"Checking {containerChild.Label} - {containerChild.Name}");
                            var childStarmapObj = rawGameData.GetEntry<StarmapObject>(containerChild.StarmapRecord.Value, AllowMissing: true);

                            if (string.IsNullOrWhiteSpace(childStarmapObj?.Name))
                            {
                                Logger?.LogWarning($"No starmap object exists for ID {containerChild.StarmapRecord.Value} on record {containerChild?.Name}. Skipping");
                                continue;
                            }

                            Logger?.LogDebug($"Child: [{containerChild.GUID}]\tChild Starmap: [{containerChild.StarmapRecord}]\tChild:{containerChild?.Name ?? containerChild.Name}");

                            switch (childStarmapObj.ObjectType.Name)
                            {
                                case "Planet":
                                case "Moon":
                                case "Lagrange Point":
                                case "Star":
                                case "CardinalPoint":
                                case "YouAreHere":
                                case "QuantumTracePoint":
                                    break;
                                case "Asteroid":
                                    break;
                                case "Manmade":
                                case "Outpost":
                                case "LandingZone":
                                case "PointOfInterest":
                                    Logger?.LogInformation($"\tFound {childStarmapObj.ObjectType.Name} - [{childStarmapObj.ID}] {childStarmapObj?.Name}");

                                    SystemLocation parentLocation = null;

                                    var starmapParent = containerChild.SearchUpSoc(FindStarmapParent, true);

                                    if (starmapParent != null && starmapParent.StarmapRecord.HasValue)
                                    {
                                        parentLocation = systemImporter.ImportedLocations.FindByGUID(starmapParent.GUID).Record;
                                    }

                                    containerChild.CalculateGlobalPositions();

                                    var locationAdditional = CoordinateHelper.BuildLocationAdditionalProperties(containerChild.GlobalPosition, bodySOC.GlobalPosition, containerChild.Rotation);
                                    locationAdditional.QTDistance = childStarmapObj.HasQuantum ? (int?)childStarmapObj.ArrivalRadius : null;

                                    var sysLocation = new SystemLocation()
                                    {
                                        SCRecordID = containerChild.GUID,
                                        ParentLocation = parentLocation,
                                        ParentBody = body,
                                        StarmapID = childStarmapObj.ID,
                                        Key = Utilities.CleanKey(childStarmapObj.Name),
                                        Name = childStarmapObj.Name,
                                        Description = childStarmapObj.Description,
                                        LocationType = Enum.Parse<Enums.LocationType>(childStarmapObj.ObjectType.Name, true),
                                        IsHidden = childStarmapObj.HiddenInStarmap,
                                        QTDistance = childStarmapObj.HasQuantum ? (int?)childStarmapObj.ArrivalRadius : null,
                                        ChildLocations = new List<SystemLocation>(),
                                        AdditionalProperties = JsonConvert.SerializeObject(locationAdditional)
                                    };

                                    ImportedLocations.Add(containerChild, sysLocation);

                                    workingDataset.SystemLocations.Add(sysLocation);
                                    break;
                                default:
                                    Logger?.LogError($"Unknown starmap point type {childStarmapObj.ObjectType.Name} - {bodyEntry.Entry.Name} - {bodyEntry.Record.Name}");
                                    break;
                            }
                        }
                    }
                }
            }
        }



    }

    [DebuggerDisplay("AA {ActionAreaEntity.Label} - {ActionAreaSoc.Label}")]
    class ActionArea
    {
        public ActionArea Parent { get; private set; }
        public List<ActionArea> Children { get; private set; }

        public Guid GUID { get { return ActionAreaEntity.GUID; } }
        public ExposedEntity ActionAreaEntity { get; private set; }
        public SOC_Entry ActionAreaSoc { get; private set; }

        private ActionArea(ActionArea parent, ExposedEntity actionAreaEntity, SOC_Entry actionAreaSoc)
        {
            Parent = parent;
            Parent?.Children.Add(this);
            ActionAreaEntity = actionAreaEntity;
            ActionAreaSoc = actionAreaSoc;
            Children = new List<ActionArea>();
        }

        public static ActionArea ExploreSoc(SOC_Entry child, DFDatabase rawGameData, Func<(int Depth, SOC_Entry Container), bool> ShouldExplore, bool ReturnRoot = true)
        {
            Dictionary<Guid, ActionArea> ExploredParents = new Dictionary<Guid, ActionArea>();

            Func<SOC_Entry, ActionArea> SearchParent = (soc) =>
            {
                while(soc != null)
                {
                    if (ExploredParents.ContainsKey(soc.GUID)) return ExploredParents[soc.GUID];
                    soc = soc.Parent;
                }
                return null;
            };

            ActionArea rootActionArea = null;

            foreach (var body_child in child.ExploreRecursive(ReturnRoot, ShouldExplore))
            {
                if (body_child.Item2 != null)
                {
                    foreach (var body_entity in body_child.Item2.Entities)
                    {
                        if (body_entity.Tags.Contains("ActionAreaComponent"))
                        {
                            var aa = new ActionArea(SearchParent(body_child.Item2), body_entity, body_child.Item2);

                            ExploredParents[body_child.Item2.GUID] = aa;

                            if (rootActionArea == null) rootActionArea = aa;
                        }
                    }
                }
            }

            return rootActionArea;
        }
    }
}
