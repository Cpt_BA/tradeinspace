﻿using DataImport.GameModels;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using TradeInSpace.Data;
using TradeInSpace.Explore;
using TradeInSpace.Explore.DataForge;
using TradeInSpace.Models;

namespace DataImport.Importers
{
    using Models = TradeInSpace.Models;
    using Components = TradeInSpace.Explore.SC.Data;

    public class ManufacturerImporter : DataImporter
    {
        public ManufacturerImporter(ILoggerFactory factory = null)
            : base(factory?.CreateLogger("Manufacturer Importer"))
        {
        }

        public ParserResponseCollection<Components.Manufacturer, Manufacturer> ImportedManufacturers = new ParserResponseCollection<Components.Manufacturer, Manufacturer>();

        public override void CreateRecords(TISContext context, DFDatabase rawGameData, GameDatabase workingDataset)
        {
            foreach (var manufacturerRecord in rawGameData.GetEntries<Components.Manufacturer>())
            {
                var manufacturer = new Manufacturer()
                {
                    SCRecordID = manufacturerRecord.ID,
                    Key = Utilities.CleanKey(manufacturerRecord.InstanceName),
                    Name = manufacturerRecord.Name,
                    Description = manufacturerRecord.Description,
                    Code = manufacturerRecord.Code,
                    LogoName = Path.GetFileNameWithoutExtension(manufacturerRecord.IconPath)
                };

                workingDataset.Manufacturers.Add(manufacturer);
                ImportedManufacturers.Add(manufacturerRecord, manufacturer);

                Logger?.LogInformation($"Imported manufacturer {manufacturerRecord.InstanceName}");
            }
        }
    }
}
