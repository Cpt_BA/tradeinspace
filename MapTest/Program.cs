﻿using CgfConverter;
using CgfConverter.CryEngineCore;
using ICSharpCode.SharpZipLib.Zip;
using Serilog;
using Svg;
using Svg.Pathing;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TradeInSpace.Explore;
using TradeInSpace.Explore.DataForge;
using TradeInSpace.Explore.SC;
using TradeInSpace.Explore.SC.Components;
using TradeInSpace.Explore.SC.Data;
using TradeInSpace.Explore.SC.Loadout;
using TradeInSpace.Explore.Socpak;
using TradeInSpace.Explore.Subsumption;
using TradeInSpace.Export;
using TradeInSpace.Parse;
using UkooLabs.FbxSharpie;
using UkooLabs.FbxSharpie.Tokens;
using UkooLabs.FbxSharpie.Tokens.Value;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Xml;
using System.Runtime.InteropServices;
using TradeInSpace.Export.Planet;
using OSGeo.GDAL;
using OSGeo.OGR;

namespace MapTest
{
    using DN = System.DoubleNumerics;

    class Program
    {
        private DFDatabase gameDatabase;
        private static string RenderDirectory = @"C:\Users\benda\Documents\Unity\TestProj\PointTest\Assets\Models";
        private static string ModelExportDir = @"C:\Users\benda\Documents\Unity\TestProj\PointTest\Assets\Models";

        static void Main(string[] args)
        {
            new Program().Run();
        }

        public void Run()
        {
            GdalConfiguration.ConfigureGdal();
            GdalConfiguration.ConfigureOgr();
            
            Gdal.AllRegister();
            Ogr.RegisterAll();

            //var test = CryToFBXExporter.ConstructTestScene();
            //CryToFBXExporter.WriteScene(test, @"C:\Users\benda\Documents\Unity\TestProj\PointTest\Assets\test_cube.fbx");
            //CryToFBXExporter.WriteScene(test, @"C:\Users\benda\Documents\Unity\TestProj\PointTest\Assets\test_cube_scii.fbx", Binary: false);



            /*
            var data = Convert.FromBase64String("zpV831MGfdpEzgomsj+0uRPEkWVuG3jC+Mm4Pc5SdioYd+c9dvz40cfuuvPj+ETEB0L6n00zdORZUQZktRS8Q04xIzRuTo9zn8T3KCUGbSeh/JvItnJYNieAzKm9OOd7786K7EVXrIjHM72q60+RvEnzItxdDKOf+GIeunyAE9184Cp0cSRUDWyTnrg8YdpHG1mn2y+uJjHqsrVTNRG7Tprk1aHAnPrNJfYAdBSgN6yA4KcIu368gX9QIC2U04CQDISVx/y6W6mGy5C3aqFTu65sfEsq1pt6ia71NoaERXsnWO4X6B6Cxd6S/uyrEU5t/ovcm9tmj/X3Sz+Zfjn6c7jJKf0Ob2qDHxzDL5cNCkflVeV+VsHYB2ar+L5lXxS0KbpBL8aBIE3tVT+YKECzx2GnRRCf/LyHF2zMdb481Dn93cRJ5M7yBomnUHVd4JVB6sx2k6kdxe9G+iabi9IXQlTPzei8CVD+vnYgDriDL2Cffyj6OLr7zBgGDuWkKlYbO5cdR/HiNUhwlVmT9XpmcVU0HKHWhUl2h7mRGTMds9i7IeamSkr4C2gpBYd81mqhtnqOYhlpuOMrUoC43x5yTZoTYz4PfwRfZrxjTrihfea46cAScsFZXa7Csg8wOR1PJxO1bovtnjT1STJlIkjjP0LTzDgPSog6+75CiF5sCSoJlLHzPagLuj/8KbstZsnLgE4JytuZpsJ7g5US/Xn8mLl+mtgeZloVZscEa53/MvhRPdgXIROQ4KUt1zi3w0rD0cFKsuBXmZ8f0x/TgfqhYCmZXGigLCVOvFMX/0ksZawfrXP6GebrqO8GCGEhgF+VAMeOA5hZNLofvoowKgO610WIJ43UZ213MAYjmqBz9fny3EId85ZP1wfv4UFMo9Zd");

            using (var ms = new MemoryStream(data))
            {
                var strm = new LZ4.LZ4Stream(ms, Sys-tem.IO.Compression.CompressionMode.Decompress);
                byte[] outData = new byte[32];
                var read = strm.Read(outData, 0, outData.Length);
            }
            */

            /*
            using (var fs = File.OpenRead(@"E:\SC\hurston\PlanetTerrainHeightMapGenPass.cfib"))
            using (var bs = new BinaryReader(fs))
            {
                var res = FXB0Parser.Parse(bs);
            }
            */
            

            Log.Logger = new LoggerConfiguration()
                .WriteTo.Console(Serilog.Events.LogEventLevel.Debug)
                .WriteTo.File("mapping_log.txt", rollingInterval: RollingInterval.Day)
                .CreateLogger();
            Log.Information("Starting Export");

            var launcherInstallDir = Microsoft.Win32.Registry.GetValue(@"HKEY_LOCAL_MACHINE\SOFTWARE\81bfc699-f883-50c7-b674-2483b6baae23", "InstallLocation", null) as string;
            var RSIDir = Path.GetDirectoryName(launcherInstallDir);
            var SCSettings = SCLauncherReader.LoadGameSettings(RSIDir);

            gameDatabase = DFDatabase.ParseFromSettings(SCSettings, "LIVE", Cache: CacheLevel.Diabled);

            PlanetRenderer render = new PlanetRenderer(gameDatabase);

            
            var pd = PlanetData.CreateFromSocpakPath(gameDatabase, "Data/objectcontainers/pu/system/stanton/stanton1c.socpak");

            var settings = new RenderSettings()
            {
                SplatMapRenderArea = new Rectangle(0, 0, pd.TileCount, pd.TileCount),
                TileSize = 8
            };

            render.RenderWholeSurface(@"E:\SC\Render\Planets\test_2.geotiff", "GeoTiff", pd, settings, RenderSize: 4096);
            Log.Debug("Starting Render");

            RenderSketto(render);

            //PlanetRender("Data/objectcontainers/pu/system/stanton/stanton1.socpak", render);
            //PlanetRender("Data/objectcontainers/pu/system/stanton/stanton1a.socpak", render);
            //PlanetRender("Data/objectcontainers/pu/system/stanton/stanton1b.socpak", render);
            PlanetRender("Data/objectcontainers/pu/system/stanton/stanton1c.socpak", render);
            //PlanetRender("Data/objectcontainers/pu/system/stanton/stanton1d.socpak", render);

            //PlanetRender("Data/objectcontainers/pu/system/stanton/stanton2.socpak", render);
            //PlanetRender("Data/objectcontainers/pu/system/stanton/stanton2a.socpak", render);
            //PlanetRender("Data/objectcontainers/pu/system/stanton/stanton2b.socpak", render);
            //PlanetRender("Data/objectcontainers/pu/system/stanton/stanton2c.socpak", render);
            
            //PlanetRender("Data/objectcontainers/pu/system/stanton/stanton3.socpak", render);
            //PlanetRender("Data/objectcontainers/pu/system/stanton/stanton3a.socpak", render);
            //PlanetRender("Data/objectcontainers/pu/system/stanton/stanton3b.socpak", render);

            //PlanetRender("Data/objectcontainers/pu/system/stanton/stanton4.socpak", render);
            //PlanetRender("Data/objectcontainers/pu/system/stanton/stanton4a.socpak", render);
            //PlanetRender("Data/objectcontainers/pu/system/stanton/stanton4b.socpak", render);
            //PlanetRender("Data/objectcontainers/pu/system/stanton/stanton4c.socpak", render);

            /*
            foreach (ZipEntry entry in gameDatabase.GamePack)
            {
                if (entry.Name.Contains(".socpak") && entry.Name.Contains("system/stanton"))
                {
                    try
                    {
                        var planet = SOC_Archive.LoadFromGameDBSocPak(gameDatabase, entry.Name, false);
                        
                        if(planet.RootEntry != null)
                        {
                            planet.RootEntry.LoadAllEntities(false, false, false);

                            var json = planet.RootEntry.PlanetJSON;

                            if (string.IsNullOrEmpty(json)) continue;

                            File.WriteAllText($"E:\\SC\\Render\\{planet.Name}.json", json);
                            Console.WriteLine($"Wrote: {planet.Name}");
                        }
                    }
                    catch (Exception ex)
                    {
                    }
                }
            }
            */

            foreach (ZipEntry entry in gameDatabase.GamePack)
            {
                if (entry.Name.Contains("planets/terrain"))
                {
                    if(entry.Name.Contains(".eco"))
                    {
                        Console.WriteLine(entry.Name);
                    }
                    else if (entry.Name.Contains("climvar.dds"))
                    {
                        try
                        {
                            string baseName = entry.Name;
                            int fileIndex = 0;

                            if (Path.GetExtension(baseName) != ".dds")
                            {
                                fileIndex = int.Parse(baseName.Substring(baseName.LastIndexOf('.') + 1));
                                baseName = baseName.Substring(0, baseName.LastIndexOf('.'));
                            }

                            using (Bitmap bmp = ExportUtils.ExtractImage(gameDatabase, entry.Name))
                            {
                                if (bmp != null)
                                {
                                    var basePath = Path.GetDirectoryName(baseName);
                                    var outPath = Path.ChangeExtension(Path.Combine(@"E:\SC\hurston", basePath, $"{Path.GetFileNameWithoutExtension(baseName)}_{fileIndex}"), ".png");
                                    var outDir = Path.GetDirectoryName(outPath);
                                    if (!Directory.Exists(outDir))
                                        Directory.CreateDirectory(outDir);
                                    bmp.Save(outPath);
                                }
                            }
                            Log.Information($"Saved: {baseName}");
                        }
                        catch (Exception ex)
                        {
                            Log.Error(ex, "Error with image");
                        }
                    }
                    else if (entry.Name.Contains(".r16"))
                    {
                        string out_dir = @"E:\SC\hurston";
                        var heightmapData = gameDatabase.ReadPackedFileBinary(entry.Name);
                        var basePath = Path.GetDirectoryName(entry.Name);
                        var outPath = Path.ChangeExtension(Path.Combine(@"E:\SC\hurston", basePath, $"{Path.GetFileNameWithoutExtension(entry.Name)}"), ".png");
                        var outDir = Path.GetDirectoryName(outPath);
                        if (!Directory.Exists(outDir))
                            Directory.CreateDirectory(outDir);
                        File.WriteAllBytes(Path.Combine(outDir, $"heightmap.r16"), heightmapData);

                        var heightmapSize = (int)Math.Sqrt(heightmapData.Length / 2);
                        var heightmapSpan = new ReadOnlySpan<byte>(heightmapData);

                        byte[] raw_16bit_data = new byte[heightmapSize * heightmapSize * 2];

                        using (Bitmap bmp = new Bitmap(heightmapSize, heightmapSize))
                        {
                            for (int y = 0; y < heightmapSize; y++)
                            {
                                for (int x = 0; x < heightmapSize; x++)
                                {
                                    ushort height = System.Buffers.Binary.BinaryPrimitives.ReadUInt16BigEndian(heightmapSpan.Slice((y * heightmapSize + x) * 2));

                                    bmp.SetPixel(x, y, Color.FromArgb(255, height / 256, height / 256, height / 256));
                                    raw_16bit_data[(y * heightmapSize + x) * 2] = (byte)(height & 0xFF);
                                    raw_16bit_data[(y * heightmapSize + x) * 2 + 1] = (byte)(height >> 8);
                                }
                            }

                            bmp.Save(Path.Combine(outDir, $"heightmap.png"));
                        }


                        File.WriteAllBytes(Path.Combine(outDir, $"heightmap_16.raw"), raw_16bit_data);

                    }
                }
            }


            //var test_scene = CryToFBXExporter.ConstructTestSceneFromHierarchy();
            //CryToFBXExporter.WriteScene(test_scene, @"C:\Users\benda\Documents\Unity\TestProj\PointTest\Assets\Models\test_cube.fbx");


            //var kraken = gameDatabase.ReadPackedCGAFile("Data/Objects/Spaceships/Holoviewer_Ships/XIAN/Railen/RSI_Apollo_Holoviewer.cgf");
            //kraken.ProcessCryengineFiles();
            //var scene = CryToFBXExporter.ConvertModelToScene(kraken);
            //CryToFBXExporter.WriteScene(scene, @"C:\Users\benda\Downloads\Apollo_Test.fbx");

            gameDatabase.ParseDFContent();

            Log.Information("Done parsing. Starting import.");

            /*
            for (int i = 0; i < 100; i++)
            {
                var test = SOC_Archive.LoadFromGameDBSocPak(gameDatabase, "Data/objectcontainers/pu/station/hotel/portolisar.socpak");
            }
            */

            //foreach (var entity in gameDatabase.Entities)

            //LoadAndRenderShipTest(@"Objects\Spaceships\Ships\BANU\Defender\Banu_Defender.cga");
            //LoadAndRenderShipTest(@"Objects\Spaceships\Ships\DRAK\Cutlass\DRAK_Cutlass_Black.cga");
            //LoadAndRenderShipTest(@"Objects\Spaceships\Ships\RSI\Aurora\Exterior\RSI_Aurora.cga");
            //LoadAndRenderShipTest(@"Objects\Spaceships\Ships\ARGO\MPUV_Utility_Vehicle\ARGO_MPUV.cga");
            //LoadAndRenderShipTest(@"Objects\Spaceships\Ships\ORIG\890Jump\exterior\ORIG_890Jump.cga");

            //LoadAndRenderEquipment_FBX(new Guid("85726664-e16d-4a24-98f8-7cc8f0ba6f00"));
            //LoadAndRenderEquipment_FBX(new Guid("b8a352bb-74a8-415b-9e08-77f684ac91b8"));

            //LoadAndRenderShipSkeleton_FBX(new Guid("b58f7735-9454-4a0a-b724-2bc8d61961d7")); //Cutlass Black
            //LoadAndRenderShipSkeleton_FBX(new Guid("7cab9bbc-3d67-4ee7-99ef-fe41991f8a3b")); //Aurora
            //LoadAndRenderShipSkeleton_FBX(new Guid("081236f5-9549-4a60-9123-6ae21f0cd261")); //Banu
            //LoadAndRenderShipSkeleton_FBX(new Guid("50ddaf5e-a8e3-42fe-a1d3-7267f39b39b0")); //890J
            //LoadAndRenderShipSkeleton_FBX(new Guid("e19fc6c3-f5fa-4623-9a17-b00a29b71500")); //Argo Cargo
            //LoadAndRenderShipSkeleton_FBX(new Guid("f6d606c9-b324-4efa-814f-15a59047a6a5")); //Carrack
            //LoadAndRenderShipSkeleton_FBX(new Guid("e19fc6c3-f5fa-4623-9a17-b00a29b71500")); //MPUV
            //LoadAndRenderShipSkeleton_FBX(new Guid("dc39ca6b-1d76-4db5-9346-356f49954978")); //Cat
            //LoadAndRenderShipSkeleton_FBX(new Guid("fb484074-f33f-4977-a052-2937e8508acc")); //Mule
            //LoadAndRenderShipSkeleton_FBX(new Guid("797201fa-ca9a-48b1-abcb-48b6d4d86a37")); //Idris
            //LoadAndRenderShipSkeleton_FBX(new Guid("05a4f566-36a4-495c-99a4-dbedc5105cb6")); //Javelin
            //LoadAndRenderShipSkeleton_FBX(new Guid("b4791030-9d64-4f98-85cb-ca106f868c5e")); //Freelancer
            //LoadAndRenderShipSkeleton_FBX(new Guid("D46F4DE1-8DC1-473B-9739-88C9BA7E75A6")); //Hammerhead
            //LoadAndRenderShipSkeleton_FBX(new Guid("36726a9f-7e14-4942-bc4f-1a398b5bef6a")); //

            //LoadAndRenderShipMegaModel(new Guid("fb484074-f33f-4977-a052-2937e8508acc")); //Mule
            //LoadAndRenderShipMegaModel(new Guid("b4791030-9d64-4f98-85cb-ca106f868c5e")); //Freelancer
            //LoadAndRenderShipMegaModel(new Guid("D46F4DE1-8DC1-473B-9739-88C9BA7E75A6")); //Hammerhead
            //LoadAndRenderShipMegaModel(new Guid("36726a9f-7e14-4942-bc4f-1a398b5bef6a")); //
            //LoadAndRenderShipMegaModel(new Guid("50ddaf5e-a8e3-42fe-a1d3-7267f39b39b0")); //890J
            //LoadAndRenderShipMegaModel(new Guid("7cab9bbc-3d67-4ee7-99ef-fe41991f8a3b")); //Aurora
            //LoadAndRenderShipMegaModel(new Guid("081236f5-9549-4a60-9123-6ae21f0cd261")); //Banu
            LoadAndRenderShipMegaModel(new Guid("e1afb014-fe71-4da9-89ae-c30fcf9c0782")); //Corsair

            //LoadAndRender("Data/ObjectContainers/PU/flair/flair_spaceglobe_salvage.socpak");
            //LoadAndRender("Data/objectcontainers/pu/loc/flagship/stanton/lorville/lorville_cbd.socpak");
            //LoadAndRender("Data/objectcontainers/pu/loc/flagship/stanton/lorville/lorville_l19.socpak");
            //LoadAndRender("Data/objectcontainers/pu/loc/flagship/stanton/lorville/gates/gate_01.socpak");
            //LoadAndRender("Data/objectcontainers/pu/station/hotel/portolisar.socpak");
            //LoadAndRender("Data/objectcontainers/pu/station/hotel/portolisarstrut.socpak");
            //LoadAndRender("Data/objectcontainers/pu/station/hotel/portolisarstrut_int.socpak");
            //LoadAndRender("Data/objectcontainers/pu/station/security/securitypostkareah.socpak");
            //LoadAndRender("Data/objectcontainers/pu/station/security/securitypostkareah_int.socpak");
            //LoadAndRender("Data/objectcontainers/pu/shops/personalweapon/livefire/livefireweapons_olisar.socpak");
            //LoadAndRender("Data/objectcontainers/pu/shops/shippart/dumpersdepot/dumpersdepot_olisar.socpak");
            //LoadAndRender("Data/objectcontainers/pu/shops/food/coffee_to_go/ht_coffee_to_go.socpak");
            //LoadAndRender("Data/objectcontainers/pu/station/motel/grimhex.socpak");
            //LoadAndRender("Data/objectcontainers/pu/loc/mod/common/hangar/util_a/hangar_xltop_001.socpak");
            //LoadAndRender("Data/ObjectContainers/PU/system/stanton/stantonsystem.socpak");
            //LoadAndRender("Data/objectcontainers/pu/loc/mod/stanton/station/ser/reststop_ext/rs_ext_arc-leo1.socpak");
            //LoadAndRender("Data/objectcontainers/pu/loc/flagship/stanton/newbab/newbab_all.socpak");
            //LoadAndRender("Data/objectcontainers/pu/loc/mod/stanton/station/ser/reststop_ext/rs_ext_mic-leo1.socpak");
            //LoadAndRender("Data/objectcontainers/pu/system/stanton/stanton4/outpost/em_shelter_002_frost_{7913567c-e54a-4505-aff2-19144299b71e}.socpak");
            //LoadAndRender("Data/objectcontainers/pu/system/stanton/stanton1b/cave/rocky_lunar_unc_drive_fly_s_001_{586a882b-dd65-4994-a953-e24511a8208a}.socpak");
            //LoadAndRender("Data/objectcontainers/pu/system/stanton/stanton4/ugf/ugf_lta_a_frost_001_{21ace0fa-4e0b-4e60-acd7-0b6a7f4b2c3e}.socpak");

            //LoadAndRender("Data/objectcontainers/pu/loc/flagship/stanton/orison/orison_ind/orison_ind_lz_int.socpak");

            //LoadAndRender("Data/objectcontainers/pu/loc/flagship/stanton/newbab/newbab_sp_ext.socpak");
            //LoadAndRender("Data/objectcontainers/pu/loc/flagship/stanton/newbab/newbab_sp_int.socpak");
            //LoadAndRender("Data/objectcontainers/pu/loc/flagship/stanton/newbab/newbab_sp_transit_int_001.socpak");
            //LoadAndRender("Data/ObjectContainers/PU/loc/mod/common/hangar/hightech_a/hangar_medtop_001_newbab.socpak");

            //LoadAndRender("Data/objectcontainers/pu/system/stanton/stanton1/surfacerelay/sr_layout_001_gen_{3dfe13b4-b4b6-4d91-9cad-40fc452c7d6b}.socpak");
            //LoadAndRender("Data/objectcontainers/pu/loc/flagship/stanton/newbab/newbab_all.socpak");
            //LoadAndRender("Data/objectcontainers/pu/loc/flagship/stanton/lorville/lorville.socpak");


            //LoadAndRender("Data/objectcontainers/pu/loc/flagship/stanton/newbab/newbab_domes_int_001.socpak");

            DumpModels(false);
        }

        SOC_Explorer.SearchConfig MegaModelConfiguration = new SOC_Explorer.SearchConfig()
        {
            LoadEntities = true,
            LoadReferences = true,
            Recursive = true,
            DummyEntries = true,
            DynamicComponents = true,
            LoadModelStructure = true,
            LoadModelGeometry = true,
            IncludeModelReferences = false,
            ApplyShipLoadout = true
        };

        SOC_Explorer.SearchConfig ShipSkeletonConfiguration = new SOC_Explorer.SearchConfig()
        {
            LoadEntities = true,
            LoadReferences = true,
            Recursive = true,
            DummyEntries = true,
            DynamicComponents = true,
            LoadModelStructure = false,
            LoadModelGeometry = false,
            IncludeModelReferences = true,
            ApplyShipLoadout = false
        };

        SOC_Explorer.SearchConfig CollectModelsConfiguration = new SOC_Explorer.SearchConfig()
        {
            LoadEntities = true,
            LoadReferences = true,
            Recursive = true,
            DummyEntries = false,
            DynamicComponents = true,
            LoadModelStructure = true,
            LoadModelGeometry = false,
            IncludeModelReferences = true,
            ApplyShipLoadout = true
        };

        private void LoadAndRender(string socPath)
        {
            var loadedArchive = SOC_Archive.LoadFromGameDBSocPak(gameDatabase, socPath);
            var structure = SOC_Explorer.ExploreNode(gameDatabase, loadedArchive.RootEntry, MegaModelConfiguration);
            /*new SOC_Explorer.SearchConfig()
            {
                LoadEntities = true,
                LoadReferences = true,
                Recursive = true,
                DummyEntries = true,
                DynamicComponents = true,
                //LoadModelStructure = true
            });
            */

            var equipment_dir = Path.Combine(ModelExportDir, "SOC_Models");
            var fbx_path = Path.Combine(equipment_dir, Path.ChangeExtension(Path.GetFileNameWithoutExtension(socPath), ".fbx"));
            var fbx_scene = CryToFBXExporter.ConvertHierarchyToScene(structure, gameDatabase, FBXExportSettings.DefaultSettings);
            CryToFBXExporter.WriteScene(fbx_scene, fbx_path);

            //_DoRender(structure, true);

            Log.Information($"Done rendering {socPath}");
        }

        private void LoadAndRenderShipMegaModel(Guid ShipGUID)
        {
            var shipEntity = gameDatabase.GetEntity(ShipGUID);
            var shipComponent = shipEntity.GetComponent<Vehicle>();

            if (shipComponent != null && shipComponent.ContainerEntries.Count > 0)
            {
                var structure = SOC_Explorer.ExploreShip(gameDatabase, shipComponent, MegaModelConfiguration);

                Log.Information($"Done exploring structure for: {shipEntity.InstanceName}. Starting Render.");

                // fbx export
                if (true)
                {
                    var fbx_path = Path.Combine(ModelExportDir, Path.ChangeExtension(shipEntity.InstanceName, ".fbx"));
                    var fbx_scene = CryToFBXExporter.ConvertHierarchyToScene(structure, gameDatabase);
                    CryToFBXExporter.WriteScene(fbx_scene, fbx_path);
                }

                _DoRender(structure, true);
                _DoRenderJSON(structure, true);

                var svgText = SVGMapExporter.GenerateSVG(gameDatabase, structure, v => v.X, v => v.Y, v => v.Z, true, false);
                File.WriteAllText(Path.Combine(ModelExportDir, Path.ChangeExtension(shipEntity.InstanceName, "svg")), svgText);

                Log.Information($"Done rendering {shipEntity.InstanceName}");
            }
        }

        private void LoadAndRenderShipSkeleton_FBX(Guid ShipGUID)
        {
            var shipEntity = gameDatabase.GetEntity(ShipGUID);
            var shipComponent = shipEntity.GetComponent<Vehicle>();

            if (shipComponent != null && shipComponent.ContainerEntries.Count > 0)
            {
                Log.Information($"Starting explore 1 - {shipEntity.InstanceName}");
                var structure = SOC_Explorer.ExploreShip(gameDatabase, shipComponent, ShipSkeletonConfiguration);

                //Log.Information("Starting explore 2");
                //SOC_Explorer.ExploreShip(gameDatabase, shipComponent, CollectModelsConfiguration);

                Log.Information($"Done exploring structure for: {shipEntity.InstanceName}. Starting Render.");

                var fbx_path = Path.Combine(ModelExportDir, Path.ChangeExtension(shipEntity.InstanceName, ".fbx"));
                var fbx_scene = CryToFBXExporter.ConvertHierarchyToScene(structure, gameDatabase);
                CryToFBXExporter.WriteScene(fbx_scene, fbx_path);
            }
        }

        private void LoadAndRenderEquipment_FBX(SCEntityClassDef equipmentEntity)
        {
            var attachable = equipmentEntity?.GetComponent<AttachableComponent>();

            if (attachable != null)
            {
                //Log.Information($"Rendering - {equipmentEntity.InstanceName}");

                var equipment_dir = Path.Combine(ModelExportDir, "Equipment_Render");
                var fbx_path = Path.Combine(equipment_dir, Path.ChangeExtension(equipmentEntity.ID.ToString(), ".fbx"));

                if (!File.Exists(fbx_path))
                {
                    var structure = SOC_Explorer.ExploreEquipment(gameDatabase, equipmentEntity, MegaModelConfiguration);
                    var fbx_scene = CryToFBXExporter.ConvertHierarchyToScene(structure, gameDatabase);
                    CryToFBXExporter.WriteScene(fbx_scene, fbx_path);
                }
            }
        }

        private void LoadAndRenderEquipment_FBX(Guid EquipmentGuid)
        {
            LoadAndRenderEquipment_FBX(gameDatabase.GetEntity(EquipmentGuid));
        }

        private void LoadAndRenderShipTest(string ModelPath)
        {
            SOC_Explorer.SearchConfig config = new SOC_Explorer.SearchConfig()
            {
                LoadEntities = true,
                LoadReferences = true,
                Recursive = true,
                DummyEntries = true,
                DynamicComponents = true
            };

            var structure = SOC_Explorer.ExploreModel(gameDatabase, ModelPath, config);
            _DoRender(structure, true);
            _DoRenderJSON(structure, true);

            var svgText = SVGMapExporter.GenerateSVG(gameDatabase, structure, v => v.X, v => v.Y, v => v.Z, true, false);
            File.WriteAllText(Path.Combine(ModelExportDir, Path.ChangeExtension(Path.GetFileName(ModelPath.Replace('\\', '/')), "svg")), svgText);

            Log.Information($"Done rendering {ModelPath}");
        }

        private void _DoRender(SCHieracrhyNode entry, bool IncludeEntities = false)
        {
            var cleanName = Utilities.CleanKey(entry.Name);

            var csv = CSVExporter.ExportCSV(gameDatabase, entry, ModelExportDir);

            if (!Directory.Exists("Renders"))
                Directory.CreateDirectory("Renders");

            File.WriteAllText(Path.Combine(RenderDirectory, $"{cleanName}_points.csv"), csv);
        }

        private void _DoRenderJSON(SCHieracrhyNode entry, bool IncludeEntities = false, bool IncludeXZ = false)
        {
            var cleanName = Utilities.CleanKey(entry.Name);

            if (!Directory.Exists(RenderDirectory))
                Directory.CreateDirectory(RenderDirectory);

            var outPath = Path.Combine(RenderDirectory, $"{cleanName}_features.shp");
            GeoJsonShipExporter.ExportShip(gameDatabase, entry, outPath, v => v.X, v => v.Y, v => v.Z);
            if (IncludeXZ)
            {
                var outPathXZ = Path.Combine(RenderDirectory, $"{cleanName}_features_xz.shp");
                GeoJsonShipExporter.ExportShip(gameDatabase, entry, outPath, v => v.X, v => v.Z, v => v.Y);
            }
        }

        private void DumpModels(bool Force)
        {
            Log.Information($"Dumping {CollectModelsConfiguration.UsedModels.Count} models.");

            foreach (var model_path in CollectModelsConfiguration.UsedModels)
            {
                var fbxPath = Path.ChangeExtension(Path.Combine(ModelExportDir, model_path), ".fbx");

                if (File.Exists(fbxPath))
                {
                    if (Force)
                    {
                        File.Delete(fbxPath);
                    }
                    else
                    {
                        continue;
                    }
                }

                var modelCGA = this.gameDatabase.ReadPackedCGAFile(model_path);

                try
                {
                    modelCGA.ProcessCryengineFiles();

                    var fbxModel = CryToFBXExporter.ConvertModelToScene(modelCGA);

                    if (!Directory.Exists(Path.GetDirectoryName(fbxPath)))
                        Directory.CreateDirectory(Path.GetDirectoryName(fbxPath));

                    using (var out_stream = File.OpenWrite(fbxPath))
                        new FbxBinaryWriter(out_stream).Write(fbxModel.Construct());
                }
                catch (Exception ex)
                {
                    Log.Error(ex, $"Error dumping model: {model_path}");
                }
            }
        }


        static Dictionary<Guid, (JObject, Bitmap)> PreloadedEcoSystems = null;
        private void PlanetRender(string planetSocpak, PlanetRenderer renderer)
        {
            //PlanetRenderer render = new PlanetRenderer(gameDatabase);
            var pd = PlanetData.CreateFromSocpakPath(gameDatabase, planetSocpak, Render: renderer);
            var name = Path.GetFileNameWithoutExtension(planetSocpak);
            Log.Information("Starting Render");
            int maxWidth = 16384;
            int maxSize = maxWidth / pd.TileCount / 2;
            int shift_x = 0;// pd.TileCount / 4;
            var settings = new RenderSettings();
            settings.SplatMapRenderArea = new Rectangle(0, 0, pd.TileCount, pd.TileCount);


            var out_dir = $"E:\\SC\\Render\\{name}\\";
            if (!Directory.Exists(out_dir)) Directory.CreateDirectory(out_dir);
            var out_path = Path.Combine(out_dir, $"{name}.gpkg");
            if (File.Exists(out_path)) File.Delete(out_path);

            for (int renderScale = 4; renderScale <= maxSize; renderScale *= 2)
            {
                settings.TileSize = renderScale;
                Log.Information($"Rendering Planet {name} at {renderScale}px per tile");
                
                var render_result = renderer.RenderScaledPlanet(pd, settings);

                render_result.RenderGeoPackage(out_path, $"{name}_{renderScale}px", IncludeTemp: true, IncludeHumid: true);
                /*
                render_result.ExportImageGDAL($"{out_dir}\\{renderScale}_surface.tiff", 
                    IncludeExport: true, ExportDriver: "PNG", ExportPath: $"{out_dir}\\{renderScale}_surface.png");
                render_result.ExportHeightmapGDAL($"{out_dir}\\{renderScale}_heightmap.tiff");
                */
            }
        }

        private void RenderSketto(PlanetRenderer renderer)
        {
            string planetSocpak = "Data/objectcontainers/pu/system/stanton/stanton1c.socpak";

            //PlanetRenderer render = new PlanetRenderer(gameDatabase);
            var pd = PlanetData.CreateFromSocpakPath(gameDatabase, planetSocpak, Render: renderer);
            var name = Path.GetFileNameWithoutExtension(planetSocpak);
            Log.Information("Starting Render");
            int maxWidth = 16384;
            int maxSize = maxWidth / pd.TileCount / 2;
            int shift_x = 0;// pd.TileCount / 4;
            var settings = new RenderSettings();
            settings.SplatMapRenderArea = new Rectangle(117 - 16, 232 - 16, 32, 32);


            var out_dir = $"E:\\SC\\Render\\sketto\\";
            if (!Directory.Exists(out_dir)) Directory.CreateDirectory(out_dir);
            var out_path = Path.Combine(out_dir, $"{name}.gpkg");
            if (File.Exists(out_path)) File.Delete(out_path);

            for (int renderScale = 32; renderScale <= 32; renderScale *= 2)
            {
                settings.TileSize = renderScale;
                Log.Information($"Rendering Planet {name} at {renderScale}px per tile");
                var render_result = renderer.RenderScaledPlanet(pd);

                render_result.RenderGeoPackage(out_path, $"{name}_{renderScale}px", IncludeTemp: true, IncludeHumid: true);
                
                render_result.ExportImageGDAL($"{out_dir}\\{renderScale}_surface.tiff", 
                    IncludeExport: true, ExportDriver: "PNG", ExportPath: $"{out_dir}\\{renderScale}_surface.png");
                render_result.ExportHeightmapGDAL($"{out_dir}\\{renderScale}_heightmap.tiff");
            }
        }

        private void PlanetRender2(string planetSocpak)
        {
            SOC_Archive archive = SOC_Archive.LoadFromGameDBSocPak(gameDatabase, planetSocpak, false, null);
            var archiveNameBase = Path.GetFileNameWithoutExtension(archive.Name);

            int climate_mip = 0;
            int lut_size = 128;
            int out_tile_size = 16;
            int heightmap_size_km = 4000;
            int splat_resize_factor = 5;
            string out_dir = @"E:\SC\hurston";



            if(PreloadedEcoSystems == null)
            {
                PreloadedEcoSystems = new Dictionary<Guid, (JObject, Bitmap)>();
                foreach(ZipEntry entry in gameDatabase.GamePack)
                {
                    try
                    {
                        if (Path.GetExtension(entry.Name) == ".eco")
                        {
                            var ecosystemText = gameDatabase.ReadPackedFile(entry.Name);
                            var ecosystemJson = JObject.Parse(ecosystemText);
                            var ecosystemGuid = Guid.Parse(ecosystemJson.Value<string>("GUID"));

                            var texturePathClimate = ecosystemJson.SelectToken("$.Textures.ecoSystemAlbedoTexture").Value<string>();
                            var texturePathFull = $"Data/Textures/planets/terrain/{texturePathClimate}.6";
                            var ecosystemImage = ExportUtils.ExtractImage(gameDatabase, texturePathFull);

                            //Pre-resize all ecosystem images to output tile size
                            var resizedImage = new Bitmap(out_tile_size, out_tile_size);
                            using(var g = Graphics.FromImage(resizedImage))
                            {
                                g.DrawImage(ecosystemImage, 0, 0, out_tile_size, out_tile_size);
                            }
                            ecosystemImage = resizedImage;

                            PreloadedEcoSystems[ecosystemGuid] = (ecosystemJson, ecosystemImage);
                            Log.Information($"Loaded EcoSystem: [{ecosystemGuid}] {ecosystemJson["name"]}");
                        }
                    }
                    catch { }
                }
            }

            if(!Directory.Exists(Path.Combine(out_dir, archiveNameBase)))
                Directory.CreateDirectory(Path.Combine(out_dir, archiveNameBase));

            void AdjustHSV(Bitmap bmp, float HueAdjust, float SaturationAdjust, float ValueAdjust)
            {
                for (int tile_y = 0; tile_y < bmp.Height; tile_y++)
                {
                    for (int tile_x = 0; tile_x < bmp.Width; tile_x++)
                    {
                        var pixelColor = bmp.GetPixel(tile_x, tile_y);
                        bmp.SetPixel(tile_x, tile_y,
                             Utilities.FromAhsb(
                                 pixelColor.A,
                                 Math.Min(1, Math.Max(0, pixelColor.GetHue() + HueAdjust)),
                                 Math.Min(1, Math.Max(0, pixelColor.GetSaturation() + SaturationAdjust)),
                                 Math.Min(1, Math.Max(0, pixelColor.GetBrightness() + ValueAdjust))));
                    }
                }
            }

            int LerpValue(int Min, int Max, int Value)
            {
                return (int)(((Max - Min) * (Value / 127.0f)) + Min);
            }

            (int, int, int) LerpColor(JArray GradientA, JArray GradientB, byte GradientValue)
            {
                return (
                    LerpValue(GradientA.Value<int>(0), GradientB.Value<int>(0), GradientValue),
                    LerpValue(GradientA.Value<int>(1), GradientB.Value<int>(1), GradientValue),
                    LerpValue(GradientA.Value<int>(2), GradientB.Value<int>(2), GradientValue));
            }

            void DrawGroundLayer(JArray array, int layer)
            {
                using (Bitmap groundLayer = new Bitmap(lut_size, lut_size))
                {
                    for (int y = 0; y < lut_size; y++)
                    {
                        for (int x = 0; x < lut_size; x++)
                        {
                            int texID = array[y][x].Value<int>();
                            bool texLayer = (texID & (1 << layer)) != 0;
                            int textVal = texLayer ? 0 : 255;

                            groundLayer.SetPixel(x, y, Color.FromArgb(255, textVal, textVal, textVal));
                        }
                    }
                    groundLayer.Save(Path.Combine(out_dir, archiveNameBase, $"ground_texture_layer_{layer}.png"));
                }
            }

            void DrawSplatLayer(JArray array, int layer, int splatSize)
            {
                using (Bitmap groundLayer = new Bitmap(splatSize, splatSize))
                {
                    for (int y = 0; y < splatSize; y++)
                    {
                        for (int x = 0; x < splatSize; x++)
                        {
                            int splatValue = array[(y * splatSize + x) * 4 + layer].Value<int>();
                            groundLayer.SetPixel(x, y, Color.FromArgb(255, splatValue, splatValue, splatValue));
                        }
                    }
                    groundLayer.Save(Path.Combine(out_dir, archiveNameBase, $"splat_{layer}.png"));
                }
            }

            Bitmap LoadSplatImage(JArray array, int splatSize)
            {
                Bitmap splatMap = new Bitmap(splatSize, splatSize);
                for (int y = 0; y < splatSize; y++)
                {
                    for (int x = 0; x < splatSize; x++)
                    {
                        int temperature = array[(y * splatSize + x) * 4 + 0].Value<byte>();
                        int humidity = array[(y * splatSize + x) * 4 + 1].Value<byte>();
                        int biome_id = array[(y * splatSize + x) * 4 + 2].Value<byte>();
                        splatMap.SetPixel(x, y, Color.FromArgb(255, temperature, humidity, biome_id));
                    }
                }
                return splatMap;
            }

            Image LoadOffsetImage(JArray array, int offsetSize)
            {
                Bitmap offsetMap = new Bitmap(offsetSize, offsetSize);
                for (int y = 0; y < offsetSize; y++)
                {
                    for (int x = 0; x < offsetSize; x++)
                    {
                        int value = array[y * offsetSize + x].Value<byte>();
                        offsetMap.SetPixel(x, y, Color.FromArgb(255, value, value, value));
                    }
                }
                return offsetMap;
            }

            archive.RootEntry.LoadAllEntities(true, false, false);
            if (archive.RootEntry.PlanetJSON != null)
            {
                JObject obj = JObject.Parse(archive.RootEntry.PlanetJSON);


                List<(JObject, Bitmap)> planet_ecosystems = new List<(JObject, Bitmap)>();
                
                int eco_id = 0;
                foreach(var ecosystem in obj.SelectTokens("$.data.General.uniqueEcoSystemsGUIDs[*]"))
                {
                    var ecosystem_guid = Guid.Parse(ecosystem.Value<string>());
                    var ecosystem_data = PreloadedEcoSystems[ecosystem_guid];
                    planet_ecosystems.Add(ecosystem_data);
                    Log.Information($"[{archiveNameBase}] - Eco #[{eco_id:N2}] = {ecosystem_data.Item1.SelectToken("$.name").Value<string>()}");
                    eco_id++;
                }
                


                string texture_root = @"E:\SC\hurston\Data\Textures\planets\surface\ground";

                var textures = obj.SelectTokens(".data.General.textureLayers.groundLayers[*].textureName").Values<string>().ToList();
                var textureLookup = textures.Distinct().ToDictionary(t => t, (t) =>
                {
                    var imagePath = Path.Combine(texture_root, t.Replace(".dds", ".png"));
                    if(File.Exists(imagePath))
                        return Image.FromFile(imagePath);
                    return null;
                });

                var tileFont = new Font(FontFamily.GenericSansSerif, 8);

                var splatWidth = obj.SelectToken("$.data.globalSplatWidth").Value<int>();
                var splatMap = obj.SelectToken("$.data.splatMap").Value<JArray>();

                var planet_radius_km = obj.SelectToken("$.data.General.tSphere.fPlanetTerrainRadius").Value<float>();
                var groundTextureLUT = obj.SelectToken("$.data.groundTexIDLUT").Value<JArray>();
                var heightmapPath = obj.SelectToken("$.data.General.tSphere.sHMWorld").Value<string>();
                var heightmapData = gameDatabase.ReadPackedFileBinary(heightmapPath);
                var heightmapText = ASCExporter.GenerateASCFromHeightmap(heightmapData);
                var heightmapSize = (int)Math.Sqrt(heightmapData.Length / 2);
                var half_circumference_km = (float)Math.PI * planet_radius_km;
                var heightmapSpan = new ReadOnlySpan<byte>(heightmapData);
                var dtr = Math.PI / 180.0;
                var rtd = 180 / Math.PI;

                using (var offset_img = LoadOffsetImage(
                    obj.SelectToken("$.data.randomOffset").Value<JArray>(),
                    obj.SelectToken("$.data.randomOffsetWidth").Value<int>()))
                using (var offset_resized = new Bitmap(offset_img.Width * 2, offset_img.Height))
                {
                    using (var g = Graphics.FromImage(offset_resized))
                    {
                        g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.NearestNeighbor;
                        g.DrawImage(offset_img, new Rectangle(0, 0, offset_resized.Width, offset_resized.Height));
                    }
                    offset_img.Save(Path.Combine(out_dir, archiveNameBase, $"offset_raw.png"));
                    offset_resized.Save(Path.Combine(out_dir, archiveNameBase, $"offset_scaled.png"));
                }


                var splat_img = LoadSplatImage(splatMap, splatWidth);
                //splat_img.Save(Path.Combine(out_dir, archiveNameBase, $"splat_raw.png"));

                File.WriteAllText(Path.Combine(out_dir, archiveNameBase, $"heightmap.asc"), heightmapText);

                DrawGroundLayer(groundTextureLUT, 0);
                DrawGroundLayer(groundTextureLUT, 1);
                DrawGroundLayer(groundTextureLUT, 2);
                DrawGroundLayer(groundTextureLUT, 3);
                DrawGroundLayer(groundTextureLUT, 4);
                DrawGroundLayer(groundTextureLUT, 5);
                DrawGroundLayer(groundTextureLUT, 6);
                DrawGroundLayer(groundTextureLUT, 7);

                //DrawSplatLayer(splatMap, 0, splatWidth);
                //DrawSplatLayer(splatMap, 1, splatWidth);
                //DrawSplatLayer(splatMap, 2, splatWidth);
                //DrawSplatLayer(splatMap, 3, splatWidth);

                using (Bitmap bmp = new Bitmap(heightmapSize, heightmapSize))
                {
                    for (int y = 0; y < heightmapSize; y++)
                    {
                        for (int x = 0; x < heightmapSize; x++)
                        {
                            ushort height = System.Buffers.Binary.BinaryPrimitives.ReadUInt16BigEndian(heightmapSpan.Slice((y * heightmapSize + x) * 2));

                            bmp.SetPixel(x, y, Color.FromArgb(255, height / 256, height / 256, height / 256));
                        }
                    }
                    bmp.Save(Path.Combine(out_dir, archiveNameBase, $"heightmap.png"));
                }

                Bitmap bedrock_colormap = new Bitmap(lut_size, lut_size);
                Bitmap surface_colormap = new Bitmap(lut_size, lut_size);
                Bitmap texture_layer_lut = new Bitmap(lut_size, lut_size);
                Bitmap nearGloss = new Bitmap(lut_size, lut_size);
                Bitmap bedrockGloss = new Bitmap(lut_size, lut_size);
                {
                    for (int y = 0; y < lut_size; y++)
                    {
                        for (int x = 0; x < lut_size; x++)
                        {
                            //Surface/bedrock colormap
                            var tile_brushID = obj.SelectToken($".data.brushIDLUT[{y}][{x}]").Value<int>();
                            var brushDataLUT = obj.SelectToken($".data.brushDataLUT[{y}][{x}]");
                            var ground_texture_id = obj.SelectToken($".data.groundTexIDLUT[{y}][{x}]").Value<int>();
                            var object_preset_id = obj.SelectToken($".data.objectPresetLUT[{y}][{x}]").Value<int>();
                            var brushData = obj.SelectToken($".data.General.brushes[{tile_brushID}]");
                            var texture_layer_id = brushDataLUT.Value<int>("texturLayerIndex");

                            var brushBedrockGradientA = brushData.SelectToken("bedrockBrush.colorGradient").Value<JArray>("gradientColorA");
                            var brushBedrockGradientB = brushData.SelectToken("bedrockBrush.colorGradient").Value<JArray>("gradientColorB");
                            var brushSurfaceGradientA = brushData.SelectToken("surfaceBrush.colorGradient").Value<JArray>("gradientColorA");
                            var brushSurfaceGradientB = brushData.SelectToken("surfaceBrush.colorGradient").Value<JArray>("gradientColorB");
                            var gradientValueBedrock = brushDataLUT.Value<byte>("gradientPosBedRock");
                            var gradientValueSurface = brushDataLUT.Value<byte>("gradientPosSurface");
                            var brushSurfaceValueOffset = brushDataLUT.Value<float>("valOffsetSurface");
                            var brushBedrockValueOffset = brushDataLUT.Value<float>("valOffsetBedRock");
                            var brushSurfaceSaturationOffset = brushDataLUT.Value<float>("satOffsetSurface");
                            var brushBedrockSaturationOffset = brushDataLUT.Value<float>("satOffsetBedRock");

                            var bedrockRGB = LerpColor(brushBedrockGradientA, brushBedrockGradientB, gradientValueBedrock);
                            var surfaceRGB = LerpColor(brushSurfaceGradientA, brushSurfaceGradientB, gradientValueSurface);

                            var bedrockColor = Color.FromArgb(bedrockRGB.Item1, bedrockRGB.Item2, bedrockRGB.Item3);
                            //.AdjustHSV(0, brushBedrockSaturationOffset, brushBedrockValueOffset);
                            var surfaceColor = Color.FromArgb(surfaceRGB.Item1, surfaceRGB.Item2, surfaceRGB.Item3);
                            //.AdjustHSV(0, brushSurfaceSaturationOffset, brushSurfaceValueOffset);

                            bedrock_colormap.SetPixel(x, y, bedrockColor);
                            surface_colormap.SetPixel(x, y, surfaceColor);


                            //Near color gloss (basically  surface colormap?)
                            var near_gloss = obj.SelectToken($".data.nearColorGlossLUT.mip0[{y}][{x}]");
                            int r = near_gloss.Value<int>(0);
                            int g = near_gloss.Value<int>(1);
                            int b = near_gloss.Value<int>(2);
                            int a = near_gloss.Value<int>(3);
                            nearGloss.SetPixel(x, y, Color.FromArgb(/*a*/255, r, g, b));

                            //Bedrock color gloss, same thing
                            var bedrock_gloss = obj.SelectToken($".data.bedrockColorGlossLUT.mip0[{y}][{x}]");
                            r = bedrock_gloss.Value<int>(0);
                            g = bedrock_gloss.Value<int>(1);
                            b = bedrock_gloss.Value<int>(2);
                            a = bedrock_gloss.Value<int>(3);
                            bedrockGloss.SetPixel(x, y, Color.FromArgb(/*a*/255, r, g, b));

                            texture_layer_lut.SetPixel(x, y, Color.FromArgb(255, texture_layer_id, ground_texture_id, tile_brushID));
                        }
                    }
                    surface_colormap.Save(Path.Combine(out_dir, archiveNameBase, "surface_colormap.png"));
                    bedrock_colormap.Save(Path.Combine(out_dir, archiveNameBase, "bedrock_colormap.png"));
                    nearGloss.Save(Path.Combine(out_dir, archiveNameBase, $"near_gloss.png"));
                    bedrockGloss.Save(Path.Combine(out_dir, archiveNameBase, $"bedrock_gloss.png"));
                }

                (Color bedrock, Color surface, Color texture_id) GetClimateColors(int ScaledTemp, int ScaleHumidity)
                {
                    Color bedrockColor, surfaceColor, textureID;

                    //lock (bedrock_colormap)
                    bedrockColor = bedrock_colormap.GetPixel(ScaleHumidity, ScaledTemp);

                    //lock (surface_colormap)
                    surfaceColor = surface_colormap.GetPixel(ScaleHumidity, ScaledTemp);

                    //lock (surface_colormap)
                    textureID = texture_layer_lut.GetPixel(ScaleHumidity, ScaledTemp);

                    return (bedrockColor, surfaceColor, textureID);
                }

                /*
                
                var splat_resized = new Bitmap(splat_img.Width * splat_resize_factor, splat_img.Height * splat_resize_factor);
                using(var g = Graphics.FromImage(splat_resized))
                {
                    g.DrawImage(splat_img, new Rectangle(0, 0, splat_resized.Width, splat_resized.Height));
                }
                int clim_size = splat_resized.Width;


                //Render surface map (fast-ish)
                Bitmap i_climate_color_surface = new Bitmap(clim_size, clim_size);
                Bitmap i_climate_color_bedrock = new Bitmap(clim_size, clim_size);
                using (Bitmap i_texture_lookup_ids = new Bitmap(clim_size, clim_size))
                {
                    for (int y = 0; y < clim_size; y++)
                    {
                        Console.WriteLine($"[{archiveNameBase}]: Rendering Row {y + 1}");

                        //Parallel.For(0, clim_size, x =>
                        for (int x = 0; x < clim_size; x++)
                        {
                            Color location_pixel;

                            //lock(splat_resized)
                            location_pixel = splat_resized.GetPixel(x, y);

                            int tempScaled = location_pixel.R / 2;
                            int humiScaled = location_pixel.G / 2;
                            //int biomeValue = biome_map[x, y] / 16;

                            var (bedrockColor, surfaceColor, textureIDs) = GetClimateColors(tempScaled, humiScaled);

                            int offset_x = x - (clim_size / 4);
                            if (offset_x < 0)
                                offset_x += clim_size;

                            //lock(i_climate_color_bedrock)
                            i_climate_color_bedrock.SetPixel(offset_x, y, bedrockColor);

                            //lock(i_climate_color_surface)
                            i_climate_color_surface.SetPixel(offset_x, y, surfaceColor);

                            //lock(i_texture_lookup_ids)
                            i_texture_lookup_ids.SetPixel(offset_x, y, textureIDs);
                        }
                        //);
                    }

                    i_climate_color_bedrock.Save(Path.Combine(out_dir, archiveNameBase, $"{archiveNameBase}_climate_bedrock_color.png"));
                    i_climate_color_surface.Save(Path.Combine(out_dir, archiveNameBase, $"{archiveNameBase}_climate_surface_color.png"));
                    i_texture_lookup_ids.Save(Path.Combine(out_dir, archiveNameBase, $"{archiveNameBase}_texture_id_surface.png"));

                    for (int bit = 0; bit < 8; bit++)
                    {
                        using (Bitmap i_texture_lookup_bit = new Bitmap(clim_size, clim_size))
                        {
                            for (int y = 0; y < clim_size; y++)
                            {
                                for (int x = 0; x < clim_size; x++)
                                {
                                    Color location_pixel;
                                    location_pixel = splat_resized.GetPixel(x, y);

                                    int tempScaled = location_pixel.R / 2;
                                    int humiScaled = location_pixel.G / 2;

                                    var (bedrockColor, surfaceColor, textureIDs) = GetClimateColors(tempScaled, humiScaled);
                                    var bitvalue = (textureIDs.G & 1 << bit) != 0 ? 255 : 0;
                                    i_texture_lookup_bit.SetPixel(x, y, Color.FromArgb(255, bitvalue, bitvalue, bitvalue));
                                }
                            }
                            i_texture_lookup_bit.Save(Path.Combine(out_dir, archiveNameBase, $"{archiveNameBase}_texture_id_{bit}_surface.png"));
                        }
                    }
                }
                */

                int final_image_size = splat_img.Width * out_tile_size;


                float CircumferenceAtDistanceFromEquator(float EquitorialDistance)
                {
                    var angle = (EquitorialDistance / half_circumference_km) * Math.PI; //Normalize to +/-0.5 * pi to get +/- half_pi
                    return (float)(Math.Cos(angle) * planet_radius_km * Math.PI * 2);
                }

                (float lat, float lon) PositionToLatLon(float LongitudinalDistance, float EquitorialDistance)
                {
                    var (vert_normalized, horiz_normalized) = NormalizedLocation(LongitudinalDistance, EquitorialDistance);

                    return (vert_normalized * 180, horiz_normalized * 360);
                }

                (float x, float y) NormalizedLocation(float HorizontalDistanceKM, float VerticalDistanceKM)
                {
                    var vert_normalized = ((VerticalDistanceKM / half_circumference_km) + 0.5f);
                    var vert_circ = CircumferenceAtDistanceFromEquator(VerticalDistanceKM);
                    var horiz_normalized = ((HorizontalDistanceKM / vert_circ) + 0.5f);

                    return (Math.Min(1.0f, Math.Max(0.0f, horiz_normalized)), 
                            Math.Min(1.0f, Math.Max(0.0f, vert_normalized))); //  0.0 - 1.0 for x,y
                }


                int planet_tiles = splat_img.Width;


                Bitmap ecoMap = Image.FromFile(@"E:\SC\hurston\test_biome_map.png") as Bitmap;


                using (Bitmap i_climate_tiles = new Bitmap(planet_tiles * out_tile_size, planet_tiles * out_tile_size))
                using (Bitmap i_tile_buffer = new Bitmap(out_tile_size, out_tile_size))
                using (Graphics g_climate_map = Graphics.FromImage(i_climate_tiles))
                using (Graphics g_buffer_tile = Graphics.FromImage(i_tile_buffer))
                {
                    //NOTE: This is at the circumference horizontally. divide by 2 for vertical
                    float km_image_scalar = (float)((i_climate_tiles.Width) / (planet_radius_km * 2 * Math.PI));

                    // Step 1: Fill background with scaled climate surface colors
                    //g_climate_map.DrawImage(i_climate_color_surface, 0, 0, i_climate_tiles.Width, i_climate_tiles.Height);

                    // Step 2: Draw borders around splat grid tiles
                    for (int y = 0; y < planet_tiles; y++)
                    {
                        for (int x = 0; x < planet_tiles; x++)
                        {
                            g_climate_map.DrawRectangle(Pens.Red, x * out_tile_size, y * out_tile_size, out_tile_size, out_tile_size);
                        }
                    }


                    using (Bitmap splat_resized = new Bitmap(planet_tiles * out_tile_size, planet_tiles * out_tile_size))
                    using (Graphics g = Graphics.FromImage(splat_resized))
                    {
                        //Scale up to output size so climate data is interpolated properly
                        g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.Bicubic;
                        g.DrawImage(splat_img, new Rectangle(0, 0, splat_resized.Width, splat_resized.Height));



                        // Step 4: Get indexed ecosystem, scale it, blend climate, re-render with algorithm
                        for (int tile_y = 0; tile_y < planet_tiles; tile_y++)
                        {
                            Console.WriteLine($"[{archiveNameBase}]: Rendering Row {tile_y + 1}/{planet_tiles}");

                            for (int tile_x = 0; tile_x < planet_tiles; tile_x++)
                            {
                                //Then determine the ecosystem to render
                                var eco_pixel = ecoMap.GetPixel(tile_x, tile_y);

                                if (eco_pixel.R >= planet_ecosystems.Count)
                                    continue;

                                var eco_data = planet_ecosystems[eco_pixel.R];

                                //TODO: Random rotating/blending
                                for (int out_y = 0; out_y < out_tile_size; out_y++)
                                {
                                    for (int out_x = 0; out_x < out_tile_size; out_x++)
                                    {
                                        int global_x = tile_x * out_tile_size + out_x;
                                        int global_y = tile_y * out_tile_size + out_y;

                                        //Get global climate interpolated at inner position
                                        Color global_climate_pixel;
                                        global_climate_pixel = splat_resized.GetPixel(global_x, global_y);

                                        int global_temp = global_climate_pixel.R / 2;
                                        int global_humid = global_climate_pixel.G / 2;

                                        //Determine local climate at position
                                        Color local_climate_pixel;
                                        local_climate_pixel = eco_data.Item2.GetPixel(out_x, out_y);

                                        //TODO: Apply scaling factor from  pla json
                                        //Do mixing with floats to preserve whatever precision we have left

                                        float local_temp = local_climate_pixel.R;
                                        float local_humid = local_climate_pixel.G;

                                        local_temp = (local_temp - 128.0f) / 8.0f;
                                        local_humid = (local_humid - 128.0f) / 8.0f;

                                        //Blend local climate with global climate
                                        //TODO: Proper algorithm
                                        var final_temp = (byte)Math.Max(0, Math.Min(127, global_temp + local_temp));
                                        var final_humid = (byte)Math.Max(0, Math.Min(127, global_humid + local_humid));

                                        int offset_x = global_x - (i_climate_tiles.Width / 4);
                                        if (offset_x < 0)
                                            offset_x += i_climate_tiles.Width;

                                        var (bedrockColor, surfaceColor, textureIDs) = GetClimateColors(final_temp, final_humid);

                                        i_climate_tiles.SetPixel(offset_x, global_y, surfaceColor);
                                    }
                                }
                            }
                        }
                    }

                    // Step 3: Divide planet into 4km x 4km chunks of height +/- 1km
                    // As described in this video: https://youtu.be/IfCc_aDNsAw?t=1340
                    // Do this by finding the angle of 4km along the vertical radius (half circumference)
                    // NOTE: This will leave some overlap

                    float equator_circumference = (float)(planet_radius_km * Math.PI * 2);
                    float vertical_start = (float)Math.Floor(((half_circumference_km / 2) - heightmap_size_km) / heightmap_size_km) * heightmap_size_km;
                    float half_size = heightmap_size_km / 2;

                    for (var vert_distance = -vertical_start; vert_distance <= vertical_start; vert_distance += heightmap_size_km)
                    {
                        var latitude_circuference_km = CircumferenceAtDistanceFromEquator(vert_distance);

                        float horizontal_start = (float)Math.Floor(((equator_circumference / 2) - heightmap_size_km) / heightmap_size_km) * heightmap_size_km;

                        if (latitude_circuference_km == 0)
                        {
                            //g_climate_map.DrawRectangle(Pens.Blue, 0, 0, i_climate_tiles.Width - 1, (float)km_image_scalar / 2 * heightmapSize / 2);
                            continue;
                        }

                        for (var horiz_distance = -horizontal_start; horiz_distance <= horizontal_start; horiz_distance += heightmap_size_km)
                        {
                            var (hnorm, vnorm) = NormalizedLocation(horiz_distance, vert_distance);

                            var (bl_x, bl_y) = NormalizedLocation(horiz_distance - half_size, vert_distance - half_size);
                            var (tl_x, tl_y) = NormalizedLocation(horiz_distance - half_size, vert_distance + half_size);
                            var (br_x, br_y) = NormalizedLocation(horiz_distance + half_size, vert_distance - half_size);
                            var (tr_x, tr_y) = NormalizedLocation(horiz_distance + half_size, vert_distance + half_size);

                            float w = i_climate_tiles.Width, h = i_climate_tiles.Height;

                            var col = Color.FromArgb((int)(255 * vnorm), (int)(255 * hnorm), 255);


                            g_climate_map.DrawPolygon(new Pen(col, 1.0f), new Point[]
                            {
                                new Point((int)(tl_x * w), (int)(tl_y * h)),
                                new Point((int)(tr_x * w), (int)(tr_y * h)),
                                new Point((int)(br_x * w), (int)(br_y * h)),
                                new Point((int)(bl_x * w), (int)(bl_y * h)),
                                new Point((int)(tl_x * w), (int)(tl_y * h)),
                            });
                        }
                    }



                    //for (int y = 0; y < planet_tiles; y++)
                    //{
                    //    Console.WriteLine($"[{archiveNameBase}]: Rendering Row {y + 1}");

                    //    //Parallel.For(export_start, export_end, x =>
                    //    for (int x = 0; x < planet_tiles; x++)
                    //    {
                    //        int tileX = x * out_tile_size, tileY = y * out_tile_size;
                    //        Color location_pixel;
                    //        location_pixel = splat_resized.GetPixel(x, y);
                    //        int tempScaled = location_pixel.R / 2;
                    //        int humiScaled = location_pixel.G / 2;
                    //        int biomeValue = location_pixel.B / 16;

                    //        lock (g_climate_map)
                    //        {
                    //            //g_buffer_tile.Clear(Color.Transparent);
                    //            //g_buffer_tile.DrawImage(layerImage, 0, 0, out_tile_size, out_tile_size);
                    //            //AdjustHSV(i_tile_buffer, 0, brushSurfaceSaturationOffset, brushSurfaceValueOffset);
                    //            //g_climate_map.DrawImage(i_tile_buffer, tileX, tileY, out_tile_size, out_tile_size);
                    //            g_climate_map.FillRectangle(new SolidBrush(surfaceColor), tileX, tileY, out_tile_size, out_tile_size);
                    //            g_climate_map.DrawRectangle(Pens.Red, tileX, tileY, out_tile_size - 1, out_tile_size - 1);
                    //            //g_climate_map.DrawString(textureName, tileFont, Brushes.Red, tileX + 5, tileY + 20);
                    //            g_climate_map.DrawString($"{biomeValue}", tileFont, Brushes.Red, tileX + 2, tileY + 2);
                    //        }
                    //    }
                    //    //});
                    //}

                    i_climate_tiles.Save(Path.Combine(out_dir, archiveNameBase, "climate_with_heightmaps.png"));
                }

                return;
                /*
                using (Bitmap i_climate_tiles = new Bitmap(export_size * out_tile_size, export_size * out_tile_size))
                using (Bitmap i_tile_buffer = new Bitmap(out_tile_size, out_tile_size))
                using (Graphics g_climate_map = Graphics.FromImage(i_climate_tiles))
                using (Graphics g_buffer_tile = Graphics.FromImage(i_tile_buffer))
                {
                    for (int y = export_start; y < export_end; y++)
                    {
                        Console.WriteLine($"[{archiveNameBase}]: Rendering Row {y + 1}");

                        //Parallel.For(export_start, export_end, x =>
                        for (int x = export_start + (clim_size / 4); x < export_end; x++)
                        {
                            int tileX = x * out_tile_size, tileY = y * out_tile_size;
                            
                            int tempScaled = temp_map[x, y] / 2;
                            int humiScaled = humidity_map[x, y] / 2;
                            int biomeValue = biome_map[x, y] / 16;


                            var bedrockColor = bedrock_colormap.GetPixel(tempScaled, humiScaled);
                            var surfaceColor = surface_colormap.GetPixel(tempScaled, humiScaled);

                            lock (g_climate_map)
                            {
                                //g_buffer_tile.Clear(Color.Transparent);
                                //g_buffer_tile.DrawImage(layerImage, 0, 0, out_tile_size, out_tile_size);
                                //AdjustHSV(i_tile_buffer, 0, brushSurfaceSaturationOffset, brushSurfaceValueOffset);
                                //g_climate_map.DrawImage(i_tile_buffer, tileX, tileY, out_tile_size, out_tile_size);
                                g_climate_map.FillRectangle(new SolidBrush(surfaceColor), tileX, tileY, out_tile_size, out_tile_size);
                                g_climate_map.DrawRectangle(Pens.Red, tileX, tileY, out_tile_size - 1, out_tile_size - 1);
                                //g_climate_map.DrawString(textureName, tileFont, Brushes.Red, tileX + 5, tileY + 20);
                                g_climate_map.DrawString($"{biomeValue}", tileFont, Brushes.Red, tileX + 2, tileY + 2);
                            }
                        }
                        //});
                    }

                    i_climate_tiles.Save(Path.Combine(out_dir, archiveNameBase, "climate_tiles.png"));
                }
                */


                foreach (var kvp in textureLookup)
                    kvp.Value?.Dispose();
            }
        }
    }
}