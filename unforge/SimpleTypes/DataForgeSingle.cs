﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace unforge
{
    public class DataForgeSingle : _DataForgeSerializable
    {
        public Single Value { get; set; }

        public DataForgeSingle(DataForge documentRoot)
            : base(documentRoot)
        {
            this.Value = this._br.ReadSingle();
        }

        public override String ToString()
        {
            return String.Format(CultureInfo.InvariantCulture, "{0}", this.Value);
        }

        public XmlElement Read()
        {
            var element = this.DocumentRoot.CreateElement("Single");
            var attribute = this.DocumentRoot.CreateAttribute("value");
            attribute.Value = this.Value.ToString(CultureInfo.InvariantCulture);
            element.Attributes.Append(attribute);
            return element;
        }
    }
}
