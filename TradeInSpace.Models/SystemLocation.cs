﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TradeInSpace.Models
{
    [GenerateTS]
    public class SystemLocation : GameRecord
    {
        private Enums.ServiceFlags _serviceFlags;

        public int? ParentBodyID { get; set; }
        public int? ParentLocationID { get; set; }

        public string Name { get; set; }
        public string Description { get; set; }


        [TSProperty(PropertyType = "any")]
        public string AdditionalProperties { get; set; }


        public bool IsHidden { get; set; }
        public int? QTDistance { get; set; }
        public Guid? StarmapID { get; set; }

        [Column(TypeName = "int")]
        public Enums.LocationType LocationType { get; set; }

        public Enums.ServiceFlags ServiceFlags
        {
            get => _serviceFlags;
            set
            {
                _serviceFlags = value;
                //Propagate changes to parent locations.
                if (ParentLocation != null) ParentLocation.ServiceFlags |= _serviceFlags;
            }
        }

        [JsonIgnore]
        [ForeignKey(nameof(ParentBodyID))]
        public virtual SystemLocation ParentBody { get; set; }

        [JsonIgnore]
        [ForeignKey(nameof(ParentLocationID))]
        public virtual SystemLocation ParentLocation { get; set; }

        [JsonIgnore]
        public virtual ICollection<SystemLocation> ChildLocations { get; set; }
        [JsonIgnore]
        public virtual ICollection<SystemLandingZone> LandingZones { get; set; }
        [JsonIgnore]
        public virtual ICollection<HarvestableLocation> Harvestables { get; set; }

        [JsonIgnore]
        public virtual ICollection<TradeShop> Shops { get; set; }
    }
}
