﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TradeInSpace.Models
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Enum)]
    public class GenerateTSAttribute : Attribute
    {
    }

    [AttributeUsage(AttributeTargets.Property)]
    public class TSPropertyAttribute : Attribute
    {
        public string PropertyType { get; set; }
    }
}
