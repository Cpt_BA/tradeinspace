﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TradeInSpace.Models
{
    [GenerateTS]
    public class Manufacturer : GameRecord
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string Code { get; set; }
        public string LogoName { get; set; }
    }
}
