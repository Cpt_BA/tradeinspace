﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TradeInSpace.Models
{
    public class GameRecord : DBRecord
    {
        [JsonIgnore]
        public int GameDatabaseID { get; set; }

        public string Key { get; set; }

        //[JsonIgnore]
        public Guid? SCRecordID { get; set; }

        [JsonIgnore]
        [ForeignKey(nameof(GameDatabaseID))]
        public virtual GameDatabase GameDB { get; set; }
    }
}
