﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TradeInSpace.Models
{
    class TradeRoute : DBRecord
    {
        public int TradeTableID { get; set; }
        public int BuyEntryID { get; set; }
        public int SellEntryID { get; set; }
        
        [ForeignKey(nameof(TradeTableID))]
        public virtual TradeTable Table { get; set; }
        [ForeignKey(nameof(BuyEntryID))]
        public virtual TradeTableEntry BuyEntry { get; set; }
        [ForeignKey(nameof(SellEntryID))]
        public virtual TradeTableEntry SellEntry { get; set; }
    }
}
