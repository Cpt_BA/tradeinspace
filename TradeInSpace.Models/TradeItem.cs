﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TradeInSpace.Models
{
    [GenerateTS]
    public class TradeItem : GameRecord
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string Category { get; set; }
        public bool Mineable { get; set; }
    }
}
