﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TradeInSpace.Models
{
    [GenerateTS]
    public class TradeTableEntry : DBRecord
    {
        [JsonIgnore]
        public int TradeTableID { get; set; }
        public int ItemID { get; set; }
        public int StationID { get; set; }

        [Column(TypeName = "int")]
        public Enums.TradeType Type { get; set; }
        public float MinUnitPrice { get; set; }
        public float MaxUnitPrice { get; set; }
        public float UnitCapacityRate { get; set; }
        public float UnitCapacityMax { get; set; }
        public bool BadTrade { get; set; }
        
        [JsonIgnore]
        [ForeignKey(nameof(TradeTableID))]
        public virtual TradeTable Table { get; set; }
        [JsonIgnore]
        [ForeignKey(nameof(ItemID))]
        public virtual TradeItem Item { get; set; }
        [JsonIgnore]
        [ForeignKey(nameof(StationID))]
        public virtual TradeShop Station { get; set; }
    }
}
