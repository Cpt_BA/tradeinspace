﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TradeInSpace.Models
{
    [GenerateTS]
    public class HarvestableLocation : GameRecord
    {
        public int? ParentLocationID { get; set; }
        public int? HarvestableID { get; set; }


        [JsonIgnore]
        [ForeignKey(nameof(ParentLocationID))]
        public virtual SystemLocation ParentLocation { get; set; }
        [JsonIgnore]
        [ForeignKey(nameof(HarvestableID))]
        public virtual Harvestable Harvestable { get; set; }

        public string Name { get; set; }
    }
}
