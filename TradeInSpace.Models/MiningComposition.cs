﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace TradeInSpace.Models
{
    [GenerateTS]
    public class MiningComposition : GameRecord
    {
        public string Name { get; set; }
        public int MinimumDistinct { get; set; }

        [JsonIgnore]
        public virtual ICollection<MiningCompositionEntry> Entries { get; set; }
    }

    [GenerateTS]
    public class MiningCompositionEntry : GameRecord
    {
        public int? CompositionID { get; set; }
        public int MiningElementID { get; set; }


        public float Probability { get; set; }
        public float MinPercentage { get; set; }
        public float MaxPercentage { get; set; }
        public float CurveExponent { get; set; }

        [JsonIgnore]
        [ForeignKey(nameof(MiningElementID))]
        public virtual MiningElement Element { get; set; }

        [JsonIgnore]
        [ForeignKey(nameof(CompositionID))]
        public virtual MiningComposition Composition { get; set; }
    }
}
