﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TradeInSpace.Models
{
    [GenerateTS]
    public class TradeEquipmentEntry : DBRecord
    {
        [JsonIgnore]
        public int TradeTableID { get; set; }
        public int EquipmentID { get; set; }
        public int StationID { get; set; }

        public float BuyPrice { get; set; }
        public float SellPrice { get; set; }
        public float Quantity { get; set; }
        public float? RentalDuration { get; set; }


        [JsonIgnore]
        [ForeignKey(nameof(TradeTableID))]
        public virtual TradeTable Table { get; set; }
        [JsonIgnore]
        [ForeignKey(nameof(EquipmentID))]
        public virtual GameEquipment Equipment { get; set; }
        [JsonIgnore]
        [ForeignKey(nameof(StationID))]
        public virtual TradeShop Station { get; set; }
    }
}
