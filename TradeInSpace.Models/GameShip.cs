﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using static TradeInSpace.Models.Enums;

namespace TradeInSpace.Models
{
    [GenerateTS]
    public class GameShip : GameRecord
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string Manufacturer { get; set; }
        public int? CargoCapacity { get; set; }
        public int? QuantumDriveSize { get; set; }
        public PadSize? Size { get; set; }
        public string ImageURL { get; set; }

        public int? ManufacturerID { get; set; }

        [JsonIgnore]
        [ForeignKey(nameof(ManufacturerID))]
        public Manufacturer _Manufacturer { get; set; }

        [TSProperty(PropertyType = "any")]
        public string AdditionalProperties { get; set; }
    }
}
