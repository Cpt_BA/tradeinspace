﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TradeInSpace.Models
{
    [GenerateTS]
    public class SystemLandingZone : GameRecord
    {
        public int? LocationID { get; set; }

        public string Name { get; set; }
        public string Description { get; set; }

        public int TPads { get; set; }
        public int XSPads { get; set; }
        public int SPads { get; set; }
        public int MPads { get; set; }
        public int LPads { get; set; }
        public int XLPads { get; set; }


        [JsonIgnore]
        [ForeignKey(nameof(LocationID))]
        public virtual SystemLocation Location { get; set; }
    }
}
