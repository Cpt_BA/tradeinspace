﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TradeInSpace.Models
{
    [GenerateTS]
    public class TradeTable : DBRecord
    {
        public int GameDatabaseID { get; set; }

        public DateTime TableCreated { get; set; }
        public DateTime TableUpdated { get; set; }


        [JsonIgnore]
        public string RelatedSheetID { get; set; }


        public string Name { get; set; }
        [Column(TypeName = "BIT")]
        public bool Active { get; set; }


        [JsonIgnore]
        [ForeignKey(nameof(GameDatabaseID))]
        public virtual GameDatabase GameDatabase { get; set; }
        public virtual IList<TradeTableEntry> Entries { get; set; }
        public virtual IList<TradeEquipmentEntry> EquipmentEntries { get; set; }
    }
}
