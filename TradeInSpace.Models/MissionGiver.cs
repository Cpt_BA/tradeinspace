﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace TradeInSpace.Models
{
    [GenerateTS]
    public class MissionGiver : GameRecord
    {
        public int? SystemLocationID { get; set; }

        public string Name { get; set; }
        public string Description { get; set; }

        public int InviteTimeout { get; set; }
        public int VisitTimeout { get; set; }
        public int ShortCooldown { get; set; }
        public int MediumCooldown { get; set; }
        public int LongCooldown { get; set; }

        public string Icon { get; set; }

        [TSProperty(PropertyType = "any")]
        public string AdditionalProperties { get; set; }


        [JsonIgnore]
        [ForeignKey(nameof(SystemLocationID))]
        public virtual SystemLocation SystemLocation { get; set; }

        [JsonIgnore]
        public virtual ICollection<MissionGiverEntry> Missions { get; set; }
    }
}
