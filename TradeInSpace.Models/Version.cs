﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TradeInSpace.Models
{
    [GenerateTS]
    public class Version : DBRecord
    {
        public int? GameDatabaseID { get; set; }

        public string Name { get; set; }
        public string Description { get; set; }
        public bool Enabled { get; set; }
        public bool Default { get; set; }

        public DateTime? ReleaseDate { get; set; }
        public DateTime? UpdateDate { get; set; }
        
        public virtual ICollection<GameDatabase> GameDatabases { get; set; }
    }
}
