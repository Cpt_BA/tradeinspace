﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TradeInSpace.Models
{
    [GenerateTS]
    public class MiningElement : GameRecord
    {
        public int TradeItemID { get; set; }
        public int? RefinedTradeItemID { get; set; }

        public float Instability { get; set; }
        public float Resistance { get; set; }
        public float WindowMidpoint { get; set; }
        public float WindowMidpointRandomness { get; set; }
        public float WindowMidpointThinness { get; set; }
        public float ExplosionMultiplier { get; set; }
        public float ClusterFactor { get; set; }

        [JsonIgnore]
        [ForeignKey(nameof(TradeItemID))]
        public virtual TradeItem TradeItem { get; set; }
        [JsonIgnore]
        [ForeignKey(nameof(RefinedTradeItemID))]
        public virtual TradeItem RefinedTradeItem { get; set; }
    }
}
