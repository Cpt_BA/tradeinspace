﻿using Microsoft.SqlServer.Types;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TradeInSpace.Models
{
    public class OSM_Area
    {
        public int ID { get; set; }
        public int BodyID { get; set; } = 0;
        public string Name { get; set; } = "test";
        public decimal? Elevation { get; set; } = 0;
        public decimal? Height { get; set; } = 0;

        public string Tags { get; set; }
        public int? AreaType { get; set; }

        public SqlGeometry Area { get; set; }
    }

    [Serializable]
    [JsonConverter(typeof(StringEnumConverter))]
    public enum AreaType
    {
        Point = 1,
        Line = 2,
        Polygon = 3
    }
}
