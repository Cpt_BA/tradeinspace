﻿using Microsoft.SqlServer.Types;
using System;
using System.Collections.Generic;
using System.Text;

namespace TradeInSpace.Models
{
    public class OSM_Waypoint
    {
        public int ID { get; set; }
        public int BodyID { get; set; } = 0;
        public string Name { get; set; } = "test";
        public decimal? Elevation { get; set; } = 0;
        public decimal? Height { get; set; } = 0;

        public SqlGeography Coordinate { get; set; }
    }
}
