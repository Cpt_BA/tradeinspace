﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace TradeInSpace.Models
{
    [GenerateTS]
    public class GameDatabase : DBRecord
    {
        public int? VersionID { get; set; }

        public string Name { get; set; }
        public string Description { get; set; }
        [JsonIgnore]
        public bool Enabled { get; set; } = false;


        public string DB_Version { get; set; }
        [JsonIgnore]
        public int DB_Build { get; set; }
        [JsonIgnore]
        public string DB_Channel { get; set; }
        [JsonIgnore]
        public DateTime? DB_BuildTime { get; set; }
        [JsonIgnore]
        public bool FinishedImporting { get; set; } = false;
        [JsonIgnore]
        public DateTime? ImportTime { get; set; }

        [JsonIgnore]
        public virtual Version Version { get; set; }

        public virtual ICollection<GameShip> Ships { get; set; }
        public virtual ICollection<GameEquipment> Equipment { get; set; }
        
        public virtual ICollection<TradeItem> TradeItems { get; set; }
        public virtual ICollection<TradeShop> TradeShops { get; set; }

        public virtual ICollection<SystemBody> SystemBodies { get; set; }
        public virtual ICollection<SystemLocation> SystemLocations { get; set; }
        public virtual ICollection<SystemLandingZone> LandingZones { get; set; }
        public virtual ICollection<Manufacturer> Manufacturers { get; set; }

        public virtual ICollection<MiningElement> MiningElements { get; set; }
        public virtual ICollection<HarvestableLocation> HarvestableLocations { get; set; }
        public virtual ICollection<Harvestable> Harvestables { get; set; }
        public virtual ICollection<SubHarvestable> SubHarvestables { get; set; }
        public virtual ICollection<MiningComposition> MiningCompositions { get; set; }
        public virtual ICollection<MiningCompositionEntry> MiningCompositionEntries { get; set; }

        [JsonIgnore]
        public virtual ICollection<TradeTable> TradeTables { get; set; }
        public virtual ICollection<MissionGiver> MissionGivers { get; set; }
        public virtual ICollection<MissionGiverScope> MissionGiverScopes { get; set; }
        public virtual ICollection<MissionGiverEntry> Missions { get; set; }
    }
}
