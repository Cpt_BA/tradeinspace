﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace TradeInSpace.Models
{
    [GenerateTS]
    public class MissionGiverScope : GameRecord
    {
        public string Name { get; set; }
        public string Description { get; set; }

        public double StandingInitial { get; set; }
        public double StandingMax { get; set; }

        public string IconSVG { get; set; }

        [TSProperty(PropertyType = "any")]
        public string AdditionalProperties { get; set; }
    }
}
