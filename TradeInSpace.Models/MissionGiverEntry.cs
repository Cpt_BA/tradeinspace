﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TradeInSpace.Models
{
    [GenerateTS]
    public class MissionGiverEntry : GameRecord
    {
        public int? GiverID { get; set; }
        public int? AvailableLocationID { get; set; }

        public bool SecretMission { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string MissionType { get; set; }
        public bool Lawful { get; set; }
        public string GiverName { get; set; }

        public int MinUEC { get; set; }
        public int MaxUEC { get; set; }
        public bool PaysBonus { get; set; }
        public int? BuyInUEC { get; set; }


        [TSProperty(PropertyType = "MissionRequirementDTO[]")]
        public string Requirements { get; set; }
        [TSProperty(PropertyType = "MissionRewardSetDTO[]")]
        public string RewardSets { get; set; }
        [TSProperty(PropertyType = "MissionNoteDTO[]")]
        public string Notes { get; set; }
        [JsonIgnore]
        [TSProperty]
        [NotMapped]
        public MissionRequirementDTO ReputationRequirement { get; set; }
        [JsonIgnore]
        [TSProperty]
        [NotMapped]
        public string RewardTypes { get; set; }
        [JsonIgnore]
        [TSProperty]
        [NotMapped]
        public string RewardTypesFull { get; set; }


        [JsonIgnore]
        [ForeignKey(nameof(GiverID))]
        public virtual MissionGiver Giver { get; set; }
        [JsonIgnore]
        [ForeignKey(nameof(AvailableLocationID))]
        public virtual SystemLocation AvailableLocation { get; set; }
    }

    [GenerateTS]
    public class MissionRequirementDTO
    {
        public string Title { get; set; }
        [JsonConverter(typeof(StringEnumConverter))]
        [TSProperty(PropertyType = "string")]
        public RequirementType Type { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public float? Min { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public float? Max { get; set; }
        [TSProperty(PropertyType = "string[]")]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string[] MissionKeys { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string TargetGiverKey { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string RepScope { get; set; }

        [JsonIgnore]
        public MissionScopeRankDTO MinScope { get; set; }
        [JsonIgnore]
        public MissionScopeRankDTO MaxScope { get; set; }
        [JsonIgnore]
        public MissionGiver TargetGiver { get; set; }
        [JsonIgnore]
        [TSProperty(PropertyType = "MissionGiverEntry[]")]
        public MissionGiverEntry[] Missions { get; set; }
    }

    [GenerateTS]
    public class MissionRewardSetDTO
    {
        public string Title { get; set; }
        public MissionRewardDTO[] Rewards { get; set; }
    }

    [GenerateTS]
    public class MissionScopeRankDTO
    {
        public string Title { get; set; }
        public int Tier { get; set; }
        public float Min { get; set; }
        public float Drift { get; set; }
        public float DriftHours { get; set; }
    }

    [GenerateTS]
    public class MissionRewardDTO
    {
        [JsonConverter(typeof(StringEnumConverter))]
        [TSProperty(PropertyType = "string")]
        public RewardType Type { get; set; }
        public float Amount { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public float? Bonus { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string TargetGiverKey { get; set; }
        public string RepScope { get; set; }

        [JsonIgnore]
        [TSProperty]
        public MissionGiver TargetGiver { get; set; }
        [JsonIgnore]
        [TSProperty]
        public MissionGiverScope Scope { get; set; }
    }

    [GenerateTS]
    public class MissionNoteDTO
    {
        public MissionNoteDTO() { }
        public MissionNoteDTO(string Title, string Type)
        {
            this.Type = Type;
            this.Title = Title;
        }

        public string Type { get; set; }
        public string Title { get; set; }
    }

    [GenerateTS]
    public enum RequirementType
    {
        Wanted,
        Virtue,
        Reliability,
        Standing,
        MissionInvite,
        MissionLinked,
        MissionAll,
        MissionAny,
        MissionNone,
        And,
        Or
    }

    [GenerateTS]
    public enum RewardType
    {
        Wanted,
        Virtue,
        Reliability,
        Standing
    }
}
