﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TradeInSpace.Models
{
    public class Enums
    {
        [GenerateTS]
        public enum BodyType
        {
            Planet,
            Moon,
            AsteroidField,
            Other,
            Star
        }

        [Flags]
        [GenerateTS]
        public enum ServiceFlags : int
        {
            None = 0x00,
            Equipment = ShipPurchase | ShipEquipment | FPSArmor | FPSWeapon | FPSEquipment | FPSClothing | FPSCrypto,


            Trade = 0x01,
            Refinery = 0x02,
            Fine = 0x04,
            PackageTerminal = 0x08,
            MissionGiver = 0x10,
            GreenZone = 0x20,

            ShipLanding = 0x40,
            ShipClaim = 0x80,
            ShipPurchase = 0x100,
            ShipEquipment = 0x200,
            ShipRental = 0x400,

            FPSArmor = 0x800,
            FPSWeapon = 0x1000,
            FPSEquipment = 0x2000,
            FPSClothing = 0x4000,
            FPSRespawnPoint = 0x8000,

            FPSFood = 0x10000,
            FPSCrypto = 0x20000,

            Harvestable = 0x40000,
            Hacking = 0x80000
        }

        [GenerateTS]
        public enum ShopType
        {
            Other,
            Ignore,

            AdminTerminal,
            AdminRefinery,
            FenceTerminal,
            CommExTransfers,
            TDD,
            Casaba,
            Restaurant,
            ConscientiousObjects,
            AstroArmada,
            VantageRentals,
            PlatinumBay,
            HDShowcase,
            Rentals,
            NewDeal,
            TammanyAndSons,
            BulwarkArmor,
            DumpersDepot,
            LiveFireWeapons,
            GarrityDefense,
            GrandBarter,
            Technotic,
            Teachs,
            Centermass,
            CubbyBlast,
            Skutters,
            TravelerRentals,
            CongreveWeapons,
            Cordrys,
            KCTrending,
            Aparelli,
            OmegaPro,
            RegalLuxury,
            ShubinInterstellar,
            FactoryLine,
            GiftShops,
            ConvenienceStore,

            Bar,
            BurritoBar,
            BurritoCart,
            CafeMusain,
            CoffeToGo,
            Ellroys,
            GarciaGreens,
            GLoc,
            HotdogBar,
            HotdogCart,
            JuiceBar,
            NoodleBar,
            Old38,
            PizzaBar,
            RacingBar,
            Twyns,
            Whammers,
            Wallys,
            MVBar,
            KelTo,

            CryAstro,
            Covalex,
            Stanton,
            LandingServices,
            ShipWeapons,
            FPSArmor,
            Food,
            CargoOffice,
            CargoOfficeRentals,
            Pharmacy,
            Makau,
            CousinCrows,
            CrusaderShowroom
        }

        [GenerateTS]
        public enum LocationType
        {
            Star,
            Planet,
            Moon,
            Asteroid,
            AsteroidField,
            LagrangePoint,
            LandingZone,
            Outpost,
            Manmade,
            Other,
            Store,
            PointOfInterest
        }

        [GenerateTS]
        public enum EquipmentType
        {
            Ship,
            GroundVehicle,

        }

        //Matches in-game LandingPadSize ID's
        [GenerateTS]
        public enum PadSize
        {
            Tiny = 1,
            XSmall = 2,
            Small = 3,
            Medium = 4,
            Large = 5,
            XLarge = 6
        }

        [GenerateTS]
        public enum TradeType
        {
            Buy = 0,
            Sell = 1
        }

        [GenerateTS]
        public enum ModifierActivation
        {
            Passive,
            Active
        }

        [GenerateTS]
        public enum ModifierMode
        {
            AdditivePre,
            AdditivePost,
            MultiplicitivePre,
            MultiplicitivePost,
        }

        [GenerateTS]
        public enum ModifierType
        {
            Heat,
            Signature,
            WeaponBeamStrengthVFX,
            MiningWindowLevel,
            MiningResistance,
            MiningInstability,
            MiningOptimalWindowSize,
            MiningShatterDamage,
            MiningOptimalWindowRate,
            MiningCatastrophicWindowRate,
            WeaponProjectileSpeed,
            WeaponFireRate,
            WeaponSpreadDecy,
            WeaponAmmoCost,
            WeaponHeatGeneration,
            WeaponSoundRadius,
            WeaponRecoilDecay,
            WeaponSpreadMin,
            WeaponSpreadMax,
            WeaponSpreadFirst,
            WeaponSpreadAttack,
            WeaponZoom,
            WeaponDamage,
            MiningFilter,

            SalvageSpeed,
            SalvageRadius,
            SalvageEfficiency
        }
    }
}
