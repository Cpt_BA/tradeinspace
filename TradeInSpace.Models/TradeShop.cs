﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TradeInSpace.Models
{
    [GenerateTS]
    public class TradeShop : GameRecord
    {
        public int? LocationID { get; set; }

        public string Name { get; set; }
        public string Description { get; set; }
        [Column(TypeName = "varchar(64)")]
        public Enums.ShopType ShopType { get; set; }
        public float ClosestApproachKM { get; set; }
        public int? TransitDurationS { get; set; }
        public string TransitNotes { get; set; }
        public int? TravelDurationS { get; set; }
        public string TravelNotes { get; set; }
        public int RunningDurationS { get; set; }

        [JsonIgnore]
        [ForeignKey(nameof(LocationID))]
        public virtual SystemLocation Location { get; set; }
        [JsonIgnore]
        public virtual ICollection<TradeTableEntry> TradeEntries { get; set; }
        [JsonIgnore]
        public virtual ICollection<TradeEquipmentEntry> EquipmentEntries { get; set; }
    }
}
