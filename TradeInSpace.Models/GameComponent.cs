﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace TradeInSpace.Models
{
    [GenerateTS]
    public class GameComponent : GameRecord
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string Manufacturer { get; set; }
        public int Size { get; set; }

        [JsonIgnore]
        public string AdditionalProperties { get; set; }
    }
}
