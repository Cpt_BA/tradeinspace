﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace TradeInSpace.Models
{

    [GenerateTS]
    public class Harvestable : GameRecord
    {
        public int? MiningElementID { get; set; }
        public int? MiningCompositionID { get; set; }

        public float MinElevation { get; set; }
        public float MaxElevation { get; set; }
        public float RespawnTime { get; set; }
        public int SubHarvestSlots { get; set; }
        public float SubHarvestableSlotChance { get; set; }
        public int ElementUnits { get; set; }

        [JsonIgnore, ForeignKey(nameof(MiningElementID))]
        public virtual MiningElement Element { get; set; }
        [JsonIgnore, ForeignKey(nameof(MiningCompositionID))]
        public virtual MiningComposition Composition { get; set; }

        [JsonIgnore]
        public virtual ICollection<HarvestableLocation> Locations { get; set; }
        [JsonIgnore]
        public virtual ICollection<SubHarvestable> SubHarvestables { get; set; }
        [JsonIgnore]
        public virtual ICollection<SubHarvestable> ParentHarvestables { get; set; }
    }

    [GenerateTS]
    public class SubHarvestable : GameRecord
    {
        public int ParentHarvestableID { get; set; }
        public int ChildHarvestableID { get; set; }
        public float RelativeProbability { get; set; }
        public float RespawnMultiplier { get; set; }

        [JsonIgnore]
        [ForeignKey(nameof(ParentHarvestableID))]
        public virtual Harvestable ParentHarvestable { get; set; }
        [JsonIgnore]
        [ForeignKey(nameof(ChildHarvestableID))]
        public virtual Harvestable ChildHarvestable { get; set; }
    }
}
