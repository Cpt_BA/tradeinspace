﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TradeInSpace.Models
{
    [GenerateTS]
    public class GameEquipment : GameRecord
    {
        public string Name { get; set; }
        public string Description { get; set; }

        public string AttachmentType { get; set; }
        public string AttachmentSubtype { get; set; }
        public string Manufacturer { get; set; }
        public int? BaseEquipmentID { get; set; }

        public int? ManufacturerID { get; set; }

        public int Size { get; set; }
        public int Grade { get; set; }
        public string Class { get; set; }
        public string Tags { get; set; }
        public string RequiredTags { get; set; }

        public float? Health { get; set; }
        public float? Mass { get; set; }

        [JsonIgnore]
        [ForeignKey(nameof(ManufacturerID))]
        public Manufacturer _Manufacturer { get; set; }

        [JsonIgnore]
        [ForeignKey(nameof(BaseEquipmentID))]
        public GameEquipment BaseEquipment { get; set; }

        //Client code transforms these from JSON to actual objects
        [TSProperty(PropertyType = "LoadoutSlotDTO")]
        public string PortLayout { get; set; }
        [TSProperty(PropertyType = "LoadoutEntryDTO[]")]
        public string DefaultLoadout { get; set; }
        [TSProperty(PropertyType = "EquipmentProperties")]
        public string Properties { get; set; }

        //Client-only fields
        [JsonIgnore, TSProperty, NotMapped]
        public float? CheapestPrice { get; set; }
        [JsonIgnore, TSProperty, NotMapped]
        public virtual ICollection<TradeEquipmentEntry> TradeEntries { get; set; }
        [JsonIgnore, TSProperty, NotMapped]
        public virtual ICollection<GameEquipment> DerivedEquipments { get; set; }
    }
}
