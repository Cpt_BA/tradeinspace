﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace TradeInSpace.Models
{
    [GenerateTS]
    public class SystemBody : GameRecord
    {
        public int? ParentBodyID { get; set; }
        public int? SystemLocationID { get; set; }

        public string Name { get; set; }
        public string Description { get; set; }
        public float OrbitRadius { get; set; }
        public float OrbitStart { get; set; }
        public float? AtmosphereHeightKM { get; set; }
        public float? BodyRadius { get; set; }
        public float? Gravity { get; set; }

        [Column(TypeName = "varchar(64)")]
        public Enums.BodyType BodyType { get; set; }
        [TSProperty(PropertyType = "any")]
        public string AdditionalProperties { get; set; }

        [JsonIgnore]
        [ForeignKey(nameof(ParentBodyID))]
        public virtual SystemBody ParentBody { get; set; }
        [JsonIgnore]
        [ForeignKey(nameof(SystemLocationID))]
        public virtual SystemLocation SystemLocation { get; set; }

        [JsonIgnore]
        public virtual ICollection<SystemBody> ChildBodies { get; set; }
    }
}
