# TradeInSpace Docker Resources

## Running prod import in docker


### Building updated docker file for importer
```
[DEV ]: docker build -t tis_import_dev -f Configuration/Dockerfile .
[PROD]: docker build -t tis_import_prod -f Configuration/Dockerfile_Prod .
```

### Full Import (Site DB + Map)
```
[DEV ]: docker run --name tis_import_dev --rm -v /home/cpt_ba/tis_import:/sc_data -v /mnt/nas/...:/sc_data/game/StarCitizen/LIVE tis_import_dev:latest sc_import_main
[PROD]: docker run --name tis_import_prod --rm -v /home/cpt_ba/tis_import:/sc_data -v /mnt/nas/...:/sc_data/game/StarCitizen/LIVE tis_import_prod:latest sc_import_main
```

### Full Import (Site DB + Map)
```
[DEV ]: docker run --name tis_import_dev --rm -v /home/cpt_ba/tis_import:/sc_data -v /mnt/nas/...:/sc_data/game/StarCitizen/LIVE tis_import_dev:latest import_all
[PROD]: docker run --name tis_import_prod --rm -v /home/cpt_ba/tis_import:/sc_data -v /mnt/nas/...:/sc_data/game/StarCitizen/LIVE tis_import_prod:latest import_all
```

### Map-Only
```
[DEV ]: docker run --name tis_import_dev --rm -v /home/cpt_ba/tis_import:/sc_data -v /mnt/nas/...:/sc_data/game/StarCitizen/LIVE tis_import_dev:latest generate_shape --target stantonsystem
[PROD]: docker run --name tis_import_prod --rm -v /home/cpt_ba/tis_import:/sc_data -v /mnt/nas/...:/sc_data/game/StarCitizen/LIVE tis_import_prod:latest generate_shape --target stantonsystem
```

### Model Render
```
[DEV ]: docker run --name tis_import_dev --rm -v /home/cpt_ba/tis_import:/sc_data -v /mnt/nas/...:/sc_data/game/StarCitizen/LIVE tis_import_dev:latest render_models --output /sc_data/Renders [--vehicle_skeleton] [--vehicle_model] [--equipment]
[PROD]: docker run --name tis_import_prod --rm -v /home/cpt_ba/tis_import:/sc_data -v /mnt/nas/...:/sc_data/game/StarCitizen/LIVE tis_import_prod:latest render_models --output /sc_data/Renders [--vehicle_skeleton] [--vehicle_model] [--equipment]
```

### Re-Seed new maps
```
1) docker exec -it full_docker_mapproxy_1 /bin/bash
2) cd /mapproxy
3) rm -rf sc_maps/cache_data/
4) ./seed_all.sh
```

