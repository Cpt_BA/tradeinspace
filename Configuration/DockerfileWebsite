FROM node:16.18 AS node_base

RUN echo "NODE Version:" && node --version
RUN echo "NPM Version:" && npm --version

# syntax=docker/dockerfile:1
FROM mcr.microsoft.com/dotnet/sdk:2.1 AS build-env
COPY --from=node_base . .
WORKDIR /app

# Copy csproj and restore as distinct layers
COPY *.sln .
COPY Cryengine-Converter ./Cryengine-Converter
COPY DataImport ./DataImport
COPY ICSharpCode.SharpZipLib ./ICSharpCode.SharpZipLib
COPY TradeInSpace.Data ./TradeInSpace.Data
COPY TradeInSpace.Explore ./TradeInSpace.Explore
COPY TradeInSpace.Models ./TradeInSpace.Models
COPY TradeInSpace.Export ./TradeInSpace.Export
COPY TradeInSpace.Parse ./TradeInSpace.Parse
COPY TradeInSpace ./TradeInSpace
COPY unforge ./unforge
WORKDIR /app/TradeInSpace
#RUN dotnet restore

ARG ASPNETCORE_ENVIRONMENT Release
ENV ASPNETCORE_ENVIRONMENT=${ASPNETCORE_ENVIRONMENT}

# Copy everything else and build
RUN dotnet publish -c Release -o /app/out

# Build runtime image
FROM mcr.microsoft.com/dotnet/sdk:2.1
COPY --from=node_base . .
WORKDIR /app
COPY --from=build-env /app/out .

RUN dotnet --version

EXPOSE 80
ENTRYPOINT ["dotnet", "TradeInSpace.dll"]
